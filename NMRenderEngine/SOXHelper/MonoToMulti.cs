﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.IO;
using System.Security.Permissions;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using System.Runtime.InteropServices;

[assembly:PermissionSetAttribute(SecurityAction.RequestMinimum, Name = "FullTrust")]


namespace SOXHelper
{
    public class MonoToMulti
    {
        private Process m_process = null;
        const int BUFFER_SIZE = 1024;
        public ManualResetEvent doneEvent = new ManualResetEvent(false);
        public RequestState rs = new RequestState();


        public class RequestState
        {
            const int BufferSize = BUFFER_SIZE;
            public StringBuilder RequestData;
            public byte[] BufferRead;
            public Process MyProcess;
            public Stream ResponseStream;
            public Decoder StreamDecode = Encoding.UTF8.GetDecoder();

            public RequestState()
            {
                BufferRead = new byte[BufferSize];
                RequestData = new StringBuilder(string.Empty);
                ResponseStream = null;
            }
        }
        public MonoToMulti()
        {
        }

        private string ConvertMs2Sec(ulong ms)
        {
            float res = (float)ms / 1000.0f;
            return res.ToString().Replace(",", ".");
        }

        public int Start(List <string> inputFileList, string outputFileName)
        {
            int ret = 0;

            string strAudios = "";
            foreach (string audioFileName in inputFileList)
                strAudios += " \"" + audioFileName + "\"";


            string Arguments = string.Format(@"-M {0} ""{1}"" -S", strAudios, outputFileName);


            try
            {
                if (File.Exists(outputFileName))
                    File.Delete(outputFileName);


                ProcessStartInfo si;
                si = new ProcessStartInfo("sox.exe", Arguments);
                si.UseShellExecute = false;
                si.RedirectStandardInput = false;
                si.RedirectStandardOutput = false;
                si.RedirectStandardError = false;
                si.CreateNoWindow = true;

                Process MyProcess = new Process();
                MyProcess = Process.Start(si);
                MyProcess.EnableRaisingEvents = true;


                MyProcess.WaitForExit();

                /*
                rs.MyProcess = MyProcess;
                rs.ResponseStream = MyProcess.StandardError.BaseStream;

                //WaitHandle[] waitHandles = new WaitHandle[1];
                //waitHandles[0] = WaitForExit();
                //WaitHandle.WaitAll(waitHandles,1000,false);

                rs.ResponseStream.BeginRead(rs.BufferRead, 0, BUFFER_SIZE, new AsyncCallback(ReadCallBack), rs);

                while (!MyProcess.HasExited)
                    Thread.Sleep(1000);

                if(rs.RequestData.Length>0)
                {
                    string strContent;
                    strContent = rs.RequestData.ToString();
                }
                rs.ResponseStream.Close();
                */
            }
            catch (Exception ex)
            {
                ret = -1;
            }
            finally
            {
            }

            return ret;
        }

        [MTAThread]
        public WaitHandle WaitForExit()
        {
            rs.ResponseStream.BeginRead(rs.BufferRead, 0,BUFFER_SIZE, new AsyncCallback(ReadCallBack), rs);
            return(doneEvent);
        }

        [MTAThread]
        private void ReadCallBack(IAsyncResult asyncResult)
        {
            try
            {
                RequestState rs = (RequestState)asyncResult.AsyncState;
                int read = rs.ResponseStream.EndRead(asyncResult);
                if (read > 0)
                {
                    char[] charBuffer = new char[BUFFER_SIZE];
                    int len = rs.StreamDecode.GetChars(rs.BufferRead, 0, rs.BufferRead.Length, charBuffer, 0);
                    string str = new string(charBuffer, 0, len);

                    rs.RequestData.Append(Encoding.ASCII.GetString(rs.BufferRead, 0, read));

                    int startIn = rs.RequestData.ToString().IndexOf("In:");
                    if (startIn >= 0)
                    {
                        string strPerc = rs.RequestData.ToString().Substring(startIn + 3, 4);
                        Console.WriteLine(strPerc);

                        rs.RequestData.Remove(0, startIn + 3);
                    }
                    rs.ResponseStream.BeginRead(rs.BufferRead, 0, BUFFER_SIZE, new AsyncCallback(ReadCallBack), rs);
                }
            }
            catch(Exception ex) { }

            return;
        }

    }


//}
//return;
//}
//}
//            int iPerc = 0;
//            try
//            {
//                int nByte = m_process.StandardError.BaseStream.EndRead(result);

//                int startPerc = temp2.LastIndexOf("In:");
//                string Perc = temp2.Substring(startPerc + 3, 4);

//                Perc = Perc.Replace("%", "");

//                iPerc = Convert.ToInt32(Perc);
//            }
//            catch { }


//        }

//        public void Stop()
//        {
//            try
//            {
//                if (m_process != null)
//                {
//                    m_process.Kill();
//                    m_process.Close();
//                    m_process = null;
//                }
//            }
//            catch { }
//        }

    
}
