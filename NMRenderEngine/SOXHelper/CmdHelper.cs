﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace SOXHelper
{
        public static class CommandLineHelper
        {
            public static string Run(string exe, string args, out string errorMessage)
            {
                return Run(exe, args, out errorMessage, null, null);
            }

            public static string Run(string exe, string command, StringDictionary stringDictionary)
            {
                return Run(exe, command, stringDictionary, null);
            }

            public static string Run(string command, string args, StringDictionary environmentVariables, string throwIfOutputStartsWith)
            {
                string errorMessage;
                string result = Run(command, args, out errorMessage, environmentVariables, throwIfOutputStartsWith);

                if (!string.IsNullOrEmpty(errorMessage))
                    throw new Exception(string.Format("Command {0} failed: {1}", args, errorMessage));

                return result;
            }

            public static string Run(string command, string args, out string errorMessage, StringDictionary environmentVariables, string throwIfOutputStartsWith)
            {
                if (string.IsNullOrEmpty(command))
                    throw new ArgumentException("can't be null or Empty", "command");
                if (!File.Exists(command))
                    throw new FileNotFoundException(command);

                if (environmentVariables == null)
                    environmentVariables = new StringDictionary();

                errorMessage = "";
                Process cmdLineProcess = new Process();
                using (cmdLineProcess)
                {
                    ProcessStartInfo startInfo = cmdLineProcess.StartInfo;
                    startInfo.FileName = command;
                    startInfo.Arguments = args;
                    startInfo.UseShellExecute = false;
                    startInfo.CreateNoWindow = true;
                    startInfo.RedirectStandardOutput = true;
                    startInfo.RedirectStandardError = true;
                    //startInfo.RedirectStandardInput = true;
                    foreach (string key in environmentVariables.Keys)
                        cmdLineProcess.StartInfo.EnvironmentVariables[key] = environmentVariables[key];

                    if (cmdLineProcess.Start())
                        return ReadOutput(cmdLineProcess, out errorMessage, throwIfOutputStartsWith);

                    throw new Exception(String.Format("Could not start command line process: {0}", command));
                }
            }

            private static string ReadOutput(Process process, out string errorMessage, string throwIfOutputStartsWith)
            {
                errorMessage = string.Empty;

                Encoding outEncoding = process.StandardError.CurrentEncoding;
                bool outDone = false;
                StringBuilder outSB = new StringBuilder();

                Encoding errEncoding = process.StandardOutput.CurrentEncoding;
                bool errDone = false;
                StringBuilder errSB = new StringBuilder();

                bool? throwStringEncountered = null;
                byte[] outBuffer = new byte[4096];
                bool outReady = true;
                AsyncCallback outCallBack = delegate(IAsyncResult sender)
                {
                    int bytesRead = process.StandardError.BaseStream.EndRead(sender);
                    if (bytesRead <= 0)
                    {
                        outDone = true;
                        return;
                    }
                    string s = outEncoding.GetString(outBuffer, 0, bytesRead);

                    outSB.Append(s);
                    Console.WriteLine(s);

                    if (throwIfOutputStartsWith != null && outSB.Length >= throwIfOutputStartsWith.Length)
                    {
                        throwStringEncountered = outSB.ToString().StartsWith(throwIfOutputStartsWith);
                    }
                    outReady = true;
                };

                byte[] errBuffer = new byte[4096];
                bool errReady = true;
                AsyncCallback errCallBack = delegate(IAsyncResult sender)
                {
                    int bytesRead = process.StandardOutput.BaseStream.EndRead(sender);
                    if (bytesRead <= 0)
                    {
                        errDone = true;
                        return;
                    }
                    string s = outEncoding.GetString(errBuffer, 0, bytesRead);
                    errSB.Append(s);
                    errReady = true;
                };

                while (!(outDone && errDone))
                {
                    if (outReady)
                    {
                        if (throwStringEncountered ?? false)
                        {
                            outDone = true;
                            process.Kill();
                        }
                        else
                        {
                            outReady = false;
                            process.StandardError.BaseStream.Flush();
                            IAsyncResult result = process.StandardError.BaseStream.BeginRead(outBuffer, 0, outBuffer.Length,
                                                                                                          outCallBack, null);
                        }
                    }
                    if (errReady)
                    {
                        errReady = false;
                        process.StandardOutput.BaseStream.BeginRead(errBuffer, 0, errBuffer.Length,
                                                                                                  errCallBack, null);
                    }
                    if (!(outDone && errDone))
                        Thread.Sleep(100);
                }

                if (throwStringEncountered ?? false)
                    throw new Exception(string.Format("Output starts with {0}", throwIfOutputStartsWith));

                errorMessage = errSB.ToString();
                return outSB.ToString();
            }
        }

}
