
[CustomMessages]
DependenciesDir=MyProgramDependencies
en.depdownload_title=Download dependencies
en.depinstall_title=Install dependencies
en.depinstall_status=Installing %1... (May take a few minutes)
en.depdownload_msg=The following applications are required before setup can continue:%n%1%nDownload and install now?
en.depinstall_missing=%1 must be installed before setup can continue. Please install %1 and run Setup again.


[Code]
type
	ProductsArray = array[0..3] of string;
var
	installMemo, downloadMemo, downloadMessage: string;
	nProduct , id, ResultCode: Integer;
	Products: ProductsArray;



procedure InstallPackage(PackageName, FileName, Title, Size, URL: string);
var
	path: string;
begin
    Products[nProduct] := FileName;
    nProduct := nProduct + 1;

	installMemo := installMemo + '%1' + Title + #13;


	SetIniString('install', PackageName, path, ExpandConstant('{tmp}{\}dep.ini'));
end;

function NextButtonClick(CurPage: Integer): Boolean;
var
	ErrorCode: Integer;
begin
	Result := true;

	if CurPage = wpReady then begin
		for id:= 0 to 1 do begin
			if Products[id] <> '' then begin

			//SuppressibleMsgBox(Products[id], mbConfirmation, MB_YESNO, IDYES)

			ShellExec('open', ExpandConstant('{src}\ProgramDependencies\'+ Products[id]),'', '', SW_SHOW, ewWaitUntilTerminated, ErrorCode);

			//	if (SuppressibleMsgBox(Products[id], mbConfirmation, MB_YESNO, IDYES) <> IDYES) then begin
			//		Result := false;
			//	end;
			end;
		end;
	end;
end;

function UpdateReadyMemo(Space, NewLine, MemoUserInfoInfo, MemoDirInfo, MemoTypeInfo, MemoComponentsInfo, MemoGroupInfo, MemoTasksInfo: String): String;
var
	s: string;
begin
	if downloadMemo <> '' then
		s := s + CustomMessage('depdownload_title') + ':' + NewLine + FmtMessage(downloadMemo, [Space]) + NewLine;
	if installMemo <> '' then
		s := s + CustomMessage('depinstall_title') + ':' + NewLine + FmtMessage(installMemo, [Space]) + NewLine;

	s := s + MemoDirInfo + NewLine + NewLine + MemoGroupInfo

	if MemoTasksInfo <> '' then
		s := s + NewLine + NewLine + MemoTasksInfo;

	Result := s
end;




























