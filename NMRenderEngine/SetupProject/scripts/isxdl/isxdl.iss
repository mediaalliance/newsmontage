[Files]
Source: "scripts\isxdl\isxdl.dll"; Flags: dontcopy

[Code]
procedure isxdl_AddFile(URL, Filename: PChar);
external 'isxdl_AddFile@files:isxdl.dll stdcall';

function isxdl_DownloadFiles(hWnd: Integer): Integer;
external 'isxdl_DownloadFiles@files:isxdl.dll stdcall';

function isxdl_SetOption(Option, Value: PChar): Integer;
external 'isxdl_SetOption@files:isxdl.dll stdcall';


type
	TProduct = record
		  File: String;
		  Title: String;
		  Parameters: String;
      InstallClean: Boolean;
      MustRebootAfter: Boolean;
      RequestRestart: Boolean;
	end;
	
var
	path: string;
	//products: array of TProduct;
  i: Integer;
  prods: array of TProduct;
	DependencyPage: TOutputProgressWizardPage;


procedure AddProduct(FileName, Parameters, Title, Size, URL: string; InstallClean : Boolean; MustRebootAfter : Boolean);
begin
    installMemo := installMemo + '%1' + Title + #13;
    
    // Step 1: search the 'DependenciesDir' first (locally)
    path := ExpandConstant('{src}{\}') + CustomMessage('DependenciesDir') + '\' + FileName;
    
    if not FileExists(path) then begin
        // Step 2: search the '{tmp}' dir (locally)
        // NOTE: you need to add the local programs in [Files] sections in your main Setup.iss, and
        //       use ExtractTemporaryFile('localprogram.exe') in InitializeSetup() function in Setup.iss
        path := ExpandConstant('{tmp}{\}') + FileName;
        
        if not FileExists(path) then begin
            // Step 3: download from the specified URL
            path := ExpandConstant('{tmp}{\}') + FileName;
            
            isxdl_AddFile(URL, path);
            
            downloadMemo := downloadMemo + '%1' + Title + #13;
            downloadMessage := downloadMessage + '    ' + Title + ' (' + Size + ')' + #13;
        end;
    end;        
    
    i := GetArrayLength(products);
    SetArrayLength(prods, i + 1);
    prods[i].File := path;
    prods[i].Title := Title;
    prods[i].Parameters := Parameters;
    prods[i].InstallClean := InstallClean;
    prods[i].MustRebootAfter := MustRebootAfter;
    prods[i].RequestRestart := false;
end;