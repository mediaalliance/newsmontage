// requires Windows Server 2003 Service Pack 1, Windows Server 2008, Windows Vista, Windows XP Service Pack 2
// requires windows installer 3.1
// WARNING: express setup (downloads and installs the components depending on your OS) if you want to deploy it on cd or network download the full bootsrapper on website below
// http://www.microsoft.com/downloads/details.aspx?FamilyID=ab99342f-5d1a-413d-8319-81da479ab0d7

[CustomMessages]
vcredist_x86_VS2008_title=Microsoft Visual C++ 2008 Redistributable Setup

en.vcredist_x86_VS2008_size=4 MB


[Run]
Filename: {ini:{tmp}{\}dep.ini,install,vcredist_x86_VS2008}; Description: {cm:vcredist_x86_VS2008_title}; StatusMsg: {cm:depinstall_status,{cm:vcredist_x86_VS2008_title}}; Parameters: "/q:a /t:{tmp}{\}vcredist_x86_VS2008 /c:""install /q /l /msipassthru MSI_PROP_BEGIN"" REBOOT=Suppress ""MSI_PROP_END"""; Flags: skipifdoesntexist
// Parameters: /lang:enu /quiet /norestart; Flags: skipifdoesntexist

[Code]
const
	vcredist_x86_VS2008_url = 'http://download.microsoft.com/download/1/1/1/1116b75a-9ec3-481a-a3c8-1777b5381140/vcredist_x86.exe';

procedure vcredist_x86_VS2008();
var
	version: cardinal;
begin
	//RegQueryDWordValue(HKLM, 'Software\Microsoft\NET Framework Setup\NDP\v3.5', 'SP', version);
	if IntToStr(version) < '1' then
		InstallPackage('vcredist_x86_VS2008', 'vcredist_x86.exe', CustomMessage('vcredist_x86_VS2008_title'), CustomMessage('vcredist_x86_VS2008_size'), vcredist_x86_VS2008_url);
end;
