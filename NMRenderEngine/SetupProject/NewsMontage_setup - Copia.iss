#include "scripts\products.iss"
#include "scripts\products\winversion.iss"
#include "scripts\products\fileversion.iss"
#include "scripts\products\msi31.iss"
#include "scripts\products\dotnetfx35sp1.iss"


#define GroupName "Media-Alliance"
#define MyAppName "NewsMontage Render Engine"
#define MyAppVerName "NewsMontage 1.5.15.3"
#define MyAppPublisher "Media-Alliance S.r.l."
#define MyAppURL "http://www.media-alliance.com"
#define MyAppExeName "NewsMontage.exe"

[CustomMessages]
win2000sp3_title=Windows 2000 Service Pack 3
winxpsp2_title=Windows XP Service Pack 2
winxpsp3_title=Windows XP Service Pack 3

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{481FEBF6-72F1-4FF9-9CFE-9A0290891FD3}
AppName={#MyAppName}
AppVerName={#MyAppVerName}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName=C:\{#GroupName}\{#MyAppName}
DefaultGroupName={#GroupName}
AllowNoIcons=false
InfoBeforeFile=
VersionInfoVersion=1.5.15.3
OutputDir=C:\DEVELOP\MediaAlliance\SOURCE\SETUP Application\NewsMontage\bin
OutputBaseFilename=NewsMontage_Setup1.5.15.3
SetupIconFile=C:\DEVELOP\MediaAlliance\SOURCE\SETUP Application\NewsMontage\application.ico
Compression=lzma
SolidCompression=true
UsePreviousGroup=false
WizardImageBackColor=clNavy
UsePreviousAppDir=false
WindowVisible=false
VersionInfoCompany=Media-Alliance S.r.l.
VersionInfoDescription=NewsMontage
VersionInfoTextVersion=1.5.15.3
VersionInfoProductName=NewsMontage
VersionInfoProductVersion=1.5.15.3
AppCopyright=Media-Alliance S.r.l.

[Languages]
Name: en; MessagesFile: compiler:Default.isl
;Name: fr; MessagesFile: compiler:Languages\French.isl
;Name: ge; MessagesFile: compiler:Languages\German.isl
;Name: it; MessagesFile: compiler:Languages\Italian.isl
;Name: ru; MessagesFile: compiler:Languages\Russian.isl
;Name: sp; MessagesFile: compiler:Languages\Spanish.isl

[Tasks]
Name: desktopicon; Description: {cm:CreateDesktopIcon}; GroupDescription: {cm:AdditionalIcons}; Flags: unchecked
Name: quicklaunchicon; Description: {cm:CreateQuickLaunchIcon}; GroupDescription: {cm:AdditionalIcons}; Flags: unchecked

[Files]
; NOTE: Don't use "Flags: ignoreversion" on any shared system files
Source: C:\DEVELOP\MediaAlliance\SOURCE\NewsMontage\NewsMontage\bin\Release\NewsMontage.exe; DestDir: {app}; Flags: ignoreversion
Source: C:\DEVELOP\MediaAlliance\SOURCE\NewsMontage\NewsMontage\bin\Release\ffmpeg.exe; DestDir: {app}; Flags: ignoreversion
Source: C:\DEVELOP\MediaAlliance\SOURCE\NewsMontage\NewsMontage\bin\Release\sox.exe; DestDir: {app}; Flags: ignoreversion
Source: C:\DEVELOP\MediaAlliance\SOURCE\NewsMontage\NewsMontage\bin\Release\application.ico; DestDir: {app}; Flags: ignoreversion
Source: C:\DEVELOP\MediaAlliance\SOURCE\NewsMontage\NewsMontage\bin\Release\*.dll; DestDir: {app}; Flags: ignoreversion
Source: C:\DEVELOP\MediaAlliance\SOURCE\NewsMontage\NewsMontage\bin\Release\QTSource.lic; DestDir: {app}; Flags: ignoreversion
Source: C:\DEVELOP\MediaAlliance\SOURCE\NewsMontage\NewsMontage\bin\Release\QTExporter.Wave16bit.Settings.plist; DestDir: {app}; Flags: ignoreversion
Source: C:\DEVELOP\MediaAlliance\SOURCE\NewsMontage\NewsMontage\bin\Release\README.TXT; DestDir: {app}; Flags: ignoreversion
Source: C:\DEVELOP\MediaAlliance\SOURCE\NewsMontage\NewsMontage\bin\Release\StartProcessForAudioInfo.bat; DestDir: {app}; Flags: ignoreversion
Source: C:\DEVELOP\MediaAlliance\SOURCE\NewsMontage\NewsMontage\bin\Release\Microsoft.*; DestDir: {app}; Flags: ignoreversion
Source: C:\DEVELOP\MediaAlliance\SOURCE\NewsMontage\NewsMontage\bin\Release\MASystemConfiguration.sys; DestDir: {app}; Flags: ignoreversion
Source: C:\DEVELOP\MediaAlliance\SOURCE\NewsMontage\NewsMontage\bin\Release\Registry\ffdshow_enc.reg; DestDir: {app}\Registry
Source: C:\DEVELOP\MediaAlliance\SOURCE\NewsMontage\NewsMontage\bin\Release\Registry\ffdshowenc.reg; DestDir: {app}\Registry
Source: C:\DEVELOP\MediaAlliance\SOURCE\NewsMontage\NewsMontage\bin\Release\Registry\MainConceptMXFDemultiplexer.reg; DestDir: {app}\Registry
Source: C:\DEVELOP\MediaAlliance\SOURCE\NewsMontage\NewsMontage\bin\Release\Registry\MainConceptMXFMPEG_Encoder-D10_30.reg; DestDir: {app}\Registry
Source: C:\DEVELOP\MediaAlliance\SOURCE\NewsMontage\NewsMontage\bin\Release\Registry\MainConceptMXFMPEG_Encoder-D10_50.reg; DestDir: {app}\Registry
Source: C:\DEVELOP\MediaAlliance\SOURCE\NewsMontage\NewsMontage\bin\Release\Registry\MainConceptMXFMPEG_Encoder-XDCAMHD_1920_50.reg; DestDir: {app}\Registry
Source: C:\DEVELOP\MediaAlliance\SOURCE\NewsMontage\NewsMontage\bin\Release\Registry\MainConceptMXFMPEG2_Decoder.reg; DestDir: {app}\Registry
Source: C:\DEVELOP\MediaAlliance\SOURCE\NewsMontage\NewsMontage\bin\Release\Registry\MainConceptMXFMultiplexer-Default.reg; DestDir: {app}\Registry
Source: C:\DEVELOP\MediaAlliance\SOURCE\NewsMontage\NewsMontage\bin\Release\Registry\MainConceptMXFMultiplexer-IMX.reg; DestDir: {app}\Registry
Source: C:\DEVELOP\MediaAlliance\SOURCE\NewsMontage\NewsMontage\bin\Release\Registry\MainConceptMXFMultiplexer-XDCAMHD.reg; DestDir: {app}\Registry
Source: C:\DEVELOP\MediaAlliance\SOURCE\SETUP Application\NewsMontage\QuickTimeInstaller.exe; DestDir: {app}; Flags: ignoreversion
Source: C:\DEVELOP\MediaAlliance\SOURCE\SETUP Application\NewsMontage\ffdshow_rev3562_20100907.exe; DestDir: {app}; Flags: ignoreversion
Source: C:\DEVELOP\MediaAlliance\SOURCE\SETUP Application\NewsMontage\quicktime_directshow_source_filter_1.7.2.4.exe; DestDir: {app}; Flags: ignoreversion


[Icons]
Name: {group}\{#MyAppName}; Filename: {app}\{#MyAppExeName}; IconFilename: {app}\application.ico; IconIndex: 0; WorkingDir: {app}; Tasks: ; Languages: 
Name: {group}\{cm:UninstallProgram,{#MyAppName}}; Filename: {uninstallexe}
Name: {commondesktop}\{#MyAppName}; Filename: {app}\{#MyAppExeName}; WorkingDir: {app}; Tasks: desktopicon; IconFilename: {app}\application.ico; IconIndex: 0
Name: {userappdata}\Microsoft\Internet Explorer\Quick Launch\{#MyAppName}; Filename: {app}\{#MyAppExeName}; WorkingDir: {app}; Tasks: quicklaunchicon; IconFilename: {app}\application.ico; IconIndex: 0
;Name: {group}\; Filename: {app}\application.ico; IconFilename: {app}\application.ico; IconIndex: 0

[Run]
Filename: {app}\QuickTimeInstaller.exe; WorkingDir: {app}; Tasks: ; Languages: ; Flags: postinstall waituntilterminated; Description: Installer of QuickTime Player 7.6.6
Filename: {app}\ffdshow_rev3562_20100907.exe; WorkingDir: {app}; Tasks: ; Languages: ; Flags: postinstall waituntilterminated; Description: Installer of FFDShow 20100907
Filename: {app}\quicktime_directshow_source_filter_1.7.2.4.exe; Flags: postinstall waituntilterminated; Description: MediaLooks QuickTime Filter 1.7.2.4; WorkingDir: {app}
Filename: {app}\{#MyAppExeName}; Description: {cm:LaunchProgram,{#MyAppName}}; WorkingDir: {app}; Flags: nowait postinstall skipifsilent unchecked




[UninstallDelete]
Name: {app}\*.*; Type: files; Tasks: ; Languages: 
Name: {app}\Projects; Type: filesandordirs
Name: {app}\NewsNetStories; Type: filesandordirs

[Dirs]
Name: {app}\Projects
Name: {app}\NewsNetStories
Name: {app}\Registry

[Code]
function InitializeSetup(): Boolean;
begin
	initwinversion();

	if (not minspversion(5, 0, 3)) then begin
		MsgBox(FmtMessage(CustomMessage('depinstall_missing'), [CustomMessage('win2000sp3_title')]), mbError, MB_OK);
		exit;
	end;
	if (not minspversion(5, 1, 2)) then begin
		MsgBox(FmtMessage(CustomMessage('depinstall_missing'), [CustomMessage('winxpsp3_title')]), mbError, MB_OK);
		exit;
	end;

	msi31('3.0');
	dotnetfx35sp1();

	Result := true;
end;



[_ISTool]
UseAbsolutePaths=true
[INI]
Filename: C:\{#GroupName}\{#MyAppName}\Settings.ini; Section: Application; Key: Language; String: en
Filename: C:\{#GroupName}\{#MyAppName}\Settings.ini; Section: Application; Key: NewsNetMediaXML; String: C:\{#GroupName}\{#MyAppName}\NewsNetStories
Filename: C:\{#GroupName}\{#MyAppName}\Settings.ini; Section: Application; Key: LocalMediaXML; String: LocalMedia.xml
Filename: C:\{#GroupName}\{#MyAppName}\Settings.ini; Section: Application; Key: ProjectsFolder; String: C:\{#GroupName}\{#MyAppName}\Projects
Filename: C:\{#GroupName}\{#MyAppName}\Settings.ini; Section: Application; Key: DestinationFolder; String: C:\{#GroupName}\{#MyAppName}\Video Samples
Filename: C:\{#GroupName}\{#MyAppName}\Settings.ini; Section: Application; Key: StartingScale; String: 1
Filename: C:\{#GroupName}\{#MyAppName}\Settings.ini; Section: Render; Key: Encoder; String: MainConcept MPEG-2 Video Encoder
Filename: C:\{#GroupName}\{#MyAppName}\Settings.ini; Section: Audio; Key: DeviceInfo; String: 0
Filename: C:\{#GroupName}\{#MyAppName}\Settings.ini; Section: Audio; Key: MaxTracks; String: 8
Filename: C:\{#GroupName}\{#MyAppName}\Settings.ini; Section: Audio; Key: BitsPerSample; String: 16
Filename: C:\{#GroupName}\{#MyAppName}\Settings.ini; Section: Audio; Key: SamplesPerSecond; String: 48000
Filename: C:\{#GroupName}\{#MyAppName}\Settings.ini; Section: Audio; Key: Channels; String: 2
Filename: C:\{#GroupName}\{#MyAppName}\Settings.ini; Section: Video; Key: Thumbnail; String: 0
Filename: C:\{#GroupName}\{#MyAppName}\Settings.ini; Section: VoiceTrackMapping; Key: SourceCoupleTracksIndex; String: 1
Filename: C:\{#GroupName}\{#MyAppName}\Settings.ini; Section: VoiceTrackMapping; Key: DestinationCoupleTracksIndex; String: 1
Filename: C:\{#GroupName}\{#MyAppName}\Settings.ini; Section: VoiceTrackMapping; Key: PCMSourceBitNumber; String: 16
Filename: C:\{#GroupName}\{#MyAppName}\Settings.ini; Section: Codec; Key: DV; String: ffdshow video encoder
Filename: C:\{#GroupName}\{#MyAppName}\Settings.ini; Section: Codec; Key: IMX; String: MainConcept MPEG-2 Video Encoder
[Registry]
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: lvCodecsSelected; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: lastPage; ValueData: $000000d5
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: dlgRestorePos; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: bpsDlgFPS1000; ValueData: $000061a8
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: dlgPosX; ValueData: $00000424
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: lvCodecsWidth0; ValueData: $00000064
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: lvCodecsWidth2; ValueData: $00000032
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: bpsDlgLen; ValueData: $000900b0
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: dlgEncCurrentPage; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: dlgPosY; ValueData: $000001aa
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: showGraph; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: lvCodecsWidth1; ValueData: $00000096
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: perfectDlgX; ValueData: $ffffffff
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: perfectDlgY; ValueData: $ffffffff
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: showHints; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: translateMode; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: multipleInstances; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: outputdebug; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: psnr; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: trayIcon; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: isBlacklist; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: isDyInterlaced; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: outputdebugfile; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: isCompMgr; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: dyInterlaced; ValueData: $00000120
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: addToROT; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: trayIconExt; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: errorbox1; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: isWhitelist; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: string; ValueName: whitelist; ValueData: "3wPlayer.exe;ACDSee10.exe;ACDSee11.exe;ACDSee5.exe;ACDSee6.exe;ACDSee7.exe;ACDSee8.exe;ACDSee8Pro.exe;ACDSee9.exe;ACDSeePro2.exe;ACDSeePro25.exe;acdseepro3.exe;Acer Crystal Eye webcam.exe;aegisub.exe;afreecaplayer.exe;afreecastudio.exe;AfterFX.exe;aim6.exe;aircamwin.exe;ALLPlayer.exe;allradio.exe;AlltoaviV4.exe;ALShow.exe;ALSong.exe;AltDVB.exe;amcap.exe;amf_slv.exe;amvtransform.exe;Apollo DivX to DVD Creator.exe;Apollo3GPVideoConverter.exe;Ares.exe;AsfTools.exe;ass_help3r.exe;ASUSDVD.exe;Audition.exe;AutoGK.exe;autorun.exe;avant.exe;AVerTV.exe;Avi2Dvd.exe;avi2mpg.exe;avicodec.exe;avipreview.exe;aviutl.exe;avs2avi.exe;Badak.exe;BearShare.exe;BePipe.exe;bestplayer.exe;bestplayer1.0.exe;bestpl~1.exe;BitComet.exe;BlazeDVD.exe;BoonPlayer.exe;bplay.exe;bsplay.exe;bsplayer.exe;BTVD3DShell.exe;Camfrog Video Chat.exe;CamRecorder.exe;CamtasiaStudio.exe;carom.exe;CEC_MAIN.exe;christv.exe;chrome.exe;cinemaplayer.exe;CinergyDVR.exe;CodecInstaller.exe;ConvertXtoDvd.exe;coolpro2.exe;CorePlayer.exe;Crystal.exe;crystalfree.exe;CrystalPro.exe;cscript.exe;CTCMS.exe;CTCMSU.exe;CTWave.exe;CTWave32.exe;cut_assistant.exe;dashboard.exe;demo32.exe;divx plus player.exe;DivX Player.exe;DivxToDVD.exe;dllhost.exe;dpgenc.exe;Dr.DivX.exe;drdivx.exe;drdivx2.exe;DreamMaker.exe;DSBrws.exe;DScaler.exe;dv.exe;dvbdream.exe;dvbviewer.exe;DVD Shrink 3.2.exe;DVDAuthor.exe;dvdfab.exe;DVDMaker.exe;DVDMF.exe;dvdplay.dll;dvdplay.exe;dvdSanta.exe;DXEffectTester.exe;DXEnum.exe;Easy RealMedia Tools.exe;ehExtHost.exe;ehshell.exe;Encode360.exe;explorer.exe;fenglei.exe;ffmpeg.exe;filtermanager.exe;firefox.exe;Flash.exe;FLVPlayer4Free.exe;FMRadio.exe;Fortius.exe;FreeStyle.exe;FSViewer.exe;Funshion.exe;FusionHDTV.exe;gameex.exe;GDivX Player.exe;gdsmux.exe;GoldWave.exe;gom.exe;GomEnc.exe;GoogleDesktop.exe;GoogleDesktopCrawl.exe;graphedit.exe;graphedt.exe;GraphStudio.exe;GraphStudio64.exe;gspot.exe;HBP.exe;HDVSplit.exe;hmplayer.exe;honestechTV.exe;HPWUCli.exe;i_view32.exe;ICQ.exe;ICQLite.exe;iexplore.exe;IHT.exe;IncMail.exe;InfoTool.exe;infotv.exe;InstallChecker.exe;Internet TV.exe;iPlayer.exe;ipod_video_converter.exe;IPODConverter.exe;JetAudio.exe;jwBrowser.exe;kiteplayer.exe;kmplayer.exe;KwMusic.exe;LA.exe;LifeCam.exe;LifeFrame.exe;Lilith.exe;makeAVIS.exe;MatroskaDiag.exe;Maxthon.exe;MDirect.exe;Media Center 12.exe;Media Jukebox.exe;Media Player Classic.exe;MediaLife.exe;MediaPortal.exe;MEDIAREVOLUTION.EXE;MediaServer.exe;megui.exe;mencoder.exe;Metacafe.exe;MMPlayer.exe;MovieMaker.exe;moviemk.exe;moviethumb.exe;MP4Converter.exe;Mp4Player.exe;mpc-hc.exe;mpc-hc64.exe;mpcstar.exe;MpegVideoWizard.exe;mplayer2.exe;mplayerc.exe;mplayerc64.exe;msnmsgr.exe;msoobe.exe;MultimediaPlayer.exe;Munite.exe;MusicManager.exe;Muzikbrowzer.exe;Mv2PlayerPlus.exe;My Movies.exe;myplayer.exe;nero.exe;NeroHome.exe;NeroVision.exe;NicoPlayer.exe;NMSTranscoder.exe;nvplayer.exe;Omgjbox.exe;OnlineTV.exe;Opera.exe;OrbStreamerClient.exe;PaintDotNet.exe;paltalk.exe;pcwmp.exe;PhotoScreensaver.scr;Photoshop.exe;Picasa2.exe;playwnd.exe;PowerDirector.exe;powerdvd.exe;POWERPNT.EXE;PPLive.exe;ppmate.exe;PPStream.exe;PQDVD_PSP.exe;Procoder2.exe;Producer.exe;progdvb.exe;ProgDvbNet.exe;PVCR.exe;Qonoha.exe;QQ.exe;QQLive.exe;QQMusic.exe;QQPlayerSvr.exe;QvodPlayer.exe;QzoneMusic.exe;RadLight.exe;realplay.exe;ReClockHelper.dll;Recode.exe;RecordingManager.exe;ripbot264.exe;rlkernel.exe;RoxMediaDB10.exe;RoxMediaDB9.exe;rundll32.exe;Safari.exe;SelfMV.exe;Shareaza.exe;sherlock2.exe;ShowTime.exe;sidebar.exe;SinkuHadouken.exe;Sleipnir.exe;smartmovie.exe;soffice.bin;songbird.exe;SopCast.exe;SplitCam.exe;START.EXE;staxrip.exe;stillcap.exe;Studio.exe;subedit.exe;SubtitleEdit.exe;SubtitleWorkshop.exe;SubtitleWorkshop4.exe;SWFConverter.exe;telewizja.exe;TheaterTek DVD.exe;time_adjuster.exe;timecodec.exe;tmc.exe;TMPGEnc.exe;TMPGEnc4XP.exe;TOTALCMD.EXE;TSPlayer.exe;Tvants.exe;tvc.exe;TVersity.exe;TVPlayer.exe;TVUPlayer.exe;UCC.exe;Ultra EDIT.exe;UUSeePlayer.exe;VCD_PLAY.EXE;VeohClient.exe;VFAPIFrameServer.exe;VideoConvert.exe;videoconverter.exe;videoenc.exe;VideoManager.exe;VideoSnapshot.exe;VideoSplitter.exe;VIDEOS~1.SCR;VideoWave9.exe;ViPlay.exe;ViPlay3.exe;ViPlay4.exe;virtualdub.exe;virtualdubmod.exe;vplayer.exe;WaveChk.exe;WCreator.exe;WebMediaPlayer.exe;WFTV.exe;winamp.exe;WinAVI 9.0.exe;WinAVI MP4 Converter.exe;WinAVI.exe;WindowsPhotoGallery.exe;windvd.exe;WinDvr.exe;WinMPGVideoConvert.exe;WINWORD.EXE;WLXPhotoGallery.exe;wmenc.exe;wmplayer.exe;wmprph.exe;wscript.exe;x264.exe;XNVIEW.EXE;Xvid4PSP.exe;zplayer.exe;Zune.exe;"
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: string; ValueName: debugfile; ValueData: \ffdebug.log
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: string; ValueName: blacklist; ValueData: "age3.exe;AvatarMP.exe;BeachSoccer.exe;Bully.exe;cm2008.exe;cm2010.exe;cmr4.exe;cmr5.exe;conflictdeniedops.exe;conquer.exe;crazy_taxi_pc.exe;daoriginslauncher.exe;dark_horizon.exe;DaybreakDX.exe;DisciplesIII.exe;divinity2.exe;dmc3se.exe;dragonica.exe;drakensang.exe;drome racers.exe;em3.exe;em4.exe;empires_dmw.exe;engine.exe;fable.exe;Edition.exe;FalconAF.exe;fallout3.exe;ff7.exe;fordorr.exe;fr2.exe;fr3.exe;freedom.exe;fsr.exe;game.bin;game.exe;Grandia2.exe;gta-vc.exe;gta3.exe;gta_sa.exe;gta-underground.exe;gta t�rk.exe;guildII.exe;hl.exe;hl2.exe;juiced.exe;juiced2_hin.exe;king.exe;launch.exe;launcher.exe;ldd.exe;legoracers.exe;lf2.exe;Main.exe;MassEffectLauncher.exe;MassEffect2Launcher.exe;maxpayne.exe;mixcraft.exe;mixcraft3.exe;mixcraft4.exe;mixcra~1.exe;morrowind.exe;msninst.exe;nations.exe;ninjablade.exe;oblivion.exe;outlook.exe;patriots.exe;pes2008.exe;pes2009.exe;pes2010.exe;pes4.exe;pes5.exe;pes6.exe;powerdvd8.exe;powerdvd9.exe;powerdvd10.exe;power2go.exe;premiere.exe;pwo.exe;questviewer.exe;rct.exe;rct3.exe;rct3plus.exe;residentevil3.exe;rf.exe;rf_online.bin;RomeGame.exe;rometw.exe;sacred.exe;sega rally.exe;sh3.exe;sh4.exe;shattered_horizon.exe;shippingpc-skygame.exe;sims.exe;simtractor.exe;skin.exe;smartlogon.exe;starsword.exe;StreetFighterIV.exe;streetlegal_redline.exe;SWRepublicCommando.exe;tw2008.exe;twoworlds.exe;vmcExtenderSetup.exe;wa.exe;war3.exe;worms 4 mayhem.exe;worms3d.exe;wwp.exe;Yahoo.Messenger.YmApp.exe;yahoom~1.exe;YahooMessenger.exe;YahooMusicEngine.exe;yahoow~1.exe;YahooWidgetEngine.exe;YahooWidgets.exe;ymini.exe;zoo.exe;"
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: packedBitstream; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: ffv1_csp; ValueData: $32315659
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: desiredSize; ValueData: $0008b290
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: dv_profile; ValueData: $00000004
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_intra4x4C_custom3; ValueData: $10101010
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_inter_custom10; ValueData: $18171615
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_intra_custom3; ValueData: $1c1b1917
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_intra_custom12; ValueData: $1e1c1a19
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: credits_mode; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: trellisquant; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: ff1_vqcomp; ValueData: $00000032
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: me_hq; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: huffyuv_pred; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: wmv9_aviout; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: ffv1_key_interval; ValueData: $0000000a
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: x264_mixed_ref; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qns; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: ff1_rc_min_rate1000; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_inter_custom2; ValueData: $14131211
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: is_lavc_nr; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: graycredits; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: kfreduction; ValueData: $0000001e
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: xvid_rc_averaging_period; ValueData: $00000064
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: me_subcmp; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: dia_size_pre; ValueData: $00000004
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: storeExt; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: x264_i_direct_mv_pred; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: skalSearchMetric; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: x264_me_inter2; ValueData: $00000113
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: is_ff_lumi_masking; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: twopass_max_overflow_degradation; ValueData: $0000003c
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: alt_curve_type; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: creditsEndBegin; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: xvid_vhq; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: interlacing; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: x264_b_aud; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_inter4x4Y_custom2; ValueData: $10101010
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: x264_i_deblocking_filter_alphac0; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: is_ff_p_masking; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: alt_curve_use_auto_bonus_bias; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: q_p_max; ValueData: $0000001f
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: ff1_stats_mode; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: dx50bvop; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: wmv9_kfsecs; ValueData: $00000006
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: quant; ValueData: $00000002
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_inter4x4C_custom0; ValueData: $10101010
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_inter_custom11; ValueData: $1e1c1b1a
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_intra_custom4; ValueData: $17161514
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_intra_custom13; ValueData: $29262320
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: mp2e_mpeg2_dc_prec; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: credits_percent; ValueData: $00000014
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: b_quant_factor; ValueData: $0000007d
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: ff1_vqblur1; ValueData: $00000032
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: me_4mv; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: ffv1_coder; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: forceIncsp; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: darY1000; ValueData: $00000bb8
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: ljpeg_csp; ValueData: $32315659
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: ff1_rc_buffer_size; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_inter_custom3; ValueData: $18171615
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: lavc_nr; ValueData: $00000064
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: curve_compression_high; ValueData: $00000019
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: xvid_rc_buffer; ValueData: $00000064
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: me_subcmp_chroma; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: me_subq; ValueData: $00000008
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: is_ff_border_masking; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: H263Pflags; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: x264_me_intra2; ValueData: $00000003
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: ff_lumi_masking1000; ValueData: $00000046
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: keyframe_boost; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: alt_curve_high_dist; ValueData: $000001f4
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: q_i_min; ValueData: $00000002
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: xvid_vhq_modedecisionbits; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: gray; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: x264_me_method; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_inter4x4Y_custom3; ValueData: $10101010
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: x264_i_deblocking_filter_beta; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_intra_custom8; ValueData: $1a181716
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: ff_p_masking1000; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: alt_curve_bonus_bias; ValueData: $00000032
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: q_b_min; ValueData: $00000002
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: aspectMode; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: wmv9_ivtc; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qual; ValueData: $00000055
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_inter4x4C_custom1; ValueData: $10101010
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_inter_custom12; ValueData: $1a181716
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_intra_custom5; ValueData: $1e1c1a18
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_intra_custom14; ValueData: $201e1c1b
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: me_mv0; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: credits_quant_i; ValueData: $00000014
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: b_quant_offset; ValueData: $0000007d
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: ff1_vqblur2; ValueData: $00000032
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: me_qpel; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: me_last_predictor_count; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: incsp; ValueData: $32315659
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: theo_sharpness; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: svcd_scan_offset; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_inter_custom4; ValueData: $15141312
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: isElimLum; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: curve_compression_low; ValueData: $0000000a
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: isCreditsStart; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: mb_cmp; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: xvid_motion_search; ValueData: $00000005
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: muxer; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: ff_border_masking1000; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_intra4x4Y_custom2; ValueData: $10101010
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: me_nsse_weight; ValueData: $00000008
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: is_ff_temporal_cplx_masking; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: kftreshold; ValueData: $0000000a
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: alt_curve_low_dist; ValueData: $0000005a
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: q_i_max; ValueData: $0000001f
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: is_xvid_vhq_custom; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: isBframes; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: b_refine; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: x264_me_range; ValueData: $00000010
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_intra4x4C_custom0; ValueData: $10101010
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: darX1000; ValueData: $00000fa0
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_intra_custom0; ValueData: $13121108
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_intra_custom9; ValueData: $23201e1c
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: is_ff_dark_masking; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: xvid_lum_masking; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: q_b_max; ValueData: $0000001f
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: intraQuantBias; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: isFPSoverride; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: sarX1000; ValueData: $000003e8
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: wmv9_deint; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: codecId; ValueData: $00000019
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_inter4x4C_custom2; ValueData: $10101010
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_inter_custom13; ValueData: $1f1e1c1b
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_intra_custom6; ValueData: $18171615
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_intra_custom15; ValueData: $2d292623
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: me_cbp_rd; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: credits_quant_p; ValueData: $00000014
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: isInterQuantBias; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: ff1_vqdiff; ValueData: $00000003
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: me_gmc; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: me_prepass; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: isProc; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: theo_noisesensitivity; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: x264_max_ref_frames; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_inter_custom5; ValueData: $19181716
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: x264_interlaced; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: elimLumThres; ValueData: $fffffffc
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: xvid2pass_use_write; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: bitrate_payback_delay; ValueData: $000000fa
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: isCreditsEnd; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: mb_cmp_chroma; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: is_xvid_me_custom; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: min_key_interval; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: huffyuv_ctx; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_intra4x4Y_custom3; ValueData: $10101010
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: numthreads; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: ff_temporal_cplx_masking1000; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: alt_curve_use_auto; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: i_quant_factor; ValueData: $ffffffb0
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: xvid_vhq_custom; ValueData: $00003f00
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: max_b_frames; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: mode; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_intra4x4Y_custom0; ValueData: $10101010
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_intra4x4C_custom1; ValueData: $10101010
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_inter_custom8; ValueData: $17161514
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_intra_custom1; ValueData: $1b191715
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_intra_custom10; ValueData: $1c1a1817
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: ff_dark_masking1000; ValueData: $00000064
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: xvid_chromaopt; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: q_mb_min; ValueData: $00000002
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: dct_algo; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: fpsOverride1000; ValueData: $000061a8
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: sarY1000; ValueData: $000003e8
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: wmv9_cplx; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: fourcc; ValueData: $30357664
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_inter4x4C_custom3; ValueData: $10101010
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_inter_custom14; ValueData: $1b191817
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_intra_custom7; ValueData: $201e1c1a
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_inter_custom0; ValueData: $13121110
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: theo_hq; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: credits_size_start; ValueData: $00002710
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: interQuantBias; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: ff1_rc_squish; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: me_cmp; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: me_precmp; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: flip; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: dropFrames; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: isSkalMasking; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: x264_cabac; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_inter_custom6; ValueData: $16151413
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: isElimChrom; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: twopass_max_bitrate; ValueData: $00989680
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: bitrate_payback_method; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: creditsStartBegin; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: dia_size; ValueData: $00000004
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: xvid_me_custom; ValueData: $028a0000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: globalHeader; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: x264_aq_strength100; ValueData: $0000001e
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: x264_mv_range; ValueData: $000001ff
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_inter4x4Y_custom0; ValueData: $10101010
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: x264_me_subpelrefine; ValueData: $00000005
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: is_ff_spatial_cplx_masking; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: alt_curve_auto_str; ValueData: $0000001e
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: i_quant_offset; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: quant_type; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: b_dynamic; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: ffv1_context; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: bitrate1000; ValueData: $00000384
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: me_iterative; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_intra4x4Y_custom1; ValueData: $10101010
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_intra4x4C_custom2; ValueData: $10101010
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_inter_custom9; ValueData: $1c1b1a19
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_intra_custom2; ValueData: $15131211
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_intra_custom11; ValueData: $2623201e
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: ff_naq; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: creditsEndEnd; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: q_mb_max; ValueData: $0000001f
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: ff1_vratetol; ValueData: $00000400
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: fpsOverrideDen; ValueData: $000003e8
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: huffyuv_csp; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: wmv9_crisp; ValueData: $00000050
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: max_key_interval; ValueData: $000000fa
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: interlacing_tff; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_inter_custom15; ValueData: $211f1e1c
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: ff1_rc_max_rate1000; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_inter_custom1; ValueData: $17161514
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: raw_fourcc; ValueData: $32424752
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: credits_size_end; ValueData: $00002710
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: isIntraQuantBias; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: xvid_rc_reaction_delay_factor; ValueData: $00000010
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: me_cmp_chroma; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: me_precmp_chroma; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: storeAVI; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: keySceneChange; ValueData: $00000050
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: skalMaskingAmp; ValueData: $00000320
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_inter_custom7; ValueData: $1b1a1817
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: elimChromThres; ValueData: $00000007
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: twopass_max_overflow_improvement; ValueData: $0000003c
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: use_alt_curve; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: creditsStartEnd; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: xvid_me_inter4v; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: part; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: x264_b_dct_decimate; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: x264_b_bframe_pyramid; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: qmatrix_inter4x4Y_custom1; ValueData: $10101010
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: ff_spatial_cplx_masking1000; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: alt_curve_min_rel_qual; ValueData: $00000032
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: dword; ValueName: q_p_min; ValueData: $00000002
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: string; ValueName: stats1flnm; ValueData: \video.stats
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: string; ValueName: ff1_stats_flnm; ValueData: \video.ffstats
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: string; ValueName: stats2flnm; ValueData: \videogk.stats
Root: HKCU; SubKey: Software\GNU\ffdshow_enc; ValueType: string; ValueName: storeExtFlnm; ValueData: 
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: needOutcspsFix; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: needGlobalFix; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dec_DXVA_VC1; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: h264skipOnDelay; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: errorRecognition; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoloadSizeXmax; ValueData: $00000800
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: videoDelayEnd; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dec_dxva_compatibilityMode; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: h264skipDelayTime; ValueData: $0000015e
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: bordersBrightness; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoloadSizeCond; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: queueCount; ValueData: $0000000a
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dec_dxva_postProcessingMode; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoloadSize; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: isDyInterlaced; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: softTelecine; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoloadSizeYmin; ValueData: $00000010
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: workaroundBugs2; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoloadScreenSize; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoloadSizeXmin; ValueData: $00000010
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dyInterlaced; ValueData: $00000120
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: threadsnum; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoloadScreenSizeCond; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: idct; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoloadSizeYmax; ValueData: $00000800
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoloadScreenSizeXmin; ValueData: $00000010
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dropOnDelay; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: multiThread; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoloadScreenSizeYmin; ValueData: $00000010
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: queueVMR9YV12; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: videoDelay; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: grayscale; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dec_DXVA_H264; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoloadScreenSizeXmax; ValueData: $00001000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dropDelayTime; ValueData: $000005dc
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: errorConcealment; ValueData: $00000003
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dontQueueInWMP; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoloadScreenSizeYmax; ValueData: $00001000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoLoadLogic; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: isVideoDelayEnd; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: useQueueOnlyIn; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: useQueueOnlyInList; ValueData: "mpc-hc.exe;mplayerc.exe;mpc-hc64.exe;mplayerc64.exe;"
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: cropLeft; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: panscanZoom; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: cropTolerance; ValueData: $0000001e
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fullCropNzoom; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: cropRight; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: panscanX; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: orderCropNzoom; ValueData: $ffffffff
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: cropRefreshDelay; ValueData: $00001388
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: magnificationY; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: cropTop; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: panscanY; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: cropStopScan; ValueData: $000186a0
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: isCropNzoom; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: magnificationLocked; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: cropBottom; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: isZoom; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: magnificationX; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: showCropNzoom; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: frameRateDoublerSE; ValueData: $00000003
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dgbobThreshold; ValueData: $0000000c
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: deinterlaceMethod; ValueData: $00000002
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: yadifMode; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: kernelDeintMap; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: showDeinterlace; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dgbobAP; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: yadifParity; ValueData: $ffffffff
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: kernelDeintLinked; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: tomocompVF; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: kernelDeintThreshold; ValueData: $0000000a
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: swapFields; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: orderDeinterlace; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: isDeinterlace; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: kernelDeintSharp; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: tomocompSE; ValueData: $00000003
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: frameRateDoublerThreshold; ValueData: $000000ff
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dgbobMode; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fullDeinterlace; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: deinterlaceAlways; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: kernelDeintTwoway; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: dscalerDIflnm; ValueData: 
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: dscalerDIcfg; ValueData: 
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: logoawayBorde_mode; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: showLogoaway; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: logoawayPointnw; ValueData: $00000005
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: logoawayBords_mode; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: logoawayY; ValueData: $0000000a
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: orderLogoaway; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: logoawayPointne; ValueData: $00000006
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: logoawayBordw_mode; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: logoawayDx; ValueData: $00000010
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fullLogoaway; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: logoawayPointsw; ValueData: $00000007
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: logoawayVhweight; ValueData: $00000005
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: logoawayDy; ValueData: $00000010
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: logoawayX; ValueData: $00000064
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: logoawayPointse; ValueData: $00000008
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: logoawaySolidcolor; ValueData: $00ffffff
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: logoawayMode; ValueData: $00000002
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: logoawayBordn_mode; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: logoawayLumaOnly; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: isLogoaway; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: logoawayBlur; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: logoawayParambitmap; ValueData: 
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fullPostproc; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: showPostproc; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: isPostproc; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: postprocNicXthresh; ValueData: $00000014
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: halfPostproc; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: ppqual; ValueData: $00000006
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelFixLum; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: postprocNicYthresh; ValueData: $00000028
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoq; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: postprocMethodNicFirst; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: deblockMplayerAccurate; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: ppIsCustom; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fullYrange; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: orderPostproc; ValueData: $00000002
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: ppcustom; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: deblockStrength; ValueData: $00000100
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: postprocSPPmode; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: postprocMethod; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: hue; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: halfPictProp; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: saturation; ValueData: $00000040
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fullPictProp; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: colorizeStrength; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: isPictProp; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: gammaCorrectionR; ValueData: $00000064
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: colorizeColor; ValueData: $00ffffff
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: gammaCorrection; ValueData: $00000064
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: gammaCorrectionG; ValueData: $00000064
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: colorizeChromaonly; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: scanlineEffect; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: lumGain; ValueData: $00000080
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: orderPictProp; ValueData: $00000003
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: gammaCorrectionB; ValueData: $00000064
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: pictProcLevelFix; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: lumOffset; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: showPictProp; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: pictProcLevelFixFull; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: orderGradFun; ValueData: $00000004
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: isGradFun; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fullGradFun; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: halfGradFun; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: gradFunThreshold; ValueData: $00000078
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: showGradFun; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: isLevels; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsOutMin; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsNumPoints; ValueData: $00000002
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsPoint9y; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsMode; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsPoint2y; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsPoint5y; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: orderLevels; ValueData: $00000005
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsOutMax; ValueData: $000000ff
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsPoint0x; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: YmaxDelta; ValueData: $00000014
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsPosterize; ValueData: $000000ff
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsPoint3x; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsPoint6x; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fullLevels; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsPoint7y; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsOnlyLuma; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsPoint0y; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: Ythreshold; ValueData: $0000000a
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsInAuto; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsPoint3y; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsPoint6y; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsInMin; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsPoint8x; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsFullY; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsPoint1x; ValueData: $000000ff
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: Ytemporal; ValueData: $00000003
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsPoint4x; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsPoint7x; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsGamma; ValueData: $00000064
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsPoint8y; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: showLevels; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsPoint1y; ValueData: $000000ff
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsPoint4y; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsInMax; ValueData: $000000ff
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsPoint9x; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: halfLevels; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsPoint2x; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: levelsPoint5x; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: offsetY_Y; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: isOffset; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: showOffset; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: offsetU_X; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: orderOffset; ValueData: $00000006
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: halfOffset; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: offsetU_Y; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: offsetY_X; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: transfFlip; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: offsetV_X; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: transMirror; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: offsetV_Y; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fullOffset; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: avcodecBlurRadius; ValueData: $00000003
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fullBlur; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: denoise3Dtime; ValueData: $00000258
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: smoothStrengthLuma; ValueData: $0000012c
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: avcodecBlurLuma; ValueData: $00000064
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: avcodecTNR1; ValueData: $000002bc
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: denoise3Dhq; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: blurIsSoften; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: blurIsAvcodecBLur; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: isBlur; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: avcodecBlurChroma; ValueData: $00000096
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: avcodecTNR2; ValueData: $000005dc
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: showBlur; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: blurIsTempSmooth; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: blurStrength; ValueData: $0000001e
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: isDenoise3d; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: blurIsSmoothChroma; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: avcodecTNR3; ValueData: $00000bb8
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: halfBlur; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: blurIsSmoothLuma; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: orderBlur; ValueData: $00000007
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: tempSmoothColor; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: denoise3Dluma; ValueData: $00000190
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: smoothStrengthChroma; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: blurIsGradual; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: tempSmooth; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: gradualStrength; ValueData: $00000028
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: denoise3Dchroma; ValueData: $0000012c
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: blurIsAvcodecTNR; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: msharpMask; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: xsharp_strenght; ValueData: $00000014
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fullSharpen; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: halfSharpen; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: msharpThreshold; ValueData: $0000000f
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: xsharp_threshold; ValueData: $00000096
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: asharpT; ValueData: $000000c8
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: msharpHQ; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: unsharp_strength; ValueData: $00000028
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: asharpD; ValueData: $00000190
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: unsharp_threshold; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: asharpB; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: mplayerSharpLuma; ValueData: $00000032
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: sharpenMethod; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: asharpHQBF; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: mplayerSharpChroma; ValueData: $00000032
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: xsharpen; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: orderSharpen; ValueData: $00000008
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: showSharpen; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: msharpStrength; ValueData: $00000064
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: showWarpsharp; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: awarpsharpBlur; ValueData: $00000002
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: halfWarpsharp; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: orderWarpsharp; ValueData: $00000009
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: awarpsharpCM; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fullWarpsharp; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: awarpsharpBM; ValueData: $00000002
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: warpsharpDepth; ValueData: $00000028
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: warpsharpMethod; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: isWarpsharp; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: warpsharpThreshold; ValueData: $00000080
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: awarpsharpDepth; ValueData: $00000640
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: awarpsharpThresh; ValueData: $00000032
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fullDScaler; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: showDScaler; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: isDScaler; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: halfDScaler; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: orderDScaler; ValueData: $0000000a
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: dscalerFltflnm; ValueData: 
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: dscalerCfg; ValueData: 
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: noiseFlickerA; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: noiseLinesTransparency; ValueData: $0000007f
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: orderNoise; ValueData: $0000000b
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: noiseFlickerF; ValueData: $00000032
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: isNoise; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: noiseScratchesA; ValueData: $00000032
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fullNoise; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: noiseShakeA; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: uniformNoise; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: noiseScratchesF; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: noiseShakeF; ValueData: $00000008
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: noiseStrength; ValueData: $0000001e
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: noiseScratchesTransparency; ValueData: $0000007f
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: showNoise; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: noiseLinesA; ValueData: $0000000a
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: noiseStrengthChroma; ValueData: $0000000a
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: noiseLinesC; ValueData: $0000007f
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: noisePattern; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: halfNoise; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: noiseLinesF; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: noiseMethod; ValueData: $00000002
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: noiseScratchesC; ValueData: $0000007f
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: noiseAveraged; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeIfXYcond; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeBicubicParam; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: isResize; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeMult1000; ValueData: $000007d0
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeA2; ValueData: $00000003
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeSimpleWarpYparam; ValueData: $000003b6
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeMethodChroma; ValueData: $00000002
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeAscpect; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeSharpenLum; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeIfPixCond; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeDx; ValueData: $00000280
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeAccurateRounding; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: showResize; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeBicubicChromaParam; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeDy; ValueData: $000001e0
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeSharpenChrom; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeIfPixVal; ValueData: $0004b000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeGaussParam; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeIsDy0; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: bordersInside; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: bordersDivX; ValueData: $00000032
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeGaussChromaParam; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeMethod; ValueData: $00000002
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: bordersX; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeLanczosParam; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeOutDeviceA1; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: bordersUnits; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeIf; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: bordersDivY; ValueData: $00000032
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeLanczosChromaParam; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: aspectRatio; ValueData: $0001547a
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeIfYcond; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: orderResize; ValueData: $0000000c
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeSARinternally; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: bordersY; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeMode; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeOutDeviceA2; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeInterlaced; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: bordersPixelsX; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeIfXcond; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeMethodsLocked; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeGblurLum; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeIfYval; ValueData: $000001e0
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fullResize; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: bordersLocked; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeA1; ValueData: $00000004
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeSimpleWarpXparam; ValueData: $0000047e
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: bordersPixelsY; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeIfXval; ValueData: $00000280
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeMulfOf; ValueData: $00000010
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: resizeGblurChrom; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: perspectiveY3; ValueData: $00000064
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: halfPerspective; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: isPerspective; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: perspectiveX1; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: perspectiveX4; ValueData: $00000064
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: orderPerspective; ValueData: $0000000d
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: perspectiveY1; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: perspectiveY4; ValueData: $00000064
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fullPerspective; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: perspectiveX2; ValueData: $00000064
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: perspectiveInterpolation; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: perspectiveY2; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: perspectiveIsSrc; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: perspectiveX3; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: showPerspective; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: avisynthInYUY2; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: avisynthEnableBuffering; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: isAvisynth; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: avisynthInRGB24; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: avisynthBufferAhead; ValueData: $0000000a
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: orderAvisynth; ValueData: $0000000e
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: avisynthInRGB32; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fullAvisynth; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: avisynthFfdshowSource; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: showAvisynth; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: avisynthInYV12; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: avisynthBufferBack; ValueData: $0000000a
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: avisynthApplyPulldown; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: avisynthScriptMULTI_SZ; ValueData: hex(7):00,00
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: avisynthScript; ValueData: 
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: orderShowMV; ValueData: $0000000f
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: visMV; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: visQuants; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: visGraph; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: showVis; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: isVis; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dct2; ValueData: $000003e8
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dctQuant; ValueData: $00000005
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: showDCT; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dctMatrix5; ValueData: $19181716
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dctMatrix11; ValueData: $1e1c1b1a
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: isDCT; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dctMatrix14; ValueData: $1b191817
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dct3; ValueData: $000003e8
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dctMatrix0; ValueData: $13121110
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: halfDCT; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dctMatrix6; ValueData: $16151413
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dctMatrix12; ValueData: $1a181716
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: orderDCT; ValueData: $00000010
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dctMatrix15; ValueData: $211f1e1c
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dct4; ValueData: $000003e8
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dctMatrix1; ValueData: $17161514
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dctMatrix7; ValueData: $1b1a1817
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dctMatrix13; ValueData: $1f1e1c1b
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fullDCT; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dct5; ValueData: $000003e8
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dctMatrix2; ValueData: $14131211
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dctMatrix8; ValueData: $17161514
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dct0; ValueData: $000003e8
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dct6; ValueData: $000001f4
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dctMatrix3; ValueData: $18171615
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dctMatrix9; ValueData: $1c1b1a19
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dct1; ValueData: $000003e8
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dctMode; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dct7; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dctMatrix4; ValueData: $15141312
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dctMatrix10; ValueData: $18171615
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: bitmapPosX; ValueData: $00000032
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: isBitmap; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: bitmapPosY; ValueData: $00000032
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: showBitmap; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: bitmapPosMode; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: orderBitmap; ValueData: $00000011
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: bitmapAlign; ValueData: $00000002
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fullBitmap; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: bitmapMode; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: bitmapStrength; ValueData: $00000080
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: bitmapFlnm; ValueData: 
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subDefLang; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subSpeed; ValueData: $000003e8
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subMinDurationSubtitle; ValueData: $00000bb8
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subVobsubAAswgauss; ValueData: $000002bc
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: showSubtitles; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fullSubtitles; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subVobsubAA; ValueData: $00000004
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subAutoFlnm; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subMinDurationLine; ValueData: $000005dc
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subWordWrap; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subDefLang2; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subIsExpand; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subPGS; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subLinespacing; ValueData: $00000064
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: orderSubtitles; ValueData: $00000012
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subSSAOverridePlacement; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subMinDurationChar; ValueData: $0000001e
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subOpacity; ValueData: $00000100
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subSpeed2; ValueData: $000003e8
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subStereoscopic; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subPosX; ValueData: $00000032
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subFiles; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subExpand; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subSSAMaintainInside; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subCC; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subStereoscopicPar; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subPosY; ValueData: $0000005d
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subText; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subExtendedTags; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subIsMinDuration; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subVobsubChangePosition; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subSSAUseMovieDimensions; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subFix; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subSplitBorder; ValueData: $00000014
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subVobsub; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subDelay; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subMinDurationType; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subImgScale; ValueData: $00000100
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subAlign; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: subFixLang; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: isSubtitles; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: subFixDict; ValueData: 
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: subFlnm; ValueData: 
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontShadowAlpha; ValueData: $00000080
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontUnderline; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontYscale; ValueData: $00000064
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontCharset; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontSettingsOverride; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontSpacing; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontShadowMode; ValueData: $00000002
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontAutosize; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontOutlineWidthOverride; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontAutosizeVideoWindow; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontBodyAlpha; ValueData: $00000100
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontSize; ValueData: $0000001a
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontColorOverride; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontXscale; ValueData: $00000064
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontShadowColor; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontColor; ValueData: $00ffffff
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontShadowSize; ValueData: $00000005
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontSizeA; ValueData: $0000001f
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontSizeOverride; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontOutlineAlpha; ValueData: $00000100
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontWeight; ValueData: $000002bc
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: scaleBorderAndShadowOverride; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontBlurMode; ValueData: $00000003
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontSplitting; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontOutlineColor; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontItalic; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontOpaqueBox; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontBlur; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontAspectAuto; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontShadowOverride; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fontOutlineWidth; ValueData: $00000002
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: fontName; ValueData: Arial
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: isGrab; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: grabFormat; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: showGrab; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: orderGrab; ValueData: $00000013
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: grabMode; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: grabStep; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: fullGrab; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: grabFrameNum; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: grabFrameNum1; ValueData: $00000064
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: grabFrameNum2; ValueData: $0000006e
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: grabDigits; ValueData: $00000005
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: grabQual; ValueData: $00000050
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: grabPath; ValueData: c:\
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: grabPrefix; ValueData: grab
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: orderOSD; ValueData: $00000014
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDdurationVisible; ValueData: $00000064
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: isOSD; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: showOSD; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDisSave; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDsaveOnly; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDisAutoHide; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDuserFormat; ValueData: $00000003
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDposX; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDposY; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: OSDformat; ValueData: 
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: OSDsaveFlnm; ValueData: 
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDfontOutlineAlpha; ValueData: $00000100
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDfontOutlineWidth; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDfontWeight; ValueData: $00000190
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDfontUnderline; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDfontShadowAlpha; ValueData: $00000080
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDfontCharset; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDfontYscale; ValueData: $00000064
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDfontShadowSize; ValueData: $00000008
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDfontSize; ValueData: $00000011
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDfontAspectAuto; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDfontXscale; ValueData: $00000064
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDfontOutlineColor; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDfontShadowMode; ValueData: $00000003
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDfontSpacing; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDfontShadowColor; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDfontBlurMode; ValueData: $00000003
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDfontColor; ValueData: $0000dc6e
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDfontItalic; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDfontBodyAlpha; ValueData: $00000100
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDfontOpaqueBox; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: OSDfontBlur; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: OSDfontName; ValueData: Arial
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: outRGB32; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: flip; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: cspOptionsWhiteCutoff; ValueData: $000000eb
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: outChangeCompatOnly; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: hwDeintFieldOrder; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: outRGB24; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: cspOptionsChromaCutoff; ValueData: $00000010
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: highQualityRGB; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: outYV12; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: dithering; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: outRGB555; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: setSARinOutSample; ValueData: $00000002
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: outClosest; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: outYUY2; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: outRGB565; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: hwOverlayAspect; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: outNV12; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: cspOptionsInputLevelsMode; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: cspOptionsOutputLevelsMode; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: hwOverlay; ValueData: $00000002
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: outYVYU; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: outI420; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: allowOutChange; ValueData: $00000002
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: hwDeintMethod; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: outDV; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: hwDeinterlace; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: outUYVY; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: cspOptionsBlackCutoff; ValueData: $00000010
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: setDeintInOutSample; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: cspOptionsIturBt2; ValueData: $00000003
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: cspOptionsRgbInterlaceMode; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: outDVnorm; ValueData: $00000002
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoloadExtsNeedFix; ValueData: $00000001
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoloadFlnm; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoloadExt; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: autoloadExts; ValueData: 
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoloadExe; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: autoloadExes; ValueData: 
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoloadVolumeName; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: autoloadVolumeNames; ValueData: 
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoloadVolumeSerial; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: autoloadVolumeSerials; ValueData: 
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoloadDecoder; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: autoloadDecoders; ValueData: 
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoloadDSfilter; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: autoloadDSfilters; ValueData: 
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoloadFOURCC; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: autoloadFOURCCs; ValueData: 
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoloadPreviousFOURCC; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: autoloadPreviousFOURCCs; ValueData: 
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoloadSAR; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: autoloadSARs; ValueData: 
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoloadDAR; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: autoloadDARs; ValueData: 
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: dword; ValueName: autoloadFrameRate; ValueData: $00000000
Root: HKCU; SubKey: Software\GNU\ffdshow\ffdshowenc; ValueType: string; ValueName: autoloadFrameRatess; ValueData: 
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Demultiplexer\NewsMontage.exe; ValueType: dword; ValueName: Settings Version; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Demultiplexer\NewsMontage.exe; ValueType: dword; ValueName: Separate audio channels; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Demultiplexer\NewsMontage.exe; ValueType: dword; ValueName: Parse external streams; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Demultiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Settings Version; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Demultiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Separate audio channels; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Demultiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Parse external streams; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Settings Version; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Set default values; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Video type; ValueData: $00000015
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Video format; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Profile; ValueData: $00000007
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Level; ValueData: $00000008
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: GOP length; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: B-frames count; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Closed GOP interval; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Aspect ratio; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Frame rate; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Scene change detection; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Pulldown flag; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Bitrate; ValueData: $01c9c380
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Maximum bitrate (for VBR); ValueData: $01c9c380
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Minimum bitrate (for VBR); ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Bitrate mode; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Quantization paramer for I-Frames; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Quantization paramer for P-Frames; ValueData: $00000004
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Quantization paramer for B-Frames; ValueData: $00000006
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Write a sequence header every GOP; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Write a sequence end code; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Write a sequence display extension; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Write a picture display extension; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Intra DC precision; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Chroma format; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Progressive sequence; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Progressive frame; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Top field first; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Repeat first field; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Motion search quality; ValueData: $00000011
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Motion search range; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Halfpel search; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Deinterlacing mode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Quantization scale type; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Intra VLC; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Scanning type; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Out sample size; ValueData: $00493e00
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Pad skipped frames; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Preview; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: ClosedCaption; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Two-pass rate control mode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Skip mode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Prefiltering; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: string; ValueName: 2-pass encoding statistics file; ValueData: C:\Users\ADMINI~1\AppData\Local\Temp\enc_mp2v_ds_stat_20277
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Calculate PSNR; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Online mode; ValueData: $ffffffff
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Offline mode; ValueData: $ffffffff
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: constrained parameters flag (MPEG-1 only); ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Use Drop Frame timecode notation in GOP; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: timecode of first frame; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Ignore frame interval; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: VBV buffer size (* 16 kbit); ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: macroblock quantization value; ValueData: $06040200
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: source primary chromaticity coordinates; ValueData: $00000004
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: opto-electronic transfer char. (gamma); ValueData: $00000004
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: different flags e.g. FEATURE_AUTO_GOP_PLACEMENT; ValueData: $03200000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Eg,Eb,Er / Y,Cb,Cr matrix coefficients; ValueData: $00000005
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: use non-default intra quant. matrices; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: use non-default quant. matrices; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: put blank SVCD scan offset blocks in the video stream; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: "initial VBV-fullnes in "; ValueData: $00000019
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: "Padding limint int "; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Settings Version; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Set default values; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Video type; ValueData: $00000015
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Video format; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Profile; ValueData: $00000007
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Level; ValueData: $00000008
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: GOP length; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: B-frames count; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Closed GOP interval; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Aspect ratio; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Frame rate; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Scene change detection; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Pulldown flag; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Bitrate; ValueData: $01c9c380
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Maximum bitrate (for VBR); ValueData: $01c9c380
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Minimum bitrate (for VBR); ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Bitrate mode; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Quantization paramer for I-Frames; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Quantization paramer for P-Frames; ValueData: $00000004
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Quantization paramer for B-Frames; ValueData: $00000006
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Write a sequence header every GOP; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Write a sequence end code; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Write a sequence display extension; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Write a picture display extension; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Intra DC precision; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Chroma format; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Progressive sequence; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Progressive frame; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Top field first; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Repeat first field; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Motion search quality; ValueData: $00000011
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Motion search range; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Halfpel search; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Deinterlacing mode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Quantization scale type; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Intra VLC; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Scanning type; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Out sample size; ValueData: $00493e00
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Pad skipped frames; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Preview; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: ClosedCaption; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Two-pass rate control mode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Skip mode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Prefiltering; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: string; ValueName: 2-pass encoding statistics file; ValueData: C:\Users\ADMINI~1\AppData\Local\Temp\enc_mp2v_ds_stat_20277
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Calculate PSNR; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Online mode; ValueData: $ffffffff
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Offline mode; ValueData: $ffffffff
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: constrained parameters flag (MPEG-1 only); ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Use Drop Frame timecode notation in GOP; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: timecode of first frame; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Ignore frame interval; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: VBV buffer size (* 16 kbit); ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: macroblock quantization value; ValueData: $06040200
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: source primary chromaticity coordinates; ValueData: $00000004
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: opto-electronic transfer char. (gamma); ValueData: $00000004
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: different flags e.g. FEATURE_AUTO_GOP_PLACEMENT; ValueData: $03200000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Eg,Eb,Er / Y,Cb,Cr matrix coefficients; ValueData: $00000005
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: use non-default intra quant. matrices; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: use non-default quant. matrices; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: put blank SVCD scan offset blocks in the video stream; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: "initial VBV-fullnes in "; ValueData: $00000019
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: "Padding limint int "; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Settings Version; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Set default values; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Video type; ValueData: $00000017
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Video format; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Profile; ValueData: $00000007
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Level; ValueData: $00000008
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: GOP length; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: B-frames count; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Closed GOP interval; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Aspect ratio; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Frame rate; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Scene change detection; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Pulldown flag; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Bitrate; ValueData: $02faf080
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Maximum bitrate (for VBR); ValueData: $02faf080
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Minimum bitrate (for VBR); ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Bitrate mode; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Quantization paramer for I-Frames; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Quantization paramer for P-Frames; ValueData: $00000004
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Quantization paramer for B-Frames; ValueData: $00000006
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Write a sequence header every GOP; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Write a sequence end code; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Write a sequence display extension; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Write a picture display extension; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Intra DC precision; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Chroma format; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Progressive sequence; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Progressive frame; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Top field first; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Repeat first field; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Motion search quality; ValueData: $00000011
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Motion search range; ValueData: $0000001f
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Halfpel search; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Deinterlacing mode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Quantization scale type; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Intra VLC; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Scanning type; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Out sample size; ValueData: $007a1200
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Pad skipped frames; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Preview; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: ClosedCaption; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Two-pass rate control mode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Skip mode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Prefiltering; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: string; ValueName: 2-pass encoding statistics file; ValueData: C:\Users\ADMINI~1\AppData\Local\Temp\enc_mp2v_ds_stat27774
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Calculate PSNR; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Online mode; ValueData: $ffffffff
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Offline mode; ValueData: $ffffffff
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: constrained parameters flag (MPEG-1 only); ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Use Drop Frame timecode notation in GOP; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: timecode of first frame; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Ignore frame interval; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: VBV buffer size (* 16 kbit); ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: macroblock quantization value; ValueData: $06040200
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: source primary chromaticity coordinates; ValueData: $00000005
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: opto-electronic transfer char. (gamma); ValueData: $00000005
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: different flags e.g. FEATURE_AUTO_GOP_PLACEMENT; ValueData: $03000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Eg,Eb,Er / Y,Cb,Cr matrix coefficients; ValueData: $00000005
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: use non-default intra quant. matrices; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: use non-default quant. matrices; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: put blank SVCD scan offset blocks in the video stream; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: "initial VBV-fullnes in "; ValueData: $00000019
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: "Padding limint int "; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Settings Version; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Set default values; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Video type; ValueData: $00000017
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Video format; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Profile; ValueData: $00000007
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Level; ValueData: $00000008
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: GOP length; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: B-frames count; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Closed GOP interval; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Aspect ratio; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Frame rate; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Scene change detection; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Pulldown flag; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Bitrate; ValueData: $02faf080
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Maximum bitrate (for VBR); ValueData: $02faf080
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Minimum bitrate (for VBR); ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Bitrate mode; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Quantization paramer for I-Frames; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Quantization paramer for P-Frames; ValueData: $00000004
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Quantization paramer for B-Frames; ValueData: $00000006
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Write a sequence header every GOP; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Write a sequence end code; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Write a sequence display extension; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Write a picture display extension; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Intra DC precision; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Chroma format; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Progressive sequence; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Progressive frame; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Top field first; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Repeat first field; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Motion search quality; ValueData: $00000011
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Motion search range; ValueData: $0000001f
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Halfpel search; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Deinterlacing mode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Quantization scale type; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Intra VLC; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Scanning type; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Out sample size; ValueData: $007a1200
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Pad skipped frames; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Preview; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: ClosedCaption; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Two-pass rate control mode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Skip mode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Prefiltering; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: string; ValueName: 2-pass encoding statistics file; ValueData: C:\Users\ADMINI~1\AppData\Local\Temp\enc_mp2v_ds_stat27774
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Calculate PSNR; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Online mode; ValueData: $ffffffff
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Offline mode; ValueData: $ffffffff
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: constrained parameters flag (MPEG-1 only); ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Use Drop Frame timecode notation in GOP; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: timecode of first frame; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Ignore frame interval; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: VBV buffer size (* 16 kbit); ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: macroblock quantization value; ValueData: $06040200
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: source primary chromaticity coordinates; ValueData: $00000005
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: opto-electronic transfer char. (gamma); ValueData: $00000005
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: different flags e.g. FEATURE_AUTO_GOP_PLACEMENT; ValueData: $03000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Eg,Eb,Er / Y,Cb,Cr matrix coefficients; ValueData: $00000005
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: use non-default intra quant. matrices; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: use non-default quant. matrices; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: put blank SVCD scan offset blocks in the video stream; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: "initial VBV-fullnes in "; ValueData: $00000019
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: "Padding limint int "; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Settings Version; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Set default values; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Video type; ValueData: $00000027
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Video format; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Profile; ValueData: $00000007
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Level; ValueData: $00000004
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: GOP length; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: B-frames count; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Closed GOP interval; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Aspect ratio; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Frame rate; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Scene change detection; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Pulldown flag; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Bitrate; ValueData: $02faf080
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Maximum bitrate (for VBR); ValueData: $01c9c380
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Minimum bitrate (for VBR); ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Bitrate mode; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Quantization paramer for I-Frames; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Quantization paramer for P-Frames; ValueData: $00000004
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Quantization paramer for B-Frames; ValueData: $00000006
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Write a sequence header every GOP; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Write a sequence end code; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Write a sequence display extension; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Write a picture display extension; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Intra DC precision; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Chroma format; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Progressive sequence; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Progressive frame; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Top field first; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Repeat first field; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Motion search quality; ValueData: $00000011
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Motion search range; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Halfpel search; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Deinterlacing mode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Quantization scale type; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Intra VLC; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Scanning type; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Out sample size; ValueData: $00493e00
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Pad skipped frames; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Preview; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: ClosedCaption; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Two-pass rate control mode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Skip mode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Prefiltering; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: string; ValueName: 2-pass encoding statistics file; ValueData: C:\Users\ADMINI~1\AppData\Local\Temp\enc_mp2v_ds_stat_20277
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Calculate PSNR; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Online mode; ValueData: $ffffffff
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Offline mode; ValueData: $ffffffff
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: constrained parameters flag (MPEG-1 only); ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Use Drop Frame timecode notation in GOP; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: timecode of first frame; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Ignore frame interval; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: VBV buffer size (* 16 kbit); ValueData: $00000440
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: macroblock quantization value; ValueData: $06040200
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: source primary chromaticity coordinates; ValueData: $00000004
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: opto-electronic transfer char. (gamma); ValueData: $00000004
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: different flags e.g. FEATURE_AUTO_GOP_PLACEMENT; ValueData: $03000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: Eg,Eb,Er / Y,Cb,Cr matrix coefficients; ValueData: $00000005
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: use non-default intra quant. matrices; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: use non-default quant. matrices; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: put blank SVCD scan offset blocks in the video stream; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: "initial VBV-fullnes in "; ValueData: $00000019
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.exe; ValueType: dword; ValueName: "Padding limint int "; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Settings Version; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Set default values; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Video type; ValueData: $00000027
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Video format; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Profile; ValueData: $00000007
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Level; ValueData: $00000004
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: GOP length; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: B-frames count; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Closed GOP interval; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Aspect ratio; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Frame rate; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Scene change detection; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Pulldown flag; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Bitrate; ValueData: $02faf080
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Maximum bitrate (for VBR); ValueData: $01c9c380
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Minimum bitrate (for VBR); ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Bitrate mode; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Quantization paramer for I-Frames; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Quantization paramer for P-Frames; ValueData: $00000004
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Quantization paramer for B-Frames; ValueData: $00000006
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Write a sequence header every GOP; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Write a sequence end code; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Write a sequence display extension; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Write a picture display extension; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Intra DC precision; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Chroma format; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Progressive sequence; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Progressive frame; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Top field first; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Repeat first field; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Motion search quality; ValueData: $00000011
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Motion search range; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Halfpel search; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Deinterlacing mode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Quantization scale type; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Intra VLC; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Scanning type; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Out sample size; ValueData: $00493e00
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Pad skipped frames; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Preview; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: ClosedCaption; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Two-pass rate control mode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Skip mode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Prefiltering; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: string; ValueName: 2-pass encoding statistics file; ValueData: C:\Users\ADMINI~1\AppData\Local\Temp\enc_mp2v_ds_stat_20277
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Calculate PSNR; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Online mode; ValueData: $ffffffff
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Offline mode; ValueData: $ffffffff
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: constrained parameters flag (MPEG-1 only); ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Use Drop Frame timecode notation in GOP; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: timecode of first frame; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Ignore frame interval; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: VBV buffer size (* 16 kbit); ValueData: $00000440
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: macroblock quantization value; ValueData: $06040200
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: source primary chromaticity coordinates; ValueData: $00000004
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: opto-electronic transfer char. (gamma); ValueData: $00000004
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: different flags e.g. FEATURE_AUTO_GOP_PLACEMENT; ValueData: $03000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Eg,Eb,Er / Y,Cb,Cr matrix coefficients; ValueData: $00000005
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: use non-default intra quant. matrices; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: use non-default quant. matrices; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: put blank SVCD scan offset blocks in the video stream; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: "initial VBV-fullnes in "; ValueData: $00000019
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Encoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: "Padding limint int "; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Settings Version; ValueData: $00000031
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Quality; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Brightness; ValueData: $00000080
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Hardware acceleration; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: State of hardware acceleration; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Resolution; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: IDCT precision; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Post process; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Deinterlace; ValueData: $00000004
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Condition of deinterlace; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Upsample; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Double rate; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Fields reordering; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Condition of fields reordering; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Format type; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: OSD; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: CC CCube decode order; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: License state; ValueData: $00000004
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Decoded frames; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Skipped frames; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Error concealment; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: SMP; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Forced subpicture show; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Cropping; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Crop top; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Crop left; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Crop bottom; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Crop right; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Reset statistics; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Affinity mask; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: MediaTime Source; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Auto persist; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: VMR maintain aspect ratio; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Aspect ratio; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Set media time; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Synchronization; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Condition of CC byte pairs processing; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Stream order; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Delivery Thread; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Output YV12; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Output YUY2; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Output YUYV; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Output UYVY; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Output AYUV; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Output ARGB32; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Output RGB32; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Output RGB24; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Output RGB565; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Output RGB555; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Output NV12; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Output NV24; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Output Y211; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Output Y41P; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Output CLJR; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Output IYUV; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Output YVU9; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Output IMC1; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Output IMC2; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Output IMC3; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.exe; ValueType: dword; ValueName: Output IMC4; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Settings Version; ValueData: $00000031
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Quality; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Brightness; ValueData: $00000080
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Hardware acceleration; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: State of hardware acceleration; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Resolution; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: IDCT precision; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Post process; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Deinterlace; ValueData: $00000004
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Condition of deinterlace; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Upsample; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Double rate; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Fields reordering; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Condition of fields reordering; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Format type; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: OSD; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: CC CCube decode order; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: License state; ValueData: $00000004
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Decoded frames; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Skipped frames; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Error concealment; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: SMP; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Forced subpicture show; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Cropping; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Crop top; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Crop left; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Crop bottom; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Crop right; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Reset statistics; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Affinity mask; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: MediaTime Source; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Auto persist; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: VMR maintain aspect ratio; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Aspect ratio; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Set media time; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Synchronization; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Condition of CC byte pairs processing; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Stream order; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Delivery Thread; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Output YV12; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Output YUY2; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Output YUYV; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Output UYVY; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Output AYUV; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Output ARGB32; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Output RGB32; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Output RGB24; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Output RGB565; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Output RGB555; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Output NV12; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Output NV24; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Output Y211; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Output Y41P; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Output CLJR; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Output IYUV; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Output YVU9; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Output IMC1; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Output IMC2; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Output IMC3; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MPEG-2 Video Decoder\NewsMontage.vshost.exe; ValueType: dword; ValueName: Output IMC4; ValueData: $00000001
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Settings Version; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Profile; ValueData: $0000012c
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Operational Pattern; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Frame Rate; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Start timecode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Start video duration; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: string; ValueName: Start audio duration; ValueData: 0
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Use drop frame timecode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Use old style XDCAM spec 1.1; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Use 48fps for DCI 2K; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Settings Version; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Profile; ValueData: $0000012c
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Operational Pattern; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Frame Rate; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Start timecode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Start video duration; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: string; ValueName: Start audio duration; ValueData: 0
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Use drop frame timecode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Use old style XDCAM spec 1.1; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Use 48fps for DCI 2K; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Settings Version; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Profile; ValueData: $0000001c
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Operational Pattern; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Frame Rate; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Start timecode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Start video duration; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: string; ValueName: Start audio duration; ValueData: 0
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Use drop frame timecode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Use old style XDCAM spec 1.1; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Use 48fps for DCI 2K; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Settings Version; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Profile; ValueData: $0000001c
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Operational Pattern; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Frame Rate; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Start timecode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Start video duration; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: string; ValueName: Start audio duration; ValueData: 0
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Use drop frame timecode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Use old style XDCAM spec 1.1; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Use 48fps for DCI 2K; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Settings Version; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Profile; ValueData: $00000020
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Operational Pattern; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Frame Rate; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Start timecode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Start video duration; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: string; ValueName: Start audio duration; ValueData: 0
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Use drop frame timecode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Use old style XDCAM spec 1.1; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.vshost.exe; ValueType: dword; ValueName: Use 48fps for DCI 2K; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Settings Version; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Profile; ValueData: $00000020
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Operational Pattern; ValueData: $00000002
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Frame Rate; ValueData: $00000003
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Start timecode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Start video duration; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: string; ValueName: Start audio duration; ValueData: 0
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Use drop frame timecode; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Use old style XDCAM spec 1.1; ValueData: $00000000
Root: HKCU; SubKey: Software\MainConcept\MainConcept MXF Multiplexer\NewsMontage.exe; ValueType: dword; ValueName: Use 48fps for DCI 2K; ValueData: $00000000

[InnoIDE_Settings]
LogFileOverwrite=false

