#include "scripts\products.iss"
#include "scripts\products\winversion.iss"
#include "scripts\products\fileversion.iss"
#include "scripts\products\msi31.iss"
#include "scripts\isxdl\isxdl.iss"
#include "scripts\products\dotnetfx35sp1.iss"
#include "scripts\products\dotnetfx40.iss"

#define GroupName "Media-Alliance"
#define MyAppName "NewsMontage Render Engine"
#define MyAppPublisher "Media-Alliance S.r.l."
#define MyAppURL "http://www.media-alliance.com"
#define MyAppExeName "NmRenderEngine.vshost.exe"
#define SrcApp "..\NMRenderEngine\bin\Release\NmRenderEngine.exe"
#define MyAppVersion GetFileVersion(SrcApp)

[CustomMessages]
win2000sp3_title=Windows 2000 Service Pack 3
winxpsp2_title=Windows XP Service Pack 2
winxpsp3_title=Windows XP Service Pack 3

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{481FEBF6-72F1-4FF9-9CFE-9A0290891FD3}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}

ChangesAssociations=yes

DefaultDirName=C:\{#GroupName}\{#MyAppName}
DefaultGroupName={#GroupName}
AllowNoIcons=false
InfoBeforeFile=
VersionInfoVersion={#MyAppVersion}
OutputDir=.\Output
OutputBaseFilename=NMRenderEngine_Setup_{#MyAppVersion}
SetupIconFile=.\Styles\NM Render Engine.ico
Compression=lzma
SolidCompression=true
UsePreviousGroup=false
WizardImageBackColor=clNavy
UsePreviousAppDir=false
WindowVisible=false

VersionInfoCompany=Media-Alliance S.r.l.
VersionInfoDescription=NM Render Engine
VersionInfoTextVersion=MyAppVersion
VersionInfoProductName=NM Render Engine
VersionInfoProductVersion={#MyAppVersion}
AppCopyright=Media-Alliance S.r.l.

; Unistall
Uninstallable=true
UninstallDisplayName=MediaAlliance NM Render Engine 2011
UninstallIconFile=".\Styles\NM Render Engine.ico"
PrivilegesRequired=admin

; SETUP PITCURE
WizardImageFile=Styles\MedAll_Banner_big.bmp
WizardSmallImageFile=Styles\MedAll_Banner_small.bmp


[Languages]
Name: en; MessagesFile: compiler:Default.isl
;Name: fr; MessagesFile: compiler:Languages\French.isl
;Name: ge; MessagesFile: compiler:Languages\German.isl
;Name: it; MessagesFile: compiler:Languages\Italian.isl
;Name: ru; MessagesFile: compiler:Languages\Russian.isl
;Name: sp; MessagesFile: compiler:Languages\Spanish.isl

[Tasks]

[Files]
; NOTE: Don't use "Flags: ignoreversion" on any shared system files
Source: ..\NMRenderEngine\bin\Release\NmRenderEngine.exe; DestDir: {app}; DestName:{#MyAppExeName}; Flags: ignoreversion
Source: ..\NMRenderEngine\bin\Release\NmRenderEngine.exe.config; DestDir:{app}; DestName:"{#MyAppExeName}.config"; Flags: ignoreversion
Source: ..\NMRenderEngine\bin\Release\ffmpeg.exe; DestDir: {app}; Flags: ignoreversion
Source: ..\NMRenderEngine\bin\Release\sox.exe; DestDir: {app}; Flags: ignoreversion
Source: ..\NMRenderEngine\bin\Release\*.dll; DestDir: {app}; Flags: ignoreversion
Source: ..\NMRenderEngine\bin\Release\QTSource.lic; DestDir: {app}; Flags: ignoreversion
Source: ..\NMRenderEngine\bin\Release\README.TXT; DestDir: {app}; Flags: ignoreversion
Source: ..\NMRenderEngine\bin\Release\StartProcessForAudioInfo.bat; DestDir: {app}; Flags: ignoreversion
Source: ..\NMRenderEngine\bin\Release\Registry\*.*; DestDir: {app}\Registry; Flags: ignoreversion

; PROFILES
Source: ..\NMRenderEngine\bin\Release\Profiles\*.*; DestDir: {app}\Profiles; Flags: ignoreversion

; VISUAL SKIN FILES
Source: ISSkin.dll; DestDir: {app}; Flags: dontcopy
Source: Styles\MedAll_Style_001.skinproj; DestDir: {tmp}; Flags: dontcopy

; EXTRA Tools
Source: D:\SetupCommon\Extra\QuickTimeInstaller.exe; DestDir: {app}\..\Extra; Flags: onlyifdoesntexist sharedfile; 
Source: D:\SetupCommon\Extra\dotNetFx40_Full_setup.exe; DestDir: {app}\..\Extra; Flags: onlyifdoesntexist sharedfile; 
Source: D:\SetupCommon\Extra\ffdshow_rev3562_20100907.exe; DestDir: {app}\..\Extra; Flags: onlyifdoesntexist sharedfile; 
Source: D:\SetupCommon\Extra\quicktime_directshow_source_filter_1.7.2.4.exe; DestDir: {app}\..\Extra; Flags: onlyifdoesntexist sharedfile; 
Source: D:\SetupCommon\Extra\DSFMgr.exe; DestDir: {app}\..\Extra; Flags: onlyifdoesntexist sharedfile; 
Source: D:\SetupCommon\Extra\GRAPHEDT.EXE; DestDir: {app}\..\Extra; Flags: onlyifdoesntexist sharedfile; 
Source: D:\SetupCommon\Extra\WinTail.exe; DestDir: {app}\..\Extra; Flags: onlyifdoesntexist sharedfile; 

; COMMON FILES > MEDIALOOKS
Source: D:\SetupCommon\Codecs\Medialooks\QTSource.lic; DestDir: {app}\..\Common Files\Medialooks; Flags: onlyifdoesntexist sharedfile;
Source: D:\SetupCommon\Codecs\Medialooks\QTSource.wmp.lic; DestDir: {app}\..\Common Files\Medialooks; Flags: onlyifdoesntexist sharedfile;
Source: D:\SetupCommon\Codecs\Medialooks\QTSourcePXT.dll; DestDir: {app}\..\Common Files\Medialooks; Flags: onlyifdoesntexist regserver 32bit sharedfile;
Source: D:\SetupCommon\Codecs\Medialooks\vcomp100.dll; DestDir: {app}\..\Common Files\Medialooks; Flags: onlyifdoesntexist sharedfile; 

; COMMON FILES > MAINCONCEPT
Source: D:\SetupCommon\Codecs\MainConcept\MpegDecX86\*.dll; DestDir: "{app}\..\Common Files\MainConcept\"; Flags: onlyifdoesntexist sharedfile;
Source: D:\SetupCommon\Codecs\MainConcept\MpegDecX86\*.ax; DestDir: "{app}\..\Common Files\MainConcept\"; Flags: onlyifdoesntexist regserver 32bit sharedfile;

Source: .\Extra\FFD.reg; DestDir: {app}\Extra; 
Source: ".\Extra\FixFFDShow.bat"; DestDir: {app}\Extra; 



[Icons]
Name: "{group}\NM Render Engine\{#MyAppName}"; Filename: {app}\{#MyAppExeName}; IconFilename: {app}\NmRenderEngine.vshost.exe; WorkingDir: {app}; 
Name: "{group}\NM Render Engine\{cm:UninstallProgram,{#MyAppName}}"; Filename: {uninstallexe}; 
;Name: {group}\; Filename: {app}\application.ico; IconFilename: {app}\application.ico; IconIndex: 0

[Run]
Filename: {app}\..\Extra\QuickTimeInstaller.exe; WorkingDir: {app}; Flags: unchecked postinstall waituntilterminated; Description: "Installer of QuickTime Player 7.6.6"; 
Filename: {app}\..\Extra\ffdshow_rev3562_20100907.exe; WorkingDir: {app}; Flags: postinstall waituntilterminated; Description: "Installer of FFDShow 20100907"; 
;Filename: {app}\quicktime_directshow_source_filter_1.7.2.4.exe; Flags: postinstall waituntilterminated; Description: MediaLooks QuickTime Filter 1.7.2.4; WorkingDir: {app}
Filename: {app}\Extra\FixFFDShow.bat; Flags: shellexec postinstall; Description: "Force FFDShow use Only for NewsMontage";



[UninstallDelete]
Name: {app}\*.*; Type: files; Tasks: ; Languages: 
Name: {app}\NewsNetStories; Type: filesandordirs
Name: {app}\RenderTemp; Type: filesandordirs
Name: {app}\Log; Type: filesandordirs
Name: {app}; Type: filesandordirs

[Dirs]
Name: {app}\Projects
Name: {app}\NewsNetStories
Name: {app}\Registry

[Code]
// SKIN
// Importing LoadSkin API from ISSkin.DLL
procedure LoadSkin(lpszPath: String; lpszIniFileName: String);
external 'LoadSkin@files:isskin.dll stdcall';

// Importing UnloadSkin API from ISSkin.DLL
procedure UnloadSkin();
external 'UnloadSkin@files:isskin.dll stdcall';

// Importing ShowWindow Windows API from User32.DLL
function ShowWindow(hWnd: Integer; uType: Integer): Integer;
external 'ShowWindow@user32.dll stdcall';
// End Skin



function InitializeSetup(): Boolean;
begin
	initwinversion();
	
  // SKIN
  ExtractTemporaryFile('MedAll_Style_001.skinproj');
  LoadSkin(ExpandConstant('{tmp}\MedAll_Style_001.skinproj'), '');
  // End Skin

	if (not minspversion(5, 0, 3)) then begin
		MsgBox(FmtMessage(CustomMessage('depinstall_missing'), [CustomMessage('win2000sp3_title')]), mbError, MB_OK);
		exit;
	end;
	if (not minspversion(5, 1, 2)) then begin
		MsgBox(FmtMessage(CustomMessage('depinstall_missing'), [CustomMessage('winxpsp3_title')]), mbError, MB_OK);
		exit;
	end;

	msi31('3.0');
	// dotnetfx40();
	
	Result := true;

	if not IsDotNetDetected('v4\Full', 0) then begin
        MsgBox('NM Render Engine requires Microsoft .NET Framework 4.0.'#13#13
            'Please use Windows Update to install this version,'#13
            'and then re-run the MyApp setup program.', mbInformation, MB_OK);
        Result := false;
    end else
        result := true;

end;


procedure DeinitializeSetup();
begin
	// Hide Window before unloading skin so user does not get
	// a glimse of an unskinned window before it is closed.
	ShowWindow(StrToInt(ExpandConstant('{wizardhwnd}')), 0);
	UnloadSkin();
end;

[_ISTool]
UseAbsolutePaths=true
[INI]

[Registry]
Root: HKLM; SubKey: Software\MediaAlliance\NMRenderEngine; ValueType: string; ValueName: Ver; ValueData: {#MyAppVersion};
Root: HKLM; SubKey: Software\MediaAlliance\NMRenderEngine; ValueType: string; ValueName: path; ValueData: {app}\{#MyAppExeName};

;File Association .LOG
Root: HKCR; Subkey: ".log"; ValueType: string; ValueName: ""; ValueData: "LogFile"; Flags: uninsdeletevalue
Root: HKCR; Subkey: "LogFile"; ValueType: string; ValueName: ""; ValueData: "Log File"; Flags: uninsdeletekey
Root: HKCR; Subkey: "LogFile\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\..\Extra\WinTail.exe,0"
Root: HKCR; Subkey: "LogFile\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\..\Extra\WinTail.exe"" ""%1"""

;Root: HKCU; SubKey: Software\GNU\ffdshow; ValueName: isBlacklist; ValueData: 00000000; ValueType: dword;
;Root: HKCU; SubKey: Software\GNU\ffdshow; ValueName: isWhitelist; ValueData: 00000001; ValueType: dword; 
;Root: HKCU; SubKey: Software\GNU\ffdshow; ValueName: blacklist; ValueData: """age3.exe;AvatarMP.exe;BeachSoccer.exe;Bully.exe;cm2008.exe;cm2010.exe;cmr4.exe;cmr5.exe;conflictdeniedops.exe;conquer.exe;crazy_taxi_pc.exe;daoriginslauncher.exe;dark_horizon.exe;DaybreakDX.exe;DisciplesIII.exe;divinity2.exe;dmc3se.exe;dragonica.exe;drakensang.exe;drome racers.exe;Edition.exe;em3.exe;em4.exe;empires_dmw.exe;engine.exe;fable.exe;FalconAF.exe;fallout3.exe;ff7.exe;fordorr.exe;fr2.exe;fr3.exe;freedom.exe;fsr.exe;game.bin;game.exe;Grandia2.exe;gta t�rk.exe;gta-underground.exe;gta-vc.exe;gta3.exe;gta_sa.exe;guildII.exe;hl.exe;hl2.exe;juiced.exe;juiced2_hin.exe;king.exe;launch.exe;launcher.exe;ldd.exe;legoracers.exe;lf2.exe;Main.exe;MassEffect2Launcher.exe;MassEffectLauncher.exe;maxpayne.exe;mixcraft.exe;mixcraft3.exe;mixcraft4.exe;mixcra~1.exe;morrowind.exe;msninst.exe;nations.exe;ninjablade.exe;oblivion.exe;outlook.exe;patriots.exe;pes2008.exe;pes2009.exe;pes2010.exe;pes4.exe;pes5.exe;pes6.exe;power2go.exe;powerdvd10.exe;powerdvd8.exe;powerdvd9.exe;premiere.exe;pwo.exe;questviewer.exe;rct.exe;rct3.exe;rct3plus.exe;residentevil3.exe;rf.exe;rf_online.bin;RomeGame.exe;rometw.exe;sacred.exe;sega rally.exe;sh3.exe;sh4.exe;shattered_horizon.exe;shippingpc-skygame.exe;sims.exe;simtractor.exe;skin.exe;smartlogon.exe;starsword.exe;StreetFighterIV.exe;streetlegal_redline.exe;SWRepublicCommando.exe;tw2008.exe;twoworlds.exe;vmcExtenderSetup.exe;wa.exe;war3.exe;worms 4 mayhem.exe;worms3d.exe;wwp.exe;Yahoo.Messenger.YmApp.exe;YahooMessenger.exe;YahooMusicEngine.exe;yahoom~1.exe;YahooWidgetEngine.exe;YahooWidgets.exe;yahoow~1.exe;ymini.exe;zoo.exe;"""; ValueType: string; 
;Root: HKCU; SubKey: Software\GNU\ffdshow; ValueName: whitelist; ValueData: """NewsMontage.exe;NmRenderEngine.vshost.exe;NmRenderEngine.exe"""; ValueType: string; 


[InnoIDE_Settings]
LogFileOverwrite=false
