﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace audioHelper
{
    public class TimelinePoint : IComparable
    {
        public long m_lTime;//{get{return this.m_lTime;}set{this.m_lTime=value;}}
        public float m_fGain;// { get { return this.m_fGain; } set { this.m_fGain = value; } }

        
        private class sortTPHelper : IComparer
        {
            int IComparer.Compare(object a, object b)
            {
                TimelinePoint p1 = (TimelinePoint)a;
                TimelinePoint p2 = (TimelinePoint)b;

                if (p1.m_lTime < p2.m_lTime)
                    return -1;

                if (p1.m_lTime > p2.m_lTime)
                    return 1;
                else
                    return 0;
            }

        }
        

        public TimelinePoint(long lTime, float fGain)
        {
            m_lTime = lTime;
            m_fGain = fGain;

        }

        int IComparable.CompareTo(object obj)
        {
            TimelinePoint p = (TimelinePoint)obj;

            if(this.m_lTime<p.m_lTime)
                return -1;
            
            if(this.m_lTime>p.m_lTime)
                return 1;
            else
                return 0;

        }

        public static IComparer sortTP()
        {
            return (IComparer)new sortTPHelper();
        }


    }
}
