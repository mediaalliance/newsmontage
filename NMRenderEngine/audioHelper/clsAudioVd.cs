﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices.ComTypes;
using System.Runtime.InteropServices;
using DirectShowLib;
using System.Collections;
using System.Diagnostics;
using Garbe.Sound;
using DirectShowLib.Utils;
using DirectShowLib.DMO;

namespace audioHelper
{
    public class clsAudioVd:IDisposable
    {
        protected const string AUDIO_PIN_PREFIX = "PCM Audio ID";
        protected const string WAV_PREFIX_VIDEO = "vd";
        protected const string WAV_PREFIX_AUDIO = "trk";


        protected string m_videofile = "";
        protected int m_nTracks = 0;
        protected IGraphBuilder pGraph = null;
        protected string m_videoid = "";
        protected string m_outPath = "";
        protected ArrayList m_TrackPins;
        protected bool m_ErrorState = false;


        public clsAudioVd(string videofile, string videoid, string outPath)
        {
            m_ErrorState = false;

            m_videofile = videofile;
            m_videoid = videoid;
            m_outPath = outPath;

            pGraph = (IGraphBuilder)new FilterGraph();
            m_TrackPins = new ArrayList();


            string[] fileParts = videofile.Split('.');
            string ext = fileParts[fileParts.Length - 1].ToLower();

            switch (ext)
            {
                case "mxf":
                    {
                        int iRes = BuildGraphMXFDVCPRO50_MLooks();
                        if (iRes == -1)
                        {
                            m_TrackPins.Clear();
                            m_nTracks = 0;
                            FilterGraphTools.RemoveAllFilters(pGraph);

                            iRes = BuildGraphMXFDV10_MLooks();
                            if (iRes == -1)
                                m_ErrorState = true;
                        }    

                        //BuildGraph();
                        break;
                    }

                case "mpg":
                case "mpeg":
                    {
                        int iRes = BuildGraphMPEG();
                        if (iRes == -1)
                            m_ErrorState = true;

                        break;
                    }
            }

            

        }


        public int getNTracks()
        {
            return m_nTracks;
        }

        public int SaveTracksAll()
        {
            if (m_ErrorState)
                return -1;

            int iRes = 0;

            IMediaControl mediaControl = (IMediaControl)pGraph;
            IMediaEvent mediaEvent = (IMediaEvent)pGraph;
            
            try
            {


                int hr = mediaControl.Run();
                checkHR(hr, "Can't run the graph");

                bool stop = false;
                while (!stop)
                {
                    System.Threading.Thread.Sleep(500);
                    //Console.Write(".");
                    EventCode ev;
                    IntPtr p1, p2;
                    if (mediaEvent.GetEvent(out ev, out p1, out p2, 0) == 0)
                    {
                        if (ev == EventCode.Complete || ev == EventCode.UserAbort)
                        {
                            Console.WriteLine("Done!");
                            stop = true;
                        }
                        else
                            if (ev == EventCode.ErrorAbort)
                            {
                                Console.WriteLine("An error occured: HRESULT={0:X}", p1);

                                stop = true;
                            }
                        mediaEvent.FreeEventParams(ev, p1, p2);
                    }

                }
                mediaControl.Stop();

                if (mediaControl != null)
                    mediaControl = null;

                if (mediaEvent != null)
                    mediaEvent = null;
            }
            catch
            {
                iRes = -1;
            }
            finally
            {
                if (mediaControl != null)
                {
                    mediaControl.Stop();
                    mediaControl = null;
                }

                if (mediaEvent != null)
                    mediaEvent = null;
            
            }

            return 0;
        }

        public long GetLenAudio()
        {
            long kkk=0;
            /*
            WaveReader wr1 = new WaveReader(@"C:\develop\ma\codice\butta\vd1trk1.wav");
            long inter = wr1.Interations;
            kkk = (inter * 1000) / wr1.SampleRate;
            */

            IMediaParamInfo mediaParamInfo = (IMediaParamInfo)pGraph;
            return kkk;        
        }

        private int BuildGraphMXFDVCPRO50_MLooks()
        {
            int iResult = 0;
            try
            {
                int hr = 0;

                //graph builder
                ICaptureGraphBuilder2 pBuilder = (ICaptureGraphBuilder2)new CaptureGraphBuilder2();
                hr = pBuilder.SetFiltergraph(pGraph);
                checkHR(hr, "Can't SetFiltergraph");

                Guid CLSID_MediaLooksMXFSplitter = new Guid("{DAA6D7FF-E1B1-415E-BF76-096C079C3EB1}"); //MLMXFSplitterL.dll
                Guid CLSID_MediaLooksDVSplitter = new Guid("{C128C530-AA60-4508-A83F-E34E433F03DB}"); //DVSplitterL.dll

                Guid CLSID_ffdshowVideoDecoder = new Guid("{04FE9017-F873-410E-871E-AB91661A4EF7}"); //ffdshow.ax
                Guid CLSID_WAVDest = new Guid("{3C78B8E2-6C4D-11D1-ADE2-0000F8754B99}"); //wavdest.ax
                //Guid CLSID_ffdshowAudioProcessor = new Guid("{B86F6BEE-E7C0-4D03-8D52-5B4430CF6C88}"); //ffdshow.ax
                Guid CLSID_ffdshowAudioDecoder = new Guid("{0F40E1E5-4F79-4988-B1A9-CC98794E6B55}"); //ffdshow.ax

                //add File Source (Async.)
                IBaseFilter pFileSourceAsync = (IBaseFilter)new AsyncReader();
                hr = pGraph.AddFilter(pFileSourceAsync, "File Source (Async.)");
                checkHR(hr, "Can't add File Source (Async.) to graph");
                //set source filename
                IFileSourceFilter pFileSourceAsync_src = pFileSourceAsync as IFileSourceFilter;
                if (pFileSourceAsync_src == null)
                    checkHR(unchecked((int)0x80004002), "Can't get IFileSourceFilter");
                hr = pFileSourceAsync_src.Load(m_videofile, null);
                checkHR(hr, "Can't load file");

                //add MediaLooks MXF Splitter
                IBaseFilter pMXFDemultiplexer = (IBaseFilter)Activator.CreateInstance(Type.GetTypeFromCLSID(CLSID_MediaLooksMXFSplitter));
                hr = pGraph.AddFilter(pMXFDemultiplexer, "MediaLooks MXF Splitter");
                checkHR(hr, "Can't add MediaLooks MXF Splitter to graph");

                //connect File Source (Async.) and MediaLooks MXF Splitter
                hr = pGraph.ConnectDirect(GetPin(pFileSourceAsync, "Output"), GetPin(pMXFDemultiplexer, "MXF Stream"), null);
                checkHR(hr, "Can't connect File Source (Async.) and MediaLooks MXF Splitter");

                //add MediaLooks DV Splitter
                IBaseFilter pMediaLooksDVSplitter = (IBaseFilter)Activator.CreateInstance(Type.GetTypeFromCLSID(CLSID_MediaLooksDVSplitter));
                hr = pGraph.AddFilter(pMediaLooksDVSplitter, "MediaLooks DV Splitter");
                checkHR(hr, "Can't add MediaLooks DV Splitter to graph");


                //connect MediaLooks MXF Splitter and MediaLooks DV Splitter
                hr = pGraph.ConnectDirect(GetPin(pMXFDemultiplexer, "DV-DIF"), GetPin(pMediaLooksDVSplitter, "Input"), null);
                checkHR(hr, "Can't connect MediaLooks MXF Splitter and MediaLooks DV Splitter");


                //add ffdshow Video Decoder
                IBaseFilter pffdshowVideoDecoder = (IBaseFilter)Activator.CreateInstance(Type.GetTypeFromCLSID(CLSID_ffdshowVideoDecoder));
                hr = pGraph.AddFilter(pffdshowVideoDecoder, "ffdshow Video Decoder");
                checkHR(hr, "Can't add ffdshow Video Decoder to graph");

                //connect MediaLooks DV Splitter and ffdshow Video Decoder
                hr = pGraph.ConnectDirect(GetPin(pMediaLooksDVSplitter, "DV Video"), GetPin(pffdshowVideoDecoder, "In"), null);
                checkHR(hr, "Can't connect MediaLooks DV Splitter and ffdshow Video Decoder");


                m_nTracks = GetNTracks2(pMXFDemultiplexer);

                int iCurTrack = 1;
                foreach (string trackpin in m_TrackPins)
                {
                    //add ffdshow Audio Processor
                    IBaseFilter pffdshowAudioDecoder = (IBaseFilter)Activator.CreateInstance(Type.GetTypeFromCLSID(CLSID_ffdshowAudioDecoder));
                    hr = pGraph.AddFilter(pffdshowAudioDecoder, "ffdshow Audio Processor");
                    checkHR(hr, "Can't add ffdshow Audio Processor to graph");

                    //connect MXF Demultiplexer and ffdshow Audio Processor
                    hr = pGraph.ConnectDirect(GetPin(pMXFDemultiplexer, trackpin), GetPin(pffdshowAudioDecoder, "In"), null);
                    checkHR(hr, "Can't connect MXF Demultiplexer and ffdshow Audio Processor");


                    //add WAV Dest
                    IBaseFilter pWAVDest = (IBaseFilter)Activator.CreateInstance(Type.GetTypeFromCLSID(CLSID_WAVDest));
                    hr = pGraph.AddFilter(pWAVDest, "WAV Dest");
                    checkHR(hr, "Can't add WAV Dest to graph");

                    //connect ffdshow Audio Processor and WAV Dest
                    hr = pGraph.ConnectDirect(GetPin(pffdshowAudioDecoder, "Out"), GetPin(pWAVDest, "In"), null);
                    checkHR(hr, "Can't connect ffdshow Audio Processor and WAV Dest");

                    string dstFile1 = getWavFilename(iCurTrack);

                    //add File writer
                    IBaseFilter pFilewriter = (IBaseFilter)new FileWriter();
                    hr = pGraph.AddFilter(pFilewriter, "File writer");
                    checkHR(hr, "Can't add File writer to graph");
                    //set destination filename
                    IFileSinkFilter pFilewriter_sink = pFilewriter as IFileSinkFilter;
                    if (pFilewriter_sink == null)
                        checkHR(unchecked((int)0x80004002), "Can't get IFileSinkFilter");
                    hr = pFilewriter_sink.SetFileName(dstFile1, null);
                    checkHR(hr, "Can't set filename");

                    //connect WAV Dest and File writer
                    hr = pGraph.ConnectDirect(GetPin(pWAVDest, "Out"), GetPin(pFilewriter, "in"), null);
                    checkHR(hr, "Can't connect WAV Dest and File writer");

                    iCurTrack++;

                }

            }
            catch
            {
                iResult = -1;
            }

            return iResult;

        }



        private int BuildGraphMXFDV10_MLooks()
        {
            int iResult = 0;
            try
            {
                int hr = 0;

                //graph builder
                ICaptureGraphBuilder2 pBuilder = (ICaptureGraphBuilder2)new CaptureGraphBuilder2();
                hr = pBuilder.SetFiltergraph(pGraph);
                checkHR(hr, "Can't SetFiltergraph");


                Guid CLSID_MediaLooksMXFSplitter = new Guid("{DAA6D7FF-E1B1-415E-BF76-096C079C3EB1}"); //MLMXFSplitterL.dll
                
                Guid CLSID_ffdshowVideoDecoder = new Guid("{04FE9017-F873-410E-871E-AB91661A4EF7}"); //ffdshow.ax
                //Guid CLSID_VideoRenderer = new Guid("{B87BEB7B-8D29-423F-AE4D-6582C10175AC}"); //quartz.dll

                Guid CLSID_WAVDest = new Guid("{3C78B8E2-6C4D-11D1-ADE2-0000F8754B99}"); //wavdest.ax
                //Guid CLSID_ffdshowAudioProcessor = new Guid("{B86F6BEE-E7C0-4D03-8D52-5B4430CF6C88}"); //ffdshow.ax
                Guid CLSID_ffdshowAudioDecoder = new Guid("{0F40E1E5-4F79-4988-B1A9-CC98794E6B55}"); //ffdshow.ax

                //add File Source (Async.)
                IBaseFilter pFileSourceAsync = (IBaseFilter)new AsyncReader();
                hr = pGraph.AddFilter(pFileSourceAsync, "File Source (Async.)");
                checkHR(hr, "Can't add File Source (Async.) to graph");
                //set source filename
                IFileSourceFilter pFileSourceAsync_src = pFileSourceAsync as IFileSourceFilter;
                if (pFileSourceAsync_src == null)
                    checkHR(unchecked((int)0x80004002), "Can't get IFileSourceFilter");
                hr = pFileSourceAsync_src.Load(m_videofile, null);
                checkHR(hr, "Can't load file");

                //add MediaLooks MXF Splitter
                IBaseFilter pMXFDemultiplexer = (IBaseFilter)Activator.CreateInstance(Type.GetTypeFromCLSID(CLSID_MediaLooksMXFSplitter));
                hr = pGraph.AddFilter(pMXFDemultiplexer, "MediaLooks MXF Splitter");
                checkHR(hr, "Can't add MediaLooks MXF Splitter to graph");

                //connect File Source (Async.) and MediaLooks MXF Splitter
                hr = pGraph.ConnectDirect(GetPin(pFileSourceAsync, "Output"), GetPin(pMXFDemultiplexer, "MXF Stream"), null);
                checkHR(hr, "Can't connect File Source (Async.) and MediaLooks MXF Splitter");


                m_nTracks = GetNTracks2(pMXFDemultiplexer);

                int iCurTrack = 1;
                foreach (string trackpin in m_TrackPins)
                {
                    //add ffdshow Audio Processor
                    IBaseFilter pffdshowAudioProcessor = (IBaseFilter)Activator.CreateInstance(Type.GetTypeFromCLSID(CLSID_ffdshowAudioDecoder));
                    hr = pGraph.AddFilter(pffdshowAudioProcessor, "ffdshow Audio Processor");
                    checkHR(hr, "Can't add ffdshow Audio Processor to graph");

                    //connect MXF Demultiplexer and ffdshow Audio Processor
                    hr = pGraph.ConnectDirect(GetPin(pMXFDemultiplexer, trackpin), GetPin(pffdshowAudioProcessor, "In"), null);
                    checkHR(hr, "Can't connect MXF Demultiplexer and ffdshow Audio Processor");


                    //add WAV Dest
                    IBaseFilter pWAVDest = (IBaseFilter)Activator.CreateInstance(Type.GetTypeFromCLSID(CLSID_WAVDest));
                    hr = pGraph.AddFilter(pWAVDest, "WAV Dest");
                    checkHR(hr, "Can't add WAV Dest to graph");

                    //connect ffdshow Audio Processor and WAV Dest
                    hr = pGraph.ConnectDirect(GetPin(pffdshowAudioProcessor, "Out"), GetPin(pWAVDest, "In"), null);
                    checkHR(hr, "Can't connect ffdshow Audio Processor and WAV Dest");

                    string dstFile1 = getWavFilename(iCurTrack);

                    //add File writer
                    IBaseFilter pFilewriter = (IBaseFilter)new FileWriter();
                    hr = pGraph.AddFilter(pFilewriter, "File writer");
                    checkHR(hr, "Can't add File writer to graph");
                    //set destination filename
                    IFileSinkFilter pFilewriter_sink = pFilewriter as IFileSinkFilter;
                    if (pFilewriter_sink == null)
                        checkHR(unchecked((int)0x80004002), "Can't get IFileSinkFilter");
                    hr = pFilewriter_sink.SetFileName(dstFile1, null);
                    checkHR(hr, "Can't set filename");

                    //connect WAV Dest and File writer
                    hr = pGraph.ConnectDirect(GetPin(pWAVDest, "Out"), GetPin(pFilewriter, "in"), null);
                    checkHR(hr, "Can't connect WAV Dest and File writer");

                    iCurTrack++;

                }


            }
            catch
            {
                iResult = -1;
            }

            return iResult;

        }

        private int BuildGraphMPEG()
        {
            int iResult = 0;
            try
            {
                int hr = 0;

                //graph builder
                ICaptureGraphBuilder2 pBuilder = (ICaptureGraphBuilder2)new CaptureGraphBuilder2();
                hr = pBuilder.SetFiltergraph(pGraph);
                checkHR(hr, "Can't SetFiltergraph");


                Guid CLSID_MainConceptMPEGDemultiplexer = new Guid("{136DCBF5-3874-4B70-AE3E-15997D6334F7}"); //mc_demux_mp2_ds.ax
                Guid CLSID_ffdshowVideoDecoder = new Guid("{04FE9017-F873-410E-871E-AB91661A4EF7}"); //ffdshow.ax
                //Guid CLSID_VideoRenderer = new Guid("{B87BEB7B-8D29-423F-AE4D-6582C10175AC}"); //quartz.dll
                Guid CLSID_ffdshowAudioDecoder = new Guid("{0F40E1E5-4F79-4988-B1A9-CC98794E6B55}"); //ffdshow.ax

                //Guid CLSID_ffdshowAudioProcessor = new Guid("{B86F6BEE-E7C0-4D03-8D52-5B4430CF6C88}"); //ffdshow.ax
                
                Guid CLSID_WAVDest = new Guid("{3C78B8E2-6C4D-11D1-ADE2-0000F8754B99}"); //wavdest.ax



                //add File Source (Async.)
                IBaseFilter pFileSourceAsync = (IBaseFilter)new AsyncReader();
                hr = pGraph.AddFilter(pFileSourceAsync, "File Source (Async.)");
                checkHR(hr, "Can't add File Source (Async.) to graph");
                //set source filename
                IFileSourceFilter pFileSourceAsync_src = pFileSourceAsync as IFileSourceFilter;
                if (pFileSourceAsync_src == null)
                    checkHR(unchecked((int)0x80004002), "Can't get IFileSourceFilter");
                hr = pFileSourceAsync_src.Load(m_videofile, null);
                checkHR(hr, "Can't load file");

                //add MainConcept MPEG Demultiplexer
                IBaseFilter pMXFDemultiplexer = (IBaseFilter)Activator.CreateInstance(Type.GetTypeFromCLSID(CLSID_MainConceptMPEGDemultiplexer));
                hr = pGraph.AddFilter(pMXFDemultiplexer, "MainConcept MPEG Demultiplexer");
                checkHR(hr, "Can't add MainConcept MPEG Demultiplexer to graph");

                //connect File Source (Async.) and MainConcept MPEG Demultiplexer
                hr = pGraph.ConnectDirect(GetPin(pFileSourceAsync, "Output"), GetPin(pMXFDemultiplexer, "Input"), null);
                checkHR(hr, "Can't connect File Source (Async.) and MainConcept MPEG Demultiplexer");


                m_nTracks = GetNTracks2(pMXFDemultiplexer);

                int iCurTrack = 1;
                foreach (string trackpin in m_TrackPins)
                {


                    //add ffdshow Audio Processor
                    IBaseFilter pffdshowAudioDecoder = (IBaseFilter)Activator.CreateInstance(Type.GetTypeFromCLSID(CLSID_ffdshowAudioDecoder));
                    hr = pGraph.AddFilter(pffdshowAudioDecoder, "ffdshow Audio Processor");
                    checkHR(hr, "Can't add ffdshow Audio Processor to graph");

                    //connect MXF Demultiplexer and ffdshow Audio Processor
                    hr = pGraph.ConnectDirect(GetPin(pMXFDemultiplexer, trackpin), GetPin(pffdshowAudioDecoder, "In"), null);
                    checkHR(hr, "Can't connect MXF Demultiplexer and ffdshow Audio Processor");


                    //add WAV Dest
                    IBaseFilter pWAVDest = (IBaseFilter)Activator.CreateInstance(Type.GetTypeFromCLSID(CLSID_WAVDest));
                    hr = pGraph.AddFilter(pWAVDest, "WAV Dest");
                    checkHR(hr, "Can't add WAV Dest to graph");

                    //connect ffdshow Audio Processor and WAV Dest
                    hr = pGraph.ConnectDirect(GetPin(pffdshowAudioDecoder, "Out"), GetPin(pWAVDest, "In"), null);
                    checkHR(hr, "Can't connect ffdshow Audio Processor and WAV Dest");

                    string dstFile1 = getWavFilename(iCurTrack);

                    //add File writer
                    IBaseFilter pFilewriter = (IBaseFilter)new FileWriter();
                    hr = pGraph.AddFilter(pFilewriter, "File writer");
                    checkHR(hr, "Can't add File writer to graph");
                    //set destination filename
                    IFileSinkFilter pFilewriter_sink = pFilewriter as IFileSinkFilter;
                    if (pFilewriter_sink == null)
                        checkHR(unchecked((int)0x80004002), "Can't get IFileSinkFilter");
                    hr = pFilewriter_sink.SetFileName(dstFile1, null);
                    checkHR(hr, "Can't set filename");

                    //connect WAV Dest and File writer
                    hr = pGraph.ConnectDirect(GetPin(pWAVDest, "Out"), GetPin(pFilewriter, "in"), null);
                    checkHR(hr, "Can't connect WAV Dest and File writer");

                    iCurTrack++;

                }



            }
            catch
            {
                iResult = -1;
            }

            return iResult;

        }



        private void BuildGraph()
        {
            int hr = 0;

            //graph builder
            ICaptureGraphBuilder2 pBuilder = (ICaptureGraphBuilder2)new CaptureGraphBuilder2();
            hr = pBuilder.SetFiltergraph(pGraph);
            checkHR(hr, "Can't SetFiltergraph");

            Guid CLSID_MainConceptMXFDemultiplexer = new Guid("{5E20C1E1-950D-4FE0-8AA9-E2251FB813A7}"); //mc_demux_mxf_ds.ax
            Guid CLSID_ffdshowAudioProcessor = new Guid("{B86F6BEE-E7C0-4D03-8D52-5B4430CF6C88}"); //ffdshow.ax
            Guid CLSID_WAVDest = new Guid("{3C78B8E2-6C4D-11D1-ADE2-0000F8754B99}"); //wavdest.ax
            Guid CLSID_ffdshowVideoDecoder = new Guid("{04FE9017-F873-410E-871E-AB91661A4EF7}"); //ffdshow.ax
            Guid CLSID_VideoRenderer = new Guid("{B87BEB7B-8D29-423F-AE4D-6582C10175AC}"); //quartz.dll

            //add File Source (Async.)
            IBaseFilter pFileSourceAsync = (IBaseFilter)new AsyncReader();
            hr = pGraph.AddFilter(pFileSourceAsync, "File Source (Async.)");
            checkHR(hr, "Can't add File Source (Async.) to graph");
            //set source filename
            IFileSourceFilter pFileSourceAsync_src = pFileSourceAsync as IFileSourceFilter;
            if (pFileSourceAsync_src == null)
                checkHR(unchecked((int)0x80004002), "Can't get IFileSourceFilter");
            hr = pFileSourceAsync_src.Load(m_videofile, null);
            checkHR(hr, "Can't load file");

            //add MainConcept MXF Demultiplexer
            IBaseFilter pMainConceptMXFDemultiplexer = (IBaseFilter)Activator.CreateInstance(Type.GetTypeFromCLSID(CLSID_MainConceptMXFDemultiplexer));
            hr = pGraph.AddFilter(pMainConceptMXFDemultiplexer, "MainConcept MXF Demultiplexer");
            checkHR(hr, "Can't add MainConcept MXF Demultiplexer to graph");

            //connect File Source (Async.) and MainConcept MXF Demultiplexer
            hr = pGraph.ConnectDirect(GetPin(pFileSourceAsync, "Output"), GetPin(pMainConceptMXFDemultiplexer, "Input"), null);
            checkHR(hr, "Can't connect File Source (Async.) and MainConcept MXF Demultiplexer");


            //add ffdshow Video Decoder
            IBaseFilter pffdshowVideoDecoder = (IBaseFilter)Activator.CreateInstance(Type.GetTypeFromCLSID(CLSID_ffdshowVideoDecoder));
            hr = pGraph.AddFilter(pffdshowVideoDecoder, "ffdshow Video Decoder");
            checkHR(hr, "Can't add ffdshow Video Decoder to graph");

            //connect MainConcept MXF Demultiplexer and ffdshow Video Decoder
            hr = pGraph.ConnectDirect(GetPin(pMainConceptMXFDemultiplexer, "DV Video ID1"), GetPin(pffdshowVideoDecoder, "In"), null);
            checkHR(hr, "Can't connect MainConcept MXF Demultiplexer and ffdshow Video Decoder");

            /*
            //add Video Renderer
            IBaseFilter pVideoRenderer = (IBaseFilter)Activator.CreateInstance(Type.GetTypeFromCLSID(CLSID_VideoRenderer));
            hr = pGraph.AddFilter(pVideoRenderer, "Video Renderer");
            checkHR(hr, "Can't add Video Renderer to graph");

            //connect ffdshow Video Decoder and Video Renderer
            hr = pGraph.ConnectDirect(GetPin(pffdshowVideoDecoder, "Out"), GetPin(pVideoRenderer, "VMR Input0"), null);
            checkHR(hr, "Can't connect ffdshow Video Decoder and Video Renderer");
            */

            m_nTracks = GetNTracks(pMainConceptMXFDemultiplexer);

            int iCurTrack = 1;
            foreach (string trackpin in m_TrackPins)
            {
                //add ffdshow Audio Processor
                IBaseFilter pffdshowAudioProcessor = (IBaseFilter)Activator.CreateInstance(Type.GetTypeFromCLSID(CLSID_ffdshowAudioProcessor));
                hr = pGraph.AddFilter(pffdshowAudioProcessor, "ffdshow Audio Processor");
                checkHR(hr, "Can't add ffdshow Audio Processor to graph");

                //connect MainConcept MXF Demultiplexer and ffdshow Audio Processor
                hr = pGraph.ConnectDirect(GetPin(pMainConceptMXFDemultiplexer, trackpin), GetPin(pffdshowAudioProcessor, "In"), null);
                checkHR(hr, "Can't connect MainConcept MXF Demultiplexer and ffdshow Audio Processor");


                //add WAV Dest
                IBaseFilter pWAVDest = (IBaseFilter)Activator.CreateInstance(Type.GetTypeFromCLSID(CLSID_WAVDest));
                hr = pGraph.AddFilter(pWAVDest, "WAV Dest");
                checkHR(hr, "Can't add WAV Dest to graph");

                //connect ffdshow Audio Processor and WAV Dest
                hr = pGraph.ConnectDirect(GetPin(pffdshowAudioProcessor, "Out"), GetPin(pWAVDest, "In"), null);
                checkHR(hr, "Can't connect ffdshow Audio Processor and WAV Dest");

                string dstFile1 = getWavFilename(iCurTrack);

                //add File writer
                IBaseFilter pFilewriter = (IBaseFilter)new FileWriter();
                hr = pGraph.AddFilter(pFilewriter, "File writer");
                checkHR(hr, "Can't add File writer to graph");
                //set destination filename
                IFileSinkFilter pFilewriter_sink = pFilewriter as IFileSinkFilter;
                if (pFilewriter_sink == null)
                    checkHR(unchecked((int)0x80004002), "Can't get IFileSinkFilter");
                hr = pFilewriter_sink.SetFileName(dstFile1, null);
                checkHR(hr, "Can't set filename");

                //connect WAV Dest and File writer
                hr = pGraph.ConnectDirect(GetPin(pWAVDest, "Out"), GetPin(pFilewriter, "in"), null);
                checkHR(hr, "Can't connect WAV Dest and File writer");

                iCurTrack++;

            }
        }

        private string getWavFilename(int iTrack)
        {
            string WavFilename = m_outPath + "\\" + WAV_PREFIX_VIDEO + m_videoid + WAV_PREFIX_AUDIO + iTrack.ToString() + ".wav";

            return WavFilename;
        }

        private void checkHR(int hr, string msg)
        {
            if (hr < 0)
            {
                Console.WriteLine(msg);
                DsError.ThrowExceptionForHR(hr);
            }
        }


        private int GetNTracks2(IBaseFilter filter)
        {
            int nTracks = 0;
            m_TrackPins.Clear();

            for (int i = 1; i < 20; i++)
            {
                try
                {
                    IPin curPin = DsFindPin.ByDirection(filter, PinDirection.Output, i);

                    if (curPin != null)
                    {
                        PinInfo pinfo;
                        curPin.QueryPinInfo(out pinfo);

                        string szPinName = pinfo.name.ToString();
                        if (szPinName!="")
                        {
                            nTracks++;
                            m_TrackPins.Add(szPinName);
                        }

                        DsUtils.FreePinInfo(pinfo);
                    }

                }
                catch
                {
                    break;
                }
            }

            return nTracks;
        }


        private int GetNTracks(IBaseFilter filter)
        {
            int nTracks = 0;
            m_TrackPins.Clear();

            IEnumPins epins;
            int hr = filter.EnumPins(out epins);
            checkHR(hr, "Can't enumerate pins");
            IntPtr fetched = Marshal.AllocCoTaskMem(4);
            IPin[] pins = new IPin[1];
            while (epins.Next(1, pins, fetched) == 0)
            {
                PinInfo pinfo;
                pins[0].QueryPinInfo(out pinfo);
                string szPinName = pinfo.name.ToString();
                if (szPinName.IndexOf(AUDIO_PIN_PREFIX) >= 0)
                {
                    nTracks++;
                    m_TrackPins.Add(szPinName);
                }
                
                DsUtils.FreePinInfo(pinfo);
            }

            return nTracks;
        }



        private IPin GetPin(IBaseFilter filter, string pinname)
        {
            IEnumPins epins;
            int hr = filter.EnumPins(out epins);
            checkHR(hr, "Can't enumerate pins");
            IntPtr fetched = Marshal.AllocCoTaskMem(4);
            IPin[] pins = new IPin[1];
            while (epins.Next(1, pins, fetched) == 0)
            {
                PinInfo pinfo;
                pins[0].QueryPinInfo(out pinfo);
                bool found = (pinfo.name == pinname);
                DsUtils.FreePinInfo(pinfo);
                if (found)
                    return pins[0];
            }
            checkHR(-1, "Pin not found");
            return null;
        }




        #region IDisposable Membri di

        public void Dispose()
        {
            if (pGraph != null)
            {
                pGraph.Abort();
                FilterGraphTools.RemoveAllFilters(pGraph);
                Marshal.ReleaseComObject(pGraph);
                pGraph = null;
                GC.Collect();
            }
        }

        #endregion
    }
}
