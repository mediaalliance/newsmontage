﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Garbe.Sound;

namespace audioHelper
{
    public enum FADER_MODE { crop = 1, padwithsilence };

    
    
    
    public class clsFader
    {
        protected string m_InFile;  //original audio file
        protected string m_OutFile;
        protected long m_lIn; //ms
        protected long m_lOut; //ms
        protected FADER_MODE m_iFaderMode;
        protected ArrayList m_KeyPoints;

        public clsFader(string InFile, string OutFile, long lIn, long lOut, FADER_MODE iFaderMode)
		{
            m_InFile = InFile;
            m_OutFile = OutFile;
            m_lIn = lIn;
            m_lOut = lOut;
            m_iFaderMode = iFaderMode;

            m_KeyPoints = new ArrayList();
            
            //aggiungere check parametri in
        }

        public void addKeyPoint(TimelinePoint KeyPoint)
        {
            m_KeyPoints.Add(KeyPoint);

            //aggiungere controllo di esistenza
        }

        public void resetKeyPoints()
        {
            m_KeyPoints.Clear();
        }

        public TimelinePoint[] getKeyPoints(bool bSorted)
        {
            TimelinePoint[] KeyPoints = (TimelinePoint[])m_KeyPoints.ToArray(typeof(TimelinePoint));

            if (bSorted)
            {
                Array.Sort(KeyPoints, TimelinePoint.sortTP());
            }

            return KeyPoints;
        }

        public int getPointsCount()
        {
            return m_KeyPoints.Count;
        }

        public int DoProcess()
        {
            long sampleIn,sampleOut;

            WaveReader wr1 = new WaveReader(m_InFile);
            WaveWriter wwl = new WaveWriter(m_OutFile, wr1.NumChannels, wr1.SampleRate, wr1.BitsPerSamples, 1, true);

            TimelinePoint[] tpArray = getVolumeArray(wr1.SampleRate);

            Gain ga1 = new Gain(1.0f);

            ga1.Input = wr1;
            wwl.Input = ga1;

            int inter = wwl.Interations;
            long kkk = (inter * 1000)/wr1.SampleRate;

            if(m_lIn==-1)
                sampleIn=0;
            else
                sampleIn=ms2sample(m_lIn, wr1.SampleRate);
                

            if(m_lOut==-1)
                sampleOut = inter;
            else
                sampleOut=ms2sample(m_lOut, wr1.SampleRate);



            int k = 0;
            for (k = 0; k < inter; k++)
            {
                if ((m_iFaderMode == FADER_MODE.padwithsilence) && ((k < sampleIn) || (k > sampleOut)))
                    ga1.Value = 0.0f;
                else
                    ga1.Value = getVolume(k, tpArray);

                wr1.DoProcess();
                ga1.DoProcess();
                if (!((m_iFaderMode == FADER_MODE.crop) && ((k < sampleIn) || (k > sampleOut))))
                    wwl.DoProcess();

            }

            wr1.Close();
            wwl.Close();



            return 0;
        }


        private float getVolume(int iSample, TimelinePoint[] tpArray)
        {
            int MaxIndex = tpArray.GetUpperBound(0);

            if (iSample <= ((TimelinePoint)tpArray.GetValue(0)).m_lTime)
                return ((TimelinePoint)tpArray.GetValue(0)).m_fGain;


            for (int i = 1; i <= MaxIndex; i++)
            {
                TimelinePoint tp2 = (TimelinePoint)tpArray.GetValue(i);
                TimelinePoint tp1 = (TimelinePoint)tpArray.GetValue(i - 1);
                if (iSample < tp2.m_lTime)
                {
                    float fVol = tp1.m_fGain + ((tp2.m_fGain - tp1.m_fGain) * (((float)iSample - (float)(tp1.m_lTime)) / ((float)(tp2.m_lTime) - (float)(tp1.m_lTime))));
                    return fVol;
                }
            }

            //            if (iSample >= ((TimelinePoint)tpArray.GetValue(MaxIndex)).m_lTime)
            return ((TimelinePoint)tpArray.GetValue(MaxIndex)).m_fGain;
        }

/*
        private float getVolume(int iSample, TimelinePoint[] tpArray)
        {
            int MaxIndex = tpArray.GetUpperBound(0);

            if (iSample <= ((TimelinePoint)tpArray.GetValue(0)).m_lTime)
                return ((TimelinePoint)tpArray.GetValue(0)).m_fGain;


            for (int i = 1; i <= MaxIndex; i++)
            {
                TimelinePoint tp2=(TimelinePoint)tpArray.GetValue(i);
                TimelinePoint tp1=(TimelinePoint)tpArray.GetValue(i-1);
                if (iSample < tp2.m_lTime)
                {
                    float fVol = tp1.m_fGain + ((tp2.m_fGain - tp1.m_fGain) * (((float)iSample - (float)(tp1.m_lTime)) / ((float)(tp2.m_lTime))));
                    return fVol;
                }
            }

//            if (iSample >= ((TimelinePoint)tpArray.GetValue(MaxIndex)).m_lTime)
                return ((TimelinePoint)tpArray.GetValue(MaxIndex)).m_fGain;
            

        }
*/

        private TimelinePoint[] getVolumeArray(long SampleRate)
        {
            TimelinePoint[] tpArray = this.getKeyPoints(true);

            foreach (TimelinePoint tp in tpArray)
            {
                tp.m_lTime = ms2sample(tp.m_lTime, SampleRate); 
            }     
   
            return tpArray;
        }

        private long ms2sample(long ms, long SampleRate)
        {
            return (SampleRate * ms) / 1000;
        }

        /*
        public string szInFile
        {
            get { return this.szInFile;}
            set {this.szInFile=value;}
        }

        public string szOutFile
        {
            get { return this.szOutFile; }
            set { this.szOutFile = value; }
        }
        */





    }
}
