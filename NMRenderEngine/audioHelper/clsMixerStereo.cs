﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;

namespace audioHelper
{
    public class clsMixerStereo
    {
        protected string m_OutFile;
        protected ArrayList m_InFiles;



        public clsMixerStereo(string OutFile)
        {
            m_OutFile = OutFile;

            m_InFiles = new ArrayList();

            //aggiungere check parametri in    
        }

        public bool addInFile(string InFile)
        {
            bool added = false;

            if (File.Exists(InFile))
            {
                m_InFiles.Add(InFile);
                added = true;
            }

            return added;
            //aggiungere controllo di esistenza
        }

        private static Int16 ScaleValue(Int64 pShortVal)
        {
            Int16 val_8bit = 0;
            double scaleMultiplier = 0.0;
            if (pShortVal > 0)
            {
                scaleMultiplier = (double)pShortVal * (double)Int16.MaxValue;
                val_8bit = (Int16)(scaleMultiplier / (double)Int64.MaxValue);
            }
            else if (pShortVal < 0)
            {
                scaleMultiplier = (double)pShortVal * (double)Int16.MinValue;
                val_8bit = (Int16)( scaleMultiplier/(double)Int16.MinValue);
            }

            return val_8bit;
        }


        public int DoProcess(int nFile)
        {
            // SOMMA TUTTE LE TRACCE MONO IN UN UNICO MONO

            int nFiles = m_InFiles.Count;
            int i = 0;
            ArrayList InData = new ArrayList();


            clsWavManager.WaveHeaderInfo WaveHeaderInfoOut = new clsWavManager.WaveHeaderInfo();
            Int16[] output = null;
            Int16 HighestSamplesValue = short.MinValue;

            #region READ ALL FILES IN MEMORY

            try
            {
                foreach (string InFile in m_InFiles)
                {
                    clsWavManager.WaveHeaderInfo WaveHeaderInfoIn = clsWavManager.WaveHeaderIN(InFile);

                    if (i == 0)
                    {
                        WaveHeaderInfoOut.bitsPerSample = WaveHeaderInfoIn.bitsPerSample;
                        WaveHeaderInfoOut.channels = WaveHeaderInfoIn.channels;
                        WaveHeaderInfoOut.dataLength = WaveHeaderInfoIn.dataLength;
                        WaveHeaderInfoOut.length = WaveHeaderInfoIn.length;
                        WaveHeaderInfoOut.sampleRate = WaveHeaderInfoIn.sampleRate;

                        output = new Int16[WaveHeaderInfoIn.dataLength / 2];
                    }

                    FileStream fs = new FileStream(@InFile, FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);

                    Int16[] arrfile = new Int16[WaveHeaderInfoIn.dataLength / 2];
                    fs.Position = 44;
                    int r;
                    for (r = 0; r < WaveHeaderInfoIn.dataLength / 2; r++)
                    {
                        arrfile[r] = br.ReadInt16();
                        if (arrfile[r] > HighestSamplesValue) HighestSamplesValue = arrfile[r];
                    }
                    br.Close();
                    fs.Close();
                    fs.Dispose();
                    InData.Add(arrfile);

                    i++;
                }
            }
            catch
            { 
            
            }
            
            #endregion READ ALL FILES IN MEMORY



            //short difference = (short)(HighestSamplesValue - (short.MaxValue / (short)nFile));
            short difference = (short)(HighestSamplesValue - (short.MaxValue / (short)m_InFiles.Count));
            double multiplier = 1.0 - ((double)difference / (double)HighestSamplesValue);
            // HighestSamplesValue : 1 = x : numFile
            // 100 : 32000 x : 1

            if (m_InFiles.Count == 1) multiplier = 1.0d;

            if (multiplier < 0.0) multiplier = -multiplier;

            try
            {
                for (long iCounter = 0; iCounter < (WaveHeaderInfoOut.dataLength / 2) - 1; iCounter += 1)
                {
                    Int64 curSample64_0 = 0, max0 = 0;

                    foreach (Int16[] arrfile in InData)
                    {
                        double val0 = 0;

                        if (arrfile.Length > iCounter)
                        {
                            //val0 = (double)((double)arrfile[iCounter] / (double)m_InFiles.Count);
                            //val0 = (double)arrfile[iCounter];
                            val0 = (double)((double)arrfile[iCounter] * multiplier);
                            
                            curSample64_0 += (Int64)Math.Round(val0, MidpointRounding.ToEven);

                            //if (max0 < arrfile[iCounter])
                            //    max0 = arrfile[iCounter];
                        }
                    }

                    //curSample64_0 = (Int64)(curSample64_0 * nFile);

                    if (curSample64_0 > Int16.MaxValue) curSample64_0 = Int16.MaxValue;
                    if (curSample64_0 < Int16.MinValue) curSample64_0 = Int16.MinValue;

                    output[iCounter] = Convert.ToInt16(curSample64_0);// (float)nFiles);
                }
            }
            catch 
            { 
            }

            try
            {
                clsWavManager.WaveHeaderOUT(m_OutFile, WaveHeaderInfoOut);
                FileStream fo = new FileStream(@m_OutFile, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
                BinaryWriter bw = new BinaryWriter(fo);
                for (long r = 0; r < output.Length; r++)
                    bw.Write(output[r]);
                bw.Close();
                fo.Close();
                fo.Dispose();
            }
            catch
            { 
            }

            return 0;
        }

        public class FileAudioData: IComparable
        {
            //public FileStream fs;
            public FileStream fs;
            public BinaryReader br;

            public short bitsPerSample;
            public int channels;
            public int dataLength;
            public int length;
            public int sampleRate;
            public byte[] audioVal = null;

            public void Close()
            {
                br.Close();
                fs.Close();
                fs.Dispose();
            }

            #region IComparable Members

            public int CompareTo(object obj)
            {
                FileAudioData newFileAudioData = obj as FileAudioData;
                if (newFileAudioData.dataLength > this.dataLength)
                    return 1;
                else
                    return -1;
            }

            #endregion
        }

        /// <summary>
        /// Combine all Left Mono Tracks and all Right Mono Tracks in one Stereo File
        /// </summary>
        /// <param name="Leftfiles">All mono left file to combine</param>
        /// <param name="Rightfiles">All mono right file to combine</param>
        /// <param name="outputfile">Output wave filename</param>
        public void Mono2Stereo(List<string> Leftfiles, List<string> Rightfiles, string outputfile)
        {
            string InFile = "";

            if (Leftfiles.Count > 0)
            {
                InFile = Leftfiles[0];
            }
            else if (Rightfiles.Count > 0)
            {
                InFile = Rightfiles[0];
            }

            // CREO L'HEADER DEL FILE DI OUTPUT
            clsWavManager.WaveHeaderInfo WaveHeaderInfoOut  = new clsWavManager.WaveHeaderInfo();
            clsWavManager.WaveHeaderInfo WaveHeaderInfoIn   = clsWavManager.WaveHeaderIN(InFile);

            WaveHeaderInfoOut.bitsPerSample     = WaveHeaderInfoIn.bitsPerSample;
            WaveHeaderInfoOut.channels          = 2;                                    // FORZO 2 CANALI
            WaveHeaderInfoOut.dataLength        = WaveHeaderInfoIn.dataLength * 2;      // PRENDO QUELLA DEL PRIMO MONO TANTO SONO TUTTE UGUALI
            WaveHeaderInfoOut.length            = WaveHeaderInfoIn.length;              // PRENDO QUELLA DEL PRIMO MONO TANTO SONO TUTTE UGUALI
            WaveHeaderInfoOut.sampleRate        = WaveHeaderInfoIn.sampleRate;

            Int16[]  OutStereo  = new Int16[WaveHeaderInfoIn.dataLength];               // FILE OUTPUT WAVE IN MEMORIA

            List<Int16[]> left_files  = new List<Int16[]>();                             // FILES INPUT WAVE LEFT IN MEMORIA
            List<Int16[]> right_files = new List<Int16[]>();                             // FILES INPUT WAVE RIGHT IN MEMORIA

            try
            {
                #region READ LEFT FILES

                foreach (string left_file in Leftfiles)
                {
                    // LEGGO TUTTI I FILES DEL CANALE SINISTRO

                    Int16[] temp_buffer = new Int16[WaveHeaderInfoIn.dataLength / 2];
                    FileStream fs = new FileStream(left_file, FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);

                    fs.Position = 44;
                    int r;
                    for (r = 0; r < WaveHeaderInfoIn.dataLength / 2; r++)
                    {
                        temp_buffer[r] = br.ReadInt16();
                    }
                    br.Close();
                    fs.Close();
                    fs.Dispose();

                    left_files.Add(temp_buffer);
                }

                #endregion READ LEFT FILES
            }
            catch
            { 
            }

            try
            {
                #region READ RIGHT FILES

                foreach (string right_file in Rightfiles)
                {
                    // LEGGO TUTTI I FILES DEL CANALE DESTRO

                    Int16[] temp_buffer = new Int16[WaveHeaderInfoIn.dataLength / 2];
                    FileStream fs = new FileStream(right_file, FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);

                    fs.Position = 44;
                    int r;
                    for (r = 0; r < WaveHeaderInfoIn.dataLength / 2; r++)
                    {
                        temp_buffer[r] = br.ReadInt16();
                    }

                    br.Close();
                    fs.Close();
                    fs.Dispose();

                    right_files.Add(temp_buffer);
                }

                #endregion READ RIGHT FILES
            }
            catch
            { 
                
            }

            try
            {
                #region WRITE WAVE STEREO ON MEMORY


                for (long srcCount = 0, outpos = 0; srcCount < (WaveHeaderInfoIn.dataLength / 2) - 1; srcCount += 1, outpos += 2)
                {
                    #region MIX LEFT DATA

                    Int64 curSample64_0 = 0, max0 = 0;

                    foreach (Int16[] arrfile in left_files)
                    {
                        double val0 = 0;

                        if (arrfile.Length > srcCount)
                        {
                            val0 = (double)((double)arrfile[srcCount] / (double)Leftfiles.Count);
                            curSample64_0 += (Int64)Math.Round(val0, MidpointRounding.ToEven);

                            if (max0 < arrfile[srcCount]) max0 = arrfile[srcCount];
                        }
                    }

                    curSample64_0 = (Int64)(curSample64_0 * Leftfiles.Count);

                    if (curSample64_0 > Int16.MaxValue) curSample64_0 = Int16.MaxValue;
                    if (curSample64_0 < Int16.MinValue) curSample64_0 = Int16.MinValue;

                    OutStereo[outpos] = Convert.ToInt16(curSample64_0);

                    #endregion MIX LEFT DATA


                    #region MIX RIGHT DATA

                    Int64 curSample64_1 = 0, max1 = 0;

                    foreach (Int16[] arrfile in right_files)
                    {
                        double val1 = 0;

                        if (arrfile.Length > srcCount)
                        {
                            val1 = (double)((double)arrfile[srcCount] / (double)Rightfiles.Count);
                            curSample64_1 += (Int64)Math.Round(val1, MidpointRounding.ToEven);

                            if (max1 < arrfile[srcCount]) max1 = arrfile[srcCount];
                        }
                    }

                    curSample64_1 = (Int64)(curSample64_1 * Rightfiles.Count);

                    if (curSample64_1 > Int16.MaxValue) curSample64_1 = Int16.MaxValue;
                    if (curSample64_1 < Int16.MinValue) curSample64_1 = Int16.MinValue;

                    OutStereo[outpos + 1] = Convert.ToInt16(curSample64_1);

                    #endregion MIX RIGHT DATA
                }

                #endregion WRITE WAVE STEREO ON MEMORY
            }
            catch
            {
            }

            try
            {
                #region CLEAN ALL

                for (int i = 0; i < left_files.Count; i++)
                {
                    Array.Clear(left_files[i], 0, left_files[i].Length);
                }

                for (int i = 0; i < right_files.Count; i++)
                {
                    Array.Clear(right_files[i], 0, right_files[i].Length);
                }

                left_files.Clear();
                right_files.Clear();

                #endregion CLEAN ALL
            }
            catch
            {            
            }


            clsWavManager.WaveHeaderOUT(outputfile, WaveHeaderInfoOut);
            FileStream fo = new FileStream(outputfile, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
            BinaryWriter bw = new BinaryWriter(fo);
            for (long r = 0; r < OutStereo.Length; r++)
            {
                bw.Write(OutStereo[r]);
            }
            bw.Close();
            fo.Close();
            fo.Dispose();

            Array.Clear(OutStereo, 0, OutStereo.Length);
        }

        //public void Resampling16to24(string file16, string file24)
        //{
        //    // CREO L'HEADER DEL FILE DI OUTPUT
        //    clsWavManager.WaveHeaderInfo WaveHeaderInfoOut = new clsWavManager.WaveHeaderInfo();
        //    clsWavManager.WaveHeaderInfo WaveHeaderInfoIn = clsWavManager.WaveHeaderIN(file16);

        //    WaveHeaderInfoOut.bitsPerSample     = WaveHeaderInfoIn.bitsPerSample;
        //    WaveHeaderInfoOut.channels          = WaveHeaderInfoIn.channels;
        //    WaveHeaderInfoOut.dataLength        = WaveHeaderInfoIn.dataLength * 3;
        //    WaveHeaderInfoOut.length            = WaveHeaderInfoIn.length;
        //    WaveHeaderInfoOut.sampleRate        = WaveHeaderInfoIn.sampleRate;

        //    Int32[] output = new Int32[WaveHeaderInfoIn.dataLength];               // FILE OUTPUT WAVE IN MEMORIA


        //}

        //Attualmente solo per 2 files
        //public int DoProcess()
        //{
        //    int nFiles = m_InFiles.Count;
        //    //if (nFiles < 2)
        //    //    return -1;

        //    List<FileAudioData> AudioFileList = new List<FileAudioData>();

        //    foreach (string filename in m_InFiles)
        //    {
        //        FileAudioData fileAudioData = new FileAudioData();

        //        clsWavManager.WaveHeaderInfo WaveHeaderInfoIn1 = clsWavManager.WaveHeaderIN(@filename);
        //        fileAudioData.fs = new FileStream(@filename, FileMode.Open, FileAccess.Read);
        //        fileAudioData.fs.Position = 44;
        //        fileAudioData.br = new BinaryReader(fileAudioData.fs);

        //        fileAudioData.bitsPerSample = WaveHeaderInfoIn1.bitsPerSample;
        //        fileAudioData.channels = WaveHeaderInfoIn1.channels;
        //        fileAudioData.dataLength = WaveHeaderInfoIn1.dataLength;
        //        fileAudioData.length = WaveHeaderInfoIn1.length;
        //        fileAudioData.sampleRate = WaveHeaderInfoIn1.sampleRate;



        //        AudioFileList.Add(fileAudioData);
        //    }

        //    AudioFileList.Sort();
           
        //    //Setup output file
        //    FileAudioData firstFileAudioData = AudioFileList[0];
        //    clsWavManager.WaveHeaderInfo WaveHeaderInfoOut = new clsWavManager.WaveHeaderInfo();
        //    WaveHeaderInfoOut.bitsPerSample = firstFileAudioData.bitsPerSample;
        //    WaveHeaderInfoOut.channels = firstFileAudioData.channels;
        //    WaveHeaderInfoOut.dataLength = firstFileAudioData.dataLength;
        //    WaveHeaderInfoOut.length = firstFileAudioData.length;
        //    WaveHeaderInfoOut.sampleRate = firstFileAudioData.sampleRate;
        //    clsWavManager.WaveHeaderOUT(m_OutFile, WaveHeaderInfoOut);
        //    FileStream fo = new FileStream(@m_OutFile, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
        //    BinaryWriter bw = new BinaryWriter(fo);
           


        //    //Create 5MB memory buffer
        //    byte[] Out_1MB_Stereo = new byte[5 * 1024 * 1024];

        //    long idx = 0;
        //    long r = 0;
        //    long step = Out_1MB_Stereo.Length;
            
        //    while (r < WaveHeaderInfoOut.dataLength)
        //    //for (long r = 0; r < WaveHeaderInfoOut.dataLength; r+=step)
        //    {
        //        long numMaxByte = 0;
        //        foreach (FileAudioData fAudioData in AudioFileList)
        //        {
        //            try
        //            {
        //                long checkRemainSize = (fAudioData.br.BaseStream.Position + step) >= fAudioData.br.BaseStream.Length ? fAudioData.br.BaseStream.Length - fAudioData.br.BaseStream.Position : step;
        //                fAudioData.audioVal = fAudioData.br.ReadBytes((int)checkRemainSize);

        //                if(numMaxByte < checkRemainSize)
        //                    numMaxByte = checkRemainSize;
        //            }
        //            catch { }
        //        }



        //        for (int i = 0; i < step; i++)
        //            foreach (FileAudioData fAudioData in AudioFileList)
        //                try
        //                {
        //                    if (i < fAudioData.audioVal.Length)
        //                        Out_1MB_Stereo[i] += (byte)fAudioData.audioVal[i];
        //                }
        //                catch { }

        //        //                if (idx == Out_1MB_Stereo.Length)
        //        {
        //            bw.Write(Out_1MB_Stereo, 0, (int)numMaxByte);

        //            Array.Clear(Out_1MB_Stereo, 0, Out_1MB_Stereo.Length);

        //            idx = 0;
        //        }

        //        r+=step;
        //    } 

        //    //if (WaveHeaderInfoOut.dataLength -r > 0)
        //    //{
        //    //    step = WaveHeaderInfoOut.dataLength - r;

        //    //    foreach (FileAudioData fAudioData in AudioFileList)
        //    //    {
        //    //        try
        //    //        {
        //    //            long checkRemainSize = (fAudioData.br.BaseStream.Position + step) >= fAudioData.br.BaseStream.Length ? fAudioData.br.BaseStream.Length - fAudioData.br.BaseStream.Position : step;
        //    //            fAudioData.audioVal = fAudioData.br.ReadBytes((int)checkRemainSize);
        //    //        }
        //    //        catch { }
        //    //    }



        //    //    for (int i = 0; i < step; i++)
        //    //        foreach (FileAudioData fAudioData in AudioFileList)
        //    //            try
        //    //            {
        //    //                if (i < fAudioData.audioVal.Length)
        //    //                    Out_1MB_Stereo[i] += (byte)fAudioData.audioVal[i];
        //    //            }
        //    //            catch { }

        //    //    //                if (idx == Out_1MB_Stereo.Length)
        //    //    {
        //    //        bw.Write(Out_1MB_Stereo);

        //    //        Array.Clear(Out_1MB_Stereo, 0, Out_1MB_Stereo.Length);

        //    //        idx = 0;
        //    //    }

        //    //}
            
        //    /*
        //    //Create 3MB memory buffer
        //    Int16[] Out_1MB_Stereo = new Int16[5 * 1024 * 1024];

        //    long idx = 0;
        //    //long totalByteWritten = 0;
        //    for (long r = 0; r < WaveHeaderInfoOut.dataLength / 2; r++)
        //    {
        //        if (idx == Out_1MB_Stereo.Length)
        //        {
        //            for (int b = 0; b < Out_1MB_Stereo.Length; b++)
        //                bw.Write(Out_1MB_Stereo[b]);

        //            //Array.Clear(Out_1MB_Stereo, 0, Out_1MB_Stereo.Length);

        //            idx = 0;
        //        }

        //        Int16 audioVal = 0;
        //        foreach (FileAudioData fAudioData in AudioFileList)
        //        {
        //            try
        //            {
        //                if (fAudioData.br.BaseStream.Position < fAudioData.br.BaseStream.Length)
        //                    audioVal += fAudioData.br.ReadInt16();
        //            }
        //            catch { }
        //        }

        //        Out_1MB_Stereo[idx++] = (Int16)audioVal;
        //    }

        //    if (idx > 0)
        //    {
        //        //Array.Clear(Out_1MB_Stereo, 0, Out_1MB_Stereo.Length);
        //        for (int b = 0; b < idx; b++)
        //            bw.Write(Out_1MB_Stereo[b]);

        //    }
        //     */

        //    //Close Files
        //    foreach (FileAudioData fAudioData in AudioFileList)
        //        fAudioData.Close();


        //    AudioFileList.Clear();

        //    bw.Close();
        //    fo.Close();
        //    fo.Dispose();

        //    GC.Collect();

        //    return 0;
        //}

    }
}
