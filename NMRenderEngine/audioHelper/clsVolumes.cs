﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace audioHelper
{
    public class clsVolumes
    {
        protected ArrayList m_KeyPoints;
        protected long m_lDuration; //ms

        public clsVolumes(long lDuration)
		{
            m_lDuration = lDuration;
            m_KeyPoints = new ArrayList();
        }

        public void addKeyPoint(TimelinePoint KeyPoint)
        {
            m_KeyPoints.Add(KeyPoint);

            //aggiungere controllo di esistenza
        }

        public void resetKeyPoints()
        {
            m_KeyPoints.Clear();
        }

        public TimelinePoint[] getKeyPoints(bool bSorted)
        {
            TimelinePoint[] KeyPoints = (TimelinePoint[])m_KeyPoints.ToArray(typeof(TimelinePoint));

            if (bSorted)
            {
                Array.Sort(KeyPoints, TimelinePoint.sortTP());
            }

            return KeyPoints;
        }

        public int getPointsCount()
        {
            return m_KeyPoints.Count;
        }


        private float ConvertVolumeScale(float val)
        {
            float outValue = 0.1f + (val * (1.0f - 0.1f));

            try
            {
                outValue = (float)(4000.0f * Math.Log10((double)outValue));
            }
            catch { }

            return outValue<-3998.0f ? -10000 : outValue;
        }

        public float getVolume(long lCurrentTime)
        {
            TimelinePoint[] tpArray = this.getKeyPoints(true);
            float fVol = 0.0f;

            int MaxIndex = tpArray.GetUpperBound(0);
            if (MaxIndex > 0)
            {
                if (lCurrentTime <= ((TimelinePoint)tpArray.GetValue(0)).m_lTime)
                    return ((TimelinePoint)tpArray.GetValue(0)).m_fGain;

                for (int i = 1; i <= MaxIndex; i++)
                {
                    TimelinePoint tp2 = (TimelinePoint)tpArray.GetValue(i);
                    TimelinePoint tp1 = (TimelinePoint)tpArray.GetValue(i - 1);
                    if (lCurrentTime < tp2.m_lTime)
                    {
                        fVol = tp1.m_fGain + ((tp2.m_fGain - tp1.m_fGain) * (((float)lCurrentTime - (float)(tp1.m_lTime)) / ((float)(tp2.m_lTime) - (float)(tp1.m_lTime))));

                        fVol = ConvertVolumeScale(fVol);

                        return fVol;
                    }

                }

                fVol = ConvertVolumeScale(((TimelinePoint)tpArray.GetValue(MaxIndex)).m_fGain);
            }
            return fVol;
        }

        public float getVolume(long lCurrentTime, TimelinePoint[] tpArray)
        {
            float fVol = 0.0f;

            int MaxIndex = tpArray.GetUpperBound(0);

            if (MaxIndex > 0)
            {
                if (lCurrentTime <= ((TimelinePoint)tpArray.GetValue(0)).m_lTime)
                    return ((TimelinePoint)tpArray.GetValue(0)).m_fGain;


                for (int i = 1; i <= MaxIndex; i++)
                {
                    TimelinePoint tp2 = (TimelinePoint)tpArray.GetValue(i);
                    TimelinePoint tp1 = (TimelinePoint)tpArray.GetValue(i - 1);
                    if (lCurrentTime < tp2.m_lTime)
                    {
                        fVol = tp1.m_fGain + ((tp2.m_fGain - tp1.m_fGain) * (((float)lCurrentTime - (float)(tp1.m_lTime)) / ((float)(tp2.m_lTime) - (float)(tp1.m_lTime))));
                        return fVol;
                    }

                }

                fVol = ((TimelinePoint)tpArray.GetValue(MaxIndex)).m_fGain;
            }

            return fVol;
        }

        /*
        private TimelinePoint[] getVolumeArray(long SampleRate)
        {
            TimelinePoint[] tpArray = this.getKeyPoints(true);

            foreach (TimelinePoint tp in tpArray)
            {
                tp.m_lTime = ms2sample(tp.m_lTime, SampleRate);
            }

            return tpArray;
        }

        private long ms2sample(long ms, long SampleRate)
        {
            return ((SampleRate * ms) / 1000) * 2;
        }
        */
    
    
    
    }
}
