﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using System.Diagnostics;

namespace audioHelper
{
    public enum FADER_MODE_STEREO { crop = 1, padwithsilence };


    public class clsFaderStereo
    {
        protected string m_InFile;  //original audio file
        protected string m_OutFile;
        protected long m_lIn; //ms
        protected long m_lOut; //ms
        protected FADER_MODE_STEREO m_iFaderMode;
        protected ArrayList m_KeyPoints;

        public clsFaderStereo(string InFile, string OutFile, long lIn, long lOut, FADER_MODE_STEREO iFaderMode)
		{
            m_InFile = InFile;
            m_OutFile = OutFile;
            m_lIn = lIn;
            m_lOut = lOut;
            m_iFaderMode = iFaderMode;

            m_KeyPoints = new ArrayList();
            
            //aggiungere check parametri in
        }

        public int DoProcess3()
        {
            clsWavManager.WaveHeaderInfo WaveHeaderInfoIn = clsWavManager.WaveHeaderIN(m_InFile);


            //read file wav to convert
            try
            {
                FileStream fs = new FileStream(@m_InFile, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                Int16[] arrfile = new Int16[WaveHeaderInfoIn.dataLength / 2];
                fs.Position = 44;

                clsWavManager.WaveHeaderInfo WaveHeaderInfoOut = new clsWavManager.WaveHeaderInfo();
                WaveHeaderInfoOut.bitsPerSample = WaveHeaderInfoIn.bitsPerSample;
                WaveHeaderInfoOut.channels = 2;
                WaveHeaderInfoOut.dataLength = WaveHeaderInfoIn.dataLength;
                WaveHeaderInfoOut.length = WaveHeaderInfoIn.length;
                WaveHeaderInfoOut.sampleRate = WaveHeaderInfoIn.sampleRate;
                
                clsWavManager.WaveHeaderOUT(m_OutFile, WaveHeaderInfoOut);
                FileStream fo = new FileStream(@m_OutFile, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
                BinaryWriter bw = new BinaryWriter(fo);


                long sampleIn, sampleOut;
                TimelinePoint[] tpArray = getVolumeArray(WaveHeaderInfoIn.sampleRate,2);

                int inter = WaveHeaderInfoIn.dataLength;

                if (m_lIn == -1)
                    sampleIn = 0;
                else
                    sampleIn = ms2sample(m_lIn, WaveHeaderInfoIn.sampleRate,2);


                if (m_lOut == -1)
                    sampleOut = inter;
                else
                    sampleOut = ms2sample(m_lOut, WaveHeaderInfoIn.sampleRate,2);

                sampleIn *= 2;
                sampleOut *= 2;

                //Create 5MB memory buffer
                UInt16[] Out_1MB_Stereo = new UInt16[5 * 1024 * 1024];

                byte[] tempBuffer = null;
                int step = Out_1MB_Stereo.Length;
                int r = 0;
                float fVolume = 0.0f;
                while (r < WaveHeaderInfoOut.dataLength)
                {
                    long checkRemainSize = (br.BaseStream.Position + step) >= br.BaseStream.Length ? br.BaseStream.Length - br.BaseStream.Position : step;
                    tempBuffer = br.ReadBytes((int)checkRemainSize);

                    int idInt16=0;
                    for (int i = 0; i < tempBuffer.Length; i+=4)
                            try
                            {
                                if (i < tempBuffer.Length)
                                {
                                    //Out_1MB_Stereo[idInt16] |= (Int16)(tempBuffer[i] << 8);
                                    //Out_1MB_Stereo[idInt16] |= (Int16)tempBuffer[i + 1];


                                    if ((m_iFaderMode == FADER_MODE_STEREO.padwithsilence) && (((i) < sampleIn) || ((i) > sampleOut)))
                                        fVolume = 0.0f;
                                    else
                                        fVolume = getVolume(idInt16, tpArray);

                                    if (!((m_iFaderMode == FADER_MODE_STEREO.crop) && (((i) < sampleIn) || ((i) >= sampleOut))))
                                    {
                                        UInt16 val1 = (UInt16)(tempBuffer[i] << 8 | (tempBuffer[i+1] & 0x00ff));
                                        UInt16 val2 = (UInt16)(tempBuffer[i + 2] << 8 | (tempBuffer[i + 3] & 0x00ff));


                                       
                                 //       Out_1MB_Stereo[idInt16] = val1;
                                 //       Out_1MB_Stereo[idInt16 + 1] = val2;
                                 
                                        Out_1MB_Stereo[idInt16] = Math.Min((UInt16)(val1 * fVolume), UInt16.MaxValue);
                                        Out_1MB_Stereo[idInt16 + 1] = Math.Min((UInt16)((float)val2 * fVolume), UInt16.MaxValue);
                                        idInt16 +=2;
                                    }


                                    
                                }
                            }
                            catch { }

                    //int OutPos = 0;
                    //for (int i = 0; i < tempBuffer.Length; i+=2)
                    //    try
                    //    {
                    //        if (i < tempBuffer.Length)
                    //        {
                    //            if ((m_iFaderMode == FADER_MODE_STEREO.padwithsilence) && (((i) < sampleIn) || ((i) > sampleOut)))
                    //                fVolume = 0.0f;
                    //            else
                    //                fVolume = getVolume(i, tpArray);

                    //            if (!((m_iFaderMode == FADER_MODE_STEREO.crop) && (((i) <= sampleIn) || ((i) >= sampleOut))))
                    //            {
                    //                Out_1MB_Stereo[OutPos] = Math.Min((Int16)((float)Out_1MB_Stereo[i] * fVolume), Int16.MaxValue);
                    //                Out_1MB_Stereo[OutPos + 1] = Math.Min((Int16)((float)Out_1MB_Stereo[i + 1] * fVolume), Int16.MaxValue);

                    //                OutPos += 2;
                    //            }


                    //        }
                    //    }
                    //    catch { }

                    byte[] outBuff = new byte[tempBuffer.Length];
                    idInt16 = 0;
                    for (int i = 0; i < outBuff.Length; i +=2)
                        try
                        {
                            if (i < tempBuffer.Length)
                            {
                                outBuff[i] = (byte)(Out_1MB_Stereo[idInt16] >> 8);
                                outBuff[i + 1] = (byte)(Out_1MB_Stereo[idInt16] & 0x00ff);
                                
                                idInt16++;
                            }
                        }
                        catch { }
                    bw.Write(outBuff, 0, (int)tempBuffer.Length);

                    Array.Clear(Out_1MB_Stereo, 0, Out_1MB_Stereo.Length);


                    r += step;
                }
               
                br.Close();
                fs.Close();
                fs.Dispose();

      
                bw.Close();
                fo.Close();
                fo.Dispose();
            }
            catch { return -1; }

            return 0;

        }

        public int DoProcess(int nChannels)
        {
            clsWavManager.WaveHeaderInfo WaveHeaderInfoIn = clsWavManager.WaveHeaderIN(m_InFile);


            //read file wav to convert
            try
            {
                FileStream fs = new FileStream(@m_InFile, FileMode.Open, FileAccess.Read);
                fs.Position = 44;
                BinaryReader br = new BinaryReader(fs);
 
                int rawLen = WaveHeaderInfoIn.dataLength;
                Int16[] arrfile = new Int16[WaveHeaderInfoIn.dataLength / 2];
               
                int r;
                try
                {
                    for (r = 0; r < WaveHeaderInfoIn.dataLength / 2; r++)
                        arrfile[r] = br.ReadInt16();
                }
                catch (Exception ex)
                {
                    string sss = ex.Message;
                }
                //byte[] temp = br.ReadBytes(WaveHeaderInfoIn.dataLength);
                //int id = 0;
                //for (r = 0; r < WaveHeaderInfoIn.dataLength; r += nChannels)
                //{
                //    arrfile[id++] = (Int16)((Int16)(temp[r + 1] << 8) | (Int16)temp[r]);

                //}

                br.Close();
                fs.Close();



                long sampleIn, sampleOut;
                TimelinePoint[] tpArray = getVolumeArray(WaveHeaderInfoIn.sampleRate, nChannels);

                int inter = WaveHeaderInfoIn.dataLength / 2;
              
                if (m_lIn == -1)
                    sampleIn = 0;
                else
                    sampleIn = ms2sample(m_lIn, WaveHeaderInfoIn.sampleRate, nChannels);


                if (m_lOut == -1)
                    sampleOut = inter;
                else
                    sampleOut = ms2sample(m_lOut, WaveHeaderInfoIn.sampleRate, nChannels);

                Int16[] OutStereo = new Int16[WaveHeaderInfoIn.dataLength / 2];

                float fVolume = 0.0f;
                long OutPos = 0;

                int k = 0;
                for (k = 0; k < inter; k += nChannels)
                {
                    if ((m_iFaderMode == FADER_MODE_STEREO.padwithsilence) && (((k) < sampleIn) || ((k) > sampleOut)))
                        fVolume = 0.0f;
                    else
                        fVolume = getVolume(k, tpArray);

                    if (fVolume > 1 || fVolume < 0)
                    { 
                    
                    }

                    for (int nCh = 0; nCh < nChannels; nCh++)
                    {
                        if (!((m_iFaderMode == FADER_MODE_STEREO.crop) && (((k) <= sampleIn) || ((k) >= sampleOut))))
                            OutStereo[OutPos + nCh] = Math.Min((Int16)((float)arrfile[k] * fVolume), Int16.MaxValue);               
                    }
                    OutPos += nChannels;
 
                }

                uint OutFileLen = (uint)(OutPos * 2);
                clsWavManager.WaveHeaderInfo WaveHeaderInfoOut = new clsWavManager.WaveHeaderInfo();
                WaveHeaderInfoOut.bitsPerSample = WaveHeaderInfoIn.bitsPerSample;
                WaveHeaderInfoOut.channels = nChannels;
                WaveHeaderInfoOut.dataLength = (int)OutFileLen;
                WaveHeaderInfoOut.length = (WaveHeaderInfoIn.length - WaveHeaderInfoIn.dataLength) + WaveHeaderInfoOut.dataLength;
                WaveHeaderInfoOut.sampleRate = WaveHeaderInfoIn.sampleRate;


                clsWavManager.WaveHeaderOUT(m_OutFile, WaveHeaderInfoOut);
                FileStream fo = new FileStream(@m_OutFile, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
                BinaryWriter bw = new BinaryWriter(fo);
                for (r = 0; r < OutStereo.Length; r++)
                    bw.Write(OutStereo[r]);
                bw.Close();
                fo.Close();
                fo.Dispose();
            }
            catch { return -1; }

            return 0;

        }




        public void addKeyPoint(TimelinePoint KeyPoint)
        {
            m_KeyPoints.Add(KeyPoint);

            //aggiungere controllo di esistenza
        }

        public void resetKeyPoints()
        {
            m_KeyPoints.Clear();
        }

        public TimelinePoint[] getKeyPoints(bool bSorted)
        {
            TimelinePoint[] KeyPoints = (TimelinePoint[])m_KeyPoints.ToArray(typeof(TimelinePoint));

            if (bSorted)
            {
                Array.Sort(KeyPoints, TimelinePoint.sortTP());
            }

            return KeyPoints;
        }

        public int getPointsCount()
        {
            return m_KeyPoints.Count;
        }



        private float getVolume(int iSample, TimelinePoint[] tpArray)
        {
            int MaxIndex = tpArray.GetUpperBound(0);
            if (MaxIndex <= 0)
                return 1.0f;

            if (iSample <= ((TimelinePoint)tpArray.GetValue(0)).m_lTime)
                return ((TimelinePoint)tpArray.GetValue(0)).m_fGain;


            for (int i = 1; i <= MaxIndex; i++)
            {
                TimelinePoint tp2 = (TimelinePoint)tpArray.GetValue(i);
                TimelinePoint tp1 = (TimelinePoint)tpArray.GetValue(i - 1);
                if (iSample < tp2.m_lTime)
                {
                    float fVol = tp1.m_fGain + ((tp2.m_fGain - tp1.m_fGain) * (((float)iSample - (float)(tp1.m_lTime)) / ((float)(tp2.m_lTime) - (float)(tp1.m_lTime))));

                    return fVol;
                }

            }

            //            if (iSample >= ((TimelinePoint)tpArray.GetValue(MaxIndex)).m_lTime)
            return ((TimelinePoint)tpArray.GetValue(MaxIndex)).m_fGain;


        }


        private TimelinePoint[] getVolumeArray(long SampleRate, int nChannels)
        {
            TimelinePoint[] tpArray = this.getKeyPoints(true);

            foreach (TimelinePoint tp in tpArray)
            {
                tp.m_lTime = ms2sample(tp.m_lTime, SampleRate, nChannels);
            }

            return tpArray;
        }

        private long ms2sample(long ms, long SampleRate, int nChannels)
        {
            return ((SampleRate * ms) / 1000) * nChannels;
        }







    }
}
