﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Garbe.Sound;

namespace audioHelper
{
    public class clsMixer
    {
        protected string m_OutFile;
        protected ArrayList m_InFiles;

        public clsMixer(string OutFile)
        {
            m_OutFile = OutFile;

            m_InFiles = new ArrayList();

            //aggiungere check parametri in    
        }

        public void addInFile(string InFile)
        {
            m_InFiles.Add(InFile);

            //aggiungere controllo di esistenza
        }

        public int DoProcess()
        {
            int nFiles = m_InFiles.Count;
            int i;
            

            
            WaveReader[] readers = new WaveReader[nFiles];
            Gain[] gains = new Gain[nFiles];
            Mixer mx = new Mixer((ushort)nFiles);
            
            i=0;
            foreach (string InFile in m_InFiles )
            {
                readers.SetValue(new WaveReader(InFile), i);
                gains.SetValue(new Gain(1.0f), i);
                ((Gain)(gains.GetValue(i))).Input = ((WaveReader)(readers.GetValue(i)));
                mx[i] = ((Gain)(gains.GetValue(i)));

                i++;
            }

            
            WaveWriter wwl = new WaveWriter(m_OutFile, ((WaveReader)(readers.GetValue(0))).NumChannels, ((WaveReader)(readers.GetValue(0))).SampleRate, ((WaveReader)(readers.GetValue(0))).BitsPerSamples, 1, true);





            wwl.Input = mx;


            int h = 0;
            int inter = wwl.Interations;
            for (int k = 0; k < inter; k++)
            {
                for (h = 0; h < nFiles; h++)
                {
                    ((WaveReader)(readers.GetValue(h))).DoProcess();
                    ((Gain)(gains.GetValue(h))).DoProcess();
                }

                mx.DoProcess();
                wwl.DoProcess();

            }
            for (h = 0; h < nFiles; h++)
            {
                ((WaveReader)(readers.GetValue(h))).Close();
            }
            wwl.Close();


            return 0;
        }

    }
}
