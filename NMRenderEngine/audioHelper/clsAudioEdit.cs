﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace audioHelper
{
    public static class clsAudioEdit
    {
        public static int m_bitsPerSample=0;
        public static int m_channels = 0;
        public static int m_sampleRate = 0;
        public static bool m_initialized = false;

        private const string TOTAL_VOICE_NAME = "total_voice.wav";
        private const string TOTAL_MUSIC_NAME = "total_music.wav";
   

        public static void initialize(int bitsPerSample, int channels, int sampleRate)
        { 
            m_bitsPerSample=bitsPerSample;
            m_channels=channels;
            m_sampleRate = sampleRate;
            m_initialized = true;
        }


        public static void CreateEmptyMonoFile(string mainFile, int duration, short bps, int sampleRate)
        {
            int length = ms2byte(duration, bps, sampleRate, 1);

            clsWavManager.WaveHeaderInfo WaveHeaderInfoOut = new clsWavManager.WaveHeaderInfo();
            WaveHeaderInfoOut.bitsPerSample = (short)bps;
            WaveHeaderInfoOut.channels = 1;
            WaveHeaderInfoOut.dataLength = length + 44;
            WaveHeaderInfoOut.length = length;
            WaveHeaderInfoOut.sampleRate = sampleRate;

            Int16[] OutBuffer = new Int16[WaveHeaderInfoOut.dataLength / 2];
            Array.Clear(OutBuffer, 0, OutBuffer.Length);

            clsWavManager.WaveHeaderOUT(mainFile, WaveHeaderInfoOut);

            FileStream fo = new FileStream(mainFile, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
            BinaryWriter bw = new BinaryWriter(fo);
            for (int r = 0; r < OutBuffer.Length; r++)
                bw.Write(OutBuffer[r]);

            bw.Close();
            fo.Close();
            fo.Dispose();

        }

     

        public static int append(string mainFile, string file2Append, string workFolder)
        {
            if ((!File.Exists(file2Append))||(!Directory.Exists(workFolder)))
                return -1;

            if (!File.Exists(mainFile))
            {
                File.Copy(file2Append, mainFile);
                return 0;
            }


            string outFile =  "";
            if (workFolder.EndsWith("\\"))
                outFile = workFolder + "aetemp.wav";
            else
                outFile = workFolder + "\\aetemp.wav";

            if (File.Exists(outFile))
                File.Delete(outFile);

            clsWavManager.WaveHeaderInfo WaveHeaderInfoInMain = clsWavManager.WaveHeaderIN(mainFile);
            clsWavManager.WaveHeaderInfo WaveHeaderInfoIn2 = clsWavManager.WaveHeaderIN(file2Append);

            clsWavManager.WaveHeaderInfo WaveHeaderInfoOut = new clsWavManager.WaveHeaderInfo();
            WaveHeaderInfoOut.bitsPerSample = WaveHeaderInfoInMain.bitsPerSample;
            WaveHeaderInfoOut.channels = WaveHeaderInfoInMain.channels;
            WaveHeaderInfoOut.dataLength = WaveHeaderInfoInMain.dataLength + WaveHeaderInfoIn2.dataLength;
            WaveHeaderInfoOut.length = (WaveHeaderInfoInMain.length - WaveHeaderInfoInMain.dataLength) + WaveHeaderInfoOut.dataLength;
            WaveHeaderInfoOut.sampleRate = WaveHeaderInfoInMain.sampleRate;


            //Create 3MB memory buffer
            Int16[] Out_1MB_Stereo = new Int16[5 * 1024 * 1024];

            //File base.
            FileStream fs = new FileStream(mainFile, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            fs.Position = 44;

            //Destination file
            clsWavManager.WaveHeaderOUT(outFile, WaveHeaderInfoOut);
            FileStream fo = new FileStream(@outFile, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
            BinaryWriter bw = new BinaryWriter(fo);


            long idx=0;
            //long totalByteWritten = 0;
            for (long r = 0; r < WaveHeaderInfoInMain.dataLength / 2; r++)
            {
                if (idx == Out_1MB_Stereo.Length)
                {
                    for (int b = 0; b < Out_1MB_Stereo.Length; b++)
                        bw.Write(Out_1MB_Stereo[b]);

                    Array.Clear(Out_1MB_Stereo, 0, Out_1MB_Stereo.Length);

                    idx = 0;
                }

                Out_1MB_Stereo[idx++] = br.ReadInt16();
                //totalByteWritten++;
            }

            //long remainByteToWrite = (WaveHeaderInfoInMain.dataLength / 2) - totalByteWritten;
            if (idx > 0)
            {
                //Array.Clear(Out_1MB_Stereo, 0, Out_1MB_Stereo.Length);
                for (int b = 0; b < idx; b++)
                    bw.Write(Out_1MB_Stereo[b]);

            }
            br.Close();
            fs.Close();
            fs.Dispose();


            //File to add.
            Array.Clear(Out_1MB_Stereo, 0, Out_1MB_Stereo.Length);

            fs = new FileStream(file2Append, FileMode.Open, FileAccess.Read);
            br = new BinaryReader(fs);
            fs.Position = 44;

            idx = 0;
            //totalByteWritten = 0;
            for (long r = 0; r < WaveHeaderInfoIn2.dataLength / 2; r++)
            {
                if (idx == Out_1MB_Stereo.Length)
                {
                    for (int b = 0; b < Out_1MB_Stereo.Length; b++)
                        bw.Write(Out_1MB_Stereo[b]);

                    Array.Clear(Out_1MB_Stereo, 0, Out_1MB_Stereo.Length);

                    idx = 0;
                }

                Out_1MB_Stereo[idx++] = br.ReadInt16();
                //totalByteWritten++;
            }

 //           remainByteToWrite = (WaveHeaderInfoIn2.dataLength / 2) - totalByteWritten;
            if (idx >= 0)
            {
                //Array.Clear(Out_1MB_Stereo, 0, Out_1MB_Stereo.Length);
           
                for (int b = 0; b < idx; b++)
                    bw.Write(Out_1MB_Stereo[b]);

            }
            br.Close();
            fs.Close();
            fs.Dispose();


            bw.Close();
            fo.Close();
            fo.Dispose();

            File.Delete(mainFile);
            File.Move(outFile, mainFile);

            //Int16[] OutStereo = new Int16[WaveHeaderInfoOut.dataLength / 2];
            ////Array.Clear(OutStereo, 0, OutStereo.Length);

            //FileStream fs = new FileStream(mainFile, FileMode.Open, FileAccess.Read);
            //BinaryReader br = new BinaryReader(fs);
            //fs.Position = 44;
            //int r;
            //for (r = 0; r < WaveHeaderInfoInMain.dataLength / 2; r++)
            //    OutStereo[r] = br.ReadInt16();
            //br.Close();
            //fs.Close();
            //fs.Dispose();

            //fs = new FileStream(file2Append, FileMode.Open, FileAccess.Read);
            //br = new BinaryReader(fs);
            //fs.Position = 44;
            //int l;
            //for (l = 0; l < WaveHeaderInfoIn2.dataLength / 2; l++)
            //    OutStereo[l + r] = br.ReadInt16();
            //br.Close();
            //fs.Close();
            //fs.Dispose();



            //clsWavManager.WaveHeaderOUT(outFile, WaveHeaderInfoOut);
            //FileStream fo = new FileStream(@outFile, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
            //BinaryWriter bw = new BinaryWriter(fo);
            //for (r = 0; r < OutStereo.Length; r++)
            //    bw.Write(OutStereo[r]);
            //bw.Close();
            //fo.Close();
            //fo.Dispose();

            //File.Delete(mainFile);
            //File.Move(outFile, mainFile);


            return 0;
        }


        public static int copypaste(string mainFile, int selIn, int selOut, int insertPoint, string workFolder)
        {
            if ((!File.Exists(mainFile)) || (!Directory.Exists(workFolder)))
                return -1;


            string outFile = "";
            string bkFile = "";

            if (workFolder.EndsWith("\\"))
            {
                outFile = workFolder + "aetemp.wav";
                bkFile = workFolder + "bk.wav";
            }
            else
            {
                outFile = workFolder + "\\aetemp.wav";
                bkFile = workFolder + "\\bk.wav";
            }

            if (File.Exists(outFile))
                File.Delete(outFile);

            clsWavManager.WaveHeaderInfo WaveHeaderInfoInMain = clsWavManager.WaveHeaderIN(mainFile);

            int selInSample = ms2byte(selIn, WaveHeaderInfoInMain.bitsPerSample, WaveHeaderInfoInMain.sampleRate, WaveHeaderInfoInMain.channels)/2;
            int selOutSample = ms2byte(selOut, WaveHeaderInfoInMain.bitsPerSample, WaveHeaderInfoInMain.sampleRate, WaveHeaderInfoInMain.channels) / 2;
            int insertPointSample = ms2byte(insertPoint, WaveHeaderInfoInMain.bitsPerSample, WaveHeaderInfoInMain.sampleRate, WaveHeaderInfoInMain.channels) / 2;

            if (selInSample < 0)
                selInSample = 0;

            if (selOutSample > WaveHeaderInfoInMain.dataLength / 2)
                selInSample = WaveHeaderInfoInMain.dataLength / 2;

            if (insertPointSample < 0)
                insertPointSample = 0;

            if (insertPointSample > WaveHeaderInfoInMain.dataLength / 2)
                insertPointSample = WaveHeaderInfoInMain.dataLength / 2;

            Int16[] mainInArray = new Int16[WaveHeaderInfoInMain.dataLength / 2];
            //Array.Clear(OutStereo, 0, OutStereo.Length);

            FileStream fs = new FileStream(mainFile, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            fs.Position = 44;
            int r;
            for (r = 0; r < WaveHeaderInfoInMain.dataLength / 2; r++)
                mainInArray[r] = br.ReadInt16();
            br.Close();
            fs.Close();
            fs.Dispose();


            int iOutLength = (WaveHeaderInfoInMain.dataLength / 2) + ((selOutSample - selInSample) );
            Int16[] OutStereo = new Int16[iOutLength];
            int k = 0;

            for (k = 0; k < insertPointSample; k++)
                OutStereo[k] = mainInArray[k];
            
            for (k = selInSample; k < selOutSample; k++)
                OutStereo[insertPointSample + (k - selInSample)] = mainInArray[k];

            for (k = insertPointSample; k < WaveHeaderInfoInMain.dataLength / 2; k++)
                OutStereo[insertPointSample + (selOutSample - selInSample) + (k - insertPointSample)] = mainInArray[k];


            clsWavManager.WaveHeaderInfo WaveHeaderInfoOut = new clsWavManager.WaveHeaderInfo();
            WaveHeaderInfoOut.bitsPerSample = WaveHeaderInfoInMain.bitsPerSample;
            WaveHeaderInfoOut.channels = WaveHeaderInfoInMain.channels;
            WaveHeaderInfoOut.dataLength = iOutLength * 2;
            WaveHeaderInfoOut.length = (WaveHeaderInfoInMain.length - WaveHeaderInfoInMain.dataLength) + WaveHeaderInfoOut.dataLength;
            WaveHeaderInfoOut.sampleRate = WaveHeaderInfoInMain.sampleRate;


            clsWavManager.WaveHeaderOUT(outFile, WaveHeaderInfoOut);
            FileStream fo = new FileStream(@outFile, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
            BinaryWriter bw = new BinaryWriter(fo);
            for (r = 0; r < OutStereo.Length; r++)
                bw.Write(OutStereo[r]);
            bw.Close();
            fo.Close();
            fo.Dispose();


            if (File.Exists(bkFile))
                File.Delete(bkFile);
            File.Copy(mainFile, bkFile);

        
            File.Delete(mainFile);
            File.Move(outFile, mainFile);


            return 0;
        }

        public static int restoreBackup(string mainFile, string workFolder)
        {
            if ((!File.Exists(mainFile)) || (!Directory.Exists(workFolder)))
                return -1;

            string bkFile = "";

            if (workFolder.EndsWith("\\"))
            {
                bkFile = workFolder + "bk.wav";
            }
            else
            {
                bkFile = workFolder + "\\bk.wav";
            }

            if (!File.Exists(bkFile))
                return -1;

            File.Delete(mainFile);
            File.Move(bkFile, mainFile);


            return 0;
        }

        private static int ms2byte(int iTime, int bitsPerSample, int sampleRate, int channels)
        {
            return ((sampleRate / 1000) * bitsPerSample * channels * iTime) / 8;
        }


        public static bool IsCutExists(string workFolder)
        {
            string outFile = "";
            string bkFile = "";
            string cutFile = "";
            string cuttedFile = "";

            if (workFolder.EndsWith("\\"))
            {
                outFile = workFolder + "aetemp.wav";
                bkFile = workFolder + "bk.wav";
                cutFile = workFolder + "cuttemp.wav";
                cuttedFile = workFolder + "cuttedtemp.wav";
            }
            else
            {
                outFile = workFolder + "\\aetemp.wav";
                bkFile = workFolder + "\\bk.wav";
                cutFile = workFolder + "\\cuttemp.wav";
                cuttedFile = workFolder + "\\cuttedtemp.wav";
            }


            if ((!File.Exists(cutFile)) || (!File.Exists(cuttedFile)))
                return false;

            return true;
        }


        public static bool IsUndoExists(string mainFile, string workFolder)
        {
            if (!workFolder.EndsWith("\\"))
                workFolder += "\\";

            DirectoryInfo dirInfo = new DirectoryInfo(workFolder);

            FileInfo[] undoFileList = null;
            //Cerca tutti i file undo
            if(mainFile.Contains("voice"))
                undoFileList = dirInfo.GetFiles("*voice*undo.wav");
            else
                undoFileList = dirInfo.GetFiles("*music*undo.wav");

            if (undoFileList == null)
                return false;

            if(undoFileList.Length == 0)
                return false;

            return true;
        }


        public static void Insert(string mainFile, string wavClip, int insertPoint, short bps, int sampleRate)
        {
            FileStream fs = new FileStream(mainFile, FileMode.Open, FileAccess.ReadWrite);

            BinaryWriter bw = new BinaryWriter(fs);

            bw.Seek(44, SeekOrigin.Begin);    // FINE HEADER


            int insertPointSample = ms2byte(insertPoint, bps, sampleRate, 1);

            bw.Seek(insertPointSample, SeekOrigin.Current);     // INSERT POSITION


            FileStream fr = new FileStream(wavClip, FileMode.Open, FileAccess.Read);
            byte[] readBuffer = new byte[fr.Length - 44];
            fr.Seek(44, SeekOrigin.Begin);
            fr.Read(readBuffer, 0, readBuffer.Length);
            fr.Close();
            fr.Dispose();

            bw.Write(readBuffer);
            bw.Close();


            fs.Close();
            fs.Dispose();
        }


        public static int paste(string mainFile, int insertPoint, string workFolder)
        {
            return paste(mainFile, insertPoint, workFolder, true);
        }
        public static int paste(string mainFile, int insertPoint, string workFolder, bool makeundo)
        {
            if(makeundo)
                CreateUndo(mainFile, workFolder);

            if ((!File.Exists(mainFile)) || (!Directory.Exists(workFolder)))
                return -1;


            string outFile = "";
            string bkFile = "";
            string cutFile = "";
            string cuttedFile = "";

            if (workFolder.EndsWith("\\"))
            {
                outFile = workFolder + "aetemp.wav";
                bkFile = workFolder + "bk.wav";
                cutFile = workFolder + "cuttemp.wav";
                cuttedFile = workFolder + "cuttedtemp.wav";
            }
            else
            {
                outFile = workFolder + "\\aetemp.wav";
                bkFile = workFolder + "\\bk.wav";
                cutFile = workFolder + "\\cuttemp.wav";
                cuttedFile = workFolder + "\\cuttedtemp.wav";
            }

            if (!File.Exists(cutFile))
                return -1;

            if (File.Exists(outFile))
                File.Delete(outFile);

            //clsWavManager.WaveHeaderInfo WaveHeaderInfoInMain = clsWavManager.WaveHeaderIN(cuttedFile);
            clsWavManager.WaveHeaderInfo WaveHeaderInfoInMain = clsWavManager.WaveHeaderIN(mainFile);

            int insertPointSample = ms2byte(insertPoint, WaveHeaderInfoInMain.bitsPerSample, WaveHeaderInfoInMain.sampleRate, WaveHeaderInfoInMain.channels) / 2;

            if (insertPointSample > (WaveHeaderInfoInMain.dataLength / 2))
                insertPointSample = WaveHeaderInfoInMain.dataLength / 2;


            Int16[] mainInArray = new Int16[WaveHeaderInfoInMain.dataLength / 2];


            //FileStream fs = new FileStream(cuttedFile, FileMode.Open, FileAccess.Read);
            FileStream fs = new FileStream(mainFile, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            fs.Position = 44;
            int r;
            for (r = 0; r < WaveHeaderInfoInMain.dataLength / 2; r++)
                mainInArray[r] = br.ReadInt16();
            br.Close();
            fs.Close();
            fs.Dispose();


            clsWavManager.WaveHeaderInfo WaveHeaderInfoInCut = clsWavManager.WaveHeaderIN(cutFile);

            Int16[] cutInArray = new Int16[WaveHeaderInfoInCut.dataLength / 2];

            fs = new FileStream(cutFile, FileMode.Open, FileAccess.Read);
            br = new BinaryReader(fs);
            fs.Position = 44;
            for (r = 0; r < WaveHeaderInfoInCut.dataLength / 2; r++)
                cutInArray[r] = br.ReadInt16();
            br.Close();
            fs.Close();
            fs.Dispose();

            
            
            int iOutLength = (WaveHeaderInfoInMain.dataLength / 2) + (WaveHeaderInfoInCut.dataLength / 2);
            Int16[] OutStereo = new Int16[iOutLength];
            int k = 0;

            for (k = 0; k < insertPointSample; k++)
                OutStereo[k] = mainInArray[k];

            for (k = 0; k < (WaveHeaderInfoInCut.dataLength / 2); k++)
                OutStereo[insertPointSample + k] = cutInArray[k];

            for (k = insertPointSample; k < (WaveHeaderInfoInMain.dataLength / 2); k++)
                OutStereo[(WaveHeaderInfoInCut.dataLength / 2) + k] = mainInArray[k];


            clsWavManager.WaveHeaderInfo WaveHeaderInfoOut = new clsWavManager.WaveHeaderInfo();
            WaveHeaderInfoOut.bitsPerSample = WaveHeaderInfoInMain.bitsPerSample;
            WaveHeaderInfoOut.channels = WaveHeaderInfoInMain.channels;
            WaveHeaderInfoOut.dataLength = iOutLength * 2;
            WaveHeaderInfoOut.length = (WaveHeaderInfoInMain.length - WaveHeaderInfoInMain.dataLength) + WaveHeaderInfoOut.dataLength;
            WaveHeaderInfoOut.sampleRate = WaveHeaderInfoInMain.sampleRate;


            clsWavManager.WaveHeaderOUT(outFile, WaveHeaderInfoOut);
            FileStream fo = new FileStream(@outFile, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
            BinaryWriter bw = new BinaryWriter(fo);
            for (r = 0; r < OutStereo.Length; r++)
                bw.Write(OutStereo[r]);
            bw.Close();
            fo.Close();
            fo.Dispose();

            if (File.Exists(bkFile))
                File.Delete(bkFile);
            File.Copy(mainFile, bkFile);


            File.Delete(mainFile);
            File.Move(outFile, mainFile);

            return 0;
        }


        public static int deleteSelection(string mainFile, int selIn, int selOut, string workFolder)
        {
            CreateUndo(mainFile, workFolder);

            savecut(mainFile,selIn,selOut,workFolder);

            string cuttedFile = "";
            if (workFolder.EndsWith("\\"))
            {
                cuttedFile = workFolder + "cuttedtemp.wav";
            }
            else
            {
                cuttedFile = workFolder + "\\cuttedtemp.wav";
            }

            try
            {
                File.Copy(cuttedFile, mainFile, true);
            }
            catch { }

            return 0;
        }

        public static int GetCountUndoForVoice(string workFolder)
        {
            int numUndo = 0;

            if (!workFolder.EndsWith("\\"))
                workFolder += "\\";

            DirectoryInfo dirInfo = new DirectoryInfo(workFolder);

            FileInfo[] undoFileList = null;
            undoFileList = dirInfo.GetFiles("*voice*undo.wav");
            if (undoFileList != null)
                numUndo = undoFileList.Length;

            return numUndo;
        }

        public static int GetCountUndoForSound(string workFolder)
        {
            int numUndo = 0;

            if (!workFolder.EndsWith("\\"))
                workFolder += "\\";

            DirectoryInfo dirInfo = new DirectoryInfo(workFolder);

            FileInfo[] undoFileList = null;
            undoFileList = dirInfo.GetFiles("*music*undo.wav");
            if (undoFileList != null)
                numUndo = undoFileList.Length;

            return numUndo;
        }

        public static void loadUndo(string mainFile, string workFolder)
        {
            if (!workFolder.EndsWith("\\"))
                workFolder += "\\";

            DirectoryInfo dirInfo = new DirectoryInfo(workFolder);

            FileInfo[] undoFileList = null;
            //Cerca tutti i file undo
            if (mainFile.Contains("voice"))
            {
                undoFileList = dirInfo.GetFiles("*voice*undo.wav");
            }
            else
                undoFileList = dirInfo.GetFiles("*music*undo.wav");

            if (undoFileList != null)
            {
                Array.Sort(undoFileList, delegate(FileInfo x, FileInfo y)
                {
                    return string.Compare(y.Name, x.Name);
                });

                if (undoFileList.Length > 0)
                {
                    if (File.Exists(undoFileList[0].FullName))
                    {
                        if (File.Exists(mainFile))
                            File.Delete(mainFile);

                        File.Copy(undoFileList[0].FullName, mainFile);

                        File.Delete(undoFileList[0].FullName);
                    }
                }
            }
        }

        public static void CreateUndo(string mainFile, string workFolder)
        {
            if (!workFolder.EndsWith("\\"))
                workFolder += "\\";

            DirectoryInfo dirInfo = new DirectoryInfo(workFolder);

            FileInfo[] undoFileList = null;

            //Cerca tutti i file undo
            if (mainFile.Contains("voice"))
            {
                undoFileList = dirInfo.GetFiles("*voice*undo.wav");
            }else
                undoFileList = dirInfo.GetFiles("*music*undo.wav");

            string undoFile = workFolder + Path.GetFileName(mainFile).Replace(".wav", undoFileList.Length + "_undo.wav");
            if (File.Exists(mainFile))
                File.Copy(mainFile, undoFile);

        }

        public static int savecut(string mainFile, int selIn, int selOut, string workFolder)
        {
            if ((!File.Exists(mainFile)) || (!Directory.Exists(workFolder)))
                return -1;

            string cutFile = "";
            string cuttedFile = "";


            if (workFolder.EndsWith("\\"))
            {
                cutFile = workFolder + "cuttemp.wav";
                cuttedFile = workFolder + "cuttedtemp.wav";
            }
            else
            {
                cutFile = workFolder + "\\cuttemp.wav";
                cuttedFile = workFolder + "\\cuttedtemp.wav";
            }

            if (File.Exists(cutFile))
                File.Delete(cutFile);
            if (File.Exists(cuttedFile))
                File.Delete(cuttedFile);

            clsWavManager.WaveHeaderInfo WaveHeaderInfoInMain = clsWavManager.WaveHeaderIN(mainFile);

            int selInSample = ms2byte(selIn, WaveHeaderInfoInMain.bitsPerSample, WaveHeaderInfoInMain.sampleRate, WaveHeaderInfoInMain.channels) / 2;
            int selOutSample = ms2byte(selOut, WaveHeaderInfoInMain.bitsPerSample, WaveHeaderInfoInMain.sampleRate, WaveHeaderInfoInMain.channels) / 2;

            if (selInSample < 0)
                selInSample = 0;

            if (selOutSample > WaveHeaderInfoInMain.dataLength / 2)
                selOutSample = WaveHeaderInfoInMain.dataLength / 2;

            Int16[] mainInArray = new Int16[WaveHeaderInfoInMain.dataLength / 2];


            FileStream fs = new FileStream(mainFile, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            fs.Position = 44;
            int r;
            for (r = 0; r < WaveHeaderInfoInMain.dataLength / 2; r++)
                mainInArray[r] = br.ReadInt16();
            br.Close();
            fs.Close();
            fs.Dispose();


            int iOutLength = selOutSample - selInSample;
            Int16[] OutStereo = new Int16[iOutLength];
            int k = 0;

            for (k = selInSample; k < selOutSample; k++)
                OutStereo[k - selInSample] = mainInArray[k];


            clsWavManager.WaveHeaderInfo WaveHeaderInfoOut = new clsWavManager.WaveHeaderInfo();
            WaveHeaderInfoOut.bitsPerSample = WaveHeaderInfoInMain.bitsPerSample;
            WaveHeaderInfoOut.channels = WaveHeaderInfoInMain.channels;
            WaveHeaderInfoOut.dataLength = iOutLength * 2;
            WaveHeaderInfoOut.length = (WaveHeaderInfoInMain.length - WaveHeaderInfoInMain.dataLength) + WaveHeaderInfoOut.dataLength;
            WaveHeaderInfoOut.sampleRate = WaveHeaderInfoInMain.sampleRate;


            clsWavManager.WaveHeaderOUT(cutFile, WaveHeaderInfoOut);
            FileStream fo = new FileStream(@cutFile, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
            BinaryWriter bw = new BinaryWriter(fo);
            for (r = 0; r < OutStereo.Length; r++)
                bw.Write(OutStereo[r]);
            bw.Close();
            fo.Close();
            fo.Dispose();


            iOutLength = (WaveHeaderInfoInMain.dataLength / 2) - (selOutSample - selInSample);
            OutStereo = new Int16[iOutLength];


            for (k = 0; k < selInSample; k++)
                OutStereo[k] = mainInArray[k];

            for (k = selOutSample; k < WaveHeaderInfoInMain.dataLength / 2; k++)
                OutStereo[(k - selOutSample) + selInSample] = mainInArray[k];


            WaveHeaderInfoOut = new clsWavManager.WaveHeaderInfo();
            WaveHeaderInfoOut.bitsPerSample = WaveHeaderInfoInMain.bitsPerSample;
            WaveHeaderInfoOut.channels = WaveHeaderInfoInMain.channels;
            WaveHeaderInfoOut.dataLength = iOutLength * 2;
            WaveHeaderInfoOut.length = (WaveHeaderInfoInMain.length - WaveHeaderInfoInMain.dataLength) + WaveHeaderInfoOut.dataLength;
            WaveHeaderInfoOut.sampleRate = WaveHeaderInfoInMain.sampleRate;


            clsWavManager.WaveHeaderOUT(cuttedFile, WaveHeaderInfoOut);
            fo = new FileStream(@cuttedFile, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
            bw = new BinaryWriter(fo);
            for (r = 0; r < OutStereo.Length; r++)
                bw.Write(OutStereo[r]);
            bw.Close();
            fo.Close();
            fo.Dispose();


            return 0;
        }


        public static int pasteOld(string mainFile, int insertPoint, string workFolder)
        {
            if ((!File.Exists(mainFile)) || (!Directory.Exists(workFolder)))
                return -1;


            string outFile = "";
            string bkFile = "";
            string cutFile = "";
            string cuttedFile = "";

            if (workFolder.EndsWith("\\"))
            {
                outFile = workFolder + "aetemp.wav";
                bkFile = workFolder + "bk.wav";
                cutFile = workFolder + "cuttemp.wav";
                cuttedFile = workFolder + "cuttedtemp.wav";
            }
            else
            {
                outFile = workFolder + "\\aetemp.wav";
                bkFile = workFolder + "\\bk.wav";
                cutFile = workFolder + "\\cuttemp.wav";
                cuttedFile = workFolder + "\\cuttedtemp.wav";
            }

            if ((!File.Exists(cutFile)) || (!File.Exists(cuttedFile)))
                return -1;

            if (File.Exists(outFile))
                File.Delete(outFile);

            clsWavManager.WaveHeaderInfo WaveHeaderInfoInMain = clsWavManager.WaveHeaderIN(cuttedFile);

            int insertPointSample = ms2byte(insertPoint, WaveHeaderInfoInMain.bitsPerSample, WaveHeaderInfoInMain.sampleRate, WaveHeaderInfoInMain.channels) / 2;

            if (insertPointSample > (WaveHeaderInfoInMain.dataLength / 2))
                insertPointSample = WaveHeaderInfoInMain.dataLength / 2;


            Int16[] mainInArray = new Int16[WaveHeaderInfoInMain.dataLength / 2];


            FileStream fs = new FileStream(cuttedFile, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            fs.Position = 44;
            int r;
            for (r = 0; r < WaveHeaderInfoInMain.dataLength / 2; r++)
                mainInArray[r] = br.ReadInt16();
            br.Close();
            fs.Close();
            fs.Dispose();


            clsWavManager.WaveHeaderInfo WaveHeaderInfoInCut = clsWavManager.WaveHeaderIN(cutFile);

            Int16[] cutInArray = new Int16[WaveHeaderInfoInCut.dataLength / 2];

            fs = new FileStream(cutFile, FileMode.Open, FileAccess.Read);
            br = new BinaryReader(fs);
            fs.Position = 44;
            for (r = 0; r < WaveHeaderInfoInCut.dataLength / 2; r++)
                cutInArray[r] = br.ReadInt16();
            br.Close();
            fs.Close();
            fs.Dispose();



            int iOutLength = (WaveHeaderInfoInMain.dataLength / 2) + (WaveHeaderInfoInCut.dataLength / 2);
            Int16[] OutStereo = new Int16[iOutLength];
            int k = 0;

            for (k = 0; k < insertPointSample; k++)
                OutStereo[k] = mainInArray[k];

            for (k = 0; k < (WaveHeaderInfoInCut.dataLength / 2); k++)
                OutStereo[insertPointSample + k] = cutInArray[k];

            for (k = insertPointSample; k < (WaveHeaderInfoInMain.dataLength / 2); k++)
                OutStereo[(WaveHeaderInfoInCut.dataLength / 2) + k] = mainInArray[k];


            clsWavManager.WaveHeaderInfo WaveHeaderInfoOut = new clsWavManager.WaveHeaderInfo();
            WaveHeaderInfoOut.bitsPerSample = WaveHeaderInfoInMain.bitsPerSample;
            WaveHeaderInfoOut.channels = WaveHeaderInfoInMain.channels;
            WaveHeaderInfoOut.dataLength = iOutLength * 2;
            WaveHeaderInfoOut.length = (WaveHeaderInfoInMain.length - WaveHeaderInfoInMain.dataLength) + WaveHeaderInfoOut.dataLength;
            WaveHeaderInfoOut.sampleRate = WaveHeaderInfoInMain.sampleRate;


            clsWavManager.WaveHeaderOUT(outFile, WaveHeaderInfoOut);
            FileStream fo = new FileStream(@outFile, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
            BinaryWriter bw = new BinaryWriter(fo);
            for (r = 0; r < OutStereo.Length; r++)
                bw.Write(OutStereo[r]);
            bw.Close();
            fo.Close();
            fo.Dispose();

            if (File.Exists(bkFile))
                File.Delete(bkFile);
            File.Copy(mainFile, bkFile);


            File.Delete(mainFile);
            File.Move(outFile, mainFile);

            return 0;
        }


    }
}
