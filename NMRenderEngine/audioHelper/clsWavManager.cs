﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace audioHelper
{
    public static class clsWavManager
    {
        public class WaveHeaderInfo
        { 
            public int sampleRate;
            public short bitsPerSample;
            public int length;
            public int dataLength;
            public int channels;
        }

        public static WaveHeaderInfo WaveHeaderIN(string spath)
        {
            WaveHeaderInfo HeaderInfo = new WaveHeaderInfo();

            try
            {
                FileStream fs = new FileStream(spath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);


                BinaryReader br = new BinaryReader(fs);
                HeaderInfo.length = (int)fs.Length - 8;
                fs.Position = 22;
                HeaderInfo.channels = br.ReadInt16();
                fs.Position = 24;
                HeaderInfo.sampleRate = br.ReadInt32();
                fs.Position = 34;

                HeaderInfo.bitsPerSample = br.ReadInt16();
                HeaderInfo.dataLength = (int)fs.Length - 44;
                br.Close();
                fs.Close();

            }
            catch { }


            return HeaderInfo;
        }

        public static void WaveHeaderOUT(string sPath, WaveHeaderInfo HeaderInfo)
        {
            try
            {
                FileStream fs = new FileStream(sPath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);

                BinaryWriter bw = new BinaryWriter(fs);
                fs.Position = 0;
                bw.Write(new char[4] { 'R', 'I', 'F', 'F' });

                bw.Write(HeaderInfo.length);

                bw.Write(new char[8] { 'W', 'A', 'V', 'E', 'f', 'm', 't', ' ' });

                bw.Write((int)16);

                bw.Write((short)1);
                bw.Write((Int16)HeaderInfo.channels);

                bw.Write(HeaderInfo.sampleRate);

                bw.Write((int)(HeaderInfo.sampleRate * ((HeaderInfo.bitsPerSample * HeaderInfo.channels) / 8)));

                bw.Write((short)((HeaderInfo.bitsPerSample * HeaderInfo.channels) / 8));

                bw.Write(HeaderInfo.bitsPerSample);

                bw.Write(new char[4] { 'd', 'a', 't', 'a' });
                bw.Write(HeaderInfo.dataLength);
                bw.Close();
                fs.Close();
            }
            catch { }
        }

        public static int CreateEmptyWav(string referenceWav, string outFile)
        {
            WaveHeaderInfo WaveHeaderInfoIn = WaveHeaderIN(referenceWav);

            WaveHeaderInfo WaveHeaderInfoOut = new WaveHeaderInfo();
            WaveHeaderInfoOut.bitsPerSample = WaveHeaderInfoIn.bitsPerSample;
            WaveHeaderInfoOut.channels = WaveHeaderInfoIn.channels;
            WaveHeaderInfoOut.dataLength = WaveHeaderInfoIn.dataLength;
            WaveHeaderInfoOut.length = WaveHeaderInfoIn.length;
            WaveHeaderInfoOut.sampleRate = WaveHeaderInfoIn.sampleRate;

            Int16[] OutStereo = new Int16[WaveHeaderInfoOut.dataLength / 2];
            Array.Clear(OutStereo, 0, OutStereo.Length);

            WaveHeaderOUT(outFile, WaveHeaderInfoOut);
            FileStream fo = new FileStream(@outFile, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
            BinaryWriter bw = new BinaryWriter(fo);
            for (int r = 0; r < OutStereo.Length; r++)
                bw.Write(OutStereo[r]);
            bw.Close();
            fo.Close();
            fo.Dispose();


            return 0;
        }

        public static long GetAudioDuration(string fileWav)
        {
            WaveHeaderInfo WaveHeaderInfoIn = WaveHeaderIN(fileWav);

            float totalBits = WaveHeaderInfoIn.dataLength * 8;

            float duration = (totalBits / ((float)WaveHeaderInfoIn.channels));
            duration /= (float)WaveHeaderInfoIn.bitsPerSample;
            duration /= (float)WaveHeaderInfoIn.sampleRate;



            return (long)(duration * 1000) ;
        }

        public static int ConvertMonoToStereo(string inFile, string outFile)
        {
            WaveHeaderInfo WaveHeaderInfoIn = WaveHeaderIN(inFile);

            if (WaveHeaderInfoIn.channels > 1)
                return -1;


            WaveHeaderInfo WaveHeaderInfoOut = new WaveHeaderInfo();
            WaveHeaderInfoOut.bitsPerSample = WaveHeaderInfoIn.bitsPerSample;
            WaveHeaderInfoOut.channels = 2;
            WaveHeaderInfoOut.dataLength = WaveHeaderInfoIn.dataLength * 2;
            WaveHeaderInfoOut.length = WaveHeaderInfoIn.length * 2;
            WaveHeaderInfoOut.sampleRate = WaveHeaderInfoIn.sampleRate;

            //read file wav to convert
            FileStream fs = new FileStream(@inFile, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            Int16[] arrfile = new Int16[WaveHeaderInfoIn.dataLength / 2];
            fs.Position = 44;
            int r;
            for (r = 0; r < WaveHeaderInfoIn.dataLength / 2; r++)
            {
                arrfile[r] = br.ReadInt16();
            }
            br.Close();
            fs.Close();

            Int16[] OutStereo = new Int16[WaveHeaderInfoOut.dataLength / 2];

            long OutPos = 0;
            for (long i = 0; i < arrfile.Length; i ++)
            {
                OutStereo[OutPos] = arrfile[i];
                OutStereo[OutPos+1] = arrfile[i];
                OutPos += 2;
            }

            WaveHeaderOUT(outFile, WaveHeaderInfoOut);
            FileStream fo = new FileStream(@outFile, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
            BinaryWriter bw = new BinaryWriter(fo);
            for (r = 0; r < OutStereo.Length; r++)
                bw.Write(OutStereo[r]);
            bw.Close();
            fo.Close();
            fo.Dispose();


            return 0;
        }        


        public static int Combine2MonosToStereo(string inFileL, string inFileR, string outFile)
        {
            WaveHeaderInfo WaveHeaderInfoIn = WaveHeaderIN(inFileL);
            if (WaveHeaderInfoIn.channels > 1)
                return -1;

            WaveHeaderInfo WaveHeaderInfoInR = WaveHeaderIN(inFileR);
            if (WaveHeaderInfoInR.channels > 1)
                return -1;

            WaveHeaderInfo WaveHeaderInfoOut = new WaveHeaderInfo();
            WaveHeaderInfoOut.bitsPerSample = WaveHeaderInfoIn.bitsPerSample;
            WaveHeaderInfoOut.channels = 2;
            WaveHeaderInfoOut.dataLength = WaveHeaderInfoIn.dataLength * 2;
            WaveHeaderInfoOut.length = WaveHeaderInfoIn.length * 2;
            WaveHeaderInfoOut.sampleRate = WaveHeaderInfoIn.sampleRate;

            //read files wav to convert
            FileStream fsL = new FileStream(@inFileL, FileMode.Open, FileAccess.Read);
            BinaryReader brL = new BinaryReader(fsL);
            Int16[] arrfileL = new Int16[WaveHeaderInfoIn.dataLength / 2];
            fsL.Position = 44;

            FileStream fsR = new FileStream(@inFileR, FileMode.Open, FileAccess.Read);
            BinaryReader brR = new BinaryReader(fsR);
            Int16[] arrfileR = new Int16[WaveHeaderInfoIn.dataLength / 2];
            fsR.Position = 44;
            
            int r;
            for (r = 0; r < WaveHeaderInfoIn.dataLength / 2; r++)
            {
                arrfileL[r] = brL.ReadInt16();
                arrfileR[r] = brR.ReadInt16();
            }
            brL.Close();
            fsL.Close();
            brR.Close();
            fsR.Close();

            Int16[] OutStereo = new Int16[WaveHeaderInfoOut.dataLength / 2];

            long OutPos = 0;
            for (long i = 0; i < arrfileL.Length; i++)
            {
                OutStereo[OutPos] = arrfileL[i];
                OutStereo[OutPos + 1] = arrfileR[i];
                OutPos += 2;
            }

            WaveHeaderOUT(outFile, WaveHeaderInfoOut);
            FileStream fo = new FileStream(@outFile, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
            BinaryWriter bw = new BinaryWriter(fo);
            for (r = 0; r < OutStereo.Length; r++)
                bw.Write(OutStereo[r]);
            bw.Close();
            fo.Close();
            fo.Dispose();


            return 0;
        }



        public static int SpitStereoTo2Monos(string inFile, string outFileL, string outFileR)
        {
            WaveHeaderInfo WaveHeaderInfoIn = WaveHeaderIN(inFile);

            WaveHeaderInfo WaveHeaderInfoOut = new WaveHeaderInfo();
            WaveHeaderInfoOut.bitsPerSample = WaveHeaderInfoIn.bitsPerSample;
            WaveHeaderInfoOut.channels = 1;
            WaveHeaderInfoOut.dataLength = WaveHeaderInfoIn.dataLength/2;
            WaveHeaderInfoOut.length = WaveHeaderInfoIn.length/2;
            WaveHeaderInfoOut.sampleRate = WaveHeaderInfoIn.sampleRate;

            
            //read file wav to convert
            FileStream fs = new FileStream(@inFile, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            Int16[] arrfile = new Int16[WaveHeaderInfoIn.dataLength/2];
            fs.Position = 44;
            int r;
            for(r=0;r<WaveHeaderInfoIn.dataLength/2;r++)
                arrfile[r] = br.ReadInt16();
            br.Close();
            fs.Close();

            Int16[] OutLeftChannel = new Int16[WaveHeaderInfoOut.dataLength/2];
            Int16[] OutRightChannel = new Int16[WaveHeaderInfoOut.dataLength / 2];

            long OutPos = 0;
            for (long i = 0; i < arrfile.Length; i += 2)
            {
                OutLeftChannel[OutPos] = arrfile[i];
                OutRightChannel[OutPos] = arrfile[i + 1];
                OutPos += 1;
            }
            
            WaveHeaderOUT(outFileL, WaveHeaderInfoOut);
            FileStream foL = new FileStream(@outFileL, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
            BinaryWriter bwL = new BinaryWriter(foL);
            for (r = 0; r < OutLeftChannel.Length; r++)
                bwL.Write(OutLeftChannel[r]);
            bwL.Close();
            foL.Close();
            foL.Dispose();


            WaveHeaderOUT(outFileR, WaveHeaderInfoOut);
            FileStream foR = new FileStream(@outFileR, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
            BinaryWriter bwR = new BinaryWriter(foR);
            for (r = 0; r < OutRightChannel.Length; r++)
                bwR.Write(OutRightChannel[r]);
            bwR.Close();
            foR.Close();
            foR.Dispose();

            
            return 0;
        }


        public static int ConvertStereoToMono(string inFile, string outFile)
        {
            WaveHeaderInfo WaveHeaderInfoIn = WaveHeaderIN(inFile);

            WaveHeaderInfo WaveHeaderInfoOut = new WaveHeaderInfo();
            WaveHeaderInfoOut.bitsPerSample = WaveHeaderInfoIn.bitsPerSample;
            WaveHeaderInfoOut.channels = 1;
            WaveHeaderInfoOut.dataLength = WaveHeaderInfoIn.dataLength / 2;
            WaveHeaderInfoOut.length = WaveHeaderInfoIn.length / 2;
            WaveHeaderInfoOut.sampleRate = WaveHeaderInfoIn.sampleRate;


            //read file wav to convert
            FileStream fs = new FileStream(@inFile, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            Int16[] arrfile = new Int16[WaveHeaderInfoIn.dataLength / 2];
            fs.Position = 44;
            int r;
            for (r = 0; r < WaveHeaderInfoIn.dataLength / 2; r++)
                arrfile[r] = br.ReadInt16();
            br.Close();
            fs.Close();

            /*
            Int16[] OutLeftChannel = new Int16[WaveHeaderInfoOut.dataLength / 2];
            Int16[] OutRightChannel = new Int16[WaveHeaderInfoOut.dataLength / 2];
            */

            Int16[] OutMono = new Int16[WaveHeaderInfoOut.dataLength / 2];


            long OutPos = 0;
            for (long i = 0; i < arrfile.Length; i += 2)
            {
                /*
                OutLeftChannel[OutPos] = arrfile[i];
                OutRightChannel[OutPos] = arrfile[i + 1];
                */
                OutMono[OutPos]=(Int16)(((float)arrfile[i] + (float) arrfile[i + 1])/2.0f);
                OutPos += 1;
            }

            WaveHeaderOUT(outFile, WaveHeaderInfoOut);
            FileStream fo = new FileStream(@outFile, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
            BinaryWriter bw = new BinaryWriter(fo);
            for (r = 0; r < OutMono.Length; r++)
                bw.Write(OutMono[r]);
            bw.Close();
            fo.Close();
            fo.Dispose();



            return 0;
        }



    }
}
