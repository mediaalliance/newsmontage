using System;

namespace Garbe.Sound
{
	/// <summary> Calculate the signal with some gain </summary>
	public sealed class DudaHRTFRight : SoundObj
	{
		private Source   _source;
		private Listener _listener;
		private Ambient  _ambient;

		private int      _delayValue;
		private double   _angle;
		private float    _alpha;
		private float    _beta;
		private float    _ebt;
		private float    _ebta;
		private float    _anterior;

		private Delay    _delayFilter;

		/// <summary> Calculate the signal with some gain </summary>
		public DudaHRTFRight(Source s, Listener l, Ambient a, uint f) : base()
		{
			float dif_x;
			float dif_y;

			_source   = s;
			_listener = l;
			_ambient  = a;

			dif_x = _source.PositionX - _listener.PositionX;
			dif_y = _source.PositionY - _listener.PositionY;

			_angle = Math.Atan2(dif_y, dif_x) - _listener.Angle;
			_alpha = (float)(1 + Math.Sin(_angle));
			_beta  = (2 * _ambient.Velocity) / 0.0885f;
			_ebt = (float)(Math.Exp(-_beta / f));

			if(_alpha >= 0.01)
				_ebta = (float)(Math.Exp(-_beta / (f * _alpha)));
			else
				_ebta = 0;

			_delayValue = (int)((0.0885f * f / _ambient.Velocity) * (1 - Math.Sin(_angle)) + 1);
			_delayFilter   = new Delay(_delayValue);

		}

		/// <summary> Do the signal processing </summary>
		public override void DoProcess()
		{
			_anterior = _delayFilter.Output;
			_delayFilter.DoProcess();

			base._output = (float)1.0*( _ebt * base.Output + ((1-_ebt)/(1-_ebta)) * (_delayFilter.Output - _ebta * _anterior) );
		}

		/// <summary> Get or Set the input object </summary>
		public override SoundObj Input
		{
			get{return(_input);}
			set
			{
				_input = value;
				_delayFilter.Input= value;
			}
		}

		/// <summary> Number of interations expected to do the signal processing </summary>
		public override int Interations
		{
			get{return(base._input.Interations + _delayValue + 1);}
		}

	}
}
