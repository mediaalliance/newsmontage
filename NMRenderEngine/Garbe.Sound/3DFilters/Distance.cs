using System;

namespace Garbe.Sound
{
	/// <summary> Calculate the signal with some gain </summary>
	public sealed class Distance : SoundObj
	{
		private Source   _source;
		private Listener _listener;
		private Ambient  _ambient;

		private float    _gainValue;
		private int      _delayValue;
		private float    _lowpassValue;

		private Gain     _gainFilter;
		private Delay    _delayFilter;
		private LowPass  _lowpassFilter;

		/// <summary> Calculate the signal with some gain </summary>
		public Distance(Source s, Listener l, Ambient a, uint f) : base()
		{
			float dif_x;
			float dif_y;
			float distance;
			double frequency;
			double temp;

			_source   = s;
			_listener = l;
			_ambient  = a;

			dif_x = _listener.PositionX - _source.PositionX;
			dif_y = _listener.PositionY - _source.PositionY;

			distance = (float)(Math.Sqrt(dif_x * dif_x + dif_y * dif_y));

			if(distance == 0)
				throw(new Exception("The source position can not be the same of the listener!!!"));

			_gainValue = (float)(1 / Math.Sqrt(distance) ); // aa distancia pode ser zero!!!!!
			_delayValue = (int)((distance * f) / _ambient.Velocity);

			frequency = Math.Exp(-0.596d * Math.Log10(distance) + 10.577d);
			temp = Math.Cos(2*Math.PI*(frequency/f));

			_lowpassValue = (float)( (2 - temp - Math.Sqrt((temp-2)*(temp-2) - 1)) / 10 );

			_gainFilter    = new Gain(_gainValue);		
			_delayFilter   = new Delay(_delayValue);
			_lowpassFilter = new LowPass(_lowpassValue);

			_delayFilter.Input   = _gainFilter;
			_lowpassFilter.Input = _delayFilter;

		}

		/// <summary> Do the signal processing </summary>
		public override void DoProcess()
		{
			_gainFilter.DoProcess();
			_delayFilter.DoProcess();
			_lowpassFilter.DoProcess();

			base._output = _lowpassFilter.Output;
		}

		/// <summary> Get or Set the input object </summary>
		public override SoundObj Input
		{
			get{return(_input);}
			set
			{
				_input = value;
				_gainFilter.Input= value;
			}
		}

		/// <summary> Number of interations expected to do the signal processing </summary>
		public override int Interations
		{
			get{return(base._input.Interations + _delayValue + 1);}
		}

	}
}
