using System;

namespace Garbe.Sound
{
	/// <summary> Create a delay of certain sample numbers in the signal </summary>
	public sealed class EarlyReflection: SoundObj
	{
		private DelayAux[]	_delay;
		private float[]		_temp;
		private float[]		_gain;
		private int			_index;
		private int			_intera;
		private uint		_sr;
		private float		_temp2;
		private float		_lpGain;

		/// <summary> Create a delay of certain sample numbers in the signal </summary>
		/// <param name="x">Number of samples delayed inthe signal</param>
		public EarlyReflection(ReflectionCollection rc, uint sr) : base()
		{
			int a;
			double temp;
			double frequency;

			frequency = Math.Exp(-0.596d * Math.Log10(20d) + 10.577d);
			temp = Math.Cos(2*Math.PI*(frequency/sr));

			_lpGain = (float)( 2 - temp - Math.Sqrt((temp-2)*(temp-2) - 1) );

			_index = rc.Counter;

			_sr = sr;

			_delay = new DelayAux[_index];
			_delay[0] = new DelayAux( (int)(sr * rc[0].Time) );
			for(a = 1; a < rc.Counter; a++)
				_delay[a] = new DelayAux( (int)(sr * (rc[a].Time - rc[a-1].Time)) );

			_temp = new float[_index];

			_gain = new float[_index];
			for(a = 0; a < _index; a++)
                _gain[a] = rc[a].Amplitude;	
		
			_intera = (int)(sr * rc[_index - 1].Time);
				
		}

		/// <summary> Do the signal processing </summary>
		public override void DoProcess()
		{
			int a;

			_temp[0] = _delay[0].DoProcess(base._input.Output);
			for(a = 1; a < _index; a++)
				_temp[a] = _delay[a].DoProcess(_temp[a-1]);

			_temp2 = 0;
			for(a = 0; a < _index; a++)
				_temp2 += (_gain[a] * _temp[a]);

			base._output = (1 - _lpGain) * _temp2 + _lpGain * base._output;
		}
		
		/// <summary> Number of interations expected to do the signal processing </summary>
		public override int Interations
		{
			get{return(base._input.Interations + _intera);}
		}

		/// <summary> Number of interations expected to do the signal processing </summary>
		public override uint SampleRate
		{
			get{return(_sr);}
		}
	}
}