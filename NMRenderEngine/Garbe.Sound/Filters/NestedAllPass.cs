using System;

namespace Garbe.Sound
{
	/// <summary> Calculate the signal with a all pass filter </summary>
	public sealed class NestedAllPass : SoundObj
	{
		private NestedAllPassAux  _allPass;

		/// <summary> Calculate the signal with a all pass filter </summary>
		/// <param name="g1">Value of the internal gain of the nested all pass filter</param>
		/// <param name="g2">Value of the external gain of the nested all pass filter</param>
		/// <param name="d1">Value of the internal delay of the nested all pass filter</param>
		/// <param name="d2">Value of the external delay of the nested all pass filter</param>
		public NestedAllPass(float g1, float g2, int d1, int d2) : base()
		{
			_allPass = new NestedAllPassAux(g1, g2, d1, d2);
		}

		/// <summary> Do the signal processing </summary>
		public override void DoProcess()
		{
			base._output = _allPass.DoProcess(base._input.Output);
		}
		
		/// <summary> Number of interations expected to do the signal processing </summary>
		public override int Interations
		{
			get{return(base._input.Interations);}
		}

	}
}
