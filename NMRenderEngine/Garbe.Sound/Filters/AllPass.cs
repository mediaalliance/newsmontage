using System;

namespace Garbe.Sound
{
	/// <summary> Calculate the signal with a all pass filter </summary>
	public sealed class AllPass : SoundObj
	{
		private AllPassAux	_allPass;
		private int			_delayValue;

		/// <summary> Calculate the signal with a all pass filter </summary>
		/// <param name="g">Value of the gain of the all pass filter</param>
		/// <param name="d">Value of the delay of the all pass filter</param>
		public AllPass(float g, int d) : base()
		{
			_delayValue = d;
			_allPass = new AllPassAux(g, d);
		}

		/// <summary> Do the signal processing </summary>
		public override void DoProcess()
		{
			base._output = _allPass.DoProcess(base._input.Output);
		}
		
		/// <summary> Number of interations expected to do the signal processing </summary>
		public override int Interations
		{
			get{return(base._input.Interations);}
		}

	}
}

//using System;
//
//namespace Garbe.Sound
//{
//	/// <summary> Calculate the signal with a all pass filter </summary>
//	public sealed class AllPass : SoundObj
//	{
//		float	_gain;
//		int		_delay;
//		int		_pos;
//
//		float[]  _inputBuffer;
//		float[]  _outputBuffer;
//		
//
//		/// <summary> Calculate the signal with a all pass filter </summary>
//		/// <param name="g">Value of the gain of the all pass filter</param>
//		/// <param name="d">Value of the delay of the all pass filter</param>
//		public AllPass(float g, int d) : base()
//		{
//			_gain  = g;
//			_delay = d - 1;
//
//			_inputBuffer  = new float[d];
//			_outputBuffer = new float[d];
//
//			this.Reset();			
//		}
//
//		/// <summary> Do the signal processing </summary>
//		public override void DoProcess()
//		{
//			float inputTemp;
//			float outputTemp;
//
//			// Retrieve
//			inputTemp  = _inputBuffer[_pos];
//			outputTemp = _outputBuffer[_pos];
//			
//			// Calculate the output value in a temp variable
//			base._output = _gain * base._input.Output + inputTemp - _gain * outputTemp;
//
//			// Store
//			_inputBuffer[_pos]  = base._input.Output;
//			_outputBuffer[_pos] = base._output;	
//
//			if(_pos == _delay)
//				_pos = 0;
//			else
//				_pos++;
//		}
//		
//		/// <summary> Number of interations expected to do the signal processing </summary>
//		public override int Interations
//		{
//			get{return(base._input.Interations);}
//		}
//
//		/// <summary> Erase the data in the buffer of the delay </summary>
//		public void Reset()
//		{
//			for(_pos = 0; _pos <= _delay; _pos++)
//			{
//				_inputBuffer[_pos]  = 0; 
//				_outputBuffer[_pos] = 0; 
//			}
//
//			_pos = 0;
//		}
//
//
//	}
//}