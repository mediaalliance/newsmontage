using System;

namespace Garbe.Sound
{
	/// <summary> Create a delay of certain sample numbers in the signal </summary>
	public sealed class Delay : SoundObj
	{
		private DelayAux	_delay;
		private int			_delayValue;

		/// <summary> Create a delay of certain sample numbers in the signal </summary>
		/// <param name="x">Number of samples delayed inthe signal</param>
		public Delay(int x) : base()
		{
			_delayValue = x;
			_delay = new DelayAux(x);
		}

		/// <summary> Do the signal processing </summary>
		public override void DoProcess()
		{
			base._output = _delay.DoProcess(base._input.Output);
		}
		
		/// <summary> Number of interations expected to do the signal processing </summary>
		public override int Interations
		{
			get{return(base._input.Interations + _delayValue + 1);}
		}
	}
}
