using System;

namespace Garbe.Sound
{
	/// <summary> Calculate the signal with a all pass filter </summary>
	public sealed class SmallGardnerReverb : SoundObj
	{
		private DelayAux			_delay;
		private DNestedAllPassAux	_allPass1;
		private NestedAllPassAux	_allPass2;

		private float	_gain;
		private float	_tempDelay;
		private float	_tempAllPass1;
		private float	_tempAllPass2;
		private float	_tempLowPass;
		private float	_lpGain;

		/// <summary> Calculate the signal with a all pass filter </summary>
		/// <param name="g1">Value of the internal gain of the nested all pass filter</param>
		/// <param name="g2">Value of the external gain of the nested all pass filter</param>
		/// <param name="d1">Value of the internal delay of the nested all pass filter</param>
		/// <param name="d2">Value of the external delay of the nested all pass filter</param>
		public SmallGardnerReverb(float g) : base()
		{
			_gain = g;
			_tempLowPass = 0.0f;

			_delay = new DelayAux(1058);
			_allPass1 = new DNestedAllPassAux(0.4f, 0.6f, 0.3f, 970, 366, 1543);
			_allPass2 = new NestedAllPassAux(0.4f, 0.1f, 1323, 2910);
			
		}

		/// <summary> Do the signal processing </summary>
		public override void DoProcess()
		{
			_tempDelay		= _delay.DoProcess(base._input.Output + (_gain * _tempLowPass));
			_tempAllPass1	= _allPass1.DoProcess(_tempDelay);
			_tempAllPass2	= _allPass2.DoProcess(_tempAllPass1);
			_tempLowPass	= (1 - _lpGain) * _tempAllPass2 + _lpGain * _tempLowPass;

			base._output = _tempAllPass1 + _tempAllPass2;
		}
		
		/// <summary> Number of interations expected to do the signal processing </summary>
		public override int Interations
		{
			get{return(base._input.Interations);}
		}

		/// <summary> Get or Set the input object </summary>
		public override SoundObj Input
		{
			get{return(_input);}
			set
			{
				_input = value;

				double temp = Math.Cos(2*Math.PI*(4200.0f/this.SampleRate));
				_lpGain = (float)( 2 - temp - Math.Sqrt((temp-2)*(temp-2) - 1) );
			}
		}

	}
}