using System;

namespace Garbe.Sound
{
	/// <summary> Calculate the signal with a all pass filter </summary>
	public sealed class LargeGardnerReverb : SoundObj
	{

		private AllPassAux			_allPass1;
		private AllPassAux			_allPass2;
		private DelayAux			_delay1;
		private DelayAux			_delay2;
		private NestedAllPassAux	_allPass3;
		private DelayAux			_delay3;
		private DelayAux			_delay4;
		private DNestedAllPassAux	_allPass4;

		private float	_gain;

		private float	_tempDelay1;
		private float	_tempDelay3;
		private float	_tempAllPass3;
		private float	_tempLowPass;
		private float	_lpGain;

		/// <summary> Calculate the signal with a all pass filter </summary>
		/// <param name="g1">Value of the internal gain of the nested all pass filter</param>
		public LargeGardnerReverb(float g) : base()
		{
			_gain = g;
			_tempLowPass = 0.0f;

			_allPass1	= new AllPassAux(0.3f, 352);
			_allPass2	= new AllPassAux(0.3f, 529);
			_delay1		= new DelayAux(176);
			_delay2		= new DelayAux(749);
			_allPass3	= new NestedAllPassAux(0.25f, 0.5f, 2734, 3826);
			_delay3		= new DelayAux(1367);
			_delay4		= new DelayAux(661);
			_allPass4	= new DNestedAllPassAux(0.25f, 0.25f, 0.5f, 3351, 1323, 5292);
			
		}

		/// <summary> Do the signal processing </summary>
		public override void DoProcess()
		{
			_tempDelay1		= _delay1.DoProcess(_allPass2.DoProcess(_allPass1.DoProcess(base._input.Output + (_gain * _tempLowPass))));
			_tempDelay3		= _delay3.DoProcess(_allPass3.DoProcess(_delay2.DoProcess(_tempDelay1)));
			_tempAllPass3	= _allPass4.DoProcess(_delay4.DoProcess(_tempDelay3));
			_tempLowPass	= (1 - _lpGain) * _tempAllPass3 + _lpGain * _tempLowPass;

			base._output = (0.32f * _tempDelay1) + (0.14f * _tempDelay3) + (0.14f * _tempAllPass3);
		}
		
		/// <summary> Number of interations expected to do the signal processing </summary>
		public override int Interations
		{
			get{return(base._input.Interations);}
		}

		/// <summary> Get or Set the input object </summary>
		public override SoundObj Input
		{
			get{return(_input);}
			set
			{
				_input = value;

				double temp = Math.Cos(2*Math.PI*(2600.0f/this.SampleRate));
				_lpGain = (float)( 2 - temp - Math.Sqrt((temp-2)*(temp-2) - 1) );
			}
		}

	}
}