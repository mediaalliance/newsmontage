using System;

namespace Garbe.Sound
{
	/// <summary> Calculate the signal with a all pass filter </summary>
	public sealed class MediumGardnerReverb : SoundObj
	{
		private DNestedAllPassAux	_allPass1;
		private DelayAux			_delay1;
		private AllPassAux			_allPass2;
		private DelayAux			_delay2;
		private DelayAux			_delay3;
		private NestedAllPassAux	_allPass3;
		private DelayAux			_delay4;

		private float	_gain;
		private float	_tempAllPass1;
		private float	_tempDelay2;
		private float	_tempAllPass3;
		private float	_tempDelay4;
		private float	_tempLowPass;
		private float	_lpGain;

		/// <summary> Calculate the signal with a all pass filter </summary>
		/// <param name="g1">Value of the internal gain of the nested all pass filter</param>
		public MediumGardnerReverb(float g) : base()
		{
			_gain = g;
			_tempLowPass = 0.0f;

			_allPass1	= new DNestedAllPassAux(0.7f, 0.5f, 0.3f, 970, 366, 1543);
			_delay1		= new DelayAux(220);
			_allPass2	= new AllPassAux(0.5f, 1323);
			_delay2		= new DelayAux(2954);
			_delay3		= new DelayAux(661);
			_allPass3	= new NestedAllPassAux(0.6f, 0.3f, 432, 1719);
			_delay4		= new DelayAux(661);
			
		}

		/// <summary> Do the signal processing </summary>
		public override void DoProcess()
		{
			_tempAllPass1	= _allPass1.DoProcess(base._input.Output + (_gain * _tempLowPass));
			_tempDelay2		= _delay2.DoProcess(_allPass2.DoProcess(_delay1.DoProcess(_tempAllPass1)));
			_tempAllPass3	= _allPass3.DoProcess((_gain *_delay3.DoProcess(_tempDelay2)) + base._input.Output);
			_tempDelay4		= _delay4.DoProcess(_tempAllPass3);
			_tempLowPass	= (1 - _lpGain) * _tempDelay4 + _lpGain * _tempLowPass;

			base._output = _tempAllPass1 + _tempDelay2 + _tempAllPass3;
		}
		
		/// <summary> Number of interations expected to do the signal processing </summary>
		public override int Interations
		{
			get{return(base._input.Interations);}
		}

		/// <summary> Get or Set the input object </summary>
		public override SoundObj Input
		{
			get{return(_input);}
			set
			{
				_input = value;

				double temp = Math.Cos(2*Math.PI*(2500.0f/this.SampleRate));
				_lpGain = (float)( 2 - temp - Math.Sqrt((temp-2)*(temp-2) - 1) );
			}
		}

	}
}