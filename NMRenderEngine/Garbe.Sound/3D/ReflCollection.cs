using System;

namespace Garbe.Sound
{
	/// <summary>
	/// Summary description for EarlyReflections.
	/// </summary>
	public sealed class ReflectionCollection
	{
		private Reflection[] _early;
		private int _index;
		private int _pos;

		private float _maxDelay;
		private float _maxGain;
		private int   _maxGainIndex;
		private float _maxGainDelay;

		public ReflectionCollection(int index)
		{
			_index = index;
			_pos = 0;
			_early = new Reflection[index];

			_maxDelay = 0f;
			_maxGain = 0f;
			_maxGainIndex = 0;
			_maxGainDelay = 0f;
		}

		public void AddReflection(Reflection refl)
		{
			_early[_pos] = refl;
			_pos++;
		}

		public void Sort()
		{
			int a;
			int b;
			Reflection temp;

			// Bubble Sort

			for(a = 1; a < _index; ++a)
			{
				for(b = _index - 1; b >= a; --b)
				{
					if(_early[b-1].Time > _early[b].Time)
					{
						// exchange elements
						temp = _early[b-1];
						_early[b-1] = _early[b];
						_early[b] = temp;
					}
				}
			}

			// Fill Some data

			_maxDelay = _early[_index - 1].Time;
			for(a = 0; a < _index; a++)
			{
				if(_maxGain < _early[a].Amplitude)
				{
					_maxGain = _early[a].Amplitude;
					_maxGainIndex = a;
					_maxGainDelay = _early[a].Time;
				}
			}
		}

		public Reflection this[int index]
		{
			// Read one byte at offset index and return it.
			get 
			{
				return(_early[index]);
			}
		}

		public int Counter
		{
			get{return(_index);}
		}

		public float MaxDelay
		{
			get{return(_maxDelay);}
		}

		public float MaxGain
		{
			get{return(_maxGain);}
		}

		public int MaxGainIndex
		{
			get{return(_maxGainIndex);}
		}

		public float MaxGainDelay
		{
			get{return(_maxGainDelay);}
		}

		public static ReflectionCollection GetRightER(ReflectionCollection rc)
		{
			int a;
			float angle;
			float amp;
			float time;

			ReflectionCollection earlyRight = new ReflectionCollection(rc.Counter);

			for(a = 0; a < rc.Counter; a++)
			{
				angle = rc[a].Angle;
				amp = rc[a].Amplitude;
				time = rc[a].Time;

				amp *= (float)( (1 - Math.Sin(angle)) / 1.5d );

				earlyRight.AddReflection(new Reflection(time, amp, angle));
			}

			earlyRight.Sort();

			return(earlyRight);
		}

		public static ReflectionCollection GetLeftER(ReflectionCollection rc)
		{
			int a;
			float angle;
			float amp;
			float time;

			ReflectionCollection earlyRight = new ReflectionCollection(rc.Counter);

			for(a = 0; a < rc.Counter; a++)
			{
				angle = rc[a].Angle;
				amp = rc[a].Amplitude;
				time = rc[a].Time;

				amp *= (float)( (1 + Math.Sin(angle)) / 1.5d);

				earlyRight.AddReflection(new Reflection(time, amp, angle));
			}

			earlyRight.Sort();

			return(earlyRight);
		}

	}
}
