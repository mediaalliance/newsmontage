using System;

namespace Garbe.Sound
{
	/// <summary> Summary description for Listener. </summary>
	public class Listener
	{
		private float _xPosition;
		private float _yPosition;
		private float _zPosition;
		private float _angle;

		/// <summary> Summary description for Listener. </summary>
		public Listener(float x, float y, float z, float a)
		{
			_xPosition = x;
			_yPosition = y;
			_zPosition = z;
			_angle = a;
		}

		/// <summary> Get or set the x position of the listener </summary>
		public float PositionX
		{
			get { return _xPosition; }
			set {_xPosition = value; }
		}

		/// <summary> Get or set the y position of the listener </summary>
		public float PositionY
		{
			get { return _yPosition; }
			set {_yPosition = value; }
		}

		/// <summary> Get or set the z position of the listener </summary>
		public float PositionZ
		{
			get { return _zPosition; }
			set {_zPosition = value; }
		}

		/// <summary> Get or set the angle of the listener </summary>
		public float Angle
		{
			get { return ((float)(Math.PI * _angle / 180d)); }
			set {_angle = value; }
		}
	}
}
