using System;

namespace Garbe.Sound
{
	/// <summary> Summary description for Source. </summary>
	public class Source
	{
		private float _xPosition;
		private float _yPosition;
		private float _zPosition;

		/// <summary> Summary description for Source. </summary>
		public Source(float x, float y, float z)
		{
			_xPosition = x;
			_yPosition = y;
			_zPosition = z;
		}

		/// <summary> Get or set the x position of the source. </summary>
		public float PositionX
		{
			get { return _xPosition; }
			set {_xPosition = value; }
		}

		/// <summary> Get or set the y position of the source. </summary>
		public float PositionY
		{
			get { return _yPosition; }
			set {_yPosition = value; }
		}

		/// <summary> Get or set the z position of the source. </summary>
		public float PositionZ
		{
			get { return _zPosition; }
			set {_zPosition = value; }
		}
	}
}
