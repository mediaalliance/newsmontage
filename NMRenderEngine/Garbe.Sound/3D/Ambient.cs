using System;

namespace Garbe.Sound
{
	/// <summary> Summary description for Ambient. </summary>
	public class Ambient
	{
		private float _temperature;
		
		/// <summary> Summary description for Ambient. </summary>
		public Ambient()
		{
			_temperature = 30 + 273;
		}

		/// <summary> Summary description for Ambient. </summary>
		public Ambient(float t)
		{
			_temperature = t + 273;
		}

		/// <summary> Get or set the temperature of the ambient. </summary>
		public float Temperature
		{
			get { return _temperature; }
			set {_temperature = value; }
		}

		/// <summary> Get velocity of the air in the ambient. </summary>
		public float Velocity
		{
			get { return((float)(20.06f * Math.Sqrt(this._temperature))); }
		}
	}
}
