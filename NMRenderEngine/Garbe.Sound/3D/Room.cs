using System;

namespace Garbe.Sound
{
	/// <summary> Summary description for Source. </summary>
	public class Room
	{
		private float _xSize;
		private float _ySize;
		private float _zSize;

		private float _topAC;
		private float _bottomAC;
		private float _leftAC;
		private float _rightAC;
		private float _frontAC;
		private float _backAC;

		/// <summary> Summary description for Source. </summary>
		public Room(float xs, float ys, float zs, float ta, float boa, float la,
			float ra, float fa, float baa)
		{

			_xSize = xs;
			_ySize = ys;
			_zSize = zs;

			_topAC    = ta;
			_bottomAC = boa;
			_leftAC   = la;
			_rightAC  = ra;
			_frontAC  = fa;
			_backAC   = baa;

		}

		/// <summary> Get the Reverberation Time of the room by Sabine's equation. </summary>
		public float RT60
		{
			get
			{
				float volume;
				float sabine;
				
				volume = _xSize * _ySize * _zSize;
				sabine =	_topAC		* (_ySize * _xSize) +
							_bottomAC	* (_ySize * _xSize) +
							_leftAC		* (_zSize * _ySize) +
							_rightAC	* (_zSize * _ySize) +
							_frontAC	* (_xSize * _zSize) +
							_backAC		* (_xSize * _zSize);

				return((float)0.16 * (volume / sabine));
			}
		}

		public ReflectionCollection GetER(Source src, Listener lst, Ambient amb, int order)
		{
			int numberReflections;
			int index;
			int a;
			int b;
			int a1;
			int b1;
			int pos;

			float[] xCalc;
			float[] yCalc;
			
			float[] dist;
			float[] angle;
			float[] delay;
			float[] gain;

			index = 2 * order + 1;

			if(order > 0)
				numberReflections = index * index - 1;
			else
				throw new Exception("Order must be greater than zero!!!");

			if(src.PositionX > _xSize)
				throw new Exception("Source out of the room!!");
			if(src.PositionY > _ySize)
				throw new Exception("Source out of the room!!");
			if(src.PositionZ > _zSize)
				throw new Exception("Source out of the room!!");

			if(lst.PositionX > _xSize)
				throw new Exception("Listener out of the room!!");
			if(lst.PositionY > _ySize)
				throw new Exception("Listener out of the room!!");
			if(lst.PositionZ > _zSize)
				throw new Exception("Listener out of the room!!");

			xCalc = new float[index];
			yCalc = new float[index];
			dist = new float[numberReflections];
			angle = new float[numberReflections];
			delay = new float[numberReflections];
			gain = new float[numberReflections];

			for(a = 0; a < index; a++)
			{
				b = a - order;
				if(b == 0)
					xCalc[a] = src.PositionX - lst.PositionX;
				else if(b % 2 == 0 && b > 0)
					xCalc[a] = Math.Abs(b) * _xSize + src.PositionX - lst.PositionX;
				else if(b % 2 == 0 && b < 0)
					xCalc[a] = src.PositionX - Math.Abs(b) * _xSize - lst.PositionX;
				else if(b % 2 == 1 && b > 0)
					xCalc[a] = (Math.Abs(b - 1) + 2f) * _xSize - src.PositionX - lst.PositionX;
				else
					xCalc[a] = -(Math.Abs(b + 1) * _xSize) - src.PositionX - lst.PositionX;
			}

			for(a = 0; a < index; a++)
			{
				b = a - order;
				if(b == 0)
					yCalc[a] = src.PositionY - lst.PositionY;
				else if(b % 2 == 0 && b > 0)
					yCalc[a] = Math.Abs(b) * _ySize + src.PositionY - lst.PositionY;
				else if(b % 2 == 0 && b < 0)
					yCalc[a] = src.PositionY - Math.Abs(b) * _ySize - lst.PositionY;
				else if(b % 2 == 1 && b > 0)
					yCalc[a] = (Math.Abs(b - 1) + 2f) * _ySize - src.PositionY - lst.PositionY;
				else
					yCalc[a] = -(Math.Abs(b + 1) * _ySize) - src.PositionY - lst.PositionY;
			}

			pos = 0;
			for(a = 0; a < index; a++)
			{
				for(a1 = 0; a1 < index; a1++)
				{
					if((a1 - order != 0) || (a - order != 0))
					{
						dist[pos] = (float)( Math.Sqrt(Math.Pow(xCalc[a], 2d) + Math.Pow(yCalc[a1], 2d)) );
						angle[pos] = (float)( Math.Atan2(yCalc[a1], xCalc[a]) );

						angle[pos] = (float)( -((180d * (angle[pos] - lst.Angle) / Math.PI) - 90d) );
						if(angle[pos] > 180f)
							angle[pos] -= 360f;
						if(angle[pos] < -180f)
							angle[pos] += 360f;

						delay[pos] = dist[pos] / amb.Velocity;

						if(dist[pos] == 0)
							throw(new Exception("The source position can not be the same of the listener!!!"));

						gain[pos] = (2*this.RT60)*(1f / dist[pos]);

						b = a - order;
						if(b != 0)
						{
							if(b % 2 == 0 && b > 0)
								gain[pos] *= (float)(Math.Pow((1d-_rightAC), Math.Abs(b) / 2d) * 
									Math.Pow((1d-_leftAC), Math.Abs(b) / 2d));
							else if(b % 2 == 0 && b < 0)
								gain[pos] *= (float)(Math.Pow((1d-_rightAC), Math.Abs(b) / 2d) * 
									Math.Pow((1d-_leftAC), Math.Abs(b) / 2d));
							else if(b % 2 == 1 && b > 0)
								gain[pos] *= (float)(Math.Pow((1d-_rightAC), (Math.Abs(b - 1) / 2d) + 1d) * 
									Math.Pow((1d-_leftAC), Math.Abs(b - 1) / 2d));
							else
								gain[pos] *= (float)(Math.Pow((1d-_rightAC), Math.Abs(b + 1) / 2d) * 
									Math.Pow((1d-_leftAC), (Math.Abs(b + 1) / 2d) + 1d));
						}

						b1 = a1 - order;
						if(b1 != 0)
						{
							if(b1 % 2 == 0 && b1 > 0)
								gain[pos] *= (float)(Math.Pow((1d-_backAC), Math.Abs(b) / 2d) * 
									Math.Pow((1d-_frontAC), Math.Abs(b) / 2d));
							else if(b1 % 2 == 0 && b1 < 0)
								gain[pos] *= (float)(Math.Pow((1d-_backAC), Math.Abs(b) / 2d) * 
									Math.Pow((1d-_frontAC), Math.Abs(b) / 2d));
							else if(b1 % 2 == 1 && b1 > 0)
								gain[pos] *= (float)(Math.Pow((1d-_backAC), (Math.Abs(b - 1) / 2d) + 1d) * 
									Math.Pow((1d-_frontAC), Math.Abs(b - 1) / 2d));
							else
								gain[pos] *= (float)(Math.Pow((1d-_backAC), Math.Abs(b + 1) / 2d) * 
									Math.Pow((1d-_frontAC), (Math.Abs(b + 1) / 2d) + 1d));
						}

						pos++;
					}
				}
			}


			// Save the reflections information in the ReflectionCollection

			ReflectionCollection early = new ReflectionCollection(numberReflections);

			for(a = 0; a < numberReflections; a++)
				early.AddReflection(new Reflection(delay[a], gain[a], angle[a]));

			early.Sort();

			return(early);
		}
	}
}
