using System;

namespace Garbe.Sound
{
	/// <summary>
	/// Summary description for Reflection.
	/// </summary>
	public struct Reflection
	{
		public float Time;
		public float Amplitude;
		public float Angle;

		public Reflection(float time, float amp, float dir)
		{
			this.Time = time;
			this.Amplitude = amp;
			this.Angle = dir;
		}
	}
}
