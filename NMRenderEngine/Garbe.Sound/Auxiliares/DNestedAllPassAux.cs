using System;

namespace Garbe.Sound
{
	/// <summary>
	/// Summary description for AllPassAux.
	/// </summary>
	internal sealed class DNestedAllPassAux
	{
		private AllPassAux  _allPass1;
		private AllPassAux  _allPass2;
		private DelayAux	_delay;

		private float		_temp;
		private float		_gain;
		private float		_tempAllPass;

		internal DNestedAllPassAux(float g1, float g2, float g3, int d1, int d2, int d3)
		{
			_gain = g3;
			_temp = 0.0f;
			_allPass1 = new AllPassAux(g1, d1);
			_allPass2 = new AllPassAux(g2, d2);
			_delay = new DelayAux(d3);
		}

		internal float DoProcess(float input)
		{
			_tempAllPass = _allPass1.DoProcess(input + _gain * _temp);
			_tempAllPass = _allPass2.DoProcess(_tempAllPass);
			_temp = _delay.DoProcess(_tempAllPass) - (_gain * input);

			return(_temp);
		}
	}
}
