using System;

namespace Garbe.Sound
{
	/// <summary>
	/// Summary description for AllPassAux.
	/// </summary>
	internal sealed class NestedAllPassAux
	{
		private AllPassAux  _allPass;
		private DelayAux	_delay;

		private float		_temp;
		private float		_gain;
		private float		_tempAllPass;

		internal NestedAllPassAux(float g1, float g2, int d1, int d2)
		{
			_gain = g2;
			_temp = 0.0f;
			_allPass = new AllPassAux(g1, d1);
			_delay = new DelayAux(d2);
		}

		internal float DoProcess(float input)
		{
			_tempAllPass = _allPass.DoProcess(input + _gain * _temp);
			_temp = _delay.DoProcess(_tempAllPass) - _gain * input;

			return(_temp);
		}
	}
}
