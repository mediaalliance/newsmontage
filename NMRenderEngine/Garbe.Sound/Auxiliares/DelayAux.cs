using System;

namespace Garbe.Sound
{
	/// <summary>
	/// Summary description for DelayAux.
	/// </summary>
	internal sealed class DelayAux
	{
		float[]	_delayBuffer;
		int		_pos;
		int		_delay;

		float	_temp;

		internal DelayAux(int d)
		{
			if(d == 0)
				_delay = 0;
			else
			{
				_delay = d - 1;
				_delayBuffer = new float[d];
				this.Reset();
			}
			
		}

		internal float DoProcess(float input)
		{
			if(_delay == 0)
			{
				return(input);
			}
			else
			{
				// Retrieve
				_temp = _delayBuffer[_pos];
			
				// Store
				_delayBuffer[_pos] = input;
				if(_pos == _delay)
					_pos = 0;
				else
					_pos++;
			
				return(_temp);
			}
		}

		private void Reset()
		{
			for(_pos = 0; _pos <= _delay; _pos++)
				_delayBuffer[_pos] = 0; 

			_pos = 0;
		}
	}
}
