using System;

namespace Garbe.Sound
{
	/// <summary>
	/// Summary description for AllPassAux.
	/// </summary>
	internal sealed class AllPassAux
	{
		float	_gain;
		int		_delay;
		int		_pos;

		float	_inputTemp;
		float	_outputTemp;

		float[]  _inputBuffer;
		float[]  _outputBuffer;

		internal AllPassAux(float g, int d)
		{
			_gain  = g;
			_delay = d - 1;

			_inputBuffer  = new float[d];
			_outputBuffer = new float[d];

			this.Reset();	
		}

		internal float DoProcess(float input)
		{
			// Retrieve
			_inputTemp  = _inputBuffer[_pos];
			_outputTemp = _outputBuffer[_pos];
			
			// Calculate the output value in a temp variable
			_outputTemp = _gain * input + _inputTemp - _gain * _outputTemp;

			// Store
			_inputBuffer[_pos]  = input;
			_outputBuffer[_pos] = _outputTemp;	

			if(_pos == _delay)
				_pos = 0;
			else
				_pos++;

			return(_outputTemp);
		}

		private void Reset()
		{
			for(_pos = 0; _pos <= _delay; _pos++)
			{
				_inputBuffer[_pos]  = 0; 
				_outputBuffer[_pos] = 0; 
			}

			_pos = 0;
		}
	}
}
