﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;
using System.Xml;
using System.Reflection;

namespace MediaAlliance.NewsMontage.RenderEngine.Classes
{
    public enum RenderMode
    { 
        audioVideo  = 0,
        audioOnly   = 1,
        videoOnly   = 2
    }

    public class CRenderSettings
    {
        public int SamplesPerSeconds        = 48000;
        public int BitPerSample             = 16;
        public int MaxMonoTrackPerClip      = 2;
        public string Container             = "MOV";
        public string OutputExtension       = ".mov";
        public string OutputScanOrderInfo   = "BFF";
        public string Codec                 = "DV420";
        public string RenderTempFolder      = ".\\RenderTemp";
        public float FrameRate              = 25.0F;
        public bool VideoIntermediateAvi    = false;
        public RenderMode m_rendermode      = RenderMode.audioVideo;

        public string EncoderProfile        = "";           // Profilo Reg da caricare per impostare l'encoder in base al formato
        public string ContainerProfile      = "";           // Profilo Reg da caricare per impostare l'encoder in base al container AVI, MOV, Etc
        
        public int VideoWidth               = 720;
        public int VideoHeight              = 576;

        private string profileXml           = "";

        public CRenderSettings()
        {
            UpdateValue();
        }

        public string GetProfileInfo(string profilename, string paramname)
        {
            string result = "";

            if (profilename.Trim() != "")
            {
                FileInfo profile_xml = new FileInfo(".\\Profiles\\" + profilename + ".xml");

                if (profile_xml.Exists)
                {

                    Program.Logger.info(" Profile found : " + profilename);

                    try
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(profile_xml.FullName);

                        if (doc["RenderProfile"] != null)
                        {
                            XmlNode MAIN = doc["RenderProfile"];

                            if (MAIN[paramname] != null)
                            {
                                result = MAIN[paramname].InnerText;
                            }
                        }
                    }
                    catch
                    {
                    }
                }
            }

            return result;
        }

        private void UpdateValue()
        {
            // PRIMA LEGGO I VALORI IMPOSTATI NEL APP.CONFIG PER I VALORI DI DEFAULT

            SamplesPerSeconds       = Convert.ToInt32(ConfigurationManager.AppSettings["SamplesPerSeconds"].ToString());
            BitPerSample            = Convert.ToInt32(ConfigurationManager.AppSettings["BitPerSample"].ToString());
            Container               = ConfigurationManager.AppSettings["Container"].ToString();
            OutputExtension         = ConfigurationManager.AppSettings["OutputExtension"].ToString();
            Codec                   = ConfigurationManager.AppSettings["Codec"].ToString();
            RenderTempFolder        = ConfigurationManager.AppSettings["RenderTemp"].ToString();

            if (!Path.IsPathRooted(RenderTempFolder))
            {
                RenderTempFolder = new DirectoryInfo(RenderTempFolder).FullName;
            }

            if (!Directory.Exists(RenderTempFolder))
            {
                Directory.CreateDirectory(RenderTempFolder);
            }

            VideoWidth              = Convert.ToInt32(ConfigurationManager.AppSettings["VideoWidth"].ToString());
            VideoHeight             = Convert.ToInt32(ConfigurationManager.AppSettings["VideoHeight"].ToString());

            string sframeRate       = ConfigurationManager.AppSettings["FrameRate"].ToString();
            sframeRate              = sframeRate.Replace(",", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator);
            sframeRate              = sframeRate.Replace(".", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator);
            
            FrameRate               = Convert.ToSingle(sframeRate);

            // SE E' STATO IMPOSTATO UN FILE DI CONFIGURAZIONE ESISTENTE RILEGGO I VALORI DA LI

            if (profileXml.Trim() != "")
            {
                FileInfo profile_xml = new FileInfo(".\\Profiles\\" + profileXml + ".xml");

                if (profile_xml.Exists)
                {

                    Program.Logger.info(" Profile found : " + profile_xml);

                    try
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(profile_xml.FullName);

                        if (doc["RenderProfile"] != null)
                        {
                            XmlNode MAIN = doc["RenderProfile"];

                            if (MAIN["SamplesPerSeconds"] != null)
                            {
                                int valore = 48000;
                                int.TryParse(MAIN["SamplesPerSeconds"].InnerText, out valore);
                                this.SamplesPerSeconds = valore;
                            }

                            if (MAIN["BitPerSample"] != null)
                            {
                                int valore = 16;
                                int.TryParse(MAIN["BitPerSample"].InnerText, out valore);
                                this.BitPerSample = valore;
                            }

                            if (MAIN["MaxMonoTrackPerClip"] != null)
                            {
                                int valore = 16;
                                int.TryParse(MAIN["MaxMonoTrackPerClip"].InnerText, out valore);
                                this.MaxMonoTrackPerClip = valore;
                            }

                            if (MAIN["Container"] != null)
                            {
                                this.Container = MAIN["Container"].InnerText;
                            }

                            if (MAIN["Codec"] != null)
                            {
                                this.Codec = MAIN["Codec"].InnerText;
                            }

                            if (MAIN["FrameRate"] != null)
                            {
                                float valore = 25.0F;

                                string val_str = MAIN["FrameRate"].InnerText;
                                val_str = val_str.Replace(",", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator);
                                val_str = val_str.Replace(".", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator);

                                float.TryParse(val_str, out valore);
                                this.FrameRate = valore;
                            }

                            if (MAIN["VideoWidth"] != null)
                            {
                                int valore = 720;
                                int.TryParse(MAIN["VideoWidth"].InnerText, out valore);
                                this.VideoWidth = valore;
                            }

                            if (MAIN["VideoHeight"] != null)
                            {
                                int valore = 576;
                                int.TryParse(MAIN["VideoHeight"].InnerText, out valore);
                                this.VideoHeight = valore;
                            }

                            if (MAIN["OutputExtension"] != null)
                            {
                                this.OutputExtension = MAIN["OutputExtension"].InnerText;
                            }

                            if (MAIN["OutputScanOrderInfo"] != null)
                            {
                                this.OutputScanOrderInfo = MAIN["OutputScanOrderInfo"].InnerText;
                            }

                            if (MAIN["EncoderProfile"] != null)
                            {
                                this.EncoderProfile = MAIN["EncoderProfile"].InnerText;
                            }

                            if (MAIN["ContainerProfile"] != null)
                            {
                                this.ContainerProfile = MAIN["ContainerProfile"].InnerText;
                            }

                            if (MAIN["VideoIntermediateAvi"] != null)
                            {
                                bool valore = false;
                                bool.TryParse(MAIN["VideoIntermediateAvi"].InnerText, out valore);

                                this.VideoIntermediateAvi = valore;
                            }

                            if (MAIN["RenderMode"] != null)
                            {
                                RenderMode valore = RenderMode.audioVideo;
                                Enum.TryParse(MAIN["RenderMode"].InnerText, out valore);

                                m_rendermode = valore;
                            }
 
                            //FieldInfo[] fInfos = this.GetType().GetFields();

                            //foreach (FieldInfo fInfo in fInfos)
                            //{
                            //    if (MAIN[fInfo.Name] != null)
                            //    {
                            //        string val_str = MAIN[fInfo.Name].InnerText;

                            //        Program.Logger.debug(" READING FROM PROFILE : " + fInfo.Name + " - " + val_str);

                            //        try
                            //        {
                            //            fInfo.SetValue(this, Convert.ChangeType(val_str, fInfo.FieldType));
                            //        }
                            //        catch (System.Exception ex)
                            //        {
                            //        }
                            //    }
                            //}
                        }
                    }
                    catch (Exception ex)
                    {
                        Program.Logger.error(" ERROR LOADING PROFILE : " + ex.Message);
                    }
                }
                else
                {
                    Program.Logger.error(" Profile not found : " + profile_xml);
                }
            }
        }

        public string ProfileXml
        {
            get { return profileXml; }
            set
            {
                if (profileXml != value)
                {
                    profileXml = value;
                    UpdateValue();
                }
            }
        }
    }
}
