﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace MediaAlliance.NewsMontage.RenderEngine
{
    public class CWriter
    {
        public static void WriteLocalInfo(string message)
        {
            Console.WriteLine(message);
        }

        public static void WriteProgress(int percent)
        {
            Console.WriteLine("[%] " + percent);
        }

        public static void WriteError(string message)
        {
            Console.WriteLine("[E] " + message);
        }

        public static void WriteReply(string message)
        {
            Console.WriteLine("[R] " + message);
        }

        public static void WriteInfo(string message)
        {
            Console.WriteLine("[I] " + message);
        }

        public static void WriteEndProcess(string message)
        {
            Console.WriteLine("[F] " + message);
        }

        public static void WriteHelp()
        {
            Console.WriteLine("");
            Console.WriteLine(" Help Commands");
            Console.WriteLine(" -----------------------------------------------");
            Console.WriteLine("");
            Console.WriteLine(" [-h] || [/h]                : Help");
            Console.WriteLine("");
            Console.WriteLine(" [-profiles] || [/profiles]  : Get profiles list");
            Console.WriteLine("");
            Console.WriteLine(" [-pinfo] || [/pinfo]        : Get profiles information");
            Console.WriteLine("");
            Console.WriteLine(" Render syntax options :");
            Console.WriteLine(" -p ProjectFileName -o outputfilename -i profilename [-t] 1,x,y");
            Console.WriteLine("");
            Console.WriteLine(" -p or /p    : project file name");
            Console.WriteLine(" -o or /o    : output file name");
            Console.WriteLine(" -i or /i    : profile name, to check profiles list use /profiles option");
            Console.WriteLine(" -t ot /t    : [optional] give the list of enabled track, others will be hidden");
            Console.WriteLine("");
            Console.WriteLine(" Render examples : ");
            Console.WriteLine("   NmRenderEngine /pinfo paramname profilename");
            Console.WriteLine("      >   NmRenderEngine /pinfo OutputScanOrderInfo MOV_NTSC_420");
            Console.WriteLine("");
            Console.WriteLine("   NmRenderEngine /p [project filename] /o [output filename]");
            Console.WriteLine("   NmRenderEngine /p [project filename] /o [output filename] /i [profile name]");
            Console.WriteLine("   NmRenderEngine /p [project filename] /o [output filename] /i [profile name] /t 1,2,3");
            Console.WriteLine("");
            Console.WriteLine("");
        }

        public static void WriteProfileList()
        {
            string folder = Path.Combine(new FileInfo(Program.MyExeFilename).Directory.FullName, "Profiles");

            Program.Logger.info(" Looking for profiles on folder : " + folder);
            if (Directory.Exists(folder))
            {
                foreach (string file in Directory.GetFiles(folder, "*.xml"))
                {
                    WriteReply(Path.GetFileName(file).Replace(Path.GetExtension(file), "").ToUpper());
                }
            }
            else
            {
                Program.Logger.error(" Profiles folder not found!");
            }
        }
    }
}
