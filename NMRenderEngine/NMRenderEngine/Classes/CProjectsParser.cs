﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

using System.IO;
using System.Xml;



namespace MediaAlliance.NewsMontage.RenderEngine.ProjectParser
{
    public class CProjectsParser
    {
        // DELEGATES
        public delegate void Prj_OnStartParsing(object Sender);
        public delegate void Prj_OnParsed(object Sender);
        public delegate void Prj_OnProgress(object Sender, string step_description, int perc);

        // EVENTS
        public event Prj_OnStartParsing OnStartParsing      = null;
        public event Prj_OnProgress OnProgress              = null;
        public event Prj_OnParsed OnParsed                  = null;

        // CLASSES 
        public CTracksCollection Tracks             = new CTracksCollection();
        public CClipInfo_Collection VoiceTrack      = new CClipInfo_Collection();
        public CClipInfo_Collection VideoTrack_A    = new CClipInfo_Collection();
        public CClipInfo_Collection VideoTrack_B    = new CClipInfo_Collection();
        public CClipInfo_Collection BackgroundTrack = new CClipInfo_Collection();
        

        // FIELDS
        private string projectPath                  = "";
        private bool isLoading                      = false;
        private float fps                           = 25.0f;



        public CProjectsParser(string ProjectPath, float fps)
        {
            if (File.Exists(ProjectPath))
            {
                this.projectPath = ProjectPath;
                this.fps = fps;

                try
                {
                    Parse();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                throw new FileNotFoundException();
            }
        }



        private void Parse()
        {
            isLoading = true;

            Program.Logger.info(" Project parse started : " + this.projectPath);

            // MESSAGGIO INIZIO ATTIVITA'
            if (OnStartParsing != null) OnStartParsing(this);

            XmlDocument doc = new XmlDocument();
            doc.Load(this.projectPath);

            Program.Logger.debug("     > Project xml loaded");

            if (doc["project"] != null)
            {
                if (OnProgress != null) OnProgress(this, "", 0);


                #region PARSE XML

                XmlNode MAIN                = doc["project"];
                XmlNode MEDIA_NODE          = (MAIN["media"] != null) ? MAIN["media"] : null;
                XmlNodeList CLIP_SRC_NODES  = doc.GetElementsByTagName("clip");
                XmlNodeList CLIP_NODES      = doc.GetElementsByTagName("videocliptl");
                XmlNodeList TRACK_INFO      = doc.GetElementsByTagName("tracks_info");


                Program.Logger.debug("     > Main nodes indexed");

                if (MEDIA_NODE != null)
                {
                    int percent = 0;
                    int current = 0;

                    foreach (XmlNode clip_node in CLIP_NODES)
                    {
                        try
                        {
                            percent = current * 100 / CLIP_NODES.Count;
                            if (OnProgress != null) OnProgress(this, "", percent);

                            if (clip_node.NodeType == XmlNodeType.Element)
                            {
                                CClipInfo clip_info = new CClipInfo();


                                #region PARSE DISABLED TRACKS

                                if (TRACK_INFO.Count > 0)
                                {
                                    foreach (XmlNode tInfoNode in TRACK_INFO[0])
                                    {
                                        if (tInfoNode.NodeType == XmlNodeType.Element &&
                                            tInfoNode.Name.Equals("track", StringComparison.OrdinalIgnoreCase))
                                        {
                                            int track_id = -1;
                                            bool track_disabled = false;
                                            string title = "";

                                            if (tInfoNode.Attributes["id"] != null)
                                            {
                                                int.TryParse(tInfoNode.Attributes["id"].Value, out track_id);
                                            }

                                            if (tInfoNode.Attributes["title"] != null)
                                            {
                                                title = tInfoNode.Attributes["title"].Value;
                                            }

                                            if (tInfoNode.Attributes["off"] != null)
                                            {
                                                bool.TryParse(tInfoNode.Attributes["off"].Value, out track_disabled);
                                            }

                                            Tracks.Add(new CTrackInfo(title, track_id, track_disabled));

                                            switch (track_id)
                                            {
                                                case 0:
                                                    VoiceTrack.AudioDisabled = track_disabled;
                                                    break;
                                                case 1:
                                                    VideoTrack_A.VideoDisabled = track_disabled;
                                                    break;
                                                case 2:
                                                    VideoTrack_A.AudioDisabled = track_disabled;
                                                    break;
                                                case 3:
                                                    VideoTrack_B.VideoDisabled = track_disabled;
                                                    break;
                                                case 4:
                                                    VideoTrack_B.AudioDisabled = track_disabled;
                                                    break;
                                                case 5:
                                                    BackgroundTrack.AudioDisabled = track_disabled;
                                                    break;
                                            }
                                        }
                                    }
                                }

                                #endregion PARSE DISABLED TRACKS


                                clip_info.id = (clip_node.Attributes["id"] != null) ? Guid.Parse(clip_node.Attributes["id"].Value) : Guid.Empty;


                                #region PARSE CLIP POSITION

                                if (clip_node.Attributes["startTimeline"] != null)
                                {
                                    clip_info.Framein = 0;
                                    long.TryParse(clip_node.Attributes["startTimeline"].Value, out clip_info.Framein);
                                    clip_info.FrameInMs = (long)(clip_info.Framein * (1000 / fps));
                                }

                                if (clip_node.Attributes["In"] != null)
                                {
                                    clip_info.ClippingIn = 0;
                                    long.TryParse(clip_node.Attributes["In"].Value, out clip_info.ClippingIn);
                                    clip_info.ClippingInMs = clip_info.ClippingIn * (1000.0f / fps);
                                }

                                if (clip_node.Attributes["Out"] != null)
                                {
                                    clip_info.ClippingOut = 0;
                                    long.TryParse(clip_node.Attributes["Out"].Value, out clip_info.ClippingOut);
                                    clip_info.ClippingOutMs = clip_info.ClippingOut * (1000.0f / fps);
                                }

                                clip_info.ClipDuration = clip_info.ClippingOut - clip_info.ClippingIn;
                                clip_info.ClipDurationMs = (clip_info.ClippingOut - clip_info.ClippingIn) * (1000.0f / fps);

                                #endregion PARSE CLIP POSITION

                                #region PARSE SOURCE FILEINFO

                                clip_info.Source_id = 0;

                                if (clip_node.Attributes["sourceclipid"] != null)
                                {
                                    clip_info.Source_id = 0;
                                    int.TryParse(clip_node.Attributes["sourceclipid"].Value, out clip_info.Source_id);

                                    if (clip_info.Source_id > 0)
                                    {
                                        foreach (XmlNode source_node in CLIP_SRC_NODES)
                                        {
                                            if (source_node.NodeType == XmlNodeType.Element)
                                            {
                                                int src_id = 0;

                                                if (source_node.Attributes["clipid"] != null)
                                                {
                                                    int.TryParse(source_node.Attributes["clipid"].Value, out src_id);
                                                }

                                                if (src_id == clip_info.Source_id)
                                                {
                                                    #region PARSE CLIPSOURCE INFO

                                                    if (source_node.Attributes["assetid"] != null)
                                                    {
                                                        clip_info.Source_AssetId = source_node.Attributes["assetid"].Value;
                                                    }

                                                    if (source_node.Attributes["title"] != null)
                                                    {
                                                        clip_info.Source_Title = source_node.Attributes["title"].Value;
                                                    }

                                                    if (source_node.Attributes["file"] != null)
                                                    {
                                                        clip_info.Source_Filename = source_node.Attributes["file"].Value;
                                                    }

                                                    if (source_node.Attributes["description"] != null)
                                                    {
                                                        clip_info.Source_Description = source_node.Attributes["description"].Value;
                                                    }

                                                    if (source_node.Attributes["full"] != null)
                                                    {
                                                        clip_info.Source_IsFullclip = true;
                                                        bool.TryParse(source_node.Attributes["full"].Value, out clip_info.Source_IsFullclip);
                                                    }

                                                    if (source_node.Attributes["totaldur"] != null)
                                                    {
                                                        clip_info.Source_Duration = 0;
                                                        long.TryParse(source_node.Attributes["totaldur"].Value, out clip_info.Source_Duration);
                                                    }

                                                    if (source_node.Attributes["used"] != null)
                                                    {
                                                        clip_info.Source_Used = 0;
                                                        int.TryParse(source_node.Attributes["used"].Value, out clip_info.Source_Used);
                                                    }

                                                    #endregion PARSE CLIPSOURCE INFO
                                                }
                                            }
                                        }
                                    }
                                }

                                #endregion PARSE SOURCE FILEINFO

                                #region PARSE AUDIONODES

                                if (clip_node["volumes"] != null)
                                {
                                    if (clip_node["volumes"].Attributes["global"] != null)
                                    {
                                        // LEGGO IL VALORE GLOBALE DELL'AUDIO

                                        string gValueStr = clip_node["volumes"].Attributes["global"].Value;

                                        gValueStr = gValueStr.Replace(",", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator);
                                        gValueStr = gValueStr.Replace(".", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator);

                                        float val = 1F;

                                        float.TryParse(gValueStr, out val);
                                        clip_info.AudioGlobal = val;


                                        // LEGGO I SINGOLI NODI

                                        foreach (XmlNode sub_node in clip_node["volumes"].ChildNodes)
                                        {
                                            if (sub_node.NodeType == XmlNodeType.Element &&
                                                sub_node.Name.Equals("volumepoints", StringComparison.OrdinalIgnoreCase))
                                            {
                                                long node_tc = -1;
                                                float node_val = -1;

                                                if (sub_node.Attributes["time"] != null)
                                                {
                                                    long.TryParse(sub_node.Attributes["time"].Value, out node_tc);
                                                }

                                                if (sub_node.Attributes["volume"] != null)
                                                {
                                                    string val_str = sub_node.Attributes["volume"].Value;

                                                    val_str = val_str.Replace(",", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator);
                                                    val_str = val_str.Replace(".", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator);

                                                    float.TryParse(val_str, out node_val);
                                                }

                                                if (node_tc >= 0 && node_val >= 0)
                                                {
                                                    node_tc -= clip_info.ClippingIn;

                                                    clip_info.AudioNodes.Add(new CAudioNode(node_tc, node_val));
                                                }
                                            }
                                        }
                                    }
                                }

                                #endregion PARSE AUDIONODES

                                #region PARSE TYPE

                                if (clip_node.ParentNode.NodeType == XmlNodeType.Element)
                                {
                                    if (clip_node.ParentNode.Name.Equals("voice", StringComparison.OrdinalIgnoreCase))
                                    {
                                        clip_info.type = SourceType.voice;
                                        if (Tracks[0].off) clip_info.AudioGlobal = 0;
                                    }
                                    else if (clip_node.ParentNode.Name.Equals("video_1", StringComparison.OrdinalIgnoreCase))
                                    {
                                        clip_info.type = SourceType.av_a;
                                        if (Tracks[2].off) clip_info.AudioGlobal = 0;
                                    }
                                    else if (clip_node.ParentNode.Name.Equals("video_2", StringComparison.OrdinalIgnoreCase))
                                    {
                                        clip_info.type = SourceType.av_b;

                                        if (Tracks[4].off) clip_info.AudioGlobal = 0;
                                    }
                                    else if (clip_node.ParentNode.Name.Equals("background", StringComparison.OrdinalIgnoreCase))
                                    {
                                        clip_info.type = SourceType.background;
                                        if (Tracks[5].off) clip_info.AudioGlobal = 0;
                                    }
                                }

                                #endregion PARSE TYPE


                                if (clip_info.type != SourceType.unknown &&
                                    clip_info.Source_Filename.Trim() != "" &&
                                    clip_info.Source_Duration > 0 &&
                                    clip_info.ClippingOut > clip_info.ClippingIn)
                                {
                                    if (File.Exists(clip_info.Source_Filename))
                                    {
                                        // SE LA CLIP PARSATA, HA I REQUISITI MINIMI LA AGGIUNGO ALLE LISTE

                                        switch (clip_info.type)
                                        {
                                            case SourceType.voice:
                                                VoiceTrack.Add(clip_info);
                                                clip_info.AudioTrack = Tracks[0];
                                                Program.Logger.debug("     > Clip added as voice source : " + clip_info.Source_Filename);
                                                break;
                                            case SourceType.av_a:
                                                VideoTrack_A.Add(clip_info);
                                                clip_info.VideoTrack = Tracks[1];
                                                clip_info.AudioTrack = Tracks[2];
                                                Program.Logger.debug("     > Clip added as video_a source : " + clip_info.Source_Filename);
                                                break;
                                            case SourceType.av_b:
                                                VideoTrack_B.Add(clip_info);
                                                clip_info.VideoTrack = Tracks[3];
                                                clip_info.AudioTrack = Tracks[4];
                                                Program.Logger.debug("     > Clip added as voice_b source : " + clip_info.Source_Filename);
                                                break;
                                            case SourceType.background:
                                                BackgroundTrack.Add(clip_info);
                                                clip_info.AudioTrack = Tracks[5];
                                                Program.Logger.debug("     > Clip added as background source : " + clip_info.Source_Filename);
                                                break;
                                        }
                                    }
                                    else
                                    {
                                        // GESTIRE ERRORE IN CASO DI FILE SORGENTE NON TROVATO
                                        Program.Logger.error("     > Source clipfile not found : " + clip_info.Source_Filename);
                                    }
                                }
                                else
                                {
                                    // GESTIRE ERRORE IN CASO DI MANCANZA DI INFOMAZIONI
                                    throw new NotImplementedException();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Program.Logger.error("     > Clip node parse error : " + ex.Message);
                            Program.Logger.error("          > on node " + clip_node.InnerXml);                      
                        }

                        current++;
                    }
                }

                Program.Logger.info("     > Parse finished");

                #endregion PARSE XML


                if (OnProgress != null) OnProgress(this, "", 100);
            }


            // MESSAGGIO FINE ATTIVITA'
            if (OnParsed != null) OnParsed(this);


            isLoading = false;
        }

       

        public string ProjectPath
        {
            get { return projectPath; }
            set
            {
                projectPath = value;
            }
        }

        public bool IsLoading
        {
            get { return isLoading; }
            set 
            {
                isLoading = value;
            }
        }

        public List<string> GetAllClips()
        {
            List<string> clips = new List<string>();

            foreach (CClipInfo ci in VoiceTrack)
            {
                if (!clips.Contains(ci.Source_Filename.ToLower())) clips.Add(ci.Source_Filename.ToLower());
            }

            foreach (CClipInfo ci in VideoTrack_A)
            {
                if (!clips.Contains(ci.Source_Filename.ToLower())) clips.Add(ci.Source_Filename.ToLower());
            }

            foreach (CClipInfo ci in VideoTrack_B)
            {
                if (!clips.Contains(ci.Source_Filename.ToLower())) clips.Add(ci.Source_Filename.ToLower());
            }

            foreach (CClipInfo ci in BackgroundTrack)
            {
                if (!clips.Contains(ci.Source_Filename.ToLower())) clips.Add(ci.Source_Filename.ToLower());
            }

            return clips;
        }


        private XmlNode AddSubnode(XmlNode parent, string name, string innertext)
        {
            XmlNode nn = parent.OwnerDocument.CreateNode(XmlNodeType.Element, name, "");

            if (innertext.Trim() != "")
            {
                nn.InnerText = innertext;
            }

            parent.AppendChild(nn);

            return nn;
        }

    }
}
