﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Threading;
using System.ComponentModel;
using System.Configuration;
using audioHelper;
using System.IO;
using System.Diagnostics;
using MAMuxEncoder;
using MediaAlliance.NewsMontage.RenderEngine.ProjectParser;
using Microsoft.Win32;
using DESCombineLib;
using MediaAlliance.NewsMontage.RenderEngine.Classes;
using System.Reflection;



namespace MediaAlliance.NewsMontage.RenderEngine.Render
{
  
    public class CRender : IDisposable
    {
        // DELEGATES
        public delegate void Render_OnStart(object Sender, string step_descrition);
        public delegate void Render_OnProgress(object Sender, string step_descrition, int percent);
        public delegate void Render_OnFinish(object Sender, string step_descrition);
        public delegate void Render_OnError(object Sender, string descrition);

        // EVENTS
        public event Render_OnStart OnStart         = null;
        public event Render_OnProgress OnProgress   = null;
        public event Render_OnFinish OnFinish = null;
        public event Render_OnError OnError = null;

        public ManualResetEvent mre = new ManualResetEvent(false);

        private Thread renderThread                 = null;
        private bool renderThread_Stop              = false;

        private CProjectsParser cProjectsParser     = null;
        private string projectFile                  = "";
        private string profile                      = "";
        private List<int> m_enabledTracks           = new List<int>();

        private string TempFolder                   = "";
        private string outputFileName               = "";
        public MediaAlliance.Logger.ILogger logger  = null;

        private string ExePath = new FileInfo(System.Reflection.Assembly.GetEntryAssembly().CodeBase.Replace("file:///", "")).Directory.FullName;

        private enum EnumSteps
        {
            ExtractAudioRaw = 0,
            CombineAudioTrack,
            Mixer,
            ExtractMonoFromRaw,
            ExtractAudioFromVideos,
            CombineVideo,
            Mux,
            Idle,
            End
        }

        private EnumSteps m_actualStep = EnumSteps.ExtractAudioRaw;

        //Application Settings
        public CRenderSettings RenderSettings = new CRenderSettings(); 
 
        private MARenderer.MARenderer m_maRender = null;


        public CRender()
        { 
        }

        public CRender(String projectFile, string outputFileName, string profile, MediaAlliance.Logger.ILogger logger, List<int> EnabledTracks)
        {
            this.logger = logger;

            logger.info("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
            logger.info(" NEW RENDER STARTING");
            logger.info("");

            logger.info("   > Project file      : " + projectFile);
            logger.info("   > Output filename   : " + outputFileName);
            logger.info("   > Profile xml       : " + profile);
            logger.info("");

            this.projectFile        = projectFile;
            this.outputFileName     = outputFileName;
            m_enabledTracks         = EnabledTracks;

            if (m_enabledTracks == null) m_enabledTracks = new List<int>();
            
            // EVENTUALE PARSE DEL PROFILE PASSATO
            if (profile.Trim() != "")
            {
                string prof_file = Path.Combine(".\\Profiles", profile + ".xml");

                if (File.Exists(prof_file))
                {
                    logger.info(" Loading parameters from \"" + Path.GetFileName(prof_file) + "\"");
                    this.profile                = profile;
                    RenderSettings.ProfileXml   = profile;
                }
                else
                {
                    logger.info(" Profile specified not found on : " + profile);
                    logger.info("    > so use default parameters from app.config");
                }
            }
            else
            {
                logger.info(" No profile specified");
                logger.info("    > so use default parameters from app.config");
            }

            logger.info("");
            logger.info(" RENDER PARAMETERS");
            logger.info("");

            FieldInfo[] fInfos = RenderSettings.GetType().GetFields();

            foreach (FieldInfo fInfo in fInfos)
            {
                string name = " " + fInfo.Name;
                logger.info(name.PadRight(30, ' ') + " : " + fInfo.GetValue(RenderSettings).ToString());
            }

            logger.info("RenderMode : " + RenderSettings.m_rendermode.ToString());
                 

            logger.info("");

            // FOLDER DESTINAZIONE FILE TEMPORANEI
            TempFolder              = Path.Combine(RenderSettings.RenderTempFolder, Path.GetFileName(outputFileName)) + "\\";

            logger.info(" Temp folder is : " + TempFolder);

            logger.info(" Parsing project...");
            try
            {
                this.cProjectsParser = new CProjectsParser(projectFile, RenderSettings.FrameRate);
            }
            catch (Exception ex)
            {
                logger.error(" Project parse error : " + ex.Message);
                CWriter.WriteError("Project parse error !");
                Environment.Exit(1);
            }
            logger.info("");


            logger.info(" Scanning source files on project : ");

            foreach (string file in cProjectsParser.GetAllClips())
            {
                logger.debug("   > About going to check file : " + file);
                try
                {
                    logger.info("   > " + file + " [found : " + File.Exists(file) + "]");
                }
                catch (Exception ex)
                {
                    logger.error(ex);
                }
            }
            logger.info("");
        }


        private void renderThread_Task()
        {
            string message = "";
            
            try
            {
                try
                {
                    Directory.CreateDirectory(TempFolder);
                }
                catch (Exception ex)
                {
                    logger.error(" Error creating temp folder on : " + TempFolder);
                    logger.error("    > Render aborted");
                    Raise_Render_OnError("Error Creating folder :" + TempFolder);
                    return;
                }

                RenderSettings.MaxMonoTrackPerClip = int.Parse(ConfigurationManager.AppSettings["MaxMonoTrackPerClip"].ToString());
            }
            catch
            {
                logger.error(" Error creating temp folder on : " + TempFolder);
                logger.error("    > Render aborted");
                Raise_Render_OnError("Error Creating folder :" + TempFolder);
            }


            logger.debug(" After folder create : " + TempFolder);

            Raise_Render_OnStart("Rendering operation started");

            try
            {

                while (!renderThread_Stop)
                {
                    logger.info(" [RENDER STEP] " + m_actualStep.ToString());

                    switch (m_actualStep)
                    {
                        case EnumSteps.ExtractAudioRaw:

                            // QUI VENGONO PRIMA ESTRATTI TUTTI GLI AUDIO-RAW DAI FILE SORGENTI
                            // POI VENGO ESTRATTI TUTTI I MONO-WAV DAI RAW
                            // IN FINE VENGONO ESEGUITI I FADE DELL'AUDIO SULLE VARIE CLIPS
                            // TUTTE QUESTE OPERAZIONE SONO FATTE TRAMITE IL METODO "ExtratAudioRawFromVideo"

                            Raise_Render_OnProgress("Conforming audio tracks", 5);

                            #region EXTRACTAUDIO

                            if (RenderSettings.m_rendermode == RenderMode.audioVideo || RenderSettings.m_rendermode == RenderMode.audioOnly)
                            {
                                m_actualStep = EnumSteps.Idle;

                                message = "1 of 5  Extract audio stream ...";

                                if (m_enabledTracks.Count == 0 || m_enabledTracks.Contains(0))
                                {
                                    ExtratAudioRawFromVideo(cProjectsParser.VoiceTrack, "TrackVoice_");
                                    Raise_Render_OnProgress("Conforming audio tracks", 10);
                                    logger.info("    > Extracted raw audios from voice track");
                                }
                                else
                                {
                                    logger.info("    > Track 0 (VOICE) -> OFF");
                                }

                                if (m_enabledTracks.Count == 0 || m_enabledTracks.Contains(1))
                                {
                                    ExtratAudioRawFromVideo(cProjectsParser.VideoTrack_A, "TrackA_");
                                    Raise_Render_OnProgress("Conforming audio tracks", 15);
                                    logger.info("    > Extracted raw audios from video track A");
                                }
                                else
                                {
                                    logger.info("    > Track 1 (A/V 1) -> OFF");
                                }

                                if (m_enabledTracks.Count == 0 || m_enabledTracks.Contains(2))
                                {
                                    ExtratAudioRawFromVideo(cProjectsParser.VideoTrack_B, "TrackB_");
                                    Raise_Render_OnProgress("Conforming audio tracks", 20);
                                    logger.info("    > Extracted raw audios from video track B");
                                }
                                else
                                {
                                    logger.info("    > Track 2 (A/V 2) -> OFF");
                                }


                                if (m_enabledTracks.Count == 0 || m_enabledTracks.Contains(3))
                                {
                                    ExtratAudioRawFromVideo(cProjectsParser.BackgroundTrack, "TrackBG_");
                                    Raise_Render_OnProgress("Conforming audio tracks", 25);
                                    logger.info("    > Extracted raw audios from background track");
                                }
                                else
                                {
                                    logger.info("    > Track 3 (BACKGROUND) -> OFF");
                                }
                            }

                            #endregion EXTRACTAUDIO

                            m_actualStep = EnumSteps.CombineAudioTrack;
                            break;

                        case EnumSteps.CombineAudioTrack:

                            // QUI VENGONO CREATE TUTTE LE TRACCE MONO VUOTE UTILI E SUCCESSIVAMENTE
                            // DELLA DURATA MASSIMA (QUELLA DELLA TRACCIA PIU' LUNGA)
                            // VENGONO INSERITE LE VARIE CLIP AUDIO COMPONENDO UN PREMONTATO PER OGNI 
                            // TRACCIA, LA FUNZIONE CHE CREA QUESTA LAVORAZIONE E' "CombineAudioTrack"

                            Raise_Render_OnProgress("Combining audio tracks", 30);

                            if (RenderSettings.m_rendermode == RenderMode.audioVideo || RenderSettings.m_rendermode == RenderMode.audioOnly)
                            {
                                #region COMBINE AUDIO TRACKS
                                {
                                    long maxTrackDuration = 0;

                                    int track_id = 0;
                                    int clip_count = 0;

                                    #region FIND MAX DURATION

                                    if (m_enabledTracks.Count == 0 || m_enabledTracks.Contains(0))
                                    {
                                        if (maxTrackDuration < cProjectsParser.VoiceTrack.WorkingArea_Duration)
                                        {
                                            maxTrackDuration = cProjectsParser.VoiceTrack.WorkingArea_Duration;
                                            clip_count = cProjectsParser.VoiceTrack.Count;
                                            track_id = 0;
                                        }
                                    }

                                    if (m_enabledTracks.Count == 0 || m_enabledTracks.Contains(1))
                                    {
                                        if (maxTrackDuration < cProjectsParser.VideoTrack_A.WorkingArea_Duration)
                                        {
                                            maxTrackDuration = cProjectsParser.VideoTrack_A.WorkingArea_Duration;
                                            clip_count = cProjectsParser.VideoTrack_A.Count;
                                            track_id = 1;
                                        }
                                    }

                                    if (m_enabledTracks.Count == 0 || m_enabledTracks.Contains(2))
                                    {
                                        if (maxTrackDuration < cProjectsParser.VideoTrack_B.WorkingArea_Duration)
                                        {
                                            maxTrackDuration = cProjectsParser.VideoTrack_B.WorkingArea_Duration;
                                            clip_count = cProjectsParser.VideoTrack_B.Count;
                                            track_id = 2;
                                        }
                                    }

                                    if (m_enabledTracks.Count == 0 || m_enabledTracks.Contains(3))
                                    {
                                        if (maxTrackDuration < cProjectsParser.BackgroundTrack.WorkingArea_Duration)
                                        {
                                            maxTrackDuration = cProjectsParser.BackgroundTrack.WorkingArea_Duration;
                                            clip_count = cProjectsParser.BackgroundTrack.Count;
                                            track_id = 3;
                                        }
                                    }

                                    #endregion FIND MAX DURATION

                                    logger.info("    > Max duration found : " + maxTrackDuration);
                                    logger.info("    > Longest track      : " + track_id);
                                    logger.info("    > Clips on track     : " + clip_count);

                                    if (m_enabledTracks.Count == 0 || m_enabledTracks.Contains(0))
                                    {
                                        CombineAudioTrack(cProjectsParser.VoiceTrack, "TrackVoice_",
                                                            (long)(maxTrackDuration * (1000 / RenderSettings.FrameRate)));
                                        Raise_Render_OnProgress("Combining audio tracks", 35);
                                        logger.info("    > Combined audios of voice track");
                                    }
                                    else
                                    {
                                        logger.info("    > Track 0 (VOICE) -> OFF");
                                    }

                                    if (m_enabledTracks.Count == 0 || m_enabledTracks.Contains(1))
                                    {
                                        CombineAudioTrack(cProjectsParser.VideoTrack_A, "TrackA_",
                                                            (long)(maxTrackDuration * (1000 / RenderSettings.FrameRate)));
                                        Raise_Render_OnProgress("Combining audio tracks", 40);
                                        logger.info("    > Combined audios of video track A");
                                    }
                                    else
                                    {
                                        logger.info("    > Track 1 (A/V 1) -> OFF");
                                    }


                                    if (m_enabledTracks.Count == 0 || m_enabledTracks.Contains(2))
                                    {
                                        CombineAudioTrack(cProjectsParser.VideoTrack_B, "TrackB_",
                                                            (long)(maxTrackDuration * (1000 / RenderSettings.FrameRate)));
                                        Raise_Render_OnProgress("Combining audio tracks", 45);
                                        logger.info("    > Combined audios of video track B");
                                    }
                                    else
                                    {
                                        logger.info("    > Track 2 (A/V 2) -> OFF");
                                    }

                                    if (m_enabledTracks.Count == 0 || m_enabledTracks.Contains(3))
                                    {
                                        CombineAudioTrack(cProjectsParser.BackgroundTrack, "TrackBG_",
                                                            (long)(maxTrackDuration * (1000 / RenderSettings.FrameRate)));
                                        Raise_Render_OnProgress("Combining audio tracks", 50);
                                        logger.info("    > Combined audios of background");
                                    }
                                    else
                                    {
                                        logger.info("    > Track 3 (BACKGROUND) -> OFF");
                                    }

                                }

                                #endregion COMBINE AUDIO TRACKS
                            }

                            m_actualStep = EnumSteps.Mixer;
                            break;

                        case EnumSteps.Mixer:

                            // QUI VENGONO MIXATI ASSIEME TUTTI I FILE MONO DI OGNI SINGOLO TRACCIA IN
                            // UN UNICO FILE, QUINDI POTREBBERO RISULTARE DA 0 A "MaxMonoTrackPerClip"
                            // FILES PER OGNUNA DELLE TRACCE DA RENDERIZZARE

                            Raise_Render_OnProgress("Mixing audios", 55);

                            if (RenderSettings.m_rendermode == RenderMode.audioVideo || RenderSettings.m_rendermode == RenderMode.audioOnly)
                            {
                                #region MIXER

                                for (int idtrack = 1; idtrack <= RenderSettings.MaxMonoTrackPerClip; idtrack++)
                                {
                                    string fileWaveMixed = TempFolder + "AudioChannel_mixed_" + idtrack + ".wav";

                                    if (File.Exists(fileWaveMixed))
                                        File.Delete(fileWaveMixed);

                                    clsMixerStereo mixerStereo = new clsMixerStereo(fileWaveMixed);

                                    string fileWaveVoice = TempFolder + "TrackVoice_combined_" + idtrack + ".wav";
                                    Raise_Render_OnProgress("Mixing audios", 60);

                                    string fileWaveVideoA = TempFolder + "TrackA_combined_" + idtrack + ".wav";
                                    Raise_Render_OnProgress("Mixing audios", 65);

                                    string fileWaveVideoB = TempFolder + "TrackB_combined_" + idtrack + ".wav";
                                    Raise_Render_OnProgress("Mixing audios", 70);

                                    string fileWaveBG = TempFolder + "TrackBG_combined_" + idtrack + ".wav";
                                    Raise_Render_OnProgress("Mixing audios", 75);

                                    if (m_enabledTracks.Count == 0 || m_enabledTracks.Contains(0))
                                    {
                                        if(File.Exists(fileWaveVoice)) mixerStereo.addInFile(fileWaveVoice);
                                    }
                                    if (m_enabledTracks.Count == 0 || m_enabledTracks.Contains(1))
                                    {
                                        if (File.Exists(fileWaveVideoA)) mixerStereo.addInFile(fileWaveVideoA);
                                    }
                                    if (m_enabledTracks.Count == 0 || m_enabledTracks.Contains(2))
                                    {
                                        if (File.Exists(fileWaveVideoB)) mixerStereo.addInFile(fileWaveVideoB);
                                    }
                                    if (m_enabledTracks.Count == 0 || m_enabledTracks.Contains(3))
                                    {
                                        if (File.Exists(fileWaveBG)) mixerStereo.addInFile(fileWaveBG);
                                    }

                                    mixerStereo.DoProcess(4);

                                    // REMOVE TEMP FILES
                                    if (File.Exists(fileWaveVoice)) File.Delete(fileWaveVoice);
                                    if (File.Exists(fileWaveVideoA)) File.Delete(fileWaveVideoA);
                                    if (File.Exists(fileWaveVideoB)) File.Delete(fileWaveVideoB);
                                    if (File.Exists(fileWaveBG)) File.Delete(fileWaveBG);

                                    logger.info("    > Track " + idtrack + " audio mixed");
                                }

                                #endregion MIXER
                            }

                            m_actualStep = EnumSteps.CombineVideo;
                            break;

                        case EnumSteps.CombineVideo:

                            // QUI VIENE RENDERIZZATA LA PARTE VIDEO TRAMITE DES-COMBINE

                            if (RenderSettings.m_rendermode == RenderMode.audioVideo || RenderSettings.m_rendermode == RenderMode.videoOnly)
                            {

                                Raise_Render_OnProgress("Generating video track", 80);

                                #region Combine Video

                                try
                                {
                                    logger.info("    > Setting-up Registry...");

                                    //Setup registry for MXF selected Encoder.

                                    if (RenderSettings.EncoderProfile.Trim() != "")
                                    {
                                        // SE E' IMPOSTATO NEL PROFILE IL VALORE INSERITO NELL'ULTIMA VERSIONE CONSIDERO QUELLO
                                        // ALTRIMENTI... GIRO CLASSICO

                                        if (!ExePath.EndsWith("\\")) ExePath += "\\";
                                        if (RenderSettings.EncoderProfile.StartsWith("\\")) RenderSettings.EncoderProfile = RenderSettings.EncoderProfile.Substring(1);

                                        string fileReg = ExePath + RenderSettings.EncoderProfile;

                                        Program.Logger.info(" Profile requiring to load EncoderProfile : " + RenderSettings.EncoderProfile);


                                        if (File.Exists(fileReg))
                                        {
                                            Program.Logger.info("    > Profile importing");
                                            CMARegistryTools.ManualSetupReg(fileReg);
                                        }
                                        else
                                        {
                                            Program.Logger.error("    > Profile not found on : " + fileReg);
                                        }
                                    }
                                    else
                                    {
                                        #region GIRO CLASSICO SWITCH

                                        if (RenderSettings.Codec.Equals(MAMuxEncoder.Codec.XDCAM_HD_1920))
                                        {
                                            MuxEncoder.SetupRegistry(MAMuxEncoder.Codec.XDCAM_HD_1920, RenderSettings.FrameRate);
                                        }
                                        if (RenderSettings.Codec.Equals(MAMuxEncoder.Codec.IMX30))
                                        {
                                            MuxEncoder.SetupRegistry(MAMuxEncoder.Codec.IMX30, RenderSettings.FrameRate);
                                        }
                                        if (RenderSettings.Codec.Equals(MAMuxEncoder.Codec.IMX50))
                                        {
                                            MuxEncoder.SetupRegistry(MAMuxEncoder.Codec.IMX50, RenderSettings.FrameRate);
                                        }
                                        if (RenderSettings.Codec.Equals(MAMuxEncoder.Codec.DVCAM420))
                                        {
                                            MuxEncoder.SetupRegistry(MAMuxEncoder.Codec.DVCAM420, RenderSettings.FrameRate);
                                        }
                                        else
                                        {
                                            MuxEncoder.SetupRegistry(MAMuxEncoder.Codec.DEFAULT, RenderSettings.FrameRate);
                                        }

                                        #endregion GIRO CLASSICO SWITCH
                                    }

                                    #region Make registry info for ffdshow encoder

                                    if (RenderSettings.ContainerProfile.Trim() != "")
                                    {
                                        // SE E' IMPOSTATO NEL PROFILE IL VALORE INSERITO NELL'ULTIMA VERSIONE CONSIDERO QUELLO
                                        // ALTRIMENTI... GIRO CLASSICO

                                        if (!ExePath.EndsWith("\\")) ExePath += "\\";
                                        if (RenderSettings.ContainerProfile.StartsWith("\\")) RenderSettings.ContainerProfile = RenderSettings.ContainerProfile.Substring(1);

                                        string fileReg = Path.Combine(ExePath, RenderSettings.ContainerProfile);

                                        Program.Logger.info(" Profile requiring to load EncoderProfile : " + RenderSettings.ContainerProfile);

                                        if (File.Exists(fileReg))
                                        {
                                            Program.Logger.info("    > Profile importing");
                                            CMARegistryTools.ManualSetupReg(fileReg);
                                        }
                                        else
                                        {
                                            Program.Logger.error("    > Profile not found on : " + fileReg);
                                        }
                                    }
                                    else
                                    {
                                        #region GIRO CLASSICO SWITCH

                                        switch (RenderSettings.Container)
                                        {
                                            case MAMuxEncoder.Container.MPG:
                                                {
                                                    RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Software\GNU\ffdshow_enc", true);
                                                    key.SetValue("interlacing", 0x00000001);
                                                    key.SetValue("interlacing_tff", 0x00000001);
                                                    key.SetValue("lastPage", 0x0000010d);
                                                    key.SetValue("codecId", 0x00000007);
                                                    key.SetValue("dv_profile", 0x00000004);
                                                    key.SetValue("fourcc", 0x3247504d);
                                                    key.SetValue("flip", 0x00000000);
                                                    key.SetValue("isProc", 0x00000000);

                                                    //Image processing
                                                    key = Registry.CurrentUser.OpenSubKey(@"Software\GNU\ffdshow\ffdshowenc", true);
                                                    key.SetValue("cropTop", 0x00000000);
                                                    key.SetValue("isCropNzoom", 0x00000000);
                                                    key.SetValue("isZoom", 0x00000000);


                                                    break;
                                                }

                                            case MAMuxEncoder.Container.AVI:
                                                {
                                                    ////DVCAM420
                                                    RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Software\GNU\ffdshow_enc", true);
                                                    key.SetValue("interlacing", 0x00000000);
                                                    key.SetValue("interlacing_tff", 0x00000000);
                                                    key.SetValue("codecId", 0x00000019);
                                                    key.SetValue("lastPage", 0x000000c8);
                                                    key.SetValue("dv_profile", 0x00000002);
                                                    key.SetValue("fourcc", 0x64737664);
                                                    key.SetValue("flip", 0x00000000);
                                                    key.SetValue("isProc", 0x00000000);

                                                    //Image processing
                                                    key = Registry.CurrentUser.OpenSubKey(@"Software\GNU\ffdshow\ffdshowenc", true);
                                                    key.SetValue("cropTop", 0x00000000);
                                                    key.SetValue("isCropNzoom", 0x00000000);
                                                    key.SetValue("isZoom", 0x00000000);


                                                    break;
                                                }


                                            case MAMuxEncoder.Container.MOV:
                                                {
                                                    if (RenderSettings.Codec.Equals(MAMuxEncoder.Codec.DVCAM420))
                                                    {
                                                        RegistryKey key = null;
                                                        try
                                                        {
                                                            key = Registry.CurrentUser.OpenSubKey(@"Software\GNU\ffdshow_enc", true);
                                                        }
                                                        catch
                                                        {
                                                            key = Registry.CurrentUser.CreateSubKey(@"Software\GNU\ffdshow_enc");
                                                        }
                                                        key.SetValue("interlacing", 0x00000000);
                                                        key.SetValue("interlacing_tff", 0x00000000);
                                                        key.SetValue("codecId", 0x00000019);
                                                        key.SetValue("lastPage", 0x000000c8);
                                                        key.SetValue("dv_profile", 0x00000001);
                                                        key.SetValue("fourcc", 0x64737664);
                                                        key.SetValue("flip", 0x00000000);
                                                        key.SetValue("isProc", 0x00000000);

                                                        //Image processing
                                                        try
                                                        {
                                                            key = Registry.CurrentUser.OpenSubKey(@"Software\GNU\ffdshow\ffdshowenc", true);
                                                        }
                                                        catch
                                                        {
                                                            key = Registry.CurrentUser.CreateSubKey(@"Software\GNU\ffdshow\ffdshowenc");
                                                        }
                                                        key.SetValue("cropTop", 0x00000000);
                                                        key.SetValue("isCropNzoom", 0x00000000);
                                                        key.SetValue("isZoom", 0x00000000);



                                                        //CMARegistryTools.ManualSetupReg(Path.Combine(ExePath, "Registry\\FFDShow_Container_MOV.reg"));

                                                    }

                                                    if (RenderSettings.Codec.Equals(MAMuxEncoder.Codec.IMX30))
                                                    {
                                                        RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Software\GNU\ffdshow_enc", true);
                                                        key.SetValue("interlacing", 0x00000000);
                                                        key.SetValue("interlacing_tff", 0x00000000);
                                                        key.SetValue("codecId", 0x00000007);
                                                        key.SetValue("lastPage", 0x000000d5);
                                                        key.SetValue("dv_profile", 0x00000001);
                                                        key.SetValue("fourcc", 0x4745504d);
                                                        key.SetValue("flip", 0x00000000);
                                                        key.SetValue("isProc", 0x00000000);

                                                        //Image processing
                                                        key = Registry.CurrentUser.OpenSubKey(@"Software\GNU\ffdshow\ffdshowenc", true);
                                                        key.SetValue("cropTop", 0x00000000);
                                                        key.SetValue("isCropNzoom", 0x00000000);
                                                        key.SetValue("isZoom", 0x00000000);

                                                    }

                                                    break;
                                                }

                                            case MAMuxEncoder.Container.MXF:
                                                {

                                                    if (RenderSettings.Codec.Equals(MAMuxEncoder.Codec.DVCPRO50))
                                                    {
                                                        RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Software\GNU\ffdshow_enc", true);
                                                        key.SetValue("interlacing", 0x00000000);
                                                        key.SetValue("interlacing_tff", 0x00000000);
                                                        key.SetValue("lastPage", 0x000000c8);
                                                        key.SetValue("codecId", 0x00000019);
                                                        key.SetValue("dv_profile", 0x00000004);
                                                        key.SetValue("fourcc", 0x30357664);
                                                        key.SetValue("flip", 0x00000000);
                                                        key.SetValue("isProc", 0x00000000);

                                                        //Image processing
                                                        key = Registry.CurrentUser.OpenSubKey(@"Software\GNU\ffdshow\ffdshowenc", true);
                                                        key.SetValue("cropTop", 0x00000000);
                                                        key.SetValue("isCropNzoom", 0x00000000);
                                                        key.SetValue("isZoom", 0x00000000);

                                                    }

                                                    if (RenderSettings.Codec.Equals(MAMuxEncoder.Codec.IMX30))
                                                    {
                                                        RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Software\GNU\ffdshow_enc", true);
                                                        key.SetValue("interlacing", 0x00000000);
                                                        key.SetValue("interlacing_tff", 0x00000000);
                                                        key.SetValue("codecId", 0x00000007);
                                                        key.SetValue("lastPage", 0x000000d5);
                                                        key.SetValue("dv_profile", 0x00000001);
                                                        key.SetValue("fourcc", 0x4745504d);
                                                        key.SetValue("flip", 0x00000000);
                                                        key.SetValue("isProc", 0x00000001);

                                                        //Image processing
                                                        key = Registry.CurrentUser.OpenSubKey(@"Software\GNU\ffdshow\ffdshowenc", true);
                                                        key.SetValue("cropTop", 0x00000020);
                                                        key.SetValue("isCropNzoom", 0x00000001);
                                                        key.SetValue("isZoom", 0x00000001);

                                                    }


                                                    break;
                                                }
                                        }

                                        #endregion GIRO CLASSICO SWITCH
                                    }
                                    #endregion

                                }
                                catch (Exception ex)
                                {
                                    logger.error("    > Get ffdshow Encoder default configuration. Check Software GNU\\ffdshow_enc registry value.");
                                    logger.debug(ex);
                                    //Debug.WriteLine(@"Get ffdshow Encoder default configuration. Check Software\GNU\ffdshow_enc registry value.");

                                    message = ex.Message;

                                    return;
                                }

                                try
                                {
                                    message = "3 of 5  Extract and Combine Video ...";

                                    Raise_Render_OnProgress("Generating video track", 90);

                                    logger.info("    > Start video combining...");
                                    StartCombine();
                                }
                                catch (Exception ex)
                                {
                                    logger.error("    > " + ex.Message);
                                    logger.debug(ex);
                                    //Debug.WriteLine(message + " :" + ex.Message);

                                    message = ex.Message;


                                }
                                #endregion
                            }

                            m_actualStep = EnumSteps.Mux;
                            break;

                        case EnumSteps.Mux:

                            // QUI VIENE GENERARO IL MUX FINALE DA TUTTE LE TRACCE GENERATE
                            // SEPARATAMENTE

                            if (RenderSettings.m_rendermode == RenderMode.audioVideo || RenderSettings.m_rendermode == RenderMode.videoOnly)
                            {
                                // RENDER AUDIO/VIDEO O SOLO VIDEO

                                Raise_Render_OnProgress("Finalizing and Mux", 95);


                                #region RESEMPLING

                                if (RenderSettings.BitPerSample != 16)
                                {
                                    for (int idtrack = 1; idtrack <= RenderSettings.MaxMonoTrackPerClip; idtrack++)
                                    {
                                        string fileWaveMixed    = TempFolder + "AudioChannel_mixed_" + idtrack + ".wav";
                                        string fileWaveMixed16  = TempFolder + "AudioChannel_mixed_" + idtrack + "_16bit.wav";

                                        if (File.Exists(fileWaveMixed16)) File.Delete(fileWaveMixed16);

                                        File.Move(fileWaveMixed, fileWaveMixed16);

                                        FFMpegHelper.WavResampling resampling = new FFMpegHelper.WavResampling();
                                        resampling.Start(fileWaveMixed16, fileWaveMixed, RenderSettings.BitPerSample);

                                        File.Delete(fileWaveMixed16);       // Rimuovo il file a 16Bit tengo quello con il nuovo bps
                                    }   
                                }

                                #endregion RESEMPLING

                                
                                #region MUX

                                MAMuxEncoder.MuxEncoder maMuxEncoder = new MAMuxEncoder.MuxEncoder(RenderSettings.SamplesPerSeconds, RenderSettings.BitPerSample);

                                //MAMuxEncoder.MuxEncoder.CodecData codeData = new MuxEncoder.CodecData("MOV", MuxEncoder.DVCAM420, "");
                                
                                MAMuxEncoder.MuxEncoder.CodecData codeData = new MuxEncoder.CodecData(RenderSettings.Container, RenderSettings.Codec, "");

                                maMuxEncoder.SelectEncoder(codeData);


                                for (int idtrack = 1; idtrack <= RenderSettings.MaxMonoTrackPerClip; idtrack++)
                                {
                                    // AGGIUNGO TUTTE LE TRACCE MONO-MIXATE CHE TROVO DA 1 A MAXMONOTRACKPERCLIP

                                    string fileWaveMixed = TempFolder + "AudioChannel_mixed_" + idtrack + ".wav";
                                    maMuxEncoder.AddAudio(fileWaveMixed);

                                    logger.info("    > Added audio track " + idtrack + " to Mux");
                                }

                                string combinedVideoFile = TempFolder + "VideoTracksCombined.avi";

                                if (!RenderSettings.VideoIntermediateAvi)
                                {
                                    combinedVideoFile = TempFolder + "VideoTracksCombined.dv";
                                }

                                if (File.Exists(combinedVideoFile))
                                {
                                    // AGGIUNGO LA TRACCIA VIDEO

                                    maMuxEncoder.AddVideo(combinedVideoFile);
                                    logger.info("    > Added video track to Mux");
                                }

                                string error = "";
                                maMuxEncoder.Execute(outputFileName, RenderSettings.BitPerSample, out error);
                                logger.info("    > Mux operation completed");

                                #endregion MUX
                            }
                            else
                            {
                                // RENDER SOLO AUDIO

                                Raise_Render_OnProgress("Finalizing and Mux", 95);

                                if (RenderSettings.Container.Equals("WAV", StringComparison.OrdinalIgnoreCase))
                                {
                                    logger.info("    > Render container WAV on file : " + outputFileName);
                                    logger.info("        > Mixing mono tracks to one stereo file");

                                    clsMixerStereo mix = new clsMixerStereo(outputFileName);

                                    List<string> Left_audiofiles = new List<string>();
                                    List<string> Right_audiofiles = new List<string>();



                                    for (int idtrack = 1; idtrack <= RenderSettings.MaxMonoTrackPerClip; idtrack++)
                                    {
                                        // AGGIUNGO TUTTE LE TRACCE MONO-MIXATE CHE TROVO DA 1 A MAXMONOTRACKPERCLIP

                                        string fileWaveMixed = TempFolder + "AudioChannel_mixed_" + idtrack + ".wav";

                                        if (File.Exists(fileWaveMixed))
                                        {
                                            if (idtrack % 2 == 0)
                                            {
                                                logger.info("        > Adding right source : " + fileWaveMixed);
                                                Right_audiofiles.Add(fileWaveMixed);
                                            }
                                            else
                                            {
                                                logger.info("        > Adding left source  : " + fileWaveMixed);
                                                Left_audiofiles.Add(fileWaveMixed);
                                            }
                                        }
                                    }

                                    logger.info("        > Go mixing...");

                                    mix.Mono2Stereo(Left_audiofiles, Right_audiofiles, outputFileName);



                                    #region RESEMPLING

                                    if (RenderSettings.BitPerSample != 16)
                                    {
                                        logger.info("        > Go resampling...");

                                        string File16bit = Path.GetFileNameWithoutExtension(outputFileName) + "_16bit" + Path.GetExtension(outputFileName);
                                        File16bit = TempFolder + File16bit;

                                        if (File.Exists(File16bit)) File.Delete(File16bit);

                                        File.Move(outputFileName, File16bit);

                                        FFMpegHelper.WavResampling resampling = new FFMpegHelper.WavResampling();
                                        resampling.Start(File16bit, outputFileName, RenderSettings.BitPerSample, 2);

                                        if (File.Exists(File16bit)) File.Delete(File16bit);

                                        // EXTRA INFORMATION ON FILE

                                        logger.info("        > Writing extra information on Wave file");

                                        try
                                        {
                                            StreamWriter sw = new StreamWriter(outputFileName, true);
                                            sw.WriteLine("<NM_INFO>" +
                                                         "<APP>NMRenderEngine</APP>" +
                                                         "<VER>" + Program.MyAppVersion + "</VER>" +
                                                         "<NOW>" + DateTime.Now.ToString() + "</NOW>" +
                                                         "<PROJ>" + Program.ProjectSource + "</PROJ>" +
                                                         "<PROFILE>" + Program.ProfileXml + "</PROFILE>" +
                                                         "</NM_INFO>");
                                            sw.Close();
                                            sw.Dispose();
                                            logger.error("          > Ok");
                                        }
                                        catch(Exception ex)
                                        {
                                            logger.error("          > Error writing info : " + ex.Message);
                                        }
                                    }


                                    #endregion RESEMPLING

                                    logger.info("        > Done");
                                }
                                else
                                {
                                    logger.error("    > Container not implemented : " + RenderSettings.Container);
                                    if (OnError != null) OnError(this, " Container not implemented : " + RenderSettings.Container);
                                }
                            }

                            Raise_Render_OnProgress("Finalizing and Mux", 100);
                            m_actualStep = EnumSteps.End;
                            break;

                        case EnumSteps.End:

                            renderThread_Stop = true;

                            break;
                    }
                }

                Thread.Sleep(10);
            }
            catch(Exception ex)
            {
                Program.Logger.error(" MAIN THREAD ERROR : ");
                Program.Logger.error(ex);
            }

            Raise_Render_OnFinish("Rendering operation successfully completed");
        }


        #region RAISE METHODS

        private void Raise_Render_OnProgress(string step_descrition, int percent)
        {
            if (OnProgress != null)
            {
                foreach (Render_OnProgress singleCast in OnProgress.GetInvocationList())
                {
                    ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;
                    
                    if (syncInvoke != null && syncInvoke.InvokeRequired)
                    {
                        syncInvoke.Invoke(OnProgress, new object[] { this, step_descrition, percent });
                    }
                    else
                    {
                        singleCast(this, step_descrition, percent);
                    }
                }
            }
        }

        private void Raise_Render_OnStart(string step_descrition)
        {
            if (OnStart != null)
            {
                foreach (Render_OnStart singleCast in OnStart.GetInvocationList())
                {
                    ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;
                    
                    if (syncInvoke != null && syncInvoke.InvokeRequired)
                    {
                        syncInvoke.Invoke(OnProgress, new object[] { this, step_descrition });
                    }
                    else
                    {
                        singleCast(this, step_descrition);
                    }
                }
            }
        }

        private void Raise_Render_OnFinish(string step_descrition)
        {
            if (OnFinish != null)
            {
                foreach (Render_OnFinish singleCast in OnFinish.GetInvocationList())
                {
                    ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;
                   
                    if (syncInvoke != null && syncInvoke.InvokeRequired)
                    {
                        syncInvoke.Invoke(OnProgress, new object[] { this, step_descrition });
                    }
                    else
                    {
                        singleCast(this, step_descrition);
                    }
                }
            }
        }

        private void Raise_Render_OnError(string descrition)
        {
            if (OnError != null)
            {
                foreach (Render_OnError singleCast in OnError.GetInvocationList())
                {
                    ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;
                    
                    if (syncInvoke != null && syncInvoke.InvokeRequired)
                    {
                        syncInvoke.Invoke(OnProgress, new object[] { this, descrition });
                    }
                    else
                    {
                        singleCast(this, descrition);
                    }
                }
            }
        }

        #endregion RAISE METHODS



        public void Render()
        {
            logger.debug(" Calling Render");
            try
            {
                renderThread = new Thread(new ThreadStart(renderThread_Task));
                renderThread.IsBackground = true;
                renderThread.Start();
                logger.debug("      > Thread created");
            }
            catch (Exception ex)
            {
                logger.error("      > Thread creation error : " + ex.Message);
                logger.error(ex);
            }
        }

        public void Dispose()
        {
            if (renderThread != null && renderThread.IsAlive)
            {
                renderThread_Stop = true;

                if (!renderThread.Join(200))
                {
                    renderThread.Abort();
                }

                renderThread = null;
                renderThread_Stop = false;
            }
        }

        

        #region RENDER VIDEO commentato ...
        /*
        long totalTimeCombine = 0;
        private bool StartCombine()
        {
            m_maRender = new MARenderer.MARenderer();
            m_maRender.SetupVideoCallBack(new MARenderer.MARenderer.DelegateOnVideoProcessEvent(OnCombineVideoProcessEvent));
            m_maRender.OnStarted += new MARenderer.MARenderer.DelegateStarted(RendererOnStarted);
            m_maRender.OnCompleted += new MARenderer.MARenderer.DelegateCompleted(RendererOnCompleted);

            int videoWidth = MAIniFile.IniReadIntValue("Render", "VideoWidth");
            int videoHeight = MAIniFile.IniReadIntValue("Render", "VideoHeight");

            if (codec.Equals(MuxEncoder.DVCPRO50) ||
                codec.Equals(MuxEncoder.DVCAM420))
            {
                videoWidth = 720;
                videoHeight = 576;
            }
            if (codec.Equals(MuxEncoder.IMX50) ||
                codec.Equals(MuxEncoder.IMX30))
            {
                videoWidth = 720;
                videoHeight = 608;
            }
            if (codec.Equals(MuxEncoder.XDCAM_HD_1920))
            {
                videoWidth = 1920;
                videoHeight = 1080;
            }



            m_maRender.CreateCombine(videoWidth, videoHeight);
            m_maRender.CreateTracks(m_videoList.Count, 0);//m_videoMontageUC.GetAudioCount());


            m_maRender.EmptyAudioList();
            m_maRender.EmptyVideoList();

            foreach (MAPanelMovieUC movie in m_videoList)
            {
                long startMedia = movie.StartMediaTime * 10000;
                long stopMedia = movie.StopMediaTime * 10000;

                //ATTENZIONE: PATCH PER MAINCONCEPT COME FILTRO
                if (Path.GetExtension(movie.FullFileName).ToLower().Equals(".mxf"))
                {
                    stopMedia = (stopMedia - startMedia);
                    startMedia /= 2;
                    stopMedia += startMedia;
                }

                m_maRender.AddVideo(movie.FullFileName, movie.StartTime * 10000,
                        startMedia,
                        stopMedia,

                        false);



            }


            string tempFileName = clsProject2.GetCombinedVideoFilename();
            string encoder = MAIniFile.IniReadStringValue("Render", "Encoder");

            if (codec.Equals(MAMuxEncoder.MuxEncoder.IMX30) ||
                codec.Equals(MAMuxEncoder.MuxEncoder.IMX50) ||
                codec.Equals(MAMuxEncoder.MuxEncoder.XDCAM_HD_1920))
            {
                tempFileName = tempFileName.Replace(".avi", ".m2v");
                encoder = MAIniFile.IniReadStringValue("Codec", "IMX");
            }

            if (codec.Equals(MAMuxEncoder.MuxEncoder.DVCPRO50) ||
                codec.Equals(MAMuxEncoder.MuxEncoder.DVCAM420))
            {
                encoder = MAIniFile.IniReadStringValue("Codec", "DV");
            }



            if (File.Exists(tempFileName))
                File.Delete(tempFileName);



            bool ret = m_maRender.Start(tempFileName, @encoder, null);
            totalTimeCombine = Environment.TickCount;
            return ret;
        }

        private void OnCombineVideoProcessEvent(long bytes, double mediaTime, double elapsed, System.IntPtr pBuffer, int BufferLen)
        {
            try
            {
                this.Invoke(new DelegateInvokeRendererProgressBar(InvokeRendererProgressBar), new object[] { (long)(mediaTime * 1000.0f) });
            }
            catch { }
        }
        private void InvokeRendererProgressBar(long mediaLenght)
        {
            toolStripProgressBar1.Value = (int)mediaLenght;

            if (m_abort)
            {
                m_maRender.Stop();
                m_maRender.Dispose();
                m_maRender = null;

                m_processError = ProcessError.Aborted;
            }
        }

        private void RendererOnStarted(long mediaLenght)
        {
            m_renderIsStarted = true;

            this.Invoke(new DelegateInvokeRendererProgressBarSetup(InvokeRendererProgressBarSetup), new object[] { mediaLenght });
        }
        private void InvokeRendererProgressBarSetup(long mediaLenght)
        {
            if (m_maRender != null)
            {
                //Caption = "1 of 3  Combine Video Started ...";
               
                toolStripProgressBar1.Maximum = (int)(mediaLenght / (m_maRender.Units / 1000));
                //toolStripProgressBar1.Step = toolStripProgressBar1.Maximum / 20;
                toolStripProgressBar1.Value = 0;

                if (m_abort)
                {
                    m_maRender.Stop();
                    m_maRender.Dispose();
                    m_maRender = null;

                    m_processError = ProcessError.Aborted;
                }
            }
        }

        private void RendererOnCompleted(CompletedArgs msg)
        {
            this.Invoke(new DelegateInvokeRendererCompleted(InvokeRendererCompleted), new object[] { msg });
        }

        //Mainconcept.MXF_Encoder mxfIMXEncoder = null;
        //       MuxEncoder muxEncoder = null;
        private void InvokeRendererCompleted(CompletedArgs msg)
        {
            try
            {
                toolStripProgressBar1.Value = toolStripProgressBar1.Maximum;
                
                long totalCombine = Environment.TickCount - totalTimeCombine;
                if (totalCombine < 1000)
                {
                    MessageBox.Show("Try again please.");
                    Close();
                    return;
                }


                #region Abort
                if (m_processError != ProcessError.None)
                {
                    Thread.Sleep(2000);

                    DialogResult = DialogResult.Abort;
                    Close();

                    return;
                }
                #endregion


                m_maRender.Stop();
                m_maRender.Dispose();
                m_maRender = null;


                Caption = "4 of 5  Create Multi Channel audio stream ...";
                

                #region Split rendered audio to mono

                int sourceCoupleTracksIndex = MAIniFile.IniReadIntValue("VoiceTrackMapping", "SourceCoupleTracksIndex");
                int destinationCoupleTracksIndex = MAIniFile.IniReadIntValue("VoiceTrackMapping", "DestinationCoupleTracksIndex");

                //                int maxTracks = MAIniFile.IniReadIntValue("Audio", "MaxTracks");

                int[] monoToStereoMapping = new int[] { 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8 };


                int idTrack = (2 * destinationCoupleTracksIndex) - 1;
                clsWavManager.SpitStereoTo2Monos(clsProject2.GetRenderedAudioFilename(),
                                                 clsProject2.GetRenderedAudioFilenameByTrack(idTrack),
                                                 clsProject2.GetRenderedAudioFilenameByTrack(idTrack + 1));

                #endregion


                List<string> audioList = new List<string>();
                for (int idDestTrack = 1; idDestTrack <= m_maxTracksAfterExtract; idDestTrack++)
                {
                    if (File.Exists(clsProject2.GetRenderedAudioFilenameByTrack(idDestTrack)))
                        audioList.Add(clsProject2.GetRenderedAudioFilenameByTrack(idDestTrack));
                }


                SOXHelper.MonoToMulti monoToMulti = new SOXHelper.MonoToMulti();
                monoToMulti.Start(audioList, clsProject2.GetMultiChannelAudioFilename());


                Caption = "5 of 5  Preparing Encoding  ...";
                Application.DoEvents();

                string combinedFileName = clsProject2.GetCombinedVideoFilename();
                if (m_selectedCodecData.Format.Equals(MAMuxEncoder.MuxEncoder.IMX30))
                    combinedFileName = combinedFileName.Replace(".avi", ".m2v");
                if (m_selectedCodecData.Format.Equals(MAMuxEncoder.MuxEncoder.IMX50))
                    combinedFileName = combinedFileName.Replace(".avi", ".m2v");
                if (m_selectedCodecData.Format.Equals(MAMuxEncoder.MuxEncoder.XDCAM_HD_1920))
                    combinedFileName = combinedFileName.Replace(".avi", ".m2v");


                toolStripProgressBar1.Value = 0;
                toolStripProgressBar1.Maximum = 100;


                #region Prepare Mux

                Caption = "5 of 5  Encoding ...";
                Application.DoEvents();

                //MXF IMX30
                if ((m_selectedCodecData.Format.Equals(MAMuxEncoder.MuxEncoder.IMX30) ||
                      m_selectedCodecData.Format.Equals(MAMuxEncoder.MuxEncoder.IMX50) ||
                      m_selectedCodecData.Format.Equals(MAMuxEncoder.MuxEncoder.XDCAM_HD_1920)) &&
                        m_selectedCodecData.Ext.Equals(MAMuxEncoder.MuxEncoder.ContainerMXF))
                {
                    MuxEncoder.OnComputing(new MuxEncoder.ReceiveCallbackFunction(OnComputingMXFEncoder));

                    int preset = 30;
                    if (m_selectedCodecData.Format.Equals(MAMuxEncoder.MuxEncoder.IMX30))
                    {
                        MuxEncoder.SetBitsPerSecond(30000000);
                        preset = 30;
                    }
                    if (m_selectedCodecData.Format.Equals(MAMuxEncoder.MuxEncoder.IMX50))
                    {
                        MuxEncoder.SetBitsPerSecond(50000000);
                        preset = 50;
                    }
                    if (m_selectedCodecData.Format.Equals(MAMuxEncoder.MuxEncoder.XDCAM_HD_1920))
                    {
                        MuxEncoder.SetBitsPerSecond(50000000);
                        preset = 1920;
                    }

                    int ret = 0;
                    if (m_selectedCodecData.Format.Equals(MAMuxEncoder.MuxEncoder.XDCAM_HD_1920))
                        ret = MuxEncoder.StartMuxToMXFXDCAMHD(clsProject2.GetMultiChannelAudioFilename(),
                                                           combinedFileName,
                                                           @m_finalVideoName, preset);
                    else
                        ret = MuxEncoder.StartMuxToMXFIMX(clsProject2.GetMultiChannelAudioFilename(),
                                                           combinedFileName,
                                                            @m_finalVideoName, preset);

                    if (ret < 0)
                        MessageBox.Show("Retry Render process, some system error.", "Encoder Process");
                }
                else
                //if (m_selectedCodecData.Format.Equals(MAMuxEncoder.MuxEncoder.DVCPRO50) && m_selectedCodecData.Ext.Equals(MAMuxEncoder.MuxEncoder.ContainerMXF))
                {
                    MAMuxEncoder.MuxEncoder maMuxEncoder = new MAMuxEncoder.MuxEncoder(clsAudioEdit.m_sampleRate, clsAudioEdit.m_bitsPerSample);
                    maMuxEncoder.SelectEncoder(m_selectedCodecData);


                    for (int idDestTrack = 1; idDestTrack <= m_maxTracksAfterExtract; idDestTrack++)
                    {
                        if (File.Exists(clsProject2.GetRenderedAudioFilenameByTrack(idDestTrack)))
                            maMuxEncoder.AddAudio(clsProject2.GetRenderedAudioFilenameByTrack(idDestTrack));
                    }

                  
                    maMuxEncoder.AddVideo(combinedFileName);

                    string error = "";
                    maMuxEncoder.Execute(@m_finalVideoName, out error);
                }
                #endregion

         

                toolStripProgressBar1.Value = toolStripProgressBar1.Maximum;
                OnFinished_MXFEncoder();

            }
            catch (Exception ex)
            {
                Debug.WriteLine("InvokeRendererCompleted :" + ex.Message);
            }

        }

        private void OnFinished_MXFEncoder()
        {
            this.Invoke(new DelegateInvokeEndEncoder(InvokeEndEncoder));
        }
        private void InvokeEndEncoder()
        {

            //if (mxfIMXEncoder != null)
            //{
            //    mxfIMXEncoder.Close();
            //    mxfIMXEncoder = null;
            //}

            GC.Collect();
            Close();
        }

        */
        #endregion


        /// <summary>
        /// Estrae tracce raw-audio e da quelle estrae tutte le singole tracce mono in wav
        /// </summary>
        /// <param name="track"></param>
        /// <param name="prefix"></param>
        private void ExtratAudioRawFromVideo(CClipInfo_Collection track, string prefix)
        {
            int clipProgressId = 0;
            foreach (CClipInfo clipInfo in track)
            {
                FFMpegHelper.AudioRawExtract audioRawExtract = new FFMpegHelper.AudioRawExtract();


                string rawTempFile = TempFolder + prefix + clipProgressId + ".raw";
                if (File.Exists(rawTempFile)) File.Delete(rawTempFile);


                FFMpegHelper.FileInfo fi = new FFMpegHelper.FileInfo();
                fi.CheckForAudio(clipInfo.Source_Filename);

                logger.info("        > Working on : " + clipInfo.Source_Filename);
                logger.info("            > MultipleAudioStream      : " + fi.MultipleAudioStream);
                logger.info("            > HasVideo                 : " + fi.HasVideo);
                logger.info("            > BitPerSecond             : " + fi.BitPerSecond);
                logger.info("            > AudioChannels            : " + fi.AudioChannels);

                if (fi.MultipleAudioStream)
                {
                    int firstAudioStream = (fi.HasVideo ? 1 : 0);
                    int idStreamAudio = 0;

                    if (fi.HasVideo)
                        idStreamAudio = 1;

                    List<string> rawMonoList = new List<string>();

                    for (; idStreamAudio < fi.AudioChannels + firstAudioStream; idStreamAudio++)
                    {
                        rawTempFile = TempFolder + prefix + clipProgressId + "_" + idStreamAudio + ".raw";

                        if (File.Exists(rawTempFile)) File.Delete(rawTempFile);

                        audioRawExtract.StartSingleStreamFromMulti(idStreamAudio,
                                                                    clipInfo.Source_Filename,
                                                                    (ulong)clipInfo.ClippingInMs,
                                                                    (ulong)clipInfo.ClipDurationMs,
                                                                    rawTempFile,
                                                                    fi.BitPerSecond);
                        rawMonoList.Add(rawTempFile);
                    }

                    List<string> wavList = ExtractMonoWaveFomRaw(rawTempFile, fi.AudioChannels, fi.BitPerSecond, rawMonoList);

                    foreach (string wavFileName in wavList) FadeWavMono(wavFileName, clipInfo);
                }
                else
                {
                    if (fi.TrackType != FFMpegHelper.FileInfo.EnumTrackType.Mono)
                    {
                        audioRawExtract.StartSingleStream(clipInfo.Source_Filename, 
                                                            (ulong)clipInfo.ClippingInMs, 
                                                            (ulong)clipInfo.ClipDurationMs, 
                                                            rawTempFile, 
                                                            fi.BitPerSecond);

                        List<string> rawMonoList = ExtractMonoRawFromRaw(rawTempFile, fi.AudioChannels);

                        List<string> wavList = ExtractMonoWaveFomRaw(rawTempFile, fi.AudioChannels, fi.BitPerSecond, rawMonoList);

                        foreach (string wavFileName in wavList)
                            FadeWavMono(wavFileName, clipInfo);

                    }
                    else //multi channel
                    {
                        int firstAudioStream = (fi.HasVideo ? 1 : 0);
                        int idStreamAudio = 0;

                        if (fi.HasVideo)
                            idStreamAudio = 1;

                        for (; idStreamAudio < fi.AudioChannels + firstAudioStream; idStreamAudio++)
                        {
                            int fileAudioIndex = idStreamAudio + (fi.HasVideo ? 0 : 1);

                            rawTempFile = TempFolder + prefix + clipProgressId + ".raw";

                            if (File.Exists(rawTempFile))
                                File.Delete(rawTempFile);

                            audioRawExtract.StartSingleStreamFromMulti(idStreamAudio, 
                                                                        clipInfo.Source_Filename, 
                                                                        (ulong)clipInfo.ClippingInMs, 
                                                                        (ulong)clipInfo.ClipDurationMs, 
                                                                        rawTempFile, 
                                                                        fi.BitPerSecond);

                            List<string> rawMonoList = ExtractMonoRawFromRaw(rawTempFile, fi.AudioChannels);

                            List<string> wavList = ExtractMonoWaveFomRaw(rawTempFile, fi.AudioChannels, fi.BitPerSecond, rawMonoList);

                            foreach (string wavFileName in wavList)
                                FadeWavMono(wavFileName, clipInfo);

                        }
                    }
                }
                clipProgressId++;
            }
        }

        private List<string> ExtractMonoRawFromRaw(string rawFileName, int numInternalTrack)
        {
            //Extract single channel raw from main raw.
            WAV_Multi_2_StereoPairs.CMtoN2 test = new WAV_Multi_2_StereoPairs.CMtoN2();
            test.Setup("1964");
            List<string> outputStereoRaws;

            //numInternalTrack = 2;
            test.ConvertRaw(rawFileName, numInternalTrack, 1, TempFolder, out outputStereoRaws);

            return outputStereoRaws;
        }

        private List<string> ExtractMonoWaveFomRaw(string rawFileName, int numInternalTrack, int bps, List<string> rawMonoList)
        {
            foreach (string singleRawFileName in rawMonoList)
            {
                FFMpegHelper.AudioRawToWavMono audioRawToWavMono = new FFMpegHelper.AudioRawToWavMono();

                string wavFileName = singleRawFileName.Replace(".raw", "_temp.wav");
                
                if (File.Exists(wavFileName))
                    File.Delete(wavFileName);

                audioRawToWavMono.Start(singleRawFileName, wavFileName, bps);

                //Resample
                //if (bps == 24 || bps == 32)
                {
                    FFMpegHelper.AudioResample audioResample = new FFMpegHelper.AudioResample(RenderSettings.SamplesPerSeconds, 1, RenderSettings.BitPerSample);

                    if (File.Exists(wavFileName))
                    {
                        audioResample.Start(wavFileName, wavFileName.Replace("_temp", ""));
                        File.Delete(wavFileName);                 
                    }
                }
            }

            for (int i = 0; i < rawMonoList.Count; i++ )
                rawMonoList[i] = rawMonoList[i].Replace(".raw", ".wav");

            return rawMonoList;
        }

        private void FadeWavMono(string wavInFileName, CClipInfo clipInfo)
        {

            string tempFile = wavInFileName+".temp.wav";

            if (File.Exists(tempFile))
                File.Delete(tempFile);

            File.Move(wavInFileName, tempFile);

            clsFaderStereo audioHelperFader = new clsFaderStereo(tempFile, wavInFileName, -1, -1, FADER_MODE_STEREO.crop);
           
            
            foreach(CAudioNode audioNode in clipInfo.AudioNodes)
            {
                float vol = audioNode.Value * clipInfo.AudioGlobal;

                TimelinePoint tlPoint = new TimelinePoint((long)(audioNode.Timecode * (1000.0f / RenderSettings.FrameRate)), vol);
                audioHelperFader.addKeyPoint(tlPoint);
            }
            
            if (clipInfo.AudioNodes.Count == 0)
            {
                // SE NON CI SONO PUNTI AUDIO NE AGGIUNGO DUE PER APPLICARE IL VALORE DI AUDIOGLOBAL E/O PER AZZERARE
                // L'AUDIO DI TRACCE DISABILITATE. SE LA TRACCIA ERA DISABILITATA, L'AUDIO GLOBAL ARRIVA A ZERO

                TimelinePoint tlPoint = new TimelinePoint((long)(0 * (1000.0f / RenderSettings.FrameRate)), 1 * clipInfo.AudioGlobal);
                audioHelperFader.addKeyPoint(tlPoint);
                tlPoint = new TimelinePoint((long)(clipInfo.ClippingOut * (1000.0f / RenderSettings.FrameRate)), 1 * clipInfo.AudioGlobal);
                audioHelperFader.addKeyPoint(tlPoint);
            }

            audioHelperFader.DoProcess(1);

            if (File.Exists(tempFile))
                File.Delete(tempFile);

        }

        private void CombineAudioTrack(CClipInfo_Collection track, string prefix, long maxTrackDuration)
        {
            for (int idtrack = 1; idtrack <= RenderSettings.MaxMonoTrackPerClip; idtrack++)
            {
                string fileWave = TempFolder + prefix + "combined_" + idtrack + ".wav";

                if (File.Exists(fileWave))
                    File.Delete(fileWave);

                //Create empty file because the mail file (fileWave) doesn't exist.
                clsAudioEdit.CreateEmptyMonoFile(fileWave, (int)maxTrackDuration, 16, RenderSettings.SamplesPerSeconds);//(short)renderSettings.BitPerSample, renderSettings.SamplesPerSeconds);
                
                int currentIdClip = 0;

                foreach (CClipInfo clipInfo in track)
                {
                    string wavClipFilename = TempFolder + prefix + currentIdClip + "_" + idtrack + ".wav";

                    if (File.Exists(wavClipFilename))
                    {
                        clsAudioEdit.Insert(fileWave, 
                                            wavClipFilename, 
                                            (int)clipInfo.FrameInMs, 
                                            16, 
                                            RenderSettings.SamplesPerSeconds);// (short)renderSettings.BitPerSample, renderSettings.SamplesPerSeconds);
                    }
                    
                    currentIdClip++;
                }
            }
            
        }




        long totalTimeCombine = 0;

        private bool StartCombine()
        {
            m_maRender = new MARenderer.MARenderer();
            m_maRender.SetupVideoCallBack(new MARenderer.MARenderer.DelegateOnVideoProcessEvent(OnCombineVideoProcessEvent));
            //m_maRender.OnStarted += new MARenderer.MARenderer.DelegateStarted(RendererOnStarted);
            m_maRender.OnCompleted += new MARenderer.MARenderer.DelegateCompleted(RendererOnCompleted);

            if (RenderSettings.Codec.Equals(MAMuxEncoder.Codec.DVCPRO50) ||
                RenderSettings.Codec.Equals(MAMuxEncoder.Codec.DVCAM420))
            {
                //renderSettings.VideoWidth = 720;
                //renderSettings.VideoHeight = 576;
            }
            if (RenderSettings.Codec.Equals(MAMuxEncoder.Codec.IMX50) ||
                RenderSettings.Codec.Equals(MAMuxEncoder.Codec.IMX30))
            {
                RenderSettings.VideoWidth = 720;
                RenderSettings.VideoHeight = 608;
            }
            if (RenderSettings.Codec.Equals(MAMuxEncoder.Codec.XDCAM_HD_1920))
            {
                RenderSettings.VideoWidth = 1920;
                RenderSettings.VideoHeight = 1080;
            }

            int numMovieTracks = cProjectsParser.VideoTrack_B.Count + cProjectsParser.VideoTrack_A.Count;

            m_maRender.CreateCombine(RenderSettings.VideoWidth, RenderSettings.VideoHeight, RenderSettings.FrameRate);
            m_maRender.CreateTracks(numMovieTracks, 0);

            m_maRender.EmptyAudioList();
            m_maRender.EmptyVideoList();


            foreach (CClipInfo clipInfo in cProjectsParser.VideoTrack_B)
            {
                if (!clipInfo.VideoTrack.off)
                {
                    long startMedia = (long)clipInfo.ClippingInMs * 10000;
                    long stopMedia = (long)clipInfo.ClippingOutMs * 10000;

                    //ATTENZIONE: PATCH PER MAINCONCEPT COME FILTRO
                    if (Path.GetExtension(clipInfo.Source_Filename).ToLower().Equals(".mxf"))
                    {
                        stopMedia = (stopMedia - startMedia);
                        startMedia /= 2;
                        stopMedia += startMedia;
                    }


                    m_maRender.AddVideo(clipInfo.Source_Filename, (long)clipInfo.FrameInMs * 10000,
                            startMedia,
                            stopMedia,

                            false);
                }
            }

            foreach (CClipInfo clipInfo in cProjectsParser.VideoTrack_A)
            {
                if (!clipInfo.VideoTrack.off)
                {
                    // SE LA TRACCIA A CUI APPARTIENE LA CLIP NON E' DISABILITATA DA PROGETTO
                    // AGGIUNGO LA CLIP NEL PROGETTO DES

                    long startMedia = (long)clipInfo.ClippingInMs * 10000;
                    long stopMedia = (long)clipInfo.ClippingOutMs * 10000;

                    //ATTENZIONE: PATCH PER MAINCONCEPT COME FILTRO
                    if (Path.GetExtension(clipInfo.Source_Filename).ToLower().Equals(".mxf"))
                    {
                        stopMedia = (stopMedia - startMedia);
                        startMedia /= 2;
                        stopMedia += startMedia;
                    }


                    m_maRender.AddVideo(clipInfo.Source_Filename, (long)clipInfo.FrameInMs * 10000,
                            startMedia,
                            stopMedia,

                            false);
                }
            }

            string combinedVideoFile = TempFolder + "VideoTracksCombined.avi";

            if (!RenderSettings.VideoIntermediateAvi)
            {
                combinedVideoFile = TempFolder + "VideoTracksCombined.dv";
            }

            if (File.Exists(combinedVideoFile))
                File.Delete(combinedVideoFile);


            bool ret = m_maRender.Start(combinedVideoFile, "ffdshow video encoder", null);

            mre.WaitOne();

            totalTimeCombine = Environment.TickCount;
            return ret;
        }

        private void OnCombineVideoProcessEvent(long bytes, double mediaTime, double elapsed, System.IntPtr pBuffer, int BufferLen)
        {
            Debug.WriteLine((long)mediaTime);
          
        }

        private void RendererOnCompleted(CompletedArgs msg)
        {
            mre.Set();
        }

        /*
        public int ExtraAllAudioFromRaw(CClipInfo_Collection track, string prefix)
        {
            int nAudioTrack = 0;
          

            int clipProgressId = 0;
            foreach (CClipInfo clipInfo in track)
            {
            }
       
            for (int iRow = 2; iRow >= 1; iRow--)
            {
                int[] videos = clsVideoTimeline.GetVideos(iRow);
                if (videos != null)
                {

                    foreach (int curVideo in videos)
                    {
                        string videoFileName = clsVideoTimeline.GetSourceFile(curVideo);

                        FFMpegHelper.FileInfo fi = new FFMpegHelper.FileInfo();
                        fi.CheckForAudio(videoFileName);
                        int maxTracks = fi.AudioChannels;

                        if (nAudioTrack < maxTracks)
                            nAudioTrack = maxTracks;


                        if (fi.TrackType != FFMpegHelper.FileInfo.EnumTrackType.Mono)
                        {
                            WAV_Multi_2_StereoPairs.CMtoN2 test = new WAV_Multi_2_StereoPairs.CMtoN2();
                            test.Setup("1964");
                            List<string> outputStereoRaws;

                            string tmpFile = clsInternalFunctions.addPath(clsProject2.GetAudioTmpFolder(), curVideo.ToString() + ".raw");

                            test.ConvertRaw(tmpFile, maxTracks, 1, clsProject2.GetAudioTmpFolder(), out outputStereoRaws);


                            for (int idAudioTrack = 1; idAudioTrack <= maxTracks; idAudioTrack++)
                            {
                                FFMpegHelper.AudioRawToWavMono audioRawToWavMono = new FFMpegHelper.AudioRawToWavMono();
                                string rawFileName = clsInternalFunctions.addPath(clsProject2.GetAudioTmpFolder(), curVideo.ToString() + "_" + idAudioTrack + ".raw");


                                string wavFileName = clsInternalFunctions.addPath(clsProject2.GetAudioTmpFolder(), curVideo.ToString() + "_m" + idAudioTrack + ".wav");
                                if (File.Exists(wavFileName))
                                    File.Delete(wavFileName);

                                audioRawToWavMono.Start(rawFileName, wavFileName, fi.BitPerSecond);
                            }

                            #region Create Stero Track

                            string tmpDestFile = clsInternalFunctions.addPath(clsProject2.GetAudioTmpFolder(), curVideo.ToString() + ".wav");

                            int baseTrack = (2 * maxMonoTrackPerClip) - 1;
                            string tmpSrc1File = clsInternalFunctions.addPath(clsProject2.GetAudioTmpFolder(), curVideo.ToString() + "_m" + baseTrack + ".wav");
                            string tmpSrc2File = clsInternalFunctions.addPath(clsProject2.GetAudioTmpFolder(), curVideo.ToString() + "_m" + (baseTrack + 1) + ".wav");

                            audioHelper.clsWavManager.Combine2MonosToStereo(tmpSrc1File, tmpSrc2File, tmpDestFile);

                            #endregion
                        }
                        else ExtractAllAudioByVideoId(curVideo, maxTracks, fi.BitPerSecond);
                    }
                }

                NewsMontageFrm main = this.Owner as NewsMontageFrm;
                main.AddTotalAudioForVideo(iRow);

            }

            return nAudioTrack;
        }
 * 
 * */

        #region RENDER AUDIO commentato ....
        /*
        public int ExtraAllAudioFromMultiRaw()
        {
            int nAudioTrack = 0;
            int sourceCoupleTracksIndex = MAIniFile.IniReadIntValue("VoiceTrackMapping", "SourceCoupleTracksIndex");

            //            int maxTracks = MAIniFile.IniReadIntValue("Audio", "MaxTracks");

            for (int iRow = 2; iRow >= 1; iRow--)
            {
                int[] videos = clsVideoTimeline.GetVideos(iRow);
                if (videos != null)
                {

                    foreach (int curVideo in videos)
                    {
                        string videoFileName = clsVideoTimeline.GetSourceFile(curVideo);

                        FFMpegHelper.FileInfo fi = new FFMpegHelper.FileInfo();
                        fi.CheckForAudio(videoFileName);
                        int maxTracks = fi.AudioChannels;

                        if (nAudioTrack < maxTracks)
                            nAudioTrack = maxTracks;

                        if (fi.MultipleAudioStream)
                        {
                            for (int idAudioTrack = 1; idAudioTrack <= maxTracks; idAudioTrack++)
                            {

                                FFMpegHelper.AudioRawToWavMono audioRawToWavMono = new FFMpegHelper.AudioRawToWavMono();
                                string rawFileName = clsInternalFunctions.addPath(clsProject2.GetAudioTmpFolder(), curVideo.ToString() + "_" + idAudioTrack + ".raw");


                                string wavFileName = clsInternalFunctions.addPath(clsProject2.GetAudioTmpFolder(), curVideo.ToString() + "_m" + idAudioTrack + ".wav");
                                if (File.Exists(wavFileName))
                                    File.Delete(wavFileName);

                                audioRawToWavMono.Start(rawFileName, wavFileName, fi.BitPerSecond);
                            }
                        }
                        else ExtraAllAudioFromMutiRawByVideoId(curVideo, maxTracks, fi.BitPerSecond);

                        #region Create Stero Track

                        string tmpDestFile = clsInternalFunctions.addPath(clsProject2.GetAudioTmpFolder(), curVideo.ToString() + ".wav");

                        int baseTrack = (2 * sourceCoupleTracksIndex) - 1;
                        string tmpSrc1File = clsInternalFunctions.addPath(clsProject2.GetAudioTmpFolder(), curVideo.ToString() + "_m" + baseTrack + ".wav");
                        string tmpSrc2File = clsInternalFunctions.addPath(clsProject2.GetAudioTmpFolder(), curVideo.ToString() + "_m" + (baseTrack + 1) + ".wav");

                        audioHelper.clsWavManager.Combine2MonosToStereo(tmpSrc1File, tmpSrc2File, tmpDestFile);

                        #endregion
                    }
                }

                NewsMontageFrm main = this.Owner as NewsMontageFrm;
                main.AddTotalAudioForVideo(iRow);

            }

            return nAudioTrack;
        }
        private void ExtraAllAudioFromMutiRawByVideoId(int curVideo, int maxTracks, int bps)
        {
            int sourceCoupleTracksIndex = MAIniFile.IniReadIntValue("VoiceTrackMapping", "SourceCoupleTracksIndex");

            WAV_Multi_2_StereoPairs.CMtoN2 test = new WAV_Multi_2_StereoPairs.CMtoN2();
            test.Setup("1964");
            List<string> outputStereoRaws;

            string tmpFile = clsInternalFunctions.addPath(clsProject2.GetAudioTmpFolder(), curVideo.ToString() + ".raw");

            test.ConvertRaw(tmpFile, maxTracks, 1, clsProject2.GetAudioTmpFolder(), out outputStereoRaws);


            for (int idAudioTrack = 1; idAudioTrack <= maxTracks; idAudioTrack++)
            {
                FFMpegHelper.AudioRawToWavMono audioRawToWavMono = new FFMpegHelper.AudioRawToWavMono();
                string rawFileName = clsInternalFunctions.addPath(clsProject2.GetAudioTmpFolder(), curVideo.ToString() + "_" + idAudioTrack + ".raw");


                string wavFileName = clsInternalFunctions.addPath(clsProject2.GetAudioTmpFolder(), curVideo.ToString() + "_m" + idAudioTrack + ".wav");
                if (File.Exists(wavFileName))
                    File.Delete(wavFileName);

                audioRawToWavMono.Start(rawFileName, wavFileName, bps);
            }

            #region Create Stero Track

            string tmpDestFile = clsInternalFunctions.addPath(clsProject2.GetAudioTmpFolder(), curVideo.ToString() + ".wav");

            int baseTrack = (2 * sourceCoupleTracksIndex) - 1;
            string tmpSrc1File = clsInternalFunctions.addPath(clsProject2.GetAudioTmpFolder(), curVideo.ToString() + "_m" + baseTrack + ".wav");
            string tmpSrc2File = clsInternalFunctions.addPath(clsProject2.GetAudioTmpFolder(), curVideo.ToString() + "_m" + (baseTrack + 1) + ".wav");

            audioHelper.clsWavManager.Combine2MonosToStereo(tmpSrc1File, tmpSrc2File, tmpDestFile);

            #endregion
        }

     
        private void ExtractAllAudioByVideoId(int curVideo, int maxTracks, int bps)
        {
            int sourceCoupleTracksIndex = MAIniFile.IniReadIntValue("VoiceTrackMapping", "SourceCoupleTracksIndex");

            string videoFileName = clsVideoTimeline.GetSourceFile(curVideo);

            for (int idAudioTrack = 1; idAudioTrack <= maxTracks; idAudioTrack++)
            {

                FFMpegHelper.AudioRawToWavMono audioRawToWavMono = new FFMpegHelper.AudioRawToWavMono(48000, 1, 16);
                string rawFileName = clsInternalFunctions.addPath(clsProject2.GetAudioTmpFolder(), curVideo.ToString() + "_" + idAudioTrack + ".raw");


                string wavFileName = clsInternalFunctions.addPath(clsProject2.GetAudioTmpFolder(), curVideo.ToString() + "_m_temp" + idAudioTrack + ".wav");
                if (File.Exists(wavFileName))
                    File.Delete(wavFileName);

                audioRawToWavMono.Start(rawFileName, wavFileName, bps);

                //Resample to 16 bit
                if (bps == 24 || bps == 32)
                {
                    FFMpegHelper.AudioResample audioResample = new FFMpegHelper.AudioResample(clsAudioEdit.m_sampleRate, 1, clsAudioEdit.m_bitsPerSample);

                    if (File.Exists(wavFileName))
                    {
                        audioResample.Start(wavFileName, wavFileName.Replace("_temp", ""));
                        File.Delete(wavFileName);
                    }
                }
                else
                {
                    if (File.Exists(wavFileName))
                    {
                        string WaveFileDest = wavFileName.Replace("_temp", "");

                        if (File.Exists(WaveFileDest))
                            File.Delete(WaveFileDest);

                        File.Copy(wavFileName, WaveFileDest);
                    }
                }

            }

            #region Create Stero Track

            string tmpDestFile = clsInternalFunctions.addPath(clsProject2.GetAudioTmpFolder(), curVideo.ToString() + ".wav");

            int baseTrack = (2 * sourceCoupleTracksIndex) - 1;
            string tmpSrc1File = clsInternalFunctions.addPath(clsProject2.GetAudioTmpFolder(), curVideo.ToString() + "_m" + baseTrack + ".wav");
            string tmpSrc2File = clsInternalFunctions.addPath(clsProject2.GetAudioTmpFolder(), curVideo.ToString() + "_m" + (baseTrack + 1) + ".wav");

            audioHelper.clsWavManager.Combine2MonosToStereo(tmpSrc1File, tmpSrc2File, tmpDestFile);

            #endregion
        }
*/
        #endregion


    }
}
