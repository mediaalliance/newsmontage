﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Microsoft.Win32;
using MediaAlliance.Tools.Hextools;

namespace MediaAlliance.NewsMontage.RenderEngine.Classes
{
    public class CMARegistryTools
    {
        public static void ManualSetupReg(string regfile)
        {
            if (File.Exists(regfile))
            {
                StreamReader sr = new StreamReader(regfile);

                string[] currentKeyName = new string[0];
                string[] currentSubkeys = new string[0];

                RegistryKey currentKey = null;
                bool undifined_king = true;            // RegistryValueKind.None c'è solo su FW 4 per cui... patch

                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();

                    if (line.Trim().StartsWith("[") && line.Trim().EndsWith("]"))
                    {
                        #region KEY PATH

                        currentKeyName = line.Trim().Replace("[", "").Replace("]", "").Split('\\');
                        currentSubkeys = new string[currentKeyName.Length - 1];
                        Array.Copy(currentKeyName, 1, currentSubkeys, 0, currentSubkeys.Length);

                        if (currentKeyName[0] == "HKEY_CURRENT_USER")
                        {
                            currentKey = Registry.CurrentUser.CreateSubKey(string.Join("\\", currentSubkeys));

                        }

                        #endregion KEY PATH
                    }
                    else if (line.Trim().StartsWith("\"") && currentKey != null && line.IndexOf("=") > 0)
                    {
                        #region VALUES

                        string name         = line.Split('=')[0].Replace("\"", "");
                        string valueComplex = line.Split('=')[1];

                        RegistryValueKind kind = RegistryValueKind.None;

                        string value_str = "";
                        object value = null;

                        if (valueComplex.Trim().StartsWith("\"") && valueComplex.Trim().EndsWith("\""))
                        {
                            #region STRINGS

                            kind = RegistryValueKind.String;
                            value_str = valueComplex.Replace("\"", "");
                            value = value_str;

                            undifined_king = false;

                            #endregion STRINGS
                        }
                        else if (valueComplex.Trim().StartsWith("dword", StringComparison.OrdinalIgnoreCase) && valueComplex.IndexOf(":") > 0)
                        {
                            #region DWORD

                            kind = RegistryValueKind.DWord;
                            value_str = valueComplex.Substring(valueComplex.IndexOf(":") + 1);
                            int hexval = CHexTools.HexString2Int(value_str);
                            value = hexval;

                            undifined_king = false;

                            #endregion DWORD
                        }
                        else if (valueComplex.Trim().StartsWith("hex(b)", StringComparison.OrdinalIgnoreCase))
                        {
                            #region DWORD 64bit

                            kind = RegistryValueKind.QWord;
                            value_str = valueComplex.Substring(valueComplex.IndexOf(":") + 1);
                            string[] bytes_str = value_str.Split(new char[]{','}, StringSplitOptions.RemoveEmptyEntries);
                            value_str = "";
                            for(int i = bytes_str.Length - 1; i >= 0; i--)
                            {
                                value_str += bytes_str[i];
                            }
                            value = CHexTools.HexString2Int(value_str.Replace(",", ""));

                            undifined_king = false;

                            #endregion DWORD 64bit
                        }
                        else if (valueComplex.Trim().StartsWith("hex", StringComparison.OrdinalIgnoreCase))
                        {
                            #region Binary

                            kind = RegistryValueKind.Binary;
                            value_str = valueComplex.Substring(valueComplex.IndexOf(":") + 1);

                            string[] hex_strs = value_str.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                            byte[] bytes = CHexTools.HEXArrayToByteArray(string.Join("", hex_strs));
                            value = bytes;

                            undifined_king = false;

                            #endregion Binary
                        }
                        else if (valueComplex.Trim().StartsWith("hex(7)", StringComparison.OrdinalIgnoreCase))
                        {
                            #region MultiString

                            kind = RegistryValueKind.MultiString;
                            value_str = valueComplex.Substring(valueComplex.IndexOf(":") + 1);

                            throw new NotImplementedException();

                            undifined_king = false;

                            #endregion MultiString
                        }
                        else if (valueComplex.Trim().StartsWith("hex(2)", StringComparison.OrdinalIgnoreCase))
                        {
                            #region ExpandString

                            kind = RegistryValueKind.ExpandString;
                            value_str = valueComplex.Substring(valueComplex.IndexOf(":") + 1);

                            throw new NotImplementedException();

                            undifined_king = false;

                            #endregion ExpandString
                        }


                        if (!undifined_king)
                        {
                            currentKey.SetValue(name, value, kind);
                        }

                        #endregion VALUES
                    }
                    else if (line.Trim() == "")
                    {
                        // AZZERO AL PASSAGGIO DI UN'ALTRA CHIAVE

                        currentKeyName  = new string[0];
                        currentSubkeys  = new string[0];
                        currentKey      = null;
                    }
                }

                sr.Close();
                sr.Dispose();
            }
        }
    }
}
