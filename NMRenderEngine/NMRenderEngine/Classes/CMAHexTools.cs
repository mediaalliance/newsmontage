﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MediaAlliance.Tools.Hextools
{
    public class CHexTools
    {
        public static int HexString2Int(string S)
        {
            if(S.Length < 2) return 0;

            int partsCount = S.Length / 2;
            string[] parts = new string[partsCount];

            for (int i = 0, ichar = 0; ichar < S.Length; i++, ichar += 2)
            {
                parts[i] = S.Substring(ichar, 2);
            }

            long result = 0; 

            for (int i = 0; i < parts.Length; i++)
            {
                string part = parts[i];

                string CnvTbl = "0123456789ABCDEF";

                int ch1 = CnvTbl.IndexOf(part.Substring(0, 1));
                int ch2 = CnvTbl.IndexOf(part.Substring(1, 1));

                long current = (long)(ch1 * 16 + ch2);

                result |= current & 0xFF;
                if (i < parts.Length - 1) result <<= 8;
            }

            return (int)result;
        }

        

        public static byte HexString2Byte(string S)
        {
            return (byte)HexString2Int(S);
        }

        public static string ByteToHexString(byte[] bytes)
        {
            string hexString = "";
            for (int i = 0; i < bytes.Length; i++)
            {
                hexString += bytes[i].ToString("X2");
            }
            return hexString;
        }

        public static byte[] HEXArrayToByteArray(string HEXs)
        {
            int BSize = HEXs.Length / 2;

            byte[] result = new byte[BSize];

            for (int i = 0, c = 0; i < BSize; i++, c += 2)
            {
                string xx = HEXs.Substring((i * 2), 2);
                result[i] = HexString2Byte(xx);
            }

            return result;
        }

        public static bool IsBitSet(byte b, int pos)
        {
            return (b & (1 << pos)) != 0;
        }
    }
}
