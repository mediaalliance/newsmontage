﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("NewsMontage Render Engine")]
[assembly: AssemblyDescription("NewsMontage Render Engine")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Media Alliance S.r.l.")]
[assembly: AssemblyProduct("NMRenderEngine")]
[assembly: AssemblyCopyright("Copyright © Microsoft Media Alliance 2011")]
[assembly: AssemblyTrademark("Media Alliance S.r.l.")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("014cb963-6165-46c2-9c04-6ac84821b2c7")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.15")]
[assembly: AssemblyFileVersion("1.0.0.15")]
