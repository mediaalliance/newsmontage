﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Reflection;
using System.Threading;
using Microsoft.Win32;

using MediaAlliance.NewsMontage.RenderEngine.Render;
using MediaAlliance.Logger;
using System.Diagnostics;
using MediaAlliance.NewsMontage.RenderEngine.Classes;



namespace MediaAlliance.NewsMontage.RenderEngine
{
    class Program
    {
        private static ManualResetEvent mre         = new ManualResetEvent(false);
        private static string last_engine_message   = "";

        public static ILogger Logger                = null;
        public static string MyExeFilename          = "";

        public static string MyAppVersion = "";

        // FIELDS PARSED ARGUMENTS

        public static string ProjectSource    = "";
        private static string NoteInfo         = "";
        public static string ProfileXml = "";
        private static string RenderOutput     = "";

        private static bool ArgumentsError     = false;
        private static List<int> m_enabledTracks = new List<int>();       // If empty all tracks will be rendered



        static void Main(string[] args)
        {

            // /p "D:\! MyExtra\Controls\NewsMontage\TEST\Test_Projects\SA_20110510T101206\newnew.nmprj" /o "C:\NewsMontageOutput\Export_Test2\newnew2_16.mov" /i MOV_PAL_420_A24Bit
            // /pinfo OutputScanOrderInfo MOV_NTSC_420

            // INITIALIZE LOG

            if (!Directory.Exists("Log")) Directory.CreateDirectory("Log");

            Logger = CLoggerManager.instance().logInstance();

            MyExeFilename = System.Reflection.Assembly.GetEntryAssembly().CodeBase.Replace("file:///", "");

            //CMARegistryTools.ManualSetupReg("C:\\TestReg.reg");

            FileVersionInfo fv = FileVersionInfo.GetVersionInfo(MyExeFilename);

            try
            {
                RegistryKey swkey = Registry.LocalMachine.CreateSubKey("Software\\MediaAlliance\\NMRenderEngine");

                swkey.SetValue("path", MyExeFilename);
                swkey.SetValue("ver", fv.ProductVersion + " Build " + fv.FileBuildPart);

                swkey.Close();
                swkey.Dispose();
            }
            catch (System.Exception ex)
            {
            	
            }

            MyAppVersion = fv.FileVersion;


            Logger.info("===========================================================================");
            Logger.info(" NEWSMONTAGE RENDER ENGINE - NEW SESSION STARTED");
            Logger.info("   Software ver.   : " + MyAppVersion + " build " + fv.ProductBuildPart);
            Logger.info("   Company name    : " + fv.CompanyName);
            Logger.info("   Software path   : " + MyExeFilename);
            Logger.info("   MACHINE NAME    : " + Environment.MachineName);
            Logger.info("   DOMAIN NAME     : " + Environment.UserDomainName);
            Logger.info("   USER NAME       : " + Environment.UserName);
            Logger.info("   IS DEBUG VER    : " + fv.IsDebug);
            Logger.info("===========================================================================");

            Logger.info("");
            Logger.info(" Assembly loaded : ");
            Logger.info("");

            AppDomain MyDomain = AppDomain.CurrentDomain;

            Assembly[] AssembliesLoaded = MyDomain.GetAssemblies();

            foreach (Assembly MyAssembly in AssembliesLoaded)
            {
                Logger.info(" > " + MyAssembly.GetName().Name + " - [" + MyAssembly.GetName().Version + "]");
            }

            Logger.info("");


            Logger.info("");
            Logger.info(" Arguments : ");
            Logger.info("");
            Logger.info("   " + string.Join(" ", args));
            Logger.info("");


            CWriter.WriteLocalInfo(" ========================================= ");
            CWriter.WriteLocalInfo("  News Montage Render Task ");
            CWriter.WriteLocalInfo("  version : " + Assembly.GetExecutingAssembly().GetName().Version.ToString());
            CWriter.WriteLocalInfo(" ========================================= ");

            Environment.ExitCode = 0;


            CRender render_engine = null;

            argumentAction action = ParseArgumentAction(args);

            if (ArgumentsError)
            {
                CWriter.WriteError("Render cannot start");
                CWriter.WriteEndProcess("Render cannot start");
            }
            else
            {
                Logger.info("PARSED ACTION : " + action.ToString());

                switch (action)
                {
                    case argumentAction.Undefined:
                        CWriter.WriteError("Undefined action on arguments");
                        Environment.ExitCode = 1;
                        mre.Set();
                        break;
                    case argumentAction.RequestInfo:
                        mre.Set();
                        break;
                    case argumentAction.ProfileList:
                        mre.Set();
                        CWriter.WriteProfileList();
                        break;
                    case argumentAction.ProfileInfo:
                        mre.Set();
                        render_engine = new CRender();
                        string param = render_engine.RenderSettings.GetProfileInfo(ProfileXml, NoteInfo);
                        CWriter.WriteInfo(param);
                        break;
                    case argumentAction.Help:
                        CWriter.WriteHelp();
                        mre.Set();
                        break;
                    case argumentAction.Render:
                        render_engine = new CRender(ProjectSource, RenderOutput, ProfileXml, Logger, m_enabledTracks);

                        render_engine.OnStart       += new CRender.Render_OnStart(render_engine_OnStart);
                        render_engine.OnProgress    += new CRender.Render_OnProgress(render_engine_OnProgress);
                        render_engine.OnFinish      += new CRender.Render_OnFinish(render_engine_OnFinish);

                        render_engine.Render();

                        break;
                    default:
                        break;
                }

                mre.WaitOne();

                CWriter.WriteEndProcess("Finish");
            }
            
            if(render_engine != null) render_engine.Dispose();
        }




        #region RENDER EVENTS

        static void render_engine_OnStart(object Sender, string step_descrition)
        {
            CWriter.WriteInfo("Render started...");
        }

        static void render_engine_OnFinish(object Sender, string step_descrition)
        {
            mre.Set();
        }

        static void render_engine_OnProgress(object Sender, string step_descrition, int percent)
        {
            if (last_engine_message != step_descrition)
            {
                last_engine_message = step_descrition;
                CWriter.WriteInfo(step_descrition);
            }
            CWriter.WriteProgress(percent);
        }

        #endregion RENDER EVENTS




        enum argumentAction
        {
            Undefined,
            RequestInfo,
            ProfileList,
            ProfileInfo,
            Help,
            Render
        }

        static argumentAction ParseArgumentAction(string[] args)
        {
            string joined = string.Join(" ", args);

            if (joined.IndexOf("-h", StringComparison.OrdinalIgnoreCase) >= 0 ||
                joined.IndexOf("/h", StringComparison.OrdinalIgnoreCase) >= 0)
            {
                return argumentAction.Help;
            }

            if (joined.IndexOf("-?", StringComparison.OrdinalIgnoreCase) >= 0 ||
                joined.IndexOf("/?", StringComparison.OrdinalIgnoreCase) >= 0)
            {
                return argumentAction.Help;
            }

            if (joined.IndexOf("-profiles", StringComparison.OrdinalIgnoreCase) >= 0 ||
                joined.IndexOf("/profiles", StringComparison.OrdinalIgnoreCase) >= 0)
            {
                return argumentAction.ProfileList;
            }

            for (int i = 0; i < args.Length; i++)
            {
                if ((args[i].Equals("/pinfo", StringComparison.OrdinalIgnoreCase) ||
                     args[i].Equals("-pinfo", StringComparison.OrdinalIgnoreCase)) &&
                     args.Length > i + 1)
                {
                    NoteInfo = args[i + 1];
                    ProfileXml = args[i + 2];

                    return argumentAction.ProfileInfo;
                }
            }

            for (int i = 0; i < args.Length; i++)
            {
                if ((args[i].Equals("/p", StringComparison.OrdinalIgnoreCase) || 
                     args[i].Equals("-p", StringComparison.OrdinalIgnoreCase)) &&
                     args.Length > i)
                {
                    ProjectSource = args[i + 1];
                }
            }

            for (int i = 0; i < args.Length; i++)
            {
                if ((args[i].Equals("/o", StringComparison.OrdinalIgnoreCase) ||
                     args[i].Equals("-o", StringComparison.OrdinalIgnoreCase)) &&
                     args.Length > i)
                {
                    RenderOutput = args[i + 1];
                }
            }

            for (int i = 0; i < args.Length; i++)
            {
                if ((args[i].Equals("/i", StringComparison.OrdinalIgnoreCase) ||
                     args[i].Equals("-i", StringComparison.OrdinalIgnoreCase)) &&
                     args.Length > i)
                {
                    ProfileXml = args[i + 1];
                }
            }

            for (int i = 0; i < args.Length; i++)
            {
                if ((args[i].Equals("/t", StringComparison.OrdinalIgnoreCase) ||
                     args[i].Equals("-t", StringComparison.OrdinalIgnoreCase)) &&
                     args.Length > i)
                {
                    string tlist = args[i + 1];

                    string[] tlist_parts = tlist.Split(new char[] { ',', ';', '-' }, StringSplitOptions.RemoveEmptyEntries);

                    m_enabledTracks.Clear();

                    foreach (string part in tlist_parts)
                    {
                        int tid = -1;
                        int.TryParse(part, out tid);
                        if (tid >= 0) m_enabledTracks.Add(tid);
                    }
                }
            }

            bool error = false;

            if (ProjectSource.Trim() == "")
            {
                Logger.error("Source project not specified");
                CWriter.WriteError("No source project specified");
                error = true;
            }
            else
            {
                if (!File.Exists(ProjectSource))
                {
                    Logger.error("Source project not found");
                    CWriter.WriteError("Project file not found");
                    error = true;
                }
            }

            if (RenderOutput.Trim() == "")
            {
                Logger.error("No output file specified");
                CWriter.WriteError("No render output file specified");
                error = true;
            }
            else
            {
                int counter = 1;

                string filename = RenderOutput;

                if (File.Exists(filename))
                {
                    while (File.Exists(filename))
                    {
                        filename = RenderOutput.Replace(Path.GetExtension(RenderOutput), " (BAK " + counter.ToString().PadLeft(3, '0') + ")" + Path.GetExtension(RenderOutput));
                        counter++;
                    }

                    File.Move(RenderOutput, filename);

                    CWriter.WriteInfo("Backup existing output file to : " + filename);

                    Logger.info("Destination render output exist : " + RenderOutput);
                    Logger.info("   > Backup to : " + filename);
                }
            }

            if (error)
            {
                ArgumentsError = true;
                Logger.error("Argument parsed with errors");
            }


            return argumentAction.Render;
        }
    }
}
