﻿=============================================================
Media Alliance NewsMontage RenderEngine 1.0.0.15- 2011-12-20
=============================================================

[Install Instruction]
- Replace NmRenderEngine.vshost.exe and the modules updated listed below in the application's folder.

[What's new]
- Fixed  : Audio slow on exporting Voice Track Audio-Only.


[Modules updated] 
FFMpegHelper.dll                    Ver:[1.1.0.9       ]  Created:[2011-04-30T00:15:41]
audioHelper.dll                     Ver:[1.0.2.7       ]  Created:[2011-04-30T00:15:41]
MARenderer.dll                      Ver:[1.0.1.5       ]  Created:[2011-04-30T00:15:41]


=============================================================
Media Alliance NewsMontage RenderEngine 1.0.0.13- 2011-11-xx
=============================================================

[Install Instruction]
- Replace NmRenderEngine.vshost.exe and the modules updated listed below in the application's folder.

[What's new]
- Added  : Wave information

[Modules updated] 
audioHelper.dll                     Ver:[1.0.2.5       ]  Created:[2011-04-30T00:15:41]
MAMuxEncoder.dll                    Ver:[1.0.0.6       ]  Created:[2011-04-30T00:15:41]
MARenderer.dll                      Ver:[1.0.1.3       ]  Created:[2011-04-30T00:15:41]


=============================================================
Media Alliance NewsMontage RenderEngine 1.0.0.11- 2011-07-26
=============================================================

[Install Instruction]
- Replace NmRenderEngine.vshost.exe and the modules updated listed below in the application's folder.

[What's new]
- Added  : Profiles info request

[Modules updated] 
audioHelper.dll                     Ver:[1.0.2.5       ]  Created:[2011-04-30T00:15:41]
MAMuxEncoder.dll                    Ver:[1.0.0.6       ]  Created:[2011-04-30T00:15:41]
MARenderer.dll                      Ver:[1.0.1.3       ]  Created:[2011-04-30T00:15:41]

=============================================================
Media Alliance NewsMontage RenderEngine 1.0.0.10- 2011-05-27
=============================================================

[Install Instruction]
- Replace NmRenderEngine.vshost.exe and the modules updated listed below in the application's folder.

[What's new]
- Fixed : Some issues on audio export "16 on 24bit" profiles

[Modules updated] 
audioHelper.dll                     Ver:[1.0.2.4       ]  Created:[2011-04-30T00:15:41]
FFMpegHelper.dll                    Ver:[1.1.0.7       ]  Created:[2011-04-30T00:15:41]
MAMuxEncoder.dll                    Ver:[1.0.0.5       ]  Created:[2011-04-30T00:15:41]
MARenderer.dll                      Ver:[1.0.1.2       ]  Created:[2011-04-30T00:15:41]



=============================================================
Media Alliance NewsMontage RenderEngine 1.0.0.9 - 2011-05-26
=============================================================

[Install Instruction]
- Replace NmRenderEngine.vshost.exe and the modules updated listed below in the application's folder.

[What's new]
- Improved : Audio mixing (peaks cliping issues)

[Modules updated] 
audioHelper.dll                     Ver:[1.0.2.3       ]  Created:[2011-04-30T00:15:41]
MAMuxEncoder.dll
FFMpegHelper.dll
audioHelper.dll
MARenderer.dll
Profiles\MOV_PAL_420_A24Bit.xml
Profiles\WAV _A24Bit.xml


=============================================================
Media Alliance NewsMontage RenderEngine 1.0.0.8 - 2011-05-24
=============================================================

[Install Instruction]
- Replace NmRenderEngine.vshost.exe and the modules updated listed below in the application's folder.

[What's new]
- Added  : Profile and to export audio 24bit

[Modules updated] 
audioHelper.dll                     Ver:[1.0.2.0       ]  Created:[2011-04-30T00:15:41]
MAMuxEncoder.dll
FFMpegHelper.dll
audioHelper.dll
MARenderer.dll
Profiles\MOV_PAL_420_A24Bit.xml
Profiles\WAV _A24Bit.xml


=============================================================
Media Alliance NewsMontage RenderEngine 1.0.0.5 - 2011-05-16
=============================================================

[Install Instruction]
- Replace NmRenderEngine.vshost.exe and the modules updated listed below in the application's folder.

[What's new]
- Added  : Possibility to specified list of track to render
- Added  : RenderMode on profile (audioVideo, audioOnly, videoOnly)

[Modules updated] 
audioHelper.dll                     Ver:[1.0.2.0       ]  Created:[2011-04-30T00:15:41]
Profiles\WAV.xml

=============================================================
Media Alliance NewsMontage RenderEngine 1.0.0.4 - 2011-05-03
=============================================================

[Install Instruction]
- Replace NmRenderEngine.vshost.exe and the modules updated listed below in the application's folder.

[What's new]
- Fixed : Video production of NTSC project
- Fixed : Disabled audio and video tracks are rendered anyway

[Modules updated]
audioHelper.dll                     Ver:[1.0.1.1       ]  Created:[2011-04-30T00:15:41] 

=============================================================
Media Alliance NewsMontage RenderEngine 1.0.0.3 - 2011-05-03
=============================================================

[Install Instruction]
- Replace NmRenderEngine.vshost.exe and the modules updated listed below in the application's folder.

[What's new]
- Added : This new version doesn't request UAC disabled on machine where RenderEngine will work. 
- Fixed : Backup filename when render to existing output file

[Modules updated]
MAMuxEncoder.dll                    Ver:[1.0.0.2       ]  Created:[2011-04-30T00:15:41]

=============================================================
Media Alliance NewsMontage RenderEngine 1.0.0.2 - 2011-05-02
=============================================================

[Install Instruction]
- First released version, install it by NMRenderEngine_Setup_1.0.0.2.exe

[What's new]
- 

[Modules updated]
