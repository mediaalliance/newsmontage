﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Security.Permissions;

using System.Diagnostics;
using System.Threading;
using System.Runtime.InteropServices;

[assembly:PermissionSetAttribute(SecurityAction.RequestMinimum, Name = "FullTrust")]


//ffmpeg -i "C:\DEVELOP\MediaAlliance\DEMO MXF\adri_DVCPRO50_2.mxf" -y -sameq -ss 25 -T 2 -s 100x80 -f image2 -r 1/5 .\frames\nome%03d.png

namespace FFMpegHelper
{

    public class MuxerAviToM2V
    {
        private Process m_process = null;


        public int Start(string inVideoFile, string outVideoFile)
        {
            int ret = 0;

            m_process = new Process();
            m_process.StartInfo.FileName = Application.StartupPath + @"\ffmpeg.exe";


            //m_process.StartInfo.Arguments = string.Format(@"-i ""{0}"" -pix_fmt yuv422p -vcodec mpeg2video -flags +ildct+ilme -top 1 -dc 10 -flags2 +ivlc+non_linear_q -qmin 1 -lmin '1*QP2LAMBDA' -vtag xd5c -rc_max_vbv_use 1 -rc_min_vbv_use 1 -b 30000k -minrate 30000k -maxrate 30000k -bufsize 1200000 -bf 2  -an -y ""{1}""", inVideoFile, outVideoFile);
            m_process.StartInfo.Arguments = string.Format(@"-i ""{0}"" -vcodec mpeg2video -r 25 -pix_fmt yuv422p  -minrate 30000k -maxrate 30000k -b 30000k -intra -flags +ildct+low_delay -dc 10 -flags2 +ivlc+non_linear_q -ps 1 -qmin 1 -qmax 3 -top 1 -bufsize 1200000 -rc_init_occupancy 1200000 -rc_buf_aggressivity 0.25 -an -padtop 32 -y ""{1}""", inVideoFile, outVideoFile);

            
            //m_process.StartInfo.Arguments = string.Format(@"-i ""{0}"" -vcodec mpeg2video -r 25 -pix_fmt yuv422p  -minrate 30000k -maxrate 30000k -b 30000k -intra -flags +ildct+low_delay -dc 10 -flags2 +ivlc+non_linear_q -ps 1 -qmin 1 -qmax 3 -top 1 -bufsize 1200000 -rc_init_occupancy 1200000 -rc_buf_aggressivity 0.25 -an -padtop 32 -y ""{1}""", inVideoFile, outVideoFile);
            //m_process.StartInfo.Arguments = string.Format(@"-i ""{0}"" -vcodec copy {1} -flags +ildct+ilme -top 1 -dc 10 -flags2 +ivlc+non_linear_q -qmin 1 -lmin '1*QP2LAMBDA' -vtag xd5c -rc_max_vbv_use 1 -rc_min_vbv_use 1 -b 50000k -minrate 50000k -maxrate 50000k -bufsize 36408333 -bf 2  -acodec copy -f mov ""{2}""", inVideoFile, strAudios, outVideoFile);


            m_process.StartInfo.CreateNoWindow = true;
            m_process.StartInfo.UseShellExecute = false;
            m_process.StartInfo.RedirectStandardOutput = false;
            m_process.StartInfo.RedirectStandardError = false;
            m_process.StartInfo.WorkingDirectory = ".\\";

            bool started = true;
            try
            {
                m_process.Start();
                m_process.WaitForExit();
                ret = m_process.ExitCode;

            }
            catch (Exception ex)
            {
                ret = -1;
            }
            finally
            {
                if (m_process != null)
                {
                    m_process.Close();
                    m_process.Dispose();
                    m_process = null;
                }

            }

            return ret;
        }

        public void Stop()
        {
            if (m_process != null)
            {
                m_process.Kill();
                m_process.Close();
                m_process = null;
            }
        }
    }

    public class MuxerM2vToMovWithAudios
    {
        private Process m_process = null;

        public int Start(string inVideoFile, string[] audioFiles, string outVideoFile)
        {
            int ret = 0;

            m_process = new Process();
            m_process.StartInfo.FileName = Application.StartupPath + @"\ffmpeg.exe";


            string strAudios = "";
            string strNewAudios = "";
            bool bFirst = true;
            foreach (string audioFileName in audioFiles)
            {
                strAudios += " -i \"" + audioFileName + "\"";

                if (bFirst)
                    bFirst = false;
                else
                    strNewAudios += " -acodec pcm_s16le -newaudio";

            }




            m_process.StartInfo.Arguments = string.Format(@"-i ""{0}"" -vcodec mpeg2video  -vtag mx3p -vbsf imxdump {1} -acodec pcm_s16le -y ""{2}""  {3}", inVideoFile, strAudios, outVideoFile, strNewAudios);
            //m_process.StartInfo.Arguments = string.Format(@"-i ""{0}"" -vcodec copy {1} -flags +ildct+ilme -top 1 -dc 10 -flags2 +ivlc+non_linear_q -qmin 1 -lmin '1*QP2LAMBDA' -vtag xd5c -rc_max_vbv_use 1 -rc_min_vbv_use 1 -b 50000k -minrate 50000k -maxrate 50000k -bufsize 36408333 -bf 2  -acodec copy -f mov ""{2}""", inVideoFile, strAudios, outVideoFile);


            m_process.StartInfo.CreateNoWindow = true;
            m_process.StartInfo.UseShellExecute = false;
            m_process.StartInfo.RedirectStandardOutput = false;
            m_process.StartInfo.RedirectStandardError = false;
            m_process.StartInfo.WorkingDirectory = ".\\";

            bool started = true;
            try
            {
                m_process.Start();
                m_process.WaitForExit();
                ret = m_process.ExitCode;

            }
            catch (Exception ex)
            {
                ret = -1;
            }
            finally
            {
                if (m_process != null)
                {
                    m_process.Close();
                    m_process.Dispose();
                    m_process = null;
                }

            }

            return ret;
        }

        public void Stop()
        {
            if (m_process != null)
            {
                m_process.Kill();
                m_process.Close();
                m_process = null;
            }
        }
    }

    public class Muxer
    {
        private Process m_process = null;

        public int Start(string inVideoFile, string[] audioFiles, string outVideoFile, int AudioBPS)
        {
            int ret = 0;

            m_process = new Process();
            m_process.StartInfo.FileName = Application.StartupPath + @"\ffmpeg.exe";


            string strAudios    = "";
            string strNewAudios = "";
            string audio_frm    = "pcm_s" + AudioBPS + "le";


            bool bFirst = true;

            foreach (string audioFileName in audioFiles)
            {
                strAudios += " -i \"" + audioFileName + "\"";

                if (bFirst)
                {
                    bFirst = false;
                }
                else
                {
                    strNewAudios += " -acodec " + audio_frm + " -newaudio";      //" -acodec pcm_s16le -newaudio";
                }
            }



            m_process.StartInfo.Arguments = string.Format(@"-i ""{0}"" -pix_fmt yuv420p -vcodec copy {1} -acodec {2} -y ""{3}""  {4}", 
                                                            inVideoFile, 
                                                            strAudios, 
                                                            audio_frm,
                                                            outVideoFile, 
                                                            strNewAudios);
            //m_process.StartInfo.Arguments = string.Format(@"-i ""{0}"" -vcodec copy {1} -flags +ildct+ilme -top 1 -dc 10 -flags2 +ivlc+non_linear_q -qmin 1 -lmin '1*QP2LAMBDA' -vtag xd5c -rc_max_vbv_use 1 -rc_min_vbv_use 1 -b 50000k -minrate 50000k -maxrate 50000k -bufsize 36408333 -bf 2  -acodec copy -f mov ""{2}""", inVideoFile, strAudios, outVideoFile);


            m_process.StartInfo.CreateNoWindow = true;
            m_process.StartInfo.UseShellExecute = false;
            m_process.StartInfo.RedirectStandardOutput = false;
            m_process.StartInfo.RedirectStandardError = false;
            m_process.StartInfo.WorkingDirectory = ".\\";

            bool started = true;
            try
            {
                m_process.Start();
                m_process.WaitForExit();
                ret = m_process.ExitCode;

            }
            catch (Exception ex)
            {
                ret = -1;
            }
            finally
            {
                if (m_process != null)
                {
                    m_process.Close();
                    m_process.Dispose();
                    m_process = null;
                }

            }

            return ret;
        }

        public void Stop()
        {
            if (m_process != null)
            {
                m_process.Kill();
                m_process.Close();
                m_process = null;
            }
        }
    }

    public class Muxer2_mxfdv
    {
        private Process m_process = null;

        public int Start(string inVideoFile, string[] audioFiles, string outVideoFile, int AudioBPS)
        {
            int ret = 0;

            m_process = new Process();
            m_process.StartInfo.FileName = Application.StartupPath + @"\ffmpeg.exe";


            string strAudios = "";
            string strNewAudios = "";
            string audio_frm = "pcm_s" + AudioBPS + "le";


            bool bFirst = true;

            foreach (string audioFileName in audioFiles)
            {
                strAudios += " -i \"" + audioFileName + "\"";

                if (bFirst)
                {
                    bFirst = false;
                }
                else
                {
                    strNewAudios += " -acodec " + audio_frm + " -newaudio";      //" -acodec pcm_s16le -newaudio";
                }
            }



            m_process.StartInfo.Arguments = string.Format(@"-i ""{0}"" -target dv {1} -acodec {2} -y ""{3}""  {4}",
                                                            inVideoFile,
                                                            strAudios,
                                                            audio_frm,
                                                            outVideoFile,
                                                            strNewAudios);
            //m_process.StartInfo.Arguments = string.Format(@"-i ""{0}"" -vcodec copy {1} -flags +ildct+ilme -top 1 -dc 10 -flags2 +ivlc+non_linear_q -qmin 1 -lmin '1*QP2LAMBDA' -vtag xd5c -rc_max_vbv_use 1 -rc_min_vbv_use 1 -b 50000k -minrate 50000k -maxrate 50000k -bufsize 36408333 -bf 2  -acodec copy -f mov ""{2}""", inVideoFile, strAudios, outVideoFile);


            m_process.StartInfo.CreateNoWindow = true;
            m_process.StartInfo.UseShellExecute = false;
            m_process.StartInfo.RedirectStandardOutput = false;
            m_process.StartInfo.RedirectStandardError = false;
            m_process.StartInfo.WorkingDirectory = ".\\";

            bool started = true;
            try
            {
                m_process.Start();
                m_process.WaitForExit();
                ret = m_process.ExitCode;

            }
            catch (Exception ex)
            {
                ret = -1;
            }
            finally
            {
                if (m_process != null)
                {
                    m_process.Close();
                    m_process.Dispose();
                    m_process = null;
                }

            }

            return ret;
        }

        public void Stop()
        {
            if (m_process != null)
            {
                m_process.Kill();
                m_process.Close();
                m_process = null;
            }
        }
    }

    public class WavResampling
    {
        private Process m_process = null;


        public int Start(string wave16, string wave24, int newBitPerSecond)
        {
            return Start(wave16, wave24, newBitPerSecond, 1);
        }

        public int Start(string wave16, string wave24, int newBitPerSecond, int chNumber)
        {
            // Ex. -i wave16.wav -vn -acodec pcm_s24le -ar 48000 -ac 1 -f wav wave24.WAV

            int ret = 0;

            m_process = new Process();
            m_process.StartInfo.FileName = Application.StartupPath + @"\ffmpeg.exe";
            string m_outfrm = "pcm_s" + newBitPerSecond + "le";

            m_process.StartInfo.Arguments = string.Format(@"-i ""{0}"" -vn -acodec {1} -ar 48000 -ac {2} -f wav ""{3}""", 
                                                            wave16.Replace("\"", ""),
                                                            m_outfrm,
                                                            chNumber.ToString(),
                                                            wave24.Replace("\"", ""));


            m_process.StartInfo.CreateNoWindow = true;
            m_process.StartInfo.UseShellExecute = false;
            m_process.StartInfo.RedirectStandardOutput = false;
            m_process.StartInfo.RedirectStandardError = false;
            m_process.StartInfo.WorkingDirectory = ".\\";

            bool started = true;
            try
            {
                m_process.Start();
                m_process.WaitForExit();
                ret = m_process.ExitCode;

            }
            catch (Exception ex)
            {
                ret = -1;
            }
            finally
            {
                if (m_process != null)
                {
                    m_process.Close();
                    m_process.Dispose();
                    m_process = null;
                }

            }

            return ret;
        }

        public void Stop()
        {
            if (m_process != null)
            {
                m_process.Kill();
                m_process.Close();
                m_process = null;
            }
        }
    }


    public class Converter
    {
        private Process m_process = null;

        public int Start(string aviFile, string dvFile)
        {
            int ret = 0;

            m_process = new Process();
            m_process.StartInfo.FileName = Application.StartupPath + @"\ffmpeg.exe";
            //m_process.StartInfo.Arguments = string.Format(@"-i ""{0}"" -pix_fmt yuv422p ""{1}""", aviFile, dvFile);
            m_process.StartInfo.Arguments = string.Format(@"-i ""{0}"" -vcodec copy ""{1}""", aviFile, dvFile);
            m_process.StartInfo.CreateNoWindow = true;
            m_process.StartInfo.UseShellExecute = false;
            m_process.StartInfo.RedirectStandardOutput = false;
            m_process.StartInfo.RedirectStandardError = false;
            m_process.StartInfo.WorkingDirectory = ".\\";

            bool started = true;
            try
            {
                m_process.Start();
                m_process.WaitForExit();
                ret = m_process.ExitCode;

            }
            catch (Exception ex)
            {
                ret = -1;
            }
            finally
            {
                if(m_process != null)
                {
                    m_process.Close();
                    m_process.Dispose();
                    m_process = null;
                }

            }

            return ret;
        }

        public void Stop()
        {
            if (m_process != null)
            {
                m_process.Kill();
                m_process.Close();
                m_process = null;
            }
        }
    }


    public class SceneChange
    {
        private Process m_process = null;

        public SceneChange()
        {
            m_process = new Process();
        }

        public int Start(string srcFile, long startSecs, long durSecs, string destFolder)
        {
            int ret = 0;

            //ffmpeg -i "C:\DEVELOP\MediaAlliance\DEMO MXF\adri_DVCPRO50_2.mxf" -y -sameq -ss 25 -T 2 -s 100x80 -f image2 -r 1/5 .\frames\nome%03d.png

            try
            {                
                m_process.StartInfo.FileName = Application.StartupPath + @"\ffmpeg.exe";
                //m_process.StartInfo.Arguments = string.Format(@"-ss {1} -t {2} -i ""{0}"" -y -sameq  -s 80x60 -r 1/5 -f image2  ""{3}\frame%06d.jpg""",
                //                                               srcFile, startSecs, durSecs, destFolder);
                m_process.StartInfo.Arguments = string.Format(@"-ss {1} -t {2} -i ""{0}"" -y -sameq  -s 80x60 -sameq -f image2  ""{3}\frame{1}_%06d.jpg""",
                                                                srcFile, startSecs, durSecs, destFolder);

                m_process.StartInfo.CreateNoWindow = true;
                m_process.StartInfo.UseShellExecute = false;
                m_process.StartInfo.RedirectStandardOutput = false;
                m_process.StartInfo.RedirectStandardError = false;
                m_process.StartInfo.WorkingDirectory = ".\\";

                m_process.Start();

                Debug.WriteLine("SceneChange start for file:" + srcFile);
                

            }
            catch (Exception ex)
            {
                ret = -1;
            }
            finally
            {
            }

            return ret;
        }

        public int StartAndWait(string srcFile, long startSecs, long durSecs, string destFolder)
        {
            int ret = 0;

            //ffmpeg -i "C:\DEVELOP\MediaAlliance\DEMO MXF\adri_DVCPRO50_2.mxf" -y -sameq -ss 25 -T 2 -s 100x80 -f image2 -r 1/5 .\frames\nome%03d.png

            try
            {
                m_process.StartInfo.FileName = Application.StartupPath + @"\ffmpeg.exe";
                //m_process.StartInfo.Arguments = string.Format(@"-ss {1} -t {2} -i ""{0}"" -y -sameq  -s 80x60 -r 1/5 -f image2  ""{3}\frame%06d.jpg""",
                //                                               srcFile, startSecs, durSecs, destFolder);
                m_process.StartInfo.Arguments = string.Format(@"-ss {1} -t {2} -i ""{0}"" -y -s 80x60  -r 1/5 -f image2  ""{3}\frame_sec{1}_%06d.jpg""",
                                                                srcFile, startSecs, durSecs, destFolder);

                string fileToCheck = string.Format("{0}\\frame_sec{1}_{2:#000000}.jpg", destFolder, startSecs, durSecs);
                if (File.Exists(fileToCheck))
                    return 0;

                m_process.StartInfo.CreateNoWindow = true;
                m_process.StartInfo.UseShellExecute = false;
                m_process.StartInfo.RedirectStandardOutput = false;
                m_process.StartInfo.RedirectStandardError = false;
                m_process.StartInfo.WorkingDirectory = ".\\";

                m_process.Start();
                if (!m_process.WaitForExit(10000))
                    ret = -1;

                Debug.WriteLine("SceneChange start for file:" + srcFile);
                //CLog.FlushLog();


            }
            catch (Exception ex)
            {
                ret = -1;
            }
            finally
            {
            }

            return ret;
        }

        //public int StartAndWait(string srcFile, float startSecs, long durSecs, string destFolder)
        //{
        //    int ret = 0;

        //    //ffmpeg -i "C:\DEVELOP\MediaAlliance\DEMO MXF\adri_DVCPRO50_2.mxf" -y -sameq -ss 25 -T 2 -s 100x80 -f image2 -r 1/5 .\frames\nome%03d.png

        //    try
        //    {
        //        m_process.StartInfo.FileName = Application.StartupPath + @"\ffmpeg.exe";
        //        //m_process.StartInfo.Arguments = string.Format(@"-ss {1} -t {2} -i ""{0}"" -y -sameq  -s 80x60 -r 1/5 -f image2  ""{3}\frame%06d.jpg""",
        //        //                                               srcFile, startSecs, durSecs, destFolder);
        //        m_process.StartInfo.Arguments = string.Format(@"-ss {1} -t {2} -i ""{0}"" -y -sameq  -s 80x60 -r 1/5 -f image2  ""{3}\frame_sec{1}_%06d.jpg""",
        //                                                        srcFile, startSecs.ToString("N2").Replace(".",","), durSecs, destFolder);

        //        m_process.StartInfo.CreateNoWindow = true;
        //        m_process.StartInfo.UseShellExecute = false;
        //        m_process.StartInfo.RedirectStandardOutput = false;
        //        m_process.StartInfo.RedirectStandardError = false;
        //        m_process.StartInfo.WorkingDirectory = ".\\";

        //        m_process.Start();
        //        if (!m_process.WaitForExit(10000))
        //            ret = -1;

        //        CLog.Log("SceneChange start for file:" + srcFile);
        //        CLog.FlushLog();


        //    }
        //    catch (Exception ex)
        //    {
        //        ret = -1;
        //    }
        //    finally
        //    {
        //    }

        //    return ret;
        //}

        public void Stop()
        {
            try
            {
                if (m_process != null)
                {
                    m_process.Kill();
                    m_process.Close();
                    m_process = null;
                }
            }
            catch { }
        }
 
    }


    public class AudioExtract
    {
        private Process m_process = null;

        private int m_samplerate;
        private int m_out_channels;
        private int m_bitPerSample;
        private int m_bitrate;


        public AudioExtract()
        {
            m_samplerate = 48000;
            m_out_channels = 2;
            m_bitPerSample = 16;

            m_bitrate = m_samplerate * m_bitPerSample * m_out_channels;
        }

        public AudioExtract(int samplerate, int channels, int bitPerSample)
        {
            m_samplerate = samplerate;
            m_out_channels = channels;
            m_bitPerSample = bitPerSample;

            m_bitrate = m_samplerate * m_bitPerSample * m_out_channels;
        }

        private string ConvertMs2Sec(ulong ms)
        {
            float res = (float)ms / 1000.0f;
            return res.ToString().Replace(",", ".");
        }


        //public string GetOutFileName(int idClip, int idColor, int trackIndex, string destFolder)
        //{
        //    string outFileName = string.Format("originalaudio_{0}_{1}_{2}.wav", idClip, idColor, trackIndex);

        //    return projectHelper.clsUtil.addPath(destFolder, outFileName);
        //}


        public int Start(string srcFile, ulong startMs, ulong durMs, string outFileName, int trackIndex)
        {
            int ret = 0;
            //ffmpeg -i adri_DVCPRO50_2.mxf -ss 20 -t 5 -ab 96000 -ar 48000 -map 0.1 -ac 2 test2_stereo.wav


            // out file with postixed idClip(timeline), idcolor and track index number 1-based (tipically 1-4)


            //string Arguments = string.Format(@"-i ""{0}"" -ss {1} -t {2} -ab 96000 -ar {3} -map 0.{4} -ac {5} -acodec pcm_s16le {6}",
            //                                    srcFile, ConvertMs2Sec(startMs), ConvertMs2Sec(durMs), m_samplerate, trackIndex,
            //                                    m_out_channels, outFileName);

            //string Arguments = string.Format(@"-acodec pcm_s24le -acodec pcm_s24le -acodec pcm_s24le -acodec pcm_s24le -acodec pcm_s24le -acodec pcm_s24le -acodec pcm_s24le -acodec pcm_s24le -ac 8 -ss {1} -t {2} -i ""{0}"" -ab {3} -ar {4} -ac {5} -acodec pcm_s16le -acodec pcm_s16le -acodec pcm_s16le -acodec pcm_s16le ""{6}""",
            //                                         srcFile, ConvertMs2Sec(startMs), ConvertMs2Sec(durMs), m_bitrate, m_samplerate, m_out_channels, outFileName);
            string Arguments = string.Format(@"-ss {1} -t {2} -i ""{0}"" -ab {3} -ar {4} -ac {5} -acodec pcm_s16le ""{6}""",
                                                                srcFile, 
                                                                ConvertMs2Sec(startMs), 
                                                                ConvertMs2Sec(durMs), 
                                                                m_bitrate, 
                                                                m_samplerate, 
                                                                m_out_channels, 
                                                                outFileName);


            try
            {
                if (File.Exists(outFileName))
                    File.Delete (outFileName);

                
                m_process = new Process();
                m_process.StartInfo.FileName = Application.StartupPath + @"\ffmpeg.exe";
                m_process.StartInfo.Arguments = Arguments;
                m_process.StartInfo.CreateNoWindow = true;
                m_process.StartInfo.UseShellExecute = false;
                m_process.StartInfo.RedirectStandardOutput = false;
                m_process.StartInfo.RedirectStandardError = false;
                m_process.StartInfo.WorkingDirectory = ".\\";

                bool started = true;
                try
                {
                    m_process.Start();
                    m_process.WaitForExit();
                    ret = m_process.ExitCode;

                }
                catch (Exception ex)
                {
                    ret = -1;                    
                }
                finally
                {
                    if (m_process != null)
                    {
                        m_process.Close();
                        m_process.Dispose();
                        m_process = null;
                    }

                }
            

            }
            catch (Exception ex)
            {
                ret = -1;
            }
            finally
            {
            }

            return ret;
        }
        
 /*    old start    
        public int Start(string srcFile, int startSecs, int durSecs, int startMilliSecs, int durMillisecs, string destFolder, int idTrack)
        {
            int ret = 0;

            //ffmpeg -i "C:\DEVELOP\MediaAlliance\DEMO MXF\adri_DVCPRO50_2.mxf" -y -sameq -ss 25 -T 2 -s 100x80 -f image2 -r 1/5 .\frames\nome%03d.png

            try
            {
                
                m_process = new Process();
                m_process.StartInfo.FileName = Application.StartupPath + @"\ffmpeg.exe";
                m_process.StartInfo.Arguments = string.Format(@"-i ""{0}"" -ss {1}.{5} -t {2}.{6} -map 0:{3}  ""{4}""",
                                                              srcFile, startSecs, durSecs, idTrack, destFolder, startMilliSecs, durMillisecs);
                m_process.StartInfo.CreateNoWindow = true;
                m_process.StartInfo.UseShellExecute = false;
                m_process.StartInfo.RedirectStandardOutput = false;
                m_process.StartInfo.RedirectStandardError = false;
                m_process.StartInfo.WorkingDirectory = ".\\";

                bool started = true;
                try
                {
                    m_process.Start();
                    m_process.WaitForExit();
                    ret = m_process.ExitCode;

                }
                catch (Exception ex)
                {
                    ret = -1;                    
                }
                finally
                {
                    if (m_process != null)
                    {
                        m_process.Close();
                        m_process.Dispose();
                        m_process = null;
                    }

                }
            

            }
            catch (Exception ex)
            {
                ret = -1;
            }
            finally
            {
            }

            return ret;
        }
    */


        public void Stop()
        {
            try
            {
                if (m_process != null)
                {
                    m_process.Kill();
                    m_process.Close();
                    m_process = null;
                }
            }
            catch { }
        }

    }

    public class AudioMonoExtract
    {
        private Process m_process = null;

        private int m_samplerate;
        private int m_out_channels;
        private int m_bitPerSample;
        private int m_bitrate;


        public AudioMonoExtract()
        {
            m_samplerate = 48000;
            m_out_channels = 2;
            m_bitPerSample = 16;

            m_bitrate = m_samplerate * m_bitPerSample * m_out_channels;
        }

        public AudioMonoExtract(int samplerate, int bitPerSample)
        {
            m_samplerate = samplerate;
            m_out_channels = 1;
            m_bitPerSample = bitPerSample;

            m_bitrate = m_samplerate * m_bitPerSample * m_out_channels;
        }

        private string ConvertMs2Sec(ulong ms)
        {
            float res = (float)ms / 1000.0f;
            return res.ToString().Replace(",", ".");
        }



        public int Start(string srcFile, ulong startMs, ulong durMs, string outFileName, int trackIndex)
        {
            int ret = 0;
            //ffmpeg -i adri_DVCPRO50_2.mxf -ss 20 -t 5 -ab 96000 -ar 48000 -map 0.1 -ac 2 test2_stereo.wav


            // out file with postixed idClip(timeline), idcolor and track index number 1-based (tipically 1-4)


            //string Arguments = string.Format(@"-i ""{0}"" -ss {1} -t {2} -ab 96000 -ar {3} -map 0.{4} -ac {5} -acodec pcm_s16le {6}",
            //                                    srcFile, ConvertMs2Sec(startMs), ConvertMs2Sec(durMs), m_samplerate, trackIndex,
            //                                    m_out_channels, outFileName);

            //string Arguments = string.Format(@"-acodec pcm_s24le -acodec pcm_s24le -acodec pcm_s24le -acodec pcm_s24le -acodec pcm_s24le -acodec pcm_s24le -acodec pcm_s24le -acodec pcm_s24le -ac 8 -ss {1} -t {2} -i ""{0}"" -ab {3} -ar {4} -ac {5} -acodec pcm_s16le -acodec pcm_s16le -acodec pcm_s16le -acodec pcm_s16le ""{6}""",
            //                                         srcFile, ConvertMs2Sec(startMs), ConvertMs2Sec(durMs), m_bitrate, m_samplerate, m_out_channels, outFileName);

//            int bitNumber = MAIniFile.IniReadIntValue("VoiceTrackMapping", "PCMSourceBitNumber");
            int bitNumber = 16;
            string acodec = "pcm_s16le";
            if (bitNumber == 16)
                acodec = "pcm_s16le";
            if (bitNumber == 24)
                acodec = "pcm_s24le";

            string Arguments = string.Format(@"-ss {0} -t {1} -acodec {2} -i ""{3}"" -map 0:{4} -ab {5} -vn -ar {6} -ac 1 -acodec pcm_s16le ""{7}""",
                                                                ConvertMs2Sec(startMs), ConvertMs2Sec(durMs), acodec, srcFile, trackIndex, m_bitrate, m_samplerate, outFileName);


//            ffmpeg -acodec pcm_s24le -i prova_audio2.mov -map 0:4 -vn -acodec pcm_s16le -ar 48000 -ab 96k -ac 1 mr4.wav

            try
            {
                if (File.Exists(outFileName))
                    File.Delete(outFileName);


                m_process = new Process();
                m_process.StartInfo.FileName = Application.StartupPath + @"\ffmpeg.exe";
                m_process.StartInfo.Arguments = Arguments;
                m_process.StartInfo.CreateNoWindow = true;
                m_process.StartInfo.UseShellExecute = false;
                m_process.StartInfo.RedirectStandardOutput = false;
                m_process.StartInfo.RedirectStandardError = false;
                m_process.StartInfo.WorkingDirectory = ".\\";

                bool started = true;
                try
                {
                    m_process.Start();
                    m_process.WaitForExit();
                    ret = m_process.ExitCode;

                }
                catch (Exception ex)
                {
                    ret = -1;
                }
                finally
                {
                    if (m_process != null)
                    {
                        m_process.Close();
                        m_process.Dispose();
                        m_process = null;
                    }

                }


            }
            catch (Exception ex)
            {
                ret = -1;
            }
            finally
            {
            }

            return ret;
        }

        /*    old start    
               public int Start(string srcFile, int startSecs, int durSecs, int startMilliSecs, int durMillisecs, string destFolder, int idTrack)
               {
                   int ret = 0;

                   //ffmpeg -i "C:\DEVELOP\MediaAlliance\DEMO MXF\adri_DVCPRO50_2.mxf" -y -sameq -ss 25 -T 2 -s 100x80 -f image2 -r 1/5 .\frames\nome%03d.png

                   try
                   {
                
                       m_process = new Process();
                       m_process.StartInfo.FileName = Application.StartupPath + @"\ffmpeg.exe";
                       m_process.StartInfo.Arguments = string.Format(@"-i ""{0}"" -ss {1}.{5} -t {2}.{6} -map 0:{3}  ""{4}""",
                                                                     srcFile, startSecs, durSecs, idTrack, destFolder, startMilliSecs, durMillisecs);
                       m_process.StartInfo.CreateNoWindow = true;
                       m_process.StartInfo.UseShellExecute = false;
                       m_process.StartInfo.RedirectStandardOutput = false;
                       m_process.StartInfo.RedirectStandardError = false;
                       m_process.StartInfo.WorkingDirectory = ".\\";

                       bool started = true;
                       try
                       {
                           m_process.Start();
                           m_process.WaitForExit();
                           ret = m_process.ExitCode;

                       }
                       catch (Exception ex)
                       {
                           ret = -1;                    
                       }
                       finally
                       {
                           if (m_process != null)
                           {
                               m_process.Close();
                               m_process.Dispose();
                               m_process = null;
                           }

                       }
            

                   }
                   catch (Exception ex)
                   {
                       ret = -1;
                   }
                   finally
                   {
                   }

                   return ret;
               }
           */


        public void Stop()
        {
            try
            {
                if (m_process != null)
                {
                    m_process.Kill();
                    m_process.Close();
                    m_process = null;
                }
            }
            catch { }
        }

    }

    public class AudioResample
    {
        private Process m_process = null;

        private int m_samplerate;
        private int m_out_channels;
        private int m_bitPerSample;
        private int m_bitrate;


        public AudioResample()
        {
            m_samplerate = 48000;
            m_out_channels = 2;
            m_bitPerSample = 16;

            m_bitrate = m_samplerate * m_bitPerSample * m_out_channels;
        }

        public AudioResample(int samplerate, int channels, int bitPerSample)
        {
            m_samplerate = samplerate;
            m_out_channels = channels;
            m_bitPerSample = bitPerSample;

            m_bitrate = m_samplerate * m_bitPerSample * m_out_channels;
        }

        public int Start(string srcFile, string outFileName)
        {
            int ret = 0;

            string Arguments = string.Format(@"-i ""{0}"" -ab {1} -ar {2} -ac {3} -acodec pcm_s16le ""{4}""",
                                                     srcFile, m_bitrate, m_samplerate, m_out_channels, outFileName);


            try
            {
                if (File.Exists(outFileName))
                    File.Delete(outFileName);


                m_process = new Process();
                m_process.StartInfo.FileName = Application.StartupPath + @"\ffmpeg.exe";
                m_process.StartInfo.Arguments = Arguments;
                m_process.StartInfo.CreateNoWindow = true;
                m_process.StartInfo.UseShellExecute = false;
                m_process.StartInfo.RedirectStandardOutput = false;
                m_process.StartInfo.RedirectStandardError = false;
                m_process.StartInfo.WorkingDirectory = ".\\";

                bool started = true;
                try
                {
                    m_process.Start();
                    m_process.WaitForExit();
                    ret = m_process.ExitCode;

                }
                catch (Exception ex)
                {
                    ret = -1;
                }
                finally
                {
                    if (m_process != null)
                    {
                        m_process.Close();
                        m_process.Dispose();
                        m_process = null;
                    }

                }


            }
            catch (Exception ex)
            {
                ret = -1;
            }
            finally
            {
            }

            return ret;
        }


        public void Stop()
        {
            try
            {
                if (m_process != null)
                {
                    m_process.Kill();
                    m_process.Close();
                    m_process = null;
                }
            }
            catch { }
        }

    }


    public class AudioExport
    {
        private Process m_process = null;

        private int m_samplerate;
        private int m_out_channels;
        private int m_bitPerSample;
        private int m_bitrate;
        private string m_ext = "wav";

        public AudioExport(string ext)
        {
            m_samplerate = 48000;
            m_out_channels = 2;
            m_bitPerSample = 16;
            m_ext = ext;

            m_bitrate = m_samplerate * m_bitPerSample * m_out_channels;
        }

        public AudioExport(int samplerate, int channels, int bitPerSample, string ext)
        {
            m_samplerate = samplerate;
            m_out_channels = channels;
            m_bitPerSample = bitPerSample;
            m_ext = ext;

            m_bitrate = m_samplerate * m_bitPerSample * m_out_channels;
        }

        public int Start(string srcFile, string outFileName)
        {
            int ret = 0;

            string acodeOut = "";
            if (m_ext.ToLower().Contains("wav"))
            {
                if (m_bitPerSample == 16)
                    acodeOut = string.Format("-ab {0} -ar {1} -ac {2} -acodec pcm_s16le", m_bitrate, m_samplerate, m_out_channels);

                if (m_bitPerSample == 24)
                    acodeOut = string.Format("-ab {0} -ar {1} -ac {2} -acodec pcm_s24le", m_bitrate, m_samplerate, m_out_channels);
            }
            if (m_ext.ToLower().Contains("mp3"))
            {
                acodeOut = "-ab 192k";
            }

            string Arguments = string.Format(@"-i ""{0}"" {1} ""{2}""",
                                                     srcFile, acodeOut, outFileName);


            try
            {
                if (File.Exists(outFileName))
                    File.Delete(outFileName);


                m_process = new Process();
                m_process.StartInfo.FileName = Application.StartupPath + @"\ffmpeg.exe";
                m_process.StartInfo.Arguments = Arguments;
                m_process.StartInfo.CreateNoWindow = true;
                m_process.StartInfo.UseShellExecute = false;
                m_process.StartInfo.RedirectStandardOutput = false;
                m_process.StartInfo.RedirectStandardError = false;
                m_process.StartInfo.WorkingDirectory = ".\\";

                bool started = true;
                try
                {
                    m_process.Start();
                    m_process.WaitForExit();
                    ret = m_process.ExitCode;

                }
                catch (Exception ex)
                {
                    ret = -1;
                }
                finally
                {
                    if (m_process != null)
                    {
                        m_process.Close();
                        m_process.Dispose();
                        m_process = null;
                    }

                }


            }
            catch (Exception ex)
            {
                ret = -1;
            }
            finally
            {
            }

            return ret;
        }


        public void Stop()
        {
            try
            {
                if (m_process != null)
                {
                    m_process.Kill();
                    m_process.Close();
                    m_process = null;
                }
            }
            catch { }
        }

    }

    public class AudioRawExtract
    {
        private Process m_process = null;

        private int m_samplerate;
        private int m_out_channels;
        private int m_bitPerSample;
        private int m_bitrate;
        private string m_ext = "raw";
        private bool m_processTeminate = true;

        public AudioRawExtract()
        {
            m_samplerate = 48000;
            m_out_channels = 2;
            m_bitPerSample = 16;


            m_bitrate = m_samplerate * m_bitPerSample * m_out_channels;
        }

        public int StartSingleStream(string srcFile, ulong startMs, ulong durMs, string outFileName, int bps)
        {
            int ret = 0;

            string strBPS = "s16le";
            if (bps == 24)
                strBPS = "s24le";
            if (bps == 32)
                strBPS = "s32le";                        

            string Arguments = string.Format(@"-ss {0} -t {1} -i ""{2}"" -f {3} ""{4}""",
                                                     ConvertMs2Sec(startMs), 
                                                     ConvertMs2Sec(durMs), 
                                                     srcFile, 
                                                     strBPS, 
                                                     outFileName);


            try
            {
                if (File.Exists(outFileName))
                    File.Delete(outFileName);


                m_process = new Process();
                m_process.StartInfo.FileName = Application.StartupPath + @"\ffmpeg.exe";
                m_process.StartInfo.Arguments = Arguments;
                m_process.StartInfo.CreateNoWindow = true;
                m_process.StartInfo.UseShellExecute = false;
                m_process.StartInfo.RedirectStandardOutput = false;
                m_process.StartInfo.RedirectStandardError = false;
                m_process.StartInfo.WorkingDirectory = ".\\";
                m_process.EnableRaisingEvents = true;
 
                m_process.Exited += new EventHandler(OnExitedProcess);
                m_process.ErrorDataReceived += new DataReceivedEventHandler(OnOutputDataReceived);


                Debug.WriteLine("Start Extract Audio at " + DateTime.Now.ToLongTimeString());
                try
                {
                    m_processTeminate = false;
                    m_process.Start();
                    while (!m_processTeminate)
                    {
                        Application.DoEvents();

                        Thread.Sleep(5);

                        //string str1 = m_process.StandardError.ReadLine();
                        //Debug.WriteLine(str1);
                    }

                    //m_process.WaitForExit();
                    ret = m_process.ExitCode;

                }
                catch (Exception ex)
                {
                    ret = -1;
                }
                finally
                {
                    if (m_process != null)
                    {
                        m_process.Close();
                        m_process.Dispose();
                        m_process = null;
                    }

                }


            }
            catch (Exception ex)
            {
                ret = -1;
            }
            finally
            {
            }

            return ret;
        }

        public int StartSingleStreamFromMulti(int idStream, string srcFile, ulong startMs, ulong durMs, string outFileName, int bps)
        {
            int ret = 0;

            string strBPS = "s16le";
            string strAcodec = "";
            if (bps == 24)
            {
                strBPS = "s24le";
                strAcodec = "-acodec copy";
            }
            if (bps == 32)
            {
                strBPS = "s32le";
                strAcodec = "-acodec copy";
            }

            string Arguments = string.Format(@"-ss {0} -t {1} -i ""{2}"" -map 0:{3} {4} -f {5} ""{6}""",
                                                     ConvertMs2Sec(startMs), ConvertMs2Sec(durMs), srcFile, idStream, strAcodec, strBPS, outFileName);


            try
            {
                if (File.Exists(outFileName))
                    File.Delete(outFileName);


                m_process = new Process();
                m_process.StartInfo.FileName = Application.StartupPath + @"\ffmpeg.exe";
                m_process.StartInfo.Arguments = Arguments;
                m_process.StartInfo.CreateNoWindow = true;
                m_process.StartInfo.UseShellExecute = false;
                m_process.StartInfo.RedirectStandardOutput = false;
                m_process.StartInfo.RedirectStandardError = false;
                m_process.StartInfo.WorkingDirectory = ".\\";
                m_process.EnableRaisingEvents = true;
 
                m_process.Exited += new EventHandler(OnExitedProcess);
                m_process.ErrorDataReceived += new DataReceivedEventHandler(OnOutputDataReceived);
                bool started = true;

                Debug.WriteLine("Start Extract Audio at " + DateTime.Now.ToLongTimeString());
                try
                {
                    m_processTeminate = false;
                    m_process.Start();
                    while (!m_processTeminate)
                    {
                        Application.DoEvents();

                        Thread.Sleep(5);

                        //string str1 = m_process.StandardError.ReadLine();
                        //Debug.WriteLine(str1);
                    }

                    //m_process.WaitForExit();
                    ret = m_process.ExitCode;

                }
                catch (Exception ex)
                {
                    ret = -1;
                }
                finally
                {
                    if (m_process != null)
                    {
                        m_process.Close();
                        m_process.Dispose();
                        m_process = null;
                    }

                }


            }
            catch (Exception ex)
            {
                ret = -1;
            }
            finally
            {
            }

            return ret;
        }
        
        
   /*     public int StartMultiStream(string srcFile, ulong startMs, ulong durMs, string[] outFileNames)
        {
            int ret = 0;


            string strAudios = "";
            int nStream = 1;
            foreach (string audioFileName in audioFiles)
            {
                strAudios += " -f s16le -map 0."+nStream+" \"" + audioFileName + "\"";
            }

            string Arguments = string.Format(@"-ss {0} -t {1} -i ""{2}"" {3}",
                                                     ConvertMs2Sec(startMs), ConvertMs2Sec(durMs), srcFile, strAudios);


            try
            {
                if (File.Exists(outFileName))
                    File.Delete(outFileName);


                m_process = new Process();
                m_process.StartInfo.FileName = Application.StartupPath + @"\ffmpeg.exe";
                m_process.StartInfo.Arguments = Arguments;
                m_process.StartInfo.CreateNoWindow = true;
                m_process.StartInfo.UseShellExecute = false;
                m_process.StartInfo.RedirectStandardOutput = false;
                m_process.StartInfo.RedirectStandardError = false;
                m_process.StartInfo.WorkingDirectory = ".\\";
                m_process.EnableRaisingEvents = true;

                m_process.Exited += new EventHandler(OnExitedProcess);
                m_process.ErrorDataReceived += new DataReceivedEventHandler(OnOutputDataReceived);
                bool started = true;

                CLog.Log("Start Extract Audio at " + DateTime.Now.ToLongTimeString());
                try
                {
                    m_processTeminate = false;
                    m_process.Start();
                    while (!m_processTeminate)
                    {
                        Application.DoEvents();

                        Thread.Sleep(5);

                        //string str1 = m_process.StandardError.ReadLine();
                        //Debug.WriteLine(str1);
                    }

                    //m_process.WaitForExit();
                    ret = m_process.ExitCode;

                }
                catch (Exception ex)
                {
                    ret = -1;
                }
                finally
                {
                    if (m_process != null)
                    {
                        m_process.Close();
                        m_process.Dispose();
                        m_process = null;
                    }

                }


            }
            catch (Exception ex)
            {
                ret = -1;
            }
            finally
            {
            }

            return ret;
        }
        */
        private void OnExitedProcess(object data, EventArgs args)
        {

            Debug.WriteLine("Stop Extract Audio at " + DateTime.Now.ToLongTimeString());

            if (m_process != null)
            {
                //string str1 = m_process.StandardError.ReadLine();
                //Debug.WriteLine(str1);
            }

            m_processTeminate = true;
        }
        private void OnOutputDataReceived(object data, DataReceivedEventArgs dataArgs)
        {
            string a = dataArgs.Data;

            a += "";
        }

        public void Stop()
        {
            try
            {
                if (m_process != null)
                {
                    m_process.Kill();
                    m_process.Close();
                    m_process = null;
                }
            }
            catch { }
        }

        private string ConvertMs2Sec(ulong ms)
        {
            float res = (float)ms / 1000.0f;
            return res.ToString().Replace(",", ".");
        }

    }


    public class AudioRawToWavMono
    {
        private Process m_process = null;

        private int m_samplerate;
        private int m_out_channels;
        private int m_bitPerSample;
        private int m_bitrate;


        public AudioRawToWavMono()
        {
            m_samplerate = 48000;
            m_out_channels = 2;
            m_bitPerSample = 16;

            m_bitrate = m_samplerate * m_bitPerSample * m_out_channels;
        }

        public AudioRawToWavMono(int samplerate, int channels, int bitPerSample)
        {
            m_samplerate = samplerate;
            m_out_channels = channels;
            m_bitPerSample = bitPerSample;

            m_bitrate = m_samplerate * m_bitPerSample * m_out_channels;
        }

        private string ComputeBitPerSecond(string srcFile)
        {
            string strBPS = "s16le";

            try
            {
                FileInfo fi = new FileInfo();
                fi.CheckForAudio(srcFile);
                m_bitPerSample = fi.BitPerSecond;
                m_bitrate = m_samplerate * m_bitPerSample * m_out_channels;

                if (m_bitPerSample == 24)
                    strBPS = "s24le";
                if (m_bitPerSample == 32)
                    strBPS = "s32le";
            }
            catch (Exception ex)
            {
                Debug.WriteLine("AudioRawExtract :" + ex.Message);
            }

            return strBPS;
        }


        public int Start(string srcFile, string outFileName, int bps)
        {
            int ret = 0;

            m_bitPerSample = bps;
            ComputeBitPerSecond(srcFile);

            string strBPS = "s16le";

            if (bps == 24)
            {
                strBPS = "s24le";
                //m_bitrate = m_samplerate * m_bitPerSample * m_out_channels;
            }
            if (bps == 32)
            {
                strBPS = "s32le";
                //m_bitrate = m_samplerate * m_bitPerSample * m_out_channels;
            }

            string Arguments = string.Format(@" -f {0} -ac 1 -ar {1} -i ""{2}"" -acodec copy -ab {3} ""{4}""",
                                                     strBPS, m_samplerate, srcFile, m_bitrate, outFileName);

            //24 bit source
            //string Arguments = string.Format(@" -f s24le -ac 1 -ar {0} -i ""{1}"" -acodec copy ""{2}""",
            //                                         m_samplerate, srcFile, outFileName);


            try
            {
                if (File.Exists(outFileName))
                    File.Delete(outFileName);


                m_process = new Process();
                m_process.StartInfo.FileName = Application.StartupPath + @"\ffmpeg.exe";
                m_process.StartInfo.Arguments = Arguments;
                m_process.StartInfo.CreateNoWindow = true;
                m_process.StartInfo.UseShellExecute = false;
                m_process.StartInfo.RedirectStandardOutput = false;
                m_process.StartInfo.RedirectStandardError = false;
                m_process.StartInfo.WorkingDirectory = ".\\";

                bool started = true;
                try
                {
                    m_process.Start();
                    m_process.WaitForExit();
                    ret = m_process.ExitCode;

                }
                catch (Exception ex)
                {
                    ret = -1;
                }
                finally
                {
                    if (m_process != null)
                    {
                        m_process.Close();
                        m_process.Dispose();
                        m_process = null;
                    }

                }


            }
            catch (Exception ex)
            {
                ret = -1;
            }
            finally
            {
            }

            return ret;
        }


        public void Stop()
        {
            try
            {
                if (m_process != null)
                {
                    m_process.Kill();
                    m_process.Close();
                    m_process = null;
                }
            }
            catch { }
        }

    }

}
