using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace WAV_Multi_2_StereoPairs
{
    internal class CWaveStereoOutput
    {
        public static int BUFFER_SIZE = 1024 * 1024;

        public ulong totalData = 0;

        public string fileFullName;
        public CBuffer outBuffer = new CBuffer(BUFFER_SIZE);
        public CWaveProcessor waveHeader = new CWaveProcessor();

        private FileStream fs = null;
        private BinaryWriter bw = null;

        public CWaveStereoOutput(string fileFullName, CWaveProcessor headerToClone)
        {
            this.fileFullName = fileFullName;
            this.fs = new FileStream(fileFullName, FileMode.Create, FileAccess.Write);
            this.bw = new BinaryWriter(fs);

            this.waveHeader = (CWaveProcessor)headerToClone.Clone();
            this.waveHeader.Channels = 2;
            this.waveHeader.WaveHeaderOUT(this.bw);
        }

        public void Add(byte byteData)
        {
            this.outBuffer.Add(byteData);
            totalData++;
        }

        public void WriteBuffer()
        {
            this.outBuffer.Write(bw);
        }

        public void Close()
        {
            Debug.WriteLine(this.bw.BaseStream.Length.ToString() + " = 44 + " + totalData.ToString());

            if (this.bw != null) this.bw.Close();
            if (this.fs != null) this.fs.Close();

            waveHeader.DataLength = (uint)totalData;

            waveHeader.WaveHeaderOverwrite(this.fileFullName);
        }
    }
}
