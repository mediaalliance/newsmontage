using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using System.Diagnostics;

namespace WAV_Multi_2_StereoPairs
{
    public class CMtoSt
    {
        private CLog log = new CLog();
        private bool setupOk = false;

        /// <summary>
        /// la solita piccola protezione
        /// </summary>
        /// <param name="info"></param>
        public void Setup(string info)
        {
            try
            {
                if (Int32.Parse(info) / 4 == 491)
                {
                    setupOk = true;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[Setup] Ex: " + ex.Message);
            }
            log.Log("Setup success: " + setupOk.ToString());
        }

        public bool Convert(string inputFullName, string outputFullName)
        {
            int dummyPairs = 0;
            return Convert(inputFullName, outputFullName, out dummyPairs);
        }

        public bool Convert(string inputFullName, string outputFullName, out int stereoPairs)
        {
            bool success = false;
            stereoPairs = 0;
            try
            {
                if (!setupOk)
                {
                    log.Error("required setup first");
                    return false;
                }

                CWaveProcessor inWaveHeader = new CWaveProcessor();
                inWaveHeader.WaveHeaderIN(inputFullName);

                if (inWaveHeader.Channels == 4
                    || inWaveHeader.Channels == 6
                    || inWaveHeader.Channels == 8
                    || inWaveHeader.Channels == 16)
                {
                    log.Log("input channels: " + inWaveHeader.Channels.ToString());

                    int bytesPerSample = inWaveHeader.BitsPerSample / 8;
                    //int inputPairs = inWaveHeader.Channels / 2;
                    int bytesToSkip = bytesPerSample * (inWaveHeader.Channels - 2);

                    CWaveStereoOutput waveout = new CWaveStereoOutput(outputFullName, inWaveHeader);

                    byte[] inBuffer = new byte[CWaveStereoOutput.BUFFER_SIZE];

                    //write data
                    FileStream fin = new FileStream(inputFullName, FileMode.Open, FileAccess.Read);
                    fin.Position = 44;

                    int readBytes = 0;
                    do
                    {
                        int i, b;//, temp;
                        readBytes = fin.Read(inBuffer, 0, CWaveStereoOutput.BUFFER_SIZE);
                        if (readBytes > 0)
                        {
                            //temp = readBytes / outputFilesCount / bytesPerSample;
                            for (i = 0; i < readBytes; )
                            {
                                //ch1 + ch2 = left + right
                                for (b = 0; b < bytesPerSample * 2; b++)
                                {
                                    //if (i >= ((1024 * 1024)-1))
                                    //    Debug.Write("");
                                    waveout.Add(inBuffer[i++]);
                                }
                                i += bytesToSkip; 
                            }
                            waveout.WriteBuffer();
                        }
                    } while (readBytes > 0);

                    fin.Close();
                    waveout.Close();
                    success = true;
                }
                else
                {
                    log.Error("Invalid number of input channels: " + inWaveHeader.Channels.ToString());
                }

            }
            catch (Exception ex)
            {
                log.Error("[Convert] Ex: " + ex.Message);
            }
            log.Log("Convert success: " + success.ToString());
            return success;
        }

        public bool ConvertRaw(string inputFullName, int channels, int bytesPerSample, string outputFullName)
        {
            bool success = false;
            try
            {
                if (!setupOk)
                {
                    log.Error("required setup first");
                    return false;
                }

                const int SAMPLES_BUFFERED = 128 * 1024;
                int BUFFER_IN = bytesPerSample * channels * SAMPLES_BUFFERED;
                int BUFFER_OUT = bytesPerSample * 2 * SAMPLES_BUFFERED;

                if (channels == 4 || channels == 6 || channels == 8 || channels == 16)
                {
                    log.Log("input channels: " + channels.ToString());

                    if (File.Exists(outputFullName))
                        File.Delete(outputFullName);
                    using (FileStream fout = new FileStream(outputFullName, FileMode.Create, FileAccess.Write))
                    {

                        int bytesToSkip = bytesPerSample * (channels - 2);

                        byte[] waveout = new byte[BUFFER_OUT];

                        byte[] inBuffer = new byte[BUFFER_IN];

                        using (FileStream fin = new FileStream(inputFullName, FileMode.Open, FileAccess.Read))
                        {
                            int readBytes = 0;
                            int i, b, w;
                            int bytesToWrite = bytesPerSample * 2;
                            do
                            {
                                readBytes = fin.Read(inBuffer, 0, BUFFER_IN);
                                if (readBytes > 0)
                                {
                                    for (w = 0, i = 0; i < readBytes; )
                                    {
                                        //ch1 + ch2 = left + right
                                        for (b = 0; b < bytesToWrite; b++)
                                        {
                                            waveout[w++] = inBuffer[i++];
                                        }
                                        i += bytesToSkip;
                                    }
                                    fout.Write(waveout, 0, w);
                                }
                            } while (readBytes > 0);

                            fin.Close();
                        }
                        fout.Flush();
                        fout.Close();
                        success = true;
                    }
                }
                else
                {
                    log.Error("Invalid number of input channels: " + channels.ToString());
                }

            }
            catch (Exception ex)
            {
                log.Error("[Convert] Ex: " + ex.Message);
            }
            log.Log("Convert success: " + success.ToString());
            return success;
        }


        public string GetLogs()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                foreach (string s in log.GetLogs())
                {
                    sb.AppendLine(s);
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[GetLogs] Ex: " + ex.Message);
                return "[GetLogs] Ex: " + ex.Message;
            }
            //return "";
             
        }
    }
}
