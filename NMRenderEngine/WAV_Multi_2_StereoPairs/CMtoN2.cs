using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using System.Diagnostics;

namespace WAV_Multi_2_StereoPairs
{
    public class CMtoN2
    {
        private CLog log = new CLog();
        private bool setupOk = false;

        /// <summary>
        /// la solita piccola protezione
        /// </summary>
        /// <param name="info"></param>
        public void Setup(string info)
        {
            try
            {
                if (Int32.Parse(info) / 4 == 491)
                {
                    setupOk = true;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[Setup] Ex: " + ex.Message);
            }
            log.Log("Setup success: " + setupOk.ToString());
        }

        public bool Convert(string inputFullName, string outputPath)
        {
            int dummyPairs = 0;
            return Convert(inputFullName, outputPath, out dummyPairs);
        }

        public bool Convert(string inputFullName, string outputPath, out int stereoPairs)
        {
            bool success = false;
            stereoPairs = 0;
            try
            {
                if (!setupOk)
                {
                    log.Error("required setup first");
                    return false;
                }

                CWaveProcessor inWaveHeader = new CWaveProcessor();
                inWaveHeader.WaveHeaderIN(inputFullName);

                if (inWaveHeader.Channels == 4
                    || inWaveHeader.Channels == 6
                    || inWaveHeader.Channels == 8
                    || inWaveHeader.Channels == 16)
                {
                    log.Log("input channels: " + inWaveHeader.Channels.ToString());

                    int outputFilesCount = inWaveHeader.Channels / 2;
                    int bytesPerSample = inWaveHeader.BitsPerSample / 8;

                    //log.Log("block align = " + inWaveHeader.bl.ToString());
                    //if (bytesPerSample == 3)
                    //    bytesPerSample = 4; //sample must be padded to be word aligned!!!

                    stereoPairs = outputFilesCount;
                    log.Log("output stereo files: " + outputFilesCount.ToString());

                    CWaveStereoOutput[] waveout = new CWaveStereoOutput[outputFilesCount];
                    

                    //make file names ...
                    for (int i = 0; i < outputFilesCount; i++)
                    {
                        string fileFullName = "";
                        if (outputPath.Length > 0)
                        {
                            fileFullName = String.Format("{0}\\{1}_{2}.wav", outputPath, 
                                Path.GetFileNameWithoutExtension(inputFullName), i);
                        }
                        else
                        {
                            fileFullName = String.Format("{0}_{1}.wav", inputFullName, i);
                        }

                        waveout[i] = new CWaveStereoOutput(fileFullName, inWaveHeader);
                    }

                    byte[] inBuffer = new byte[CWaveStereoOutput.BUFFER_SIZE];

                    //write data
                    FileStream fin = new FileStream(inputFullName, FileMode.Open, FileAccess.Read);
                    fin.Position = 44;

                    int readBytes = 0;
                    do
                    {
                        int i, obuf, b;//, temp;
                        int bytesToWrite = bytesPerSample * 2;
                        readBytes = fin.Read(inBuffer, 0, CWaveStereoOutput.BUFFER_SIZE);
                        if (readBytes > 0)
                        {
                            //temp = readBytes / outputFilesCount / bytesPerSample;
                            for (i = 0; i < readBytes; )
                            {
                                for(obuf=0; obuf < outputFilesCount; obuf++)
                                {
                                    //ch1 + ch2 = left + right
                                    for (b = 0; b < bytesToWrite; b++)
                                    {
                                        //if (i >= ((1024 * 1024)-1))
                                        //    Debug.Write("");
                                        waveout[obuf].Add(inBuffer[i++]);
                                    }
                                }
                            }
                            for (obuf = 0; obuf < outputFilesCount; obuf++)
                            {
                                waveout[obuf].WriteBuffer();
                            }
                        }
                    } while (readBytes > 0);

                    fin.Close();
                    for (int obuf = 0; obuf < outputFilesCount; obuf++)
                    {
                        waveout[obuf].Close();
                    }
                    success = true;
                }
                else
                {
                    log.Error("invalid number of input channels: " + inWaveHeader.Channels.ToString());
                }

            }
            catch (Exception ex)
            {
                log.Error("[Convert] Ex: " + ex.Message);
            }
            log.Log("Convert success: " + success.ToString());
            return success;
        }

        /// <summary>
        /// [2010-04-28] aggiunto l'operazione con formato raw
        /// </summary>
        /// <returns></returns>
        public bool ConvertRaw(string inputFullName, int channels, int bytesPerSample, string outputPath, out List<string> outputStereoRaws)
        {
            bool success = false;
            outputStereoRaws = new List<string>();
            try
            {
                if (!setupOk)
                {
                    log.Error("required setup first");
                    return false;
                }

                const int SAMPLES_BUFFERED = 128 * 1024;
                int BUFFER_IN = bytesPerSample * channels * SAMPLES_BUFFERED;
                int BUFFER_OUT = bytesPerSample * 2 * SAMPLES_BUFFERED;

                if (channels == 1 || channels == 2 || channels == 4 || channels == 6 || channels == 8 || channels == 16)
                {
                    log.Log("input channels: " + channels.ToString());

                    int outputFilesCount = channels;
                    log.Log("output stereo files: " + outputFilesCount.ToString());

                    string[] rawnames = new string[outputFilesCount];
                    FileStream[] fout = new FileStream[outputFilesCount];

                    //make files ...
                    for (int i = 0; i < outputFilesCount; i++)
                    {
                        if (outputPath.Length > 0)
                        {
                            rawnames[i] = String.Format("{0}\\{1}_{2}.raw", outputPath,
                                Path.GetFileNameWithoutExtension(inputFullName), i+1);
                        }
                        else
                        {
                            rawnames[i] = String.Format("{0}_{1}.raw", inputFullName, i+1);
                        }

                        if (File.Exists(rawnames[i]))
                            File.Delete(rawnames[i]);

                        //open the output stream ...
                        fout[i] = new FileStream(rawnames[i], FileMode.Create, FileAccess.Write);

                    }

                    outputStereoRaws.AddRange(rawnames);

                    CBuffer[] outbuf = new CBuffer[outputFilesCount];
                    for (int i = 0; i < outputFilesCount; i++) outbuf[i] = new CBuffer(BUFFER_OUT);

                    byte[] inBuffer = new byte[BUFFER_IN];

                    using (FileStream fin = new FileStream(inputFullName, FileMode.Open, FileAccess.Read))
                    {
                        log.Log("Total bytes to process: " + fin.Length.ToString("N0"));
                        int readBytes = 0;
                        int bytesToWrite = bytesPerSample * 2;
                        int i, obuf, b;
                        do
                        {
                            readBytes = fin.Read(inBuffer, 0, BUFFER_IN);
                            if (readBytes > 0)
                            {
                                for (i = 0; i < readBytes; )
                                {
                                    for(obuf=0; obuf < outputFilesCount; obuf++)
                                    {
                                        //ch1 + ch2 = left + right
                                        for (b = 0; b < bytesToWrite; b++)
                                        {
                                            //if (i >= ((1024 * 1024)-1))
                                            //    Debug.Write("");
                                            outbuf[obuf].Add(inBuffer[i++]);
                                        }
                                    }
                                }
                                for (obuf = 0; obuf < outputFilesCount; obuf++)
                                {
                                    fout[obuf].Write(outbuf[obuf].buffer, 0, outbuf[obuf].counter);
                                    outbuf[obuf].ResetCounter();
                                }
                            }
                        } while (readBytes > 0);
                    }
                    for (int i = 0; i < outputFilesCount; i++)
                    {
                        log.Log("Total bytes written on buffer [" + i.ToString() + "]: " + outbuf[i].total.ToString("N0"));
                        log.Log("Total bytes written on disk [" + i.ToString() + "]: " + fout[i].Length.ToString("N0"));
                        fout[i].Close();
                    }
                    success = true;
                }
                else
                {
                    log.Error("Invalid number of input channels: " + channels.ToString());
                }
            }
            catch (Exception ex)
            {
                log.Error("[Convert] Ex: " + ex.Message);
            }
            log.Log("Convert success: " + success.ToString());
            return success;
        }


        public string GetLogs()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                foreach (string s in log.GetLogs())
                {
                    sb.AppendLine(s);
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[GetLogs] Ex: " + ex.Message);
                return "[GetLogs] Ex: " + ex.Message;
            }
            //return "";
             
        }
    }
}
