using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace WAV_Multi_2_StereoPairs
{
    internal class CBuffer
    {
        public byte[] buffer;
        private int _counter = 0;
        private long _total = 0;

        public CBuffer(int length)
        {
            buffer = new byte[length];
        }
        
        public void Add(byte oneByte)
        {
            buffer[_counter++] = oneByte;
        }

        public void Write(BinaryWriter bw)
        {
            bw.Write(buffer, 0, counter);
            _counter = 0;
            _total += counter;
        }

        public void ResetCounter()
        {
            _total += _counter;
            _counter = 0;
        }

        public int counter 
        {
            get
            {
                return _counter;
            }
        }

        public long total
        {
            get
            {
                return _total + _counter;
            }
        }


    }
}
