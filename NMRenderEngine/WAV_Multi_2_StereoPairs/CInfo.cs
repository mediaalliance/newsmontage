using System;
using System.Collections.Generic;
using System.Text;

namespace WAV_Multi_2_StereoPairs
{
    public class CInfo
    {
        public static bool GetInfo(string waveFile, out int channels, out int sampleRate, out int bitsPerSample, out string logString)
        {
            bool success = false;
            channels = 0;
            sampleRate = 0;
            bitsPerSample = 0;
            logString = "";
            StringBuilder log = new StringBuilder();
            try
            {
                log.AppendLine("GetInfo on [" + waveFile + "] ...");
                CWaveProcessor p = new CWaveProcessor();
                if (p.WaveHeaderIN(waveFile))
                {
                    channels = p.Channels;
                    sampleRate = p.SampleRate;
                    bitsPerSample = p.BitsPerSample;
                    success = true;
                }
                else
                {
                    log.AppendLine("GetInfo failed decoding [" + waveFile + "]");
                }
            }
            catch(Exception ex)
            {
                log.AppendLine("[ERROR][GetInfo] Ex: " + ex.Message);
            }
            logString = log.ToString();
            return success;
        }
    }
}
