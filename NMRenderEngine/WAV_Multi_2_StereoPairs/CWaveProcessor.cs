using System;
using System.Text;
using System.IO;
using System.Diagnostics;

/// <summary>
/// Class Name  : clsWaveProcessor
/// 
/// 2007-12-09: Max
/// implementato gestione dello stream per evitare di mangiare tutta la memmoria con file grandi
/// 
/// <summary>

namespace WAV_Multi_2_StereoPairs
{
    internal class CWaveProcessor : ICloneable 
    {
        // Constants for default or base format [16bit 8kHz Mono]
        private const short CHNL = 1;
        private const int SMPL_RATE = 8000;
        private const int BIT_PER_SMPL = 16;
        private const short FILTER_FREQ_LOW = -10000;
        private const short FILTER_FREQ_HIGH = 10000;

        // Public Fields can be used for various operations
        public uint Length;
        public short Channels;
        public int SampleRate;
        public uint DataLength;
        public short BitsPerSample;
        public ushort MaxAudioLevel;

        //-----------------------------------------------------
        //Methods Starts Here
        //-----------------------------------------------------

        public object Clone()
        {
            CWaveProcessor clone = new CWaveProcessor();
            clone = (CWaveProcessor)this.MemberwiseClone();
            return clone;
        }


        /// <summary>
        /// Read the wave file header and store the key values in public variable.
        /// </summary>
        /// <param name="strPath">The physical path of wave file incl. file name for reading</param>
        /// <returns>True/False</returns>
        public bool WaveHeaderIN(string strPath)
        {
            if (strPath == null) strPath = "";
            if (strPath == "") return false;

            FileStream fs = new FileStream(strPath, FileMode.Open, FileAccess.Read);

            BinaryReader br = new BinaryReader(fs);
            try
            {
                Length = (uint)fs.Length - 8;
                fs.Position = 22;
                Channels = br.ReadInt16(); //1
                fs.Position = 24;
                SampleRate = br.ReadInt32(); //8000
                fs.Position = 34;
                BitsPerSample = br.ReadInt16(); //16
                DataLength = (uint)fs.Length - 44;
                //byte[] arrfile = new byte[fs.Length - 44];
                //fs.Position = 44;
                //fs.Read(arrfile, 0, arrfile.Length);
            }
            catch //(Exception ex)
            {
                return false;
            }
            finally
            {
                br.Close();
                fs.Close();
            }
            return true;
        }

        /// <summary>
        /// Write default WAVE header to the output. See constants above for default settings.
        /// </summary>
        /// <param name="strPath">The physical path of wave file incl. file name for output</param>
        /// <returns>True/False</returns>
        public bool WaveHeaderOUT(string strPath)
        {
            if (strPath == null) strPath = "";
            if (strPath == "") return false;

            if (File.Exists(strPath))
                File.Delete(strPath);
            FileStream fs = new FileStream(strPath, FileMode.Create, FileAccess.Write);

            BinaryWriter bw = new BinaryWriter(fs);
            try
            {
                fs.Position = 0;
                bw.Write(new char[4] { 'R', 'I', 'F', 'F' });

                bw.Write(Length);

                bw.Write(new char[8] { 'W', 'A', 'V', 'E', 'f', 'm', 't', ' ' });

                bw.Write((int)16);

                bw.Write((short)1);
                bw.Write(Channels);

                bw.Write(SampleRate);

                bw.Write((int)(SampleRate * ((BitsPerSample * Channels) / 8)));

                bw.Write((short)((BitsPerSample * Channels) / 8));

                bw.Write(BitsPerSample);

                bw.Write(new char[4] { 'd', 'a', 't', 'a' });
                bw.Write(DataLength);
            }
            catch
            {
                return false;
            }
            finally
            {
                bw.Close();
                fs.Close();
            }
            return true;
        }


        /// <summary>
        /// </summary>
        /// <returns>True/False</returns>
        public bool WaveHeaderOUT(BinaryWriter bw)
        {
            try
            {
                bw.BaseStream.Position = 0;
                bw.Write(new char[4] { 'R', 'I', 'F', 'F' });

                bw.Write(Length);

                bw.Write(new char[8] { 'W', 'A', 'V', 'E', 'f', 'm', 't', ' ' });

                bw.Write((int)16);

                bw.Write((short)1);
                bw.Write(Channels);

                bw.Write(SampleRate);

                bw.Write((int)(SampleRate * ((BitsPerSample * Channels) / 8)));

                bw.Write((short)((BitsPerSample * Channels) / 8));

                bw.Write(BitsPerSample);

                bw.Write(new char[4] { 'd', 'a', 't', 'a' });
                bw.Write(DataLength);
            }
            catch
            {
                return false;
            }
            return true;
        }


        public bool WaveHeaderOverwrite(string strPath)
        {
            if (strPath == null) strPath = "";
            if (strPath == "") return false;

            FileStream fs = new FileStream(strPath, FileMode.Open, FileAccess.Write);

            BinaryWriter bw = new BinaryWriter(fs);
            try
            {
                fs.Position = 0;
                bw.Write(new char[4] { 'R', 'I', 'F', 'F' });

                bw.Write(Length);

                bw.Write(new char[8] { 'W', 'A', 'V', 'E', 'f', 'm', 't', ' ' });

                bw.Write((int)16);

                bw.Write((short)1);
                bw.Write(Channels);

                bw.Write(SampleRate);

                bw.Write((int)(SampleRate * ((BitsPerSample * Channels) / 8)));

                bw.Write((short)((BitsPerSample * Channels) / 8));

                bw.Write(BitsPerSample);

                bw.Write(new char[4] { 'd', 'a', 't', 'a' });
                bw.Write(DataLength);
            }
            catch
            {
                return false;
            }
            finally
            {
                bw.Close();
                fs.Close();
            }
            return true;
        }


        public bool WaveHeaderOverwrite(BinaryWriter bw)
        {
            try
            {
                bw.BaseStream.Position = 0;
                bw.Write(new char[4] { 'R', 'I', 'F', 'F' });

                bw.Write(Length);

                bw.Write(new char[8] { 'W', 'A', 'V', 'E', 'f', 'm', 't', ' ' });

                bw.Write((int)16);

                bw.Write((short)1);
                bw.Write(Channels);

                bw.Write(SampleRate);

                bw.Write((int)(SampleRate * ((BitsPerSample * Channels) / 8)));

                bw.Write((short)((BitsPerSample * Channels) / 8));

                bw.Write(BitsPerSample);

                bw.Write(new char[4] { 'd', 'a', 't', 'a' });
                bw.Write(DataLength);
            }
            catch
            {
                return false;
            }
            return true;
        }


        /// <summary>
        /// Ensure any given wave file path that the file matches with default or base format [16bit 8kHz Mono]
        /// </summary>
        /// <param name="strPath">Wave file path</param>
        /// <returns>True/False</returns>
        public bool Validate(string strPath)
        {
            if (strPath == null) strPath = "";
            if (strPath == "") return false;

            CWaveProcessor wa_val = new CWaveProcessor();
            wa_val.WaveHeaderIN(strPath);
            if (wa_val.BitsPerSample != BIT_PER_SMPL) return false;
            if (wa_val.Channels != CHNL) return false;
            if (wa_val.SampleRate != SMPL_RATE) return false;
            return true;
        }

        /// <summary>
        /// Compare two wave files to ensure both are in same format
        /// </summary>
        /// <param name="Wave1">ref. to processor object</param>
        /// <param name="Wave2">ref. to processor object</param>
        /// <returns>True/False</returns>
        private static bool Compare(ref CWaveProcessor Wave1, ref CWaveProcessor Wave2)
        {
            if (Wave1.Channels != Wave2.Channels) return false;
            if (Wave1.BitsPerSample != Wave2.BitsPerSample) return false;
            if (Wave1.SampleRate != Wave2.SampleRate) return false;
            return true;
        }

        /// <summary>
        /// Read the WAVE file then position to DADA segment and return the chunk as byte array 
        /// </summary>
        /// <param name="strWAVEPath">Path of WAVE file</param>
        /// <returns>byte array</returns>
        private byte[] GetWAVEData(string strWAVEPath)
        {
            try
            {
                FileStream fs = new FileStream(@strWAVEPath, FileMode.Open, FileAccess.Read);
                byte[] arrfile = new byte[fs.Length - 44];
                fs.Position = 44;
                fs.Read(arrfile, 0, arrfile.Length);
                fs.Close();
                return arrfile;
            }
            catch (IOException ioex)
            {
                throw ioex;
            }
        }

        /// <summary>
        /// Write data chunk to the file. The header must be written before hand.
        /// </summary>
        /// <param name="strWAVEPath">Path of WAVE file</param>
        /// <param name="arrData">data in array</param>
        /// <returns>True</returns>
        private bool WriteWAVEData(string strWAVEPath, ref byte[] arrData)
        {
            try
            {
                FileStream fo = new FileStream(@strWAVEPath, FileMode.Append, FileAccess.Write);
                BinaryWriter bw = new BinaryWriter(fo);
                bw.Write(arrData);
                bw.Close();
                fo.Close();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    } // End of clsWaveProcessor class
}