using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace WAV_Multi_2_StereoPairs
{
    internal class CLog
    {
        private List<string> logList = new List<string>();

        public void Log(string text)
        {
            Debug.WriteLine("[LOG] " + text);
            Console.WriteLine("[LOG] " + text);
            logList.Add("[LOG] " + text);
        }
        public void Error(string text)
        {
            Debug.WriteLine("[ERROR] " + text);
            Console.WriteLine("[ERROR] " + text);
            logList.Add("[ERROR] " + text);
        }
        public void Warning(string text)
        {
            Debug.WriteLine("[WARNING] " + text);
            Console.WriteLine("[WARNING] " + text);
            logList.Add("[WARNING] " + text);
        }
        public List<string> GetLogs()
        {
            return logList;
        }
    }
}
