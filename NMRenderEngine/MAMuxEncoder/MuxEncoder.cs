﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;
//using Microsoft.Win32;

using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;
using Microsoft.Win32;
using MediaAlliance.NewsMontage.RenderEngine.Classes;


namespace MAMuxEncoder
{
    public class MuxEncoder
    {
        // DELEGATES
        public delegate void Mux_OnLog(object Sender, string message);

        // EVENTS
        public event Mux_OnLog OnLog = null;


        [DllImport("DV50Op1aWrapper.dll")]
        static extern IntPtr WrapFiles([MarshalAs(UnmanagedType.LPStr)]String videoFileEssence,
                                       [MarshalAs(UnmanagedType.LPStr)]String audioFileEssence,
                                       [MarshalAs(UnmanagedType.LPStr)]String outputFilePath);

        [DllImport("DV50Op1aWrapper.dll")]
        static extern IntPtr WrapWithMultipleAudioFiles([MarshalAs(UnmanagedType.LPStr)]String videoFileEssence,
                                                        String[] audioFileEssences,
                                                        Int16 nrOfAudioFiles,
                                                        [MarshalAs(UnmanagedType.LPStr)]String outputFilePath,
                                                        Int32 audioSamplingRateNum,
                                                        Int32 audioSamplingRateDen,
                                                        UInt16 audioQuantBits,
                                                        Int32 bytesPerSample);



        [DllImport("ConfigMCDll.dll", EntryPoint = "?SetBitsPerSecond@@YAXH@Z")]
        public static extern void SetBitsPerSecond(int bitsPerSecond);

        [DllImport("ConfigMCDll.dll", EntryPoint = "?StartMuxToMXFIMX@@YAHPAD00H@Z")]
        public static extern int StartMuxToMXFIMX(string audioFile, string videoFile, string destFile, int preset);

        [DllImport("ConfigMCDll.dll", EntryPoint = "?StartMuxToMXFXDCAMHD@@YAHPAD00H@Z")]
        public static extern int StartMuxToMXFXDCAMHD(string audioFile, string videoFile, string destFile, int preset);
        

        [DllImport("ConfigMCDll.dll", EntryPoint = "?OnComputing@@YAXP6GX_J0@Z@Z")]
        public static extern void OnComputing(ReceiveCallbackFunction _OnComputingMXFEncoder);

        public delegate void ReceiveCallbackFunction(Int64 pCurrent, Int64 pStop);



        private List<string> m_audioList = new List<string>();
        private string m_videoIn = "";

        private int m_samplesPerSecond;
        private int m_bitsPerSample;


        private CodecData m_selectedCodecData = null;

        public List<string> AudioList { get { return m_audioList; } }

        public class CodecData
        {
            public string Ext;
            public string Format;
            public string Description;

            public CodecData(string ext, string format, string description)
            {
                Ext = ext;
                Format = format;
                Description = description;
            }

            public override string ToString()
            {
                return Ext + " " + Format + " (*." + Ext.ToLower() + ")|*." + Ext.ToLower();
            }
        }
       
        public List<CodecData> CodecList = new List<CodecData>();


        public MuxEncoder(int samplesPerSecond, int bitsPerSample)
        {         
            m_samplesPerSecond = samplesPerSecond;
            m_bitsPerSample = bitsPerSample;

            CodecData codecData = new CodecData(Container.MOV, Codec.DVCAM420, "DVCAM 420");
            CodecList.Add(codecData);

            codecData = new CodecData(Container.MXF, Codec.DVCPRO50, "DVCPRO50");
            CodecList.Add(codecData);

            codecData = new CodecData(Container.MXF, Codec.IMX30, "IMX 30Mbit");
            CodecList.Add(codecData);

            codecData = new CodecData(Container.MXF, Codec.IMX50, "IMX 50Mbit");
            CodecList.Add(codecData);

            codecData = new CodecData(Container.MXF, Codec.XDCAM_HD_1920, "XDCAM HD");
            CodecList.Add(codecData);

            codecData = new CodecData(Container.AVI, Codec.DVCAM420, "DVCAM 420");
            CodecList.Add(codecData);

            codecData = new CodecData(Container.MOV, Codec.IMX30, "IMX 30Mbit");
            CodecList.Add(codecData);

        }

        public void SelectEncoder(CodecData selectedCodecData)
        {
            m_selectedCodecData = selectedCodecData;
        }

        public void AddAudio(string filename)
        {
            m_audioList.Add(filename);
        }

        public void AddVideo(string filename)
        {
            m_videoIn = filename;
        }

        public int Execute(string filenameDest, int AudioBPS, out string error)
        {
            int res = 0;
            error = "";

            if (File.Exists(filenameDest))
                File.Delete(filenameDest);

            string[] strArrayAudioFiles = (string[])m_audioList.ToArray();

            switch (m_selectedCodecData.Ext)
            {
                case Container.MOV:
                    {
                        if (m_selectedCodecData.Format.Equals(Codec.DVCAM420))
                        {
                            FFMpegHelper.Muxer ffmpegMux = new FFMpegHelper.Muxer();
                            ffmpegMux.Start(m_videoIn, strArrayAudioFiles, filenameDest, AudioBPS);
                        }

                        if (m_selectedCodecData.Format.Equals(Codec.IMX30))
                        {
                            FFMpegHelper.MuxerAviToM2V muxerAviToM2V = new FFMpegHelper.MuxerAviToM2V();
                            
                            muxerAviToM2V.Start(m_videoIn, filenameDest.Replace(".mov",".m2v") );

                            FFMpegHelper.MuxerM2vToMovWithAudios muxerM2vToMovWithAudios = new FFMpegHelper.MuxerM2vToMovWithAudios();
                            muxerM2vToMovWithAudios.Start(filenameDest.Replace(".mov", ".m2v"), strArrayAudioFiles, filenameDest);

                            break;
                        }
                        break;
                    }
                case Container.AVI:
                case Container.MPG:
                    {
                        FFMpegHelper.Muxer ffmpegMux = new FFMpegHelper.Muxer();
                        ffmpegMux.Start(m_videoIn, strArrayAudioFiles, filenameDest, AudioBPS);

                        break;
                    }

                case Container.MXF:
                    {
                        switch (m_selectedCodecData.Format)
                        {
                            case Codec.DVCPRO50:
                                {
                                    #region DVCPRO50

                                    if (File.Exists(m_videoIn.Replace(".avi", ".dv")))
                                        File.Delete(m_videoIn.Replace(".avi", ".dv"));

                                    FFMpegHelper.Converter ffmpegMux = new FFMpegHelper.Converter();
                                    ffmpegMux.Start(m_videoIn, m_videoIn.Replace(".avi", ".dv"));


                                    //Check length audio files
                                    long maxLength = 0;
                                    List<string> alignedAudioFileList = new List<string>();
                                    foreach (string audioFile in strArrayAudioFiles)
                                    {
                                        FileInfo fInfo = new FileInfo(audioFile);

                                        if (maxLength == 0)
                                        {
                                            maxLength = fInfo.Length;
                                            alignedAudioFileList.Add(audioFile);
                                        }
                                        else if (maxLength == fInfo.Length)
                                            alignedAudioFileList.Add(audioFile);
                                    }


                                    string[] finalAudioList = (string[])alignedAudioFileList.ToArray();
                                    //Mux with DVCPRO50
                                    error = Marshal.PtrToStringAnsi(WrapWithMultipleAudioFiles(m_videoIn.Replace(".avi", ".dv"),
                                                                                   finalAudioList,
                                                                                   Convert.ToInt16(finalAudioList.Length),
                                                                                   filenameDest,
                                                                                   m_samplesPerSecond,
                                                                                   1,
                                                                                   (ushort)m_bitsPerSample,
                                                                                   m_bitsPerSample / 8));

                                    if ((error != null && error.Length > 0))
                                        res = -1;

                                    #endregion DVCPRO50
                                }
                                break;
                            case Codec.DV:
                                {
                                    #region DV25

                                    //if (File.Exists(m_videoIn.Replace(".avi", ".dv")))
                                    //    File.Delete(m_videoIn.Replace(".avi", ".dv"));

                                    //FFMpegHelper.Converter ffmpegMux = new FFMpegHelper.Converter();
                                    //ffmpegMux.Start(m_videoIn, m_videoIn.Replace(".avi", ".dv"));


                                    ////Check length audio files
                                    //long maxLength = 0;
                                    //List<string> alignedAudioFileList = new List<string>();
                                    //foreach (string audioFile in strArrayAudioFiles)
                                    //{
                                    //    FileInfo fInfo = new FileInfo(audioFile);

                                    //    if (maxLength == 0)
                                    //    {
                                    //        maxLength = fInfo.Length;
                                    //        alignedAudioFileList.Add(audioFile);
                                    //    }
                                    //    else if (maxLength == fInfo.Length)
                                    //        alignedAudioFileList.Add(audioFile);
                                    //}


                                    //string[] finalAudioList = (string[])alignedAudioFileList.ToArray();


                                    //FFMpegHelper.Muxer2_mxfdv mxf_mux = new FFMpegHelper.Muxer2_mxfdv();
                                    //mxf_mux.Start(m_videoIn.Replace(".avi", ".dv"), finalAudioList, filenameDest, AudioBPS);

                                    //if ((error != null && error.Length > 0))
                                    //    res = -1;

                                    #endregion DV25
                                }
                                break;
                        }

                        break;
                    }
            }

            return res;
        }

        public static void SetupRegistry(string encoderType, float fps)
        {
            string filterName = Application.StartupPath + @"\Registry\MainConceptMXFDemultiplexer.reg";
            //Process regeditProcess = Process.Start("regedit.exe", "/s \"" + filterName + "\"");
            //regeditProcess.WaitForExit();
            CMARegistryTools.ManualSetupReg(filterName);

            //ffdshow_enc
            filterName = Application.StartupPath + @"\Registry\ffdshow_enc.reg";
            //regeditProcess = Process.Start("regedit.exe", "/s \"" + filterName + "\"");
            //regeditProcess.WaitForExit();
            CMARegistryTools.ManualSetupReg(filterName);

            //ffdshowenc
            filterName = Application.StartupPath + @"\Registry\ffdshowenc.reg";
            //regeditProcess = Process.Start("regedit.exe", "/s \"" + filterName + "\"");
            //regeditProcess.WaitForExit();
            CMARegistryTools.ManualSetupReg(filterName);


            switch (encoderType)
            {
                case Codec.IMX30:// EncoderType.IMX_30:
                    {
                        filterName = Application.StartupPath + @"\Registry\MainConceptMXFMPEG_Encoder-D10_30.reg";
                        //regeditProcess = Process.Start("regedit.exe", "/s \"" + filterName + "\"");
                        //regeditProcess.WaitForExit();
                        CMARegistryTools.ManualSetupReg(filterName);


                        filterName = Application.StartupPath + @"\Registry\MainConceptMXFMultiplexer-IMX.reg";
                        //regeditProcess = Process.Start("regedit.exe", "/s \"" + filterName + "\"");
                        //regeditProcess.WaitForExit();
                        CMARegistryTools.ManualSetupReg(filterName);
                        break;
                    }

                case Codec.IMX50:// EncoderType.IMX_50:
                    {
                        filterName = Application.StartupPath + @"\Registry\MainConceptMXFMPEG_Encoder-D10_50.reg";
                        //regeditProcess = Process.Start("regedit.exe", "/s \"" + filterName + "\"");
                        //regeditProcess.WaitForExit();
                        CMARegistryTools.ManualSetupReg(filterName);


                        filterName = Application.StartupPath + @"\Registry\MainConceptMXFMultiplexer-IMX.reg";
                        //regeditProcess = Process.Start("regedit.exe", "/s \"" + filterName + "\"");
                        //regeditProcess.WaitForExit();
                        CMARegistryTools.ManualSetupReg(filterName);
                        break;
                    }

                case Codec.XDCAM_HD_1920:// EncoderType.XDCAM_HD_1920:
                    {
                        filterName = Application.StartupPath + @"\Registry\MainConceptMXFMPEG_Encoder-XDCAMHD_1920_50.reg";
                        //regeditProcess = Process.Start("regedit.exe", "/s \"" + filterName + "\"");
                        //regeditProcess.WaitForExit();
                        CMARegistryTools.ManualSetupReg(filterName);



                        filterName = Application.StartupPath + @"\Registry\MainConceptMXFMultiplexer-XDCAMHD.reg";
                        //regeditProcess = Process.Start("regedit.exe", "/s \"" + filterName + "\"");
                        //regeditProcess.WaitForExit();
                        CMARegistryTools.ManualSetupReg(filterName);
                        break;
                    }
                case Codec.DVCAM420:
                    {
                        if (fps == 29.97f)
                        {
                            filterName = Application.StartupPath + @"\Registry\ffdshow_DVCAM420_NTSC.reg";
                            CMARegistryTools.ManualSetupReg(filterName);
                            break;
                        }
                        else
                        {
                            filterName = Application.StartupPath + @"\Registry\MainConceptMXFMultiplexer-Default.reg";
                            CMARegistryTools.ManualSetupReg(filterName);
                            break;
                        }
                    }
                default:
                    {
                        filterName = Application.StartupPath + @"\Registry\MainConceptMXFMultiplexer-Default.reg";
                        //regeditProcess = Process.Start("regedit.exe", "/s \"" + filterName + "\"");
                        //regeditProcess.WaitForExit();
                        CMARegistryTools.ManualSetupReg(filterName);
                    }
                    break;
            }

            filterName = Application.StartupPath + @"\Registry\MainConceptMXFMPEG2_Decoder.reg";
            //regeditProcess = Process.Start("regedit.exe", "/s \"" + filterName + "\"");
            //regeditProcess.WaitForExit();
            CMARegistryTools.ManualSetupReg(filterName);


        }
    }
}
