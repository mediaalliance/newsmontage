﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MAMuxEncoder
{
    public class Codec
    {
        public const string DVCAM420        = "DVCAM 420";
        public const string DVCPRO50        = "DVCPRO50";
        public const string DV              = "DV";
        public const string IMX30           = "IMX 30Mb";
        public const string IMX50           = "IMX 50Mb";
        public const string XDCAM_HD_1920   = "XDCAM HD 422 1920x1080";
        public const string DEFAULT         = "Default";
    }

    public class Container
    {
        public const string MOV    = "MOV";
        public const string MXF    = "MXF";
        public const string MPG    = "MPG";
        public const string AVI    = "AVI"; 
    }
}
