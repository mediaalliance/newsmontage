using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("DESCombine")]
[assembly: AssemblyDescription("Library for combining video files")]
[assembly: AssemblyCompany("Media-Alliance")]
[assembly: AssemblyProduct("DirectShowLib")]
[assembly: AssemblyCopyright("Public Domain")]

[assembly: AssemblyVersion("1.0.0.2")]
[assembly: AssemblyTrademarkAttribute("Media-Alliance")]
[assembly: AssemblyFileVersionAttribute("1.0.0.2")]
