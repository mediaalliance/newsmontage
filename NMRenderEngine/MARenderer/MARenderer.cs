﻿using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;

//using DESCombineLib;
using DirectShowLib;
using DESCombineLib;

namespace MARenderer
{
    public class MARenderer: IDisposable
    {
        [DllImport(@"oleaut32.dll")]
        public static extern int OleCreatePropertyFrame(
            IntPtr hwndOwner,
            int x,
            int y,
            [MarshalAs(UnmanagedType.LPWStr)] string lpszCaption,
            int cObjects,
            [MarshalAs(UnmanagedType.Interface, ArraySubType = UnmanagedType.IUnknown)] 
			ref object ppUnk,
            int cPages,
            IntPtr lpPageClsID,
            int lcid,
            int dwReserved,
            IntPtr lpvReserved);

        #region Private Members

        //private IMediaControl mediaControl = null;
        //private IGraphBuilder graphBuilder = null;
        //private IBaseFilter theDevice = null;
        //private IBaseFilter theCompressor = null;
        //private double m_FPS = 25;
        
        private short m_bitCount    = 24;
        private int m_videoWidth    = 320;
        private int m_videoHeight   = 200;
        private float m_fps         = 25;           // Default PAL

        private DESCombine m_ds;

        private struct Chunk
        {
            public string sFile;
            public long lGlobalStart;
            public long lStart;
            public long lEnd;
            public int Index;

            public Chunk(string v,long g, long s, long e, int index)
            {
                Index = index;

                if (v != null && v.Length == 0)
                {
                    v = null;
                }

                sFile = v;

                lGlobalStart = g;
                lStart = s;
                lEnd = e;
            }
        }

        private ArrayList m_audioList = new ArrayList();
        private ArrayList m_movieList = new ArrayList();

        #endregion

        #region Public Members and Properties
        public class MyCallback : IDESCombineCB
        {
            private int m_FrameCount;
            private long m_TotBytes;
            private DateTime m_StartTime;
            private double m_LastSampleTime;
            private bool m_KeepTime;

            /*private TextBox m_tbBytes;
            private TextBox m_MediaTime;
            private TextBox m_ElapsedTime;
            private ProgressBar m_pb;
            */
            public DelegateOnVideoProcessEvent OnVideoProcessEvent = null;
            public DelegateOnAudioProcessEvent OnAudioProcessEvent = null;

            public MyCallback()
            {
                m_FrameCount = 0;
                m_TotBytes = 0;
                m_KeepTime = false;

                //m_tbBytes = c;
                //m_MediaTime = null;
                //m_ElapsedTime = null;
                //m_pb = null;
            }

            ~MyCallback()
            {
                //m_tbBytes.Text = m_TotBytes.ToString();
                //if (m_MediaTime != null)
                {
                    //m_MediaTime.Text = m_LastSampleTime.ToString("0.00");
                    //m_ElapsedTime.Text = (DateTime.Now - m_StartTime).ToString();
                    //m_pb.Value = m_pb.Maximum;

                    if (OnVideoProcessEvent != null)
                        OnVideoProcessEvent(m_TotBytes, m_LastSampleTime, (DateTime.Now - m_StartTime).TotalMilliseconds, IntPtr.Zero, 0);

                    if (OnAudioProcessEvent != null)
                        OnAudioProcessEvent(m_TotBytes, m_LastSampleTime, (DateTime.Now - m_StartTime).TotalMilliseconds);

                }
            }

            public void KeepTime()
            {
                m_KeepTime = true;
            }

            public int BufferCB(string sFilename, double SampleTime, System.IntPtr pBuffer, int BufferLen)
            {
                if (m_FrameCount == 0 && (OnVideoProcessEvent != null || OnAudioProcessEvent != null))
                {
                    m_StartTime = DateTime.Now;
                }

                if (m_KeepTime && (OnVideoProcessEvent != null || OnAudioProcessEvent != null))
                {
                    TimeSpan ts = DateTime.Now - m_StartTime;
                    int iSleep = (int)((SampleTime - ts.TotalSeconds) * 1000);
                    if (iSleep > 0)
                    {
                        System.Threading.Thread.Sleep(iSleep);
                    }
                }

                m_FrameCount++;
                m_TotBytes += BufferLen;

                if (m_FrameCount % 15 == 0)
                {
                    /*m_tbBytes.Text = m_TotBytes.ToString();
                    if (m_MediaTime != null)
                    {
                        m_pb.Value = (int)(SampleTime * 10);
                        m_MediaTime.Text = SampleTime.ToString("0.00");
                        m_ElapsedTime.Text = (DateTime.Now - m_StartTime).ToString();
                    }
                    */
                    if (OnVideoProcessEvent != null)
                        OnVideoProcessEvent(m_TotBytes, SampleTime, (DateTime.Now - m_StartTime).TotalMilliseconds, pBuffer, BufferLen);

                    if (OnAudioProcessEvent != null)
                        OnAudioProcessEvent(m_TotBytes, SampleTime, (DateTime.Now - m_StartTime).TotalMilliseconds);

                }
                m_LastSampleTime = SampleTime;


                return 0;
            }
        }

        public MyCallback m_pVideo = null;
        public MyCallback m_pAudio = null;

        public enum CompressorType
        {
            Video = 0,
            Audio
        }

        public long Units { get { return DESCombine.UNITS; } }

        public int Stride
        {
            get
            {
                if (m_ds != null)
                    return m_ds.GetStride();
                else
                    return 0;
            }
        }

        #endregion

        #region Public Events
        public delegate void DelegateOnVideoProcessEvent(long bytes, double mediaTime, double elapsed, System.IntPtr pBuffer, int BufferLen);
        public DelegateOnVideoProcessEvent OnVideoProcessEvent = null;

        public delegate void DelegateOnAudioProcessEvent(long bytes, double mediaTime, double elapsed);
        public DelegateOnAudioProcessEvent OnAudioProcessEvent = null;

        public delegate void DelegateStarted(long mediaLenght);
        public DelegateStarted OnStarted = null;

        public delegate void DelegateCompleted(CompletedArgs msg);
        public DelegateCompleted OnCompleted = null;

        public delegate void DelegateFileCompleted(string filename);
        public DelegateFileCompleted OnFileCompleted = null;

        #endregion

        #region Contructors 

        public MARenderer()
        {
        }

        public MARenderer(double FPS, short bitCount, int width, int height)
        {
            m_bitCount = bitCount;
            m_videoWidth = width;
            m_videoHeight = height;

            CreateDESCombine();
        }

        #endregion

        #region Public methods

        public bool CreateCombine(int width, int height, float fps)
        {
            bool res = false;
            try
            {
                DestroyDESCombine();

                m_videoWidth    = width;
                m_videoHeight   = height;
                m_fps           = fps;               

                CreateDESCombine();

                res = true;
            }
            catch { }

            return res;
        }

        public void CreateTracks(int movieTracks, int audioTracks)
        {
            if (m_ds != null)
                m_ds.CreateTracks(movieTracks, audioTracks);
        }

        public void SetupVideoCallBack(DelegateOnVideoProcessEvent videoProcessEvent)
        {
            m_pVideo = new MyCallback();
            m_pVideo.OnVideoProcessEvent = videoProcessEvent;
        }

        public void SetupAudioCallBack(DelegateOnAudioProcessEvent OnAudioProcessEvent)
        {
            m_pAudio = new MyCallback();
            m_pAudio.OnAudioProcessEvent = OnAudioProcessEvent;
        }

        public void EmptyVideoList()
        {
            m_movieList.Clear();
        }
        public void EmptyAudioList()
        {
            m_audioList.Clear();
        }

        public void AddVideo(string movieFilename, long globalStart, long start, long end, bool audioEnabled)
        {
            CreateDESCombine();

            Chunk c = new Chunk(
                                movieFilename, 
                                globalStart,
                                start,
                                /*start+*/end, m_movieList.Count);


            m_ds.AddVideoFile2(c.sFile, c.lGlobalStart, c.lStart, c.lEnd, m_movieList.Count, audioEnabled);

            m_movieList.Add(c);
        }

        public void AddAudio(string audioFilename, long globalStart, long start, long end)
        {
            CreateDESCombine();

            Chunk c = new Chunk(
                                audioFilename,
                                globalStart,
                                start,
                                /*start+*/end, m_audioList.Count);

            m_ds.AddAudioFile2(c.sFile, c.lGlobalStart, c.lStart, c.lEnd, m_audioList.Count);

            m_audioList.Add(c);
        }

        public void LoadCompressorPropertiesByName(IntPtr owner, string name)
        {
            DisplayPropertyPage(owner, GetVideoCompressor(name));
        }

        public bool Start(string filename, string videoCompresor, string audioCompressor)
        {
            bool started = false;

            try
            {
                if (m_ds != null)
                {
                    IBaseFilter ibfVideoCompressor = GetVideoCompressor(videoCompresor);
                    IBaseFilter ibfAudioCompressor = GetAudioCompressor(audioCompressor);

                    m_ds.RenderToAVI(filename, ibfVideoCompressor, null, m_pVideo, m_pAudio);


                    m_ds.Completed += new EventHandler(Completed);
                    m_ds.FileCompleted += new EventHandler(FileCompleted);

                    m_ds.StartRendering();

                    if (OnStarted != null)
                        OnStarted(m_ds.MediaLength);


                    started = true;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Start Render Error :" + ex.Message);

                DestroyDESCombine();

                started = false;                
            }

            return started;
        }

        public void Stop()
        {
            try
            {
                if (m_ds != null)
                {
                    m_ds.Cancel();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Stop Render Error :" + ex.Message);
            }

        }

        public static ArrayList GetVideoCompressorList()
        {
            ArrayList vlist = new ArrayList();

            DsDevice[] dsd = DsDevice.GetDevicesOfCat(FilterCategory.VideoCompressorCategory);
            foreach (DsDevice encoder in dsd)
                vlist.Add(encoder.Name);

            return vlist;
        }

        public static ArrayList GetAudioCompressorList()
        {
            ArrayList alist = new ArrayList();

            DsDevice[] dsdA = DsDevice.GetDevicesOfCat(FilterCategory.AudioCompressorCategory);
            foreach (DsDevice encoderA in dsdA)
                alist.Add(encoderA.Name);

            return alist;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Displays a property page for a filter
        /// </summary>
        /// <param name="dev">The filter for which to display a property page</param>
        private void DisplayPropertyPage(IntPtr owner, IBaseFilter dev)
        {
            //Get the ISpecifyPropertyPages for the filter
            ISpecifyPropertyPages pProp = dev as ISpecifyPropertyPages;
            int hr = 0;

            if (pProp == null)
            {
                //If the filter doesn't implement ISpecifyPropertyPages, try displaying IAMVfwCompressDialogs instead!
                IAMVfwCompressDialogs compressDialog = dev as IAMVfwCompressDialogs;
                if (compressDialog != null)
                {

                    hr = compressDialog.ShowDialog(VfwCompressDialogs.Config, IntPtr.Zero);
                    return;
                }
                return;
            }

            //Get the name of the filter from the FilterInfo struct
            FilterInfo filterInfo;
            hr = dev.QueryFilterInfo(out filterInfo);
            DsError.ThrowExceptionForHR(hr);

            // Get the propertypages from the property bag
            DsCAUUID caGUID;
            hr = pProp.GetPages(out caGUID);
            DsError.ThrowExceptionForHR(hr);

            // Check for property pages on the output pin
            IPin pPin = DsFindPin.ByDirection(dev, PinDirection.Output, 0);
            ISpecifyPropertyPages pProp2 = pPin as ISpecifyPropertyPages;
            if (pProp2 != null)
            {
                DsCAUUID caGUID2;
                hr = pProp2.GetPages(out caGUID2);
                DsError.ThrowExceptionForHR(hr);

                if (caGUID2.cElems > 0)
                {
                    int soGuid = Marshal.SizeOf(typeof(Guid));

                    // Create a new buffer to hold all the GUIDs
                    IntPtr p1 = Marshal.AllocCoTaskMem((caGUID.cElems + caGUID2.cElems) * soGuid);

                    // Copy over the pages from the Filter
                    for (int x = 0; x < caGUID.cElems * soGuid; x++)
                    {
                        Marshal.WriteByte(p1, x, Marshal.ReadByte(caGUID.pElems, x));
                    }

                    // Add the pages from the pin
                    for (int x = 0; x < caGUID2.cElems * soGuid; x++)
                    {
                        Marshal.WriteByte(p1, x + (caGUID.cElems * soGuid), Marshal.ReadByte(caGUID2.pElems, x));
                    }

                    // Release the old memory
                    Marshal.FreeCoTaskMem(caGUID.pElems);
                    Marshal.FreeCoTaskMem(caGUID2.pElems);

                    // Reset caGUID to include both
                    caGUID.pElems = p1;
                    caGUID.cElems += caGUID2.cElems;
                }
            }

            // Create and display the OlePropertyFrame
            object oDevice = (object)dev;
            hr = OleCreatePropertyFrame(owner, Screen.PrimaryScreen.WorkingArea.X, Screen.PrimaryScreen.WorkingArea.Y, 
                                        filterInfo.achName, 1, ref oDevice, caGUID.cElems, caGUID.pElems, 0, 0, IntPtr.Zero);
            DsError.ThrowExceptionForHR(hr);

            // Release COM objects
            Marshal.FreeCoTaskMem(caGUID.pElems);
            Marshal.ReleaseComObject(pProp);
            if (filterInfo.pGraph != null)
            {
                Marshal.ReleaseComObject(filterInfo.pGraph);
            }
        }

        private void CreateDESCombine()
        {
            if (m_ds == null)
            {
                m_ds = new DESCombine(m_fps, m_bitCount, m_videoWidth, m_videoHeight);
            }
        }

        private void DestroyDESCombine()
        {
            if (m_ds != null)
            {
                m_ds.Cancel();
                m_ds.Dispose();
                m_ds = null;
            }
        }

        private IBaseFilter GetVideoCompressor(string sName)
        {
            IBaseFilter ibf = null;

            DsDevice[] dsd = DsDevice.GetDevicesOfCat(FilterCategory.VideoCompressorCategory);
            int x;

            for (x = 0; x < dsd.Length; x++)
            {
                if (dsd[x].Name.ToLower() == sName.ToLower())
                {
                    Guid grf = typeof(IBaseFilter).GUID;
                    object o;
                    dsd[x].Mon.BindToObject(null, null, ref grf, out o);
                    ibf = o as IBaseFilter;
                    break;
                }
            }

            return ibf;
        }

        private IBaseFilter GetAudioCompressor(string sName)
        {
            IBaseFilter ibf = null;

            DsDevice[] dsd = DsDevice.GetDevicesOfCat(FilterCategory.AudioCompressorCategory);
            int x;

            for (x = 0; x < dsd.Length; x++)
            {
                if (dsd[x].Name == sName)
                {
                    Guid grf = typeof(IBaseFilter).GUID;
                    object o;
                    dsd[x].Mon.BindToObject(null, null, ref grf, out o);
                    ibf = o as IBaseFilter;
                    break;
                }
            }

            return ibf;
        }

        private void Completed(object o, System.EventArgs e)
        {
            CompletedArgs ca = e as CompletedArgs;

            try
            {
                DestroyDESCombine();
            }
            catch(Exception ex) { MessageBox.Show("DESCombine completed :"+ex.Message); }

            try
            {
                if (OnCompleted != null)
                    OnCompleted(ca);
            }
            catch { }

        }

        private void FileCompleted(object o, System.EventArgs e)
        {
            FileCompletedArgs ca = e as FileCompletedArgs;

            if(OnFileCompleted != null)     
                OnFileCompleted(ca.FileName);
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            DestroyDESCombine();
        }

        #endregion
    }
}
