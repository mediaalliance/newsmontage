﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MARenderer
{
    public partial class MAAdvancedSettingsUC : UserControl
    {
        public MAAdvancedSettingsUC()
        {
            InitializeComponent();
        }

        public bool GetVideoRes(out int wRes, out int hRes)
        {
            bool isResOk = false;
            wRes = 0;
            hRes = 0;
            string[] splitVideoRes = cbVideoRes.Text.ToLower().Split(new char[] { 'x' });
            if (splitVideoRes.Length == 2)
            {
                try
                {
                    wRes = int.Parse(splitVideoRes[0]);
                    hRes = int.Parse(splitVideoRes[1]);

                    isResOk = true;
                }
                catch
                {
                    MessageBox.Show(cbVideoRes.Text + " - is not correct", "Video Resolution Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show(cbVideoRes.Text + " - is not correct", "Video Resolution Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return isResOk;
        }
    }
}
