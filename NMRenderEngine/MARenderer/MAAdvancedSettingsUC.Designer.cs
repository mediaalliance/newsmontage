﻿namespace MARenderer
{
    partial class MAAdvancedSettingsUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MAAdvancedSettingsUC));
            this.cbAudioCompressor = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbVideoCompressor = new System.Windows.Forms.ComboBox();
            this.labCompressor = new System.Windows.Forms.Label();
            this.btnAudioCompressorProperties = new System.Windows.Forms.Button();
            this.btnVideoCompressorProperties = new System.Windows.Forms.Button();
            this.cbVideoRes = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.btnAudioCompressorProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVideoCompressorProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbVideoRes)).BeginInit();
            this.SuspendLayout();
            // 
            // cbAudioCompressor
            // 
            this.cbAudioCompressor.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbAudioCompressor.FormattingEnabled = true;
            this.cbAudioCompressor.Location = new System.Drawing.Point(15, 101);
            this.cbAudioCompressor.Name = "cbAudioCompressor";
            this.cbAudioCompressor.Size = new System.Drawing.Size(252, 23);
            this.cbAudioCompressor.TabIndex = 120;
            this.cbAudioCompressor.Text = "PCM";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(12, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 23);
            this.label3.TabIndex = 119;
            this.label3.Text = "Audio Compressor";
            // 
            // cbVideoCompressor
            // 
            this.cbVideoCompressor.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbVideoCompressor.FormattingEnabled = true;
            this.cbVideoCompressor.Location = new System.Drawing.Point(15, 30);
            this.cbVideoCompressor.Name = "cbVideoCompressor";
            this.cbVideoCompressor.Size = new System.Drawing.Size(161, 23);
            this.cbVideoCompressor.TabIndex = 118;
            this.cbVideoCompressor.Text = "ffdshow video encoder";
            // 
            // labCompressor
            // 
            this.labCompressor.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labCompressor.ForeColor = System.Drawing.Color.White;
            this.labCompressor.Location = new System.Drawing.Point(12, 11);
            this.labCompressor.Name = "labCompressor";
            this.labCompressor.Size = new System.Drawing.Size(124, 23);
            this.labCompressor.TabIndex = 117;
            this.labCompressor.Text = "Video Compressor";
            // 
            // btnAudioCompressorProperties
            // 
            this.btnAudioCompressorProperties.BackColor = System.Drawing.Color.Transparent;
            this.btnAudioCompressorProperties.Font = new System.Drawing.Font("Arial", 8.25F);
            this.btnAudioCompressorProperties.ForeColor = System.Drawing.Color.Black;
            this.btnAudioCompressorProperties.Image = ((System.Drawing.Image)(resources.GetObject("btnAudioCompressorProperties.Image")));
            this.btnAudioCompressorProperties.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAudioCompressorProperties.Location = new System.Drawing.Point(274, 101);
            this.btnAudioCompressorProperties.Name = "btnAudioCompressorProperties";
            // 
            // 
            // 
            this.btnAudioCompressorProperties.Size = new System.Drawing.Size(31, 23);
            this.btnAudioCompressorProperties.TabIndex = 123;
            // 
            // btnVideoCompressorProperties
            // 
            this.btnVideoCompressorProperties.BackColor = System.Drawing.Color.Transparent;
            this.btnVideoCompressorProperties.Font = new System.Drawing.Font("Arial", 8.25F);
            this.btnVideoCompressorProperties.ForeColor = System.Drawing.Color.Black;
            this.btnVideoCompressorProperties.Image = ((System.Drawing.Image)(resources.GetObject("btnVideoCompressorProperties.Image")));
            this.btnVideoCompressorProperties.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnVideoCompressorProperties.Location = new System.Drawing.Point(274, 30);
            this.btnVideoCompressorProperties.Name = "btnVideoCompressorProperties";
            // 
            // 
            // 
            this.btnVideoCompressorProperties.Size = new System.Drawing.Size(31, 23);
            this.btnVideoCompressorProperties.TabIndex = 122;
            // 
            // cbVideoRes
            // 
            this.cbVideoRes.Font = new System.Drawing.Font("Arial", 8.25F);
            this.cbVideoRes.ForeColor = System.Drawing.Color.Black;
            this.cbVideoRes.Location = new System.Drawing.Point(182, 30);
            this.cbVideoRes.Name = "cbVideoRes";
            // 
            // 
            // 
            this.cbVideoRes.Size = new System.Drawing.Size(85, 24);
            this.cbVideoRes.TabIndex = 124;
            this.cbVideoRes.Text = "720 x 576";
            // 
            // MAAdvancedSettingsUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.DodgerBlue;
            this.Controls.Add(this.cbVideoRes);
            this.Controls.Add(this.btnAudioCompressorProperties);
            this.Controls.Add(this.btnVideoCompressorProperties);
            this.Controls.Add(this.cbAudioCompressor);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbVideoCompressor);
            this.Controls.Add(this.labCompressor);
            this.Name = "MAAdvancedSettingsUC";
            this.Size = new System.Drawing.Size(321, 147);
            ((System.ComponentModel.ISupportInitialize)(this.btnAudioCompressorProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVideoCompressorProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbVideoRes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbAudioCompressor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbVideoCompressor;
        private System.Windows.Forms.Label labCompressor;
        private System.Windows.Forms.Button btnAudioCompressorProperties;
        private System.Windows.Forms.Button btnVideoCompressorProperties;
        private System.Windows.Forms.ComboBox cbVideoRes;


    }
}
