﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Diagnostics;
using MediaAlliance.NewsMontage.RenderTM.Classes;
using System.ComponentModel;
using System.Xml;
using System.Threading;



namespace MediaAlliance.NewsMontage.RenderTM
{
    public class CRenderTM
    {
        // DELEGATES
        public delegate void Render_OnRun(object Sender, string command_exec);
        public delegate void Render_OnProgress(object Sender, CRenderOutputInfo output);
        public delegate void Render_OnInfo(object Sender, CRenderOutputInfo output);
        public delegate void Render_OnVerboseInfo(object Sender, CRenderOutputInfo output);
        public delegate void Render_OnFinish(object Sender, CRenderOutputInfo output, enRenderCmdType commandType);
        public delegate void Render_OnEngineNotFound(object Sender);

        // EVENTS
        public event Render_OnRun OnRun                                 = null;
        public event Render_OnProgress OnProgress                       = null;
        public event Render_OnInfo OnInfo                               = null;
        public event Render_OnVerboseInfo OnVerboseInfo                 = null;
        public event Render_OnFinish OnFinish                           = null;
        public event Render_OnEngineNotFound OnEngineNotFound           = null;
        
        // CLASSES

        // FIELDS

        private bool isActive = false;
        private string renderEngine_path = "";
        private string renameRenderExeFilename = "";


        public List<string> Profiles = new List<string>();

        private List<CRenderTaskInfo> Tasks = new List<CRenderTaskInfo>();
        private List<CRenderOutputInfo> ReceivedQue = new List<CRenderOutputInfo>();

        private enRenderCmdType CurrentCommand = enRenderCmdType.none;

        private string m_syncreply = "";
        private ManualResetEvent m_sync_mre = new ManualResetEvent(false);


        public CRenderTM()
        { 

        }



        private void ExecuteCommand(enRenderCmdType cmdType, params string[] arguments)
        {
            ExecuteCommand(cmdType, false, 0, arguments);
        }

        private void ExecuteCommand(enRenderCmdType cmdType, bool wait, int timeout, params string[] arguments)
        {
            if (isActive)
            {
                CurrentCommand = cmdType;

                switch (cmdType)
                {
                    case enRenderCmdType.RequestInfo:
                        arguments = new string[] { "/?" };
                        break;
                    case enRenderCmdType.Render:
                    case enRenderCmdType.ProfileInfo:
                        break;
                    case enRenderCmdType.ProfileList:
                        Profiles.Clear();
                        arguments = new string[] { "/profiles" };
                        break;
                }

                string exe_filename = renderEngine_path;

                if (renameRenderExeFilename.Trim() != "")
                {
                    exe_filename = Path.Combine(new FileInfo(exe_filename).DirectoryName, renameRenderExeFilename);
                }

                Process p = new Process();

                CRenderTaskInfo tInfo               = new CRenderTaskInfo(p, wait);
                tInfo.CmdType                       = cmdType;
                tInfo.Arguments                     = arguments;

                p.StartInfo.WorkingDirectory        = new FileInfo(exe_filename).DirectoryName;
                p.StartInfo.FileName                = "\"" + exe_filename + "\"";
                p.StartInfo.Arguments               = string.Join(" ", arguments);

                p.StartInfo.RedirectStandardOutput  = true;
                p.StartInfo.RedirectStandardInput   = true;
                p.StartInfo.UseShellExecute         = false;
                p.EnableRaisingEvents               = true;

                p.Exited                += new EventHandler(Process_Exited);
                p.ErrorDataReceived     += new DataReceivedEventHandler(Process_ErrorDataReceived);
                p.OutputDataReceived    += new DataReceivedEventHandler(Process_OutputDataReceived);

                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

                if (OnRun != null)
                {
                    OnRun(this, p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                }

                if (p.Start())
                {
                    tInfo.IsWorking = true;
                }
                p.BeginOutputReadLine();

                Tasks.Add(tInfo);

                if (wait)
                {
                    if (timeout > 0)
                    {
                        m_sync_mre.WaitOne(timeout);
                    }
                    else
                    {
                        m_sync_mre.WaitOne();
                    }

                    m_sync_mre.Reset();


                }
            }
        }

        private CRenderTaskInfo GetTaskInfoFromProcess(Process p)
        {
            CRenderTaskInfo result = null;

            List<CRenderTaskInfo> tasks = Tasks.Where(X => X.MyProcess == p).ToList();

            if (Tasks.Count > 0)
            {
                result = tasks[0];
            }

            return result;
        }




        private void Raise_OnProgress(CRenderOutputInfo output)
        {
            if (OnProgress != null)
            {
                foreach (Render_OnProgress singleCast in OnProgress.GetInvocationList())
                {
                    ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;
                    
                    if (syncInvoke != null && syncInvoke.InvokeRequired)
                    {
                        syncInvoke.Invoke(OnProgress, new object[] { this, output });
                    }
                    else
                    {
                        singleCast(this, output);
                    }
                }
            }
        }

        private void Raise_OnInfo(CRenderOutputInfo output)
        {
            if (OnInfo != null)
            {
                foreach (Render_OnInfo singleCast in OnInfo.GetInvocationList())
                {
                    ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;

                    if (syncInvoke != null && syncInvoke.InvokeRequired)
                    {
                        syncInvoke.Invoke(OnInfo, new object[] { this, output });
                    }
                    else
                    {
                        singleCast(this, output);
                    }
                }
            }
        }

        private void Raise_OnVerboseInfo(CRenderOutputInfo output)
        {
            if (OnInfo != null)
            {
                foreach (Render_OnVerboseInfo singleCast in OnVerboseInfo.GetInvocationList())
                {
                    ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;

                    if (syncInvoke != null && syncInvoke.InvokeRequired)
                    {
                        syncInvoke.Invoke(OnVerboseInfo, new object[] { this, output });
                    }
                    else
                    {
                        singleCast(this, output);
                    }
                }
            }
        }

        private void Raise_OnFinish(CRenderOutputInfo output, enRenderCmdType commandType)
        {
            if (OnInfo != null)
            {
                foreach (Render_OnFinish singleCast in OnFinish.GetInvocationList())
                {
                    ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;

                    if (syncInvoke != null && syncInvoke.InvokeRequired)
                    {
                        syncInvoke.Invoke(OnFinish, new object[] { this, output, commandType });
                    }
                    else
                    {
                        singleCast(this, output, commandType);
                    }
                }
            }
        }



        void Process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            CRenderTaskInfo rInfo = GetTaskInfoFromProcess((Process)sender);

            CRenderOutputInfo outInfo = new CRenderOutputInfo(e.Data);
            outInfo.TaskInfo = rInfo;

            if (rInfo.IsWaiting)
            {
                // SE IL TASK E' STATO RICHIESTO PER UN RISPOSTA SINCRONA

                switch (outInfo.InfoType)
                {
                    case enEngineInfoType.unknown:
                        break;
                    case enEngineInfoType.localInfo:
                        break;
                    case enEngineInfoType.info:
                        m_syncreply = e.Data;
                        break;
                    case enEngineInfoType.progress:
                        break;
                    case enEngineInfoType.error:
                        break;
                    case enEngineInfoType.finish:
                        m_sync_mre.Set();
                        break;
                    case enEngineInfoType.reply:
                        break;
                    case enEngineInfoType.verbose:
                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch (outInfo.InfoType)
                {
                    case enEngineInfoType.localInfo:
                        break;
                    case enEngineInfoType.info:
                        Raise_OnInfo(outInfo);
                        break;
                    case enEngineInfoType.progress:
                        Raise_OnProgress(outInfo);
                        break;
                    case enEngineInfoType.finish:
                        Raise_OnFinish(outInfo, CurrentCommand);
                        CurrentCommand = enRenderCmdType.none;
                        break;
                    case enEngineInfoType.verbose:
                        Raise_OnVerboseInfo(outInfo);
                        break;
                    case enEngineInfoType.reply:
                        Profiles.Add(outInfo.Message.Trim());
                        break;
                }
            }
            //ReceivedQue.Add(outInfo);
        }

        void Process_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            CRenderTaskInfo rInfo = GetTaskInfoFromProcess((Process)sender);
        }

        void Process_Exited(object sender, EventArgs e)
        {
            CRenderTaskInfo rInfo = GetTaskInfoFromProcess((Process)sender);

            rInfo.IsWorking = false;
        }




        public void RefreshInfo()
        {
            if (isActive)
            {
                ExecuteCommand(enRenderCmdType.RequestInfo);
            }
        }

        public void Render(string project_path, string destination_path, string profile)
        {
            Render(project_path, destination_path, profile, new int[0]);
        }

        public void Render(string project_path, string destination_path, string profile, int[] tracks_ids)
        {
            if (isActive)
            {
                if (renameRenderExeFilename.Trim() != "")
                {
                    try
                    {
                        //string exe_dir = new FileInfo(renderEngine_path).DirectoryName;
                        //string exe_fake = Path.Combine(exe_dir, renameRenderExeFilename);

                        //if (File.Exists(exe_fake))
                        //{
                        //    File.Delete(exe_fake);
                        //}

                        //File.Copy(renderEngine_path, exe_fake, true);
                    }
                    catch
                    { 
                    }
                }

                string args = "/p \"" + project_path + "\" /o \"" + destination_path + "\"";

                if (profile.Trim() != "")
                {
                    args += " /i " + profile;
                }
                
                if (tracks_ids != null && tracks_ids.Length > 0)
                {
                    List<string> trkid = new List<string>();
                    foreach(int id in tracks_ids) trkid.Add(id.ToString());

                    args += " /t " + string.Join(",", trkid.ToArray());
                }

                ExecuteCommand(enRenderCmdType.Render, args);
            }            
        }

        public string GetProfileInfo(string profilename, string profileinfo)
        {
            string result = "";

            if (isActive)
            {
                string args = "/pinfo " + profileinfo + " " + profilename;

                try
                {
                    ExecuteCommand(enRenderCmdType.ProfileInfo, true, 3000, args);
                }
                catch
                { 

                }

                result = m_syncreply.Replace("[I]", "").Trim();
            }

            return result;
        }

        public string RenameRenderExeFilename
        {
            get { return renameRenderExeFilename; }
            set
            {
                renameRenderExeFilename = value;
            }
        }

        public string RenderEnginePath
        {
            get { return renderEngine_path; }
            set
            {
                renderEngine_path = value;
                isActive = false;


                if (renderEngine_path.Trim() != "")
                {
                    if (File.Exists(renderEngine_path))
                    {
                        isActive = true;
                    }
                }

                if (!isActive)
                {
                    Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("Software\\MediaAlliance\\NMRenderEngine");

                    if (key != null)
                    {
                        string path = (string)key.GetValue("path");

                        if (File.Exists(path))
                        {
                            renderEngine_path = path;
                            isActive = true;
                        }
                    }
                }

                if (!isActive)
                {
                    if (OnEngineNotFound != null) OnEngineNotFound(this);
                }
                else
                {
                    GetProfiles();
                }
            }
        }

        private void GetProfiles()
        {
            ExecuteCommand(enRenderCmdType.ProfileList);
        }

        public string ProfilesPath
        {
            get { return Path.Combine(RenderEnginePath, "Profiles"); }
        }
        
        public string GetProfile_OutputExtension(string profile_file)
        {
            if (File.Exists(profile_file))
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(profile_file);

                if (doc["RenderProfile"] != null)
                {
                    if (doc["RenderProfile"]["OutputExtension"] != null)
                    {
                        return doc["RenderProfile"]["OutputExtension"].InnerText;
                    }
                }
            }

            return "";
        }
    }
}
