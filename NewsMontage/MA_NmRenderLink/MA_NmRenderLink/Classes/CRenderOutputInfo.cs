﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MediaAlliance.NewsMontage.RenderTM.Classes
{
    public class CRenderOutputInfo
    {
        public string OriginalLine          = "";
        public enEngineInfoType InfoType    = enEngineInfoType.unknown;
        public string Message               = "";
        public float ProgressValue          = 0F;
        public CRenderTaskInfo TaskInfo     = null;

        public CRenderOutputInfo(string original_line)
        {
            this.OriginalLine = original_line;
            Parse(original_line);
        }

        private void Parse(string line)
        {
            if (line == null)
            {
                this.InfoType = enEngineInfoType.unknown;
                return;
            }

            if(line.Trim() == "")
            {
                this.InfoType = enEngineInfoType.unknown;
                return;
            }

            if(!line.Trim().StartsWith("["))
            {
                this.InfoType = enEngineInfoType.localInfo;
                return;
            }

            if(line.Trim().StartsWith("[I]", StringComparison.OrdinalIgnoreCase))
            {
                this.InfoType   = enEngineInfoType.info;
                this.Message    = line.Substring(3).Trim();
                return;
            }

            if(line.Trim().StartsWith("[E]", StringComparison.OrdinalIgnoreCase))
            {
                this.InfoType   = enEngineInfoType.error;
                this.Message    = line.Substring(3).Trim();
                return;
            }

            if (line.Trim().StartsWith("[R]", StringComparison.OrdinalIgnoreCase))
            {
                this.InfoType = enEngineInfoType.reply;
                this.Message = line.Substring(3).Trim();
                return;
            }

            if(line.Trim().StartsWith("[%]", StringComparison.OrdinalIgnoreCase))
            {
                this.InfoType       = enEngineInfoType.progress;
                this.Message        = line.Substring(3).Trim();
                this.ProgressValue  = 0;
                float.TryParse(this.Message, out this.ProgressValue);
                return;
            }

            if (line.Trim().StartsWith("[F]", StringComparison.OrdinalIgnoreCase))
            {
                this.InfoType = enEngineInfoType.finish;
                this.Message = line.Substring(3).Trim();
                return;
            }

            if (line.Trim().StartsWith("[V]", StringComparison.OrdinalIgnoreCase))
            {
                this.InfoType = enEngineInfoType.verbose;
                this.Message = line.Substring(3).Trim();
                return;
            }

        }
    }
}
