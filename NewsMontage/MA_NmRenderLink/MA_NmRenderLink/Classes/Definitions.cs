﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MediaAlliance.NewsMontage.RenderTM
{
    public enum enRenderCmdType
    { 
        none,
        RequestInfo,
        Render,
        ProfileList,
        ProfileInfo
    }

    public enum enEngineInfoType
    { 
        unknown,                // non riconosciuta
        localInfo,              // informazioni personali dell'engine non utili da monitorare
        info,                   // info spedite al manager
        progress,               // valore percentuale di progress
        error,                  // stringa di errore
        finish,
        reply,
        verbose
    }
}
