﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Diagnostics;




namespace MediaAlliance.NewsMontage.RenderTM.Classes
{
    public class CRenderTaskInfo
    {
        public Process MyProcess        = null;
        public enRenderCmdType CmdType  = enRenderCmdType.none;
        public bool IsWorking           = false;
        public DateTime LastPing        = DateTime.Now;
        public string[] Arguments       = new string[0];
        public bool IsWaiting           = false;

        public CRenderTaskInfo(Process proc, bool isWaiting)
        {
            this.MyProcess = proc;
            this.IsWaiting = isWaiting;
        }
    }
}
