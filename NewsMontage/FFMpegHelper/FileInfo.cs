﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
//using VIVA.Utilities;
using System.Diagnostics;
using System.Threading;

namespace FFMpegHelper
{
    public class FileInfo
    {
        private Process m_process = null;
        private int m_nAudioTracks = 0;
        private int m_bitPerSample = 16;
        private bool m_multipleAudioStream = false;
        private EnumTrackType m_trackType = EnumTrackType.Mono;

        public enum EnumTrackType
        {
            Mono,
            Stereo,
            Quad,
            Dolby51,
            Dolby71
        }
        public int AudioChannels { get { return m_nAudioTracks; } }

        public int BitPerSecond { get { return m_bitPerSample; } }

        public bool MultipleAudioStream { get { return m_multipleAudioStream; } }

        public EnumTrackType TrackType { get { return m_trackType; } }

        public int CheckForAudio(string inVideoFile)
        {
            int ret = 0;

            m_process = new Process();
            m_process.StartInfo.FileName = Application.StartupPath + @"\StartProcessForAudioInfo.bat";

            string tempFileName = Application.StartupPath + @"\audioinfo.txt";

//            m_process.StartInfo.Arguments = string.Format(@"-i ""{0}"" > ""{1}"" 2>>&1", inVideoFile, tempFileName);
            m_process.StartInfo.Arguments = string.Format(@" ""{0}"" ""{1}"" ", inVideoFile, tempFileName);

            m_process.StartInfo.CreateNoWindow = true;
            m_process.StartInfo.UseShellExecute = false;
            m_process.StartInfo.RedirectStandardOutput = false;
            m_process.StartInfo.RedirectStandardError = false;
            m_process.StartInfo.WorkingDirectory = ".\\";

            try
            {
                m_process.Start();
                m_process.WaitForExit();
                ret = m_process.ExitCode;


                //Open ascii file anf check result
                try
                {
                    using (StreamReader sr = new StreamReader(tempFileName))
                    {
                        while (!sr.EndOfStream)
                        {
                            string strLine = sr.ReadLine();

                            //Remove all space to from start position
                            strLine = strLine.TrimStart(new char[] { ' ' }).ToLower();

                            if (strLine.StartsWith("stream"))
                            {
                                int nStartAudioInfo = strLine.IndexOf("audio");

                                //Founded audio information
                                if (nStartAudioInfo > 0)
                                {
                                    if (m_nAudioTracks > 0)
                                        m_multipleAudioStream = true;

                                    m_nAudioTracks++;

                                    if (strLine.Contains("stereo"))
                                    {
                                        m_nAudioTracks++;
                                        m_trackType = EnumTrackType.Stereo;
                                    }
                                    if (strLine.Contains("quad"))
                                    {
                                        m_nAudioTracks += 3;
                                        m_trackType = EnumTrackType.Quad;
                                    }
                                    if (strLine.Contains("7.1"))
                                    {
                                        m_nAudioTracks += 7;
                                        m_trackType = EnumTrackType.Dolby71;
                                    }

                                    if (strLine.Contains("5.1"))
                                    {
                                        m_nAudioTracks += 5;
                                        m_trackType = EnumTrackType.Dolby51;
                                    }


                                    if (strLine.Contains("pcm"))
                                    {
                                        int nStartPCMInfo = strLine.IndexOf("pcm");
                                        if (nStartPCMInfo > 0)
                                        {
                                            string strBPS = strLine.Substring(nStartPCMInfo + 5, 2);
                                            m_bitPerSample = int.Parse(strBPS);
                                        }
                                    }
                                }

                            }
                        }
                    }


                }
                catch (Exception ex) 
                { 
                    //CLog.Log(" Check For Audio information :" + ex.Message); 
                }

            }
            catch (Exception ex)
            {
                ret = -1;
            }
            finally
            {
                if (m_process != null)
                {
                    m_process.Close();
                    m_process.Dispose();
                    m_process = null;
                }

                if (File.Exists(tempFileName))
                    File.Delete(tempFileName);
            }

            return ret;
        }

        public void Stop()
        {
            if (m_process != null)
            {
                m_process.Kill();
                m_process.Close();
                m_process = null;
            }
        }
 
    }
}
