﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Security.Permissions;

using System.Diagnostics;
using System.Threading;
using System.Runtime.InteropServices;

[assembly:PermissionSetAttribute(SecurityAction.RequestMinimum, Name = "FullTrust")]


//ffmpeg -i "C:\DEVELOP\MediaAlliance\DEMO MXF\adri_DVCPRO50_2.mxf" -y -sameq -ss 25 -T 2 -s 100x80 -f image2 -r 1/5 .\frames\nome%03d.png

namespace FFMpegHelper
{


    public class Multiplexer
    {
        private Process m_process = null;

        public int Start(string aviFile, string dvFile)
        {
            int ret = 0;

            m_process = new Process();
            m_process.StartInfo.FileName = Application.StartupPath + @"\ffmpeg.exe";
            m_process.StartInfo.Arguments = string.Format(@"-i {0} -pix_fmt yuv422p {1}", aviFile, dvFile);
            m_process.StartInfo.CreateNoWindow = true;
            m_process.StartInfo.UseShellExecute = false;
            m_process.StartInfo.RedirectStandardOutput = false;
            m_process.StartInfo.RedirectStandardError = false;
            m_process.StartInfo.WorkingDirectory = ".\\";

            bool started = true;
            try
            {
                m_process.Start();
                m_process.WaitForExit();
                ret = m_process.ExitCode;

            }
            catch (Exception ex)
            {
                ret = -1;
            }
            finally
            {
                if(m_process != null)
                {
                    m_process.Close();
                    m_process.Dispose();
                    m_process = null;
                }

            }

            return ret;
        }

        public void Stop()
        {
            if (m_process != null)
            {
                m_process.Kill();
                m_process.Close();
                m_process = null;
            }
        }
    }


    public class SceneChange
    {
        private Process m_process = null;

        public int Start(string srcFile, int startSecs, int durSecs, string destFolder)
        {
            int ret = 0;

            //ffmpeg -i "C:\DEVELOP\MediaAlliance\DEMO MXF\adri_DVCPRO50_2.mxf" -y -sameq -ss 25 -T 2 -s 100x80 -f image2 -r 1/5 .\frames\nome%03d.png

            try
            {
                m_process = new Process();
                m_process.StartInfo.FileName = Application.StartupPath + @"\ffmpeg.exe";
                m_process.StartInfo.Arguments = string.Format(@"-i ""{0}"" -y -sameq -ss {1} -t {2} -s 100x80 -f image2 -r 1/5 ""{3}\frame%06d.jpg""",
                                                              srcFile, startSecs, durSecs, destFolder);
                m_process.StartInfo.CreateNoWindow = true;
                m_process.StartInfo.UseShellExecute = false;
                m_process.StartInfo.RedirectStandardOutput = false;
                m_process.StartInfo.RedirectStandardError = false;
                m_process.StartInfo.WorkingDirectory = ".\\";

                m_process.Start();
                //m_process.WaitForExit();
                //ret = m_process.ExitCode;

            }
            catch (Exception ex)
            {
                ret = -1;
            }
            finally
            {
            }

            return ret;
        }

        public void Stop()
        {
            try
            {
                if (m_process != null)
                {
                    m_process.Kill();
                    m_process.Close();
                    m_process = null;
                }
            }
            catch { }
        }
 
    }

    public class AudioExtract
    {
        private Process m_process = null;

        public int Start(string srcFile, int startSecs, int durSecs, int startMilliSecs, int durMillisecs, string destFolder, int idTrack)
        {
            int ret = 0;

            //ffmpeg -i "C:\DEVELOP\MediaAlliance\DEMO MXF\adri_DVCPRO50_2.mxf" -y -sameq -ss 25 -T 2 -s 100x80 -f image2 -r 1/5 .\frames\nome%03d.png

            try
            {
                
                m_process = new Process();
                m_process.StartInfo.FileName = Application.StartupPath + @"\ffmpeg.exe";
                m_process.StartInfo.Arguments = string.Format(@"-i ""{0}"" -ss {1}.{5} -t {2}.{6} -map 0:{3}  ""{4}""",
                                                              srcFile, startSecs, durSecs, idTrack, destFolder, startMilliSecs, durMillisecs);
                m_process.StartInfo.CreateNoWindow = true;
                m_process.StartInfo.UseShellExecute = false;
                m_process.StartInfo.RedirectStandardOutput = false;
                m_process.StartInfo.RedirectStandardError = false;
                m_process.StartInfo.WorkingDirectory = ".\\";

                bool started = true;
                try
                {
                    m_process.Start();
                    m_process.WaitForExit();
                    ret = m_process.ExitCode;

                }
                catch (Exception ex)
                {
                    ret = -1;                    
                }
                finally
                {
                    if (m_process != null)
                    {
                        m_process.Close();
                        m_process.Dispose();
                        m_process = null;
                    }

                }
            

            }
            catch (Exception ex)
            {
                ret = -1;
            }
            finally
            {
            }

            return ret;
        }

        public void Stop()
        {
            try
            {
                if (m_process != null)
                {
                    m_process.Kill();
                    m_process.Close();
                    m_process = null;
                }
            }
            catch { }
        }

    }

}
