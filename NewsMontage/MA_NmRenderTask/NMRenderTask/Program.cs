﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Reflection;
using MediaAlliance.NewsMontage.RenderEngine.RenderEmulator;
using System.Threading;



namespace MediaAlliance.NewsMontage.RenderEngine
{
    class Program
    {
        private static string projectFile           = "";
        private static ManualResetEvent mre         = new ManualResetEvent(false);
        private static string last_engine_message   = "";


        static void Main(string[] args)
        {
            CWriter.WriteLocalInfo(" ========================================= ");
            CWriter.WriteLocalInfo("  News Montage Render Task ");
            CWriter.WriteLocalInfo("  version : " + Assembly.GetExecutingAssembly().GetName().Version.ToString());
            CWriter.WriteLocalInfo(" ========================================= ");

            Environment.ExitCode = 0;


            CRenderEmulator render_engine = null;

            switch (ParseArgumentAction(args))
            {
                case argumentAction.Undefined:
                    CWriter.WriteError("Undifined action on arguments");
                    Environment.ExitCode = 1;
                    mre.Set();
                    break;
                case argumentAction.RequestInfo:
                    mre.Set();
                    break;
                case argumentAction.Help:
                    CWriter.WriteHelp();
                    mre.Set();
                    break;
                case argumentAction.Render:

                    render_engine = new CRenderEmulator(projectFile);

                    render_engine.OnStart       += new CRenderEmulator.Render_OnStart(render_engine_OnStart);
                    render_engine.OnProgress    += new CRenderEmulator.Render_OnProgress(render_engine_OnProgress);
                    render_engine.OnFinish      += new CRenderEmulator.Render_OnFinish(render_engine_OnFinish);

                    render_engine.Render();

                    break;
                default:
                    break;
            }

            mre.WaitOne();

            CWriter.WriteInfo("Finish");

            if(render_engine != null) render_engine.Dispose();
        }

        static void render_engine_OnFinish(object Sender, string step_descrition)
        {
            mre.Set();
        }

        static void render_engine_OnProgress(object Sender, string step_descrition, int percent)
        {
            if (last_engine_message != step_descrition)
            {
                last_engine_message = step_descrition;
                CWriter.WriteInfo(step_descrition);
            }
            CWriter.WriteProgress(percent);
        }

        static void render_engine_OnStart(object Sender, string step_descrition)
        {
            throw new NotImplementedException();
        }

        enum argumentAction
        {
            Undefined,
            RequestInfo,
            Help,
            Render
        }

        static argumentAction ParseArgumentAction(string[] args)
        {
            string joined = string.Join(" ", args);

            if (joined.IndexOf("-h", StringComparison.OrdinalIgnoreCase) >= 0 ||
                joined.IndexOf("/h", StringComparison.OrdinalIgnoreCase) >= 0)
            {
                return argumentAction.Help;
            }

            if (joined.IndexOf("-?", StringComparison.OrdinalIgnoreCase) >= 0 ||
                joined.IndexOf("/?", StringComparison.OrdinalIgnoreCase) >= 0)
            {
                return argumentAction.RequestInfo;
            }

            if (args.Length == 1)
            { 
                try
                {
                    FileInfo fi = new FileInfo(args[0]);

                    if (fi.Exists)
                    {
                        projectFile = fi.FullName;
                        return argumentAction.Render;
                    }
                }
                catch (System.Exception ex)
                {
                    CWriter.WriteError("Invalid arguments or filename");
                    CWriter.WriteError("   > " + ex.Message);
                }
            }

            return argumentAction.Undefined;
        }
    }
}
