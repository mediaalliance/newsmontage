﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MediaAlliance.NewsMontage.RenderEngine.ProjectParser
{
    public enum SourceType
    {
        unknown,
        voice,
        av_a,
        av_b,
        background
    }


    public class CClipInfo
    {
        public SourceType type              = SourceType.unknown;
        public Guid id                      = Guid.Empty;

        public long Framein                 = 0;
        public long ClippingIn              = 0;
        public long ClippingOut             = 0;
        public long ClipDuration            = 0;

        public int Source_id                = 0;
        public string Source_Title          = "";
        public string Source_Description    = "";
        public string Source_Filename       = "";
        public bool Source_IsFullclip       = false;
        public long Source_Duration         = 0;
        public string Source_AssetId        = "";
        public int Source_Used              = 0;

        private float audioGlobal           = 1F;

        public List<CAudioNode> AudioNodes = new List<CAudioNode>();

        public float AudioGlobal
        {
            get { return audioGlobal; }
            set { audioGlobal = value; }
        }
    }

    public class CAudioNode
    {
        private long timecode = 0;
        private float realValue = 0F;

        public bool TrackDisabled = false;



        public CAudioNode(long tc, float val)
        {
            this.timecode = tc;
            this.realValue = val;
        }


        public long Timecode
        {
            get { return timecode; }
        }

        public float Value
        {
            get { return (TrackDisabled) ? 0 : realValue; }
        }

        public float RealValue
        {
            get { return realValue; }
        }
    }

    public class CClipInfo_Collection : List<CClipInfo>
    {
        private bool videoDisabled = false;
        private bool audioDisabled = false;

        public long WorkingArea_Duration
        {
            get
            {
                long tcout = -1;

                foreach (CClipInfo clip in this)
                {
                    if (tcout < 0)
                    {
                        tcout = clip.Framein + clip.ClipDuration;
                    }
                    else
                    {
                        if (clip.Framein + clip.ClipDuration > tcout) tcout = clip.Framein + clip.ClipDuration;
                    }
                }

                return tcout;
            }
        }

        public bool VideoDisabled
        {
            get { return videoDisabled; }
            set
            {
                videoDisabled = value;
            }
        }

        public bool AudioDisabled
        {
            get { return audioDisabled; }
            set
            {
                audioDisabled = value;

                foreach (CClipInfo cInfo in this)
                {
                    foreach (CAudioNode aNode in cInfo.AudioNodes)
                    {
                        aNode.TrackDisabled = value;
                    }
                }
            }
        }
    }
}
