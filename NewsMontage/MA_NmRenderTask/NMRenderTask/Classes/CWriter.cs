﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MediaAlliance.NewsMontage.RenderEngine
{
    public class CWriter
    {
        public static void WriteLocalInfo(string message)
        {
            Console.WriteLine(message);
        }

        public static void WriteProgress(int percent)
        {
            Console.WriteLine("[%] " + percent);
        }

        public static void WriteError(string message)
        {
            Console.WriteLine("[E] " + message);
        }

        public static void WriteInfo(string message)
        {
            Console.WriteLine("[I] " + message);
        }

        public static void WriteHelp()
        {
            Console.WriteLine(" Help");
            Console.WriteLine(" ....");
            Console.WriteLine(" ....");
            Console.WriteLine(" ....");
            Console.WriteLine(" ....");
        }
    }
}
