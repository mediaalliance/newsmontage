﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Threading;
using System.ComponentModel;
using MediaAlliance.NewsMontage.RenderEngine.ProjectParser;



namespace MediaAlliance.NewsMontage.RenderEngine.RenderEmulator
{
    public class CRenderEmulator : IDisposable
    {
        // DELEGATES
        public delegate void Render_OnStart(object Sender, string step_descrition);
        public delegate void Render_OnProgress(object Sender, string step_descrition, int percent);
        public delegate void Render_OnFinish(object Sender, string step_descrition);

        // EVENTS
        public event Render_OnStart OnStart         = null;
        public event Render_OnProgress OnProgress   = null;
        public event Render_OnFinish OnFinish       = null;

        // CLASSES
        private CProjectsParser project = null;


        private Thread myThread = null;
        private bool myThread_Stop = false;
        private int percent = 0;

        private string projectFile = "";


        public CRenderEmulator(String projectFile)
        {
            this.projectFile = projectFile;

            project = new CProjectsParser(projectFile);
        }

        private void myThread_Task()
        {
            Raise_Render_OnStart("Rending operation started");

            Random rnd = new Random(DateTime.Now.Millisecond);

            while (!myThread_Stop)
            {
                int sleep_time = rnd.Next(200, 800);
                int increment = rnd.Next(1, 2);

                percent += increment;

                if (percent > 100) percent = 100;

                string message = "";

                if (percent < 100)
                {
                    message = "Finalizing process and clear...";
                }

                if (percent < 80)
                {
                    message = "Rendering video and audio...";
                }
                
                if (percent < 50)
                {
                    message = "Rendering audio tracks...";
                }

                if (percent < 30)
                {
                    message = "Loading footage...";
                }

                if (percent < 10)
                {
                    message = "Parsing project file...";
                }


                Raise_Render_OnProgress(message, percent);

                Thread.Sleep(sleep_time);

                if (percent == 100) break;
            }

            Raise_Render_OnFinish("Rending operation successfully completed");
        }
      
  

        private void Raise_Render_OnProgress(string step_descrition, int percent)
        {
            if (OnProgress != null)
            {
                foreach (Render_OnProgress singleCast in OnProgress.GetInvocationList())
                {
                    ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;
                    try
                    {
                        if (syncInvoke != null && syncInvoke.InvokeRequired)
                        {
                            syncInvoke.Invoke(OnProgress, new object[] { this, step_descrition, percent });
                        }
                        else
                        {
                            singleCast(this, step_descrition, percent);
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }

        private void Raise_Render_OnStart(string step_descrition)
        {
            if (OnStart != null)
            {
                foreach (Render_OnStart singleCast in OnStart.GetInvocationList())
                {
                    ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;
                    try
                    {
                        if (syncInvoke != null && syncInvoke.InvokeRequired)
                        {
                            syncInvoke.Invoke(OnProgress, new object[] { this, step_descrition });
                        }
                        else
                        {
                            singleCast(this, step_descrition);
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }

        private void Raise_Render_OnFinish(string step_descrition)
        {
            if (OnFinish != null)
            {
                foreach (Render_OnFinish singleCast in OnFinish.GetInvocationList())
                {
                    ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;
                    try
                    {
                        if (syncInvoke != null && syncInvoke.InvokeRequired)
                        {
                            syncInvoke.Invoke(OnProgress, new object[] { this, step_descrition });
                        }
                        else
                        {
                            singleCast(this, step_descrition);
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }



        public void Render()
        {
            myThread = new Thread(new ThreadStart(myThread_Task));
            myThread.IsBackground = true;
            myThread.Start();            
        }

        public void Dispose()
        {
            if (myThread != null && myThread.IsAlive)
            {
                myThread_Stop = true;

                if (!myThread.Join(200))
                {
                    myThread.Abort();
                }

                myThread = null;
                myThread_Stop = false;
            }
        }

    }
}
