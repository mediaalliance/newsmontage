﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Threading;
using System.Runtime.InteropServices;

using DirectShowLib;
using MediaAlliance.AV.DS;



namespace MediaAlliance.AV
{
    public class CWaveformDS
    {
        private delegate void dlgUpdateSample(QueueItem item);

        private string filename     = "";


        MADsGraphBuilder gr         = null;
        CBGrabber grabber           = null;
        long duration               = 0;


        const int MAXSAMPLES        = 250; // Number of samples to be checked
        static int volumePeakR      = 0;
        static int volumePeakL      = 0;
        static int volumeAvgR       = 0;
        static int volumeAvgL       = 0;
        static bool volumeInfoBusy  = false;


        private List<QueueItem> Queue = new List<QueueItem>();


        // THREADS

        Thread ThDrawner        = null;
        bool ThDrawner_Stop     = false;
        ManualResetEvent mre    = new ManualResetEvent(false);





        public CWaveformDS(string filename)
        {
            this.filename = filename;
        }

        public void Run()
        {
            gr = new MADsGraphBuilder();

            IBaseFilter src;
            gr.AddSourceFilter(filename, "SOURCE", out src);

            MADsFilter SRC = new MADsFilter(src);

            SRC.OutputPin[0].Render();



            // TROVO L'ULTIMO FILTRO

            MADsFilter RND = SRC;

            while (RND.OutputPin.Count > 0)
            {
                RND = RND.OutputPin[0].ConnectedFilter;
            }

            //

            MADsFilter PRV = RND.InputPin[0].ConnectedFilter;   // FILTRO PRECEDENTE ALL'AUDIO RENDER

            PRV.OutputPin[0].Disconnect();                      // DISCONNETO IL FILTRO PRECEDENTE
            RND.InputPin[0].Disconnect();                       // DISCONNETTO L'AUDIO RENDER

            grabber = new CBGrabber(gr.BaseGraph, "GRABBER");   // CREO SAMPLEGRABBER
            grabber.OnAudioSample += new CBGrabber.CBG_OnAudioSample(grabber_OnAudioSample);
            MADsFilter GRB = new MADsFilter(grabber.MyFilter);



            MADsFilter NRN = new MADsFilter(gr, CLSID.NullRender);

            PRV.ConnectTo(GRB, 0, 0, true);                     // CONNETTO FILTRO PRECENDENTE AL SAMPLEGRABBER
            GRB.ConnectTo(NRN, 0, 0, true);                     // CONNETTO SAMPLEGRABBER ALL'AUDIO RENDER
            //GRB.ConnectTo(RND, 0, 0, true);                     // CONNETTO SAMPLEGRABBER ALL'AUDIO RENDER

            gr.RemoveFilter(RND.BaseFilter);                    // TOLGO L'AUDIO RENDER




            IReferenceClock clock;
            PRV.BaseFilter.GetSyncSource(out clock);
            SRC.BaseFilter.SetSyncSource(clock);

            //gr.SaveGraph("c:\\prova.grf");

            //imginit();

            ThDrawner = new Thread(new ThreadStart(ThDrawner_Task));
            ThDrawner.IsBackground = true;
            ThDrawner.Start();
            //gr.Seeking.SetRate(20);
            gr.Seeking.GetDuration(out duration);
            gr.Control.Run();

            //int hh = gr.Seeking.SetPositions(25 * 60 * unity, AMSeekingSeekingFlags.AbsolutePositioning, 0, AMSeekingSeekingFlags.NoPositioning);

            //tmrPosition.Start();
        }


        void grabber_OnAudioSample(object sender, short[] Frame, int bufflen)
        {
            //dlgUpdateSample d = new dlgUpdateSample(UpdateSample);

            //int[] peak =  AudioCapture(Frame, bufflen);

            long pos = 0;
            gr.Seeking.GetCurrentPosition(out pos);

            Queue.Add(new QueueItem(pos, Frame, bufflen));

            //this.Invoke(d, peak[0], peak[1]);
            volumeInfoBusy = false;
        }

        private void ThDrawner_Task()
        {
            //dlgUpdateSample d = new dlgUpdateSample(UpdateSample);

            //while (!ThDrawner_Stop)
            //{
            //    while (Queue.Count > 0)
            //    {
            //        try
            //        {
            //            this.Invoke(d, Queue[0]);
            //            mre.WaitOne();
            //            mre.Reset();
            //            Queue.RemoveAt(0);
            //        }
            //        catch { }
            //    }
            //}
        }

        public class CBGrabber : ISampleGrabberCB
        {
            public delegate void CBG_OnFrame(object sender, long Frame);
            public delegate void CBG_OnAudioSample(object sender, short[] Frame, int bufflen);
            public event CBG_OnFrame OnFrame = null;
            public event CBG_OnAudioSample OnAudioSample = null;

            public int m_stride;
            public int m_videoWidth;
            public int m_videoHeight;

            public long Position = 0;

            public ISampleGrabber MyGrabber = null;
            public IBaseFilter MyFilter = null;

            public CBGrabber(IGraphBuilder builder)
            {
                InternalCtor(builder, "SampleGrabber");
            }

            public CBGrabber(IGraphBuilder builder, string name)
            {
                InternalCtor(builder, name);
            }

            private void InternalCtor(IGraphBuilder builder, string name)
            {
                MyGrabber = (ISampleGrabber)new SampleGrabber();

                //if (OnLog != null) OnLog(this, " > SET GRABBER", false);

                MyGrabber.SetCallback(this, 1);
                MyFilter = (IBaseFilter)MyGrabber;
                builder.AddFilter(MyFilter, name);
            }


            #region ISampleGrabberCB

            public void configure(ISampleGrabber sampGrabber)
            {
                AMMediaType media = new AMMediaType();

                int hr = sampGrabber.GetConnectedMediaType(media);

                VideoInfoHeader videoInfoHeader = (VideoInfoHeader)Marshal.PtrToStructure(media.formatPtr, typeof(VideoInfoHeader));
                m_videoWidth = videoInfoHeader.BmiHeader.Width;
                m_videoHeight = videoInfoHeader.BmiHeader.Height;
                m_stride = m_videoWidth * (videoInfoHeader.BmiHeader.BitCount / 8);

                DsUtils.FreeAMMediaType(media);
                media = null;
            }

            public int BufferCB(double SampleTime, IntPtr pBuffer, int BufferLen)
            {
                if (OnAudioSample != null)
                {
                    short[] arr = new short[50000];
                    Marshal.Copy(pBuffer, arr, 0, BufferLen / 2);
                    OnAudioSample(this, arr, BufferLen / 2);
                    return 0;
                }

                Position++;
                if (OnFrame != null) OnFrame(this, Position);
                return 0;
            }

            public int SampleCB(double SampleTime, IMediaSample pSample)
            {
                return 0;
            }

            #endregion
        }
    }
    
    public class QueueItem
    {
        public long position;
        public short[] data;
        public int length;

        public QueueItem(long pos, short[] data, int len)
        {
            this.position = pos;
            this.data = data;
            this.length = len;
        }
    }
}
