﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Drawing;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;

using DirectShowLib;



namespace MediaAlliance.AV.DS
{
    public class CVideoEditorFilter : ISampleGrabberCB
    {
        // DELEGATES
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="Frame">Frame number</param>
        /// <param name="frameSize">Size of picture on the buffer</param>
        /// <param name="OverlayGraphics">Graphics overlay</param>
        /// <param name="bmpOnBuffer">Current picture on buffer</param>
        /// <param name="FrameGraphicBuffer">Current graphic of bmpOnBuffer</param>
        public delegate void VEDITOR_OnFrame(object sender, long Frame, Size frameSize, Graphics OverlayGraphics, Bitmap bmpOnBuffer, Graphics FrameGraphicBuffer);
        //public delegate void CBG_OnAudioSample(object sender, short[] Frame, int bufflen);

        // EVENTS
        public event VEDITOR_OnFrame OnFrame = null;


        public int m_stride;
        public Size FrameSize = new Size(0, 0);

        public long Position = 0;

        public ISampleGrabber MyGrabber = null;
        public IBaseFilter MyFilter = null;

        public Bitmap bitmapOverlay = null;


        public CVideoEditorFilter(IGraphBuilder builder)
        {
            InternalCtor(builder, "MA_VEDITOR");
        }

        public CVideoEditorFilter(IGraphBuilder builder, string name)
        {
            InternalCtor(builder, name);
        }

        private void InternalCtor(IGraphBuilder builder, string name)
        {
            MyGrabber = (ISampleGrabber)new SampleGrabber();

            //if (OnLog != null) OnLog(this, " > SET GRABBER", false);

            MyGrabber.SetCallback(this, 1);
            MyFilter = (IBaseFilter)MyGrabber;
            builder.AddFilter(MyFilter, name);

        }


        #region ISampleGrabberCB


        public void Init()
        {
            // INIZIALIZZO IL BUFFER PER LAVORARE SUI FRAMES

            AMMediaType media;
            int hr;

            // Set the media type to Video/RBG24
            media = new AMMediaType();
            media.majorType = MediaType.Video;
            media.subType = MediaSubType.ARGB32;
            media.formatType = FormatType.VideoInfo;

            hr = this.MyGrabber.SetMediaType(media);        // sampGrabber.SetMediaType(media);
            DsError.ThrowExceptionForHR(hr);

            DsUtils.FreeAMMediaType(media);
            media = null;

            hr = this.MyGrabber.SetCallback(this, 1);       // sampGrabber.SetCallback(this, 1);
            DsError.ThrowExceptionForHR(hr);
        }

        public void UpdateSize()
        {
            // UNA VOLTA COLLEGATO IL FILTRO E RENDERIZZATO IL GRAPH
            // AGGIORNO IL FORMATO LETTO DAL MEDIATYPE CONFIGURATO

            int hr;

            AMMediaType media = new AMMediaType();
            hr = MyGrabber.GetConnectedMediaType(media);
            DsError.ThrowExceptionForHR(hr);

            if ((media.formatType != FormatType.VideoInfo) || (media.formatPtr == IntPtr.Zero))
            {
                throw new NotSupportedException("Unknown Grabber Media Format");
            }

            // Grab the size info
            VideoInfoHeader videoInfoHeader = (VideoInfoHeader)Marshal.PtrToStructure(media.formatPtr, typeof(VideoInfoHeader));
            FrameSize.Width = videoInfoHeader.BmiHeader.Width;
            FrameSize.Height = videoInfoHeader.BmiHeader.Height;
            m_stride = FrameSize.Width * (videoInfoHeader.BmiHeader.BitCount / 8);

            if (bitmapOverlay != null) bitmapOverlay.Dispose();
            bitmapOverlay = new Bitmap(FrameSize.Width, FrameSize.Height, PixelFormat.Format32bppArgb);

            DsUtils.FreeAMMediaType(media);
            media = null;
        }

        // CALLBACK DEL SAMPLEGRABBER
        public int BufferCB(double SampleTime, IntPtr pBuffer, int BufferLen)
        {
            if (bitmapOverlay != null)
            {
                Graphics gOver;

                long frame_pos = (long)(SampleTime * 25);


                gOver = Graphics.FromImage(bitmapOverlay);
                gOver.Clear(System.Drawing.Color.Transparent);
                gOver.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;


                // RECUPERO L'IMMAGINE DAL BUFFER

                Bitmap buffer_image = new Bitmap(FrameSize.Width, FrameSize.Height, m_stride, PixelFormat.Format32bppArgb, pBuffer);
                Graphics gBuff = Graphics.FromImage(buffer_image);

                // INVIO EVENTO PER ELABORAZIONE GRAFICA ESTERNA

                if (OnFrame != null) OnFrame(this, frame_pos, FrameSize, gOver, buffer_image, gBuff);
                //gOver.DrawRectangle(new Pen(Brushes.Yellow, 2), new Rectangle(5, 5, FrameSize.Width - 15, FrameSize.Height - 15));
                gOver.Dispose();


                bitmapOverlay.RotateFlip(RotateFlipType.RotateNoneFlipY);


                // DISEGNO L'IMMAGINE ELABORATO SOPRA IL BUFFER VIDEO
                gBuff.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                gBuff.DrawImage(bitmapOverlay, 0, 0, bitmapOverlay.Width, bitmapOverlay.Height);


                gBuff.Dispose();
                buffer_image.Dispose();


                Position++;

                return 0;
            }

            return 1;
        }




        public int SampleCB(double SampleTime, IMediaSample pSample)
        {
            return 0;
        }

        #endregion
    }
}
