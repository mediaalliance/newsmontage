using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.Runtime.InteropServices;

using DirectShowLib;
using DirectShowLib.Utils;
using DirectShowLib.DES;



namespace MediaAlliance.AV.DS
{
    public enum PinMajorType
    {
        AnalogAudio,
        AnalogVideo,
        Audio,
        AuxLine21Data,
        AUXTeletextPage,
        CC_Container,
        DTVCCData,
        File,
        Interleaved,
        LMRT,
        Midi,
        Mpeg2Sections,
        MSTVCaption,
        Null,
        ScriptCommand,
        Stream,
        Texts,
        Timecode,
        URLStream,
        VBI,
        Video
    }

    public class MADsPin : IPin
    {
        private IPin Me                     = null;
        private bool MyPinInfoReaded        = false;
        private MADsGraphBuilder MyGraph    = null;
        private MADsFilter MyFilter         = null;
        private PinInfo MyPinInfo;
        public PinMajorType MajorType       = PinMajorType.Null;
        private string FName                = "";
        private bool isVideoPin = false;

        public MADsPin(IPin pin)
        {
            Me = pin;
            Me.QueryPinInfo(out MyPinInfo);
            FName = MyPinInfo.name;

            MyFilter = new MADsFilter(MyPinInfo.filter);
            MyGraph = MyFilter.GraphBuilder;

            try
            {
                IEnumMediaTypes locMTEnum = null;

                Me.EnumMediaTypes(out locMTEnum);

                IntPtr locHowManyMT = Marshal.AllocCoTaskMem(4);
                AMMediaType[] locMediaType = new AMMediaType[1];

                while (locMTEnum.Next(1, locMediaType, locHowManyMT) == 0)
                {
                    if (locMediaType[0].majorType == MediaType.Video)
                    {
                        MajorType = PinMajorType.Video;
                        isVideoPin = true;
                    }
                    else if (locMediaType[0].majorType == MediaType.Stream) MajorType = PinMajorType.Stream;
                    else if (locMediaType[0].majorType == MediaType.Timecode) MajorType = PinMajorType.Timecode;
                    else if (locMediaType[0].majorType == MediaType.URLStream) MajorType = PinMajorType.URLStream;
                    else if (locMediaType[0].majorType == MediaType.VBI) MajorType = PinMajorType.VBI;
                    else if (locMediaType[0].majorType == MediaType.AnalogAudio) MajorType = PinMajorType.AnalogAudio;
                    else if (locMediaType[0].majorType == MediaType.AnalogVideo) MajorType = PinMajorType.AnalogVideo;
                    else if (locMediaType[0].majorType == MediaType.Audio) MajorType = PinMajorType.Audio;
                    else if (locMediaType[0].majorType == MediaType.AuxLine21Data) MajorType = PinMajorType.AuxLine21Data;
                    else if (locMediaType[0].majorType == MediaType.AUXTeletextPage) MajorType = PinMajorType.AUXTeletextPage;
                    else if (locMediaType[0].majorType == MediaType.CC_Container) MajorType = PinMajorType.CC_Container;
                    else if (locMediaType[0].majorType == MediaType.DTVCCData) MajorType = PinMajorType.DTVCCData;
                    else if (locMediaType[0].majorType == MediaType.File) MajorType = PinMajorType.File;
                    else if (locMediaType[0].majorType == MediaType.Interleaved) MajorType = PinMajorType.Interleaved;
                    else if (locMediaType[0].majorType == MediaType.LMRT) MajorType = PinMajorType.LMRT;
                    else if (locMediaType[0].majorType == MediaType.Midi) MajorType = PinMajorType.Midi;
                    else if (locMediaType[0].majorType == MediaType.Mpeg2Sections) MajorType = PinMajorType.Mpeg2Sections;
                    else if (locMediaType[0].majorType == MediaType.MSTVCaption) MajorType = PinMajorType.MSTVCaption;
                    else if (locMediaType[0].majorType == MediaType.ScriptCommand) MajorType = PinMajorType.ScriptCommand;
                    else { MajorType = PinMajorType.Null; }
                }
            }
            catch { }
        }
        

        #region PUBLIC PROPERTIES

        public bool IsVideoPin
        {
            get { return isVideoPin; }
        }

        public PinInfo pinInfo
        {
            get
            {
                if (!MyPinInfoReaded)
                {
                    Me.QueryPinInfo(out MyPinInfo);
                    MyPinInfoReaded = true;
                }

                return MyPinInfo;
            }
        }

        public MADsFilter ConnectedFilter
        {
            get
            {
                IBaseFilter cnt = null;
                Guid g = Guid.Empty;

                IPin remotePin;
                Me.ConnectedTo(out remotePin);

                if (remotePin != null)
                {
                    PinInfo pi;
                    remotePin.QueryPinInfo(out pi);
                    cnt = pi.filter;
                }

                if (cnt != null)
                {
                    return new MADsFilter(cnt);
                }

                return null;
            }
        }

        public MADsPin ConnectedPin
        { 
            get
            { 
                IPin remotePin;
                Me.ConnectedTo(out remotePin);

                if (remotePin != null) return new MADsPin(remotePin);

                return null;
            }
        }

        public bool IsConnected
        {
            get
            {
                return (this.ConnectedPin != null);
            }
        }

        public string Name
        {
            get { return FName; }
        }

        public MADsGraphBuilder GraphBuilder
        {
            get { return MyGraph; }
        }

        public MADsFilter Filter
        {
            get { return MyFilter; }
        }

        public IPin BasePin
        {
            get { return Me; }
        }

        #endregion PUBLIC PROPERTIES

        
        #region PUBLIC METHODS

        public void Render()
        {
            FilterGraphTools.RenderPin(MyGraph, MyFilter, Name);
        }

        #endregion PUBLIC METHODS


        #region IPin Members

        public int BeginFlush()
        {
            return Me.BeginFlush();
        }

        public int Connect(IPin pReceivePin, AMMediaType pmt)
        {
            return Me.Connect(pReceivePin, pmt);
        }

        public int ConnectedTo(out IPin ppPin)
        {
            return Me.ConnectedTo(out ppPin);
        }

        public int ConnectionMediaType(AMMediaType pmt)
        {
            return Me.ConnectionMediaType(pmt);
        }

        public int Disconnect()
        {
            return Me.Disconnect();
        }

        public int EndFlush()
        {
            return Me.EndFlush();
        }

        public int EndOfStream()
        {
            return Me.EndOfStream();
        }

        public int EnumMediaTypes(out IEnumMediaTypes ppEnum)
        {
            return Me.EnumMediaTypes(out ppEnum);
        }

        public int NewSegment(long tStart, long tStop, double dRate)
        {
            return Me.NewSegment(tStart, tStop, dRate);
        }

        public int QueryAccept(AMMediaType pmt)
        {
            return Me.QueryAccept(pmt);
        }

        public int QueryDirection(out PinDirection pPinDir)
        {
            return Me.QueryDirection(out pPinDir);
        }

        public int QueryId(out string Id)
        {
            return Me.QueryId(out Id);
        }

        public int QueryInternalConnections(IPin[] ppPins, ref int nPin)
        {
            return Me.QueryInternalConnections(ppPins, ref nPin);
        }

        public int QueryPinInfo(out PinInfo pInfo)
        {
            return Me.QueryPinInfo(out pInfo);
        }

        public int ReceiveConnection(IPin pReceivePin, AMMediaType pmt)
        {
            return Me.ReceiveConnection(pReceivePin, pmt);
        }

        #endregion


        #region STATIC MEMBERS

        public static PinInfo GetPinInfo(IPin pin)
        {
            PinInfo pi;
            pin.QueryPinInfo(out pi);

            return pi;
        }

        #endregion STATIC MEMBERS
    }

    public class MADsPinCollection : List<MADsPin>
    {
        public MADsPinCollection() : base() { }

        public MADsPinCollection(MADsPin[] pins) : base(pins) { }

        public MADsPinCollection(List<MADsPin> pins) : base(pins) { }

        public MADsPinCollection(List<IPin> pins) : base()
        {
            foreach (IPin pin in pins) base.Add(new MADsPin(pin));
        }
    }

    public class MADsFilter : IBaseFilter
    {
        public enum FilterType
        { 
            none,
            Source,
            Writer
        }

        private MADsGraphBuilder MyGraph          = null;
        private IBaseFilter Me                   = null;
        private string MyName                    = "";
        private Guid MyGuid                      = Guid.Empty;
        private FilterType FType                 = FilterType.none;

        private MADsPinCollection BufferInputPin  = null;
        private MADsPinCollection BufferOutputPin = null;

        public ISampleGrabber SampleGrabber      = null;

        public MADsFilter(IBaseFilter filter)
        {
            if (filter != null)
            {
                Me  = filter;

                FilterInfo fi;
                filter.QueryFilterInfo(out fi);

                MyName = fi.achName;
                MyGraph = new MADsGraphBuilder((IGraphBuilder)fi.pGraph);

               filter.GetClassID(out MyGuid);
            }
        }

        public MADsFilter(IGraphBuilder graph, string FileName, string Name, FilterType type)
        {
            MyGraph      = new MADsGraphBuilder(graph);
            MyName       = Name;
            this.FType   = type;

            if (type == FilterType.Source)
            {
                if (FileName.EndsWith(".dv", StringComparison.OrdinalIgnoreCase))
                {
                    //Me = DirectShowLib.Utils.FilterGraphTools.AddFilterFromClsid(MyGraph.BaseGraph, CLSID.Kapricorn_DVRawSmartSource, "DVRawSmartSource");
                    IFileSourceFilter sour = (IFileSourceFilter)Me;
                    sour.Load(FileName, null);
                }
                else
                {
                    graph.AddSourceFilter(FileName, Name, out Me);
                }
            }
            else if (type == FilterType.Writer)
            {
                Me = FilterGraphTools.FindFilterByClsid(MyGraph,  CLSID.FileWriter);

                IFileSinkFilter sink = (IFileSinkFilter)Me;
                sink.SetFileName(FileName, null);
            }
                /*
            else if (type == FilterType.SampleGrabber)
            {
                SampleGrabber = (ISampleGrabber)new SampleGrabber();
                SampleGrabber.SetCallback(MyCBGrabber, 1);

                Me = (IBaseFilter)grab;

                graph.AddFilter(Me, Name);
            }
            */
        }

        public MADsFilter(MADsGraphBuilder graph, string FileName, string Name, FilterType type)
        {
            MyGraph      = graph;
            MyName       = Name;
            this.FType   = type;

            if (type == FilterType.Source)
            {
                if (FileName.EndsWith(".dv", StringComparison.OrdinalIgnoreCase))
                {
                    //Me = DirectShowLib.Utils.FilterGraphTools.AddFilterFromClsid(MyGraph.BaseGraph, CLSID.Kapricorn_DVRawSmartSource, "DVRawSmartSource");
                    IFileSourceFilter sour = (IFileSourceFilter)Me;
                    sour.Load(FileName, null);
                }
                else
                {
                    graph.AddSourceFilter(FileName, Name, out Me);
                    if (Me == null) throw new Exception("Source initialization error for file " + FileName);
                }
            }
            else if (type == FilterType.Writer)
            {
                Me = FilterGraphTools.AddFilterFromClsid(MyGraph.BaseGraph, CLSID.FileWriter, Name);

                IFileSinkFilter sink = (IFileSinkFilter)Me;
                sink.SetFileName(FileName, null);
            }
        }

        public MADsFilter(IGraphBuilder graph, Guid filterGuid, string Name)
        {
            MyGraph     = new MADsGraphBuilder(graph);
            MyGuid      = filterGuid;
            MyName      = Name;

            IBaseFilter Me = FilterGraphTools.AddFilterFromClsid(MyGraph, filterGuid, Name);
        }

        public MADsFilter(MADsGraphBuilder graph, Guid filterGuid, string Name)
        {
            MyGraph = graph;
            MyGuid = filterGuid;
            MyName = Name;

            Me = FilterGraphTools.AddFilterFromClsid(MyGraph, filterGuid, Name);
        }

        public MADsFilter(MADsGraphBuilder graph, Guid filterGuid)
        {
            MyGraph = graph;
            MyGuid = filterGuid;

            Me = FilterGraphTools.AddFilterFromClsid(graph.BaseGraph, filterGuid, "NULL RENDER");

            //Me = FilterGraphTools.FindFilterByClsid(MyGraph, filterGuid);
            if (Me != null)
            {
                FilterInfo info;
                Me.QueryFilterInfo(out info);

                MyName = info.achName;

            }
        }



        #region PRIVATE METHODS

        internal List<IPin> getPin(IBaseFilter filter, PinDirection direc)
        {
            IEnumPins epins;

            List<IPin> array = new List<IPin>();

            int hr = filter.EnumPins(out epins);
            IntPtr fetched = Marshal.AllocCoTaskMem(4);
            IPin[] pins = new IPin[1];
            while (epins.Next(1, pins, fetched) == 0)
            {
                PinInfo pinfo;
                pins[0].QueryPinInfo(out pinfo);
                bool found = (pinfo.dir == direc);
                DsUtils.FreePinInfo(pinfo);
                if (found)
                {
                    array.Add(pins[0]);
                }
            }
            return array;
        }

        #endregion PRIVATE METHODS



        #region PUBLIC METHODS

        public void ConnectTo(MADsFilter filter, int outpin, int inputpin)
        {
            ConnectTo(filter, outpin, inputpin, true);
        }

        public void ConnectTo(MADsFilter filter, int outpin, int inputpin, bool useIntelligentConnection)
        {
            if (this.GraphBuilder.BaseGraph != filter.GraphBuilder.BaseGraph) throw new Exception("This filter is not in the same graph of destination filter!");
            if (filter == null) throw new Exception("Destination Filter is null");
            if (filter.InputPin.Count < inputpin) throw new Exception("Destination filter " + filter.Name + " has not Pin " + inputpin);
            if (this.OutputPin.Count < outpin) throw new Exception("This filter has not Pin " + outpin);
            if (this.OutputPin[outpin].IsConnected) throw new Exception("Source Pin (" + inputpin + ") of filter " + Name + " is just Connected!");
            if (filter.InputPin[inputpin].IsConnected) throw new Exception("Destination Pin (" + inputpin + ") of filter " + filter.Name + " is just Connected!");

            if (filter.InputPin.Count > inputpin)
            {
                try
                {
                    FilterGraphTools.ConnectFilters(MyGraph, this.OutputPin[outpin].BasePin, filter.InputPin[inputpin].BasePin, useIntelligentConnection);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            Refresh();
            filter.Refresh();
        }

        public void Refresh()
        {
            BufferInputPin = new MADsPinCollection(getPin(Me, PinDirection.Input));
            BufferOutputPin = new MADsPinCollection(getPin(Me, PinDirection.Output));
        }

        #endregion PUBLIC METHODS



        #region PUBLIC PROPERTIES

        public MADsPinCollection InputPin
        {
            get
            {
                if (BufferInputPin == null)
                {
                    BufferInputPin = new MADsPinCollection(getPin(Me, PinDirection.Input));
                }

                return BufferInputPin;
            }
        }

        public MADsPinCollection OutputPin
        {
            get
            {
                if (BufferOutputPin == null)
                {
                    List<IPin> pins = getPin(Me, PinDirection.Output);
                    BufferOutputPin = new MADsPinCollection(pins);
                }

                return BufferOutputPin;
            }
        }

        public MADsGraphBuilder GraphBuilder
        {
            get { return MyGraph; }
        }

        public Guid guid
        {
            get { return MyGuid; }
        }

        public string Name
        {
            get { return MyName; }
        }

        public FilterType Type
        {
            get { return FType; }
        }

        public IBaseFilter BaseFilter
        {
            get { return Me; }
        }

        #endregion PUBLIC PROPERTIES
        


        #region IBaseFilter Members

        public int EnumPins(out IEnumPins ppEnum)
        {
            return Me.EnumPins(out ppEnum);
        }

        public int FindPin(string Id, out IPin ppPin)
        {
            return Me.FindPin(Id, out ppPin);
        }

        public int GetClassID(out Guid pClassID)
        {
            return Me.GetClassID(out pClassID);
        }

        public int GetState(int dwMilliSecsTimeout, out FilterState filtState)
        {
            return Me.GetState(dwMilliSecsTimeout, out filtState);
        }

        public int GetSyncSource(out IReferenceClock pClock)
        {
            return Me.GetSyncSource(out pClock);
        }

        public int JoinFilterGraph(IFilterGraph pGraph, string pName)
        {
            return Me.JoinFilterGraph(pGraph, pName);
        }

        public int Pause()
        {
            return Me.Pause();
        }

        public int QueryFilterInfo(out FilterInfo pInfo)
        {
            return Me.QueryFilterInfo(out pInfo);
        }

        public int QueryVendorInfo(out string pVendorInfo)
        {
            return Me.QueryVendorInfo(out pVendorInfo);
        }

        public int Run(long tStart)
        {
            return Run(tStart);
        }

        public int SetSyncSource(IReferenceClock pClock)
        {
            return Me.SetSyncSource(pClock);
        }

        public int Stop()
        {
            return Me.Stop();
        }

        #endregion

        

        #region STATIC MEMBERS

        public static FilterInfo GetFilterInfo(IBaseFilter filter)
        {
            FilterInfo fi;
            filter.QueryFilterInfo(out fi);

            return fi;
        }

        public static bool operator ==(MADsFilter filter1, MADsFilter filter2)
        {
            if ((object)filter1 == null && (object)filter2 != null) return false;
            if ((object)filter1 != null && (object)filter2 == null) return false;
            if ((object)filter1 == null && (object)filter2 == null) return true;

            if (filter1.Name != filter2.Name) return false;
            if (filter1.GraphBuilder.BaseGraph != filter2.GraphBuilder.BaseGraph) return false;

            return true;
        }

        public static bool operator !=(MADsFilter filter1, MADsFilter filter2)
        {
            return !(filter1 == filter2);
        }

        #endregion STATIC MEMBERS

    }
    
    public class MADsGraphBuilder : IGraphBuilder
    {
        private IGraphBuilder Me = null;

        public MADsGraphBuilder() : base()
        { 
            Me = (IGraphBuilder)new FilterGraph();
        }

        public MADsGraphBuilder(IGraphBuilder builder) : base()
        {
            Me = builder;
        }



        private List<IBaseFilter> InternalGetFilterList()
        {
            List<IBaseFilter> filterList = new List<IBaseFilter>();

            IEnumFilters EnumFilters;
            int hr = Me.EnumFilters(out EnumFilters);

            IBaseFilter[] currentGet = new IBaseFilter[1];
            IntPtr fetched = IntPtr.Zero;

            if (hr >= 0)
            {
                while (EnumFilters.Next(1, currentGet, fetched) == 0)
                {
                    if (currentGet[0] != null) filterList.Add(currentGet[0]);
                }
            }

            Marshal.ReleaseComObject(EnumFilters);

            return filterList;
        }



        #region PUBLIC PROPERTIES

        public List<MADsFilter> Filters
        {
            get
            {
                List<MADsFilter> filterList = new List<MADsFilter>();

                foreach(IBaseFilter flt in InternalGetFilterList())
                {
                    filterList.Add(new MADsFilter(flt));
                }

                return filterList;
            }
        }

        public IMediaControl Control
        {
            get { return (IMediaControl)this.BaseGraph; }
        }

        public IMediaSeeking Seeking
        {
            get { return (IMediaSeeking)this.BaseGraph; }
        }

        #endregion PUBLIC PROPERTIES
        


        #region PUBLIC METHODS

        public void DisconnectAllPin()
        {
            FilterGraphTools.DisconnectAllPins(Me);
        }

        public bool SaveGraph(string file)
        {
            FileInfo fi = new FileInfo(file);

            if (!fi.Directory.Exists) throw new Exception("Destination folder dosn't exist");

            DirectShowLib.Utils.FilterGraphTools.SaveGraphFile(Me, file);

            return true;
        }

        public void ReomoveAllFilterButThis(params MADsFilter[] filtersToLeave)
        {
            List<MADsFilter> FilterToLeave = new List<MADsFilter>(filtersToLeave);
            List<IBaseFilter> filters = InternalGetFilterList();

            DisconnectAllPin();

            foreach (IBaseFilter flt in filters)
            {
                bool remove = true;
                FilterInfo fi1 = MADsFilter.GetFilterInfo(flt);

                foreach (IBaseFilter f in FilterToLeave)
                {
                    FilterInfo fi2 = MADsFilter.GetFilterInfo(f);

                    if (fi1.achName == fi2.achName)
                    {
                        if (fi1.pGraph == fi2.pGraph)
                        {
                            remove = false;
                            break;
                        }
                    }
                }

                if (remove) this.RemoveFilter(flt);
            }
        }

        public IGraphBuilder BaseGraph
        {
            get { return Me; }
        }
        
        public void RemoveDownStream(MADsFilter main_filter)
        {
            List<MADsFilter> flt_to_remove = new List<MADsFilter>();

            foreach (MADsPin pin_in in main_filter.InputPin)
            {
                pin_in.Disconnect();
            }

            List<MADsFilter> downstream = new List<MADsFilter>();

            getDownStream(main_filter, downstream);

            foreach (MADsFilter filter in downstream)
            {
                this.RemoveFilter(filter.BaseFilter);
            }
        }

        public void getDownStream(MADsFilter filter, List<MADsFilter> downstream)
        {
            if (filter != null)
            {
                downstream.Add(filter);

                foreach (MADsPin out_pin in filter.OutputPin)
                {
                    MADsFilter connected_filter = out_pin.ConnectedFilter;

                    if (connected_filter != null)
                    {
                        getDownStream(connected_filter, downstream);
                    }
                }
            }
        }

        #endregion PUBLIC METHODS

        

        #region IGraphBuilder Members

        public int Abort()
        {
            return Me.Abort();
        }

        public int AddFilter(IBaseFilter pFilter, string pName)
        {
            return Me.AddFilter(pFilter, pName);
        }

        public int AddSourceFilter(string lpcwstrFileName, string lpcwstrFilterName, out IBaseFilter ppFilter)
        {
            return Me.AddSourceFilter(lpcwstrFileName, lpcwstrFilterName, out ppFilter);
        }

        public int Connect(IPin ppinOut, IPin ppinIn)
        {
            return Me.Connect(ppinOut, ppinIn);
        }

        public int ConnectDirect(IPin ppinOut, IPin ppinIn, AMMediaType pmt)
        {
            return Me.ConnectDirect(ppinOut, ppinIn, pmt);
        }

        public int Disconnect(IPin ppin)
        {
            return Me.Disconnect(ppin);
        }

        public int EnumFilters(out IEnumFilters ppEnum)
        {
            return Me.EnumFilters(out ppEnum);
        }

        public int FindFilterByName(string pName, out IBaseFilter ppFilter)
        {
            return Me.FindFilterByName(pName, out ppFilter);
        }

        public int Reconnect(IPin ppin)
        {
            return Me.Reconnect(ppin);
        }

        public int RemoveFilter(IBaseFilter pFilter)
        {
            return Me.RemoveFilter(pFilter);
        }

        public int Render(IPin ppinOut)
        {
            return Me.Render(ppinOut);
        }

        public int RenderFile(string lpcwstrFile, string lpcwstrPlayList)
        {
            return Me.RenderFile(lpcwstrFile, lpcwstrPlayList);
        }

        public int SetDefaultSyncSource()
        {
            return Me.SetDefaultSyncSource();
        }

        public int SetLogFile(IntPtr hFile)
        {
            return Me.SetLogFile(hFile);
        }

        public int ShouldOperationContinue()
        {
            return Me.ShouldOperationContinue();
        }

        #endregion
    }
}