using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using DirectShowLib;
using DirectShowLib.DES;

namespace MediaAlliance.AV.MediaInfo
{
    public struct RequestInfo
    {
        public StreamKind Kind;
        public string PropName;
        public string Value;

        public RequestInfo(string PropName, StreamKind Kind)
        {
            this.PropName = PropName;
            this.Kind = Kind;
            this.Value = "";
        }
    }

    public enum ScanOrders
    { 
        undefined,
        BFF,
        TFF
    }

    public enum VideoInfoRequest
    {
        StreamCount,       //  Number of streams of this kind available
        StreamKind,       //  Stream type name
        StreamKind_str,       //  Stream type name
        StreamKindID,       //  Number of the stream (base=0)
        StreamKindPos,       //  When multiple streams, number of the stream (base=1)
        Inform,       //  Last **Inform** call
        ID,       //  The ID for this stream in this file
        ID_str,       //  The ID for this stream in this file
        UniqueID,       //  The unique ID for this stream, should be copied with stream copy
        MenuID,       //  The menu ID for this stream in this file
        MenuID_str,       //  The menu ID for this stream in this file
        Format,       //  Format used
        Format_Version,       //  Version of this format
        Format_Profile,       //  Profile of the Format
        Format_Settings,       //  Settings needed for decoder used, summary
        MuxingMode,       //  How this file is muxed in the container
        CodecID,       //  Codec ID (found in some containers)
        CodecID_Description,       //  Manual description given by the container
        Duration,       //  Play time of the stream in ms
        BitRate_Mode,       //  Bit rate mode (VBR, CBR)
        BitRate_Mode_str,       //  Bit rate mode (Variable, Cconstant)
        BitRate,       //  Bit rate in bps
        BitRate_str,       //  Bit rate (with measurement)
        BitRate_Minimum,       //  Minimum Bit rate in bps
        BitRate_Minimum_str,       //  Minimum Bit rate (with measurement)
        BitRate_Nominal,       //  Nominal Bit rate in bps
        BitRate_Nominal_str,       //  Nominal Bit rate (with measurement)
        BitRate_Maximum,       //  Maximum Bit rate in bps
        BitRate_Maximum_str,       //  Maximum Bit rate (with measurement)
        Width,       //  Width in pixel
        Width_str,       //  Width with measurement (pixel)
        Height,       //  Height in pixel
        Height_str,       //  Width with measurement (pixel)
        PixelAspectRatio,       //  Pixel Aspect ratio
        PixelAspectRatio_str,       //  Pixel Aspect ratio
        DisplayAspectRatio,       //  Display Aspect ratio
        DisplayAspectRatio_str,       //  Display Aspect ratio
        FrameRate_Mode,       //  Frame rate mode (CFR, VFR)
        FrameRate_Mode_str,       //  Frame rate mode (Constant, Variable)
        FrameRate,       //  Frames per second
        FrameRate_str,       //  Frames per second (with measurement)
        FrameRate_Minimum,       //  Minimum Frames per second
        FrameRate_Minimum_str,       //  Minimum Frames per second (with measurement)
        FrameRate_Nominal,       //  Nominal Frames per second
        FrameRate_Nominal_str,       //  Nominal Frames per second (with measurement)
        FrameRate_Maximum,       //  Maximum Frames per second
        FrameRate_Maximum_str,       //  Maximum Frames per second (with measurement)
        FrameRate_Original,       //  Original (in the raw stream) Frames per second
        FrameRate_Original_str,       //  Original (in the raw stream) Frames per second (with measurement)
        FrameCount,       //  Number of frames
        Standard,       //  NTSC or PAL
        Resolution,       //  16/24/32
        Resolution_str,       //  16/24/32 bits
        Colorimetry,
        ScanType,
        ScanType_str,
        ScanOrder,
        ScanOrder_str,
        Delay,       //  Delay fixed in the stream (relative) IN MS
        Delay_str,       //  Delay with measurement
        Delay_Settings,       //  Delay settings (in case of timecode for example)
        StreamSize,       //  Stream size in bytes
        StreamSize_str,       //  Streamsize in with percentage value
        StreamSize_Proportion,       //  Stream size divided by file size
        Alignment,       //  How this stream file is aligned in the container
        Alignment_str,
        Title,       //  Name of the track
        Encoded_Application,       //  Software. Identifies the name of the software package used to create the file, such as Microsoft WaveEdit.
        Encoded_Library,       //  Software used to create the file
        Encoded_Library_str,       //  Software used to create the file
        Language,       //  Language (2-letter ISO 639-1 if exists, else 3-letter ISO 639-2, and with optional ISO 3166-1 country separated by a dash if available, e.g. en, en-us, zh-cn)
        Language_str,       //  Language (full)
        Language_More,       //  More info about Language (e.g. Director's Comment)
        Encoded_Date,       //  UTC time that the encoding of this item was completed began.
        Tagged_Date,       //  UTC time that the tags were done for this item.
        Encryption
    }

    public enum AudioInfoRequest
    {
        Count,     //  Number of objects available in this stream
        StreamCount,     //  Number of streams of this kind available
        StreamKind,     //  Stream type name
        StreamKind_str,     //  Stream type name
        StreamKindID,     //  Number of the stream (base=0)
        StreamKindPos,     //  When multiple streams, number of the stream (base=1)
        Inform,     //  Last **Inform** call
        ID,     //  The ID of this stream in this file
        ID_str,     //  The ID of this stream in this file
        UniqueID,     //  A unique ID for this stream, should be copied with stream copy
        MenuID,     //  The menu ID for this stream in this file
        MenuID_str,     //  The menu ID for this stream in this file
        Format,     //  Format used
        Format_Version,     //  Version of this format
        Format_Profile,     //  Profile of this Format
        Format_Settings,     //  Settings needed for decoder used, summary
        MuxingMode,     //  How this stream is muxed in the container
        CodecID,     //  Codec ID (found in some containers)
        CodecID_Description,     //  Manual description given by the container
        Duration,     //  Play time of the stream
        Duration_str,     //  Play time in format ,     //  XXx YYy only, YYy omited if zero
        BitRate_Mode,     //  Bit rate mode (VBR, CBR)
        BitRate_Mode_str,     //  Bit rate mode (Constant, Variable)
        BitRate,     //  Bit rate in bps
        BitRate_str,     //  Bit rate (with measurement)
        BitRate_Minimum,     //  Minimum Bit rate in bps
        BitRate_Minimum_str,     //  Minimum Bit rate (with measurement)
        BitRate_Nominal,     //  Nominal Bit rate in bps
        BitRate_Nominal_str,     //  Nominal Bit rate (with measurement)
        BitRate_Maximum,     //  Maximum Bit rate in bps
        BitRate_Maximum_str,     //  Maximum Bit rate (with measurement)
        Channel__s,     //  Number of channels
        Channel__s_str,     //  Number of channels (with measurement)
        ChannelPositions,     //  Position of channels
        SamplingRate,     //  Sampling rate
        SamplingRate_str,     //  in KHz
        SamplingCount,     //  Frame count
        Resolution,     //  Resolution in bits (8, 16, 20, 24)
        Resolution_str,     //   n bits
        CompressionRatio,     //  Current stream size divided by uncompressed stream size
        Delay,     //  Delay fixed in the stream (relative)
        Delay_str,     //  Delay in format ,     //  XXx YYy only, YYy omited if zero
        Video_Delay,     //  Delay fixed in the stream (absolute / video)
        ReplayGain_Gain,     //  The gain to apply to reach 89dB SPL on playback
        ReplayGain_Gain_str,
        ReplayGain_Peak,     //  The maximum absolute peak value of the item
        StreamSize,     //  Streamsize in bytes
        StreamSize_str,     //  Streamsize in with percentage value
        StreamSize_Proportion,     //  Stream size divided by file size
        Alignment,     //  How this stream file is aligned in the container
        Alignment_str,     //  Where this stream file is aligned in the container
        Interleave_VideoFrames,     //  Between how many video frames the stream is inserted
        Interleave_Duration,     //  Between how much time (ms) the stream is inserted
        Interleave_Preload,     //  How much time is buffered before the first video frame
        Interleave_Preload_str,     //  How much time is buffered before the first video frame (with measurement)
        Title,     //  Name of the track
        Encoded_Library,     //  Software used to create the file
        Encoded_Library_str,     //  Software used to create the file
        Language,     //  Language (2-letter ISO 639-1 if exists, else 3-letter ISO 639-2, and with optional ISO 3166-1 country separated by a dash if available, e.g. en, en-us, zh-cn)
        Language_str,     //  Language (full)
        Language_More,     //  More info about Language (e.g. Director's Comment)
        Encoded_Date,     //  UTC time that the encoding of this item was completed began.
        Tagged_Date,     //  UTC time that the tags were done for this item.
        Encryption

    }

    public enum ImageInfoRequest
    {
        Count,   // Count of objects available in this stream
        StreamCount,   // Count of streams of that kind available
        StreamKind,   // Stream type name
        StreamKind_str,   // Stream type name
        StreamKindID,   // Number of the stream (base=0)
        StreamKindPos,   // When multiple streams, number of the stream (base=1)
        Inform,   // Last **Inform** call
        ID,   // A ID for this stream in this file
        UniqueID,   // A unique ID for this stream, should be copied with stream copy
        Title,   // Name of the track
        Format,   // Format used
        Format_Profile,   // Profile of the Format
        CodecID,   // Codec ID (found in some containers)
        CodecID_Description,   // Manual description given by the container
        Codec,   // Deprecated
        Codec_str,   // Deprecated
        Width,   // Width
        Width_str,
        Height,   // Height
        Height_str,
        Resolution,
        Resolution_str,
        StreamSize,   // Stream size in bytes
        StreamSize_str,
        StreamSize_str1,
        StreamSize_str2,
        StreamSize_str3,
        StreamSize_str4,
        StreamSize_str5,   // With proportion
        StreamSize_Proportion,   // Stream size divided by file size
        Encoded_Library,   // Software used to create the file
        Encoded_Library_str,   // Software used to create the file
        Language,   // Language (2-letter ISO 639-1 if exists, else 3-letter ISO 639-2, and with optional ISO 3166-1 country separated by a dash if available, e.g. en, en-us, zh-cn)
        Language_str,   // Language (full)
        Summary,
        Encoded_Date,   // The time that the encoding of this item was completed began.
        Tagged_Date,   // The time that the tags were done for this item.
        Encryption
    }

    public enum GeneralInfoRequest
    {
        Count,    //  Number of objects available in this stream
        StreamCount,    //  Number of streams of this kind available
        StreamKind,    //  Stream type name
        StreamKind_str,    //  Stream type name
        StreamKindID,    //  Number of the stream (base=0)
        StreamKindPos,    //  When multiple streams, number of the stream (base=1)
        Inform,    //  Last **Inform** call
        ID,    //  The ID for this stream in this file
        ID_str,    //  The ID for this stream in this file
        UniqueID,    //  The unique ID for this stream, should be copied with stream copy
        GeneralCount,    //  Number of general streams
        VideoCount,    //  Number of video streams
        AudioCount,    //  Number of audio streams
        TextCount,    //  Number of text streams
        ChaptersCount,    //  Number of chapters streams
        ImageCount,    //  Number of image streams
        MenuCount,    //  Number of menu streams
        Video_Format_List,    //  Video Codecs in this file, separated by /
        Video_Format_WithHint_Lis,    //  Video Codecs in this file with popular name (hint), separated by /
        Video_Codec_List,    //  Deprecated, do not use in new projects
        Video_Language_List,    //  Video languagesin this file, full names, separated by /
        Audio_Format_List,    //  Audio Codecs in this file,separated by /
        Audio_Format_WithHint_Lis,    //  Audio Codecs in this file with popular name (hint), separated by /
        Audio_Codec_List,    //  Deprecated, do not use in new projects
        Audio_Language_List,    //  Audio languages in this file separated by /
        Text_Format_List,    //  Text Codecs in this file, separated by /
        Text_Format_WithHint_List,    //  Text Codecs in this file with popular name (hint),separated by /
        Text_Codec_List,    //  Deprecated, do not use in new projects
        Text_Language_List,    //  Text languages in this file, separated by /
        Chapters_Format_List,    //  Chapters Codecs in this file, separated by /
        Chapters_Format_WithHint_,    //  Chapters Codecs in this file with popular name (hint), separated by /
        Chapters_Codec_List,    //  Deprecated, do not use in new projects
        Chapters_Language_List,    //  Chapters languages in this file, separated by /
        Image_Format_List,    //  Image Codecs in this file, separated by /
        Image_Format_WithHint_Lis,    //  Image Codecs in this file with popular name (hint), separated by /
        Image_Codec_List,    //  Deprecated, do not use in new projects
        Image_Language_List,    //  Image languages in this file, separated by /
        Menu_Format_List,    //  Menu Codecsin this file, separated by /
        Menu_Format_WithHint_List,    //  Menu Codecs in this file with popular name (hint),separated by /
        Menu_Codec_List,    //  Deprecated, do not use in new projects
        Menu_Language_List,    //  Menu languages in this file, separated by /
        CompleteName,    //  Complete name (Folder+Name+Extension)
        FolderName,    //  Folder name only
        FileName,    //  File name only
        FileExtension,    //  File extension only
        Format,    //  Format used
        Format_str,    //  Deprecated, do not use in new projects
        Format_Version,    //  Version of this format
        Format_Profile,    //  Profile of the Format
        Format_Settings,    //  Settings needed for decoder used
        CodecID,    //  Codec ID (found in some containers)
        Interleaved,    //  If Audio and video are muxed
        Codec,    //  Deprecated, do not use in new projects
        Codec_Settings,    //  Deprecated, do not use in new projects
        Codec_Settings_Automatic,    //  Deprecated, do not use in new projects
        FileSize,    //  File size in bytes
        FileSize_str,    //  File size (with measure)
        Duration,    //  Play time of the stream in ms
        Duration_str,    //  Play time in format ,    //  XXx YYy only, YYy omited if zero
        OverallBitRate_Mode,    //  Bit rate mode of all streams (VBR, CBR)
        OverallBitRate,    //  Bit rate of all streams in bps
        OverallBitRate_str,    //  Bit rate of all streams (with measure)
        OverallBitRate_Minimum,    //  Minimum Bit rate in bps
        OverallBitRate_Nominal,    //  Nominal Bit rate in bps
        OverallBitRate_Maximum,    //  Maximum Bit rate in bps
        StreamSize,    //  Stream size in bytes
        StreamSize_str,
        StreamSize_Proportion,    //  Stream size divided by file size
        HeaderSize,
        DataSize,
        FooterSize,
        Album_ReplayGain_Gain,    //  The gain to apply to reach 89dB SPL on playback
        Album_ReplayGain_Peak,    //  The maximum absolute peak value of the item
        Title,    //  (Generic)Title of file
        Domain,    //  Univers movies belong to, e.g. Starwars, Stargate, Buffy, Dragonballs
        Collection,    //  Name of the series, e.g. Starwars movies, Stargate SG-1, Stargate Atlantis, Buffy, Angel
        Season,    //  Name of the season, e.g. Strawars first Trilogy, Season 1
        Season_Position,    //  Number of the Season
        Season_Position_Total,    //  Place of the season e.g. 2 of 7
        Movie,    //  Name of the movie. Eg ,    //  Starwars, a new hope
        Album,    //  Name of an audio-album. Eg ,    //  The joshua tree
        Comic,    //  Name of the comic.
        Part,    //  Name of the part. e.g. CD1, CD2
        Track,    //  Name of the track. e.g. track1, track 2
        Chapter,    //  Name of the chapter.
        SubTrack,    //  Name of the subtrack.
        Performer,    //  Main performer/artist of this file
        Accompaniment,    //  Band/orchestra/accompaniment/musician.
        Composer,    //  Name of the original composer.
        Arranger,    //  The person who arranged the piece. e.g. Ravel.
        Lyricist,    //  The person who wrote the lyrics for a musical item.
        Conductor,    //  The artist(s) who performed the work. In classical music this would be the conductor, orchestra, soloists.
        Director,    //  Name of the director.
        AssistantDirector,    //  Name of the assistant director.
        DirectorOfPhotography,    //  The name of the director of photography, also known as cinematographer.
        SoundEngineer,    //  The name of the sound engineer or sound recordist.
        ArtDirector,    //  The person who oversees the artists and craftspeople who build the sets.
        ProductionDesigner,    //  The person responsible for designing the Overall visual appearance of a movie.
        Choregrapher,    //  The name of the choregrapher.
        CostumeDesigner,    //  The name of the costume designer.
        Actor,    //  Real name of an actor or actress playing a role in the movie.
        Actor_Character,    //  Name of the character an actor or actress plays in this movie.
        WrittenBy,    //  The author of the story or script.
        ScreenplayBy,    //  The author of the screenplay or scenario (used for movies and TV shows).
        EditedBy,    //  Editors name
        CommissionedBy,    //  name of the person or organization that commissioned the subject of the file
        Producer,    //  Name of the producer of the movie.
        CoProducer,    //  The name of a co-producer.
        ExecutiveProducer,    //  The name of an executive producer.
        MusicBy,    //  Main music-artist for a movie
        DistributedBy,    //  Company the item is mainly distributed by
        MasteredBy,    //  The engineer who mastered the content for a physical medium or for digital distribution.
        EncodedBy,    //  Name of the person or organisation that encoded/ripped the audio file.
        RemixedBy,    //  Name of the artist(s), that interpreted, remixed, or otherwise modified the item.
        ProductionStudio,    //  Main production studio
        ThanksTo,    //  A very general tag for everyone else that wants to be listed.
        Publisher,    //  Name of the organization publishing the album (i.e. the 'record label') or movie.
        Label,    //  Brand or trademark associated with the marketing of music recordings and music videos.
        Genre,    //  The main genre of the audio or video. e.g. classical, ambient-house, synthpop, sci-fi, drama, etc.
        Mood,    //  Intended to reflect the mood of the item with a few keywords, e.g. Romantic, Sad, Uplifting, etc.
        ContentType,    //  The type of the item. e.g. Documentary, Feature Film, Cartoon, Music Video, Music, Sound FX, etc.
        Subject,    //  Describes the topic of the file, such as Aerial view of Seattle..
        Description,    //  A short description of the contents, such as Two birds flying.
        Keywords,    //  Keywords to the item separated by a comma, used for searching.
        Summary,    //  A plot outline or a summary of the story.
        Synopsys,    //  A description of the story line of the item.
        Period,    //  Describes the period that the piece is from or about. e.g. Renaissance.
        LawRating,    //  Depending on the country it's the format of the rating of a movie (P, R, X in the USA, an age in other countries or a URI defining a logo).
        LawRating_Reason,    //  Reason for the law rating
        ICRA,    //  The ICRA rating. (Previously RSACi)
        Released_Date,    //  The date/year that the item was released.
        Recorded_Date,    //  The time/date/year that the recording began.
        Encoded_Date,    //  The time/date/year that the encoding of this item was completed began.
        Tagged_Date,    //  The time/date/year that the tags were done for this item.
        Written_Date,    //  The time/date/year that the composition of the music/script began.
        Mastered_Date,    //  The time/date/year that the item was tranfered to a digitalmedium.
        File_Created_Date,    //  The time that the file was created on the file system
        File_Modified_Date,    //  The time that the file was modified on the file system
        Recorded_Location,    //  Location where track was recorded. (See COMPOSITION_LOCATION for format)
        Written_Location,    //  Location that the item was originaly designed/written. Information should be stored in the following format,    //  country code, state/province, city where the coutry code is the same 2 octets as in Internet domains, or possibly ISO-3166. e.g. US, Texas, Austin or US, , Austin.
        Archival_Location,    //  Location, where an item is archived, e.eg. Louvre,Paris,France
        Encoded_Application,    //  Name of the software package used to create the file, such as Microsoft WaveEdit.
        Encoded_Library,    //  Software used to create the file
        Encoded_Library_str,    //  Software used to create the file
        Cropped,    //  Describes whether an image has been cropped and, if so, how it was cropped.
        Dimensions,    //  Specifies the size of the original subject of the file. eg 8.5 in h, 11 in w
        DotsPerInch,    //  Stores dots per inch setting of the digitizer used to produce the file
        Lightness,    //  Describes the changes in lightness settings on the digitizer required to produce the file
        OriginalSourceMedium,    //  Original medium of the material, e.g. vinyl, Audio-CD, Super8 or BetaMax
        OriginalSourceForm,    //  Original form of the material, e.g. slide, paper, map
        Tagged_Application,    //  Software used to tag this file
        BPM,    //  Average number of beats per minute
        ISRC,    //  International Standard Recording Code, excluding the ISRC prefix and including hyphens.
        ISBN,    //  International Standard Book Number.
        BarCode,    //  EAN-13 (13-digit European Article Numbering) or UPC-A (12-digit Universal Product Code) bar code identifier.
        LCCN,    //  Library of Congress Control Number.
        CatalogNumber,    //  A label-specific catalogue number used to identify the release. e.g. TIC 01.
        LabelCode,    //  A 4-digit or 5-digit number to identify the record label, typically printed as (LC) xxxx or (LC) 0xxxx on CDs medias or covers, with only the number being stored.
        Owner,    //  Owner of the file
        Copyright,    //  Copyright attribution.
        Producer_Copyright,    //  The copyright information as per the productioncopyright holder.
        TermsOfUse,    //  License information, e.g., All Rights Reserved,Any Use Permitted.
        ServiceName,
        ServiceChannel,
        ServiceProvider,
        ServiceType,
        Cover,    //  Is there a cover
        Cover_Description,    //  short descriptio, e.g. Earth in space
        Cover_Type,
        Cover_Mime,
        Cover_Data,    //  Cover, in binary format encoded BASE64
        Lyrics,    //  Text of a song
        Comment,    //  Any comment related to the content.
        Rating,    //  A numeric value defining how much a person likes the song/movie. The number is between 0 and 5 with decimal values possible (e.g. 2.7), 5(.0) being the highest possible rating.
        Added_Date,    //  Date/year the item was added to the owners collection
        Played_First_Date,    //  The date, the owner first played an item
        Played_Last_Date,    //  The date, the owner last played an item
        Played_Count                   //  Number of times an item was played
    }

    public class MediaInformations
    {
        private MediaInfo MI = null;
        private bool FActive = false;

        public int FVideoCount = 0;
        public int FAudioCount = 0;
        public int FImageCount = 0;
        public string Filename = "";

        private Dictionary<GeneralInfoRequest, string> FGeneralInformation = new Dictionary<GeneralInfoRequest, string>();
        private List<Dictionary<VideoInfoRequest, string>> FVideosInformation = new List<Dictionary<VideoInfoRequest, string>>();
        private List<Dictionary<AudioInfoRequest, string>> FAudiosInformation = new List<Dictionary<AudioInfoRequest, string>>();
        private List<Dictionary<ImageInfoRequest, string>> FImagesInformation = new List<Dictionary<ImageInfoRequest, string>>();

        #region DPS FIELD

        public Size FFrameSize = new Size();
        public long FFrameCount = 0;
        public bool FAlpha = false;
        public long FileSize = 0;
        public int FFrameRate = 0;
        private FileStream fs = null;

        #endregion


        public MediaInformations()
        {
            MI = new MediaInfo();
        }


        #region PUBLIC METHODS


        public void Dispose()
        {
            Close();
        }

        public void Open(string filename)
        {
            int x = MI.Open(filename);
            this.Filename = filename;
            FActive = (x == 1);

            if (FActive)
            {
                FGeneralInformation.Clear();
                FVideosInformation.Clear();
                FAudiosInformation.Clear();
                FImagesInformation.Clear();

                ScanFile(filename);

                FVideoCount = 0;
                int.TryParse(MI.Get(StreamKind.General, 0, "VideoCount"), out FVideoCount);

                FAudioCount = 0;
                int.TryParse(MI.Get(StreamKind.General, 0, "AudioCount"), out FAudioCount);

                FImageCount = 0;
                int.TryParse(MI.Get(StreamKind.General, 0, "ImageCount"), out FImageCount);


                foreach (string rVal in Enum.GetNames(typeof(GeneralInfoRequest)))
                {
                    string nomeValore = rVal;
                    nomeValore = rVal.Replace("_str", "/String");
                    nomeValore = rVal.Replace("__s", "(s)");

                    FGeneralInformation.Add((GeneralInfoRequest)Enum.Parse(typeof(GeneralInfoRequest), rVal), MI.Get(StreamKind.General, 0, nomeValore));
                }

                for (int i = 0; i < FVideoCount; i++)
                {
                    Dictionary<VideoInfoRequest, string> Values = new Dictionary<VideoInfoRequest, string>();

                    foreach (string rVal in Enum.GetNames(typeof(VideoInfoRequest)))
                    {
                        string nomeValore = rVal;
                        nomeValore = rVal.Replace("_str", "/String");
                        nomeValore = rVal.Replace("__s", "(s)");

                        Values.Add((VideoInfoRequest)Enum.Parse(typeof(VideoInfoRequest), rVal), MI.Get(StreamKind.Video, i, nomeValore));
                    }

                    FVideosInformation.Add(Values);
                }


                for (int i = 0; i < FAudioCount; i++)
                {
                    Dictionary<AudioInfoRequest, string> Values = new Dictionary<AudioInfoRequest, string>();

                    foreach (string rVal in Enum.GetNames(typeof(AudioInfoRequest)))
                    {
                        string nomeValore = rVal;
                        nomeValore = rVal.Replace("_str", "/String");
                        nomeValore = rVal.Replace("__s", "(s)");

                        Values.Add((AudioInfoRequest)Enum.Parse(typeof(AudioInfoRequest), rVal), MI.Get(StreamKind.Audio, i, nomeValore));
                    }

                    FAudiosInformation.Add(Values);
                }

                for (int i = 0; i < FImageCount; i++)
                {
                    Dictionary<ImageInfoRequest, string> Values = new Dictionary<ImageInfoRequest, string>();

                    foreach (string rVal in Enum.GetNames(typeof(ImageInfoRequest)))
                    {
                        string nomeValore = rVal;
                        nomeValore = rVal.Replace("_str", "/String");
                        nomeValore = rVal.Replace("__s", "(s)");

                        Values.Add((ImageInfoRequest)Enum.Parse(typeof(ImageInfoRequest), rVal), MI.Get(StreamKind.Image, i, nomeValore));
                    }

                    FImagesInformation.Add(Values);
                }
            }
        }

        public void Close()
        {
            if (FActive)
            {
                FGeneralInformation.Clear();
                FVideosInformation.Clear();
                FAudiosInformation.Clear();
                FImagesInformation.Clear();
                MI.Close();
            }
        }


        public string GetValue(GeneralInfoRequest value)
        {
            if (FActive)
            {
                if (value == GeneralInfoRequest.Duration && new FileInfo(this.Filename).Extension.ToLower() == ".dps")
                {
                    return (FrameCount * 25) + "";
                }
                else
                {
                    return FGeneralInformation[value];
                }
            }

            throw new Exception("Not Active!");
        }

        public string GetValue(VideoInfoRequest value, int stream)
        {
            if (FActive)
            {
                if (value == VideoInfoRequest.FrameCount && new FileInfo(this.Filename).Extension.ToLower() == ".dps")
                {
                    return FrameCount.ToString();
                }
                if (stream < FVideoCount)
                {
                    if (string.IsNullOrEmpty(FVideosInformation[stream][value]) && value == VideoInfoRequest.Colorimetry)
                    {
                        string aa = FVideosInformation[stream][VideoInfoRequest.Format];

                        if (aa.ToLower().Trim() == "dvsd")
                        {
                            return "4:2:0";
                        }
                        if (aa.ToLower().Trim() == "dv25")
                        {
                            return "4:1:1";
                        }
                        if (aa.ToLower().Trim() == "dv50")
                        {
                            return "4:2:2";
                        }
                        return "4:2:0";
                    }
                    else
                    {

                        switch (value)
                        {
                            case VideoInfoRequest.ScanOrder:
                                {
                                    string val1     = FVideosInformation[stream][value].Trim();
                                    string val2     = FVideosInformation[stream][VideoInfoRequest.ScanOrder_str].Trim();
                                    string codec0   = FVideosInformation[stream][VideoInfoRequest.CodecID].Trim();
                                    string vformat  = FVideosInformation[stream][VideoInfoRequest.Format].Trim();
                                    string scan     = GetValue(VideoInfoRequest.ScanType, stream).Trim();

                                    if (!scan.Equals("Progressive", StringComparison.OrdinalIgnoreCase))
                                    {
                                        string[] BFF_FIX = new string[] { "dvsd", "dvh1", "dvc", "dvcp" };

                                        if (string.IsNullOrEmpty(val1))
                                        {
                                            //if (Array.IndexOf(BFF_FIX, codec0) >= 0)
                                            if(vformat.Equals("DV", StringComparison.OrdinalIgnoreCase))
                                            {
                                                return "BFF";
                                            }
                                        }
                                        else
                                        {
                                            return val1;
                                        }
                                    }
                                    else
                                    {
                                        return "";
                                    }

                                    break;
                                }
                            case VideoInfoRequest.ScanType:
                                {
                                    string result = FVideosInformation[stream][value];

                                    if (result.Trim() == "")
                                    {
                                        string vformat = FVideosInformation[stream][VideoInfoRequest.Format];

                                        if (vformat.Equals("H.263", StringComparison.OrdinalIgnoreCase)) return "Progressive";
                                        if (vformat.Equals("H.264", StringComparison.OrdinalIgnoreCase)) return "Progressive";
                                        if (vformat.Equals("AVC", StringComparison.OrdinalIgnoreCase)) return "Progressive";
                                        if (vformat.Equals("DV", StringComparison.OrdinalIgnoreCase)) return "Interlaced";
                                    }

                                    return result;
                                }
                            default:
                                {
                                    return FVideosInformation[stream][value];
                                    break;
                                }
                        }
                    }
                }

                throw new Exception("Stream not found");
            }

            throw new Exception("Not Active!");
        }

        public string GetValue(AudioInfoRequest value, int stream)
        {
            if (FActive)
            {
                if (stream < FAudioCount)
                {
                    return FAudiosInformation[stream][value];
                }

                throw new Exception("Stream not found");
            }

            throw new Exception("Not Active!");
        }

        public string GetValue(ImageInfoRequest value, int stream)
        {
            if (FActive)
            {
                if (stream < FImageCount)
                {
                    return FImagesInformation[stream][value];
                }

                throw new Exception("Stream not found");
            }

            throw new Exception("Not Active!");
        }

        public ScanOrders GetScanorder()
        {
            ScanOrders result = ScanOrders.undefined;

            if (FActive)
            {
                if (this.VideoCount > 0)
                {
                    result = GetScanorder(0);
                }
            }

            return result;
        }

        public ScanOrders GetScanorder(int idStream)
        {
            ScanOrders result = ScanOrders.undefined;

            if (FActive)
            {
                string so_str = GetValue(VideoInfoRequest.ScanOrder, idStream);

                object so_obj = Enum.Parse(typeof(ScanOrders), so_str);

                if (so_obj != null) result = (ScanOrders)so_obj;
            }

            return result;
        }

        #endregion PUBLIC METHODS


        #region DPS


        private void ScanFile(string FFilename)
        {
            try
            {
                if (FActive)
                {
                    fs = new FileStream(FFilename, FileMode.Open, FileAccess.Read, FileShare.Read);

                    byte[] riff1 = new byte[4];

                    fs.Read(riff1, 0, 4);

                    FileInfo fInfo = new FileInfo(FFilename);

                    FileSize = fInfo.Length;

                    if (Encoding.Default.GetString(riff1) == "DPS0")
                    {
                        FFrameCount = GetFrameCount();
                        FFrameSize = GetFrameSize();
                        FAlpha = GetAlpha();
                        FFrameRate = GetFrameRate();
                    }

                    fs.Close();
                }
            }
            catch { }
        }

        private Size GetFrameSize()
        {
            Size s = new Size();

            if (FActive)
            {
                fs.Seek(304, SeekOrigin.Begin);

                byte[] b2 = new byte[2];
                fs.Read(b2, 0, 2);
                s.Width = BitConverter.ToInt16(b2, 0);

                fs.Read(b2, 0, 2);
                s.Height = BitConverter.ToInt16(b2, 0);
            }

            return s;
        }

        private long GetFrameCount()
        {
            long f = 0;

            if (FActive)
            {
                fs.Seek(320, SeekOrigin.Begin);

                byte[] b4 = new byte[4];
                fs.Read(b4, 0, 4);
                f = BitConverter.ToInt32(b4, 0);
            }

            return f;
        }

        private int GetFrameRate()
        {
            int fr = 0;

            if (FActive)
            {
                fs.Seek(314, SeekOrigin.Begin);

                byte[] b2 = new byte[2];
                fs.Read(b2, 0, 2);
                fr = BitConverter.ToInt16(b2, 0);
            }

            return fr;
        }

        private bool GetAlpha()
        {
            bool a = false;

            if (FActive)
            {
                fs.Seek(8, SeekOrigin.Begin);

                int b = fs.ReadByte();

                if (b == 6)
                {
                    a = false;
                }
                else if (b == 7)
                {
                    a = true;
                }
            }

            return a;
        }


        #endregion


        #region PUBLIC PROPERTIES


        public int VideoCount
        {
            get { return FVideoCount; }
        }

        public int AudioCount
        {
            get { return FAudioCount; }
        }

        public int ImageCount
        {
            get { return FImageCount; }
        }

        public bool Alpha
        {
            get
            {
                return FAlpha;
            }
        }

        public long FrameCount
        {
            get
            {
                return FFrameCount;
            }
        }

        public Size FrameSize
        {
            get
            {
                return FFrameSize;
            }
        }

        public int FrameRate
        {
            get
            {
                return FFrameRate;
            }
        }


        #endregion PUBLIC PROPERTIES

    }

}
