﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Runtime.InteropServices.ComTypes;
using System.Runtime.InteropServices;
using DirectShowLib;



namespace MediaAlliance.AV.DS.SubClasses
{
    public class CEnumerate
    {

        public enum FilterType
        {
            VideoInput,
            AudioInput,
            VideoCompressorCategory
        }

        public class LibInfo
        {
            public string FriendlyName = "";
            public string Description = "";
            public string DevicePath = "";
        }

        public static Dictionary<int, IMoniker> Get_ListOfVideoAcquireHardware(FilterType tipo)
        {
            IEnumMoniker classEnum = null;
            IMoniker[] moniker = new IMoniker[1];

            // L'enumeratore di Devices.. questo oggetto può enumerare i devices presenti nella macchina
            ICreateDevEnum devEnum = (ICreateDevEnum)new CreateDevEnum();

            int hr = 0;

            switch (tipo)
            {
                case FilterType.VideoInput: hr = devEnum.CreateClassEnumerator(FilterCategory.VideoInputDevice, out classEnum, 0); break;
                case FilterType.AudioInput: hr = devEnum.CreateClassEnumerator(FilterCategory.AudioInputDevice, out classEnum, 0); break;
                case FilterType.VideoCompressorCategory: hr = devEnum.CreateClassEnumerator(FilterCategory.VideoCompressorCategory, out classEnum, 0); break;
            }

            if (hr != 0) throw new Exception("return code from COM: " + hr);

            Marshal.ReleaseComObject(devEnum);

            // Scorre la lista dei devices disponibili
            List<IMoniker> MList = new List<IMoniker>();

            int EndOfMoniker;
            while (true)
            {
                EndOfMoniker = classEnum.Next(moniker.Length, moniker, IntPtr.Zero);
                if (EndOfMoniker == 1)
                {
                    break;
                }
                if (moniker.Length > 0) MList.Add(moniker[0]);
            }

            // Genera la lista da ritornare
            Dictionary<int, IMoniker> toReturn = new Dictionary<int, IMoniker>();

            int k = 0;

            foreach (IMoniker it in MList)
            {
                toReturn.Add(k, it);
                k += 1;
            }


            MList.Clear();
            MList = null;

            // Puliamo un pò la memoria

            Marshal.ReleaseComObject(classEnum);
            GC.Collect();

            return toReturn;
        }

        public static Dictionary<int, LibInfo> Get_FriendlyListOfVideoAcquireHardware(ref Dictionary<int, IMoniker> IMonikerList)
        {
            IPropertyBag IProp = null;
            Dictionary<int, LibInfo> toReturn = new Dictionary<int, LibInfo>();

            // Le proprietà che intendiamo leggere

            foreach (KeyValuePair<int, IMoniker> it in IMonikerList)
            {
                LibInfo lInfo = new LibInfo();

                object FName = null;
                object FPath = null;
                object FDesc = null;

                object xx = null;
                Guid guid = typeof(IPropertyBag).GUID;
                it.Value.BindToStorage(null, null, ref guid, out xx);
                IProp = (IPropertyBag)xx;
                // Legge il nome UMANO della periferica!!! 
                IProp.Read("CLSID", out FPath, null);
                IProp.Read("Description", out FDesc, null);
                IProp.Read("FriendlyName", out FName, null);
                string devicePath = (string)FPath;

                lInfo.FriendlyName = (string)FName;
                lInfo.Description = (string)FDesc;
                lInfo.DevicePath = (string)FPath;

                toReturn.Add(it.Key, lInfo);
            }

            Marshal.ReleaseComObject(IProp);
            return toReturn;
        }

    }
}
