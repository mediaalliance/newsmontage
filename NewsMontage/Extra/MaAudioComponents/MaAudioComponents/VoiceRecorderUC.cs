using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using Microsoft.DirectX.DirectSound;
using Microsoft.DirectX.AudioVideoPlayback;

using Telerik.WinControls.UI;
using Telerik.WinControls;


using System.Diagnostics;

namespace MediaAlliance.AV
{
    public partial class VoiceRecorderUC : UserControl
    {
        #region Private members

        private AudioDevice m_audioVoiceRecorder = null;
        private int m_selectedAudioDevID = -1;

        #region Microphone and Volumes objects
        private const int SAMPLES = 8;
        private int m_sampleDelay = 200;
        private int m_frameDelay = 10;
        private static int[] SAMPLE_FORMAT_ARRAY = { SAMPLES, 2, 1 };
        private static CaptureDevicesCollection m_audioDevices = null; 
        private Microsoft.DirectX.DirectSound.CaptureBuffer m_buffer = null;
        private System.Threading.Thread m_liveVolumeThread;
        #endregion

        private int m_idTrack = 0;

        private string m_audioFilename = "voice_recorder_temp.wav";

        private TrackState m_trackState = TrackState.Stop;
        private EventHandler m_trackBar1_ValueChanged = null;

        private bool m_selected = false;
        #endregion

        #region Public Members and Properties

        public enum TrackState
        {
            Stop=0,
            Play,
            Recording,
            Pause
        }

        public string FileName { get { return m_audioFilename; } set { m_audioFilename = value; } }

        public int ID
        {
            get { return m_idTrack; }
            set
            {
                int oldIdTrack = m_idTrack;

                m_idTrack = value;

               
                try
                {
                    if (oldIdTrack != m_idTrack)
                    {
                        if (!m_audioFilename.Equals(""))
                        {
                            if (m_audioVoiceRecorder != null)
                            {
                                m_audioVoiceRecorder.StopRecord();
                                m_audioVoiceRecorder.Dispose();
                                m_audioVoiceRecorder = null;
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                
            }
        }

//        public bool IsSelectionShowed { get { return m_waveFormUC.IsSelectionShowed; } }

        public bool IsSelected { get { return m_selected; } }

        public bool SelectTrack {
            set 
            { 
                m_selected = value; 

                if(m_selected)
                    this.BackColor = Color.Gold; 
                else
                    this.BackColor = Color.FromArgb(255, 60, 60, 60);
            }
        }

        public bool IsRecording { get { return m_trackState == TrackState.Recording ? true : false; } }

        #endregion

        #region Constructors
        public VoiceRecorderUC():base()
        {
            InternalConstructor(0, "temp.wav");
        }

        public VoiceRecorderUC(string filename)
        {
            InternalConstructor(0, filename);
        }

        public VoiceRecorderUC(int idTrack, string filename)
        {
            InternalConstructor(idTrack, filename);
        }


        private void InternalConstructor(int idTrack, string filename)
        {
            try
            {
                #region Initialize graphics environment
                InitializeComponent();

                m_idTrack = idTrack;

                this.SetStyle(ControlStyles.AllPaintingInWmPaint |
                                ControlStyles.UserPaint | ControlStyles.DoubleBuffer, true);
                DoubleBuffered = true;
                this.BackColor = Color.FromArgb(255, 60, 60, 60);

                #endregion


                m_audioFilename = Application.StartupPath + "\\" + filename;

                //Local Events
                //                m_trackBar1_ValueChanged = new EventHandler(trackBar1_ValueChanged);
                this.Load += new EventHandler(AudioTrackUC_Load);
                this.Disposed += new EventHandler(AudioTrackUC_Disposed);
            }
            catch (Exception ex)
            {
                RadMessageBox.Show("AudioTrackUC :" + ex.Message);
               
            }
        }
        #endregion


        #region Public Methods
 
        public void CloseAll()
        {
            StopRecording();
        }

        public void StartRecording()
        {
            Cursor = Cursors.WaitCursor;

            if (m_trackState == TrackState.Recording)
                return;

            try
            {
                //Check if there is some selected input device.
                if (m_selectedAudioDevID != -1)
                {

                    try
                    {
                        #region Create an audio recorder object
                        m_audioVoiceRecorder = new AudioDevice(m_selectedAudioDevID);
                        m_audioVoiceRecorder.CreateSoundFile(m_audioFilename);

                        //Start to Record
                       
                         m_audioVoiceRecorder.StartRecord();

                        
                        m_trackState = TrackState.Recording;
                        #endregion
                    }
                    catch(Exception ex1)
                    {
                        #region Destroy current audio recorder object
                        if (m_audioVoiceRecorder != null)
                        {
                            m_audioVoiceRecorder.StopRecord();
                            m_audioVoiceRecorder.Dispose();
                            m_audioVoiceRecorder = null;
                        }
                        #endregion

                        
                    }
                }
                else
                {
                    MessageBox.Show("Select Input Audio Device", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
               
            }

            Cursor = Cursors.Default;
        }

        public void StopRecording()
        {
            Cursor = Cursors.WaitCursor;
    
            #region during recording process
            try
            {
                if (m_audioVoiceRecorder != null)
                {
                    m_audioFilename = m_audioVoiceRecorder.FileName;
                    m_audioVoiceRecorder.StopRecord();
                    m_audioVoiceRecorder.Dispose();
                    m_audioVoiceRecorder = null;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                
            }
            #endregion

            Cursor = Cursors.Default;
            m_trackState = TrackState.Stop;
        }

        #endregion

        #region Form and Objects Events

        private void AudioTrackUC_Load(object sender, EventArgs e)
        {
            if (DesignMode)
                return;

            try
            {
                #region Setup Microphone objects
                m_audioDevices = new CaptureDevicesCollection();

                AudioDevice.DeviceInfo[] DeviceList = AudioDevice.GetDevicesList();

                if(DeviceList == null)
                {
                    MessageBox.Show("Audio device list is empty", "Voice Recorder" );
                    return;
                }

                int maxDev = DeviceList.Length;
                if (maxDev == 0)
                {
                    MessageBox.Show("Audio device list is empty", "Voice Recorder");
                    return;
                }

                //AudioDevice.DeviceInfo devInfo = DeviceList[0];
                //m_selectedAudioDevID = devInfo.ID;
                m_selectedAudioDevID = 0;
                if (m_selectedAudioDevID >= 0 && m_selectedAudioDevID < maxDev)
                    StartCaptureForVuMeter();
                else{
                    MessageBox.Show("Check 'DeviceInfo' Entry inside [Audio] Section of Settings.ini file.\nThe Index is out of limit", "Voice Recorder");

                    string audioHWList = "";
                    foreach(AudioDevice.DeviceInfo devInfo in DeviceList)
                        audioHWList += devInfo.Name+"   ID:"+devInfo.ID+"\n";

                    MessageBox.Show(audioHWList, "Voice Recorder - Actual Audio Device list:");
                }
                    

                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(" Voice Recorder :" + ex.Message);
            }
            //this.panelBaseAudio.HorizontalScroll.Enabled = true;
            //this.panelBaseAudio.HorizontalScroll.Visible = true;
        }

        
        private void AudioTrackUC_Disposed(object sender, EventArgs e)
        {
            try
            {
                if (m_liveVolumeThread != null)
                {
                    m_liveVolumeThread.Abort();
                    m_liveVolumeThread.Join();

                    m_liveVolumeThread = null;
                }
            }
            catch { }

            try
            {
                if (m_buffer != null)
                {
                    m_buffer.Stop();
                    m_buffer.Dispose();
                    m_buffer = null;
                }
            }
            catch { }

            try
            {
                if (m_audioVoiceRecorder != null)
                {
                    m_audioVoiceRecorder.StopRecord();
                    m_audioVoiceRecorder.Dispose();
                }
            }
            catch { }

        }

        #endregion

        #region VuMeter methods
        private void StartCaptureForVuMeter()
         {
             if (m_selectedAudioDevID != -1)
             {
                 if (m_liveVolumeThread != null)
                 {
                     m_liveVolumeThread.Abort();
                     m_liveVolumeThread.Join();
                     m_liveVolumeThread = null;
                 }

                 if (m_buffer != null)
                 {
                     m_buffer.Stop();
                     m_buffer.Dispose();
                     m_buffer = null;
                 }

                 // initialize the capture buffer and start the animation thread
                 Capture cap = new Capture(m_audioDevices[m_selectedAudioDevID].DriverGuid);
                 CaptureBufferDescription desc = new CaptureBufferDescription();
                 WaveFormat wf = new WaveFormat();
                 wf.BitsPerSample = 16;
                 wf.SamplesPerSecond = 48000;
                 wf.Channels = 2;
                 wf.BlockAlign = (short)(wf.Channels * wf.BitsPerSample / 8);
                 wf.AverageBytesPerSecond = wf.BlockAlign * wf.SamplesPerSecond;
                 wf.FormatTag = WaveFormatTag.Pcm;

                 desc.Format = wf;
                 desc.BufferBytes = SAMPLES * wf.BlockAlign;
                 //desc.BufferBytes = wf.AverageBytesPerSecond * wf.BlockAlign;

                 try
                 {
                     m_buffer = new Microsoft.DirectX.DirectSound.CaptureBuffer(desc, cap);
                 }
                 catch { }

                 if (m_buffer == null)
                 {
                     cap.Dispose();
                     MessageBox.Show("An audio recording device cannot be found", "Voice Recorder");
                     return;
                 }
                 m_buffer.Start(true);

                 m_liveVolumeThread = new Thread(new ThreadStart(updateProgress));
//                 m_liveVolumeThread.Priority = ThreadPriority.Lowest;
                 m_liveVolumeThread.Start();
                
             }
         }

        private void updateProgress()
        {
            while (true)
            {
                int tempFrameDelay = m_frameDelay;
                int tempSampleDelay = m_sampleDelay;
                Array samples = m_buffer.Read(0, typeof(Int16), LockFlag.FromWriteCursor, SAMPLE_FORMAT_ARRAY);

                // for each channel, determine the step size necessary for each iteration
                int leftGoal = 0;
                int rightGoal = 0;

                // average across all samples to get the goals
                for (int i = 0; i < SAMPLES; i++)
                {
                    leftGoal += (Int16)samples.GetValue(i, 0, 0);
                    rightGoal += (Int16)samples.GetValue(i, 1, 0);
                }

                leftGoal = (int)(leftGoal * 0.08f);
                rightGoal = (int)(rightGoal * 0.08f);

                leftGoal = (int)Math.Abs(leftGoal / SAMPLES);
                rightGoal = (int)Math.Abs(rightGoal / SAMPLES);

                double range1 = leftGoal - vuMeterL.Value; //progressBar1.Value;
                double range2 = rightGoal - vuMeterR.Value;

                double exactValue1 = vuMeterL.Value; // progressBar1.Value;
                double exactValue2 = vuMeterR.Value;

                double stepSize1 = range1 / tempSampleDelay * tempFrameDelay;
                if (Math.Abs(stepSize1) < .01)
                {
                    stepSize1 = Math.Sign(range1) * .01;
                }
                double absStepSize1 = Math.Abs(stepSize1);

                double stepSize2 = range2 / tempSampleDelay * tempFrameDelay;
                if (Math.Abs(stepSize2) < .01)
                {
                    stepSize2 = Math.Sign(range2) * .01;
                }
                double absStepSize2 = Math.Abs(stepSize2);

                // increment/decrement the bars' values until both equal their desired goals,
                // sleeping between iterations
                if ((vuMeterL.Value == leftGoal) && (vuMeterR.Value == rightGoal))
                {
                    Thread.Sleep(tempSampleDelay);
                }
                else
                {

                    //	do
                    //	{
                    if (vuMeterL.Value != leftGoal)
                    {
                        if (absStepSize1 < Math.Abs(leftGoal - vuMeterL.Value))
                        {
                            exactValue1 += stepSize1;
                            int exVal = (int)Math.Round(exactValue1);
                            if (exVal > vuMeterL.Maximum)
                                vuMeterL.Value = vuMeterL.Maximum;
                            else
                                vuMeterL.Value = (int)Math.Round(exactValue1);
                        }
                        else
                        {
                            vuMeterL.Value = leftGoal;
                        }
                    }


                    if (vuMeterR.Value != rightGoal)
                    {
                        if (absStepSize2 < Math.Abs(rightGoal - vuMeterR.Value))
                        {
                            exactValue2 += stepSize2;
                            int exVal = (int)Math.Round(exactValue2);
                            if (exVal > vuMeterR.Maximum)
                                vuMeterR.Value = vuMeterR.Maximum;
                            else
                                vuMeterR.Value = (int)Math.Round(exactValue2);
                        }
                        else
                        {
                            vuMeterR.Value = rightGoal;
                        }
                    }

                    Thread.Sleep(tempFrameDelay);
                    //    } while ((m_volumeLevelUC.Value != leftGoal) || (progressBar2.Value != rightGoal));

                }
                 
            }
        }
         #endregion

      

     
    }
}
