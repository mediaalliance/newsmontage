using System;

namespace MediaAlliance.AV
{
    partial class VoiceRecorderUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            try
            {
                if (m_liveVolumeThread != null)
                {
                    m_liveVolumeThread.Abort();
                    m_liveVolumeThread.Join();
                    m_liveVolumeThread = null;
                }
            }
            catch { }

            try
            {
                if (m_buffer != null)
                {
                    m_buffer.Stop();
                    m_buffer.Dispose();
                    m_buffer = null;
                }
            }
            catch { }

            try
            {
                if (m_audioVoiceRecorder != null)
                {
                    m_audioVoiceRecorder.StopRecord();
                    m_audioVoiceRecorder.Dispose();
                }
            }
            catch { }

          
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.aquaTheme1 = new Telerik.WinControls.Themes.AquaTheme();
            this.vuMeterR = new MediaAlliance.AV.VolumeLevelUC();
            this.vuMeterL = new MediaAlliance.AV.VolumeLevelUC();
            this.SuspendLayout();
            // 
            // vuMeterR
            // 
            this.vuMeterR.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.vuMeterR.Location = new System.Drawing.Point(44, 3);
            this.vuMeterR.Maximum = 200;
            this.vuMeterR.Minimum = 0;
            this.vuMeterR.Name = "vuMeterR";
            this.vuMeterR.Size = new System.Drawing.Size(39, 132);
            this.vuMeterR.TabIndex = 10;
            this.vuMeterR.Value = 0;
            // 
            // vuMeterL
            // 
            this.vuMeterL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.vuMeterL.Location = new System.Drawing.Point(3, 3);
            this.vuMeterL.Maximum = 200;
            this.vuMeterL.Minimum = 0;
            this.vuMeterL.Name = "vuMeterL";
            this.vuMeterL.Size = new System.Drawing.Size(39, 132);
            this.vuMeterL.TabIndex = 9;
            this.vuMeterL.Value = 0;
            // 
            // VoiceRecorderUC
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.DimGray;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.vuMeterR);
            this.Controls.Add(this.vuMeterL);
            this.DoubleBuffered = true;
            this.Name = "VoiceRecorderUC";
            this.Size = new System.Drawing.Size(86, 138);
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.Themes.AquaTheme aquaTheme1;
        private VolumeLevelUC vuMeterL;
        private VolumeLevelUC vuMeterR;
    }
}
