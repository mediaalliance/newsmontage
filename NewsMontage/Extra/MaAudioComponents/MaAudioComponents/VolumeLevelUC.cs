using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace MediaAlliance.AV
{
    public partial class VolumeLevelUC: UserControl
    {
        #region Private members
        private RectangleF m_recGoodPart;
        private LinearGradientBrush m_brushGoodPart;

        //Make a set of colors to use in the blend
        private Color[] m_gradientColors = {Color.DarkGreen,
                                            Color.Yellow,
                                            Color.Red};

        //These are the positions of the colors along the Gradient line
        private float[] m_colorPositions = { 0.0f, .80f, 1.0f };

        private int m_min = 0;
        private int m_max = 100;
        private int m_val = 0;
        #endregion

        #region Public Properties
        public int Minimum
        {
            get
            {
                return m_min;
            }

            set
            {
                // Prevent a negative value.
                if (value < 0)
                {
                    m_min = 0;
                }

                // Make sure that the minimum value is never set higher than the maximum value.
                if (value > m_max)
                {
                    m_min = value;
                    m_min = value;
                }

                // Ensure value is still in range
                if (m_val < m_min)
                {
                    m_val = m_min;
                }

                // Invalidate the control to get a repaint.
                this.Invalidate();
            }
        }

        public int Maximum
        {
            get
            {
                return m_max;
            }

            set
            {
                // Make sure that the maximum value is never set lower than the minimum value.
                if (value < m_min)
                {
                    m_min = value;
                }

                m_max = value;

                // Make sure that value is still in range.
                if (m_val > m_max)
                {
                    m_val = m_max;
                }

                // Invalidate the control to get a repaint.
                this.Invalidate();
            }
        }

        public int Value
        {
            get
            {
                return m_val;
            }

            set
            {
                int oldValue = m_val;

                // Make sure that the value does not stray outside the valid range.
                if (value < m_min)
                {
                    m_val = m_min;
                }
                else if (value > m_max)
                {
                    m_val = m_max;
                }
                else
                {
                    m_val = value;
                }

                // Invalidate only the changed area.
                float percent;

                Rectangle newValueRect = this.ClientRectangle;
                Rectangle oldValueRect = this.ClientRectangle;

                // Use a new value to calculate the rectangle for progress.
                percent = (float)(m_val - m_min) / (float)(m_max - m_min);
                newValueRect.Width = (int)((float)newValueRect.Width - (float)newValueRect.Width * percent);

                // Use an old value to calculate the rectangle for progress.
                percent = (float)(oldValue - m_min) / (float)(m_max - m_min);
                oldValueRect.Width = (int)((float)oldValueRect.Width - (float)oldValueRect.Width * percent);

                Rectangle updateRect = new Rectangle();

                // Find only the part of the screen that must be updated.
                if (newValueRect.Width > oldValueRect.Width)
                {
                    updateRect.X = oldValueRect.Size.Width;
                    updateRect.Width = newValueRect.Width - oldValueRect.Width;
                }
                else
                {
                    updateRect.X = newValueRect.Size.Width;
                    updateRect.Width = oldValueRect.Width - newValueRect.Width;
                }

                updateRect.Height = this.Height ;

                // Invalidate the intersection region only.
                this.Invalidate();
            }
        }
        #endregion

        #region Constructors
        public VolumeLevelUC()
        {
            InitializeComponent();

            this.SetStyle(ControlStyles.AllPaintingInWmPaint |
                           ControlStyles.UserPaint | ControlStyles.DoubleBuffer, true);

            this.DoubleBuffered = true;
            //this.BackColor = Color.White;

            InitializeBrush();
        }
        #endregion

        #region Painting methods

        private void InitializeBrush()
        {
            m_recGoodPart = new RectangleF(new PointF(0.0f, ClientRectangle.Height), new SizeF(ClientRectangle.Width, ClientRectangle.Height));
            m_brushGoodPart = new System.Drawing.Drawing2D.LinearGradientBrush(m_recGoodPart, Color.White, Color.Black, -90.0f);

            //Fill the blend object with the colors and their positions
            ColorBlend C_Blend = new ColorBlend();
            C_Blend.Colors = m_gradientColors;
            C_Blend.Positions = m_colorPositions;
            m_brushGoodPart.InterpolationColors = C_Blend;

            // Invalidate the control to get a repaint.
            this.Invalidate();
        }

     
        protected override void OnResize(EventArgs e)
        {
            InitializeBrush();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Bitmap bmp = new Bitmap(this.ClientRectangle.Width, this.ClientRectangle.Height);
            Graphics g = Graphics.FromImage(bmp);

            float percent = (float)(m_val - m_min) / (float)(m_max - m_min);
            Rectangle rect = this.ClientRectangle;

            // Calculate area for drawing the progress.
            rect.Y = (int)(rect.Height-((float)rect.Height * percent));
            rect.Height = (int)(rect.Height * percent);
//            rect.Width = (int)(rect.Width * percent);

            g.FillRectangle(new SolidBrush (Color.Black), this.ClientRectangle);
            g.FillRectangle(m_brushGoodPart, rect);

            g.Dispose();

            e.Graphics.DrawImage(bmp, 0, 0);
            bmp.Dispose();
        }

      
        #endregion
    }
}
