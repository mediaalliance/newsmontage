using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using MediaAlliance.NewsWiresBase;
using System.Threading;
using System.Collections;
using MXPUtils;

namespace NewsWires
{
	class FolderWatcher
	{
		private FileSystemWatcher watcher;
		private AgencyOptions agency;
		private string assemblyName;

		private List<string> changedFiles = new List<string>();
		private Timer tmrCreateWire;

		private string folder;
		private string filter;

		public string Name
		{
			get
			{
				if (agency == null)
					return "(unknown)";

				return agency.GetAttribute("name");
			}
		}

		public FolderWatcher(XmlElement node)
		{
			agency = new AgencyOptions(node);
		}

		internal void Start()
		{
			// timer to collect all signals coming from all events' firing related to the same file
			tmrCreateWire = new Timer(new TimerCallback(NewWire), null, Timeout.Infinite, Timeout.Infinite);

			try
			{
				assemblyName = agency.GetValueString("assembly");
				folder = agency.GetValueString("news_folder");
				filter = agency.GetValueString("file_filter", "");

				InitWatcher();

				CLog.LogSuccess("Folder Watcher [" + Name + "] successfully started on folder " + folder + ".");
			}
			catch (Exception ex)
			{
				CLog.Error("Failed starting Folder Watcher [" + Name + "]", ex);
			}
		}

		private void InitWatcher()
		{
			watcher = new FileSystemWatcher(folder);
			watcher.IncludeSubdirectories = false;
			if (!String.IsNullOrEmpty(filter))
				watcher.Filter = filter;

			watcher.Created += new FileSystemEventHandler(watcher_Created);
			if (agency.GetValueBool("handle_modified_files"))
				watcher.Changed += new FileSystemEventHandler(watcher_Changed);
			if (agency.GetValueBool("handle_renamed_files"))
				watcher.Renamed += new RenamedEventHandler(watcher_Renamed);

			watcher.Error += new ErrorEventHandler(watcher_Error);

			watcher.EnableRaisingEvents = true;
		}

		private void watcher_Error(object sender, ErrorEventArgs e)
		{
			CLog.Error("Folder Watcher [" + Name + "] error.", e.GetException());

			if (!agency.GetValueBool("retry_on_watcher_error"))
			{
				CLog.Warning("Retry option not specified, stopping watcher [" + Name + "]...");
				Stop();
				return;
			}

			watcher.EnableRaisingEvents = false;
			int retryNumber = agency.GetValueInt("retries_number");
			int retryPause = agency.GetValueInt("retries_pause_sec", 30);
			int i = 0;
			while (!watcher.EnableRaisingEvents && (retryNumber == 0 || i++ < retryNumber))
			{
				try
				{
					InitWatcher();
				}
				catch (Exception ex)
				{
					CLog.Error("Restarting watcher [" + Name + "], attempt failed. Retrying...", ex);
					System.Threading.Thread.Sleep(retryPause * 1000);
				}
			}

			if (!watcher.EnableRaisingEvents)
			{
				CLog.Warning("Reached maximum number of retries, stopping watcher [" + Name + "]...");
				Stop();
				return;
			}

			CLog.LogSuccess("Folder Watcher [" + Name + "] successfully restarted.");
		}

		internal void Stop()
		{
			watcher.EnableRaisingEvents = false;

			CLog.Log("Folder Watcher [" + Name + "] successfully stopped.");
		}

		private void watcher_Renamed(object sender, RenamedEventArgs e)
		{
			lock (changedFiles)
			{
				changedFiles.Add(e.FullPath);
			}
			tmrCreateWire.Change(1000, System.Threading.Timeout.Infinite);
		}

		private void watcher_Created(object sender, FileSystemEventArgs e)
		{
			// thread to check the availability of arrived files
			Thread thrCheckClosedFiles = new Thread(new ParameterizedThreadStart(CheckClosedFile));
			thrCheckClosedFiles.Name = "Check closed files";
			thrCheckClosedFiles.IsBackground = true;
			thrCheckClosedFiles.Start(new object[] { e.FullPath });


			//// to ensure that the file is closed
			//DateTime now = DateTime.Now;
			//while (new TimeSpan(DateTime.Now.Ticks - now.Ticks).TotalSeconds < 5)
			//{
			//    try
			//    {
			//        FileStream fs = new FileStream(e.FullPath, FileMode.Open, FileAccess.Read, FileShare.None);
			//        fs.Close();
			//    }
			//    catch
			//    {
			//        Thread.Sleep(10);
			//    }
			//}

			//lock (changedFiles)
			//{
			//    changedFiles.Add(e.FullPath);
			//}
			//tmrCreateWire.Change(1000, System.Threading.Timeout.Infinite);
		}

		private void watcher_Changed(object sender, FileSystemEventArgs e)
		{
			// thread to check the availability of arrived files
			Thread thrCheckClosedFiles = new Thread(new ParameterizedThreadStart(CheckClosedFile));
			thrCheckClosedFiles.Name = "Check closed files";
			thrCheckClosedFiles.IsBackground = true;
			thrCheckClosedFiles.Start(new object[] { e.FullPath });

			//lock (changedFiles)
			//{
			//    changedFiles.Add(e.FullPath);
			//}
			//tmrCreateWire.Change(1000, System.Threading.Timeout.Infinite);

			// this event is fired:
			// 3 times when a new file is created: must be always ignored
			// 2 times when a file has changed: second time must be ignored

			//if (createdFiles.Contains(e.Name))	// event is fired by creation of new file
			//{
			//    FileInfo file = new FileInfo(e.FullPath);
			//    DateTime lastWrite = (DateTime) ((object[]) createdFiles[e.Name])[0];
			//    if (lastWrite == file.LastWriteTime) // date is the same, event is fired by creation
			//    {
			//        int numTimes = (int) ((object[]) createdFiles[e.Name])[1];
			//        if (numTimes == 2)	// last event, file can be removed from list
			//            createdFiles.Remove(e.Name);
			//        else
			//            createdFiles[e.Name] = new object[] { lastWrite, ++numTimes }; // updating the number of events fired

			//        return; // ignore the event
			//    }
			//}
			//else // event is fired by file modify
			//{
			//    if (changedFiles.Contains(e.Name))	// second event fired
			//    {
			//        changedFiles.Remove(e.Name);	// remove and
			//        return;							// ignore
			//    }

			//    changedFiles.Add(e.Name);	// add this event and go on creating the wire
			//}
			
			//CreateWire(e.FullPath);
		}

		private void CheckClosedFile(object state)
		{
			string fileName;
			try
			{
				fileName = ((object[]) state)[0].ToString();
			}
			catch
			{
				return;
			}

			// to ensure that the file is closed
			DateTime now = DateTime.Now;
			bool closed = false;
			while (!closed && new TimeSpan(DateTime.Now.Ticks - now.Ticks).TotalSeconds < 5)
			{
				try
				{
					FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.None);
					fs.Close();

					closed = true;
				}
				catch
				{
					Thread.Sleep(500);
				}
			}

			lock (changedFiles)
			{
				changedFiles.Add(fileName);
			}
			tmrCreateWire.Change(1000, System.Threading.Timeout.Infinite);
		}

		private void CreateWire(string fullPath)
		{
			try
			{
				Wire wire = Wire.CreateWire(assemblyName);
				wire.SetAgency(agency.Node);
				wire.ParseFile(fullPath);

				if (!wire.IsBulletin && wire.IsValid)
				{
					lock (WiresManager.Wires)
					{
						WiresManager.Wires.Enqueue(wire);
					}

					WiresManager.EvWiresArrived.Set();

					if (agency.GetValueBool("delete_imported_files"))
						File.Delete(fullPath);
				}
				else if (agency.GetValueBool("delete_ignored_files"))
					File.Delete(fullPath);
			}
			catch (Exception ex)
			{
				CLog.Error("Folder Watcher [" + Name + "]: failed creation of wire from file [" + fullPath + "].", ex);
			}
		}

		private void NewWire(object state)
		{
			lock (changedFiles)
			{
				try
				{
					List<string> processedFiles = new List<string>();
					foreach (string file in changedFiles)
					{
						if (!processedFiles.Contains(file))
						{
							CreateWire(file);
							processedFiles.Add(file);
						}
					}

					changedFiles.Clear();
				}
				catch (Exception ex)
				{
					CLog.Error("Folder Watcher [" + Name + "]: failed call to New Wire procedure", ex);
				}
			}
		}
	}
}
