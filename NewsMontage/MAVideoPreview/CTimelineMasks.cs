﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Xml;
using System.IO;

namespace MediaAlliance.AV.CensorTools
{
    public enum CensorMaskType
    { 
        none,
        pixelSmall,
        pixelMed,
        pixerBig
    }

    public class CCensorTimeline
    {
        public string Name      = "";
        public long m_duration  = 0;

        public List<CTLM_Layer> m_layers = new List<CTLM_Layer>();
        

        #region PUBLIC METHODS

        public CTLM_Layer AddLayer(CensorMaskType mask_type)
        {
            CTLM_Layer new_layer = new CTLM_Layer(this);

            new_layer.Type = mask_type;
            new_layer.ID = m_layers.Count - 1;

            m_layers.Add(new_layer);

            return new_layer;
        }

        public List<CTLM_Keyframe> GetKeyAt(long position)
        {
            List<CTLM_Keyframe> result = new List<CTLM_Keyframe>();

            foreach (CTLM_Layer layer in m_layers)
            {
                CTLM_Keyframe virtual_key = layer.GetKeyAtPos(position);

                if (virtual_key != null)
                {
                    result.Add(virtual_key);
                }
            }

            return result;
        }

        public string Save()
        {
            string xml = "";

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(@"<?xml version=""1.0"" standalone=""yes""?><CENSORTIMELINE></CENSORTIMELINE>");

            XmlNode MAIN = doc["CENSORTIMELINE"];

            foreach (CTLM_Layer layer in m_layers)
            {
                XmlNode layernode = doc.CreateNode(XmlNodeType.Element, "LAYER", "");
                
                XmlNode subnode = doc.CreateNode(XmlNodeType.Element, "ID", "");
                subnode.InnerText = layer.ID.ToString();
                layernode.AppendChild(subnode);

                subnode = doc.CreateNode(XmlNodeType.Element, "TYPE", "");
                subnode.InnerText = layer.Type.ToString();
                layernode.AppendChild(subnode);

                XmlNode keynodes = doc.CreateNode(XmlNodeType.Element, "KEYFRAMES", "");

                foreach (CTLM_Keyframe keyframe in layer.AllKeyframes)
                {
                    XmlNode kf_node = doc.CreateNode(XmlNodeType.Element, "KEYFRAME", "");
                    
                    XmlNode kf_subnode = doc.CreateNode(XmlNodeType.Element, "TIME", "");
                    kf_subnode.InnerText = keyframe.TimePos.ToString();
                    kf_node.AppendChild(kf_subnode);
                    
                    kf_subnode = doc.CreateNode(XmlNodeType.Element, "TYPE", "");
                    kf_subnode.InnerText = keyframe.Type.ToString();
                    kf_node.AppendChild(kf_subnode);

                    kf_subnode = doc.CreateNode(XmlNodeType.Element, "BOUNDS", "");
                    kf_subnode.InnerText = keyframe.Bounds.X + "," + keyframe.Bounds.Y + "," + keyframe.Bounds.Width + "," + keyframe.Bounds.Height;
                    kf_node.AppendChild(kf_subnode);

                    keynodes.AppendChild(kf_node);
                }

                layernode.AppendChild(keynodes);

                MAIN.AppendChild(layernode);
            }

            xml = doc.InnerXml;

            return xml;
        }

        public void SaveFile(string filename)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(this.Save());
            XmlTextWriter writer = new XmlTextWriter(filename, null);
            writer.Formatting = Formatting.Indented;
            doc.Save(writer);
            writer.Close();
        }

        public void Load(string xml)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNode MAIN = doc["CENSORTIMELINE"];

                if (MAIN != null)
                {
                    XmlNodeList layer_nodes = doc.GetElementsByTagName("LAYER");

                    foreach (XmlNode layernode in layer_nodes)
                    {
                        if (layernode.NodeType == XmlNodeType.Element)
                        {
                            CTLM_Layer layer = new CTLM_Layer(this);
                            layer.Type = (CensorMaskType)Enum.Parse(typeof(CensorMaskType), layernode["TYPE"].InnerText);
                            m_layers.Add(layer);
                            layer.ID = int.Parse(layernode["ID"].InnerText);

                            foreach (XmlNode kf_node in layernode["KEYFRAMES"].ChildNodes)
                            {
                                if (kf_node.NodeType == XmlNodeType.Element && kf_node.Name.Equals("KEYFRAME"))
                                {
                                    CTLM_Keyframe keyframe = layer.AddKeyframe(long.Parse(kf_node["TIME"].InnerText));

                                    //keyframe.TimePos = long.Parse(kf_node["TIME"].InnerText);
                                    keyframe.Type = (CensorMaskType)Enum.Parse(typeof(CensorMaskType), kf_node["TYPE"].InnerText);

                                    string[] bb_str = kf_node["BOUNDS"].InnerText.Split(',');

                                    int x, y, w, h;

                                    x = int.Parse(bb_str[0]);
                                    y = int.Parse(bb_str[1]);
                                    w = int.Parse(bb_str[2]);
                                    h = int.Parse(bb_str[3]);

                                    keyframe.Bounds = new Rectangle(x, y, w, h);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            { 
            
            }
        }

        public void LoadFile(string filename)
        {
            if (File.Exists(filename))
            {
                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(filename);

                    this.Load(doc.InnerXml);
                }
                catch
                {                 
                }
            }
        }

        #endregion PUBLIC METHODS


        #region PUBLIC PROPERTIES

        public long Duration
        {
            get { return m_duration; }
            set
            {
                if (m_duration != value)
                {
                    m_duration = value;
                }
            }
        }

        public CTLM_Layer this[int layerindex]
        {
            get 
            {
                if (m_layers.Count > layerindex) return m_layers[layerindex];
                return null;
            }
        }

        public List<CTLM_Layer> Layers
        {
            get { return m_layers; }
        }

        #endregion PUBLIC PROPERTIES
    }





    public class CTLM_Layer
    {
        private CensorMaskType m_type = CensorMaskType.none;
        private List<CTLM_Keyframe> m_keyframes = new List<CTLM_Keyframe>();

        public int ID = -1;

        public CCensorTimeline Owner = null;


        public CTLM_Layer(CCensorTimeline owner)
        {
            Owner = owner;
        }


        #region PUBLIC METHODS

        public CTLM_Keyframe AddKeyframe(long timePos)
        {
            CTLM_Keyframe keyframe = new CTLM_Keyframe(this);
            
            keyframe.TimePos = timePos;
            keyframe.Type = m_type;

            m_keyframes.Add(keyframe);

            return keyframe;
        }

        public CTLM_Keyframe GetKeyAtPos(long timePos)
        {
            CTLM_Keyframe result = null;

            if (m_keyframes.Count == 1)
            {
                result = m_keyframes[0];
                result.IsKeyframe = m_keyframes[0].TimePos == timePos;
            }
            else if (m_keyframes.Count > 1)
            {
                List<CTLM_Keyframe> keyframes = m_keyframes.OrderBy(x => x.TimePos).ToList();

                if (timePos >= keyframes[keyframes.Count - 1].TimePos)
                {
                    // SE LA POSIZIONE RICHIESTA E' UGUALE O OLTRE L'ULTIMO KEYFRAME

                    result = keyframes[keyframes.Count - 1].Copy();
                    result.IsKeyframe = (keyframes[keyframes.Count - 1].TimePos == timePos);
                }
                else if (timePos <= keyframes[0].TimePos)
                {
                    // SE LA POSIZIONE RICHIESTA E' UGUALE O PRECEDENTE AL PRIMO KEYFRAME

                    result = keyframes[0].Copy();
                    result.IsKeyframe = (keyframes[0].TimePos == timePos);
                }
                else
                {
                    // SE LA POSIZIONE RICHIESTA E' A CAVALLO DI DUE KEYFRAMES

                    long prev_pos = -1;
                    long next_pos = 10000000;

                    CTLM_Keyframe prev = null;
                    CTLM_Keyframe next = null;

                    foreach (CTLM_Keyframe kf in keyframes)
                    {
                        if (kf.TimePos <= timePos && kf.TimePos > prev_pos) prev_pos = kf.TimePos;
                        if (kf.TimePos >= timePos && kf.TimePos < next_pos) next_pos = kf.TimePos;
                    }

                    prev = Get_Keyframe(prev_pos);
                    next = Get_Keyframe(next_pos);

                    long relative_time = timePos - prev_pos;
                    long time_diff = next_pos - prev_pos;

                    result = new CTLM_Keyframe(this);

                    result.IsKeyframe = false;
                    result.Type = prev.Type;

                    int n_x = ScaleValue(prev.Bounds.X, next.Bounds.X, prev_pos, next_pos, timePos);
                    int n_y = ScaleValue(prev.Bounds.Y, next.Bounds.Y, prev_pos, next_pos, timePos);
                    int n_w = ScaleValue(prev.Bounds.Width, next.Bounds.Width, prev_pos, next_pos, timePos);
                    int n_h = ScaleValue(prev.Bounds.Height, next.Bounds.Height, prev_pos, next_pos, timePos);
                    
                    result.Bounds = new Rectangle(n_x, n_y, n_w, n_h);
                }
            }

            return result;
        }

        public CTLM_Keyframe Get_Keyframe(long position)
        {
            CTLM_Keyframe result = null;

            List<CTLM_Keyframe> kfs = m_keyframes.Where(x => x.TimePos == position).ToList();

            if (kfs.Count > 0) result = kfs[0];

            return result;
        }

        #endregion PUBLIC METHODS


        private int ScaleValue(int v0, int v1, long t0, long t1, long time)
        {
            int result = 0;
            // (DS / DT) * RT + Actual

            int delta_s     = v1 - v0;
            long delta_t    = t1 - t0;

            result = (int)((double)((double)delta_s / (double)delta_t) * (time - t0)) + v0;

            return result;
        }

        #region PUBLIC PROPERTIES

        public CensorMaskType Type
        {
            get { return m_type; }
            set
            {
                if (m_type != value)
                {
                    m_type = value;

                    foreach(CTLM_Keyframe keyframe in m_keyframes)
                    {
                        keyframe.Type = value;
                    }
                }
            }
        }

        public List<CTLM_Keyframe> AllKeyframes
        {
            get { return m_keyframes; }
        }

        #endregion PUBLIC PROPERTIES
    }

    public class CTLM_Keyframe
    {
        private CensorMaskType m_type = CensorMaskType.none;
        private long m_timepos = 0;
        private Rectangle m_bounds = new Rectangle(-10, -10, 5, 5);     // AZZERO FUORI DAL RIQUADRO
        private CTLM_Layer m_owner = null;
        public bool IsKeyframe = true;

        public CTLM_Keyframe(CTLM_Layer owner)
        {
            m_owner = owner;
        }


        #region PUBLIC METHODS

        public CTLM_Keyframe Copy()
        {
            CTLM_Keyframe result = new CTLM_Keyframe(m_owner);

            result.IsKeyframe = IsKeyframe;
            result.Bounds = m_bounds;
            result.TimePos = m_timepos;
            result.Type = m_type;

            return result;
        }

        public CTLM_Layer Owner
        {
            get { return m_owner; }
        }

        #endregion PUBLIC METHODS


        #region PUBLIC PROPERTIES

        public long TimePos
        {
            get { return m_timepos; }
            set
            {
                if (m_timepos != value)
                {
                    m_timepos = value;
                }
            }
        }

        public Rectangle Bounds
        {
            get { return m_bounds; }
            set
            {
                if (m_bounds != value)
                {
                    m_bounds = value;
                }
            }
        }
        
        public CensorMaskType Type
        {
            get { return m_type; }
            set
            {
                if (m_type != value)
                {
                    m_type = value;
                }
            }
        }

        #endregion PUBLIC PROPERTIES
    }
}
