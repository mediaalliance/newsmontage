﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

using MediaAlliance.AV.MediaInfo;


namespace MediaAlliance.AV
{
    public abstract class Source: Form
    {
        public enum enSourceStatus
        { 
            none,
            Play,
            Pause,
            Stop
        }

        private int m_id = 0;
        private MediaAlliance.AV.VideoPreview.Track track = null;
        private object m_tag = null;

        protected string m_fileName;
        protected bool m_viewInfo           = false;
        public enSourceStatus SourceStatus  = enSourceStatus.none;

        protected Label lblClipPosition     = new Label();
        protected Label lblTrack            = new Label();
        protected Label lblEngine           = new Label();
        protected Label lblFormat           = new Label();

        protected bool m_audioScrubEnabled    = false;

        public MediaInformations MediaInfo = null;


        protected override void OnParentChanged(EventArgs e)
        {
            if (Parent == null)
            {
                if (this is DSPreview)
                {
                    ((DSPreview)this).ChangeMainWindowPreview(IntPtr.Zero);
                }
            }
            else
            {
                if (this is DSPreview)
                {
                    ((DSPreview)this).ChangeMainWindowPreview(Parent.Handle);
                }
            }
            base.OnParentChanged(e);
        }

        public int ID { get { return m_id; } set { m_id = value; } }

        public Source()
        {
            CommonCTor();

            InitializeComponent(false);
        }

        public Source(bool audioScrubEnabled)
        {
            CommonCTor();

            InitializeComponent(audioScrubEnabled);
        }

        private void InitializeComponent(bool audioScrubEnabled)
        {
            m_audioScrubEnabled = audioScrubEnabled;

            lblClipPosition.Location    = new Point(10, 10);
            lblClipPosition.Text        = "00:00:00:00";
            lblClipPosition.AutoSize    = true;
            lblClipPosition.ForeColor   = Color.White;
            lblClipPosition.Parent      = this;
            lblClipPosition.Visible     = m_viewInfo;
            lblClipPosition.BringToFront();

            lblTrack.Location           = new Point(10, lblClipPosition.Bottom + 2);
            lblTrack.Text               = "A";
            lblTrack.AutoSize           = true;
            lblTrack.ForeColor          = Color.White;
            lblTrack.Parent             = this;
            lblTrack.Visible            = m_viewInfo;
            lblTrack.BringToFront();


            lblEngine.Location          = new Point(10, lblTrack.Bottom + 2);
            lblEngine.Text              = (this is DSPreview) ? "DS" : "QT";
            lblEngine.AutoSize          = true;
            lblEngine.ForeColor         = Color.White;
            lblEngine.Parent            = this;
            lblEngine.Visible           = m_viewInfo;
            lblEngine.BringToFront();

            lblFormat.Location          = new Point(10, lblEngine.Bottom + 2);
            lblFormat.Text              = "";
            lblFormat.AutoSize          = true;
            lblFormat.ForeColor         = Color.White;
            lblFormat.Parent            = this;
            lblFormat.Visible           = m_viewInfo;
            lblFormat.BringToFront();    
        }
        
        public string Frame2TC(long frame)
        {
            string result = "";
            long ffTot = frame;

            int FFrameRate = 25;

            int ss = (int)(frame / FFrameRate);
            int ff = (int)(frame - ss * FFrameRate);
            int mm = (int)(ss / 60);
            ss = ss - mm * 60;
            int hh = (int)(mm / 60);
            mm = mm - hh * 60;

            result += hh.ToString().PadLeft(2, '0') + ":";
            result += mm.ToString().PadLeft(2, '0') + ":";
            result += ss.ToString().PadLeft(2, '0') + ":";
            result += ff.ToString().PadLeft(2, '0');

            return result;
        }

        public Source(MediaAlliance.AV.VideoPreview.Track owner_track)
        {
            this.track = owner_track;
            CommonCTor();
        }

        private void CommonCTor()
        { 
            this.Dock            = DockStyle.Fill; 
            this.TopLevel        = false; 
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None; 
            this.Visible         = false;
            this.BackColor       = Color.Black;        
        }

        
        #region PROPERTIES

        virtual public string FileName
        {
            get { return m_fileName; }
        }

        public object Tag
        {
            get { return m_tag; }
            set { m_tag = value; }
        }

        public bool ViewInfo
        {
            get { return m_viewInfo; }
            set 
            { 
                m_viewInfo = value;
                lblClipPosition.Visible = value;
                lblEngine.Visible       = value;
                lblFormat.Visible       = value;
                lblTrack.Visible        = value;

                if (value)
                {
                    lblEngine.BringToFront();
                    lblFormat.BringToFront();
                    lblTrack.BringToFront();
                }
            }
        }

        #endregion PROPERTIES


        #region METHODS

        abstract public bool NewInstance(string filename);

        abstract public void Play();

        abstract public bool PlayMarked(long tcin, long tcout);

        abstract public void PlayScrub();

        abstract public void Stop();

        abstract public void Pause();

        abstract public void Seek(long position);

        abstract public void SetVolume(float db);
        
        abstract public void CloseInstance();

        #endregion METHODS


        public MediaAlliance.AV.VideoPreview.Track Track
        {
            get { return track; }
            set
            {
                track = value;
                
                lblTrack.Text = ((char)(65 + track.ID)).ToString();
            }
        }
    }
}
