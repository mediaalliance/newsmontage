﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MediaAlliance.AV
{
    public partial class QTPreview : Source, IDisposable
    {
        private object m_movieRate = 1.0f;

        private System.Timers.Timer tmrPlayScrub = null;



        public QTPreview()
        {
            InitializeComponent();

            tmrPlayScrub = new System.Timers.Timer(30);
            tmrPlayScrub.Elapsed += new System.Timers.ElapsedEventHandler(tmrPlayScrub_Elapsed);

            this.BackColor = Color.Black;

            this.TopLevel = false;
        }

        public QTPreview(bool audioScrubEnabled)
        {
            this.m_audioScrubEnabled = audioScrubEnabled;

            InitializeComponent();

            tmrPlayScrub = new System.Timers.Timer(30);
            tmrPlayScrub.Elapsed += new System.Timers.ElapsedEventHandler(tmrPlayScrub_Elapsed);

            this.BackColor = Color.Black;

            this.TopLevel = false;
        }


        public void Dispose()
        {
            this.MediaInfo.Dispose();
        }

        void tmrPlayScrub_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            this.tmrPlayScrub.Stop();
            this.Pause();
        }

        public override bool NewInstance(string filename)
        {
            bool done = false;

            this.MediaInfo = new MediaAlliance.AV.MediaInfo.MediaInformations();
            this.MediaInfo.Open(filename);

            string dStr = this.MediaInfo.GetValue(MediaAlliance.AV.MediaInfo.VideoInfoRequest.Duration, 0);
                    

            try
            {
                axQTControl1.QuickTimeInitialize();

                axQTControl1.URL = filename;
    
                if (axQTControl1.Movie != null)
                    done = true;

                m_fileName = filename;

            }catch{}

            return done;
        }

        public override  string FileName { get { return m_fileName; } }

        public override void Play()
        {
            if (SourceStatus == enSourceStatus.Play) return;

            if (axQTControl1.Movie != null)
            {
                axQTControl1.Movie.Play(m_movieRate);
                SourceStatus = enSourceStatus.Play;
            }
        }

        public override bool PlayMarked(long tcin, long tcout)
        {
            throw new NotImplementedException();
            return false;
        }

        public override void PlayScrub()
        {
            if (SourceStatus == enSourceStatus.Play) return;

            if (axQTControl1.Movie != null)
            {
                tmrPlayScrub.Stop();
            
                if(SourceStatus != enSourceStatus.Play)
                    axQTControl1.Movie.Play(m_movieRate);

                SourceStatus = enSourceStatus.Play;

                tmrPlayScrub.Start();
            }
        }

        public override void Stop()
        {
            if (SourceStatus == enSourceStatus.Stop) return;

            if (axQTControl1.Movie != null)
            {
                axQTControl1.Movie.Stop();
                SourceStatus = enSourceStatus.Stop;
            }
        }

        public override void Pause()
        {
            if (SourceStatus == enSourceStatus.Pause) return;

            if (axQTControl1.Movie != null)
            {
                axQTControl1.Movie.Pause();
                SourceStatus = enSourceStatus.Pause;
            }
        }

        public override void Seek(long position)
        {
            if (axQTControl1.Movie != null)
            {
                int newPos = (int)(axQTControl1.Movie.TimeScale * position / axQTControl1.Movie.StaticFrameRate);
                if (newPos <= axQTControl1.Movie.EndTime)
                    axQTControl1.Movie.Time = newPos;
                else axQTControl1.Movie.Time = axQTControl1.Movie.EndTime;
            }
        }

        public override void SetVolume(float db)
        {
            if (axQTControl1.Movie != null)
                axQTControl1.Movie.AudioVolume = db;
        }
               
        public override void CloseInstance()
        {
            if(axQTControl1.Movie != null)
                axQTControl1.Movie.Disconnect();
            
            axQTControl1.URL = "";
            axQTControl1.QuickTimeTerminate();
        }

    }
}
