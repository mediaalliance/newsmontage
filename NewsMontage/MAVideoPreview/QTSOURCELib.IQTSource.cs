﻿
using System;
using System.Runtime.InteropServices;

namespace QTSOURCELib
{
    [Guid("FBB3B8AE-EB10-423B-A3DC-710335B54874")]
    [InterfaceType(1)]
    public interface IQTSource
    {
        void CancelExport();
        void DecodeAllFrames(int _bDecodeAll);
        void EnableBuffering(int _bEnable);
        void ExportMovie(string _szFileName);
        void ExportMovie2(uint _fccContainer, string _bsFileName);
        void ExportMovieSegment(uint _fccContainer, string _bsFileName, double _dblStartSec, double _dblStopSec);
        void ExposeTimecodePin(int _bUse);
        void ForceMultichannelAudio(int _bForce);
        void ForceRGBOutput(int _lForce);
        void GetAudio(out eQTAudioType _peAudioType, out eQTAudioChannels _peAudioChannels);
        void GetAudioStreamInfo(out tagAudioStreamInfo _pAudInfo);
        void GetAudioStreamInfo2(int _nStream, out tagAudioStreamInfo _pAudInfo);
        void GetAudioStreamInfoVB(int _nStream, out AUDIO_STREAM_INFO2 _pAudInfo);
        void GetExportProgress(out int _pbExportNow, out double _pdblPercent);
        void GetLastTimecode(out QT_TIMECODE _pTimecode);
        void GetMovieInfoCount(out int _plCount);
        void GetMovieInfoItem(int lIndex, out string _pbsType, out string _pbsValue);
        void GetVideoStreamInfo(out tagVideoStreamInfo _pVidInfo);
        void GetVideoStreamInfo2(out VIDEO_STREAM_INFO2 _pVidInfo);
        void IsAlphaOutput(out int _pbAlpha);
        void IsBufferingEnabled(out int _pbEnabled);
        void IsDecoderAllFrames(out int _pbDecode);
        void IsExposeTimecodePin(out int _pbUse);
        void IsForceMultichannelAudio(out int _pbForce);
        void IsForceRGBOutput(out int _plForce);
        void IsRawOutput(out int _pbRawOutput);
        void IsRawOutput2(out int _pbRawOutputReal, out int _pbRawOutputSet);
        void IsSeparateAudioTracks(out int _pbSeparate);
        void IsUseNativeSize(out int _pbUse);
        void RawOutput(int _bRawOutput);
        void SaveMovie();
        void SaveMovie2(string _bsFileName);
        void SaveMovieSegment(string _bsFileName, double _dblStartSec, double _dblStopSec);
        void SeparateAudioTracks(int _bSeparate);
        void SetAlphaOutput(int _bAlpha);
        void SetAudio(eQTAudioType _eAudioType, eQTAudioChannels _eAudioChannels);
        void SetProps(string _bsPropName, string _bsPropValue);
        void UseNativeSize(int _bUse);
    }
}
