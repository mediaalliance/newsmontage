﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

using System.Diagnostics;
using System.Drawing;
using MediaAlliance.AV.CensorTools;
using System.Reflection;


namespace MediaAlliance.AV
{
    public class VideoPreview : IDisposable
    {
        private int m_idItem = -1;
        private Control m_controlPreview = null;
        private Source m_selectedSource = null;


        // CENSOR TOOLS
        //private Dictionary<string, CCensorTimeline> m_censorStore = new Dictionary<string, CCensorTimeline>();





        public class Track
        {
            public int ID;

            public List<Source> SourceList = new List<Source>();

            public Track(int _id) { ID = _id; }
        }

        private List<Track> m_listTracks = new List<Track>();

        public int TrackNumber { get { return m_listTracks.Count; } }

        private bool m_useDirectShowOnly = false;
        private bool m_audioScrub = false;
        private bool m_infoVisible = false;


        // CTor

        public VideoPreview(Control controlPreview, bool UseOnlyDS, bool AudioScrub)
        {
            m_controlPreview = controlPreview;
            m_useDirectShowOnly = UseOnlyDS;
            m_audioScrub = AudioScrub;
        }





        void VideoPreview_OnFrame(object sender, 
                                  long Frame, 
                                  System.Drawing.Size frameSize, 
                                  System.Drawing.Graphics OverlayGraphics, 
                                  System.Drawing.Bitmap bmpOnBuffer, 
                                  System.Drawing.Graphics FrameGraphicBuffer)
        {
            //OverlayGraphics.DrawString(Frame.ToString(), Font, Brushes.White, 10, 10);

            if (sender is DSPreview)
            {
                DSPreview preview = (DSPreview)sender;

#if CENSURA
                if (preview.Tag != null)
                {
                    FieldInfo fi = preview.Tag.GetType().GetField("CensorInfo");

                    if (fi != null)
                    {
                        CCensorTimeline censor_timeline = (CCensorTimeline)fi.GetValue(preview.Tag);

                        if (censor_timeline != null)
                        {
                            List<CTLM_Keyframe> censor_masks = censor_timeline.GetKeyAt(Frame);

                            foreach (CTLM_Keyframe mask in censor_masks)
                            {
                                DrawCensorMask(mask, ref OverlayGraphics, ref bmpOnBuffer, frameSize);
                            }
                        }
                    }
                }
#endif

                //if (m_censorStore.ContainsKey(preview.FileName))
                //{
                //    // SE LA CLIP HA UNA TIMELINE DI CENSURA

                //    CCensorTimeline censor_timeline = m_censorStore[preview.FileName];

                //    List<CTLM_Keyframe> censor_masks = censor_timeline.GetKeyAt(Frame);

                //    foreach (CTLM_Keyframe mask in censor_masks)
                //    {
                //        DrawCensorMask(mask, ref OverlayGraphics, ref bmpOnBuffer, frameSize);
                //    }

                //    //int PixSize = 20;

                //    //int x_pos = 50;
                //    //int y_pos = 50;

                //    //Rectangle coverArea = new Rectangle(x_pos, y_pos, 200, 100);

                //    //for (int xs = coverArea.X; xs < coverArea.X + coverArea.Width; xs += PixSize)
                //    //{
                //    //    for (int ys = frameSize.Height - coverArea.Y, yd = coverArea.Y; ys > frameSize.Height - (coverArea.Y + coverArea.Height); ys -= PixSize, yd += PixSize)
                //    //    {
                //    //        Rectangle megapix = new Rectangle(xs, yd, PixSize, PixSize);
                //    //        Color pix_color = bmpOnBuffer.GetPixel(xs + (PixSize / 2), ys + (PixSize / 2));
                //    //        OverlayGraphics.FillRectangle(new SolidBrush(pix_color), megapix);
                //    //    }
                //    //}
                //}
            }
        }

        private void DrawCensorMask(CTLM_Keyframe mask, ref System.Drawing.Graphics OverlayGraphics, ref Bitmap bmpOnBuffer, Size frameSize)
        {
            switch (mask.Type)
            {
                case CensorMaskType.none:
                    break;
                case CensorMaskType.pixelSmall:
                case CensorMaskType.pixelMed:
                case CensorMaskType.pixerBig:
                    int PixSize = 10;

                    switch (mask.Type)
	                {
                        case CensorMaskType.pixelSmall: PixSize = 5; break;
                        case CensorMaskType.pixelMed: PixSize = 15;  break;
                        case CensorMaskType.pixerBig: PixSize = 25;  break;
	                }

                    Rectangle coverArea = new Rectangle(mask.Bounds.X, mask.Bounds.Y, mask.Bounds.Width, mask.Bounds.Height);

                    for (int xs = coverArea.X; xs < coverArea.X + coverArea.Width; xs += PixSize)
                    {
                        for (int ys = frameSize.Height - coverArea.Y, yd = coverArea.Y; ys > frameSize.Height - (coverArea.Y + coverArea.Height); ys -= PixSize, yd += PixSize)
                        {
                            Rectangle megapix = new Rectangle(xs, yd, PixSize, PixSize);

                            int x_pick = xs + (PixSize / 2);
                            int y_pick = ys + (PixSize / 2);

                            if (x_pick > frameSize.Width - 1) x_pick    = frameSize.Width - 1;
                            if (y_pick > frameSize.Height - 1) y_pick   = frameSize.Height - 1;
                            if (x_pick < 0) x_pick = 0;
                            if (y_pick < 0) y_pick = 0;

                            Color pix_color = bmpOnBuffer.GetPixel(x_pick, y_pick);
                            OverlayGraphics.FillRectangle(new SolidBrush(pix_color), megapix);
                        }
                    }
                    break;
                default:
                    break;
            }

            //OverlayGraphics.DrawRectangle(Pens.Yellow, mask.Bounds);
        }




        #region PUBLIC METHODS

        /// <summary>
        /// Aggiunge una sorgente con più layer per ogni traccia della timeline
        /// </summary>
        /// <param name="filename">File source</param>
        /// <param name="numTracks">Numero di tracce desiderato</param>
        /// <param name="Tag">Oggetto generico, usato per contenere anche le CensorInfo</param>
        /// <returns></returns>
        public int Add(string filename, int numTracks, object Tag)
        {
            int clipID = -1;

            //Crea le tracce all'interno della lista
            int actualNumTracks = m_listTracks.Count;
            if (actualNumTracks < numTracks)
            {
                for (int nTrack = 0; nTrack < numTracks - actualNumTracks; nTrack++)
                    m_listTracks.Add(new Track(nTrack));
            }

            //Verifica la presenza del file nelle tracce esistenti.
            foreach (Track track in m_listTracks)
            {
                foreach (Source src in track.SourceList)
                {
                    if (src.FileName.Equals(filename))
                    {
                        clipID = src.ID;
                        break;
                    }
                }

            }

            long clip_duration = 0;

            //Se la clip non esiste crea una nuova istanza per ogni traccia
            if (clipID == -1)
            {
                clipID = ++m_idItem;

                for (int nTrack = 0; nTrack < numTracks; nTrack++)
                {
                    // CREAO DIVERSI OGGETTI PREVIEW PER OGNI TRACCIA IN MODO DA POTER GESTIRE
                    // PLAY CONTEMPORANEI DI PORZIONI DIVERSE DI UNA STESSA CLIP

                    Source sourcePreview = null;
                                        
                    if (Path.GetExtension(filename).ToLower().Contains("mov") && !m_useDirectShowOnly)
                    {
                        sourcePreview = new QTPreview(m_audioScrub);
                    }
                    else
                    {
                        sourcePreview = new DSPreview(m_audioScrub);
                        ((DSPreview)sourcePreview).OnFrame += new DSPreview.DSPreview_OnFrame(VideoPreview_OnFrame);
                    }

                    sourcePreview.Tag = Tag;
                    sourcePreview.ViewInfo = true;
                    sourcePreview.ID = clipID;

                    m_controlPreview.Controls.Add(sourcePreview);
                    sourcePreview.NewInstance(filename);

                    Track track = m_listTracks[nTrack];
                    sourcePreview.Track = track;
                    track.SourceList.Add(sourcePreview);

                    sourcePreview.Stop();

                    clip_duration = ((DSPreview)sourcePreview).Duration;
                }
            }


            if (clip_duration > 0)
            {
                CCensorTimeline censor_track = new CCensorTimeline();

                censor_track.Duration = clip_duration;
                censor_track.Name = filename;

                censor_track.AddLayer(CensorMaskType.pixelMed);
                CTLM_Keyframe kf0 = censor_track[0].AddKeyframe(0);
                CTLM_Keyframe kf1 = censor_track[0].AddKeyframe(clip_duration);
                kf0.Bounds = new Rectangle(300, 150, 150, 100);
                kf1.Bounds = new Rectangle(200, 100, 300, 300);

                //m_censorStore.Add(filename, censor_track);

                string xml = censor_track.Save();

                CCensorTimeline tl = new CCensorTimeline();
                tl.Load(xml);

                string xml2 = tl.Save();
            }

            return clipID;
        }

        public void UnloadAll()
        {
            foreach (Track trk in m_listTracks)
            {
                foreach (Source src in trk.SourceList)
                {
                    src.Close();
                }

                trk.SourceList.Clear();
            }

            m_listTracks.Clear();
        }

        public void SetOnLine(int idTrack, int idSource)
        {
            Track seletectedTrack = SelectTrackById(idTrack);
            if (seletectedTrack != null)
            {
                Source currSource = seletectedTrack.SourceList.Find(delegate(Source src) { return src.ID == idSource; });

                if (currSource != null && m_selectedSource != currSource)
                {
                    currSource.ViewInfo = m_infoVisible;
                    currSource.Visible = true;

                    //Console.WriteLine("Online  > " + seletectedTrack.ID + " : " + currSource.ID);

                    if (m_selectedSource != null)
                    {
                        m_selectedSource.Visible = false;
                        //Console.WriteLine("Offline > " + m_selectedSource.Track.ID + " : " + m_selectedSource.ID);
                    }

                    #if DEBUG
                    StackTrace stackTrace = new StackTrace();
                    StackFrame currunt_method = stackTrace.GetFrames()[0];
                    StackFrame caller_method = stackTrace.GetFrames()[1];

                     Console.WriteLine("Preview : " + currunt_method.GetMethod().Name.PadRight(20) +
                                        "(" + idTrack + ", " + idSource + ") caller : " +
                                        caller_method.GetMethod().Name + " -> " + ((System.Reflection.MethodInfo)caller_method.GetMethod()).DeclaringType.Name +
                                        " Line(" + caller_method.GetFileLineNumber() + ")");
                    #endif

                    m_selectedSource = currSource;

                    Application.DoEvents();
                }
            }
        }
        
        public void SetAllOffline()
        {
            if(m_selectedSource != null)
            {
                m_selectedSource.Visible = false;
                m_selectedSource = null;
            }
        }

        public void Play(int idTrack, int idSource)
        {
            Track seletectedTrack = SelectTrackById(idTrack);
            if (seletectedTrack != null)
            {
                Source currSource = seletectedTrack.SourceList.Find(delegate(Source src) { return src.ID == idSource; });
                if (currSource != null)
                {
                    if (currSource.SourceStatus != Source.enSourceStatus.Play)
                    {
                        #if DEBUG
                        StackTrace stackTrace = new StackTrace();
                        StackFrame currunt_method = stackTrace.GetFrames()[0];
                        StackFrame caller_method = stackTrace.GetFrames()[1];

                        Console.WriteLine("Preview : " + currunt_method.GetMethod().Name.PadRight(20) +
                                            "(" + idTrack + ", " + idSource + ") caller : " +
                                            caller_method.GetMethod().Name + " -> " + ((System.Reflection.MethodInfo)caller_method.GetMethod()).DeclaringType.Name +
                                            " Line(" + caller_method.GetFileLineNumber() + ")");
                        #endif
                    }
                    currSource.Play();
                }
            }
        }

        public void PlayScrub(int idTrack, int idSource)
        {
            Track seletectedTrack = SelectTrackById(idTrack);
            if (seletectedTrack != null)
            {
                Source currSource = seletectedTrack.SourceList.Find(delegate(Source src) { return src.ID == idSource; });
                if (currSource != null)
                {
                    if (currSource.SourceStatus != Source.enSourceStatus.Play)
                    {
                        #if DEBUG
                        StackTrace stackTrace = new StackTrace();
                        StackFrame currunt_method = stackTrace.GetFrames()[0];
                        StackFrame caller_method = stackTrace.GetFrames()[1];

                        Console.WriteLine("Preview : " + currunt_method.GetMethod().Name.PadRight(20) +
                                            "(" + idTrack + ", " + idSource + ") caller : " +
                                            caller_method.GetMethod().Name + " -> " + ((System.Reflection.MethodInfo)caller_method.GetMethod()).DeclaringType.Name +
                                            " Line(" + caller_method.GetFileLineNumber() + ")");
                        #endif
                    }
                    currSource.PlayScrub();
                }
            }
        }

        public void Stop(int idTrack, int idSource)
        {
            Track seletectedTrack = SelectTrackById(idTrack);
            if (seletectedTrack != null)
            {
                Source currSource = seletectedTrack.SourceList.Find(delegate(Source src) { return src.ID == idSource; });
                if (currSource != null)
                    currSource.Stop();
            }
        }

        public void Pause(int idTrack, int idSource)
        {
            Track seletectedTrack = SelectTrackById(idTrack);
            if (seletectedTrack != null)
            {
                Source currSource = seletectedTrack.SourceList.Find(delegate(Source src) { return src.ID == idSource; });
                if (currSource != null)
                {
                    
                    if (currSource.SourceStatus != Source.enSourceStatus.Pause)
                    {
                        #if DEBUG
                        StackTrace stackTrace = new StackTrace();
                        StackFrame currunt_method = stackTrace.GetFrames()[0];
                        StackFrame caller_method = stackTrace.GetFrames()[1];

                        Console.WriteLine("Preview : " + currunt_method.GetMethod().Name.PadRight(20) +
                                            "(" + idTrack + ", " + idSource +") caller : " +
                                            caller_method.GetMethod().Name + " -> " + ((System.Reflection.MethodInfo)caller_method.GetMethod()).DeclaringType.Name +
                                            " Line(" + caller_method.GetFileLineNumber() + ")");
                        #endif
                    }

                    currSource.Pause();
                }
            }
        }

        public void PauseAll()
        {
            foreach (Track trk in m_listTracks)
            {
                foreach (Source src in trk.SourceList)
                {
                    src.Pause();
                }
            }
        }

        public void SetPosition(int idTrack, int idSource, int position)
        {
            Track seletectedTrack = SelectTrackById(idTrack);
            if (seletectedTrack != null)
            {
                Source currSource = seletectedTrack.SourceList.Find(delegate(Source src) { return src.ID == idSource; });
                if (currSource != null)
                {

                    #if DEBUG
                    //StackTrace stackTrace = new StackTrace();
                    //StackFrame currunt_method   = stackTrace.GetFrames()[0];
                    //StackFrame caller_method    = stackTrace.GetFrames()[1];

                    //Console.WriteLine("Preview : " + currunt_method.GetMethod().Name.PadRight(20) +
                    //                    "(" + idTrack + ", " + idSource + ", " + position + ") caller : " + 
                    //                    caller_method.GetMethod().Name + " -> " + ((System.Reflection.MethodInfo)caller_method.GetMethod()).DeclaringType.Name +
                    //                    " Line(" + caller_method.GetFileLineNumber() + ")");
                    #endif

                    currSource.Seek(position);
                }
            }
        }

        public void SetVolume(int idTrack, int idSource, float db)
        {
            Track seletectedTrack = SelectTrackById(idTrack);
            if (seletectedTrack != null)
            {
                Source currSource = seletectedTrack.SourceList.Find(delegate(Source src) { return src.ID == idSource; });
                if (currSource != null)
                {
                     currSource.SetVolume(db);
                }
            }
        }

        private Track SelectTrackById(int idTrack)
        {
            Track seletectedTrack = null;

            //ancora vuota
            if (m_listTracks.Count == 0)
                return seletectedTrack;

            if (idTrack < m_listTracks.Count)
                seletectedTrack = m_listTracks[idTrack];
            else
                seletectedTrack = m_listTracks[0];

            return seletectedTrack;
        }

        public void Dispose()
        {
            foreach (Track trk in m_listTracks)
            {
                foreach (Source src in trk.SourceList)
                {
                    src.Close();
                }
            }
        }

        public bool InfoVisible
        {
            get { return m_infoVisible; }
            set
            {
                if (m_infoVisible != value)
                {
                    m_infoVisible = value;

                    foreach (Track trk in m_listTracks)
                    {
                        foreach (Source src in trk.SourceList)
                        {
                            src.ViewInfo = value;
                        }
                    }
                }
            }
        }

        #endregion PUBLIC METHODS


        //public CCensorTimeline GetCensorTimeline(string filename)
        //{
        //    if (m_censorStore.ContainsKey(filename))
        //    {
        //        return m_censorStore[filename];
        //    }

        //    return null;
        //}

        //public void SetCensorTimeline(CCensorTimeline censorTL, string filename)
        //{
        //    if (m_censorStore.ContainsKey(filename))
        //    {
        //        m_censorStore[filename] = censorTL;
        //    }
        //    else
        //    {
        //        m_censorStore.Add(filename, censorTL);
        //    }
        //}



    }
}
