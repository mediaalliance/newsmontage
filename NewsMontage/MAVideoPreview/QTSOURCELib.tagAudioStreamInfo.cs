﻿#region Assembly Interop.QTSOURCELib.dll, v4.0.30319
// C:\WORKS\PROFESSIONALSHOW\Kapricorn Classi.NET\Transcode.Net\KDvToAvi\KDvToAvi\obj\Debug\Interop.QTSOURCELib.dll
#endregion

using System;

namespace QTSOURCELib
{
    public struct tagAudioStreamInfo
    {
        public int lDuration;
        public int nAvgBytesPerSec;
        public int nBitsPerSample;
        public int nChannels;
        public int nSamplesPerSec;
        public ushort[] oszChannels;
        public ushort[] oszCodecName;
    }
}
