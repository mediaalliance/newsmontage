﻿#region Assembly Interop.QTSOURCELib.dll, v4.0.30319
// C:\WORKS\PROFESSIONALSHOW\Kapricorn Classi.NET\Transcode.Net\KDvToAvi\KDvToAvi\obj\Debug\Interop.QTSOURCELib.dll
#endregion

using System;

namespace QTSOURCELib
{
    public struct AUDIO_STREAM_INFO2
    {
        public string bsChannels;
        public string bsCodecName;
        public int lDuration;
        public int nAvgBytesPerSec;
        public int nBitsPerSample;
        public int nChannels;
        public int nSamplesPerSec;
    }
}
