﻿//#define GRAPHICS

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using DirectShowLib;
using System.Threading;
using System.Runtime.InteropServices;
using System.IO;
using DirectShowLib.Utils;

using MediaAlliance.AV.MediaInfo;
using MediaAlliance.AV.DS;
using System.Reflection;


namespace MediaAlliance.AV
{
    public partial class DSPreview : Source
    {
        // DELEGATES
        public delegate void DSPreview_OnChangePosition(object Sender, long OldPos, long NewPos);
        public delegate void DSPreview_OnChangeState(object Sender, MAPreviewState State);
        public delegate void DSPreview_OnFinish(object Sender);
        public delegate void DSPreview_OnChangedFile(object Sender, string FileName);
        public delegate void DSPreview_OnFrame(object sender, long Frame, Size frameSize, Graphics OverlayGraphics, Bitmap bmpOnBuffer, Graphics FrameGraphicBuffer);



        // EVENTs
        public event DSPreview_OnChangePosition OnChangePosition    = null;
        public event DSPreview_OnFinish OnFinish                    = null;
        public event DSPreview_OnChangeState OnChangeState          = null;
        public event DSPreview_OnChangedFile OnChangedFile          = null;
        public event DSPreview_OnFrame OnFrame                      = null;

        // DS OBJETS
        private IGraphBuilder graph_V           = null;
        private IGraphBuilder graph_A           = null;

        private IMediaControl control_V         = null;
        private IMediaControl control_A         = null;

        private IMediaSeeking seeking_V         = null;
        private IMediaSeeking seeking_A         = null;

        private IVideoWindow win                = null;
        private IBaseFilter VMR9                = null;
        private DsROTEntry rot_V                = null;
        private DsROTEntry rot_A                = null;

#if GRAPHICS
        private DS.CVideoEditorFilter veditor   = null;
#endif

        // THREADS
        private Thread FTimingThread            = null;
        private bool FTimingThread_Stop         = false;
        private ManualResetEvent mreTimeingTh   = new ManualResetEvent(false);

        // VARIABLES
        private long unity                      = 400000;
        private long FDuration                  = 0;
        private bool FFinishRaised              = false;
        private long FCurrentPosition           = 0;
        private IntPtr FOutputWin               = IntPtr.Zero;
        private bool FAutoPauseOnSeek           = true;
        private bool FAutoRewindOnStop          = true;
        private long FMarkIn                    = -1;
        private long FMarkOut                   = -1;
        private bool FLoaded                    = false;
        private bool m_firstseek                = true;
  //      private string filename = "";
        private bool visible                    = false;
        private MAPreviewState m_state          = MAPreviewState.None;
        private bool m_tryUseRogueStream        = false;

        private string m_assemblypath           = "";
        private string m_videogrf               = "";
        private string m_audiogrf               = "";

        private PointF FCurrentMarked           = new PointF(0, 0);

        private System.Timers.Timer tmrPlayScrub    = new System.Timers.Timer(40);      // AUTOTIMER PER STOPPARE IL PLAY DURANTE LO SCRUB
        private System.Timers.Timer tmrResync       = new System.Timers.Timer(80);      // AUTOTIMER PER SINCRONIZZARE PLAY AUDIO/VIDEO



        public int ID = 0;

        // ENUM
        public enum MAPreviewState
        {
            None,
            Playing,
            PlayingMarked,
            Stoped,
            Cued,
            Paused,
            Finished
        }


        [DllImport("user32.dll")]
        static extern bool GetClientRect(IntPtr hWnd, out Rectangle lpRect);



        public DSPreview(bool audioScrubEnabled)
        {
            this.m_audioScrubEnabled = audioScrubEnabled;
            CommonCtor();
        }

        private void CommonCtor()
        {
            InitializeComponent();

            m_assemblypath = GetMyFile();
            TryToLoadCustomSettings();

            tmrPlayScrub.Elapsed += new System.Timers.ElapsedEventHandler(tmrPlayScrub_Elapsed);
            tmrResync.Elapsed += new System.Timers.ElapsedEventHandler(tmrResync_Elapsed);

            this.BackColor = Color.Black;
            this.TopLevel = false;        
        }



        #region PRIVATE METHODS

        public string GetMyFile()
        {
            Assembly[] AssembliesLoaded = AppDomain.CurrentDomain.GetAssemblies();

            foreach (Assembly MyAssembly in AssembliesLoaded)
            {
                List<Type> tipi = new List<Type>();
                tipi.AddRange(MyAssembly.GetTypes());

                if (tipi.Contains(this.GetType()))
                {
                    return MyAssembly.GetName().CodeBase.Replace("file:///", "");
                }
            }

            return "";
        }

        private void tmrPlayScrub_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Console.WriteLine("tmrPlayScrub_Elapsed");
            if (graph_A != null)
            {
                tmrPlayScrub.Stop();
                control_A.Pause();
            }
        }

        private void tmrResync_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            tmrResync.Stop();
            AV_Resync();

            FilterState v_state = FilterState.Paused;
            FilterState a_state = FilterState.Paused;

            int hr_V = -1;
            int hr_A = -1;

            if (graph_V != null) hr_V = control_V.GetState(10, out v_state);
            if (graph_A != null) hr_A = control_A.GetState(10, out a_state);

            if (m_state == MAPreviewState.Playing || m_state == MAPreviewState.PlayingMarked)
            {
                if (hr_V == 0 && v_state != FilterState.Running) control_V.Run();
                if (hr_A == 0 && a_state != FilterState.Running) control_A.Run();
            }

            Console.WriteLine("tmrResync_Elapsed");
        }

        private void TIMEINGTHREAD_START()
        {
            FTimingThread_Stop = false;
            FTimingThread = new Thread(new ThreadStart(FTimingThread_Task));
            FTimingThread.IsBackground = true;
            FTimingThread.Start();
        }

        private void TIMEINGTHREAD_STOP()
        {
            if (FTimingThread != null)
            {
                FTimingThread_Stop = true;
                mreTimeingTh.Set();

                if (!FTimingThread.Join(200))
                {
                    FTimingThread.Abort();
                }

                FTimingThread = null;
                FTimingThread_Stop = false;
            }
        }
                
        private void AV_Resync()
        { 
            if (graph_V != null && graph_A != null)
            {
                long cur_pos = -1;
                seeking_V.GetCurrentPosition(out cur_pos);

                if (cur_pos >= 0)
                {
                    seeking_A.SetPositions(cur_pos, AMSeekingSeekingFlags.AbsolutePositioning, 0, AMSeekingSeekingFlags.NoPositioning);
                }
            }
        }

        private void TryToLoadCustomSettings()
        {
            if (m_assemblypath.Trim() != "" && File.Exists(m_assemblypath))
            {
                string settings_file = m_assemblypath + ".ini";

                if (File.Exists(settings_file))
                {
                    StreamReader sr = new StreamReader(settings_file);

                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();

                        if (!line.Trim().StartsWith(";") && !line.Trim().StartsWith("#") && line.IndexOf("=") > 0)
                        { 
                            string[] parts = line.Trim().Split(new char[]{'='}, StringSplitOptions.RemoveEmptyEntries);

                            if(parts.Length == 2)
                            {
                                if(parts[0].Trim().Equals("m_tryUseRogueStream", StringComparison.OrdinalIgnoreCase))
                                {
                                    bool val = false;
                                    bool.TryParse(parts[1].Trim(), out val);
                                    m_tryUseRogueStream = val;
                                }
                                else if(parts[0].Trim().Equals("m_videogrf", StringComparison.OrdinalIgnoreCase))
                                {
                                    m_videogrf = parts[1].Trim();
                                    
                                    if(!Path.IsPathRooted(m_videogrf))
                                    {
                                        m_videogrf = Path.Combine(new FileInfo(m_assemblypath).Directory.FullName, m_videogrf);
                                    }

                                    if(!Directory.Exists(new FileInfo(m_videogrf).Directory.FullName))
                                    {
                                        try
                                        {
                                            Directory.CreateDirectory(new FileInfo(m_videogrf).Directory.FullName);
                                        }
                                        catch
                                        {
                                            m_videogrf = "";
                                        }
                                    }
                                }
                                else if(parts[0].Trim().Equals("m_audiogrf", StringComparison.OrdinalIgnoreCase))
                                {
                                    m_audiogrf = parts[1].Trim();
                                    
                                    if(!Path.IsPathRooted(m_audiogrf))
                                    {
                                        m_audiogrf = Path.Combine(new FileInfo(m_assemblypath).Directory.FullName, m_audiogrf);
                                    }

                                    if(!Directory.Exists(new FileInfo(m_audiogrf).Directory.FullName))
                                    {
                                        try
                                        {
                                            Directory.CreateDirectory(new FileInfo(m_audiogrf).Directory.FullName);
                                        }
                                        catch
                                        {
                                            m_audiogrf = "";
                                        }
                                    }
                                }
                            }
                        }
                    }

                    sr.Close();
                    sr.Dispose();
                }
            }
        }
        
        #endregion PRIVATE METHODS
        

        #region PUBLIC METHODS

        //public bool PlayMarked(long tcin, long tcout)
        //{
        //    if (tcin >= 0 && tcout <= Duration && tcin < tcout)
        //    {
        //        Seek(tcin);

        //        FCurrentMarked.X = tcin;
        //        FCurrentMarked.Y = tcout;

        //        control.Run();
        //        FState = MAPreviewState.PlayingMarked;

        //        return true;
        //    }
        //    else
        //    {
        //        FCurrentMarked.X = 0;
        //        FCurrentMarked.Y = 0;
        //        return false;
        //    }
        //}


        //public void GotoStart()
        //{
        //    //System.Diagnostics.Debug.Write("MAPreview : GotoStart()");
        //    Seeking.SetPositions(FMarkIn * unity, AMSeekingSeekingFlags.AbsolutePositioning, 0, AMSeekingSeekingFlags.NoPositioning);
        //}

        #endregion PUBLIC METHODS


        #region OVERRIDE METHODS


        private MADsFilter AddSource(MADsGraphBuilder graph, string filename, bool tryRogueStream)
        {
            IBaseFilter Source = null;

            if (Path.GetExtension(filename).Equals(".mov", StringComparison.OrdinalIgnoreCase) && tryRogueStream)
            {
                #region ROGUESTREAM QTSource

                // HKEY_LOCAL_MACHINE\SOFTWARE\\Rogue Stream\\QuickTime Source Filter\quality
                // HKEY_LOCAL_MACHINE\SOFTWARE\\Rogue Stream\\QuickTime Source Filter\fieldmode

                try
                {
                    Source = FilterGraphTools.AddFilterFromClsid(graph_V, CLSID.RogueStreamQTSource, "RS_QTSource");

                    IFileSourceFilter s = (IFileSourceFilter)Source;
                    s.Load(filename, null);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.Write("    > MEDIALOOKS QTSource Catch : " + ex.Message);
                }

                #endregion ROGUESTREAM QTSource
            }

            if (Source == null && Path.GetExtension(filename).Equals(".mov", StringComparison.OrdinalIgnoreCase))
            {
                #region MEDIALOOKS QTSource

                try
                {
                    Source = FilterGraphTools.AddFilterFromClsid(graph, CLSID.MediaLooks_QTSource, "ML_QTSource");

                    IFileSourceFilter s = (IFileSourceFilter)Source;
                    s.Load(filename, null);

                    if (m_audioScrubEnabled)
                    {
                        IFileSourceFilter s_A = (IFileSourceFilter)Source;
                        s_A.Load(filename, null);
                    }


                    // RECUPERO IL VALORE CHE INDICA SE USA IL DEOCODER INTERNO GLI STREAM DV

                    QTSOURCELib.IQTSource qt = (QTSOURCELib.IQTSource)Source;
                    int r = -1;
                    qt.IsRawOutput(out r);

                    if (r <= 0)
                    {
                        // FORZO DI PASSARE LO STREAM RAW NON DECODIFICATO

                        qt.RawOutput(1);
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.Write("    > MEDIALOOKS QTSource Catch : " + ex.Message);
                }

                #endregion MEDIALOOKS QTSource
            }

            if (Source == null)
            {
                #region SOURCE AUTOMATICO

                try
                {
                    // TEST CON SOURCE AUTOMATICO
                    graph.AddSourceFilter(filename, "video", out Source);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.Write("    > AUTOMATIC SOURCE Catch : " + ex.Message);
                }

                #endregion SOURCE AUTOMATICO
            }

            if (Source == null)
            {
                #region FILESOURCEASYNC

                try
                {
                    Source = FilterGraphTools.AddFilterFromClsid(graph, new Guid("{E436EBB5-524F-11CE-9F53-0020AF0BA770}"), "Source");

                    IFileSourceFilter s = (IFileSourceFilter)Source;
                    s.Load(filename, null);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.Write("    > FILE SOURCE ASYNC Catch : " + ex.Message);
                }

                #endregion FILESOURCEASYNC
            }

            if (Source != null) return new MADsFilter(Source);
            return null;
        }

        public override bool NewInstance(string file)
        {
            int hr = -1;

            FLoaded = false;
            //CloseInstance();

            FMarkIn = 0;
            FMarkOut = 0;

            if (File.Exists(file))
            {
                m_fileName = file;

                this.MediaInfo = new MediaAlliance.AV.MediaInfo.MediaInformations();
                this.MediaInfo.Open(file);


                FOutputWin = Handle;


                if (MediaInfo.VideoCount > 0)
                {
                    // CLIP AUDIO-VIDEO

                    graph_V = (IGraphBuilder)new DirectShowLib.FilterGraph();
                    MADsGraphBuilder Wgraph_1 = new MADsGraphBuilder(graph_V);

                    MADsFilter WSource_1 = AddSource(Wgraph_1, file, m_tryUseRogueStream);
                    


                    #region RENDERIZZO E TENGO SOLO LA PARTE VIDEO

                    if (WSource_1 != null)
                    {
                        foreach (MADsPin out_pin in WSource_1.OutputPin)
                        {
                            if (out_pin.MajorType == PinMajorType.Video)
                            {
                                // AGGIUNGO VMR9
                                VMR9 = FilterGraphTools.AddFilterFromClsid(graph_V, new Guid("{51B4ABF3-748F-4E3B-A276-C828330E926A}"), "VMR9");

#if GRAPHICS
                                // AGGIUNGO GRAPHICS EDITOR
                                veditor = new DS.CVideoEditorFilter(graph_V);
                                veditor.OnFrame += new DS.CVideoEditorFilter.VEDITOR_OnFrame(veditor_OnFrame);
                                veditor.Init();
#endif

                                out_pin.Render();
                            }
                            else if (out_pin.MajorType == PinMajorType.Stream)
                            {
                                out_pin.Render();

                                MADsFilter next_filter = out_pin.ConnectedFilter;

                                if (next_filter != null)
                                {                                    
                                    Wgraph_1.ReomoveAllFilterButThis(WSource_1, next_filter);

                                    WSource_1.ConnectTo(next_filter, 0, 0);

                                    foreach (MADsPin out_pin2 in next_filter.OutputPin)
                                    {
                                        if (out_pin2.MajorType == PinMajorType.Video)
                                        {
                                            // AGGIUNGO VMR9
                                            VMR9 = FilterGraphTools.AddFilterFromClsid(graph_V, new Guid("{51B4ABF3-748F-4E3B-A276-C828330E926A}"), "VMR9");

#if GRAPHICS
                                            // AGGIUNGO GRAPHICS EDITOR
                                            veditor = new DS.CVideoEditorFilter(graph_V);
                                            veditor.OnFrame += new DS.CVideoEditorFilter.VEDITOR_OnFrame(veditor_OnFrame);
                                            veditor.Init();
#endif

                                            out_pin2.Render();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    
                    #endregion RENDERIZZO E TENGO SOLO LA PARTE VIDEO

                    control_V   = (IMediaControl)graph_V;
                    seeking_V   = (IMediaSeeking)control_V;
                    win         = (IVideoWindow)graph_V;

                }
                
                if(MediaInfo.AudioCount > 0)
                {
                    // CLIP AUDIO

                    graph_A = (IGraphBuilder)new DirectShowLib.FilterGraph();
                    MADsGraphBuilder Wgraph_2 = new MADsGraphBuilder(graph_A);

                    MADsFilter WSource_2 = AddSource(Wgraph_2, file, m_tryUseRogueStream);
                    
                    #region RENDERIZZO E TENGO SOLO LA PARTE AUDIO

                    if (WSource_2 != null)
                    {
                        foreach (MADsPin out_pin in WSource_2.OutputPin)
                        {
                            //Wgraph_2.SaveGraph("d:\\ttt.grf");
                            if (out_pin.MajorType == PinMajorType.Audio)
                            {
                                out_pin.Render();
                            }
                            else if (out_pin.MajorType == PinMajorType.Stream)
                            {
                                out_pin.Render();

                                MADsFilter next_filter = out_pin.ConnectedFilter;

                                if (next_filter != null)
                                {
                                    Wgraph_2.ReomoveAllFilterButThis(WSource_2, next_filter);

                                    WSource_2.ConnectTo(next_filter, 0, 0);

                                    foreach (MADsPin out_pin2 in next_filter.OutputPin)
                                    {
                                        if (out_pin2.MajorType == PinMajorType.Audio)
                                        {
                                            out_pin2.Render();
                                            break;
                                        }
                                    }
                                }
                            }
                            //Wgraph_2.SaveGraph("d:\\ttt2.grf");
                        }
                    }

                    #endregion RENDERIZZO E TENGO SOLO LA PARTE AUDIO

                    control_A   = (IMediaControl)graph_A;
                    seeking_A   = (IMediaSeeking)control_A;
                }







                //List<IPin> listaPin_A = getPin(Source_A, PinDirection.Output);

                
                Refresh_Duration();                                 // AGGIORNO LA DURATA TOTALE DEL FILE


                ChangeMainWindowPreview(this.Handle);
                if(win != null) win.put_Owner(this.Handle);


                if (graph_V != null)
                {
                    //if (graph_V != null) rot_V = new DsROTEntry(graph_V);
                    hr = control_V.Run();
                    control_V.Pause();

                    //FilterGraphTools.SaveGraphFile(graph_V, "d:\\ppp.grf");

#if GRAPHICS
                    veditor.UpdateSize();
#endif
                }

                if (graph_A != null)
                {
                    //if (graph_A != null) rot_A = new DsROTEntry(graph_A);   
                    hr = control_A.Run();
                    control_A.Pause();
                }

                if (m_videogrf != "")       // PARAMETRO OPZIONALE PASSATO DAL FILE "MAVideoPreview.dll.ini"
                {
                    try
                    {
                        if (graph_V != null) FilterGraphTools.SaveGraphFile(graph_V, m_videogrf);
                    }
                    catch {}
                }

                if (m_audiogrf != "")       // PARAMETRO OPZIONALE PASSATO DAL FILE "MAVideoPreview.dll.ini"
                {
                    try
                    {
                        if (graph_A != null) FilterGraphTools.SaveGraphFile(graph_A, m_audiogrf);
                    }
                    catch {}
                }


                //if (VMR9 != null)
                //{
                //    ((IVMRMixerControl9)VMR9).SetAlpha(0, 0.5F);
                //}


                if (Handle != IntPtr.Zero)
                {
                    m_state = MAPreviewState.Cued;
                    if (OnChangeState != null) OnChangeState(this, m_state);

                    if (OnChangedFile != null) OnChangedFile(this, file);

                    Seek(0);                                // VADO ALL'INIZIO
                    Pause();           // SE RICHIESTO METTO IN PAUSE
                }

                TIMEINGTHREAD_START();

                FLoaded = true;

                return (hr >= 0);
            }

            return false;
        }



        void veditor_OnFrame(object sender, long Frame, Size frameSize, Graphics OverlayGraphics, Bitmap bmpOnBuffer, Graphics FrameGraphicBuffer)
        {
            if (OnFrame != null)
            {
                OnFrame(this, Frame, frameSize, OverlayGraphics, bmpOnBuffer, FrameGraphicBuffer);
            }

            //OverlayGraphics.DrawString(Frame.ToString(), Font, Brushes.White, 10, 10);

            //int PixSize = 20;

            //int x_pos = 50;
            //int y_pos = 50;

            //Rectangle coverArea = new Rectangle(x_pos, y_pos, 200, 100);

            //for (int xs = coverArea.X; xs < coverArea.X + coverArea.Width; xs += PixSize)
            //{
            //    for (int ys = frameSize.Height - coverArea.Y, yd = coverArea.Y; ys > frameSize.Height - (coverArea.Y + coverArea.Height); ys -= PixSize, yd += PixSize)
            //    {
            //        Rectangle megapix = new Rectangle(xs, yd, PixSize, PixSize);
            //        Color pix_color = bmpOnBuffer.GetPixel(xs + (PixSize / 2), ys + (PixSize / 2));
            //        OverlayGraphics.FillRectangle(new SolidBrush(pix_color), megapix);
            //    }
            //}

        }



        public override void Play()
        {
            FMarkIn     = -1;
            FMarkOut    = -1;

            int hr      = -1;
            
            if (graph_V != null || graph_A != null)
            {
                if (graph_V != null) hr = control_V.Run();
                if (graph_A != null)
                {
                    if (m_state != MAPreviewState.Playing)
                    {
                        hr = control_A.Run();
                        tmrResync.Stop();

                        AV_Resync();

                        tmrResync.Start();
                    }
                }

                m_state = MAPreviewState.Playing;
                if (OnChangeState != null) OnChangeState(this, m_state);
            }
        }

        public override bool PlayMarked(long tcin, long tcout)
        {
            if (graph_V != null || graph_A != null)
            {
                FMarkIn = -1;
                FMarkOut = -1;

                if (tcin >= 0 && tcout <= FDuration && tcin < tcout)
                {
                    Seek(tcin);

                    FMarkIn = tcin;
                    FMarkOut = tcout;

                    int hr = -1;


                    mreTimeingTh.Set();

                    if (graph_V != null) hr = control_V.Run();
                    if (graph_A != null) hr = control_A.Run();

                    m_state = MAPreviewState.PlayingMarked;

                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public override void PlayScrub()
        {
            if (!m_audioScrubEnabled) return;

            int hr = -1;

            if (graph_A != null)
            {
                tmrPlayScrub.Stop();

                hr = control_A.Run();

                tmrPlayScrub.Start();
            }
        }

        public override void Stop()
        {
            tmrResync.Stop();

            if (graph_V != null || graph_A != null)
            {
                if (graph_V != null) control_V.Stop();
                if (graph_A != null) control_A.Stop();

                if (FAutoRewindOnStop)
                {
                    if (graph_V != null)
                    {
                        seeking_V.SetPositions(FMarkIn * unity, AMSeekingSeekingFlags.AbsolutePositioning, 0, AMSeekingSeekingFlags.NoPositioning);
                    }

                    if (graph_A != null)
                    {
                        seeking_A.SetPositions(FMarkIn * unity, AMSeekingSeekingFlags.AbsolutePositioning, 0, AMSeekingSeekingFlags.NoPositioning);
                    }
                }

                m_state = MAPreviewState.Stoped;
                if (OnChangeState != null) OnChangeState(this, m_state);
            }
        }

        public override void Pause()
        {
            FMarkIn     = -1;
            FMarkOut    = -1;

            tmrResync.Stop();

            //System.Diagnostics.Debug.Write("MAPreview : Pause()");
            if (graph_V != null || graph_A != null)
            {
                if (graph_V != null) control_V.Pause();
                if (graph_A != null) control_A.Pause();

                m_state = MAPreviewState.Paused;
                if (OnChangeState != null) OnChangeState(this, m_state);
            }
        }

        public override void Seek(long frame)
        {
            #region FIX MOV

            // I FILE MOVE NON ESCONO CON IL FRAME RICHIESTA DA UN SEEK FINO A CHE 
            // NON SONO STATI MESSI IN PLAY ALMENO UNA VOLTA; IN QUESTO MODO ESEGUO
            // UN PLAY-PAUSE SOLO ALLA PRIMA RICHIESTA DI SEEK SUPERIORE A ZERO

            if (m_firstseek && graph_V != null)
            {
                control_V.Run();
                control_V.Pause();
            }

            if(frame > 0) m_firstseek = false;

            #endregion FIX MOV

            long CFrameMs = 0, CFrame = 0;

            try
            {
                if (seeking_V != null)
                {
                    seeking_V.GetCurrentPosition(out CFrameMs);          // RECUPERO LA POSIZIONE DA DIRECTSHOW
                }
                else if (seeking_A != null)
                {
                    seeking_A.GetCurrentPosition(out CFrameMs);          // RECUPERO LA POSIZIONE DA DIRECTSHOW
                }
                CFrame = CFrameMs / unity;
                if (Math.Abs(CFrame - frame) <= 1)  return;
            }
            catch { }

            long HoldPos = FCurrentPosition;

            

            if (graph_V != null || graph_A != null)
            {
                if (graph_V != null) seeking_V.SetPositions(frame * unity, AMSeekingSeekingFlags.AbsolutePositioning, 0, AMSeekingSeekingFlags.NoPositioning);
                if (graph_A != null) seeking_A.SetPositions(frame * unity, AMSeekingSeekingFlags.AbsolutePositioning, 0, AMSeekingSeekingFlags.NoPositioning);
                
                if (OnChangePosition != null)
                {
                    OnChangePosition(this, HoldPos, frame);
                }
            }

            if (this.lblClipPosition.Visible)
            {
                this.lblClipPosition.Text = Frame2TC(frame) + " / " + Frame2TC(FDuration);
            }
        }

        public override void SetVolume(float db)
        {
            if (graph_A != null)
            {
                //db arriva da 0 a 1
                float internalDb = 0.1F + (db * (1.0f - 0.1f)); // (int)((db - 1.0f) * 10000.0f);

                float outvalue = (float)(4000.0f * Math.Log10((double)internalDb));

                IBasicAudio audio = (IBasicAudio)graph_A;

                int hr = audio.put_Volume((int)((outvalue < -3998f) ? -10000 : outvalue));
            }
        }
           
        public override void CloseInstance()
        {
            try
            {
                System.Diagnostics.Debug.Write("MAPreview : Clear()");

                this.Stop();

                if (rot_V != null) rot_V.Dispose();
                if (rot_A != null) rot_A.Dispose();

                rot_V = null;
                rot_A = null;

                if (FTimingThread != null)
                {
                    FTimingThread_Stop = true;
                    if (!FTimingThread.Join(100))
                    {
                        FTimingThread.Abort();
                    }
                    FTimingThread = null;
                }

                FCurrentPosition    = 0;
                FDuration           = 0;
                FMarkIn             = 0;
                FMarkOut            = 0;
                FOutputWin          = IntPtr.Zero;


                try
                {
                    if(win != null) win.put_Owner(IntPtr.Zero);        
                }
                catch { }

                try
                {
                    if(win != null) win.put_Visible(OABool.False);        
                }
                catch { }

                try
                {
                    if(win != null) win.put_MessageDrain(IntPtr.Zero);                     
                }
                catch { }


                // Release and zero DirectShow interfaces

                if (seeking_V != null) seeking_V = null;
                if (seeking_A != null) seeking_A = null;

                if (control_V != null) control_V = null;
                if (control_A != null) control_A = null;

                if (this.win != null) this.win = null;
                  
                if (graph_V != null)
                {
                    graph_V.Abort();

                    Marshal.ReleaseComObject(graph_V); 
                    graph_V = null;
                }

                if (graph_A != null)
                {
                    graph_A.Abort();

                    Marshal.ReleaseComObject(graph_A); 
                    graph_A = null;
                }


                GC.Collect();

                // -----------------------------------------------------------------------------------
                /* DISTRUGGI ELIMINA ERASA CANCELLA FINALIZZA SPEZZA FRAMMENTA TERMINA  il builder */

                //System.Diagnostics.Debug.Write("    > Going to remove pin");
                //FilterGraphTools.DisconnectAllPins(graph);
                //FilterGraphTools.RemoveAllFilters(graph);
                //Marshal.ReleaseComObject(graph);
                //Marshal.FinalReleaseComObject(graph);

                //GC.Collect();
                //GC.WaitForPendingFinalizers();

                //graph_1 = null;
                // -----------------------------------------------------------------------------------
                

                m_state = MAPreviewState.None;
                if (OnChangeState != null) OnChangeState(this, m_state);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write("KavTools->Clear() Error : " + ex.Message);
            }
        }

        #endregion
        

        #region PUBLIC PROPERTIES

        //public long Duration
        //{
        //    get
        //    {
        //        if (FMarkIn == 0 && FMarkOut == 0) return FDuration;
        //        return FMarkOut - FMarkIn;
        //    }
        //}

        //public long FileDuration
        //{
        //    get
        //    {
        //        return FDuration;
        //    }
        //}

        //public string Filename
        //{
        //    get { return filename; }
        //}

        public void ChangeMainWindowPreview(IntPtr handle)
        {
            if (graph_V != null)
            {
                System.Drawing.Rectangle rect;
                GetClientRect(handle, out rect);
                win.put_WindowStyle(DirectShowLib.WindowStyle.Child | DirectShowLib.WindowStyle.ClipChildren | DirectShowLib.WindowStyle.ClipSiblings);
                win.SetWindowPosition(0, 0, rect.Right, rect.Bottom);
            }
        }

        public bool Visible
        {
            get { return visible; }
            set
            {
                visible = value;

                IVideoWindow win = (IVideoWindow)graph_V;

                if (value)
                {
                    win.put_Visible(OABool.True);
                }
                else
                {
                    win.put_Visible(OABool.False);
                }
            }
        }

        public bool AutoPauseOnSeek
        {
            get { return FAutoPauseOnSeek; }
            set { FAutoPauseOnSeek = value; }
        }

        public bool AutoRewindOnStop
        {
            get { return FAutoRewindOnStop; }
            set { FAutoRewindOnStop = value; }
        }

        public MAPreviewState State
        {
            get { return m_state; }
        }

        public void SetAudio(float db)
        {
            if (db > 0) db = 0;
            if (db < -100) db = -100;

            IBasicAudio audio = (IBasicAudio)graph_A;

            audio.put_Volume((int)db * 100);

            //IVMRFilterConfig vmr = (IVMRFilterConfig)graph;
        }

        public long Duration
        {
            get { return FDuration; }
        }

        #endregion PUBLIC PROPERTIES



        private bool IsInsideLimit(long Frame)
        {
            if (FMarkIn == 0 && FMarkOut == 0)
            {
                return (Frame > 0 && Frame <= FDuration);
            }
            else
            {
                return (Frame > FMarkIn && Frame <= FDuration);
            }
        }

        private void Refresh_Duration()
        {
            try
            {
                string vs_str = this.MediaInfo.GetValue(AV.MediaInfo.GeneralInfoRequest.VideoCount);

                int video_stream = (vs_str.Trim() != "")? Convert.ToInt32(vs_str) : 0;

                if (video_stream > 0)
                {
                    string scanorder    = this.MediaInfo.GetValue(VideoInfoRequest.ScanOrder, 0);
                    string scantype     = this.MediaInfo.GetValue(VideoInfoRequest.ScanType, 0);
                    string resolution   = this.MediaInfo.GetValue(VideoInfoRequest.Width, 0) + "x" + this.MediaInfo.GetValue(VideoInfoRequest.Height, 0);
                    string aRatio       = this.MediaInfo.GetValue(VideoInfoRequest.DisplayAspectRatio_str, 0);

                    lblFormat.Text = resolution + " " + aRatio + " " + scantype.Substring(0, 1).ToUpper() + " - " + scanorder;

                    string str_duration = this.MediaInfo.GetValue(AV.MediaInfo.VideoInfoRequest.Duration, 0);
                    lblEngine.Text += " > " + this.MediaInfo.GetValue(AV.MediaInfo.VideoInfoRequest.CodecID, 0);
                    FDuration = Convert.ToInt32(str_duration) / 40;
                }
                else
                {
                    string str_duration = this.MediaInfo.GetValue(AV.MediaInfo.GeneralInfoRequest.Duration);
                    FDuration = Convert.ToInt32(str_duration) / 40;
                }
            }
            catch
            {
                long Frame = 0;

                if (graph_V != null)
                {
                    seeking_V.GetDuration(out Frame);
                    FDuration = Frame / unity;
                }
                else if (graph_V != null)
                {
                    seeking_A.GetDuration(out Frame);
                    FDuration = Frame / unity;
                }
            }
        }

        private void FTimingThread_Task()
        {
            while (!FTimingThread_Stop)
            {
                if (graph_V != null)
                {
                    if (FMarkIn < 0 || FMarkOut < 0)
                    {
                        mreTimeingTh.WaitOne();
                        mreTimeingTh.Reset();
                    }

                    long Frame = 0;
                    seeking_V.GetCurrentPosition(out Frame);          // RECUPERO LA POSIZIONE DA DIRECTSHOW
                    Frame = Frame / unity;

                    
                    if (Frame >= FMarkOut)
                    {
                        this.Invoke((MethodInvoker)delegate { Pause(); });
                    }

                    FCurrentPosition = Frame;
                }
                else if (graph_A != null)
                {
                    if (FMarkIn < 0 || FMarkOut < 0)
                    {
                        mreTimeingTh.WaitOne();
                        mreTimeingTh.Reset();
                    }

                    long Frame = 0;
                    seeking_A.GetCurrentPosition(out Frame);          // RECUPERO LA POSIZIONE DA DIRECTSHOW
                    Frame = Frame / unity;


                    if (Frame >= FMarkOut)
                    {
                        this.Invoke((MethodInvoker)delegate { Pause(); });
                    }

                    FCurrentPosition = Frame;
                }

                Thread.Sleep(40);
            }
        }


        protected override void OnResize(EventArgs e)
        {
            ChangeMainWindowPreview(this.Handle);
            base.OnResize(e);
        }

        private void Raise_OnChangeState(MAPreviewState state)
        {
            if (OnChangeState != null)
            {
                foreach (DSPreview_OnChangeState singleCast in OnChangeState.GetInvocationList())
                {
                    ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;
                    try
                    {
                        if (syncInvoke != null && syncInvoke.InvokeRequired)
                        {
                            // Invokie the event on the main thread
                            syncInvoke.Invoke(OnChangePosition, new object[] { this, state });
                        }
                        else
                        {
                            // Raise the event
                            OnChangeState(this, state);
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }
    }


}
