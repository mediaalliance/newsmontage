﻿#region Assembly Interop.QTSOURCELib.dll, v4.0.30319
// C:\WORKS\PROFESSIONALSHOW\Kapricorn Classi.NET\Transcode.Net\KDvToAvi\KDvToAvi\obj\Debug\Interop.QTSOURCELib.dll
#endregion

using System;

namespace QTSOURCELib
{
    public enum eQTAudioChannels
    {
        eQTAC_Auto = 0,
        eQTAC_Mono = 1,
        eQTAC_Stereo = 2,
        eQTAC_3_0 = 3,
        eQTAC_3_1 = 4,
        eQTAC_4_0 = 5,
        eQTAC_5_0 = 6,
        eQTAC_5_1 = 7,
        eQTAC_6_1 = 8,
        eQTAC_7_1 = 9,
        eQTAC_MultiInteger = 10,
        eQTAC_MultiFloat = 11,
    }
}
