using System;

namespace MediaAlliance.AV
{
    partial class VoiceRecorderUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            try
            {
                if (m_liveVolumeThread != null)
                {
                    m_liveVolumeThread.Abort();
                    m_liveVolumeThread.Join();
                    m_liveVolumeThread = null;
                }
            }
            catch { }

            try
            {
                if (m_buffer != null)
                {
                    m_buffer.Stop();
                    m_buffer.Dispose();
                    m_buffer = null;
                }
            }
            catch { }

            try
            {
                if (m_audioVoiceRecorder != null)
                {
                    m_audioVoiceRecorder.StopRecord();
                    m_audioVoiceRecorder.Dispose();
                }
            }
            catch { }

          
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.aquaTheme1 = new Telerik.WinControls.Themes.AquaTheme();
            this.vuMeterR = new MediaAlliance.AV.VolumeLevelUC();
            this.vuMeterL = new MediaAlliance.AV.VolumeLevelUC();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // vuMeterR
            // 
            this.vuMeterR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vuMeterR.Location = new System.Drawing.Point(68, 3);
            this.vuMeterR.Maximum = 200;
            this.vuMeterR.Minimum = 0;
            this.vuMeterR.Name = "vuMeterR";
            this.vuMeterR.Size = new System.Drawing.Size(59, 136);
            this.vuMeterR.TabIndex = 10;
            this.vuMeterR.Value = 0;
            // 
            // vuMeterL
            // 
            this.vuMeterL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vuMeterL.Location = new System.Drawing.Point(3, 3);
            this.vuMeterL.Maximum = 200;
            this.vuMeterL.Minimum = 0;
            this.vuMeterL.Name = "vuMeterL";
            this.vuMeterL.Size = new System.Drawing.Size(59, 136);
            this.vuMeterL.TabIndex = 9;
            this.vuMeterL.Value = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.vuMeterL, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.vuMeterR, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(130, 142);
            this.tableLayoutPanel1.TabIndex = 11;
            // 
            // VoiceRecorderUC
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.DimGray;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.tableLayoutPanel1);
            this.DoubleBuffered = true;
            this.Name = "VoiceRecorderUC";
            this.Size = new System.Drawing.Size(130, 142);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.Themes.AquaTheme aquaTheme1;
        private VolumeLevelUC vuMeterL;
        private VolumeLevelUC vuMeterR;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}
