using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Specialized;


using Microsoft.DirectX.DirectSound;


namespace MediaAlliance.AV
{
	public class AudioDevice: IDisposable
	{
		public BufferPositionNotify[] PositionNotify = new BufferPositionNotify[NumberRecordNotifications + 1];  
		public const int NumberRecordNotifications	= 16;
		public AutoResetEvent NotificationEvent	= null;
		public CaptureBuffer applicationBuffer = null;
		public Guid CaptureDeviceGuid = Guid.Empty;
		public Capture applicationDevice = null;
		public string FileName = string.Empty;
		public Notify applicationNotify = null;
		private Thread NotifyThread = null;
		private FileStream WaveFile = null;
		private BinaryWriter Writer = null;
		private string Path = string.Empty;
		public int CaptureBufferSize = 0;
		public int NextCaptureOffset = 0;
		private bool Recording = false;
		public WaveFormat InputFormat;
		private int SampleCount = 0;
		public int NotifySize = 0;
		private bool Capturing = false;
		public bool IsStartingRecord = false;

        public class DeviceInfo:Object 
        {
            public int ID;
            public string Name;
            public Guid Driver;

            public DeviceInfo(int _id, string _name, Guid driverGuid)
            {
                ID = _id;
                Name = _name;
                Driver = driverGuid;
            }

            public override string ToString()
            {
                return Name;
            }

        }

        public delegate void DelegateOnGetLevel(Byte[] data);
        public DelegateOnGetLevel OnGetLevel = null;

		private struct FormatInfo
		{
			public WaveFormat format;
			public override string ToString()
			{
				return ConvertWaveFormatToString(format);
			}
		};

		private static string ConvertWaveFormatToString(WaveFormat format)
		{
			//-----------------------------------------------------------------------------
			// Name: ConvertWaveFormatToString()
			// Desc: Converts a wave format to a text string
			//-----------------------------------------------------------------------------
			return format.SamplesPerSecond + " Hz, " + 
				format.BitsPerSample + "-bit " + 
				((format.Channels == 1) ? "Mono" : "Stereo");
		}

        public static DeviceInfo[] GetDevicesList()
        {
            CaptureDevicesCollection devices = new CaptureDevicesCollection();
            
            DeviceInfo[] DeviceInfoList = new DeviceInfo[devices.Count];
            for(int id=0; id<devices.Count; id++)
            {
                DeviceInfoList[id] = new DeviceInfo(id, devices[id].Description, devices[id].DriverGuid);
            }

            return DeviceInfoList;
        }

        public AudioDevice()
        {
            InternalConstructor(0);
        }

		public AudioDevice(int idDevice)
		{
            InternalConstructor(idDevice);
		}

        private void InternalConstructor(int idAudioDevice)
        {
            CaptureDevicesCollection devices = new CaptureDevicesCollection();
            CaptureDeviceGuid = devices[idAudioDevice].DriverGuid;

			//			DevicesForm devices = new DevicesForm(this);
			//			devices.ShowDialog();

			InitDirectSound();

			if (applicationDevice == null)
				throw(new ArgumentNullException("Application Device is null"));
			else
			{
				FormatInfo	info			= new FormatInfo();
				WaveFormat	format			= new WaveFormat();

               
                format.SamplesPerSecond = 48000;
                format.BitsPerSample =16;
                format.Channels = 2;
				format.BlockAlign = (short)(format.Channels * (format.BitsPerSample / 8));
				format.AverageBytesPerSecond = format.BlockAlign * format.SamplesPerSecond;
                format.FormatTag = WaveFormatTag.Pcm;
				
				info.format = format;

				InputFormat = format;

				//				FormatsForm	formats = new FormatsForm(this);
				//				if (formats.ShowDialog() == DialogResult.OK)
				CreateCaptureBuffer();
				//				else
				//				{
				//					throw(new ArgumentNullException("Foprmats Abort"));
				//				}
			}
        }

		public void Dispose()
		{
			StopRecord();
		}

		private void InitDirectSound()
		{
			CaptureBufferSize = 0;
			NotifySize = 0;

			// Create DirectSound.Capture using the preferred capture device
			try
			{
				applicationDevice = new Capture(CaptureDeviceGuid);
			}
			catch {}
		}
		void CreateCaptureBuffer()
		{
			//-----------------------------------------------------------------------------
			// Name: CreateCaptureBuffer()
			// Desc: Creates a capture buffer and sets the format 
			//-----------------------------------------------------------------------------
			CaptureBufferDescription dscheckboxd = new CaptureBufferDescription();

			if (null != applicationNotify)
			{
				applicationNotify.Dispose();
				applicationNotify = null;
			}
			if (null != applicationBuffer)
			{
				applicationBuffer.Dispose();
				applicationBuffer = null;
			}

			if (0 == InputFormat.Channels)
				return;

			// Set the notification size
			NotifySize = (1024 > InputFormat.AverageBytesPerSecond / 8) ? 1024 : (InputFormat.AverageBytesPerSecond / 8);
			NotifySize -= NotifySize % InputFormat.BlockAlign;   

			// Set the buffer sizes
			CaptureBufferSize = NotifySize * NumberRecordNotifications;

			// Create the capture buffer
			dscheckboxd.BufferBytes = CaptureBufferSize;
			InputFormat.FormatTag = WaveFormatTag.Pcm;

			dscheckboxd.Format = InputFormat; // Set the format during creatation

			applicationBuffer = new CaptureBuffer(dscheckboxd, applicationDevice);
			NextCaptureOffset = 0;

			InitNotifications();
		}

		void InitNotifications()
		{
			//-----------------------------------------------------------------------------
			// Name: InitNotifications()
			// Desc: Inits the notifications on the capture buffer which are handled
			//       in the notify thread.
			//-----------------------------------------------------------------------------

			if (null == applicationBuffer)
				throw new NullReferenceException();
		
			// Create a thread to monitor the notify events
			if (null == NotifyThread)
			{
				NotifyThread = new Thread(new ThreadStart(WaitThread));
				Capturing = true;
				NotifyThread.Start();

				// Create a notification event, for when the sound stops playing
				NotificationEvent = new AutoResetEvent(false);
			}


			// Setup the notification positions
			for (int i = 0; i < NumberRecordNotifications; i++)
			{
				PositionNotify[i].Offset = (NotifySize * i) + NotifySize - 1;
                PositionNotify[i].EventNotifyHandle = NotificationEvent.Handle;
			}
		
			applicationNotify = new Notify(applicationBuffer);

			// Tell DirectSound when to notify the app. The notification will come in the from 
			// of signaled events that are handled in the notify thread.
			applicationNotify.SetNotificationPositions(PositionNotify, NumberRecordNotifications);
		}

		public void StartRecord()
		{
			StartOrStopRecord(true);
		}

		public void StopRecord()
		{
			if (null != NotificationEvent)
			{
				IsStartingRecord = false;
				Capturing = false;
				NotificationEvent.Set();
			}

			if (null != applicationBuffer)
				if (applicationBuffer.Capturing)
					StartOrStopRecord(false);
		}

		private void StartOrStopRecord(bool StartRecording)
		{
			//-----------------------------------------------------------------------------
			// Name: StartOrStopRecord()
			// Desc: Starts or stops the capture buffer from recording
			//-----------------------------------------------------------------------------

			try
			{
				if (StartRecording)
				{
					// Create a capture buffer, and tell the capture 
					// buffer to start recording   
					CreateCaptureBuffer();
					applicationBuffer.Start(true);
				}
				else
				{
					// Stop the buffer, and read any data that was not 
					// caught by a notification
					applicationBuffer.Stop();

                    if (Writer != null)
                    {
                        RecordCapturedData();

                        Writer.Seek(4, SeekOrigin.Begin); // Seek to the length descriptor of the RIFF file.
                        Writer.Write((int)(SampleCount + 36));	// Write the file length, minus first 8 bytes of RIFF description.
                        Writer.Seek(40, SeekOrigin.Begin); // Seek to the data length descriptor of the RIFF file.
                        Writer.Write(SampleCount); // Write the length of the sample data in bytes.

                        Writer.Close();	// Close the file now.
                        Writer = null;	// Set the writer to null.
                        WaveFile.Close();
                        WaveFile = null; // Set the FileStream to null.
                    }
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message, "StartOrStopRecord problem");
			}

		}

		void CreateRIFF()
		{
			try
			{
				// Open up the wave file for writing.
				WaveFile = new FileStream(FileName, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
				Writer = new BinaryWriter(WaveFile);

				// Set up file with RIFF chunk info.
				char[] ChunkRiff = {'R','I','F','F'};
				char[] ChunkType = {'W','A','V','E'};
				char[] ChunkFmt	= {'f','m','t',' '};
				char[] ChunkData = {'d','a','t','a'};
			
				short shPad = 1; // File padding
				int nFormatChunkLength = 0x10; // Format chunk length.
				int nLength = 0; // File length, minus first 8 bytes of RIFF description. This will be filled in later.
				short shBytesPerSample = 0; // Bytes per sample.

				// Figure out how many bytes there will be per sample.
				if (8 == InputFormat.BitsPerSample && 1 == InputFormat.Channels)
					shBytesPerSample = 1;
				else if ((8 == InputFormat.BitsPerSample && 2 == InputFormat.Channels) || (16 == InputFormat.BitsPerSample && 1 == InputFormat.Channels))
					shBytesPerSample = 2;
				else if (16 == InputFormat.BitsPerSample && 2 == InputFormat.Channels)
					shBytesPerSample = 4;

				// Fill in the riff info for the wave file.
				Writer.Write(ChunkRiff);
				Writer.Write(nLength);
				Writer.Write(ChunkType);

				// Fill in the format info for the wave file.
				Writer.Write(ChunkFmt);
				Writer.Write(nFormatChunkLength);
				Writer.Write(shPad);
				Writer.Write(InputFormat.Channels);
				Writer.Write(InputFormat.SamplesPerSecond);
				Writer.Write(InputFormat.AverageBytesPerSecond);
				Writer.Write(shBytesPerSample);
				Writer.Write(InputFormat.BitsPerSample);
			
				// Now fill in the data chunk.
				Writer.Write(ChunkData);
				Writer.Write((int)0);	// The sample length will be written in later.
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message, "CreateRIFF problem");
			}
		}

		void RecordCapturedData() 
		{
			try
            {
                //-----------------------------------------------------------------------------
                // Name: RecordCapturedData()
                // Desc: Copies data from the capture buffer to the output buffer 
                //-----------------------------------------------------------------------------
                byte[] CaptureData = null;
                int ReadPos;
                int CapturePos;
                int LockSize;

                applicationBuffer.GetCurrentPosition(out CapturePos, out ReadPos);
                LockSize = ReadPos - NextCaptureOffset;
                if (LockSize < 0)
                    LockSize += CaptureBufferSize;

                // Block align lock size so that we are always write on a boundary
                LockSize -= (LockSize % NotifySize);

                if (0 == LockSize)
                    return;

                // Read the capture buffer.
                CaptureData = (byte[])applicationBuffer.Read(NextCaptureOffset, typeof(byte), LockFlag.None, LockSize);

                // Write the data into the wav file
                Writer.Write(CaptureData, 0, CaptureData.Length);

                // Update the number of samples, in bytes, of the file so far.
                SampleCount += CaptureData.Length;

                // Move the capture offset along
                NextCaptureOffset += CaptureData.Length;
                NextCaptureOffset %= CaptureBufferSize; // Circular buffer


                if (OnGetLevel != null)
                    OnGetLevel(CaptureData);
            }
			catch{}
		}

		public void CreateSoundFile(string filename)
		{

			//-----------------------------------------------------------------------------
			// Name: OnCreateSoundFile()
			// Desc: Called when the user requests to save to a sound file
			//-----------------------------------------------------------------------------

			//			SaveFileDialog ofd = new SaveFileDialog();
			WaveFormat wf = new WaveFormat();

			// Get the default media path (something like C:\WINDOWS\MEDIA)
			if (string.Empty == Path)
				Path = Environment.SystemDirectory.Substring(0, Environment.SystemDirectory.LastIndexOf("\\")) + "\\media";

			if (Recording)
			{
				// Stop the capture and read any data that 
				// was not caught by a notification
				StartOrStopRecord(false);
				Recording = false;
			}

			//			ofd.DefaultExt = ".wav";
			//			ofd.Filter = "Wave Files|*.wav|All Files|*.*";
			//			ofd.FileName = FileName;
			//			ofd.InitialDirectory = Path;
		
			// Display the OpenFileName dialog. Then, try to load the specified file
			//			result = ofd.ShowDialog();

			//			if (DialogResult.Abort == result || DialogResult.Cancel == result)
			//				return;

			//			FileName = ofd.FileName;		
			FileName = filename;		

			try
			{
				CreateRIFF();
			}
			catch
			{
				return;
			}

			// Remember the path for next time
            try
            {
                Path = FileName.Substring(0, FileName.LastIndexOf("\\"));
            }
            catch { }
		}

		private void WaitThread()
		{
			while(Capturing)
			{
				try
				{
					//Sit here and wait for a message to arrive
					NotificationEvent.WaitOne(Timeout.Infinite, true);

					IsStartingRecord = true;
					RecordCapturedData();
				}
				catch{}
			}

            int a = 0;
            a++;
		}

	}

}
