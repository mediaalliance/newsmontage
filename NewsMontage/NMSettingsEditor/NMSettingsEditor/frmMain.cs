﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO;
using System.Xml;



namespace NMSettingsEditor
{
    public partial class frmMain : Form
    {
        private string NMSettingsFile = "NM_Settings.config";
        private XmlDocument settings_xml = null;
        private string m_format = "PAL";

        public frmMain()
        {
            InitializeComponent();

            if (!File.Exists("NewsMontage.exe"))
            {
                MessageBox.Show("NewsMontage.exe not found");
                Environment.Exit(1);
            }

            foreach (string language_file in Directory.GetFiles(".", "language.*.config"))
            {
                LanguageInfo li = new LanguageInfo(language_file);
                
                int index = cmbLanguage.Items.Add(li);                
            }

            if(cmbLanguage.Items.Count > 0) cmbLanguage.SelectedIndex = 0;

            Load();
        }

        private void Load()
        {
            if (File.Exists(NMSettingsFile))
            {
                settings_xml = new XmlDocument();
                settings_xml.Load(NMSettingsFile);

                if (settings_xml["NEWSMONTAGE_SETTINGS"] != null)
                {
                    XmlNode MAIN = settings_xml["NEWSMONTAGE_SETTINGS"];
                }
            }
        }


        private void Set(string framerate, string profile, string language)
        {
            XmlDocument doc = new XmlDocument();

            doc.Load("NM_Settings.config");


            XmlNode PROJECTS_OPTIONS = doc["NEWSMONTAGE_SETTINGS"]["PROJECTS_OPTIONS"];

            if (PROJECTS_OPTIONS["FRAMERATE"] != null)
            {
                PROJECTS_OPTIONS["FRAMERATE"].InnerText = framerate;
            }
            else
            {
                PROJECTS_OPTIONS.AppendChild(doc.CreateNode(XmlNodeType.Element, "FRAMERATE", "")).InnerText = framerate;
            }

            XmlNode RENDER_OPTIONS = doc["NEWSMONTAGE_SETTINGS"]["RENDER_OPTIONS"];

            if (RENDER_OPTIONS["FIX_PROFILE"] != null)
            {
                RENDER_OPTIONS["FIX_PROFILE"].InnerText = profile;
            }
            else
            {
                RENDER_OPTIONS.AppendChild(doc.CreateNode(XmlNodeType.Element, "FIX_PROFILE", "")).InnerText = profile;
            }

            XmlNode GUI_OPTIONS = doc["NEWSMONTAGE_SETTINGS"]["GUI_OPTIONS"];

            if (GUI_OPTIONS["LANGUAGE"] != null)
            {
                GUI_OPTIONS["LANGUAGE"].InnerText = language;
            }
            else
            {
                GUI_OPTIONS.AppendChild(doc.CreateNode(XmlNodeType.Element, "LANGUAGE", "")).InnerText = language;
            }

            doc.Save("NM_Settings.config");

            if (File.Exists("firstrun.dat"))
            {
                File.Delete("firstrun.dat");
            }

            bool Ok;
            System.Threading.Mutex m = new System.Threading.Mutex(true, "NewsMontage", out Ok);

            if (!Ok)
            {
                MessageBox.Show("New setting will be applied on next Newsmontage startup");
            }

        }



        private void btnPAL_Click(object sender, EventArgs e)
        {
            m_format = "PAL";
            btnNTSC.BackColor = Color.Transparent;
            btnPAL.BackColor = Color.Green;

            //Set("25", "MOV_PAL_420");
            //Close();
        }

        private void btnNTSC_Click(object sender, EventArgs e)
        {
            m_format = "NTSC";
            btnNTSC.BackColor = Color.Green;
            btnPAL.BackColor = Color.Transparent;

            //Set("29.97", "MOV_NTSC_420");
            //Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            string lang = "en";

            if(cmbLanguage.SelectedItem != null)
            {
                LanguageInfo li = (LanguageInfo)cmbLanguage.SelectedItem;
                lang = li.language_shortname;
            }

            if(m_format == "PAL")
            {
                Set("25", "MOV_PAL_420", lang);
            }
            else
            {
                Set("29.97", "MOV_NTSC_420", lang);
            }
        }
    }

    public class LanguageInfo
    {
        public string filename              = "";
        public string language_shortname    = "";
        public string description_name      = "";

        public LanguageInfo(string filename)
        {
            this.filename = filename;
            string fn = Path.GetFileName(filename).ToLower();
            language_shortname = fn.Replace("language.", "").Replace(".config", "");

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(filename);

                if (doc["configuration"] != null &&
                    doc["configuration"]["description"] != null &&
                    doc["configuration"]["description"]["language_name"] != null)
                {
                    description_name = doc["configuration"]["description"]["language_name"].InnerText;
                }
                
            }
            catch
            {
            }
        }

        public override string ToString()
        {
            if (description_name != "") return description_name;
            return language_shortname;
        }
    }
}
