using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;




namespace MediaAlliance.AV
{
	/// <summary>
	/// Summary description for WaveFile.
	/// </summary>
	public class WaveFile: IDisposable
	{
        // DELEGATES
        public delegate void WF_OnRiffRead(object Sender);
        public delegate void WF_OnFormatRead(object Sender);
        public delegate void WF_OnDataRead(object Sender);
        public delegate void WF_OnRendered(object Sender, Bitmap bmp, long tcin, long tcout, object Tag);
        public delegate void WF_OnProgress(object Sender, Bitmap bmp, long tc, int perc, object Tag);

        // EVENTS
        public event WF_OnRiffRead OnRiffRead       = null;
        public event WF_OnFormatRead OnFormatRead   = null;
        public event WF_OnDataRead OnDataRead       = null;
        public event WF_OnRendered OnRendered       = null;
        public event WF_OnProgress OnProgress       = null;


        private Thread TH_Read          = null;
        private bool TH_Read_Stop       = false;

        private Thread TH_Draw          = null;
        private bool TH_Draw_Stop       = false;
        private ManualResetEvent mre_Draw = new System.Threading.ManualResetEvent(false);


        private string m_Filepath;
        private FileInfo m_FileInfo;
        private FileStream m_FileStream;

        private CWavRiff m_Riff             = null;
        private CWavFormat m_Fmt            = null;
        private CWavData m_Data             = null;

        public object Tag                   = null;

        private List<Async_Info> WaitingRender = new List<Async_Info>();
        private Async_Info WorkingRender = null;

        private class Async_Info
        {
            public Bitmap bmp               = null;
            public Graphics grf             = null;
            public Pen pen                  = null;
            public long msecond_in          = 0;
            public long msecond_out         = 0;
            public float scale_height       = 0;
            public bool IsWorking           = false;
            public object Tag               = null;
            public bool HighQuality         = false;
            public int pecision             = 1000;

            public Async_Info(long msec_in, long msec_out, Pen pen, Bitmap bmp, Graphics grf, int pecision, bool highquality, object Tag)
            {
                this.msecond_in     = msec_in;
                this.msecond_out    = msec_out;
                this.pen            = pen;
                this.bmp            = bmp;
                this.grf            = grf;
                this.Tag            = Tag;
                this.HighQuality    = highquality;
                this.pecision       = pecision;
            }
        }



        private float m_PageScale = 1.0f;

		/// <summary>
		/// The Riff header is 12 bytes long
		/// </summary>
		public class CWavRiff
		{
			public CWavRiff()
			{
				m_RiffID = new byte[ 4 ];
				m_RiffFormat = new byte[ 4 ];
			}

			public void ReadRiff( FileStream inFS )
			{
				inFS.Read( m_RiffID, 0, 4 );
				
				Debug.Assert( m_RiffID[0] == 82, "Riff ID Not Valid" );

				BinaryReader binRead = new BinaryReader( inFS );

				m_RiffSize = binRead.ReadUInt32( );

				inFS.Read( m_RiffFormat, 0, 4 );
			}

			public byte[] RiffID
			{
				get { return m_RiffID; }
			}

			public uint RiffSize
			{
				get { return ( m_RiffSize ); }
			}

			public byte[] RiffFormat
			{
				get { return m_RiffFormat; }
			}

			private byte[]			m_RiffID;
			private uint			m_RiffSize;
			private byte[]			m_RiffFormat;
		}

		/// <summary>
		/// The Format header is 24 bytes long
		/// </summary>
		public class CWavFormat
		{
			public CWavFormat()
			{
				m_FmtID = new byte[ 4 ];
			}

			public void ReadFmt( FileStream inFS )
			{
				inFS.Read( m_FmtID, 0, 4 );

				Debug.Assert( m_FmtID[0] == 102, "Format ID Not Valid" );

				BinaryReader binRead = new BinaryReader( inFS );

				m_FmtSize = binRead.ReadUInt32( );
				m_FmtTag = binRead.ReadUInt16( );
				m_Channels = binRead.ReadUInt16( );
				m_SamplesPerSec = binRead.ReadUInt32( );
				m_AverageBytesPerSec = binRead.ReadUInt32( );
				m_BlockAlign = binRead.ReadUInt16( );
				m_BitsPerSample = binRead.ReadUInt16( );

				// This accounts for the variable format header size 
				// 12 bytes of Riff Header, 4 bytes for FormatId, 4 bytes for FormatSize & the Actual size of the Format Header 
				inFS.Seek( m_FmtSize + 20, System.IO.SeekOrigin.Begin );
			}

			public byte[] FmtID
			{
				get { return m_FmtID; }
			}

			public uint FmtSize
			{
				get { return m_FmtSize; }
			}

			public ushort FmtTag
			{
				get { return m_FmtTag; }
			}

			public ushort Channels
			{
				get { return m_Channels; }
			}

			public uint SamplesPerSec
			{
				get { return m_SamplesPerSec; }
			}

			public uint AverageBytesPerSec
			{
				get { return m_AverageBytesPerSec; }
			}

			public ushort BlockAlign
			{
				get { return m_BlockAlign; }
			}

			public ushort BitsPerSample
			{
				get { return m_BitsPerSample; }
			}

			private byte[]			m_FmtID;
			private uint			m_FmtSize;
			private ushort			m_FmtTag;
			private ushort			m_Channels;
			private uint			m_SamplesPerSec;
			private uint			m_AverageBytesPerSec;
			private ushort			m_BlockAlign;
			private ushort			m_BitsPerSample;
		}

		/// <summary>
		/// The Data block is 8 bytes + ???? long
		/// </summary>
		public class CWavData
		{
            private byte[] m_DataID = null;
            private uint m_DataSize;
            private Int16[] m_Data = null;
            private int m_NumSamples;
            
            public CWavData()
			{                
				m_DataID = new byte[ 4 ];
			}

			public void ReadData( FileStream inFS )
			{
				//inFS.Seek( 36, System.IO.SeekOrigin.Begin );

				inFS.Read( m_DataID, 0, 4 );

				Debug.Assert( m_DataID[0] == 100, "Data ID Not Valid" );

				BinaryReader binRead = new BinaryReader( inFS );

				m_DataSize = binRead.ReadUInt32( );

         
                m_Data = new System.Int16[m_DataSize];

				inFS.Seek( 40, System.IO.SeekOrigin.Begin );
			
				m_NumSamples = (int) ( m_DataSize / 2 );

				for ( int i = 0; i < m_NumSamples; i++)
				{
					m_Data[ i ] = binRead.ReadInt16( );
				}
			} 

			public byte[] DataID
			{
				get { return m_DataID; }
			}

			public uint DataSize
			{
				get { return m_DataSize; }
			}

			public Int16 this[ int pos ]
			{
				get { return m_Data[ pos ]; }
			}

			public int NumSamples
			{
				get { return m_NumSamples; }
			}
		}




        private Control m_parentControl = null;
        //Bitmap m_bmp = null;
        //private float m_scaleWidthfactor = 0.0f;
        private float m_scaleHeightfactor = 0.0f;

        private int m_scaleYFactor1 = 0;
        private int m_scaleYFactor2 = 0;

        //private int m_currentPosX = 0;

		public WaveFile( String inFilepath, Control parentControl)
		{
            m_parentControl = parentControl;
			m_Filepath = inFilepath;

			m_FileInfo = new FileInfo( inFilepath );
            m_FileStream = m_FileInfo.OpenRead();

			m_Riff = new CWavRiff( );
			m_Fmt = new CWavFormat( );
			m_Data = new CWavData( );


            m_scaleYFactor2 = 32758;                    //  m_scaleYFactor1 * 2;
            m_scaleYFactor1 = m_scaleYFactor2 / 2;      // 15000;
		}

        public uint GetLength()
        {
            uint len = 0;

            if (m_Data != null)
                len = m_Data.DataSize;// m_Riff.RiffSize;

            return len;
        }

        public float Duration()
        {
            try
            {
                return (float)((float)m_Data.NumSamples / (float)m_Fmt.SamplesPerSec) / (float)m_Fmt.Channels;
            }
            catch { return 0; }
        }


		public void Read( )
		{
            if (TH_Read == null)
            {
                TH_Read = new System.Threading.Thread(new System.Threading.ThreadStart(TH_Read_Task));
                TH_Read.IsBackground = true;
                TH_Read.Start();
            }
		}



        private void TH_Read_Task()
        {
            m_Riff.ReadRiff(m_FileStream);

            RAISE_OnRiffRead();

            if (TH_Read_Stop) return;

            // ======================================

            m_Fmt.ReadFmt(m_FileStream);

            RAISE_OnFormatRead();

            if (TH_Read_Stop) return;

            // ======================================

            m_Data.ReadData(m_FileStream);

            RAISE_OnDataRead();

            if (TH_Read_Stop) return;

            // ======================================

            m_FileStream.Close();
        }




        private void RAISE_OnRiffRead()
        {
            if (OnRiffRead != null)
            {
                foreach (WF_OnRiffRead singleCast in OnRiffRead.GetInvocationList())
                {
                    ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;
                    try
                    {
                        if (syncInvoke != null && syncInvoke.InvokeRequired)
                        {
                            syncInvoke.Invoke(OnRiffRead, new object[] { this });
                        }
                        else
                        {
                            singleCast(this);
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }

        private void RAISE_OnFormatRead()
        {
            if (OnFormatRead != null)
            {
                foreach (WF_OnFormatRead singleCast in OnFormatRead.GetInvocationList())
                {
                    ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;
                    try
                    {
                        if (syncInvoke != null && syncInvoke.InvokeRequired)
                        {
                            syncInvoke.Invoke(OnFormatRead, new object[] { this });
                        }
                        else
                        {
                            singleCast(this);
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }

        private void RAISE_OnDataRead()
        {
            if (OnDataRead != null)
            {
                foreach (WF_OnDataRead singleCast in OnDataRead.GetInvocationList())
                {
                    ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;
                    try
                    {
                        if (syncInvoke != null && syncInvoke.InvokeRequired)
                        {
                            syncInvoke.Invoke(OnDataRead, new object[] { this });
                        }
                        else
                        {
                            singleCast(this);
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }

        private void RAISE_OnRendered(Bitmap bmp, long tcin, long tcout, object Tag)
        {
            if (OnRendered != null)
            {
                foreach (WF_OnRendered singleCast in OnRendered.GetInvocationList())
                {
                    ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;
                
                    if (syncInvoke != null && syncInvoke.InvokeRequired)
                    {
                        syncInvoke.Invoke(OnRendered, new object[] { this, bmp, tcin, tcout, Tag });
                    }
                    else
                    {
                        singleCast(this, bmp, tcin, tcout, Tag);
                    }
                }
            }
        }

        private void RAISE_OnProgress(Bitmap bmp, long tc, int perc, object Tag)
        {
            if (OnRendered != null)
            {
                foreach (WF_OnProgress singleCast in OnProgress.GetInvocationList())
                {
                    ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;
                    try
                    {
                        if (syncInvoke != null && syncInvoke.InvokeRequired)
                        {
                            syncInvoke.Invoke(OnProgress, new object[] { this, bmp, tc, perc, Tag });
                        }
                        else
                        {
                            singleCast(this, bmp, tc, perc, Tag);
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }


        #region PUBLIC METHODS

        public void Draw_async(Bitmap bmp, Pen pen, long msec_in, long msec_out, int precision, bool HighQuality, object Tag)
        {
            try
            {
                Async_Info info = new Async_Info(msec_in, msec_out, pen, bmp, null, precision, HighQuality, Tag);

                if (info.msecond_in <= 0 && info.msecond_out <= 0)
                {
                    // NORMALIZZO VALORI SBALLATI

                    info.msecond_in = 0;
                    info.msecond_out = (long)((float)m_Data.NumSamples / (float)m_Fmt.SamplesPerSec / (float)m_Fmt.Channels) * 1000;
                }


                if (TH_Draw != null)
                {
                    if (WorkingRender != null)
                    {
                        //WaitingRender.Insert(0, info);
                        WaitingRender.Add(info);
                    }
                    else
                    {
                        WaitingRender.Clear();
                        WorkingRender = info;
                    }

                    mre_Draw.Set();
                }
                else
                {
                    WaitingRender.Clear();
                    WorkingRender = info;

                    TH_Draw = new System.Threading.Thread(new System.Threading.ThreadStart(TH_Draw_Task));
                    TH_Draw.IsBackground = true;
                    TH_Draw.Start();
                }
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }

        public void Draw(Bitmap bmp, Pen pen)
        {
            Draw(bmp, pen, 0, 0);
        }

        public void Draw(Bitmap bmp, Pen pen, long msec_in, long msec_out)
		{
            m_scaleHeightfactor = (float)((float)bmp.Height / 10000.0f);
            
            Graphics grfx = Graphics.FromImage(bmp);

            float sampleNum = (msec_out - msec_in) * (m_Fmt.SamplesPerSec / 1000);

            if (msec_in <= 0 && msec_out <= 0)
            {
                sampleNum = (float)m_Data.NumSamples;
                msec_in = 0;
                msec_out = (long)((float)m_Data.NumSamples / (float)m_Fmt.SamplesPerSec / (float)m_Fmt.Channels) * 1000;
            }

            float scaleXFactor = (float)(bmp.Width / sampleNum);// (float)m_Data.NumSamples);

            try
            {
                grfx.ScaleTransform(scaleXFactor, 1);

                RectangleF visBounds =  grfx.VisibleClipBounds;

                grfx.FillRectangle(new SolidBrush(Color.Black), visBounds);
                grfx.DrawLine(pen, 0, visBounds.Height / 2, visBounds.Width, visBounds.Height / 2);


                Draw16Bit(grfx, pen, visBounds, msec_in, msec_out);
            }
            catch { }

            grfx.Dispose();
		}

        public void ClearAll()
        {
            WaitingRender.Clear();
        }
        
        private void Draw16Bit(Graphics grfx, Pen pen, RectangleF visBounds, long msec_in, long msec_out)
		{
			short val = m_Data[ 0 ];
            //int prevX = 0;

            Point oldPoit1 = new Point(0, (int)(visBounds.Height / 4));
            Point oldPoit2 = new Point(0, (int)(visBounds.Height / 4) + ((int)(visBounds.Height / 2)));//+ (int)(visBounds.Height / 4));

            int start   = (int)(msec_in * (m_Fmt.SamplesPerSec / 1000)) + 22;
            int end     = (int)(msec_out * (m_Fmt.SamplesPerSec / 1000)) + 22;

            for (int i = start, x = 0; i < end; i += 1000, x += 1000)// m_Data.NumSamples; i += 1000)
			{
				val = m_Data[ i ];

                int scaledVal = (int)(((val + m_scaleYFactor1) * visBounds.Height) / m_scaleYFactor2);

                Point newPoit = new Point(x, scaledVal - (int)(visBounds.Height/4));
                grfx.DrawLine(pen, oldPoit1, newPoit);
                oldPoit1 = newPoit;

                if (m_Fmt.Channels == 2)
                {
                    i++;
                    
                    val = m_Data[i];

                    scaledVal = (int)(((val + m_scaleYFactor1) * visBounds.Height) / m_scaleYFactor2);

                    newPoit = new Point(x, scaledVal - (int)(visBounds.Height / 4) + ((int)(visBounds.Height / 2)));
                    grfx.DrawLine(pen, oldPoit2, newPoit);
                    oldPoit2 = newPoit;
                }                                
			}


		}
        
        #endregion PUBLIC METHODS



        private void TH_Draw_Task()
        {
            while (!TH_Draw_Stop)
            {
                float scaleHeightfactor = (float)((float)WorkingRender.bmp.Height / 10000.0f);

                int precision = WorkingRender.pecision;

                short val           = m_Data[0];
                long req_tc_len     = WorkingRender.msecond_out - WorkingRender.msecond_in;
                long req_samp_len   = req_tc_len * (m_Fmt.SamplesPerSec / 1000);
                int req_pic_width   = WorkingRender.bmp.Width;


                int first_sample = (int)(WorkingRender.msecond_in * (m_Fmt.SamplesPerSec / 1000));  // CONVERTO MS IN SAMPLE
                int last_sample = (int)(WorkingRender.msecond_out * (m_Fmt.SamplesPerSec / 1000));  // CONVERTO MS IN SAMPLE

                first_sample = (first_sample / precision) * precision;            // TENGO SOLO LA PARTE IN 1000 INTERA PRECENDETE
                last_sample = ((last_sample / precision) + 1) * precision;        // TENGO SOLO LA PARTE IN 1000 INTERA SUCCESSIVA

                first_sample    += 22;
                last_sample     += 22;


                // CREO E DISEGNO SU UNA NUOVA BITMAP CHE COPRE I DUE NUOVI MARK, PRESI IN UNITA' DI MILLE
                // QUESTA SARA' UGUALE O PIU' GRANDE DELL'ORIGINALE, IN SEGUITO TOLGO LA PARTE IN ECCESSO

                int virtual_bmp_w = (int)((float)(last_sample - first_sample) * (float)WorkingRender.bmp.Width / (float)req_samp_len);

                Bitmap virtual_bmp = new Bitmap(virtual_bmp_w, WorkingRender.bmp.Height);
                Graphics virtual_grf = Graphics.FromImage(virtual_bmp);

                if (WorkingRender.HighQuality)
                {
                    virtual_grf.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                }

                float scaleXFactor = (float)virtual_bmp.Width / (float)(last_sample - first_sample);  // (float)WorkingRender.bmp.Width / (float)req_samp_len;// (float)m_Data.NumSamples);

                virtual_grf.Clear(Color.DimGray);
                virtual_grf.ScaleTransform(scaleXFactor, 1);

                RectangleF visBounds = virtual_grf.VisibleClipBounds;

                Point oldPoit1 = new Point(0, (int)(visBounds.Height / 4));
                Point oldPoit2 = new Point(0, (int)(visBounds.Height / 4) + ((int)(visBounds.Height / 2)));//+ (int)(visBounds.Height / 4));


                if (TH_Draw_Stop) return;

                //Console.WriteLine("FRAME IN : " + WorkingRender.msecond_in / 40 + " FRAME OUT : " + WorkingRender.msecond_out / 40);
                //Console.WriteLine("FRAME IN : " + (first_sample - 22) / 48 / 40 + " FRAME OUT : " + (last_sample - 22) / 48 / 40);

                for (int i = first_sample, x = 0; 
                    i < last_sample && !TH_Draw_Stop == true; 
                    i += precision, x += precision)// m_Data.NumSamples; i += 1000)
                {
                    try
                    {
                        val = m_Data[i];


                        int scaledVal = (int)(((val + m_scaleYFactor1) * visBounds.Height) / m_scaleYFactor2);

                        Point newPoit = new Point(x, scaledVal);// - (int)(visBounds.Height / 2));
                        virtual_grf.DrawLine(WorkingRender.pen, oldPoit1, newPoit);
                        oldPoit1 = newPoit;

                        //RAISE_OnProgress(WorkingRender.bmp, i / 40, 0, WorkingRender.Tag);

                        Thread.Sleep(1);
                        //if (m_Fmt.Channels == 2)
                        //{
                        //    i++;

                        //    val = m_Data[i];

                        //    scaledVal = (int)(((val + m_scaleYFactor1) * visBounds.Height) / m_scaleYFactor2);

                        //    newPoit = new Point(x, scaledVal - (int)(visBounds.Height / 4) + ((int)(visBounds.Height / 2)));
                        //    WorkingRender.grf.DrawLine(WorkingRender.pen, oldPoit2, newPoit);
                        //    oldPoit2 = newPoit;
                        //}
                    }
                    catch { }
                }

                // RIFILO L'IMMAGINE SU QUELLA ORIGINALE

                // original_in - new_in : new_duration = x : new_width
                long original_in  = WorkingRender.msecond_in * (m_Fmt.SamplesPerSec / 1000);
                long original_out = WorkingRender.msecond_out * (m_Fmt.SamplesPerSec / 1000);
                int cut_x = (int)(((float)virtual_bmp_w * (float)(original_in- first_sample) / (float)(last_sample - first_sample)) );
                //int cut_w = (int)((float)virtual_bmp_w * (float)(original_out - last_sample) / (float)(last_sample - first_sample));

                //Console.WriteLine("X : " + cut_x);
                //Console.WriteLine("cut_w : " + virtual_bmp_w + cut_w);

                Graphics original_grf = Graphics.FromImage(WorkingRender.bmp);
                original_grf.DrawImage(virtual_bmp, -cut_x, 0);
                original_grf.Dispose();
                virtual_grf.Dispose();
                virtual_bmp.Dispose();

                RAISE_OnRendered(WorkingRender.bmp, WorkingRender.msecond_in, WorkingRender.msecond_out, WorkingRender.Tag);

                //Console.WriteLine("Waiting : " + WaitingRender.Count);

                if (WaitingRender.Count > 0)
                {
                    lock (WaitingRender)
                    {
                        WorkingRender = WaitingRender[0];
                        //WaitingRender.Clear();
                        WaitingRender.RemoveAt(0);
                    }
                }
                else
                {
                    WorkingRender   = null;
                    mre_Draw.Reset();
                    mre_Draw.WaitOne();
                }
            }
        }



        #region PUBLIC PROPERTIES

        public CWavRiff Riff
        {
            get { return m_Riff; }
        }

        public CWavFormat Format
        {
            get { return m_Fmt; }
        }

        public CWavData Data
        {
            get { return m_Data; }
        }
        
        public long Duration_ms
        {
            get
            {
                return (long)((float)m_Data.NumSamples / (float)m_Fmt.SamplesPerSec / (float)m_Fmt.Channels) * 1000;
            }
        }

        #endregion PUBLIC PROPERTIES



        public void  Dispose()
        {
            m_FileStream.Close();
        }

    }
}