using System;
using System.Collections.Generic;
using System.Text;

namespace MediaAlliance.DSTools
{
    public enum ML_VideoConverter_FieldProcess
    {
        eVCFP_None          = 0,
        eVCFP_Auto          = 1,
        eVCFP_SwapAndDown   = 2,
        eVCFP_SwapAndUp     = 3,
        eVCFP_Swap          = 4,
        eVCFP_FirstField    = 5,
        eVCFP_SecondField   = 6,
        eVCFP_DoubleRate    = 7,
        eVCFP_SecondBlack   = 8,
        eVCFP_FirstBlack    = 9
    } 

    class CLSID
    {
        public static Guid DirectShowFilter_RegMain     = new Guid("{083863F1-70DE-11D0-BD40-00A0C911CE86}");

        // MEDIALOOKS
        public static Guid MediaLooks_QTSource          = new Guid("{7CE55CCC-403E-4A29-8281-BF8542A0C37D}");
        public static Guid MediaLooks_VideoConverter    = new Guid("{82EE9DBD-3432-437E-A676-7153B6245304}");
        public static Guid MediaLooks_AudioMixer        = new Guid("{C54EAA68-7047-415D-A7EB-5131A655A6CD}");

        // GENERICI
        public static Guid DV_Muxer                     = new Guid("{129D7E40-C10D-11D0-AFB9-00AA00B67A42}");
        public static Guid DV_Splitter                  = new Guid("{4EB31670-9FC6-11CF-AF6E-00AA00B67A42}");
        public static Guid AVI_Muxer                    = new Guid("{E2510970-F137-11CE-8B67-00AA00A3F1A6}");
        public static Guid FileWriter                   = new Guid("{8596E5F0-0DA5-11D0-BD21-00A0C911CE86}");
        public static Guid AudioMixer                   = new Guid("{AEFA5024-215A-4FC7-97A4-1043C86FD0B8}");
        public static Guid WAVDest                      = new Guid("{3C78B8E2-6C4D-11D1-ADE2-0000F8754B99}");
        public static Guid NullRender                   = new Guid("{C1F400A4-3F08-11D3-9F0B-006008039E37}");


        public static Guid FileSourceAsync              = new Guid("{E436EBB5-524F-11CE-9F53-0020AF0BA770}");
        public static Guid AVI_Splitter                 = new Guid("{1B544C20-FD0B-11CE-8C63-00AA0044B51E}");
        public static Guid AVI_Decompressor             = new Guid("{CF49D4E0-1115-11CE-B03A-0020AF0BA770}");

        //// KAPRICORN
        //public static Guid Kapricorn_DVRawWriter        = new Guid("{256A1BAC-41DF-4997-AFB9-D5A6835AF568}");
        //public static Guid Kapricorn_DVRawSmartSource   = new Guid("{EC554720-779A-4937-ADE6-1FEDCF93F951}");
        //public static Guid Kapricorn_DVRawSource        = new Guid("{82798CCD-F391-4732-A0E0-0F611E3BCE17}");
        //public static Guid Kapricorn_VideoSource        = new Guid("{03D64D10-1CA5-4CCE-A3CF-2ED87350BFD8}");



        // MAINCONCEPT
        public static Guid MainConcept_DV_Encoder       = new Guid("{E8AC42C1-0810-11D5-8062-00001C0B1B7F}");
        public static Guid MainConcept_DV_Decoder       = new Guid("{E8AC42C0-0810-11D5-8062-00001C0B1B7F}");

        public static Guid MainConcept_DVCPRO_Encoder   = new Guid("{D65A3701-F119-49ED-838B-65CCB897AC15}");
        public static Guid MainConcept_DVCPRO_Decoder   = new Guid("{D65A3700-F119-49ED-838B-65CCB897AC15}");
        
        public static Guid MainConcept_DV_Splitter      = new Guid("{88811874-395E-4577-96BE-8EEE3BA253A5}");
        public static Guid MainConcept_DV_Muxer         = new Guid("{56616502-00AA-4715-A302-086F4A7D6B3F}");

        public static Guid MainConcept_MPGVIDEO_Encoder = new Guid("{00098205-76CC-497E-98A1-6EF10D0BF26C}");
        public static Guid MainConcept_MPGAUDIO_Encoder = new Guid("{15BEBB32-5BB5-42B6-B45A-BA49F78BA19F}");
        public static Guid MainConcept_MPG_Muxer        = new Guid("{CFD28198-115C-4606-BD64-27A7E0051D2A}");

        public static Guid MainConcept_Image_Scaler     = new Guid("{BEB7FFE8-37BA-4849-AE26-7A10EF20A303}");
        public static Guid MainConcept_AudioResampler   = new Guid("{7C32A8A2-17B8-4925-9699-9863A9B7BCB8}");
        public static Guid MainConcept_FrameRateConv    = new Guid("{A4DCA218-AC9E-4D1F-8600-C5B1F390D408}");


        // MICROSOFT
        public static Guid Microsoft_DV_Decoder         = new Guid("{B1B77C00-C3E4-11CF-AF79-00AA00B67A42}");
        public static Guid Microsoft_WM_Reader          = new Guid("{187463A0-5BB7-11D3-ACBE-0080C75E246E}");
        public static Guid Microsoft_WMV_Writer         = new Guid("{7C23220E-55BB-11D3-8B16-00C04FB6BD3D}");
        public static Guid Microsoft_WM_VideoDecoder    = new Guid("{94297043-BD82-4DFD-B0DE-8177739C6D20}");
        public static Guid Microsoft_WM_AudioDecoder    = new Guid("{94297043-BD82-4DFD-B0DE-8177739C6D20}");

        // PANASONIC
        public static Guid Panasonic_MXF_Source         = new Guid("{074320DB-FB6D-4D4A-B38B-386C12BDAC5B}");

        //DPS
        public static Guid DPS_FileSource               = new Guid("{F0807866-0862-4750-99D8-59B191005E5B}");
        
    }
}
