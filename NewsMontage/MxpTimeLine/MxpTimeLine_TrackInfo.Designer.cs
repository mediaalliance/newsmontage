﻿namespace MediaAlliance.Controls.MxpTimeLine.TrackInfo
{
    partial class MxpTL_TrackInfo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // MxpTL_TrackInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "MxpTL_TrackInfo";
            this.Size = new System.Drawing.Size(131, 210);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.MxpTL_TrackInfo_Paint);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MxpTL_TrackInfo_MouseUp);
            this.Resize += new System.EventHandler(this.MxpTL_TrackInfo_Resize);
            this.ResumeLayout(false);

        }

        #endregion

    }
}
