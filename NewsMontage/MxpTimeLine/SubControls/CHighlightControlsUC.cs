﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Forms;
using System.Drawing;
using MediaAlliance.Controls.MxpTimeLine.Objects;


namespace MediaAlliance.Controls.MxpTimeLine.SubControls
{
    public class CHighlightMagnet : Control
    {
        private CMxpClip clip = null;
        private MxpTimeLine.MxpTmeLine owner = null;

        public CHighlightMagnet()
        {
            this.SetStyle(ControlStyles.ResizeRedraw, false);
            this.SetStyle(ControlStyles.Selectable, false);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.SetStyle(ControlStyles.Opaque, false);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
        }


        #region PUBLIC PROPERTIES

        public CMxpClip Clip
        {
            get { return clip; }
            set
            {
                if (clip != value)
                {
                    clip = value;

                    if (clip == null)
                    {
                        this.Visible = false;
                    }
                    else
                    {
                        this.Top        = clip.ParentTrack.Y + owner.Tracks.Y;
                        this.Height     = clip.ParentTrack.Height;
                        this.Width      = 5;
                        this.Left       = (clip.Last_magnet_tc == clip.TimcodeIn) ? clip.X : clip.X + clip.Width;
                        this.Left       -= 2;
                        this.Visible    = true;
                    }
                }
            }
        }

        #endregion PUBLIC PROPERTIES



        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.Clear(Color.Transparent);

            Pen p = new Pen(Brushes.Yellow, 1);
            p.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;

            e.Graphics.DrawLine(new Pen(Brushes.Red, 3), (Width / 2)  , 0, (Width / 2) , Height);
            e.Graphics.DrawLine(p, Width / 2, 0, Width / 2, Height);
            e.Graphics.DrawLine(new Pen(Brushes.Yellow, 1), 0, 0, Width, 0);
            e.Graphics.DrawLine(new Pen(Brushes.Yellow, 1), 0, Height, Width, Height);
            base.OnPaint(e);
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            //base.OnPaintBackground(pevent);
        }

        protected override void OnVisibleChanged(EventArgs e)
        {
            if (this.Visible == true)
            {
                if (this.Tag != null && this.Tag is CMxpClip)
                {
                    //((CMxpTLTracks_Collection)((CMxpClip)this.Tag).ParentTrack.Parent).Render();
                    //owner.Refresh();
                    owner.Invalidate(Bounds);
                }
            }

            base.OnVisibleChanged(e);
        }

        protected override void OnParentChanged(EventArgs e)
        {
            owner = (MxpTimeLine.MxpTmeLine)this.Parent;
            base.OnParentChanged(e);
        }
    }
}
