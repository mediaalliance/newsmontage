﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Forms;
using System.Drawing;
using MediaAlliance.Controls.MxpTimeLine.Objects;


namespace MediaAlliance.Controls.MxpTimeLine.SubControls
{
    public class CDraggingControlUC : UserControl
    {
        private CMxpClip clip = null;
        private MxpTimeLine.MxpTmeLine owner = null;

        private Label dragging_tcin_control     = null;
        private Label dragging_tcout_control    = null;
        private Panel pnl_timecodein            = null;
        private Panel pnl_timecodeout           = null;
        private long tcin                       = 0;
        private long tcout                      = 0;

        public CDraggingControlUC()
        {
            dragging_tcin_control               = new Label();
            dragging_tcin_control.Font          = new Font(Font.FontFamily, 8, FontStyle.Bold);
            dragging_tcin_control.ForeColor     = Color.Black;
            dragging_tcin_control.Top           = 0;
            dragging_tcin_control.BackColor     = Color.LightGreen;
            dragging_tcin_control.Height        = 20;
            dragging_tcin_control.TextAlign     = ContentAlignment.MiddleCenter;
            dragging_tcin_control.AutoSize      = false;
            dragging_tcin_control.Width         = 95;

            dragging_tcout_control              = new Label();
            dragging_tcout_control.Font         = new Font(Font.FontFamily, 8, FontStyle.Bold);
            dragging_tcout_control.ForeColor    = Color.Black;
            dragging_tcout_control.Top          = 0;
            dragging_tcout_control.BackColor    = Color.LightGreen;
            dragging_tcout_control.Height       = 20;
            dragging_tcout_control.TextAlign    = ContentAlignment.MiddleCenter;
            dragging_tcout_control.AutoSize     = false;
            dragging_tcout_control.Width        = 95;


            pnl_timecodein                      = new Panel();
            pnl_timecodein.Width                = 1;
            pnl_timecodein.BackColor            = Color.LightGreen;
            pnl_timecodein.Visible              = false;

            pnl_timecodeout                     = new Panel();
            pnl_timecodeout.Width               = 1;
            pnl_timecodeout.BackColor           = Color.LightGreen;
            pnl_timecodeout.Visible             = false;

            //dragging_tcout_control = new CTimecodeLiveMarkerUC();

            //SetStyle(ControlStyles.SupportsTransparentBackColor, true);

            //this.BackColor = Color.FromArgb(120, Color.Green);
            this.BackColor = Color.Green;
        }


        #region PUBLIC PROPERTIES

        public CMxpClip Clip
        {
            get { return clip; }
            set
            {
                if (clip != value)
                {
                    clip = value;

                    if (clip == null)
                    {
                        this.Visible = false;
                    }
                    else
                    {
                        this.Top        = clip.ParentTrack.Y + owner.Tracks.Y;
                        this.Height     = clip.ParentTrack.Height;
                        this.Width      = 5;
                        this.Left       = (clip.Last_magnet_tc == clip.TimcodeIn) ? clip.X : clip.X + clip.Width;
                        this.Left       -= 2;
                        this.Visible    = true;
                    }
                }
            }
        }

        public long TimecodeIn
        {
            get { return tcin; }
            set
            {
                if (tcin != value)
                {
                    tcin = value;
                    dragging_tcin_control.Text = CTcTools.Frame2TC(value, owner.FPS);
                    Console.WriteLine(value.ToString());
                }
            }
        }

        public long TimecodeOut
        {
            get { return tcout; }
            set
            {
                if (tcout != value)
                {
                    tcout = value;
                    dragging_tcout_control.Text = CTcTools.Frame2TC(value, owner.FPS);         
                }
            }
        }

        #endregion PUBLIC PROPERTIES


        //protected override void OnPaint(PaintEventArgs e)
        //{
        //    //e.Graphics.Clear(Color.Aqua);

        //    //Pen p = new Pen(Brushes.Yellow, 1);
        //    //p.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;

        //    //e.Graphics.DrawLine(new Pen(Brushes.Red, 3), (Width / 2)  , 0, (Width / 2) , Height);
        //    //e.Graphics.DrawLine(p, Width / 2, 0, Width / 2, Height);
        //    //e.Graphics.DrawLine(new Pen(Brushes.Yellow, 1), 0, 0, Width, 0);
        //    //e.Graphics.DrawLine(new Pen(Brushes.Yellow, 1), 0, Height, Width, Height);
        //    base.OnPaint(e);
        //}

        //protected override void OnPaintBackground(PaintEventArgs pevent)
        //{
        //    //base.OnPaintBackground(pevent);
        //}

        protected override void OnVisibleChanged(EventArgs e)
        {
            dragging_tcin_control.Visible   = this.Visible;
            dragging_tcout_control.Visible  = this.Visible;
            pnl_timecodein.Visible          = this.Visible;
            pnl_timecodeout.Visible         = this.Visible;

            base.OnVisibleChanged(e);
        }

        protected override void OnParentChanged(EventArgs e)
        {
            owner = (MxpTimeLine.MxpTmeLine)this.Parent;
            dragging_tcin_control.Parent = this.Parent;
            dragging_tcout_control.Parent = this.Parent;

            pnl_timecodeout.Parent      = this.Parent;
            pnl_timecodein.Parent       = this.Parent;

            pnl_timecodein.Top          = 0;
            pnl_timecodeout.Top         = 0;

            base.OnParentChanged(e);
        }
    
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // CDraggingControlUC
            // 
            this.Name = "CDraggingControlUC";
            this.Size = new System.Drawing.Size(284, 262);
            this.ResumeLayout(false);

        }

        protected override void OnMove(EventArgs e)
        {
            dragging_tcin_control.Left  = Left - dragging_tcin_control.Width;
            dragging_tcout_control.Left = Right;
            pnl_timecodein.Left         = Left - 1;
            pnl_timecodeout.Left        = this.Right;

            if (Parent != null)
            {
                pnl_timecodein.Height   = Parent.Height;
                pnl_timecodeout.Height  = Parent.Height;
            }

            base.OnMove(e);
        }

        protected override void OnParentBindingContextChanged(EventArgs e)
        {
            base.OnParentBindingContextChanged(e);
        }
    }
}
