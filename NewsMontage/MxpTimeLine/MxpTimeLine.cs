#define NO_RECTIME_BAR

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using System.Drawing.Drawing2D;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Threading;

using MediaAlliance.Controls.MxpTimeLine.Classes;
using MediaAlliance.Controls.MxpTimeLine.Objects;
using MediaAlliance.Controls.MxpTimeLine.Engine;
using MediaAlliance.Controls.MxpTimeLine.UndoManager;
using MediaAlliance.Controls.MxpTimeLine.UndoDefinitions;
using MediaAlliance.Controls.MxpTimeLine.SubControls;
using MediaAlliance.Controls.MxpTimeLine.TrackInfo;
using MediaAlliance.Win32;
using MediaAlliance.Controls.MxpTimeLine.Subcontrols;
using System.Reflection;


[assembly: Obfuscation(Feature = "code control flow obfuscation", Exclude = false)]
[assembly: Obfuscation(Feature = "encrypt resources", Exclude = false)]
[assembly: Obfuscation(Feature = "rename symbol names with printable characters", Exclude = false)]




namespace MediaAlliance.Controls.MxpTimeLine
{
    // PRIVATE DELEGATES

    internal delegate void Internal_Render();

    // PUBLIC DELEGATES

    public delegate void MXPTL_OnChangePosition(object Sender, long position, bool Internal);
    public delegate void MXPTL_OnRenderSubitem(object Sender);
    public delegate void MXPTL_ZoomAreaChanged(object Sender, long ZoomIn, long ZoomOut);
    public delegate void MXPTL_OnChangeClipSelection(object Sender, CMxpClip[] SelectedClips);
    public delegate void MXPTL_OnChangeMarker(object Sender, long FrameIn, long FrameOut, enTcChangeType change_type);
    public delegate void MXPTL_OnEndingClipEditing(object Sender, CMxpClip Clip, ref bool Cancel);
    public delegate void MXPTL_OnTimecodeEvent(object Sender, long tc, CTimeTableItem item, enTimeTableItemType itemposition);
    public delegate void MXPTL_OnOnlineItemsTableChanged(object Sender, long tc, List<CTimeTableItem> online_items);
    public delegate void MXPTL_OnItemChangeOnlineStatus(object Sender, CTimeTableItem Item, bool online);
    public delegate void MXPTL_OnMouseWheel(object Sender, MouseEventArgs e);
    public delegate void MXPTL_OnInternalEngineEvent(object Sender, InternEngineEventHandle e);
    public delegate void MXPTL_OnChangeActiveActions(object Sender, MxpTimelineAction Actions);
    public delegate void MXPTL_OnExitedFromActions(object Sender, MxpTimelineAction Action, object objref);
    public delegate void MXPTL_OnClipAdded(object Sender, CMxpClip clip);
    public delegate void MXPTL_OnClipRemoving(object Sender, CMxpClip clip);
    public delegate void MXPTL_OnClipDisplayInfoChanged(object Sender, CMxpClip clip);

    public delegate void MXPTL_OnAdvanceDraggingEvent(object Sender, IDataObject DragData, CMxpTrack over_track, ref int[] excluded_tracks, ref long rappresent_lenght, ref bool cancel);
    public delegate void MXPTL_OnAdvenceDragDrop(object Sender, DragEventArgs e, Point ManagedDrop);
    public delegate void MXPTL_OnRequestPlaymarked(object Sender);

    public delegate void MXPTL_OnUndoEvent(object Sender, UndoEventHandler e);

    //public delegate void MXPTL_OnProjectNeedClipInfo(object Sender, CMxpClip clip, ref CMxpTL_ClipInfo clipinfo);
    //public delegate void MXPTL_OnProjectRequestClip(object Sender, int idclip, string filename);



    [DefaultEvent("Click")]
    public partial class MxpTmeLine : UserControl
    {
        // GRAPHICAL PUBLIC PROPERTIES
        private Color FRecTimeColor                 = Color.Azure;

        #if !NO_RECTIME_BAR
        private int FRecTimeHeight                  = 5; 
        #endif

        private int FRecTimeXpos                    = 0;

        private CCursors TLCursors                  = null;            // MANAGER CAMBIO CURSORI

        // VARIABLES
        private long zoomIn                         = 0;
        private long zoomOut                        = 0;
        private long tcIn                           = 0;
        private long tcOut                          = 0;

        private float fps                           = 25;

        private int FLastClickedTrack               = -1;
        private bool FIsDragging                    = false;

        private CMagnetInfo lastDraggingMagInfo     = null;

        //private Rectangle FGridBounds;

        private Point FHoldPosition                 = new Point(0, 0);
        private Mtx_HitTest_OBJ FHitTest            = Mtx_HitTest_OBJ.None;

        private long FLastSettedFrame               = -1;

        // PUBLIC PROPERTIES FIELDS
        private CMxpClip selectedClip               = null;
        private CMxpClip currentEditingClip         = null;
        private bool canSeekOverRectime             = true;
        private bool canMarkOverRectime             = false;
        private bool canDragoutClips                = false;
        private bool canDragoutMarkers              = false;
        private bool m_canModifyMarkerByGUI         = true;
        private bool isEditingClip                  = false;
        private bool m_FireDisplayClipEventOnPan    = true;
        private bool m_CanSeekOverClip              = false;

        private bool canDisableTracks               = true;
        private bool canEditAudioNodes              = true;
        private bool canDragClip                    = true;

        private bool isResizing                     = false;
        private MxpTimelineAction actions           = MxpTimelineAction.none;
        
        private CMxpTlZoomBar.KTL_ZOOMTRACK_HITTEST_OBJ FLastClickedOn_ZoomBar = CMxpTlZoomBar.KTL_ZOOMTRACK_HITTEST_OBJ.None;

        private bool isMarking                      = false;
        private bool isClipCutting                  = false;
        private long markIn                         = 0;
        private long markOut                        = 0;
        private long recTime                        = 0;
        private bool blockPositionChangeMsg         = false;

        private GraphicsPath FGPMarkIn              = new GraphicsPath();
        private Int32 FLastDrawinIn                 = 0;
        private GraphicsPath FGPMarkOut             = new GraphicsPath();
        private Int32 FLastDrawinOut                = 0;

        private ContextMenuStrip FMnuGeneral        = null;
        private ContextMenuStrip FMnuClip           = null;
        private CMxpClip FClipOfContextMenu         = null;
        private CMxpClip dragging_clip              = null;
        private CTimeTable TT                       = null;

        private bool FFocused                       = false;
        private bool showFocusBorder                = true;
        private bool FShowFocus                     = true;
        private Color FFocusedColor                 = Color.LightSalmon;

        private Control timeLineTrackInfo_Container = null;


        public List<CMxpClip> CurrentOnlineClips    = new List<CMxpClip>();
        public CTimeTableItem LastTopOnlineItem     = null;
        private long lastMouseTc                    = 0;

        private long m_lastPositionRaised           = -1;
        private bool m_lastPositionInternal         = false;


        private DragDropEffects lastDragEffect      = DragDropEffects.None;

        private Rectangle VirtualObject;

        public enum Mtx_HitTest_OBJ
        { 
            None                = 0,
            OnZoomBar           = 1,
            GridBar             = 2,
            OnTracks            = 3,
            MarkInHandle        = 4,
            MarkOutHandle       = 5
        }

        private Dictionary<Mtx_HitTest_OBJ, Rectangle> HitTest_Areas = new Dictionary<Mtx_HitTest_OBJ, Rectangle>();

        // EVENTS
        public event MXPTL_ZoomAreaChanged           OnZoomChanged              = null;
        public event MXPTL_OnChangePosition          OnChangePosition           = null;
        public event MXPTL_OnChangeClipSelection     OnChangeClipSelection      = null;
        public event MXPTL_OnChangeMarker            OnChangeMarker             = null;
        public event MXPTL_OnEndingClipEditing       OnEndingClipEditing        = null;
        public event MXPTL_OnTimecodeEvent           OnTimecodeEvent            = null;
        public event MXPTL_OnOnlineItemsTableChanged OnOnlineItemsTableChanged  = null;
        public event MXPTL_OnItemChangeOnlineStatus  OnItemChangeOnlineStatus   = null;

        public event MXPTL_OnInternalEngineEvent     OnInternalEngineEvent      = null;
        public event MXPTL_OnChangeActiveActions     OnChangeActiveActions      = null;
        public event MXPTL_OnExitedFromActions       OnExitedFromActions        = null;
        public event MXPTL_OnClipAdded               OnClipAdded                = null;
        public event MXPTL_OnClipRemoving            OnClipRemoving             = null;
        public event MXPTL_OnClipDisplayInfoChanged  OnClipDisplayInfoChanged   = null;

        public event MXPTL_OnAdvanceDraggingEvent    OnAdvanceDraggingEvent     = null;
        public event MXPTL_OnAdvenceDragDrop         OnAdvenceDragDrop          = null;
        public event MXPTL_OnRequestPlaymarked       OnRequestPlaymarked        = null;

        public event MXPTL_OnUndoEvent               OnUndoEvent                = null;

        //public event MXPTL_OnProjectNeedClipInfo     OnProjectNeedClipInfo      = null;
        //public event MXPTL_OnProjectRequestClip      OnProjectRequestClip       = null;



        // CLASSES
        private CMxpTlZoomBar zoomBar               = null;
        private CMxpTlGrid FGrid                    = null;
        private CMxpTLTracks_Collection tracks      = null;
        private CMxpTlCursor FCursor                = null;
        private CUndoManager undoManager            = null;

        private MxpTL_TrackInfo trackinfo_control   = null;


        // ENGINEs
        private Engine.CTimeLine_Engine engine = null;

        private System.Timers.Timer tmrDelayedAction = null;
        private System.Timers.Timer tmrDelayedCursorPositioningAction = null;
        private System.Timers.Timer tmrDelayedExitFromZoomMW = null;
        private System.Timers.Timer tmrDelayedChangePosition = null;

        //private Thread thRender             = null;
        //private bool thRender_stop          = false;
        //private ManualResetEvent mreRender  = new ManualResetEvent(false);


        private Thread ThChangePos_Dispatcher               = null;
        private long ThChangePos_Value                      = -1;
        private bool ThChangePos_Internal                   = false;
        private bool ThChangePos_Dispatcher_Stop            = false;
        private ManualResetEvent ThChangePos_Dispatcher_mre = new ManualResetEvent(false);


        private bool isFirstPaint                           = true;

        // HILIGH OVER ITEMS

        private CHighlightMagnet pnltest = new CHighlightMagnet();
        private CDraggingControlUC dragging_control = new CDraggingControlUC();
        //private CTimecodeLiveMarkerUC dragging_tcin_control = new CTimecodeLiveMarkerUC();


        public MxpTmeLine()
        {
            InitializeComponent();

            TLCursors = new CCursors(this);


            // INIZIALIZZAZIONE TIMERS

            tmrDelayedAction = new System.Timers.Timer(100);
            tmrDelayedAction.Elapsed += new System.Timers.ElapsedEventHandler(tmrDelayedAction_Elapsed);


            tmrDelayedCursorPositioningAction = new System.Timers.Timer(30);
            tmrDelayedCursorPositioningAction.Elapsed += new System.Timers.ElapsedEventHandler(tmrDelayedCursorPositioningAction_Elapsed);


            tmrDelayedExitFromZoomMW = new System.Timers.Timer(30);
            tmrDelayedExitFromZoomMW.Elapsed += new System.Timers.ElapsedEventHandler(tmrDelayedExitFromZoomMW_Elapsed);

            tmrDelayedChangePosition = new System.Timers.Timer(100);
            tmrDelayedChangePosition.Elapsed += new System.Timers.ElapsedEventHandler(tmrDelayedChangePosition_Elapsed);


            pnltest.Parent  = this;
            pnltest.Visible = false;
            pnltest.Left    = -100;
            pnltest.Top     = -100;
            pnltest.Width   = 10;
            pnltest.Height  = 10;

            dragging_control.SetBounds(-300, -300, 20, 20);
            dragging_control.Visible = false;
            dragging_control.Parent = this;

            //dragging_tcin_control.Visible = false;
            //dragging_tcin_control.Parent = this;

            

            // INIZIALIZZO LE HITTEST AREAS

            foreach (Mtx_HitTest_OBJ ht in Enum.GetValues(typeof(Mtx_HitTest_OBJ)))
            {
                HitTest_Areas.Add(ht, new Rectangle());
            }


            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            //this.SetStyle(ControlStyles.ResizeRedraw, false);

            this.MouseWheel += new MouseEventHandler(MxpTL_MouseWheel);

            // UNDOMANAGER
            undoManager                         = new CUndoManager(this);
            undoManager.OnRecall                += new CUndoManager.Undo_OnRecall(undoManager_OnRecall);
            undoManager.OnUndo                  += new CUndoManager.Undo_OnUndo(undoManager_OnUndo);
            undoManager.OnNewSnapshot           += new CUndoManager.Undo_OnNewSnapshot(undoManager_OnNewSnapshot);

            // TIMETABLE
            TT                                  = new CTimeTable(this);
            TT.OnHappends                       += new CTimeTable.TT_OnHappends(TT_OnHappends);
            TT.OnEvent                          += new CTimeTable.TT_OnEvent(TT_OnEvent);

            // ZOOM SCROLL BAR INIT
            zoomBar                             = new CMxpTlZoomBar();
            zoomBar.OnRender                    += new MXPTL_OnRenderSubitem(FZoomScroll_OnRender);
            zoomBar.OnZoomChanged               += new MXPTL_ZoomAreaChanged(FZoomScroll_OnZoomChanged);
            zoomBar.Width                       = Width;
            zoomBar.Height                      = 10;

            // GRID BAR INIT
            FGrid                               = new CMxpTlGrid(this);
            FGrid.Width                         = Width;
            FGrid.Height                        = 10;
            FGrid.OnRender                      += new MXPTL_OnRenderSubitem(FGrid_OnRender);

            // CURSOR
            FCursor                             = new CMxpTlCursor(FGrid);
            FCursor.OnChangePosition            += new CMxpTlCursor.KTLCursor_OnChangePosition(FCursor_OnChangePosition);
            FCursor.FramePosition               = 0;

            // TIME LINES CONTAINER INIT
            tracks                              = new CMxpTLTracks_Collection(this);
            tracks.Width                        = Width;
            tracks.Height                       = Height - zoomBar.Height - FGrid.Height;
            tracks.OnRender                     += new MXPTL_OnRenderSubitem(tracks_OnRender);
            tracks.OnEditClip                   += new CMxpTLTracks_Collection.MxpTrack_OnEditClip(tracks_OnEditClip);
            tracks.OnChangeClipSelection        += new CMxpTLTracks_Collection.MxpTrack_OnChangeClipSelection(tracks_OnChangeClipSelection);
            tracks.OnTrackAdded                 += new CMxpTLTracks_Collection.MxpTrack_OnTrackAdded(tracks_OnTrackAdded);
            tracks.OnClipAdded                  += new CMxpTLTracks_Collection.MxpTrack_OnClipAdded(tracks_OnClipAdded);
            tracks.OnVirtualObjectNeed          += new MxpClip_OnVirtualObjectNeed(tracks_OnVirtualObjectNeed);
            tracks.OnClipRemoving               += new CMxpTLTracks_Collection.MxpTrack_OnClipRemoving(tracks_OnClipRemoving);
            tracks.AutoSizeHeight               = true;

            // ThChangePos_Dispatcher

            if (!DesignMode)
            {
                ThChangePos_Dispatcher = new Thread(new ThreadStart(ThChangePos_Dispatcher_Task));
                ThChangePos_Dispatcher.IsBackground = true;
                ThChangePos_Dispatcher.Start();
            }
        }




        private void ThChangePos_Dispatcher_Task()
        {
            long tc_lastsent = -1;

            while (!ThChangePos_Dispatcher_Stop)
            {
                while (ThChangePos_Value != tc_lastsent)
                {
                    if (ThChangePos_Value >= 0)
                    {
                        Raise_OnChangePosition(ThChangePos_Value, ThChangePos_Internal);
                    }

                    tc_lastsent = ThChangePos_Value;

                    //Thread.Sleep(20);
                }

                ThChangePos_Dispatcher_mre.WaitOne();
                ThChangePos_Dispatcher_mre.Reset();
            }

            ThChangePos_Dispatcher_Stop = false;
        }

        private void ThChangePos_Dispatcher_STOP()
        {
            if (ThChangePos_Dispatcher != null)
            {
                ThChangePos_Dispatcher_Stop = true;

                if (!ThChangePos_Dispatcher.Join(100))
                {
                    ThChangePos_Dispatcher.Abort();
                }
            }

            ThChangePos_Dispatcher = null;
            ThChangePos_Dispatcher_Stop = false;
        }

        private void DISPATCH_CHANGEPOS(long pos, bool Internal)
        {
            ThChangePos_Value = pos;
            ThChangePos_Internal = Internal;
            ThChangePos_Dispatcher_mre.Set();
        }
        
        private void tmrDelayedChangePosition_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            tmrDelayedChangePosition.Stop();

            if (m_lastPositionRaised != FCursor.FramePosition && m_lastPositionInternal)
            {
                m_lastPositionRaised = FCursor.FramePosition;
                Console.WriteLine("*** tmrDelayedChangePosition_Elapsed ***");

                DISPATCH_CHANGEPOS(m_lastPositionRaised, m_lastPositionInternal);
            }
        }

        private void Raise_OnChangePosition(long tc, bool Internal)
        {
            tmrDelayedChangePosition.Stop();            // DOPO L'ULTIMO INVIO PARTE IL TIMER PER DELAYED ACTION
            tmrDelayedChangePosition.Start();

            if (OnChangePosition != null)
            {
                foreach (MXPTL_OnChangePosition singleCast in OnChangePosition.GetInvocationList())
                {
                    ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;

                    if (syncInvoke != null && syncInvoke.InvokeRequired)
                    {
                        // Invokie the event on the main thread
                        syncInvoke.Invoke(OnChangePosition, new object[] { this, tc, Internal });
                    }
                    else
                    {
                        // Raise the event
                        singleCast(this, tc, Internal);
                    }
                }
            }
        }




        void FGrid_OnRender(object Sender)
        {
            Refresh();
        }

        

        #region UNDOMANAGER EVENTS
        
        void undoManager_OnUndo(object Sender, int total_remains)
        {
            if (OnUndoEvent != null) OnUndoEvent(this, new UndoEventHandler((CUndoManager)Sender, UndoEventType.undoDone));
        }

        void undoManager_OnRecall(object Sender, ref bool cancel)
        {
            //if (item is CUndoInfo)
            //{
            //    CUndoInfo info = (CUndoInfo)item;

            //    if (info.object_ref != null)
            //    {
            //        CMxpTL_Object obj = (CMxpTL_Object)info.object_ref;

            //        obj.SetUndo_Status(item);
            //    }
            //}

            //switch (item.UndoType)
            //{
            //    case enUndoItemType.clipChangedClippin_in:
            //        break;
            //    case enUndoItemType.clipChangedClippin_out:
            //        break;
            //    case enUndoItemType.clipMoved:
            //        break;
            //    case enUndoItemType.clipRemoved:
            //        break;
            //    case enUndoItemType.clipAdded:
            //        CMxpClip clip = item.ObjectRef as CMxpClip;
            //        foreach (CMxpClip subclip in clip.Group)
            //        {
            //            subclip.Delete();
            //        }
            //        clip.Delete();
            //        break;
            //    case enUndoItemType.audioNodeAdded:
            //        break;
            //    case enUndoItemType.audioNodeChanged:
            //        break;
            //    case enUndoItemType.audioNodeRemoved:
            //        break;
            //    case enUndoItemType.trackAdded:
            //        break;
            //    case enUndoItemType.trackRemoved:
            //        break;
            //    default:
            //        break;
            //}
        }

        void undoManager_OnNewSnapshot(object Sender)
        {
            if (OnUndoEvent != null) OnUndoEvent(this, new UndoEventHandler((CUndoManager)Sender, UndoEventType.newSnapshowTaken));
        }

        #endregion UNDOMANAGER EVENTS

        

        #region TIMETABLE EVENTS

        void TT_OnHappends(object Sender, long timecode, CTimeTableItem item, enTimeTableItemType itemposition)
        {
            if (OnTimecodeEvent != null) OnTimecodeEvent(this, timecode, item, itemposition);
        }

        void TT_OnEvent(object Sender, long timecode,  enTimeTableEventType etype, object item)
        {

            switch (etype)
            {
                case enTimeTableEventType.undefinde:
                    break;
                case enTimeTableEventType.item_goes_online:
                    if (OnItemChangeOnlineStatus != null) OnItemChangeOnlineStatus(this, (CTimeTableItem)item, true);   
                    break;
                case enTimeTableEventType.item_goes_offline:
                    if (OnItemChangeOnlineStatus != null) OnItemChangeOnlineStatus(this, (CTimeTableItem)item, false);                    
                    break;
                case enTimeTableEventType.item_added:
                    break;
                case enTimeTableEventType.item_removed:
                    break;
                case enTimeTableEventType.item_changedposition:
                    break;
                case enTimeTableEventType.online_list_changed:
                    {
                        LastTopOnlineItem = null;

                        List<CTimeTableItem> onlines = TT.OnlineItems;

                        if (onlines.Count > 0)
                        {
                            LastTopOnlineItem = onlines[0];
                        }

                        CurrentOnlineClips.Clear();

                        lock (CurrentOnlineClips)
                        {
                            foreach (CTimeTableItem itClip in onlines.Where(x => x.Tag is CMxpClip))
                            {
                                CurrentOnlineClips.Add((CMxpClip)itClip.Tag);
                            }
                        }

                        if (OnOnlineItemsTableChanged != null) OnOnlineItemsTableChanged(this, timecode, onlines);
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion TIMETABLE EVENTS




        private void SetCursorPos()
        {
            SetCursorPos(this.PointToClient(Cursor.Position).X);
        }

        private void SetCursorPos(int xPos)
        {
            long tc_at_newpos = GetTimecodeAt(xPos);

            if (tc_at_newpos < 0)       xPos = 0;
            if (tc_at_newpos > tcOut) return;

            if (MediaAlliance.Win32.CWin32.GetAsyncKeyState(Keys.ControlKey))
            {
                // SE DURANTE IL SETTAGGIO DI UNA NUOVA POSIZIONE DEL CURSORE L'UTENTE
                // STA TENENDO PREMUTO IL CONTROL, INTERCETTO CON MAGNETE EVENTUALI
                // OGGETTI NELLA TIMELINE

                long tc = FCursor.PixToFrame(xPos);

                CMagnetInfo mag = TT.GetMagnetInfo(tc, FCursor.PixInFrame(10), new object[] { FCursor });

                if (mag != null)
                {
                    tc = mag.mag_timecode;
                    xPos = FCursor.FrameToPix(tc);
                }
            }

            //tmrDelayedCursorPositioningAction.Stop();

            FCursor.PixelPosition = xPos;// (!FCanSeekOverRectime && (e.X > FRecTimeXpos)) ? FRecTimeXpos : e.X;
            Refresh();

            Actions_Add(MxpTimelineAction.isScrubbing);

            //tmrDelayedCursorPositioningAction.Start();
        }


        private void BuildMarkHandle(Point pIn, Point pOut)
        {
            if (FLastDrawinIn != pIn.X)
            {
                FGPMarkIn.Dispose();
                FGPMarkIn = new GraphicsPath();
                FGPMarkIn.AddLine(pIn.X, pIn.Y, pIn.X, pIn.Y + 10);
                FGPMarkIn.AddLine(pIn.X - 5, pIn.Y + 10, pIn.X - 5, pIn.Y + 8);
                FGPMarkIn.AddLine(pIn.X - 2, pIn.Y + 8, pIn.X - 2, pIn.Y + 2);
                FGPMarkIn.AddLine(pIn.X - 5, pIn.Y + 2, pIn.X - 5, pIn.Y);
                FLastDrawinIn = pIn.X;
            }

            if (FLastDrawinOut != pOut.X)
            {
                FGPMarkOut.Dispose();
                FGPMarkOut = new GraphicsPath();
                FGPMarkOut.AddLine(pOut.X, pOut.Y, pOut.X, pOut.Y + 10);
                FGPMarkOut.AddLine(pOut.X + 5, pOut.Y + 10, pOut.X + 5, pOut.Y + 8);
                FGPMarkOut.AddLine(pOut.X + 2, pOut.Y + 8, pOut.X + 2, pOut.Y + 2);
                FGPMarkOut.AddLine(pOut.X + 5, pOut.Y + 2, pOut.X + 5, pOut.Y);
                FLastDrawinOut = pOut.X;
            }
        }

        private void FCursor_OnChangePosition(object Sender, int x, long frame)
        {
            TT.TimeCode = frame;

            bool Internal = (FLastSettedFrame != frame);
            
            tmrDelayedCursorPositioningAction.Stop();
            DISPATCH_CHANGEPOS(frame, Internal);

            if ((actions & MxpTimelineAction.isZooming) != MxpTimelineAction.isZooming)
            {
                tmrDelayedCursorPositioningAction.Start();
            }

            FLastSettedFrame = -1;
        }

        protected override void OnGotFocus(EventArgs e)
        {
            FFocused = true;
            Refresh();
            base.OnGotFocus(e);
        }

        protected override void OnLostFocus(EventArgs e)
        {
            FFocused = false;
            Refresh();
            base.OnLostFocus(e);
        }
        


        #region TRACKS EVENTS

        private void tracks_OnRender(object Sender)
        {
            //Console.WriteLine("tracks_OnRender");
            Refresh();
        }

        private void tracks_OnEditClip(object Sender, CMxpClip clip, bool Edit)
        {
            if (Edit)
            {
                isMarking          = true;
                currentEditingClip = clip;
                isEditingClip      = true;
                markIn             = clip.TimcodeIn;
                markOut            = clip.TimecodeOut;
            }
            else
            {
                isMarking          = false;
                currentEditingClip = null;
                isEditingClip      = false;
                markIn             = -1;
                markOut            = -1;
            }
            Render();
        }

        private void tracks_OnChangeClipSelection(object Sender, CMxpClip[] SelectedClips)
        {
            if (SelectedClips.Length > 0) selectedClip = SelectedClips[SelectedClips.Length -1];

            if (OnChangeClipSelection != null)
            {
                OnChangeClipSelection(this, SelectedClips);
            }
        }

        private void tracks_OnTrackAdded(object Sender, CMxpTrack track)
        {
            Render();
        }

        private void tracks_OnClipAdded(object Sender, CMxpClip clip)
        {
            selectedClip = clip;

            if (OnExitedFromActions != null) OnExitedFromActions(this, MxpTimelineAction.isEditingClipIn, clip);
            if (OnExitedFromActions != null) OnExitedFromActions(this, MxpTimelineAction.isEditingClipOut, clip);

            clip.OnDisplayInfoChange += new CMxpClip.MxpClip_OnDisplayInfoChange(clip_OnDisplayInfoChange);

            if (OnClipAdded != null)
            {
                OnClipAdded(this, clip);
            }
            TT.AddClip(clip);
        }

        private void tracks_OnVirtualObjectNeed(object Sender, Rectangle rect)
        {
            if (Sender != null)
            {
                VirtualObject = rect;
                Refresh();
            }
            else
            {
                VirtualObject = new Rectangle(0, 0, 0, 0);
                Refresh();
            }
        }

        private void tracks_OnClipRemoving(object Sender, CMxpClip clip)
        {
            if (OnClipRemoving != null) OnClipRemoving(this, clip);
        }


        #endregion TRACKS EVENTS




        #region CLIP EVENTS

        void clip_OnDisplayInfoChange(object Sender)
        {
            if (!m_FireDisplayClipEventOnPan && 
                  (
                    (actions & MxpTimelineAction.isPanning) == MxpTimelineAction.isPanning
                    || 
                    (actions & MxpTimelineAction.isZooming) == MxpTimelineAction.isZooming
                  )) return;

            if (OnClipDisplayInfoChanged != null) OnClipDisplayInfoChanged(this, (CMxpClip)Sender);
        }

        #endregion CLIP EVENTS




        void FZoomScroll_OnZoomChanged(object Sender, long newZoomIn, long newZoomOut)
        {
            zoomIn      = newZoomIn;
            zoomOut     = newZoomOut;

            FGrid.SetZoom(newZoomIn, newZoomOut);
            tracks.SetZoom(newZoomIn, newZoomOut);

            FCursor.FramePosition = FCursor.FramePosition;

            if (OnZoomChanged != null) OnZoomChanged(this, newZoomIn, newZoomOut);
        }

        void FZoomScroll_OnRender(object Sender)
        {
            //Console.WriteLine("FZoomScroll_OnRender");
            Refresh(); 
        }

        
        private void Actions_Add(params MxpTimelineAction[] add_actions)
        {
            MxpTimelineAction new_act = actions;

            foreach (MxpTimelineAction act in add_actions)
            {
                if ((actions & act) != act)
                {
                    switch (act)
                    {
                        case MxpTimelineAction.none:
                            break;
                        case MxpTimelineAction.isZooming:
                            break;
                        case MxpTimelineAction.isPanning:
                            if (SelectedClip != null)
                            {
                                //SelectedClip.IsSelected = false;
                            }
                            undoManager.TakeSnapshot();
                            break;
                        case MxpTimelineAction.isEditingClipIn:
                            undoManager.TakeSnapshot();
                            break;
                        case MxpTimelineAction.isEditingClipOut:
                            undoManager.TakeSnapshot();
                            break;
                        case MxpTimelineAction.isScrubbing:
                            break;
                        default:
                            break;
                    }
                }

                new_act = new_act | act;

                if ((new_act & MxpTimelineAction.isZooming) == MxpTimelineAction.isZooming)
                {
                    tracks.IsResizing = true;
                }
            }

            Actions = new_act;
        }

        private void Actions_Remove(object objref, params MxpTimelineAction[] remove_actions)
        {
            MxpTimelineAction new_act = actions;

            foreach(MxpTimelineAction act in remove_actions)
            {
                if ((new_act & act) == act)
                {
                    if (OnExitedFromActions != null) OnExitedFromActions(this, act, objref);

                    if (act == MxpTimelineAction.isPanning || act == MxpTimelineAction.isZooming)
                    {
                        foreach (CMxpClip audio_clip in tracks.AllAudioClips)
                        {
                            if (audio_clip.Group.Count == 0 && audio_clip.IsVisible)
                            {
                                audio_clip.RefreshDisplayedInfo();
                                audio_clip.NotifyDisplayedInfoChanged();

                                tmrDelayedExitFromZoomMW.Stop();
                                tmrDelayedExitFromZoomMW.Start();
                            }

                            Console.WriteLine("OUT FROM ZOOM OR PAN");
                        }
                    }

                    new_act = new_act ^ act;
                }


                if ((new_act & MxpTimelineAction.isZooming) != MxpTimelineAction.isZooming)
                {
                    tracks.IsResizing = false;
                }
            }

            Actions = new_act;
        }

        
        private void MxpTL_MouseDown(object sender, MouseEventArgs e)
        {
            FIsDragging = true;
            FHoldPosition = e.Location;

            FHitTest = GetHitTest(e.Location);

            switch (FHitTest)
            {
                case Mtx_HitTest_OBJ.OnZoomBar:
                    {
                        FLastClickedOn_ZoomBar = zoomBar.GetHitTest(new Point(e.X, e.Y - (Height - zoomBar.Height)));
                        break;
                    }
                case Mtx_HitTest_OBJ.OnTracks:
                    {
                        #region SULLE TRACKS

                        tracks.GetHitTest(new Point(e.X - tracks.ClientBounds.Left, e.Y - tracks.ClientBounds.Top));

                        if (e.Button == MouseButtons.Left)
                        {
                            if (tracks.LastHitTested == Mxp_Tracks_HitTest_Info.OnTrack)
                            {
                                // SONO IN UNA TRACK

                                CMxpTrack trk = tracks.LastClickedTrak;

                                MxpTrack_HitTest_Info httrk = trk.GetHitTest(new Point(e.X - trk.ClientBounds.X - tracks.ClientBounds.Left,
                                                                                        e.Y - trk.ClientBounds.Y - tracks.ClientBounds.Top)); 

                                switch (httrk)
	                            {
                                    case MxpTrack_HitTest_Info.clip:

                                        #region SU UNA CLIP

                                        // SONO IN UNA CLIP
                                        CMxpClip clip = trk.LastClickedClip;

                                        if (clip != null)
                                        {
                                            Point pointOnClip = new Point(e.X - trk.ClientBounds.X - clip.ClientBounds.X - tracks.ClientBounds.Left,
                                                                          e.Y - trk.ClientBounds.Y - clip.ClientBounds.Y - tracks.ClientBounds.Top);

                                            MxpClip_HitTest_Info htclp = trk.LastClickedClip.GetHitTest(pointOnClip);

                                            switch (htclp)
                                            {
                                                case MxpClip_HitTest_Info.leftHandle:
                                                    break;
                                                case MxpClip_HitTest_Info.rightHandle:
                                                    break;
                                                case MxpClip_HitTest_Info.thumb:
                                                    break;
                                                case MxpClip_HitTest_Info.audioNode:
                                                    break;
                                                default:
                                                    clip.LastHitXPosition = e.Location;
                                                    break;
                                            }
                                        }
                                        
                                        #endregion SU UNA CLIP

                                        break;

                                    case MxpTrack_HitTest_Info.audio0cursor:
                                    case MxpTrack_HitTest_Info.audio0track:

                                        break;

                                    default:
                                        // SPOSTO IL CURSORE
                                        
                                        //FCursor.PixelPosition = (!FCanSeekOverRectime && (e.X > FRecTimeXpos)) ? FRecTimeXpos : e.X;
                                        int pos = (!canSeekOverRectime && (e.X > FRecTimeXpos)) ? FRecTimeXpos : e.X;
                                        SetCursorPos(pos);
                                        break;
	                            }
                            }
                            else
                            {
                                // NON SONO IN UNA TRACK
                                // SPOSTO IL CURSORE
                                //FCursor.PixelPosition = (!FCanSeekOverRectime && (e.X > FRecTimeXpos)) ? FRecTimeXpos : e.X;
                                int pos = (!canSeekOverRectime && (e.X > FRecTimeXpos)) ? FRecTimeXpos : e.X;
                                SetCursorPos(pos);
                                Refresh();                            
                            }
                        }

                        #endregion SULLE TRACKS

                        break;
                    }
                case Mtx_HitTest_OBJ.GridBar:
                    {
                        int pos = (!canSeekOverRectime && (e.X > FRecTimeXpos)) ? FRecTimeXpos : e.X;
                        SetCursorPos(pos);
                        break;
                    }
            }
        }

        private void MxpTL_MouseUp(object sender, MouseEventArgs e)
        {
            FIsDragging = false;

            if ((Actions & MxpTimelineAction.isEditingClipIn) == MxpTimelineAction.isEditingClipIn ||
                (Actions & MxpTimelineAction.isEditingClipOut) == MxpTimelineAction.isEditingClipOut)
            {
                DISPATCH_CHANGEPOS(this.CurrentPosition, true);
            }
            
            Actions_Remove( null, 
                            MxpTimelineAction.isPanning,
                            MxpTimelineAction.isZooming,
                            MxpTimelineAction.isScrubbing);

            Actions_Remove( SelectedClip,
                            MxpTimelineAction.isEditingClipIn, 
                            MxpTimelineAction.isEditingClipOut,
                            MxpTimelineAction.isMarkingIn,
                            MxpTimelineAction.isMarkingOut);

            ClipOfContextMenu = null;

            TLCursors.Set(enCursortType.Default);

            if (e.Button == MouseButtons.Right && FHitTest == Mtx_HitTest_OBJ.OnTracks)
            {
                #region MOUSE DESTRO

                tracks.GetHitTest(new Point(e.X - tracks.ClientBounds.Left, e.Y - tracks.ClientBounds.Top));

                if(tracks.LastHitTested == Mxp_Tracks_HitTest_Info.OnTrack)
                {
                    if (tracks.LastClickedTrak != null)
                    {
                        if (tracks.LastClickedTrak.LastHitTested == MxpTrack_HitTest_Info.clip)
                        {
                            ClipOfContextMenu = tracks.LastClickedTrak.LastClickedClip;

                            foreach (CMxpClip var in tracks.AllClips)
                            {
                                var.IsSelected = false;
                            } 

                            ClipOfContextMenu.IsSelected = true;

                            if (FMnuClip != null)
                            {
                                Point scrpos = this.PointToScreen(e.Location);
                                FMnuClip.Tag = ClipOfContextMenu;
                                FMnuClip.Show(scrpos);
                                return;
                            }
                        }
                    }
                }
                if (FMnuGeneral != null)
                {
                    Point scrpos = this.PointToScreen(e.Location);
                    FMnuGeneral.Show(scrpos);
                    return;
                }

                #endregion MOUSE DESTRO
            }

            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                #region MOUSE SINISTRO

                switch (FHitTest)
                {
                    case Mtx_HitTest_OBJ.OnZoomBar:
                        {
                            #region ZOOMBAR

                            switch (FLastClickedOn_ZoomBar)
                            {
                                case CMxpTlZoomBar.KTL_ZOOMTRACK_HITTEST_OBJ.LeftButton:
                                    {
                                        if (zoomBar.ZoomIn > TCIN)
                                        {
                                            zoomBar.MoveZoomArea(-1);
                                        }
                                        break;
                                    }
                                case CMxpTlZoomBar.KTL_ZOOMTRACK_HITTEST_OBJ.RightButton:
                                    {
                                        if (zoomBar.ZoomOut < TCOUT)
                                        {
                                            zoomBar.MoveZoomArea(1);
                                        }
                                        break;
                                    }
                            }

                            #endregion ZOOMBAR

                            break;
                        }
                    case Mtx_HitTest_OBJ.OnTracks:
                        {
                            #region TRACKS

                            tracks.GetHitTest(new Point(e.X - tracks.ClientBounds.Left, e.Y - tracks.ClientBounds.Top));

                            switch (tracks.LastHitTested)
                            {
                                case Mxp_Tracks_HitTest_Info.OnTrack:
                                    {
                                        #region SU UNA TRACK

                                        // SE CLICCO SOPRA UNA TRACK

                                        if (tracks.LastClickedTrak != null)
                                        {
                                            // SELEZIONO LA TRACK RELATIVA
                                            FLastClickedTrack = tracks.LastClickedTrak.ID;
                                            tracks.LastClickedTrak.Selected = true;

                                            CMxpTrack trk = tracks.LastClickedTrak;

                                            switch (tracks.LastClickedTrak.LastHitTested)
                                            {
                                                case MxpTrack_HitTest_Info.audio0track:
                                                case MxpTrack_HitTest_Info.audio0cursor:

                                                    break;

                                                case MxpTrack_HitTest_Info.clip:
                                                    {
                                                        #region CLIP
                                                        
                                                        // SE CLICCO SOPRA UNA CLIP
                                                        CMxpClip clip = trk.LastClickedClip;
                                                        bool CtrlKey = CWin32.GetAsyncKeyState(Keys.ControlKey);

                                                        if (!CtrlKey)
                                                            clip.IsMoving = false;

                                                        if (clip != null)
                                                        {
                                                            // CLIP VIDEO
                                                            Point pointOnClip = new Point(e.X - trk.ClientBounds.X - clip.ClientBounds.X - tracks.ClientBounds.Left,
                                                                                          e.Y - trk.ClientBounds.Y - clip.ClientBounds.Y - tracks.ClientBounds.Top);

                                                            if (clip.IsMoving) clip.IsMoving = false;

                                                            if (isClipCutting)
                                                            {
                                                                long tc = FGrid.PixToFrame(e.X);
                                                                undoManager.TakeSnapshot();
                                                                Console.WriteLine("Before CUT : " + clip.ClippingIn + " -> " + clip.ClippingOut);
                                                                clip.Cut(tc);
                                                                Thread.Sleep(100);
                                                                if (OnExitedFromActions != null) OnExitedFromActions(this, MxpTimelineAction.isEditingClipOut, clip);
                                                                clip.NotifyRender_NoEVents();
                                                                Console.WriteLine("After CUT  : " + clip.ClippingIn + " -> " + clip.ClippingOut);

                                                                this.isClipCutting = false;
                                                            }
                                                            else
                                                            {
                                                                MxpClip_HitTest_Info htclp = clip.GetHitTest(pointOnClip);
                                                                if (clip.Type == enCMxpClipType.Video)
                                                                {
                                                                    #region CLIP TYPE VIDEO

                                                                    switch (clip.GetHitTest(e.Location))
                                                                    {
                                                                        case MxpClip_HitTest_Info.leftHandle:
                                                                            break;
                                                                        case MxpClip_HitTest_Info.rightHandle:
                                                                            break;
                                                                        case MxpClip_HitTest_Info.thumb:
                                                                        default:
                                                                            if (!tracks.LastClickedTrak.LastClickedClip.IsSelected)
                                                                            {
                                                                                // DESELEZIONO CLIP PRECEDENTEMENTE SELEZIONATE
                                                                                DeselectAllClip();

                                                                                // SELEZIONO LA CLIP RELATIVA
                                                                                tracks.LastClickedTrak.LastClickedClip.IsSelected = true;
                                                                                selectedClip = tracks.LastClickedTrak.LastClickedClip;
                                                                            }
                                                                            break;
                                                                    }

                                                                    #endregion CLIP TYPE VIDEO
                                                                }
                                                                else
                                                                {
                                                                    #region CLIP TYPE AUDIO

                                                                    switch (clip.GetHitTest(pointOnClip))//e.Location))
                                                                    {
                                                                        case MxpClip_HitTest_Info.leftHandle:
                                                                            break;
                                                                        case MxpClip_HitTest_Info.rightHandle:
                                                                            break;
                                                                        case MxpClip_HitTest_Info.audioNode:
                                                                            if (clip.IsSelected &&
                                                                                CWin32.GetAsyncKeyState(Keys.ControlKey) && 
                                                                                clip.LastHitTestObject is CAudioNodeInfo &&
                                                                                clip.CanEditAudio)
                                                                            { 
                                                                                CAudioNodeInfo an = (CAudioNodeInfo)clip.LastHitTestObject;
                                                                                clip.AudioInfo.RemoveNode(an.timecode);
                                                                            }
                                                                            break;
                                                                        case MxpClip_HitTest_Info.thumb:
                                                                        default:
                                                                            if (CWin32.GetAsyncKeyState(Keys.ControlKey) && canEditAudioNodes)
                                                                            {
                                                                                long tc = 0;
                                                                                float value = 1F;
                                                                                if (clip.IsSelected)
                                                                                {
                                                                                    clip.GetAudioValueAt(pointOnClip.X, pointOnClip.Y, out tc, out value);
                                                                                    clip.AudioInfo.AddNode(tc - clip.TimcodeIn, value);
                                                                                }
                                                                                else
                                                                                {
                                                                                    clip.IsSelected = true;
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                if (!tracks.LastClickedTrak.LastClickedClip.IsSelected)
                                                                                {
                                                                                    // DESELEZIONO CLIP PRECEDENTEMENTE SELEZIONATE
                                                                                    DeselectAllClip();

                                                                                    // SELEZIONO LA CLIP RELATIVA
                                                                                    tracks.LastClickedTrak.LastClickedClip.IsSelected = true;
                                                                                    selectedClip = tracks.LastClickedTrak.LastClickedClip;
                                                                                }
                                                                            }
                                                                            break;
                                                                    }

                                                                    #endregion CLIP TYPE AUDIO
                                                                }
                                                            }

                                                        }

                                                        #endregion CLIP

                                                        break;
                                                    }
                                            }
                                        }
                                        
                                        #endregion SU UNA TRACK

                                        break;
                                    }
                                case Mxp_Tracks_HitTest_Info.None:
                                    {
                                        // SE NON CLICCO SOPRA NESSUNA TIMELINE O CLIP
                                        DeselectAllClip();
                                        break;
                                    }
                            }
                            
                            #endregion TRACKS

                            break;
                        }
                }
                
                #endregion MOUSE SINISTRO
            }

            FHitTest = Mtx_HitTest_OBJ.None;
        }
        
        private void MxpTL_MouseMove(object sender, MouseEventArgs e)
        {
            pnltest.Clip = null;

            lastMouseTc = FGrid.PixToFrame(e.X);

            if (e.Button == MouseButtons.None)
            {
                #region NO MOUSE BUTTON

                FHitTest = GetHitTest(e.Location);

                switch (FHitTest)
                {
                    case Mtx_HitTest_OBJ.OnZoomBar:
                        {
                            #region ZOOMBAR

                            FLastClickedOn_ZoomBar = zoomBar.GetHitTest(new Point(e.X, e.Y - (Height - zoomBar.Height)));

                            switch (FLastClickedOn_ZoomBar)
                            {
                                case CMxpTlZoomBar.KTL_ZOOMTRACK_HITTEST_OBJ.TrackSliderLeftHandle:
                                case CMxpTlZoomBar.KTL_ZOOMTRACK_HITTEST_OBJ.TrackSliderRightHandle:
                                    {
                                        TLCursors.Set(enCursortType.ZoomTrackScale);
                                        break;
                                    }
                                case CMxpTlZoomBar.KTL_ZOOMTRACK_HITTEST_OBJ.TrackSlider:
                                    {
                                        TLCursors.Set(enCursortType.ZoomTrackMove);
                                        break;
                                    }
                                default:
                                    {
                                        TLCursors.Set(enCursortType.Default);
                                        break;
                                    }
                            }

                            #endregion ZOOMBAR

                            break;
                        }
                    case Mtx_HitTest_OBJ.OnTracks:
                        {
                            #region NELLE TRACKS

                            TLCursors.Set(enCursortType.Default);

                            tracks.GetHitTest(new Point(e.X - tracks.ClientBounds.Left, e.Y - tracks.ClientBounds.Top));

                            if (tracks.LastHitTested == Mxp_Tracks_HitTest_Info.OnTrack)
                            {
                                // SONO IN UNA TRACK

                                CMxpTrack trk = tracks.LastClickedTrak;

                                MxpTrack_HitTest_Info httrk = trk.GetHitTest(new Point(e.X - trk.ClientBounds.X - tracks.ClientBounds.Left,
                                                                                        e.Y - trk.ClientBounds.Y - tracks.ClientBounds.Top));

                                switch (httrk)
                                {
                                    case MxpTrack_HitTest_Info.clip:

                                        // SONO IN UNA CLIP
                                        CMxpClip clip = trk.LastClickedClip;

                                        if (clip != null)
                                        {
                                            Point pointOnClip = new Point(e.X - trk.ClientBounds.X - clip.ClientBounds.X - tracks.ClientBounds.Left,
                                                                          e.Y - trk.ClientBounds.Y - clip.ClientBounds.Y - tracks.ClientBounds.Top);

                                            MxpClip_HitTest_Info htclp = trk.LastClickedClip.GetHitTest(pointOnClip);

                                            switch (htclp)
                                            {
                                                case MxpClip_HitTest_Info.leftHandle:
                                                    if (CWin32.GetAsyncKeyState(Keys.ControlKey))
                                                    {
                                                        // SE E' PREMUTO IL CONTROL MOSTRO IL CURSORE DI EDIT-MARKIN
                                                        TLCursors.Set(enCursortType.EditMarkIn);
                                                    }
                                                    break;
                                                case MxpClip_HitTest_Info.rightHandle:
                                                    if (CWin32.GetAsyncKeyState(Keys.ControlKey))
                                                    {
                                                        // SE E' PREMUTO IL CONTROL MOSTRO IL CURSORE DI EDIT-MARKOUT
                                                        TLCursors.Set(enCursortType.EditMarkOut);
                                                    }
                                                    break;
                                                case MxpClip_HitTest_Info.audioNode:
                                                    {
                                                        if (Win32.CWin32.GetAsyncKeyState(Keys.ControlKey) &&
                                                            clip.IsSelected &&
                                                            canEditAudioNodes && 
                                                            clip.CanEditAudio)
                                                        {
                                                            TLCursors.Set(enCursortType.AudioNodeRemove);
                                                        }
                                                    }
                                                    break;
                                                default:
                                                    if (isClipCutting)
                                                    {
                                                        TLCursors.Set(enCursortType.Split);
                                                    }
                                                    else
                                                    {
                                                        if (clip.Type == enCMxpClipType.Audio && 
                                                            clip.IsSelected &&
                                                            Win32.CWin32.GetAsyncKeyState(Keys.ControlKey) &&
                                                            canEditAudioNodes &&
                                                            clip.CanEditAudio)
                                                        {
                                                            TLCursors.Set(enCursortType.AudioNodeAdd);
                                                        }
                                                        else if (clip.Type == enCMxpClipType.Video && 
                                                            Win32.CWin32.GetAsyncKeyState(Keys.ControlKey) &&
                                                            canDragClip)
                                                        {
                                                            TLCursors.Set(enCursortType.ClipAboutMove);
                                                        }
                                                        else
                                                        {
                                                            TLCursors.Set(enCursortType.Default);
                                                        }
                                                    }
                                                    break;
                                            }
                                        }
                                        break;
                                }
                            }

                            break;
                        }
                    default:
                        {
                            TLCursors.Set(enCursortType.Default);
                            break;
                        }


                    #endregion NELLE TRACKS
                }

                #endregion NO MOUSE BUTTON
            }
            else if (e.Button == MouseButtons.Left)
            {
                #region MOUSE LEFT BUTTON

                switch (FHitTest)
                {
                    case Mtx_HitTest_OBJ.MarkInHandle:
                        {
                            #region MARKIN-HANDLE

                            #if !NO_RECTIME_BAR
                            if (!FCanMarkOverRectime && (e.X > FRecTimeXpos))
                            {
                                MarkIn(FRecTimeXpos-1);
                                if (FCurrentEditingClip != null) FCurrentEditingClip.TimcodeIn = FMarkIn;
                                Refresh();
                            }
                            else
                            #endif
                            {
                                Actions_Add(MxpTimelineAction.isMarkingIn);
                                if (lastMouseTc < tcIn) lastMouseTc = tcIn;
                                MarkIn(lastMouseTc);
                                if (currentEditingClip != null) currentEditingClip.TimcodeIn = markIn;
                                Refresh();
                            }

                            #endregion MARKIN-HANDLE

                            break;
                        }
                    case Mtx_HitTest_OBJ.MarkOutHandle:
                        {
                            #region MARKOUT-HANDLE
                            
                            #if !NO_RECTIME_BAR
                            if (!FCanMarkOverRectime && (e.X > FRecTimeXpos))
                            {
                                MarkOut(FRecTimeXpos);
                                if (FCurrentEditingClip != null) FCurrentEditingClip.TimecodeOut = FMarkOut;
                                Refresh();
                            }
                            else
                            #endif
                            {
                                Actions_Add(MxpTimelineAction.isMarkingOut);
                                if (lastMouseTc > tcOut) lastMouseTc = tcOut;

                                MarkOut(lastMouseTc);
                                if (currentEditingClip != null) currentEditingClip.TimecodeOut = markOut;
                                Refresh();
                            }
                            
                            #endregion MARKOUT-HANDLE

                            break;
                        }
                    case Mtx_HitTest_OBJ.OnZoomBar:
                        {
                            #region ZOOMBAR

                            switch (FLastClickedOn_ZoomBar)
                            {
                                case CMxpTlZoomBar.KTL_ZOOMTRACK_HITTEST_OBJ.TrackSlider:
                                    {
                                        Actions_Add(MxpTimelineAction.isPanning);
                                        zoomBar.MoveZoomArea(FHoldPosition, e.Location);
                                        break;
                                    }
                                case CMxpTlZoomBar.KTL_ZOOMTRACK_HITTEST_OBJ.TrackSliderLeftHandle:
                                    {
                                        Actions_Add(MxpTimelineAction.isZooming);
                                        zoomBar.ZoomIn = zoomBar.PixToFrame(e.X);
                                        break;
                                    }
                                case CMxpTlZoomBar.KTL_ZOOMTRACK_HITTEST_OBJ.TrackSliderRightHandle:
                                    {
                                        Actions_Add(MxpTimelineAction.isZooming);
                                        zoomBar.ZoomOut = zoomBar.PixToFrame(e.X);
                                        break;
                                    }
                                default:
                                    {
                                        break;
                                    }
                            }

                            #endregion ZOOMBAR

                            break;
                        }
                    case Mtx_HitTest_OBJ.OnTracks:
                        {
                            // IL MOUSEDOWN E' STATO FATTO NELLE TRACKS

                            #region TRACKS

                            //FTLContainer.GetHitTest(new Point(e.X - FTLContainerBounds.Left, e.Y - FTLContainerBounds.Top));

                            switch (tracks.LastHitTested)
                            {
                                case Mxp_Tracks_HitTest_Info.None:
                                    {
                                        // IL MOUSEDOWN E' STATO FATTO FUORI DA UNA DELLE TRACCE
                                        // MUOVO IL CURSORE
                                        int pos = (!canSeekOverRectime && (e.X > FRecTimeXpos)) ? FRecTimeXpos : e.X;
                                        SetCursorPos(pos);
                                        break;
                                    }
                                case Mxp_Tracks_HitTest_Info.OnTrack:
                                    {
                                        // IL MOUSEDOWN E' STATO FATTO IN UNA DELLE TRACCE

                                        CMxpTrack trk = tracks.LastClickedTrak;

                                        if (trk != null)
                                        {
                                            MxpTrack_HitTest_Info httrk = trk.LastHitTested;

                                            switch (httrk)
                                            {

                                                case MxpTrack_HitTest_Info.clip:
                                                    // IL MOUSEDOWN E' STATO FATTO IN UNA CLIP

                                                    #region CLIP

                                                    CMxpClip clip = trk.LastClickedClip;

                                                    if (clip != null)
                                                    {
                                                        Point clip_p0 = new Point(trk.ClientBounds.X + clip.ClientBounds.X + tracks.ClientBounds.Left,
                                                                                  trk.ClientBounds.Y + clip.ClientBounds.Y + tracks.ClientBounds.Top);

                                                        if ((clip.LastHitTest & MxpClip_HitTest_Info.audioNode) == MxpClip_HitTest_Info.audioNode &&
                                                            clip.CanEditAudio)
                                                        {
                                                            #region SPOSTAMENTO AUDIO NODE

                                                            if (Win32.CWin32.GetAsyncKeyState(Keys.ControlKey))
                                                            {
                                                                TLCursors.Set(enCursortType.AudioNodeRemove);
                                                            }
                                                            else
                                                            {
                                                                if (clip.LastHitTestObject != null)
                                                                {
                                                                    TLCursors.Set(enCursortType.VericalMove);

                                                                    CAudioNodeInfo audionodeinfo = (CAudioNodeInfo)clip.LastHitTestObject;
                                                                    long tc = 0;
                                                                    float val = 1F;
                                                                    clip.GetAudioValueAt(e.X - clip_p0.X, e.Y - clip_p0.Y, out tc, out val);

                                                                    if (tc - clip.TimcodeIn < clip.ClippingIn)  tc = clip.TimcodeIn + clip.ClippingIn;
                                                                    if (tc - clip.TimcodeIn > clip.ClippingOut) tc = clip.TimcodeIn + clip.ClippingOut;

                                                                    clip.AudioInfo.ChangeNode(audionodeinfo, tc, val);
                                                                }
                                                            }

                                                            #endregion SPOSTAMENTO AUDIO NODE
                                                        }
                                                        else if ((clip.LastHitTest & MxpClip_HitTest_Info.leftHandle) == MxpClip_HitTest_Info.leftHandle)
                                                        {
                                                            #region EDIT MARKIN CON CONTROL PREMUTO

                                                            Actions_Add(MxpTimelineAction.isEditingClipIn);

                                                            if (!clip.IsSelected) clip.IsSelected = true;

                                                            if (CWin32.GetAsyncKeyState(Keys.ControlKey))
                                                            {
                                                                TLCursors.Set(enCursortType.EditMarkIn);

                                                                long tc = FCursor.PixToFrame(e.X);

                                                                // VALUTAZIONE MAGNET

                                                                CMagnetInfo mag = TT.GetMagnetInfo(tc, FCursor.PixToFrame(10) - ZoomIn, clip.GetGroup());

                                                                bool position_locked = false;

                                                                //if (mag.HasItems && mag.closest_item != null)
                                                                if (mag != null)
                                                                {
                                                                    tc = mag.mag_timecode;

                                                                    if (mag.closest_item != null)
                                                                    {
                                                                        if (mag.closest_item.Tag is CMxpClip)
                                                                        {
                                                                            if (((CMxpClip)mag.closest_item.Tag).ParentTrack.ID == clip.ParentTrack.ID)
                                                                            {
                                                                                tc += 1;
                                                                            }
                                                                            //CMxpClip mag_clip = (CMxpClip)mag.closest_item.Tag;
                                                                            //pnltest.Clip = mag_clip;
                                                                        }
                                                                    }
                                                                }

                                                                foreach (CMxpClip c in Tracks.Tracks[clip.ParentTrack.ID].Clips)
                                                                {
                                                                    if (c.ID != clip.ID)
                                                                    {
                                                                        if ((tc <= (c.ClippingOut + c.TimcodeIn) && tc >= (c.ClippingIn + c.TimcodeIn)) ||    // TC COMPRESO NELLA CLIP
                                                                            (tc <= (c.ClippingIn + c.TimcodeIn) && ((clip.TimcodeIn + clip.ClippingOut) > (c.TimcodeIn + c.ClippingOut)))) // TC SCAVALCA COMPLETAMENTE LA CLIP
                                                                        {
                                                                            //tc = clip.ClippingOut + clip.TimcodeIn + 1;
                                                                            position_locked = true;
                                                                            break;
                                                                        }
                                                                    }
                                                                }

                                                                // VALUTAZIONE MAGNET

                                                                if (!position_locked)
                                                                {
                                                                    if (!clip.IsFreeClip)
                                                                    {
                                                                        if (tc < clip.TimcodeIn) tc = clip.TimcodeIn;
                                                                        clip.ClippingIn = tc - clip.TimcodeIn;
                                                                    }
                                                                    else
                                                                    {
                                                                        clip.TimcodeIn = tc;
                                                                    }
                                                                }

                                                                //if (OnChangePosition != null) OnChangePosition(this, tc, true);                                            
                                                            }

                                                            #endregion EDIT MARKIN CON CONTROL PREMUTO
                                                        }
                                                        else if ((clip.LastHitTest & MxpClip_HitTest_Info.rightHandle) == MxpClip_HitTest_Info.rightHandle)
                                                        {
                                                            #region EDIT MARKOUT CON CONTROL PREMUTO

                                                            if (!clip.IsSelected) clip.IsSelected = true;

                                                            Actions_Add(MxpTimelineAction.isEditingClipOut);

                                                            if (CWin32.GetAsyncKeyState(Keys.ControlKey))
                                                            {
                                                                TLCursors.Set(enCursortType.EditMarkOut);

                                                                long tc = FCursor.PixToFrame(e.X);

                                                                // VALUTAZIONE MAGNET

                                                                bool position_locked = false;

                                                                CMagnetInfo mag = TT.GetMagnetInfo(tc, FCursor.PixToFrame(10) - ZoomIn, clip.GetGroup());

                                                                if (mag != null)
                                                                {
                                                                    tc = mag.mag_timecode;

                                                                    if (mag.closest_item != null)
                                                                    {
                                                                        if (mag.closest_item.Tag is CMxpClip)
                                                                        {
                                                                            if (((CMxpClip)mag.closest_item.Tag).ParentTrack.ID == clip.ParentTrack.ID)
                                                                            {
                                                                                tc -= 1;
                                                                            }
                                                                            //CMxpClip mag_clip = (CMxpClip)mag.closest_item.Tag;
                                                                            //pnltest.Clip = mag_clip;
                                                                        }
                                                                    }
                                                                }
                                                                

                                                                foreach (CMxpClip c in Tracks.Tracks[clip.ParentTrack.ID].Clips)
                                                                {
                                                                    if (c.ID != clip.ID)
                                                                    {
                                                                        if ((tc >= (c.ClippingIn + c.TimcodeIn) && tc <= (c.ClippingOut + c.TimcodeIn)) || // TC COMPRESO NELLA CLIP
                                                                            (tc >= (c.ClippingOut + c.TimcodeIn) && (clip.TimcodeIn + clip.ClippingIn) < (c.TimcodeIn + c.ClippingIn))) // CLIP A CAVALLO
                                                                        {
                                                                            position_locked = true;
                                                                            break;
                                                                        }
                                                                    }
                                                                }

                                                                // VALUTAZIONE MAGNET

                                                                if (!position_locked)
                                                                {
                                                                    if (!clip.IsFreeClip)
                                                                    {
                                                                        if (tc > clip.TimecodeOut) tc = clip.TimecodeOut;
                                                                        clip.ClippingOut = tc - clip.TimcodeIn;
                                                                    }
                                                                    else
                                                                    {
                                                                        clip.TimecodeOut = tc;
                                                                    }
                                                                }
                                                                //if (OnChangePosition != null) OnChangePosition(this, tc, true);
                                                            }


                                                            #endregion EDIT MARKOUT CON CONTROL PREMUTO
                                                        }
                                                        else
                                                        {
                                                            if (CWin32.GetAsyncKeyState(Keys.ControlKey) && canDragClip)
                                                            {
                                                                if (clip.Type == enCMxpClipType.Video || clip.Group.Count == 0)
                                                                {
                                                                    #region SPOSTAMENTO CLIP CON CONTROL PREMUTO

                                                                    TLCursors.Set(enCursortType.ClipMove);

                                                                    // CONTROL PREMUTO
                                                                    // MUOVO LA POSIZIONE DELLA CLIP

                                                                    clip.IsMoving = true;
                                                                    clip.Visible = false;
                                                                    dragging_clip = clip;

                                                                    undoManager.TakeSnapshot();

                                                                    CMxpTL_ClipInfo clip_info   = clip.ClipInfo;
                                                                    clip_info.drag_point_x      = clip.LastHitXPosition.X - clip.X;
                                                                    this.DoDragDrop(clip_info, DragDropEffects.Move);

                                                                    #endregion SPOSTAMENTO CLIP CON CONTROL PREMUTO
                                                                }
                                                            }
                                                            else
                                                            {
                                                                // MUOVO IL CURSORE
                                                                int pos = e.X;
                                                                if(!canSeekOverRectime && (e.X > FRecTimeXpos)) pos =  FRecTimeXpos;
                                                                if (m_CanSeekOverClip)
                                                                {
                                                                    SetCursorPos(pos);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    
                                                    #endregion CLIP

                                                    break;

                                                case MxpTrack_HitTest_Info.audio0track:

                                                    break;

                                                case MxpTrack_HitTest_Info.audio0cursor:

                                                    #region SPOSTAMENTO VALORE 0 DELL'AUDIO

                                                    {
                                                        CMxpClip clipref = trk.Audio0bar_owner;

                                                        Point clip_p0 = new Point(trk.ClientBounds.X + clipref.ClientBounds.X + tracks.ClientBounds.Left,
                                                                                  trk.ClientBounds.Y + clipref.ClientBounds.Y + tracks.ClientBounds.Top);

                                                        TLCursors.Set(enCursortType.VericalMove);

                                                        CAudioNodeInfo aNodeInfo = (CAudioNodeInfo)clipref.LastHitTestObject;
                                                        float val   = 1F;
                                                        long tc     = 0;
                                                        clipref.GetAudioValueAt(0, e.Y - clip_p0.Y, out tc, out val);
                                                        clipref.AudioInfo.Main0gain = val;
                                                    }

                                                    #endregion SPOSTAMENTO VALORE 0 DELL'AUDIO

                                                    break;

                                                default:
                                                    {
                                                        // MUOVO IL CURSORE
                                                        //FCursor.PixelPosition = (!FCanSeekOverRectime && (e.X > FRecTimeXpos)) ? FRecTimeXpos : e.X;
                                                        int pos = (!canSeekOverRectime && (e.X > FRecTimeXpos)) ? FRecTimeXpos : e.X;
                                                        SetCursorPos(pos);
                                                    }
                                                    break;
                                            }
                                        }
                                        break;
                                    }
                            }

                            #endregion TRACKS

                            break;
                        }
                    case Mtx_HitTest_OBJ.GridBar:
                        {
                            SetCursorPos(e.X);
                            break;
                        }
                }
            }
            
            #endregion MOUSE LEFT BUTTON
        }

        private void MxpTL_MouseWheel(object sender, MouseEventArgs e)
        {
            Actions_Add(MxpTimelineAction.isZooming);

            Point mpos = Cursor.Position;
            mpos = this.PointToClient(mpos);

            long old_duration = zoomOut - zoomIn;

            int d = -(e.Delta / 40);

            long zoomSize   = zoomOut - zoomIn;
            long changeSize = (5 * zoomSize / 100) * d;

            if (changeSize == 0) changeSize = 1;

            long newSize = zoomSize + changeSize;
            
            long newIn  = zoomIn - changeSize;
            long newOut = zoomOut + changeSize;

            long new_duration = newOut - newIn;

            if (this.ClientRectangle.Contains(mpos))
            {
                // SE IL MOUSE E' SOPRA L'AREA DEL CONTROLLO ZOOM CENTRANDO LA VISTA SULLA POSIZIONE DEL MOUSE

                long tc0        = FCursor.PixToFrame(0);
                long mouse_tc   = FCursor.PixToFrame(mpos.X);
                long tc_diff    = mouse_tc - tc0;

                long new_in = tc_diff * new_duration / old_duration;
                new_in = mouse_tc - new_in; 

                if (new_in < 0) new_in = 0;
                                
                newIn   = new_in;
                newOut  = newIn + new_duration;
                
                if (newOut > tcOut) newOut = tcOut;
            }

            // ASSEGNO LE MODIFICHE

            //IsResizing = true;

            zoomBar.ZoomIn      = newIn;
            zoomBar.ZoomOut     = newOut;

            zoomIn              = zoomBar.ZoomIn;
            zoomOut             = zoomBar.ZoomOut;
            tracks.SetZoom(zoomIn, zoomOut);

            Actions_Remove(null, MxpTimelineAction.isZooming);

        }
        
        private void MxpTL_DragOver(object sender, DragEventArgs e)
        {
            lastDraggingMagInfo = null;

            if (OnAdvanceDraggingEvent != null)
            {
                Point local                 = this.PointToClient(Cursor.Position);
                bool cancel                 = false;
                int[] excluded_tracks       = new int[0];
                long rapresentation_len     = 0;
                CMxpTrack trk               = tracks.GetTrackAt(local.X, local.Y);
                CMxpTL_ClipInfo clip_info   = null;

                if (e.Data.GetData(typeof(CMxpTL_ClipInfo)) != null)
                {
                    clip_info = (CMxpTL_ClipInfo)e.Data.GetData(typeof(CMxpTL_ClipInfo));
                }

                OnAdvanceDraggingEvent(this, e.Data, trk, ref excluded_tracks, ref rapresentation_len, ref cancel);

                if (!cancel && trk != null)
                {
                    int width = FGrid.FrameInPix(rapresentation_len);

                    List<int> excluded_trks = new List<int>(excluded_tracks);

                    if (!excluded_tracks.Contains(trk.ID))
                    {
                        int x_diff = (clip_info != null) ? clip_info.drag_point_x : 0;
                        long tc_diff = FCursor.PixInFrame(x_diff);

                        long tcin = FCursor.PixToFrame(local.X - x_diff);

                        Console.WriteLine(" >> TCIN 0 : " + tcin + " - local.X : " + local.X + " - x_diff : " + x_diff);

                        #region VALUTO IL MAGNETISMO CON ALTRI OGGETTI
                        
                        CMagnetInfo mag_in  = TT.GetMagnetInfo(tcin, FCursor.PixInFrame(10), new object[0]);
                        CMagnetInfo mag_out = TT.GetMagnetInfo(tcin + rapresentation_len, FCursor.PixInFrame(10), new object[0]);

                        if (mag_in != null && mag_out != null)
                        {
                            if (mag_in.mag_distance < mag_out.mag_distance)
                            {
                                if (mag_in.mag_distance < mag_in.request_margin)
                                {
                                    tcin = mag_in.mag_timecode + 1;
                                    local.X = FGrid.FrameToPix(tcin) + x_diff;
                                    mag_in.mag_x = local.X;
                                    lastDraggingMagInfo = mag_in;
                                    lastDraggingMagInfo.track = trk;
                                }
                            }
                            else
                            {
                                if (mag_out.mag_distance < mag_out.request_margin)
                                {
                                    tcin = mag_out.mag_timecode - rapresentation_len - 1;
                                    local.X = FGrid.FrameToPix(tcin) + x_diff;
                                    mag_out.mag_x = local.X;
                                    lastDraggingMagInfo = mag_out;
                                    lastDraggingMagInfo.track = trk;
                                }
                            }
                        }
                        //else if (mag_in.HasItems)
                        else if (mag_in != null)
                        {
                            tcin = mag_in.mag_timecode + 1;
                            if (x_diff < 0) x_diff = 0;
                            local.X = FGrid.FrameToPix(tcin) + x_diff;
                            mag_in.mag_x = local.X;
                            lastDraggingMagInfo = mag_in;
                            lastDraggingMagInfo.track = trk;
                        }
                        //else if (mag_out.HasItems)
                        else if (mag_out != null)
                        { 
                            tcin = mag_out.mag_timecode - rapresentation_len - 1;
                            local.X = FGrid.FrameToPix(tcin) + x_diff;
                            mag_out.mag_x = local.X;
                            lastDraggingMagInfo = mag_out;
                            lastDraggingMagInfo.track = trk;
                        }
                        
                        #endregion VALUTO IL MAGNETISMO CON ALTRI OGGETTI

                        if (tcin < FCursor.PixInFrame(10))
                        {
                            tcin = 0;
                            dragging_control.TimecodeIn = tcin;
                            dragging_control.TimecodeOut = tcin + rapresentation_len;
                            dragging_control.SetBounds(0, trk.Y + tracks.Y, width, trk.Height);
                            Console.WriteLine(" >> TCIN 1 : " + tcin);
                            dragging_control.Visible = true;
                        }

                        if (TT.CheckForSpace(tcin, tcin + rapresentation_len, trk.ID))
                        {
                            // VALUTO LA COLLISIONE CON ALTRI OGGETTI CON L'EVENTUALE NUOVO TC
                            // DATO DAL MAGNET

                            e.Effect                        = DragDropEffects.All;
                            dragging_control.TimecodeIn     = tcin;
                            dragging_control.TimecodeOut    = tcin + rapresentation_len;
                            dragging_control.SetBounds(local.X - x_diff, trk.Y + tracks.Y, width, trk.Height);
                            Console.WriteLine(" >> TCIN 2 : " + tcin);
                            dragging_control.Visible        = true;
                        }
                        else
                        {
                            e.Effect = DragDropEffects.None;
                            dragging_control.Visible = false;
                        }

                    }
                    else
                    {
                        e.Effect = DragDropEffects.None;
                        dragging_control.Visible = false;
                    }
                }
                else
                {
                    e.Effect = DragDropEffects.None;
                    dragging_control.Visible = false;
                }
            }
        }
        
        private void MxpTL_DragDrop(object sender, DragEventArgs e)
        {            
            Point real_drop = new Point(e.X, e.Y);

            Point local = this.PointToClient(Cursor.Position);
            CMxpTrack trk = tracks.GetTrackAt(local.X, local.Y);

            if (lastDraggingMagInfo != null)
            {
                Point pxc = PointToScreen(new Point(lastDraggingMagInfo.mag_x, e.Y));
                real_drop.X = pxc.X;
            }

            if (dragging_clip != null)
            {
                // IL DROP E' DI UNA CLIP GIA' NELLA TIMELINE

                dragging_clip.Set_NewStartAt(dragging_control.TimecodeIn);


                trk.AddClip(dragging_clip);

                if (dragging_clip.Group.Count > 0)
                {
                    tracks[trk.ID + 1].AddClip(dragging_clip.Group[0]);
                }


                foreach (CMxpClip gclip in dragging_clip.GetGroup())
                {
                    gclip.Visible = true;
                }
            }
            else
            {
                if (OnAdvenceDragDrop != null) OnAdvenceDragDrop(this, e, real_drop);
            }

            if (dragging_control.Visible)
            {
                dragging_control.Visible = false;
            }

            dragging_clip = null;
        }

        private void MxpTL_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {
            if (e.Action == DragAction.Drop)
            {
                if ((lastDragEffect & DragDropEffects.Move) == DragDropEffects.Move ||
                    (lastDragEffect & DragDropEffects.Copy) == DragDropEffects.Copy)
                {
                    // IL DROP VA A BUON FINE
                }
                else
                {
                    if (dragging_clip != null) dragging_clip.Visible = true;
                    dragging_control.Hide();
                }
            }
        }
        
        private void MxpTL_GiveFeedback(object sender, GiveFeedbackEventArgs e)
        {
            lastDragEffect = e.Effect;
        }

        private void MxpTL_MouseLeave(object sender, EventArgs e)
        {
        }



        private void Render()
        {
            tracks.Render();
            FGrid.Render();
            zoomBar.Render();
        }

        private void RefreshAudioClips()
        {
            foreach (CMxpClip audio_clip in tracks.AllAudioClips)
            {
                if (audio_clip.IsVisible && audio_clip.Group.Count == 0)
                {
                    audio_clip.RefreshDisplayedInfo();
                    audio_clip.NotifyDisplayedInfoChanged();

                    Console.WriteLine("tmrDelayedExitFromZoomMW_Elapsed");
                }
            }
        }


        protected override void OnResize(EventArgs e)
        {
            if (zoomBar != null)
            {
                //FZoomScroll.Width = Width;
                zoomBar.ClientBounds = new Rectangle(0, this.Height - zoomBar.Height - 1, Width, zoomBar.Height);
            }
            
            if (tracks != null)
            {
                int yRec = 0;
                
                #if !NO_RECTIME_BAR
                yRec = FRecTimeHeight;
                #endif

                tracks.ClientBounds = new Rectangle(0, FGrid.Height + yRec, Width, Height - FGrid.Height - zoomBar.Height - yRec);
            }

            if (FGrid != null)
            {
                FGrid.ClientBounds = new Rectangle(0, 0, Width, FGrid.Height);
            }

            base.OnResize(e);
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (isFirstPaint)
            {
                isFirstPaint = false;

                zoomBar.ClientBounds = new Rectangle(0, this.Height - zoomBar.Height - 1, Width, zoomBar.Height);
            }

            #if !NO_RECTIME_BAR
            if (FRecTimeHeight > 0)
            {
                e.Graphics.FillRectangle(new SolidBrush(tracks.BackColor), 0, FGrid.Height, Width, FRecTimeHeight);
            }
            #endif

            //FGridBounds         = new Rectangle(0, 0, Width, FGrid.Height);

            e.Graphics.DrawImage(tracks.Image, tracks.ClientBounds);            
            e.Graphics.DrawImage(zoomBar.Image, zoomBar.ClientBounds);// 0, zoomBar.Y);// Height - zoomBar.Height);            
            e.Graphics.DrawImage(FGrid.Image, 0, 0);

            FGrid.IsChanged = false;

            //if (System.Diagnostics.Process.GetCurrentProcess().ProcessName != "devenv")
            //e.Graphics.DrawString("DESIGNTIME", Font, Brushes.Black, new Point(0, 0));

            // -----------------------------------------------------------------
            // DISEGNO LA BARRA DI REGISTRAZIONE

            
            #if !NO_RECTIME_BAR
            if (FRecTime > 0 && FRecTime > FZoomIn)
            {
                FRecTimeXpos = FGrid.FrameToPix(FRecTime);
                Color RecBarColor = Color.FromArgb(140, FRecTimeColor.R, FRecTimeColor.G, FRecTimeColor.B);
                e.Graphics.FillRectangle(new SolidBrush(RecBarColor), 0, FGrid.Height + 5, FRecTimeXpos, FRecTimeHeight);
            }
            #endif

            if (VirtualObject.Width > 0 && VirtualObject.Height > 0)
            {
                Console.WriteLine("RENDER VIRTUAL");
                e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(150, 40, 40, 40)), VirtualObject);
            }


            // -----------------------------------------------------------------
            // DISEGNO LA MARCATURA

            if (isMarking)
            {
                int XIN     = FGrid.FrameToPix(markIn);
                int XOUT    = FGrid.FrameToPix(markOut);

                BuildMarkHandle(new Point(XIN, 0), new Point(XOUT, 0));

                e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(150, 90, 200, 90)), XIN, FGrid.Height, XOUT - XIN, Height - FGrid.Height - zoomBar.Height - 1);
                e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(150, 200, 90, 90)), XIN, 0, XOUT - XIN, FGrid.Height);
                e.Graphics.FillPath((m_canModifyMarkerByGUI) ? Brushes.Yellow : Brushes.Silver, FGPMarkIn);
                e.Graphics.FillPath((m_canModifyMarkerByGUI) ? Brushes.Yellow : Brushes.Silver, FGPMarkOut);

                string SizeStr = CTcTools.Frame2TC(markOut - markIn, fps);
                SizeF SizeStrMis = e.Graphics.MeasureString(SizeStr, Font);

                e.Graphics.DrawString(SizeStr, Font, Brushes.Black, XIN + ((XOUT - XIN) / 2) - ((int)SizeStrMis.Width / 2), 25);



                #region DRAW MARKIN & MARKOUT

                int TC_INOUT_Y = 35;

                string TCI_Str = CTcTools.Frame2TC(markIn, fps);
                string TCO_Str = CTcTools.Frame2TC(markOut, fps);
                bool bg_in = true;
                bool bg_out = true;

                int tci_x = (int)(XIN - SizeStrMis.Width- 10);
                if (tci_x < 0)
                {
                    bg_in = false;
                    tci_x = XIN + 1;
                    TCI_Str = "< " + TCI_Str;
                }
                else
                {
                    TCI_Str = TCI_Str + " <"; 
                }

                int tco_x = (int)(XOUT + 10);
                if ((int)(tco_x + SizeStrMis.Width) > Width)
                {
                    bg_out = false;
                    tco_x = (int)(XOUT - SizeStrMis.Width);
                    TCO_Str = TCO_Str + "> ";
                }
                else
                {
                    TCO_Str = "< " + TCO_Str; 
                }

                // TCIN
                if(bg_in) e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(150, Color.LightGreen)), tci_x, TC_INOUT_Y, SizeStrMis.Width + 2, SizeStrMis.Height);
                e.Graphics.DrawString(TCI_Str, Font, Brushes.Black, tci_x, TC_INOUT_Y);

                if (tco_x < (tci_x + SizeStrMis.Width)) TC_INOUT_Y += 15;   // SE SONO SOVRAPPOSTI ABBASSO IL TCOUT

                int prefixSpace = (int)e.Graphics.MeasureString("< ", Font).Width;

                // TCOUT
                if (bg_out) e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(150, Color.LightGreen)), tco_x + prefixSpace, TC_INOUT_Y, SizeStrMis.Width + 2, SizeStrMis.Height);
                e.Graphics.DrawString(TCO_Str, Font, Brushes.Black, tco_x, TC_INOUT_Y);

                #endregion DRAW MARKIN & MARKOUT
            }

            // -----------------------------------------------------------------
            // DISEGNO IL CURSORE

            e.Graphics.DrawImage(FCursor.Image, new Point(FCursor.PixelPosition + FCursor.CenterDiff, 0));

            e.Graphics.DrawLine(new Pen(Brushes.Red, 1),
                                new Point(FCursor.PixelPosition, FGrid.Height),
                                new Point(FCursor.PixelPosition, Height - zoomBar.Height));
            
            // -----------------------------------------------------------------
            // DISEGNO LO STATO DEL FOUCUS

            if (FFocused && FShowFocus && showFocusBorder)
            {
                e.Graphics.DrawRectangle(new Pen(new SolidBrush(FFocusedColor), 1), e.ClipRectangle.X, e.ClipRectangle.Y, e.ClipRectangle.Width-1, e.ClipRectangle.Height-1);
            }
        }




        #region PUBLIC METHODS

        public CMxpTrack AddTrack(string trackTitle, object Tag)
        {
            CMxpTrack tl = tracks.AddTrack(trackTitle, enMxpTrackType.none, Tag);
            return tl;
        }

        public Mtx_HitTest_OBJ GetHitTest(Point p)
        {
            if (isMarking && m_canModifyMarkerByGUI)
            {
                if (FGPMarkIn.GetBounds().Contains(p)) return Mtx_HitTest_OBJ.MarkInHandle;
                else if (FGPMarkOut.GetBounds().Contains(p)) return Mtx_HitTest_OBJ.MarkOutHandle;
            }
            if (zoomBar.ClientBounds.Contains(p)) return Mtx_HitTest_OBJ.OnZoomBar;
            else if (FGrid.ClientBounds.Contains(p)) return Mtx_HitTest_OBJ.GridBar;
            else if (tracks.ClientBounds.Contains(p)) return Mtx_HitTest_OBJ.OnTracks;
            return Mtx_HitTest_OBJ.None;
        }

        public long GetTimecodeAt(int x)
        {
            return FCursor.PixToFrame(x);
        }

        public void DeselectAllClip()
        {
            for (int t = 0; t < tracks.Count; t++)
            {
                CMxpTrack line = tracks[t];

                for (int i = 0; i < line.ClipCount; i++)
                {
                    if (line[i] != null && line[i].IsSelected)
                    {
                        line[i].IsSelected = false;
                    }
                }
            }
        }
        
        /// <summary>
        /// Marca il punto di marcatura di ingresso, sulla posizione attuale
        /// </summary>
        public void MarkIn()
        {
            MarkIn(FCursor.FramePosition);
        }

        /// <summary>
        /// Marca il punto di marcatura di ingresso, al frame specificato
        /// </summary>
        public void MarkIn(long Frame)
        {
            isMarking = true;
            markIn = Frame;
            if (markOut <= Frame) markOut = Frame + 1;
            Refresh();
            if (OnChangeMarker != null) OnChangeMarker(this, markIn, markOut, enTcChangeType.timecode_in);
        }

        /// <summary>
        /// Marca il punto di marcatura di uscita, al frame specificato
        /// </summary>
        public void MarkOut()
        {
            MarkOut(FCursor.FramePosition);
        }

        /// <summary>
        /// Marca il punto di marcatura di uscita, sulla posizione attuale
        /// </summary>
        public void MarkOut(long Frame)
        {
            isMarking = true;
            markOut = Frame;

            if (Frame <= markIn)
            {
                if (Frame >= 1)
                {
                    markIn = Frame - 1;
                }
                else
                {
                    markIn = 0;
                    markOut = 1;
                }
            }
            Refresh();
            if (OnChangeMarker != null) OnChangeMarker(this, markIn, markOut, enTcChangeType.timecode_out);
        }

        public void HideMarks()
        {
            isMarking  = false;
            markIn     = -1;
            markOut    = -1;

            if (OnChangeMarker != null) OnChangeMarker(this, markIn, markOut, enTcChangeType.both);
            Refresh();
        }

        public CMxpClip CreateClipOnTrack(int IdTrack)
        {
            if (isMarking && tracks.Count > IdTrack)
            {
                isMarking = false;
                return tracks[IdTrack].AddFreeClip(markIn, markOut, enCMxpClipType.Video);
            }
            return null;
        }

        public void ZoomAll()
        {
            blockPositionChangeMsg = true;

            ZoomIn  = 0;
            ZoomOut = TCOUT;

            blockPositionChangeMsg = false;
        }

        public void SetZoom(long zoomin, long zoomout)
        {
            if (zoomin < zoomout && zoomin >= 0 && zoomout <= TCOUT)
            {
                this.zoomIn     = zoomin;
                this.zoomOut    = zoomout;

                zoomBar.ZoomIn  = zoomin;
                zoomBar.ZoomOut = zoomout;
                tracks.ZoomIn   = zoomin;
                tracks.ZoomOut  = zoomout;

                Render();
            }
        }

        public bool IsOnAction(MxpTimelineAction act)
        {
            return (actions & act) > 0;
        }
         
        /// <summary>
        /// Interrompe il processo di Editing di una Clip se attivo
        /// </summary>
        /// <returns>Id della clip appena chiusa</returns>
        public void StopEditing()
        {
            if (isEditingClip)
            {
                bool Cancel = false;

                if (OnEndingClipEditing != null) OnEndingClipEditing(this, currentEditingClip, ref Cancel);

                if (!Cancel)
                {
                    isEditingClip = false;
                    currentEditingClip.Edit();
                    //int idclip = -1;
                    //try
                    //{
                    //    idclip = FCurrentEditingClip.ID;
                    //}
                    //catch
                    //{

                    //}
                    markIn = -1;
                    markOut = -1;
                    Render();
                    currentEditingClip = null;

                    //return idclip;
                }
            }

            //return -1;
        }
        
        public string SaveTimelineScript()
        {
            return SaveProject("");
        }

        public string SaveProject(string fileName)
        {
            return "";
        }

        public void LoadProject(string filename)
        {
            
        }

        public void ClearAll()
        {
            Tracks.Clear(true);
        }

        public void ClearAllClips()
        {
            Tracks.Clear(false);
        }
        
        public void InitializeInternalEngine()
        {
            engine = new Engine.CTimeLine_Engine(this);
            engine.OnEventHandle += new Engine.CTimeLine_Engine.Engine_OnEventHandle(engine_OnEventHandle);
        }

        public void Undo()
        {
            undoManager.Undo();

            foreach (CMxpClip clip in this.Tracks.AllClips)
            {
                clip.NotifyRender();
            }
        }

        public void SaveUndo()
        {
            #if DEBUG
            Console.WriteLine("TAKING UNDO SNAPSHOT");
            #endif

            undoManager.TakeSnapshot();
        }

        public void Redo()
        {
            undoManager.Redo();
        }

        public void CLIP_CUTTER()
        {
            isClipCutting = true;
        }

        public void SetCurrentPositionAsExternal(long position)
        {
            if (position < 0) position = 0;
            if (position > tcOut) position = tcOut;
            
            FLastSettedFrame = -1;
            FCursor.FramePosition = position;
            Refresh();
        }

        public CUndoManager Get_UndoManager()
        {
            return this.undoManager;
        }

        public void Request_PlayMarked()
        {
            if (OnRequestPlaymarked != null) OnRequestPlaymarked(this);
        }

        public void HideDragObject()
        {
            if (dragging_control.Visible) dragging_control.Hide();
        }

        public CTimeTable Get_TimetabeObject()
        {
            return this.TT;
        }

        public void MovePreviousStep()
        {
            long step = 0;
            
            if (IsMarked)
            {
                //CurrentPosition = CurrentMarkIn;
                SetCurrentPositionAsExternal(CurrentMarkIn);
            }
            else
            {
                foreach (CMxpClip clip in tracks.AllClips)
                { 
                    long clip_start = clip.TimcodeIn + clip.ClippingIn;

                    if (clip_start > step && clip_start < CurrentPosition)
                    {
                        step = clip_start;
                    }
                }

                //CurrentPosition = step;
                SetCurrentPositionAsExternal(step);
            }
        }

        public void MoveNextStep()
        {
            long step = tcOut;

            if (IsMarked)
            {
                //CurrentPosition = CurrentMarkOut;
                SetCurrentPositionAsExternal(CurrentMarkOut);
            }
            else
            {
                foreach (CMxpClip clip in tracks.AllClips)
                {
                    long clip_end = clip.TimcodeIn + clip.ClippingOut;

                    if (clip_end < step && clip_end > CurrentPosition)
                    {
                        step = clip_end;
                    }
                }

                //CurrentPosition = step;
                SetCurrentPositionAsExternal(step);
            }
        }

        #endregion PUBLIC METHODS


        #region INTERNAL ENGINE EVENTS

        private void engine_OnEventHandle(object Sender, Engine.InternEngineEventHandle e)
        {
            if (OnInternalEngineEvent != null) OnInternalEngineEvent(this, e);
        }

        #endregion INTERNAL ENGINE EVENTS

        
        #region XML_TOOLS

        private string XmlGetSingleValue(XmlNode parent, string name, string defaultvalue)
        {
            string result = defaultvalue;

            if (parent != null)
            {
                if (parent[name] != null)
                {
                    result = parent[name].InnerText;
                }
            }

            return result;
        }

        private long XmlGetSingleValue(XmlNode parent, string name, long defaultvalue)
        {
            long result = defaultvalue;

            if (parent != null)
            {
                if (parent[name] != null)
                {
                    long.TryParse(parent[name].InnerText, out result);
                }
            }

            return result;
        }

        private XmlNode CreateLineNode(XmlNode parent, string name, long duration)
        {
            XmlNode lineNode = parent.OwnerDocument.CreateNode(XmlNodeType.Element, "LINE", "");

            XmlAttribute att = parent.OwnerDocument.CreateAttribute("name");
            att.Value = name;
            lineNode.Attributes.Append(att);

            att = parent.OwnerDocument.CreateAttribute("duration");
            att.Value = duration.ToString();
            lineNode.Attributes.Append(att);

            parent.AppendChild(lineNode);
            return lineNode;
        }
        
        #endregion XML_TOOLS




        protected override void OnVisibleChanged(EventArgs e)
        {
            // INSTANZIO ALCUNI EVENTI DELLA FORM CHE MI CONTIENE

            if (Parent is Control)
            {
                Form f = this.FindForm();

                if (f != null)
                {
                    ((Form)f).ResizeEnd     += new EventHandler(MxpTmeLine_ResizeEnd);
                    ((Form)f).ResizeBegin   += new EventHandler(MxpTmeLine_ResizeBegin);
                }
            }
            base.OnVisibleChanged(e);
        }





        protected override void WndProc(ref Message m)
        {
            switch ((uint)m.Msg)
            {
                case Engine.CTimeLine_Engine.TICK_MESSAGE:
                    {
                        // MESSAGGIO AGGIORNAMENTO TIMECODE IN ARRIVO DAL INTERNAL ENGINE
                        long tc = m.WParam.ToInt64();

                        if (tc <= this.TCOUT)
                        {
                            this.CurrentPosition = tc;

                            if (tc > zoomOut)
                            {
                                long zoom_size = zoomOut - zoomIn;

                                if (zoomOut + zoom_size > tcOut)
                                {
                                    zoomBar.SetZoom(tcOut - zoom_size, tcOut);
                                }
                                else
                                {
                                    zoomBar.SetZoom(zoomIn + zoom_size, zoomOut + zoom_size);
                                }
                            }
                        }
                        else
                        {
                            InternalEngine.Pause();
                        }
                        
                        break;
                    }
            }
            base.WndProc(ref m);
        }



        #region FORM CONTAINER EVENTS

        void MxpTmeLine_ResizeBegin(object sender, EventArgs e)
        {
            IsResizing = true;
        }

        void MxpTmeLine_ResizeEnd(object sender, EventArgs e)
        {
            IsResizing = false;
        }

        #endregion FORM CONTAINER EVENTS


        #region PUBLIC PROPERTIES
        
        [Browsable(true), Category("MXP_OPTIONS"), ReadOnly(true)]
        public int NumberOfLines
        {
            get { return tracks.NumberOfLines; }
        }

        public long CurrentPosition
        {
            get { return FCursor.FramePosition; }
            set
            {
                if (value >= 0 && value <= tcOut)
                {
                    FLastSettedFrame = value;
                    FCursor.FramePosition = value;
                    Refresh();
                }
            }
        }

        [Browsable(true), Category("MXP_OPTIONS")]
        public long ZoomIn
        {
            get { return zoomIn; }
            set
            {
                if (value < zoomOut)
                {
                    zoomIn              = value;
                    zoomBar.ZoomIn      = value;
                    tracks.ZoomIn       = value;
                    //foreach (clsSingleLine line in FTLines) line.ZoomIn = FZoomIn;
                }
                Render();
            }
        }

        [Browsable(true), Category("MXP_OPTIONS")]
        public long ZoomOut
        {
            get { return zoomOut; }
            set 
            {
                if(value > zoomIn)
                {
                    zoomOut             = value;
                    zoomBar.ZoomOut     = value;
                    tracks.ZoomOut      = value;
                    //foreach (clsSingleLine line in FTLines) line.ZoomOut = FZoomOut;
                }
                Render();
            }
        }

        [Browsable(true), Category("MXP_OPTIONS")]
        public long TCIN
        {
            get { return tcIn; }
            set
            {
                if (tcIn != value)
                {
                    tcIn = value;
                    zoomBar.TCin = value;
                    FGrid.TCin       = value;
                    Render();
                }
                Refresh();
            }
        }

        [Browsable(true), Category("MXP_OPTIONS")]
        public long TCOUT
        {
            get { return tcOut; }
            set
            {
                if (tcOut != value)
                {
                    tcOut            = value;

                    if (!this.DesignMode)
                    {
                        FCursor.TCOut = value;
                        zoomBar.TCOut = value;
                        FGrid.TCOut = value;
                        Render();
                    }
                }
                Refresh();
            }
        }

        [Browsable(true), Category("MXP_OPTIONS")]
        public bool AutoAdaptLines
        {
            get { return tracks.AutoSizeHeight; }
            set 
            {
                if (tracks.AutoSizeHeight != value)
                {
                    tracks.AutoSizeHeight = value;
                }
            }
        }

        [Browsable(true), Category("MXP_OPTIONS")]
        public int TrackHeight
        {
            get
            {
                return tracks.LineHeight;
            }

            set
            {
                tracks.LineHeight = value;
            }
        }

        [Browsable(true), Category("MXP_OPTIONS")]
        public Color TimeLineBackColor
        {
            get { return tracks.BackColor; }
            set { tracks.BackColor = value; }
        }

        [Browsable(true), Category("MXP_OPTIONS")]
        public Color TimeLineColor
        {
            get { return tracks.TrackBackColor; }
            set { tracks.TrackBackColor = value; }
        }

        [Browsable(true), Category("MXP_OPTIONS")]
        public long RecTime
        {
            get { return recTime; }
            set 
            {
                if (recTime != value)
                {
                    recTime = value;
                    Refresh();
                }
            }
        }

        [Browsable(true), Category("MXP_OPTIONS")]
        public bool CanSeekOverRectime
        {
            get { return canSeekOverRectime; }
            set { canSeekOverRectime = value; }
        }

        [Browsable(true), Category("MXP_OPTIONS")]
        public bool CanMarkOverRectime
        {
            get { return canMarkOverRectime; }
            set { canMarkOverRectime = value; }
        }

        [Browsable(true), Category("MXP_OPTIONS")]
        public bool CanSeekOverClip
        {
            get { return m_CanSeekOverClip; }
            set { m_CanSeekOverClip = value; }
        }

        [Browsable(true), Category("MXP_OPTIONS_MNU")]
        public ContextMenuStrip Menu_General
        {
            get { return FMnuGeneral; }
            set { FMnuGeneral = value; }
        }

        [Browsable(true), Category("MXP_OPTIONS_MNU")]
        public ContextMenuStrip Menu_Clips
        {
            get { return FMnuClip; }
            set { FMnuClip = value; }
        }

        [Browsable(true), Category("MXP_OPTIONS")]
        public Int32 RecTimeBarHeight
        {
            get 
            { 
                #if !NO_RECTIME_BAR
                return FRecTimeHeight; 
                #endif
                return 0;
            }
            set 
            {
                #if !NO_RECTIME_BAR
                FRecTimeHeight = value;
                #endif
            }
        }
                
        [Browsable(true), Category("Clips options")]
        public Color ClipColor
        {
            get { return tracks.ClipColor; }
            set { tracks.ClipColor = value; }
        }

        [Browsable(true), Category("Clips options")]
        public Color SelectedClipColor
        {
            get { return tracks.SelectedClipColor; }
            set { tracks.SelectedClipColor = value; }
        }
        
        [Browsable(true), Category("Zoom bar options")]
        public Color TrackColor
        {
            get { return zoomBar.TrackBarColor; }
            set { zoomBar.TrackBarColor = value; }
        }

        [Browsable(true), Category("Zoom bar options")]
        public Color TrackHandleColor
        {
            get { return zoomBar.TrackBarHandleColor; }
            set { zoomBar.TrackBarHandleColor = value; }
        }

        [Browsable(true), Category("Zoom bar options")]
        public Color TrackBackColor
        {
            get { return zoomBar.TrackBackColor; }
            set { zoomBar.TrackBackColor = value; }
        }

        [Browsable(true), Category("Zoom bar options")]
        public int ZoomBarHeight
        {
            get { return zoomBar.Height; }
            set { zoomBar.Height = value; }
        }
        
        [Browsable(true), Category("Time bar options")]
        public int TimeBarHeight
        {
            get { return FGrid.Height; }
            set 
            {
                FGrid.Height = value;
                tracks.Y = value;
                this.Refresh();
            }
        }

        [Browsable(true), Category("Zoom bar options")]
        public float ZoomLimitMin
        {
            get { return zoomBar.ZoomMinLimit; }
            set { zoomBar.ZoomMinLimit = value; }
        }

        [Browsable(true), Category("Options")]
        public int UndoTimes
        {
            get { return undoManager.UndoTimes; }
            set { undoManager.UndoTimes = value; }
        }

        [Browsable(true), Category("Tracks Header Control")]
        public Control TrackInfoControl_Container
        {
            get { return timeLineTrackInfo_Container; }
            set
            {
                if (timeLineTrackInfo_Container != value)
                {
                    timeLineTrackInfo_Container = value;

                    if (timeLineTrackInfo_Container != null)
                    {
                        if (trackinfo_control != null)
                        {
                            trackinfo_control.Dock              = DockStyle.Fill;
                            trackinfo_control.Parent            = value;
                        }
                        else
                        {
                            trackinfo_control                   = new MxpTL_TrackInfo();
                            trackinfo_control.Dock              = DockStyle.Fill;
                            trackinfo_control.Parent            = value;
                            trackinfo_control.TimeLineControl   = this;
                            trackinfo_control.CanDisableTracks  = canDisableTracks;
                        }
                    }
                }
            }
        }

        [Browsable(true), Category("Tracks Header Control")]
        public bool CanDisableTracks
        {
            get { return canDisableTracks; }
            set
            {
                if (canDisableTracks != value)
                {
                    canDisableTracks = value;

                    if (trackinfo_control != null)
                    {
                        trackinfo_control.CanDisableTracks = value;
                    }
                }
            }
        }

        [Browsable(true), Category("Audio")]
        public bool CanEditAudioNodes
        {
            get { return canEditAudioNodes; }
            set
            {
                if (canEditAudioNodes != value)
                {
                    canEditAudioNodes = value;
                }
            }
        }

        [Browsable(true), Category("Tracks Header Control")]
        public bool CanModifyMarkerByGUI
        {
            get { return m_canModifyMarkerByGUI; }
            set
            {
                if (m_canModifyMarkerByGUI != value)
                {
                    m_canModifyMarkerByGUI = value;
                    Refresh();
                }
            }
        }

        [Browsable(true), Category("Clip Options")]
        public bool CanDragClip
        {
            get { return canDragClip; }
            set
            {
                if (canDragClip != value)
                {
                    canDragClip = value;
                }
            }
        }

        [Browsable(true), Category("Timing options")]
        public float FPS
        {
            get { return fps; }
            set 
            {
                if (fps != value)
                {
                    fps = value;

                    Render();
                }
            }
        }

        [Browsable(false)]
        public CMxpClip SelectedClip
        {
            get { return selectedClip; }
        }

        [Browsable(false)]
        public CMxpTLTracks_Collection Tracks
        {
            get { return tracks; }
        }

        [Browsable(false)]
        public CMxpTrack LastClickedTrak
        {
            get
            {
                return tracks.LastClickedTrak;
            }
        }

        [Browsable(false)]
        public CMxpClip ClipOfContextMenu
        {
            get { return FClipOfContextMenu; }
            set { FClipOfContextMenu = value; }
        }

        [Browsable(false)]
        public MxpTimelineAction Actions
        {
            get { return actions; }
            
            private set
            {
                if (actions != value)
                {
                    MxpTimelineAction ex = actions;

                    actions = value;

                    if (OnChangeActiveActions != null) OnChangeActiveActions(this, actions);


                    //if ((ex & MxpTimelineAction.isPanning) > 0 && (actions & MxpTimelineAction.isPanning) == 0)
                    //{
                    //    tmrDelayedAction.Stop();
                    //    tmrDelayedAction.Start();
                    //}

                    //if ((actions & MxpTimelineAction.isPanning) > 0)
                    //{
                    //    tracks.IsResizing = true;
                    //}
                    //else
                    //{
                    //    tracks.IsResizing = false;  
                    //}
                }
            }
        }

        [Browsable(true)]
        public bool CanDragoutClips
        {
            get { return canDragoutClips; }
            set
            {
                canDragoutClips = value;
            }
        }

        [Browsable(true)]
        public bool ShowFocusBorder
        {
            get { return showFocusBorder; }
            set
            {
                showFocusBorder = value;
                Refresh();
            }
        }

        [Browsable(true)]
        public bool CanDragoutMarkers
        {
            get { return canDragoutMarkers; }
            set
            {
                canDragoutMarkers = value;
            }
        }
        
        [Browsable(true)]
        public bool FireDisplayClipEventOnPan
        {
            get { return m_FireDisplayClipEventOnPan; }
            set
            {
                m_FireDisplayClipEventOnPan = value;
            }
        }

        public long CurrentMarkIn
        {
            get { return markIn; }
        }

        public long CurrentMarkOut
        {
            get { return markOut; }
        }

        /// <summary>
        /// Restituisce True se sono attivi i punti di marcatura editabili..
        /// </summary>
        public bool IsMarked
        {
            get 
            {
                return (markIn >= 0 && markOut > 0);
            }
        }

        /// <summary>
        /// Restituisce True e si st� editando una clip esistente, altrimenti false
        /// </summary>
        public bool IsEditingClip
        {
            get { return isEditingClip; }
        }

        [Browsable(false)]
        public bool Focused
        {
            get { return FFocused; }
        }

        public CMxpClip[] LastestClips
        { 
            get
            {
                List<CMxpClip> clips = new List<CMxpClip>();

                foreach (CMxpTrack trk in tracks.Tracks)
                {
                    CMxpClip clip = trk.LastClip;

                    if (clip != null)
                    {
                        if (clips.Count > 0 && (clip.TimcodeIn + clip.ClippingOut >clips[0].TimcodeIn + clips[0].ClippingOut))
                        {
                            clips.Clear();
                            clips.Add(clip);
                        }
                        else if (clips.Count > 0 && (clip.TimcodeIn + clip.ClippingOut == clips[0].TimcodeIn + clips[0].ClippingOut))
                        {
                            clips.Add(clip);
                        }
                        else if (clips.Count == 0)
                        {
                            clips.Add(clip);
                        }
                    }
                }

                return clips.ToArray();
            }
        }

        [Browsable(true), Category("LAYOUT")]
        public bool ShowFocus
        {
            get { return FShowFocus; }
            set
            {
                FShowFocus = value;
                Refresh();
            }
        }

        [Browsable(true), Category("LAYOUT")]
        public Color ColorFocus
        {
            get { return FFocusedColor; }
            set
            {
                FFocusedColor = value;
                Refresh();
            }
        }

        public Engine.CTimeLine_Engine InternalEngine
        {
            get { return engine; }
        }

        #endregion PUBLIC PROPERTIES


        #region PRIVATE PROPERTIES

        private bool IsResizing
        {
            get { return isResizing; }
            set
            {
                if (isResizing != value)
                {
                    isResizing = value;

                    FGrid.IsResizing        = value;
                    zoomBar.IsResizing      = value;
                    tracks.IsResizing       = value;
                }
            }
        }

        #endregion PRIVATE PROPERTIES



        void tmrDelayedAction_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Internal_Render render = new Internal_Render(Render);

            this.Invoke(render);
            //Console.WriteLine("DELAYED RENDER");
            tmrDelayedAction.Stop();
        }
        
        void tmrDelayedCursorPositioningAction_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //Console.WriteLine("DELAYED SCRUB OFF");
            Actions_Remove(null, MxpTimelineAction.isScrubbing);
            tmrDelayedCursorPositioningAction.Stop();
        }

        void tmrDelayedExitFromZoomMW_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Internal_Render refreshAudioClips = new Internal_Render(RefreshAudioClips);

            tmrDelayedExitFromZoomMW.Stop();
            this.Invoke(refreshAudioClips);
        }



        public static string Frame2TC(long frame, float fps)
        {
            return CTcTools.Frame2TC(frame, fps);
        }
    }

}