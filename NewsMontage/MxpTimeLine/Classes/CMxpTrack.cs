using System;
using System.Collections.Generic;
using System.Text;

using System.Drawing;
using MediaAlliance.Controls.MxpTimeLine.Classes;
using System.Xml;
using MediaAlliance.Controls.MxpTimeLine.Objects;


namespace MediaAlliance.Controls.MxpTimeLine
{

    public enum MxpTrack_HitTest_Info
    {
        none            = 0,
        timeline        = 1,
        clip            = 2,
        audio0track     = 3,
        audio0cursor    = 4
    }

    public enum enMxpTrackType
    { 
        none        = 0,
        Audio       = 1,
        Video       = 2
    }

    public class CMxpTrack : CMxpTL_Object
    {
        // DELEGATES
        public delegate void MxpTrack_OnResize(object Sender, Size NewSize);
        public delegate void MxpTrack_OnRender(object Sender);
        public delegate void MxpTrack_OnSelectionChanged(object Sender, bool Selected);
        public delegate void MxpTrack_OnChangeClipSelection(object Sender, CMxpClip[] SelectedClips);
        public delegate void MxpTrack_OnEditClip(object Sender, CMxpClip clip, bool Start);
        public delegate void MxpTrack_OnClipAdded(object Sender, CMxpClip clip);
        public delegate void MxpTrack_OnClipRemoving(object Sender, CMxpClip clip);
        public delegate void MxpTrack_OnClipChangedTC(object Sender, CMxpClip clip, enTcChangeType cType);
        public delegate void MxpTrack_OnChangeOffState(object Sender, bool off);

        // EVENTS
        public event MxpTrack_OnResize OnResize                             = null;
        public event MxpTrack_OnRender OnRender                             = null;
        public event MxpTrack_OnSelectionChanged OnSelectionChanged         = null;
        public event MxpTrack_OnChangeClipSelection OnChangeClipSelection   = null;
        public event MxpTrack_OnEditClip OnEditClip                         = null;
        public event MxpTrack_OnClipAdded OnClipAdded                       = null;
        public event MxpTrack_OnClipRemoving OnClipRemoving                 = null;
        public event MxpTrack_OnClipChangedTC OnClipChangedTC               = null;

        public event MxpTrack_OnChangeOffState OnChangeOffState             = null;
        public event MxpClip_OnVirtualObjectNeed OnVirtualObjectNeed        = null;


        // GRAPHICAL PUBLIC PROPERTIES
        private Color videoTrackBackColor               = Color.FromArgb(80, 80, 80);
        private Color audioTrackBackColor               = Color.FromArgb(75, 75, 75);
        private Color selectedTrackBackColor            = Color.FromArgb(87, 78, 54);
        private Pen FTimeLinesPen                       = new Pen(Color.DimGray);
        private Color FClipColor                        = Color.LightSteelBlue;
        private Color FSelectedClipColor                = Color.LightSalmon;

        private MxpClipThumbLayout thumbLayout          = MxpClipThumbLayout.firstAndLast;

        // PRIVATE FIELDS
        private Int32 id                = 0;
        private long duration           = 0;
        private string title            = "";
        private object tag              = null;
        private string m_description    = "";

        private long zoomIn             = 0;
        private long zoomOut            = 0;
        private int pixUnit             = 0;
        private Image trackIcon         = null;


        private enMxpTrackType type = enMxpTrackType.none;

        private bool selected   = false;


        private System.Drawing.Drawing2D.GraphicsPath FSelectedMark = null;
        

        private CMxpClip FLastClickedClip = null;
        public MxpTrack_HitTest_Info LastHitTested = MxpTrack_HitTest_Info.none;
        public CMxpClip Audio0bar_owner = null;

        // GRAFICS VARIABLES
        private Pen FBorderPen = new Pen(Brushes.Black, 1);
        private Font FFont = new Font(FontFamily.GenericSansSerif, 8, FontStyle.Regular);
        

        private Bitmap FRendered = null;
        private System.Drawing.Graphics FGrf = null;

        private Bitmap off_picture = null;

        private bool off        = false;
        
        // CLASSES
        private CMxpClips_Collection clips = null;



        // CTor

        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public CMxpTrack()
        { 
        }

        public CMxpTrack(CMxpTL_Object parent) : base(parent)
        {
            this.type = enMxpTrackType.none;

            CommonInit();
        }

        public CMxpTrack(CMxpTL_Object parent, enMxpTrackType type) : base(parent)
        {
            this.type = type;

            CommonInit();
        }

        private void CommonInit()
        {
            // INIZIALIZZO TUTTE LE HITTEST AREA POSSIBILI

            foreach (MxpTrack_HitTest_Info ht in Enum.GetValues(typeof(MxpTrack_HitTest_Info)))
            {
                this.HitTest_Areas.Add(ht, new Rectangle());
            }



            clips = new CMxpClips_Collection();

            clips.OnItemAdded += new EventHandler<GenericItemEventArgs<CMxpClip>>(clips_OnItemAdded);
            clips.OnBeforeItemRemoved += new EventHandler<GenericItemEventArgs<CMxpClip>>(clips_OnBeforeItemRemoved);

            FTimeLinesPen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
        }

        //

        protected override void InternalChangeNotification(enInternalChangeNotificationType cType)
        {
            switch (cType)
            {
                case enInternalChangeNotificationType.none:
                    break;
                case enInternalChangeNotificationType.x:
                    break;
                case enInternalChangeNotificationType.y:
                    break;
                case enInternalChangeNotificationType.position:
                    break;
                case enInternalChangeNotificationType.width:
                    Resize(width, height);
                    if (OnResize != null) OnResize(this, new Size(width, height));
                    Render();
                    break;
                case enInternalChangeNotificationType.height:
                    Resize(width, height);
                    if (OnResize != null) OnResize(this, new Size(width, height));
                    GenerateSelectMark();
                    Render();
                    break;
                case enInternalChangeNotificationType.bound:
                    GenerateSelectMark();
                    break;
                case enInternalChangeNotificationType.isResizing:
                    foreach (CMxpClip clip in this.clips)
                    {
                        clip.IsResizing = this.IsResizing;
                    }
                    break;
                default:
                    break;
            }
        }
        
        public override string Serialize()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(Properties.Resources.CMxpTrack_Descriptor);

            doc["CMxpTrack"]["ID"].InnerText        = this.id.ToString();
            doc["CMxpTrack"]["Title"].InnerText     = this.title;
            doc["CMxpTrack"]["Type"].InnerText      = this.type.ToString();
            doc["CMxpTrack"]["Height"].InnerText    = this.height.ToString();

            foreach (CMxpClip clip in Clips)
            {
                doc["CMxpTrack"]["Clips"].InnerXml += clip.Serialize();
            }

            return doc.InnerXml;
        }

        public override void Deserialize(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            
            CMxpTLTracks_Collection tracks = (CMxpTLTracks_Collection)Parent;

            if (doc["CMxpTrack"]["Clips"] != null)
            {
                foreach (XmlNode clip_node in doc["CMxpTrack"]["Clips"].ChildNodes)
                {
                    if (clip_node["ID"] != null)
                    {
                        Guid clip_guid = new Guid(clip_node["ID"].InnerText);
                        bool found = false;

                        foreach (CMxpClip clip in Clips)
                        {
                            if (clip.ID == clip_guid)
                            {
                                clip.Deserialize(clip_node.OuterXml);
                                found = true;
                                break;
                            }
                        }

                        if (!found)
                        {
                            // CONTROLLO TRA LE CANCELLATE

                            for (int i = 0; i < tracks.DeletedClips.Count; i++)
                            {
                                CMxpClip deleted_clip = tracks.DeletedClips[i];

                                if(deleted_clip.ID == clip_guid)
                                {
                                    restoreClip(deleted_clip);
                                    found = true;
                                }
                            }
                        }

                        if (!found)
                        {
                            // CONTROLLO SU ALTRE TRACCE
                            
                            foreach (CMxpTrack track in tracks.Tracks)
                            {
                                if (track.id != id)
                                {
                                    CMxpClip clip_found = null;

                                    foreach (CMxpClip clip1 in track.clips)
                                    {
                                        if (clip1.ID == clip_guid)
                                        {
                                            found = true;

                                            clip_found = clip1;
                                            clip_found.Deserialize(clip_node.OuterXml);
                                            break;
                                        }
                                    }

                                    if (clip_found != null)
                                    {
                                        AddClip(clip_found);
                                        track.ConformClipAfterZoomChanged();
                                        track.Render();
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            this.ConformClipAfterZoomChanged();
            this.Render();
        }


        #region CLIPS COLLETION EVENTS

        void clips_OnItemAdded(object sender, GenericItemEventArgs<CMxpClip> e)
        {
            e.Item.OnChangeSelected     += new CMxpClip.MxpClip_OnChangeSelected(clip_OnChangeSelected);
            e.Item.OnChangeTitle        += new CMxpClip.MxpClip_OnChangeTitle(clip_OnChangeTitle);
            e.Item.OnDeleteClip         += new CMxpClip.MxpClip_OnDeleteClip(clip_OnDeleteClip);
            e.Item.OnRender             += new CMxpClip.MxpClip_OnRender(clip_OnClipRender);
            e.Item.OnEdit               += new CMxpClip.MxpClip_OnEdit(clip_OnEdit);
            e.Item.OnChangedTC          += new CMxpClip.MxpClip_OnChangedTC(clip_OnChangedTC);
            e.Item.OnVirtualObjectNeed  += new MxpClip_OnVirtualObjectNeed(clip_OnVirtualObjectNeed);
            e.Item.OnBeginEditing       += new MxpTL_OnBeginEditing(Item_OnBeginEditing);
            e.Item.OnEndEditing         += new MxpTL_OnEndEditing(Item_OnEndEditing);

            if (OnClipAdded != null) OnClipAdded(this, e.Item);
        }

        void clips_OnBeforeItemRemoved(object sender, GenericItemEventArgs<CMxpClip> e)
        {
            e.Item.OnChangeSelected     -= clip_OnChangeSelected;
            e.Item.OnChangeTitle        -= clip_OnChangeTitle;
            e.Item.OnDeleteClip         -= clip_OnDeleteClip;
            e.Item.OnRender             -= clip_OnClipRender;
            e.Item.OnEdit               -= clip_OnEdit;
            e.Item.OnChangedTC          -= clip_OnChangedTC;
            e.Item.OnVirtualObjectNeed  -= clip_OnVirtualObjectNeed;
        }

        #endregion CLIPS COLLETION EVENTS



        public void Render()
        {
            Audio0bar_owner = null;
            
            if (FRendered != null && FGrf != null)
            {
                if (selected)
                {
                    FGrf.Clear(selectedTrackBackColor);
                }
                else if (this.type == enMxpTrackType.Audio)
                {
                    FGrf.Clear(audioTrackBackColor);
                }
                else
                {
                    FGrf.Clear(videoTrackBackColor);
                }

                FGrf.DrawLine(FTimeLinesPen, 0, height - 1, width, height - 1);

                if (width > 0 && height > 0 && pixUnit != 0)
                {
                    CMxpClip audioSelectedClip = null;

                    for (int c = 0; c < clips.Count; c++)
                    {
                        CMxpClip clip = clips[c];


                        //clip.IsVisible = IsVisibledArea(clip.TimcodeIn, clip.TimecodeOut);     // AGGIORNO STATO DI VISIBILITA' DELLA CLIP
                        clip.IsVisible = IsVisibledArea(clip.TimcodeIn + clip.ClippingIn, clip.TimcodeIn + clip.ClippingOut); 


                        if (clip.IsVisible && clip.Visible)
                        {
                            int pixInDiff   = (Int32)(zoomIn % pixUnit);
                            int pixIn       = FrameToPix(clip.TimcodeIn + clip.ClippingIn);
                            int pixOut      = FrameToPix(clip.TimcodeIn + clip.ClippingOut);

                            if (clip.IsFreeClip)
                            {
                                pixIn   = FrameToPix(clip.TimcodeIn);
                                pixOut  = FrameToPix(clip.TimecodeOut);
                            }

                            clip.Virtual_X      = pixIn;

                            if (pixIn < -300)
                            {
                                // SE L'AREA DI DISEGNO ECCEDE DI 300 pixel DAL MARGINE INFERIORE
                                // ESCLUDO DAL PAINT L'AREA IN ECCESSO
                                clip.Virtual_X = pixIn;
                                clip.ShowTitle = false;
                                pixIn = -300;
                            }
                            //else
                            //{
                            //    clip.ShowTitle = true;
                            //}

                            clip.Virtual_Width = pixOut - clip.Virtual_X;

                            if (pixOut > width + 300)
                            {
                                pixOut = width + 300;
                            }

                            if (pixOut > Width + 40) pixOut = Width + 40;

                            //clip.Width  = pixOut - pixIn;
                            //clip.X      = pixIn;

                            clip.Width = pixOut - pixIn;

                            // AGGIORNO LA BOUNDS AREA DELLA CLIP IN DISEGNO
                            if (!isResizing)
                            {
                                // DISEGNO LA CLIP VERA E PROPRIA SE NON STO FACENDO RESIZE DEL CONTROLLO

                                //clip.SetSize(pixOut - pixIn, height);
                                clip.ClientBounds = new Rectangle(pixIn, 0, clip.Width, height);
                                                                
                                FGrf.DrawImage(clip.Image, 0 + clip.X, 0);


#if DEBUG
                                for (float x = clip.X; x < (clip.X + clip.Width); x += 40)
                                {
                                    int id_clip = this.clips.IndexOf(clip);
                                    FGrf.DrawString(id_clip.ToString(), this.FFont, Brushes.BlueViolet, x, 20f);
                                }
#endif

                                if (clip.Type == enCMxpClipType.Audio && clip.IsSelected)
                                {
                                    audioSelectedClip = clip;
                                }
                            }
                            else
                            {
                                // DISEGNO DIRETTAMENTE UN'AREA CORRISPONEDETE ALLA CLIP DURANTE LE OPERAIZONI DI RESIZE
                                // PER OTTIMIZZARE IL DISEGNA GRAFICO

                                SolidBrush br = new SolidBrush(clip.BackColor);

                                //if (clip.Type == enCMxpClipType.Video)
                                //{
                                //    br = new SolidBrush(clip.BackColor);
                                //}
                                //else
                                //{
                                //    br = new SolidBrush(clip.BackColor);
                                //}

                                FGrf.FillRectangle(br, clip.Virtual_X, 1, clip.Virtual_Width, height - 2);
                            }
                        }
                    }

                    if (audioSelectedClip != null) Render_AudioZeroTrack(audioSelectedClip);
                }

                if (selected && FSelectedMark != null)
                {
                    FGrf.FillPath(new SolidBrush(Color.Black), FSelectedMark);
                }

                if (off && !isResizing)
                {
                    try
                    {
                        FGrf.DrawImage(off_picture, 0, 0);
                    }
                    catch { }
                }

                if (OnRender != null) OnRender(this);
            }
        }



        private void Render_AudioZeroTrack(CMxpClip clip)
        {
            if (clip.CanEditAudio)
            {
                Audio0bar_owner = clip;

                Rectangle trak0 = new Rectangle(0, 0, 10, this.height - 1);

                long real_in = clip.TimcodeIn;
                long real_out = clip.TimecodeOut;

                if (!clip.IsFreeClip)
                {
                    real_in = clip.ClippingIn + clip.TimcodeIn;
                    real_out = clip.ClippingOut + clip.TimcodeIn;
                }

                int anY = height - (int)(height * clip.AudioInfo.Main0gain);// (int)half;

                bool leftside = false;

                if (clip.Virtual_X + clip.Width + 15 < width)
                {
                    // SE HO POSTO LO DISEGNO A DESTRA DELLA CLIP    

                    trak0.Location = new Point(clip.Virtual_X + clip.Virtual_Width + 5, 0);
                }
                else if (clip.Virtual_X > 15)
                {
                    // SE HO POSTO LO DISEGNO A SINISTRA DELLA CLIP

                    trak0.Location = new Point(clip.Virtual_X - 15, 0);
                    leftside = true;
                }


                this.HitTest_Areas[MxpTrack_HitTest_Info.audio0cursor] = new Rectangle(trak0.X, anY - 5, 10, 10);

                System.Drawing.Drawing2D.GraphicsPath cur = new System.Drawing.Drawing2D.GraphicsPath();
                cur.AddLine(trak0.X + 2, anY, trak0.X + 8, anY - 5);
                cur.AddLine(trak0.X + 8, anY - 5, trak0.X + 8, anY + 5);
                cur.CloseFigure();

                if (leftside)
                {
                    System.Drawing.Drawing2D.Matrix mtx = new System.Drawing.Drawing2D.Matrix();
                    mtx.RotateAt(180, new PointF(trak0.X + 5, anY));
                    cur.Transform(mtx);
                }


                this.HitTest_Areas[MxpTrack_HitTest_Info.audio0track] = trak0;

                FGrf.FillRectangle(Brushes.DimGray, trak0);
                FGrf.DrawRectangle(new Pen(Brushes.Silver, 1), trak0);
                FGrf.FillPath(Brushes.Silver, cur);

                cur.Dispose();
            }
        }




        private void GenerateSelectMark()
        {
            FSelectedMark = new System.Drawing.Drawing2D.GraphicsPath();
            FSelectedMark.AddLine(new Point(0, height / 2 - 5), new Point(6, height / 2));
            FSelectedMark.AddLine(new Point(6, height / 2), new Point(0, height / 2 + 5));
            FSelectedMark.CloseFigure();
        }

        private bool IsVisibledArea(long frameIn, long frameOut)
        {
            if (zoomIn == zoomOut) return false;
            bool result = false;

            if ((frameIn >= zoomIn && frameIn <= zoomOut) ||
                (frameOut >= zoomIn && frameOut <= zoomOut) ||
                (frameIn < zoomIn && frameOut > zoomOut))
            {
                result = true;
            }
            return result;
        }

        public void DrawToBitmap(Bitmap bmp, Rectangle rect)
        {
            if (bmp != null && FRendered != null && FGrf != null)
            {
                if (rect.Width != width || rect.Height != height)
                {
                    width = rect.Width;
                    height = rect.Height;
                    Resize(width, height);
                    Render();
                }
                System.Drawing.Graphics grf = System.Drawing.Graphics.FromImage(bmp);
                grf.DrawImage(FRendered, rect.Location);
                grf.Dispose();
            }
        }

        public Bitmap Image
        {
            get 
            {
                if (FRendered != null)
                {
                    return FRendered;
                }
                return null;
            }
        }




        public CMxpClip AddFreeClip(long tcin, long tcout, enCMxpClipType type)
        {
            return AddFreeClip("", type, null, tcin, tcout);
        }

        public CMxpClip AddFreeClip(string Title, enCMxpClipType type, object Tag, long tcin, long tcout)
        {
            return AddClip(title, type, null, Tag, tcin, tcout, 0, 0);
        }

        public CMxpClip AddClip(string Title, enCMxpClipType type, CMxpTL_ClipInfo clip_info, object Tag, long tcin, long tcout, long clipping_in, long clipping_out)
        {
            if (Tag != null)
            {
                int x = 0;
            }

            if (this.type == enMxpTrackType.Video && type != enCMxpClipType.Video)
            {
                System.Windows.Forms.MessageBox.Show("This track is only for video clips");
                return null;
            }
            if (this.type == enMxpTrackType.Audio && type != enCMxpClipType.Audio)
            {
                System.Windows.Forms.MessageBox.Show("This track is only for audio clips");

                return null;
            }
                
            CMxpClip clip = clips.Add(Title, type, clip_info, Tag, tcin, tcout, clipping_in, clipping_out, this);

            clip.ThumbLayout = thumbLayout;

            //clip.IsVisible = IsVisibledArea(tcin, tcout);     // AGGIORNO STATO DI VISIBILITA' DELLA CLIP
            clip.IsVisible = IsVisibledArea(tcin + clipping_in, tcin + clipping_out); 

            clip.NotifyRender();

            return clip;
        }

        public void AddClip(CMxpClip existing_clip)
        {
            if (clips.ContainClip(existing_clip))
            {
                // LA CLIP ERA GIA' SU QUESTA TRACCIA

                existing_clip.NotifyRender();
                ConformClipAfterZoomChanged();
                Render();
            }
            else
            { 
                // LA CLIP ERA SU UN'ALTRA TRACCIA
            
                //existing_clip.ID = clips.Count;
                existing_clip.ParentTrack.clips.Remove(existing_clip);

                existing_clip.Set_Parent(this, false);
                this.clips.Add(existing_clip);

                existing_clip.NotifyRender();
            }
        }


        
        #region CLIP EVENTs

        private void clip_OnClipRender(object Sender)
        {
            pixUnit = FrameToPix(1);
            Render();
        }

        private void clip_OnChangedTC(object Sender, long TCIn, long TCOut, enTcChangeType cType)
        {
            if (OnClipChangedTC != null) OnClipChangedTC(this, (CMxpClip)Sender, cType);
            //System.Diagnostics.Debug.Write("SingleLine clip_OnChangedTC()");
            ConformClipAfterZoomChanged();
            Render();
        }

        private void clip_OnChangeTitle(object Sender)
        {
            CMxpClip clip = (CMxpClip)Sender;

            //clip.IsVisible = IsVisibledArea(clip.TimcodeIn, clip.TimecodeOut);     // AGGIORNO STATO DI VISIBILITA' DELLA CLIP
            clip.IsVisible = IsVisibledArea(clip.TimcodeIn + clip.ClippingIn, clip.TimcodeIn + clip.ClippingOut); 

            if (clip.IsVisible)
            {
                int pixInDiff = (Int32)(zoomIn % pixUnit);
                int pixIn = FrameToPix(clip.TimcodeIn);

                // AGGIORNO LA BOUNDS AREA DELLA CLIP IN DISEGNO
                clip.ClientBounds = new Rectangle(pixIn, 0, clip.Width, height);
                FGrf.DrawImage(clip.Image, 0 + clip.X, 0);


                if (OnRender != null) OnRender(this);
            }
        }

        private void clip_OnChangeSelected(object Sender, bool Selected)
        {
            CMxpClip clip = (CMxpClip)Sender;

            clip.IsVisible = IsVisibledArea(clip.TimcodeIn, clip.TimecodeOut);     // AGGIORNO STATO DI VISIBILITA' DELLA CLIP

            if (clip.IsVisible)
            {                
                //int pixIn = FrameToPix(clip.ClippingIn + clip.TimcodeIn);

                //if (clip.IsFreeClip)
                //{
                //    pixIn = FrameToPix(clip.TimcodeIn);
                //}

                // AGGIORNO LA BOUNDS AREA DELLA CLIP IN DISEGNO
                //clip.ClientBounds = new Rectangle(clip.X, 0, clip.Width, height);
                FGrf.DrawImage(clip.Image, 0 + clip.ClientBounds.X, 0);
                
                if (off && !isResizing)
                {
                    FGrf.DrawImage(off_picture, 0, 0);
                }

                if (OnRender != null) OnRender(this);
            }

            List<CMxpClip> xSelected = new List<CMxpClip>();
            /*
            foreach(KTLClip xclip in FClips.Clips)
            {
                if (clip.Selected) xSelected.Add(xclip);
            }
            */
            if (Selected) xSelected.Add(clip);

            if (OnChangeClipSelection != null) OnChangeClipSelection(this, xSelected.ToArray());
        }

        private void clip_OnDeleteClip(object Sender)
        {
            //clips.Remove((CMxpClip)Sender);
            if (OnClipRemoving != null) OnClipRemoving(this, (CMxpClip)Sender);

            ((CMxpTLTracks_Collection)Parent).DeletedClips.Add((CMxpClip)Sender);       // AREA TEMPORANE PER RECUPERI IN CASO DI UNDO
            clips.Remove((CMxpClip)Sender);
            Render();
        }

        private void restoreClip(CMxpClip clip)
        {
            for (int i = 0; i < ((CMxpTLTracks_Collection)Parent).DeletedClips.Count; i++)
            {
                CMxpClip trk_clip = ((CMxpTLTracks_Collection)Parent).DeletedClips[i];

                if (trk_clip.ID == clip.ID)
                {
                    AddClip(clip);
                    ((CMxpTLTracks_Collection)Parent).DeletedClips.RemoveAt(i);
                    break;
                }
            }
        }

        private void clip_OnEdit(object Sender, bool Start)
        {
            if (OnEditClip != null) OnEditClip(this, (CMxpClip)Sender, Start);
        }
        
        private void clip_OnVirtualObjectNeed(object Sender, Rectangle rect)
        {
            if (OnVirtualObjectNeed != null) OnVirtualObjectNeed(Sender, rect);
        }

        private void Item_OnEndEditing(object Sender)
        {
            
        }

        private void Item_OnBeginEditing(object Sender)
        {

        }

        #endregion CLIP EVENTs



        private void Resize(int width, int height)
        {
            if (width > 0 && height > 0)
            {
                if (FRendered != null)
                {
                    FRendered.Dispose();
                    FRendered = null;

                    off_picture.Dispose();
                }
                if (FGrf != null)
                {
                    FGrf.Dispose();
                    FGrf = null;
                }

                FRendered = new Bitmap(width, height);
                if (FGrf != null) FGrf.Dispose();
                FGrf = System.Drawing.Graphics.FromImage(FRendered);
                ConformClipAfterZoomChanged();


                off_picture = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                Graphics g_off = Graphics.FromImage(off_picture);
                
                Pen p = new Pen(Brushes.Black, 1);
                g_off.Clear(Color.FromArgb(100, Color.Black));

                for(int lx = 0; lx < off_picture.Width + off_picture.Height; lx += 8)
                {
                    g_off.DrawLine(p, lx, 0, lx - off_picture.Height, off_picture.Height);
                }

                p.Dispose();
                g_off.Dispose();
            }
        }


        public long PixToFrame(int x)
        {
            if (Width <= 0) return 0;
            if ((zoomOut - zoomIn) <= 0) return 0;
            return (x * (zoomOut - zoomIn) / Width) + zoomIn;
        }

        public long PixInFrame(int x)
        {
            if (Width <= 0) return 0;
            if ((zoomOut - zoomIn) <= 0) return 0;
            return (x * (zoomOut - zoomIn) / Width);
        }

        public int FrameToPix(long frame)
        {
            if (Width <= 0) return 0;
            if ((zoomOut - zoomIn) <= 0) return 0;

            double OneFFSize = (double)width / (zoomOut - zoomIn);
            int ftp = (int)((frame - zoomIn) * OneFFSize);

            return (ftp == 0)? (int)1 : ftp;
            //return (frame * Width) / (FZoomOut - FZoomIn);
        }

        public int FrameInPix(long frame)
        {
            if (Width <= 0) return 0;
            if ((zoomOut - zoomIn) <= 0) return 0;

            double OneFFSize = (double)width / (zoomOut - zoomIn);
            int ftp = (int)(frame * OneFFSize);

            return (ftp == 0) ? (int)1 : ftp;
        }

        public void SetZoom(Int32 newZoomIn, Int32 newZoomOut)
        {
            SetZoom(newZoomIn, newZoomOut, true);
        }

        public void SetZoom(long newZoomIn, long newZoomOut, bool doRender)
        {
            foreach (CMxpClip clip in this.clips)
            {
                if (clip.IsVisible && clip.Visible)
                {
                    if (clip.FrameThumbs.Count > 0 || clip.BackgraoundImage != null)
                    {
                        clip.NotifyRender_NoEVents();
                    }
                }
            }

            if (newZoomIn != zoomIn || newZoomOut != zoomOut)
            {
                bool ToConform = (newZoomOut - newZoomIn) != (zoomOut - zoomIn);

                zoomIn = newZoomIn;
                zoomOut = newZoomOut;
                pixUnit = FrameToPix(1);

                if (ToConform) 
                    ConformClipAfterZoomChanged();

                if (doRender) Render();
            }
        }

        public MxpTrack_HitTest_Info GetHitTest(Point p)
        {
            LastHitTested = MxpTrack_HitTest_Info.none;
            FLastClickedClip = null;

            if (p.X >= 0 && p.X <= width && p.Y >= 0 && p.Y <= height)
            {
                LastHitTested = MxpTrack_HitTest_Info.timeline;
            }

            foreach (CMxpClip clip in clips)
            {
                if (clip.IsVisible)
                {
                    if (clip.ClientBounds.Contains(p))
                    {
                        FLastClickedClip    = clip;
                        LastHitTested       = MxpTrack_HitTest_Info.clip;
                    }
                }

                if (clip.Type == enCMxpClipType.Audio)
                {
                    if (HitTest_Areas[MxpTrack_HitTest_Info.audio0cursor].Contains(p))
                    {
                        LastHitTested = MxpTrack_HitTest_Info.audio0cursor;
                    }
                    else if (HitTest_Areas[MxpTrack_HitTest_Info.audio0track].Contains(p))
                    {
                        LastHitTested = MxpTrack_HitTest_Info.audio0track;
                    }
                }
            }

            return LastHitTested;
        }


        private void ConformClipAfterZoomChanged()
        {
            for (int c = 0; c < clips.Count; c++)
            {
                CMxpClip clip = clips[c];

                //clip.IsVisible = IsVisibledArea(clip.TimcodeIn, clip.TimecodeOut);     // AGGIORNO STATO DI VISIBILITA' DELLA CLIP
                clip.IsVisible = IsVisibledArea(clip.TimcodeIn + clip.ClippingIn, clip.TimcodeIn + clip.ClippingOut); 

                int pixIn   = FrameToPix(clip.ClippingIn);
                int pixOut  = FrameToPix(clip.ClippingOut);

                clip.SetSize(pixOut - pixIn, height);
            }
        }




        #region PUBLIC PROPERTIES

        public long Duration
        {
            get { return duration; }
            set
            {
                if (value != duration)
                {
                    duration = value;
                    Render();
                }
            }
        }
                
        public Int32 ID
        {
            get { return id; }
            set
            {
                id = value;
            }
        }

        public long ZoomIn
        {
            get { return height; }
            set
            {
                if (value != zoomIn)
                {
                    if (zoomOut > value)
                    {
                        zoomIn = value;
                        pixUnit = (Int32)(width / (zoomOut - zoomIn));
                        ConformClipAfterZoomChanged();
                        Render();
                    }
                }
            }
        }

        public long ZoomOut
        {
            get { return zoomOut; }
            set
            {
                if (value != zoomOut)
                {
                    zoomOut = value;
                    pixUnit = (Int32)(width / (zoomOut - zoomIn));
                    ConformClipAfterZoomChanged();
                    Render();
                }
            }
        }

        public bool Selected
        {
            get { return selected; }
            set 
            {
                if (selected != value)
                {
                    selected = value;
                    Render();

                    if (OnSelectionChanged != null) OnSelectionChanged(this, value);
                }
            }
        }

        public string Title
        {
            get { return this.title; }
            set { this.title = value; }
        }

        public string Description
        {
            get { return m_description; }
            set { m_description = value; }
        }

        public object Tag
        {
            get { return this.tag; }
            set { this.tag = value; }
        }

        public CMxpClip LastClickedClip
        {
            get { return FLastClickedClip; }
            set { FLastClickedClip = value; }
        }

        public int ClipCount
        {
            get { return clips.Count; }
        }

        public CMxpClip this[int i]
        {
            get 
            {
                if (i >= clips.Count) return null;
                return clips[i]; 
            }
        }

        public Color TimeLineBackColor
        {
            get { return videoTrackBackColor; }
            set 
            {
                if (videoTrackBackColor != value)
                {
                    videoTrackBackColor = value;
                    Render();
                }
            }
        }

        public Color ClipColor
        {
            get { return FClipColor; }
            set
            { 
                if(FClipColor != value)
                {
                    FClipColor = value;

                    foreach (CMxpClip clip in clips)
                    {
                        clip.BackColor = value;
                    }                    
                }
            }
        }

        public Color SelectedClipColor
        {
            get { return FSelectedClipColor; }
            set
            {
                if (FSelectedClipColor != value)
                {
                    FSelectedClipColor = value;

                    foreach (CMxpClip clip in clips)
                    {
                        clip.SelectedClipColor = value;
                    }
                }
            }
        }

        public List<CMxpClip> Clips
        {
            get
            {
                return clips.ToList();
            }
        }

        public enMxpTrackType Type
        {
            get { return type; }
        }

        public Color BackColor
        {
            get 
            {
                return (type == enMxpTrackType.Audio) ? audioTrackBackColor : videoTrackBackColor;
            }
        }

        public bool Off
        {
            get { return off; }
            set
            { 
                off = value;
                if (OnChangeOffState != null) OnChangeOffState(this, value);

                timeline_control.Get_TimetabeObject().NotifyOnlineTableChanged();

                Render();
            }
        }

        public CMxpClip LastClip
        {
            get
            {
                CMxpClip last_clip = null;

                foreach (CMxpClip clip in clips)
                {
                    if (last_clip == null)
                    {
                        last_clip = clip;
                    }
                    else
                    {
                        if (clip.TimcodeIn + clip.ClippingOut > last_clip.TimcodeIn + last_clip.ClippingOut)
                        {
                            last_clip = clip;
                        }
                    }
                }

                return last_clip;
            }
        }

        public MxpClipThumbLayout ThumbLayout
        {
            get { return thumbLayout; }
            set
            {
                if (thumbLayout != value)
                {
                    thumbLayout = value;

                    foreach (CMxpClip clip in clips)
                    {
                        clip.ThumbLayout = value;
                    }
                }
            }
        }

        public Image TrackIcon
        {
            get { return trackIcon; }
            set
            {
                if (trackIcon != value)
                {
                    trackIcon = value;

                    if (timeline_control.TrackInfoControl_Container != null)
                    {
                        timeline_control.TrackInfoControl_Container.Refresh();
                    }
                }
            }
        }

        #endregion
    }
}
