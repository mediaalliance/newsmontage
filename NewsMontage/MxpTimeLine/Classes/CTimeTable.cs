﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
using System.ComponentModel;
using MediaAlliance.Controls.MxpTimeLine;
using MediaAlliance.Controls.MxpTimeLine.Objects;




namespace MediaAlliance.Controls.MxpTimeLine.Classes
{
    public enum enTimeTableItemType
    { 
        uncknow,
        item_start,
        item_end,
        item_inside
    }

    public enum enTimeTableEventType
    { 
        undefinde,
        item_goes_online,
        item_goes_offline,
        item_added,
        item_removed,
        item_changedposition,
        online_list_changed
    }


    public class CMagnetInfo
    {
        public long request_timecode            = 0;
        public long request_margin              = 0;
        public long mag_timecode                = 0;
        public int mag_x                        = 0;
        public long mag_distance                = 10000;
        public CMxpTrack mag_ontrack            = null;
        public CMxpTrack track                  = null;
        public MxpMagnetPoint mag_location      = MxpMagnetPoint.undefined;
        public CTimeTableItem closest_item      = null;
        public object[] ExcludedObjects         = new object[0];
        public List<CTimeTableItem> all_items   = new List<CTimeTableItem>();

        public CMagnetInfo()
        { 
        
        }

        public bool HasItems
        {
            get { return all_items.Count > 0; }
        }
    }

    public class CTimeTable
    {
        // DELEGATES
        public delegate void TT_OnHappends(object Sender, long timecode, CTimeTableItem item, enTimeTableItemType itemposition);
        public delegate void TT_OnEvent(object Sender, long timecode, enTimeTableEventType etype, object item);

        // EVENTS
        public event TT_OnHappends OnHappends = null;
        public event TT_OnEvent OnEvent = null;

        private Thread th_check     = null;
        private bool th_check_stop  = false;

        private long current_timecode = 0;

        private long LastCheckedForLoosed = 0;

        private CMxpTimeTable_Container table           = new CMxpTimeTable_Container();

        private MxpTmeLine owner = null;

        private enum tl_direction
        { 
            none,
            going_right,
            going_left
        }

        private tl_direction current_direction = tl_direction.none;




        public CTimeTable(MxpTmeLine owner)
        {
            this.owner = owner;

            th_check = new Thread(new ThreadStart(th_check_task));
            th_check.IsBackground = true;
            th_check.Start();
        }




        private void th_check_task()
        {
            while (!th_check_stop)
            {
                try
                {
                    lock (table)
                    {
                        List<CTimeTableItem> current_online = new List<CTimeTableItem>();
                        List<CTimeTableItem> current_offline = new List<CTimeTableItem>();

                        lock (current_online)
                        {
                            lock (current_offline)
                            {
                                try
                                {
                                    current_online.AddRange(table.Where(x => x.tcin <= current_timecode && x.tcout >= current_timecode));
                                    current_offline.AddRange(table.Where(x =>
                                                            (x.tcin < current_timecode && x.tcout < current_timecode) ||
                                                            (x.tcin > current_timecode && x.tcout > current_timecode)));

                                    // CONTROLLO SE GLI ITEM ONLINE LO SONO ANCORA

                                    bool atLeastOneChange = false;

                                    for (int i = 0; i < current_online.Count; i++)
                                    {
                                        CTimeTableItem item = current_online[i];

                                        if (!item.IsOnline)
                                        {
                                            item.IsOnline = true;
                                            CheckForLoosedEvent(item, true, current_timecode);
                                            Raise_OnEvent(this, this.TimeCode, enTimeTableEventType.item_goes_online, item);
                                            atLeastOneChange = true;
                                        }
                                    }

                                    for (int i = 0; i < current_offline.Count; i++)
                                    {
                                        CTimeTableItem item = current_offline[i];

                                        if (item.IsOnline)
                                        {
                                            item.IsOnline = false;
                                            CheckForLoosedEvent(item, false, current_timecode);
                                            Raise_OnEvent(this, this.TimeCode, enTimeTableEventType.item_goes_offline, item);
                                            atLeastOneChange = true;
                                        }
                                    }

                                    if (atLeastOneChange)
                                    {
                                        Raise_OnEvent(this, this.TimeCode, enTimeTableEventType.online_list_changed, null);
                                    }

                                    Thread.Sleep(40);
                                }
                                catch
                                {
                                }
                            }
                        }
                    }
                }
                catch { }
            }
        }



        private void Raise_OnEvent(object Sender, long timecode, enTimeTableEventType etype, object item)
        {
            if (OnEvent != null)
            {
                foreach (TT_OnEvent singleCast in OnEvent.GetInvocationList())
                {
                    ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;
                    try
                    {
                        if (syncInvoke != null && syncInvoke.InvokeRequired)
                        {
                            // Invokie the event on the main thread
                            syncInvoke.Invoke(OnEvent, new object[] { Sender, timecode, etype, item });
                        }
                        else
                        {
                            // Raise the event
                            singleCast(Sender, timecode, etype, item);
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }

        private void Raise_OnHappends(object Sender, long timecode, CTimeTableItem item)
        {
            if (OnHappends != null)
            {
                foreach (TT_OnHappends singleCast in OnHappends.GetInvocationList())
                {
                    enTimeTableItemType type = item.GetEventOnTc(timecode);

                    item.LastRisedEvent = type;

                    ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;
                    try
                    {
                        if (syncInvoke != null && syncInvoke.InvokeRequired)
                        {
                            // Invokie the event on the main thread
                            syncInvoke.Invoke(OnHappends, new object[] { Sender, timecode, item, type });
                        }
                        else
                        {
                            // Raise the event
                            singleCast(Sender, timecode, item, type);
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }



        #region PUBLIC METHODS

        public void AddClip(CMxpClip clip)
        {
            addClip(clip, true);

            clip.OnChangedTC += new CMxpClip.MxpClip_OnChangedTC(clip_OnChangedTC);
            clip.OnDeleteClip += new CMxpClip.MxpClip_OnDeleteClip(clip_OnDeleteClip);
        }

        public void RemoveClip(CMxpClip clip)
        {
            for (int i = 0; i < table.Count; i++)
            {
                if (((CMxpClip)table[i].Tag) == clip)
                {
                    table.RemoveAt(i);

                    if (current_timecode >= clip.TimcodeIn && current_timecode <= clip.TimecodeOut)
                    {
                        Raise_OnEvent(this, this.TimeCode, enTimeTableEventType.online_list_changed, null);
                    }

                    break;
                }
            }
        }

        public void ModifyClip(CMxpClip clip)
        {
            //RemoveClip(clip);
            //addClip(clip, false);

            List<CTimeTableItem> items = table.Where(X => X.Tag is CMxpClip && X.Tag == clip).ToList();

            if (items.Count > 0)
            {
                long tcin   = clip.TimcodeIn + clip.ClippingIn;
                long tcout  = clip.TimcodeIn + clip.ClippingOut;

                if (clip.IsFreeClip)
                {
                    tcin    = clip.TimcodeIn;
                    tcout   = clip.TimecodeOut;
                }

                items[0].tcin   = tcin;
                items[0].tcout  = tcout;

                #if DEBUG
                Console.WriteLine("TT MODIFY " + tcin.ToString().PadRight(6, '0') + " - " + tcout.ToString());    
                #endif

                Raise_OnEvent(this, this.TimeCode, enTimeTableEventType.item_changedposition, clip);
            }
        }

        public long GetClipsOnRange(long tcin, long tcout, object except)
        {
            return GetClipsOnRange(tcin, tcout, -1, except);
        }

        public long GetClipsOnRange(long tcin, long tcout, int track, object except)
        {
            List<CTimeTableItem> items = new List<CTimeTableItem>(table.Where(x => x.tcin >= tcin && x.tcin <= tcout && x.Tag is CMxpClip && x.Tag != except));

            if (track >= 0 && items.Count > 0)
            {
                List<CTimeTableItem> items_on_track = new List<CTimeTableItem>();

                foreach (CTimeTableItem it in items)
                { 
                    CMxpClip clip = (CMxpClip)it.Tag;
                    if(((CMxpTrack)clip.ParentTrack).ID == track) items_on_track.Add(it);
                }

                return getCorrection(items_on_track, tcin, tcout);
            }

            return getCorrection(items, tcin, tcout);
        }

        public CMagnetInfo GetMagnetInfo(long tc, long margin, object[] except)
        {
            CMagnetInfo m_info = new CMagnetInfo();

            m_info.request_timecode = tc;
            m_info.request_margin   = margin;
            m_info.ExcludedObjects  = except;

            List<object> excpets = new List<object>(except);

            List<CTimeTableItem> visible_items = table.Where(x => 
                                                            (x.tcin >= owner.ZoomIn || x.tcout <= owner.ZoomOut) && 
                                                            x.Tag is CMxpClip).ToList();

            m_info.all_items = visible_items.Where(x => 
                                                    (x.tcin > tc-margin && x.tcin < tc + margin) ||
                                                    (x.tcout > tc - margin && x.tcout < tc + margin)).ToList();

            int id      = -1;
            long min    = 100000;

            for (int i = 0; i < m_info.all_items.Count; i++)
            {
                CTimeTableItem item = m_info.all_items[i];

                if (!excpets.Contains(item.Tag))
                {
                    long indiff = Math.Abs(item.tcin - tc);
                    long outdiff = Math.Abs(item.tcout - tc);


                    if (indiff < min)
                    {
                        min = indiff;
                        id = i;
                        item.last_magnet_tc = item.tcin;
                        m_info.mag_timecode = item.tcin;
                        m_info.mag_location = MxpMagnetPoint.onStart;
                        m_info.mag_distance = indiff;
                        m_info.mag_ontrack = (item.Tag is CMxpClip) ? ((CMxpClip)item.Tag).ParentTrack : null;
                        ((CMxpClip)item.Tag).Last_magnet_tc = item.tcin;
                    }

                    if (outdiff < min)
                    {
                        min = outdiff;
                        id = i;
                        item.last_magnet_tc = item.tcout;
                        m_info.mag_timecode = item.tcout;
                        m_info.mag_location = MxpMagnetPoint.onEnd;
                        m_info.mag_distance = outdiff;
                        m_info.mag_ontrack = (item.Tag is CMxpClip) ? ((CMxpClip)item.Tag).ParentTrack : null;
                        ((CMxpClip)item.Tag).Last_magnet_tc = item.tcout;
                    }
                }
                //else
                //{
                //    m_info.all_items.Remove(item);
                //}
            }

            foreach(object ex in excpets)
            {
                CTimeTableItem item_to_remove = null;

                foreach (CTimeTableItem item in m_info.all_items)
                {
                    if (item.Tag == ex)
                    {
                        item_to_remove = item;
                        break;
                    }
                }

                if (item_to_remove != null) m_info.all_items.Remove(item_to_remove);
            }


            if (id >= 0 && min < margin && id < m_info.all_items.Count)
            {
                m_info.closest_item = m_info.all_items[id];
            }

            if (!m_info.HasItems) m_info = null;


            #region CHECK FOR CURSOR POSITION

            if (excpets.Where(X => X.GetType() == typeof(CMxpTlCursor)).ToList().Count == 0)
            {
                long cursor_diff = Math.Abs(tc - owner.CurrentPosition);

                if (m_info != null)
                {
                    if (cursor_diff < m_info.mag_distance && cursor_diff < margin)
                    {
                        m_info = new CMagnetInfo();
                        m_info.mag_distance = cursor_diff;
                        m_info.mag_location = MxpMagnetPoint.onMiddle;
                        m_info.mag_ontrack = null;
                        m_info.mag_timecode = owner.CurrentPosition;
                    }
                }
                else
                {
                    if (cursor_diff < margin)
                    {
                        m_info = new CMagnetInfo();
                        m_info.mag_distance = cursor_diff;
                        m_info.mag_location = MxpMagnetPoint.onMiddle;
                        m_info.mag_ontrack = null;
                        m_info.mag_timecode = owner.CurrentPosition;
                    }
                }
            }

            #endregion CHECK FOR CURSOR POSITION


            return m_info;


            //IEnumerable<CTimeTableItem> items = table.Where(x => ((x.tcin >= tc - margin && x.tcin <= tc + margin) || 
            //                                                      (x.tcout >= tc - margin && x.tcout <= tc + margin))
            //                                                        && x.Tag != except);

            //CTimeTableItem result = null;


            //// TROVO QUELLO PIU' VICINO TRA I RISULTATI

            //foreach (CTimeTableItem item in items)
            //{
            //    if (result == null)
            //    {
            //        item.last_magnet_tc = item.tcout;

            //        if (Math.Abs(item.tcin - tc) < Math.Abs(item.tcout - tc))
            //        {
            //            item.last_magnet_tc = item.tcin;
            //        }

            //        result = item;
            //    }
            //    else
            //    {
            //        if (Math.Abs(item.tcin     - tc) < Math.Abs(result.tcin    - tc))
            //        {
            //            item.last_magnet_tc = item.tcin;
            //            result = item;
            //        }
            //        else if(Math.Abs(item.tcout    - tc) < Math.Abs(result.tcout   - tc))
            //        {
            //            item.last_magnet_tc = item.tcout;
            //            result = item;
            //        }
            //    }
            //}
        }

        public bool CheckForSpace(long tcin, long tcout, int track_id)
        { 
            List<CTimeTableItem> items =  table.Where(x => 
                                                     (x.tcin <= tcin  && x.tcout >= tcin) ||
                                                     (x.tcin <= tcout && x.tcout >= tcout) ||
                                                     (x.tcin <= tcin  && x.tcout >= tcout) ||
                                                     (x.tcout >= tcin && x.tcout <= tcout)).ToList();

            if (items.Count > 0)
            {
                bool result = true;

                foreach (CTimeTableItem it in items)
                {
                    if (it.Tag is CMxpClip)
                    {
                        if (((CMxpClip)it.Tag).ParentTrack.ID == track_id && ((CMxpClip)it.Tag).Visible)
                        {
                            result = false;
                            break;
                        }
                    }
                }

                return result;
            }

            return true;
        }

        public void NotifyOnlineTableChanged()
        {
            Raise_OnEvent(this, this.TimeCode, enTimeTableEventType.online_list_changed, null);
        }

        #endregion PUBLIC METHODS



        void clip_OnChangedTC(object Sender, long TCIn, long TCOut, enTcChangeType cType)
        {
            ModifyClip((CMxpClip)Sender);
        }

        void clip_OnDeleteClip(object Sender)
        {
            RemoveClip((CMxpClip)Sender);
        }


        private class TrackOrderer : IComparer<CTimeTableItem>
        {
            public int Compare(CTimeTableItem x, CTimeTableItem y)
            {
                if (x.Tag is CMxpClip && y.Tag is CMxpClip)
                {
                    if (((CMxpClip)x.Tag).ParentTrack.ID > ((CMxpClip)y.Tag).ParentTrack.ID) return 1;
                    if (((CMxpClip)y.Tag).ParentTrack.ID > ((CMxpClip)x.Tag).ParentTrack.ID) return -1;
                }

                return 0;
            }
        }



        #region PUBLIC PROPERTIES
        
        public long TimeCode
        {
            get { return current_timecode; }
            set
            {
                if (current_timecode != value)
                {
                    ChangeTimecode(value);
                }
            }
        }

        public List<CTimeTableItem> OnlineItems
        {
            get 
            {
                List<CTimeTableItem> ol = new List<CTimeTableItem>(table.Where(x => x.IsOnline == true));
                ol.Sort(new TrackOrderer());

                return ol;
            }
        }

        public List<CTimeTableItem> OfflineItems
        {
            get
            {
                return new List<CTimeTableItem>(table.Where(x => x.IsOnline == false));
            }
        }

        #endregion PUBLIC PROPERTIES
        


        #region PRIVATE METHODS

        private void ChangeTimecode(long tc)
        {
            current_direction = (tc > current_timecode) ? tl_direction.going_right : tl_direction.going_left;

            current_timecode = tc;

            List<CTimeTableItem> found = table[tc];

            if (found.Count > 0)
            {
                if (OnHappends != null)
                {
                    foreach (CTimeTableItem item in found)
                    {
                        item.LastRisedEvent = item.GetEventOnTc(tc);
                        Raise_OnHappends(this, tc, item);
                    }
                }
            }        
        }

        private bool IsOnline(object item)
        {
            if (item is CMxpClip)
            {
                if (((CMxpClip)item).TimcodeIn <= this.TimeCode && ((CMxpClip)item).TimecodeOut >= this.TimeCode) return true;
            }

            return false;
        }

        private void CheckForLoosedEvent(CTimeTableItem item, bool isEntering, long ontimecode)
        {
            if (ontimecode != item.tcin && ontimecode != item.tcout && item.LastRisedEvent == enTimeTableItemType.uncknow)
            {
                if ((ontimecode > item.tcin && LastCheckedForLoosed < item.tcin) ||
                    (ontimecode < item.tcin && LastCheckedForLoosed > item.tcin))
                {
                    Raise_OnHappends(this, item.tcin, item);
                }
                else if ((ontimecode > item.tcout && LastCheckedForLoosed < item.tcout) ||
                         (ontimecode < item.tcout && LastCheckedForLoosed > item.tcout))
                {
                    Raise_OnHappends(this, item.tcout, item);
                }

            }

            item.LastRisedEvent = enTimeTableItemType.uncknow;

            LastCheckedForLoosed = ontimecode;
        }

        private void addClip(CMxpClip clip, bool notifyevent)
        {            
            // AGGIUNGO GLI ELEMENTI NUOVI

            long tcin   = clip.TimcodeIn + clip.ClippingIn;
            long tcout  = clip.TimcodeIn + clip.ClippingOut;

            if (clip.IsFreeClip)
            {
                tcin    = clip.TimcodeIn;
                tcout   = clip.TimecodeOut;
            }

            CTimeTableItem newitem = new CTimeTableItem(tcin, tcout, clip);

            table.Add(newitem);

            Raise_OnEvent(this, this.TimeCode, enTimeTableEventType.item_added, clip);

            if (current_timecode >= clip.TimcodeIn && current_timecode <= clip.TimecodeOut)
            {
                newitem.IsOnline = true;
                Raise_OnEvent(this, this.TimeCode, enTimeTableEventType.online_list_changed, null);
            }
            else
            {
                newitem.IsOnline = false;
            }
        }

        private long getCorrection(List<CTimeTableItem> items, long tcin, long tcout)
        {
            if (items.Count == 0) return 0;

            if (tcin >= items[0].tcin && tcin <= items[0].tcout)
            {
                return items[0].tcin - tcout - 1;    
            }
            else if (tcout >= items[0].tcin && tcout <= items[0].tcout)
            {
                return items[0].tcin - tcin + 1;            
            }

            return 0;
        }
        
        #endregion PRIVATE METHODS
    }



    public class CTimeTableItem
    {
        public long tcin                = 0;
        public long tcout               = 0;
        public object Tag               = null;
        public bool IsOnline            = false;
        public long last_magnet_tc      = 0;
        public CMxpTimeTable_Container parent_list = null;
        public enTimeTableItemType LastRisedEvent = enTimeTableItemType.uncknow;

        public CTimeTableItem(long tcin, long tcout, CMxpClip Tag)
        {
            this.tcin           = tcin;
            this.tcout          = tcout;
            this.Tag            = Tag;
        }

        public enTimeTableItemType GetEventOnTc(long tc)
        {
            if (tc == tcin) return enTimeTableItemType.item_start;
            if (tc == tcout) return enTimeTableItemType.item_end;
            if (tc > tcin && tc < tcout) return enTimeTableItemType.item_inside;
            return enTimeTableItemType.uncknow;
        }
    }

    public class CMxpTimeTable_Container : CAdvList<CTimeTableItem>
    {
        public CMxpTimeTable_Container() : base() 
        {
            
        }

        public CTimeTableItem this[int index]
        {
            get
            {
                return base.Items[index];
            }
        }
    
        public List<CTimeTableItem> this[long tc]
        {
            get
            {
                List<CTimeTableItem> items = new List<CTimeTableItem>();

                items.AddRange(this.Where(x => (x.tcin == tc || x.tcout == tc)));

                return items;
            }
        }

        public List<CTimeTableItem> this[object tag]
        {
            get
            {
                IEnumerable<CTimeTableItem> items_raw = this.Where(x => x.Tag == tag);
                List<CTimeTableItem> items = new List<CTimeTableItem>(items_raw);

                return items;
            }
        }

        public List<CTimeTableItem> this[long tc, object tag]
        {
            get
            {
                IEnumerable<CTimeTableItem> items_raw = this.Where(x => x.tcin == tc && x.Tag == tag);
                List<CTimeTableItem> items = new List<CTimeTableItem>(items_raw);

                return items;
            }
        }
    }
}
