﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MediaAlliance.Controls.MxpTimeLine.Classes
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;
    using System.Runtime.InteropServices;

    public struct IconInfo
    {
        public bool fIcon;
        public int xHotspot;
        public int yHotspot;
        public IntPtr hbmMask;
        public IntPtr hbmColor;
    }

    public enum enCursortType
    { 
        Default         = 0,
        EditMarkIn      = 1,
        EditMarkOut     = 2,
        VericalMove     = 3,
        HorizontalMove  = 4,
        Split           = 5,
        ClipAboutMove   = 6,
        ClipMove        = 7,
        ZoomTrackMove   = 8,
        ZoomTrackScale  = 9,
        AudioNodeAdd    = 10,
        AudioNodeRemove = 11
    }

    public class CCursors
    {
        private Dictionary<enCursortType, Cursor> cursors = new Dictionary<enCursortType, Cursor>();
        private enCursortType current = enCursortType.Default;
        private MxpTimeLine.MxpTmeLine owner = null;

        public CCursors(MxpTimeLine.MxpTmeLine owner)
        {
            this.owner = owner;

            cursors.Add(enCursortType.Default,              Cursors.Default);
            cursors.Add(enCursortType.EditMarkIn,           EditMarkIn_Cursor());
            cursors.Add(enCursortType.EditMarkOut,          EditMarkOut_Cursor());
            cursors.Add(enCursortType.VericalMove,          Cursors.SizeNS);
            cursors.Add(enCursortType.HorizontalMove,       Cursors.SizeWE);
            cursors.Add(enCursortType.Split,                ClipCutter_Cursor());       //Cursors.VSplit);
            cursors.Add(enCursortType.ClipMove,             ClipMove_Cursor());
            cursors.Add(enCursortType.ClipAboutMove,        ClipAboutMove_Cursor());
            cursors.Add(enCursortType.ZoomTrackMove,        ZoomMove_Cursor());
            cursors.Add(enCursortType.ZoomTrackScale,       ZoomScale_Cursor());
            cursors.Add(enCursortType.AudioNodeAdd,         AudioNodeAdd_Cursor());
            cursors.Add(enCursortType.AudioNodeRemove,      AudioNodeRemove_Cursor());
        }

        public void Set(enCursortType type)
        {
            //if (type == enCursortType.Default) return;

            if (current != type)
            {
                //Console.WriteLine(" SETTED CURSOR : " + type.ToString());
                //Cursor.Current = cursors[type];

                owner.Cursor = cursors[type];

                current = type;
            }
        }

        public Cursor EditMarkIn_Cursor()
        {
            return CreateCursor(Properties.Resources.MarkIn_Edit, 16, 16);   
        }

        public Cursor EditMarkOut_Cursor()
        {
            return CreateCursor(Properties.Resources.MarkOut_Edit, 19, 16);  
        }

        public Cursor ZoomMove_Cursor()
        {
            return CreateCursor(Properties.Resources.Cursor_ZoomMove, 16, 16);
        }

        public Cursor ZoomScale_Cursor()
        {
            return CreateCursor(Properties.Resources.Cursor_ZoomScale, 16, 16);
        }
        
        public Cursor ClipCutter_Cursor()
        {
            return CreateCursor(Properties.Resources.ClipCutter_Cursor, 16, 16);
        }

        public Cursor AudioNodeAdd_Cursor()
        {
            return CreateCursor(Properties.Resources.AudioNodeAdd_Cursor, 16, 16);
        }
        
        public Cursor AudioNodeRemove_Cursor()
        {
            return CreateCursor(Properties.Resources.AudioNodeRemove_Cursor, 16, 16);
        }
        
        public Cursor ClipAboutMove_Cursor()
        {
            return CreateCursor(Properties.Resources.Cursor_hand_open, 16, 16);
        }

        public Cursor ClipMove_Cursor()
        {
            return CreateCursor(Properties.Resources.Cursor_hand_close, 16, 16);
        }




        private static Cursor NewCursor(Image img)
        {
            //System.IO.MemoryStream ms = new System.IO.MemoryStream(Properties.Resources.MarkIn_Edit1);
            //Image img = Properties.Resources.MarkIn_Edit1;

            //Graphics g = Graphics.FromImage(img);
            //using (Font f = new Font(FontFamily.GenericSansSerif, 10))
            //    g.DrawString("{ } Switch On The Code", f, Brushes.Green, 0, 0);

            Cursor c = CreateCursor((Bitmap) img, 3, 3);
            img.Dispose();

            return c;
        }

        [DllImport("user32.dll")]
        private static extern IntPtr CreateIconIndirect(ref IconInfo icon);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetIconInfo(IntPtr hIcon, ref IconInfo pIconInfo);

        private static Cursor CreateCursor(Bitmap bmp, int xHotSpot, int yHotSpot)
        {
            IconInfo tmp = new IconInfo();
            GetIconInfo(bmp.GetHicon(), ref tmp);
            tmp.xHotspot = xHotSpot;
            tmp.yHotspot = yHotSpot;
            tmp.fIcon = false;
            return new Cursor(CreateIconIndirect(ref tmp));
        }
    }
}
