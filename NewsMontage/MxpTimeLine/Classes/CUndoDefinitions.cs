﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MediaAlliance.Controls.MxpTimeLine.Classes;
using MediaAlliance.Controls.MxpTimeLine.Objects;


namespace MediaAlliance.Controls.MxpTimeLine.UndoDefinitions
{
    public abstract class CUndoInfo
    {
        public CMxpTL_Object object_ref { get; set; }
    }

    public class CUndoClip : CUndoInfo
    {
        public long TimecodeIn;
        public long TimecodeOut;
        public long ClippingIn;
        public long ClippingOut;
        public bool Visible;
        public int TrackID;

        public List<CUndoClip> UndoSubGroup = new List<CUndoClip>();

        public CMxpClip Clip = null;

        public CUndoClip(CMxpClip clip)
        {
            Import(clip, true);
        }

        public CUndoClip(CMxpClip clip, bool recursive)
        {
            Import(clip, recursive);
        }

        private void Import(CMxpClip clip, bool recursive)
        {
            Clip = clip;

            object_ref          = clip;

            TimecodeIn          = clip.TimcodeIn;
            TimecodeOut         = clip.TimecodeOut;
            ClippingIn          = clip.ClippingIn;
            ClippingOut         = clip.ClippingOut;
            Visible             = clip.Visible;
            TrackID             = clip.ParentTrack.ID;

            if (recursive)
            {
                foreach (CMxpClip subclip in clip.Group)
                {
                    this.UndoSubGroup.Add(new CUndoClip(subclip, false));
                }
            }
        }
    }
}
