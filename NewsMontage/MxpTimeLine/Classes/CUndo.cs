﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Reflection;
using MediaAlliance.Controls.MxpTimeLine;


namespace MediaAlliance.Controls.MxpTimeLine.UndoManager
{
    public class CUndoManager
    {
        // DELEGATES
        public delegate void Undo_OnAdded(object Sender, int total_remains);
        public delegate void Undo_OnRecall(object Sender, ref bool Cancel);
        public delegate void Undo_OnUndo(object Sender, int total_remains);
        public delegate void Undo_OnNewSnapshot(object Sender);

        // EVENTS
        public event Undo_OnAdded OnAdded               = null;
        public event Undo_OnRecall OnRecall             = null;
        public event Undo_OnUndo OnUndo                 = null;
        public event Undo_OnNewSnapshot OnNewSnapshot   = null;


        private int undoTimes = 10;
        private CAdvList<CUndoItem> undotable = new CAdvList<CUndoItem>();
        private CAdvList<CUndoItem> redotable = new CAdvList<CUndoItem>();


        private List<object> UndoSnapShot = new List<object>();
        private List<object> RedoSnapShot = new List<object>();

        private MxpTmeLine timeline_control = null;


        public CUndoManager(MxpTmeLine timeline_control)
        {
            this.timeline_control = timeline_control;
            undotable.OnItemAdded += new EventHandler<GenericItemEventArgs<CUndoItem>>(undotable_OnItemAdded);
        }


        #region UNDOTABLE EVENTS

        void undotable_OnItemAdded(object sender, GenericItemEventArgs<CUndoItem> e)
        {
            while (undotable.Count > undoTimes) undotable.RemoveAt(undotable.Count - 1);
        }

        #endregion UNDOTABLE EVENTS
        

        #region PUBLIC METHODS

        public void TakeSnapshot()
        {
            UndoSnapShot.Insert(0, timeline_control.Tracks.Serialize());
            if (OnAdded != null) OnAdded(this, UndoSnapShot.Count);

            while (UndoSnapShot.Count > undoTimes) UndoSnapShot.RemoveAt(UndoSnapShot.Count - 1);

            if (OnNewSnapshot != null) OnNewSnapshot(this);
        }

        public void Undo()
        {
            bool cancel = false;

            if (OnRecall != null) OnRecall(this, ref cancel);

            if (!cancel)
            {
                if (UndoSnapShot.Count > 0)
                {
                    timeline_control.Tracks.Deserialize((string)UndoSnapShot[0]);
                    RedoSnapShot.Add(UndoSnapShot[0]);
                    UndoSnapShot.RemoveAt(0);
                }

                if (OnUndo != null) OnUndo(this, UndoSnapShot.Count);
            }
        }

        public void Redo()
        {
            if (RedoSnapShot.Count > 0)
            {
                timeline_control.Tracks.Deserialize((string)RedoSnapShot[0]);
                UndoSnapShot.Add(RedoSnapShot[0]);
                RedoSnapShot.RemoveAt(0);
            }
        }

        #endregion PUBLIC METHODS
        

        #region PUBLIC PROPERTIES

        public int UndoTimes
        {
            get { return undoTimes; }
            set
            {
                if (value > 0 && value < 200)
                {
                    undoTimes = value;
                }
            }
        }

        public int CurrentStoredUndo
        {
            get { return undotable.Count; }
        }

        public int RemainingUndo
        {
            get { return UndoSnapShot.Count; }
        }

        #endregion PUBLIC PROPERTIES

    }


    public enum enUndoItemType
    {
        clipChangedClippin_in,
        clipChangedClippin_out,
        clipMoved,
        clipRemoved,
        clipAdded,
        audioNodeAdded,
        audioNodeChanged,
        audioNodeRemoved,
        trackAdded,
        trackRemoved
    }

    public class CUndoItem
    {

        public DateTime EventDate;
        public enUndoItemType UndoType;
        public object ObjectRef = null;

        public CUndoItem(enUndoItemType type, object objectref)
        {
            this.UndoType   = type;
            this.EventDate  = DateTime.Now;
            this.ObjectRef  = objectref;
        }
    }

    public enum UndoEventType
    {
        unknown,
        newSnapshowTaken,
        undoDone,
        redoDone
    }

    public class UndoEventHandler
    {
        public UndoEventType type = UndoEventType.unknown;
        public CUndoManager UndoManager = null;

        public UndoEventHandler(CUndoManager undoManger, UndoEventType type)
        {
            this.UndoManager = undoManger;
            this.type = type;
        }
    }
}
