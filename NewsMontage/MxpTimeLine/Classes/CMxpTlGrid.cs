using System;
using System.Collections.Generic;
using System.Text;

using System.Drawing;
using System.Diagnostics;



namespace MediaAlliance.Controls.MxpTimeLine.Classes
{
    public class CMxpTlGrid : CMxpTL_Object
    {
        // GRAPHICALS PUBLIC PROPERTIES
        private Color FBackColor = Color.FromArgb(47, 47, 47);

        // VARIABLES
        private Bitmap FRendered = null;
        private System.Drawing.Graphics FGrf = null;

        private Pen FBorderPen = new Pen(Brushes.Silver);
        private Font FFrameFont = new Font(new FontFamily("Tahoma"), 7);

        private Rectangle FGridBarBounds;

        private Point FStartDragPos = new Point(-1, -1);

        private long FZoomIn        = 0;
        private long FZoomOut       = 0;

        private double FPixUnit     = 0;
        private int FUStep          = 1;
        private int FTC_Width       = 0;
        private float fps           = 25;


        //private int[] FUnitStep;

        private bool FShowFrameNum = true;

        public KTL_GRIDTRACK_HITTEST_OBJ LastHitTested = KTL_GRIDTRACK_HITTEST_OBJ.None;

        public enum KTL_GRIDTRACK_HITTEST_OBJ
        { 
            None = 0,
            LeftButton = 1,
            RightButton = 2,
            TrackBar = 3,
            TrackSlider = 4,
            TrackSliderLeftHandle = 5,
            TrackSliderRightHandle = 6
        }

        // EVENTS
        public event MXPTL_OnRenderSubitem OnRender = null;
        public event MXPTL_ZoomAreaChanged OnZoomChanged = null;



        public CMxpTlGrid(MxpTmeLine owner): base(owner)
        {
            //FUnitStep = new int[] { 1, 5, 10, 20, 30, 40, 50, 55, 60, 65, 90, 100, 110, 120, 130, 140, 150, 160, 180, 190, 200};
        }



        public void Render()
        {
            if (FRendered != null && FGrf != null)
            {
                FGridBarBounds = new Rectangle(0, 0, width, height);
                FGrf.FillRectangle(new SolidBrush(FBackColor), FGridBarBounds);
                
                DrawTicks();
            }
        }

        private void DrawTicks()
        {
            if (FZoomOut > FZoomIn && FPixUnit != 0)
            {
                Int32 FrameToDraw = 0; //(int)(FZoomIn / FPixUnit) * (int)FPixUnit;
                //Int32 correctionDiff = (Int32)((width / (FZoomOut - FZoomIn)) * (FZoomIn % FUStep));

                Int32 xLine = FrameToPix(FrameToDraw); // = 0;// -correctionDiff;
                int count = 1;
                
                while (xLine < width + 200)
                {
                    if (xLine > -200)
                    {
                        int h = height / 2;
                        if (FrameToDraw % 2 != 0) h = height;

                        FGrf.DrawLine(new Pen(Brushes.Silver, 1), new Point(xLine, 0), new Point(xLine, 2));

                        if (!isResizing)
                        {
                            FGrf.DrawString(CTcTools.Frame2TC(FrameToDraw, timeline_control.FPS), 
                                            FFrameFont, 
                                            Brushes.Silver, 
                                            new Point(xLine - (FTC_Width / 2), 2));
                        }
                    }

                    FrameToDraw += FUStep;
                    xLine = FrameToPix(FrameToDraw);
                    count++;
                }
            }
        }

        private void UpdateVisualUnit()
        {
            if (FZoomOut > FZoomIn)
            {
                FPixUnit = (width / (FZoomOut - FZoomIn)) * FUStep;

                double xx = 50 / (((double)width / (FZoomOut - FZoomIn)));
                int gain = (int)xx / 5;
                FUStep = (gain * 5) + 15;
                FPixUnit = ((double)width / (FZoomOut - FZoomIn)) * FUStep;
            }
        }

        /// <summary>
        /// Restituisce la conversione in pixel dei frame specificati.
        /// </summary>
        /// <param name="frame"></param>
        /// <returns></returns>
        public int FrameInPix(long frame)
        {
            if (FZoomOut > FZoomIn && FGridBarBounds.Width > 0)
            {
                double OneFFSize = (double)width / (FZoomOut - FZoomIn);
                int ftp = (int)(frame * OneFFSize);

                return (ftp == 0) ? (int)(OneFFSize) : ftp;
            }
            return 0;
        }

        /// <summary>
        /// Restituisce la conversione in pixel dei frame specificati, nella vista corrente
        /// </summary>
        /// <param name="frame"></param>
        /// <returns></returns>
        public int FrameToPix(long frame)
        {
            if (FZoomOut > FZoomIn && FGridBarBounds.Width > 0)
            {
                //return FGridBarBounds.Width * frame / (FZoomOut - FZoomIn);

                double OneFFSize = (double)width / (FZoomOut - FZoomIn);
                int ftp = (int)((frame - FZoomIn) * OneFFSize);

                return (ftp == 0) ? (int)(OneFFSize) : ftp;
            }
            return 0;
        }

        public long PixToFrame(int pixel)
        {
            if (FZoomOut > FZoomIn && FGridBarBounds.Width > 0)
            {
                return (pixel * (FZoomOut - FZoomIn) / FGridBarBounds.Width) + FZoomIn;
            }
            return 0;
        }

        private void Resize()
        {
            if (width > 0 && height > 0)
            {
                if (FRendered != null)
                {
                    FRendered.Dispose();
                    FRendered = null;
                    FGrf.Dispose();
                    FGrf = null;
                }

                FRendered = new Bitmap(width, height);
                FGrf = System.Drawing.Graphics.FromImage(FRendered);
                FGrf.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                FTC_Width = (int)FGrf.MeasureString("00:00:00:00", FFrameFont).Width + 5;

                UpdateVisualUnit();
            }
        }

        public KTL_GRIDTRACK_HITTEST_OBJ GetHitTest(Point p)
        {
            /*
            if (IsInside(FTrackSlideLeftHandleBounds, p)) LastHitTested = KTL_GRIDTRACK_HITTEST_OBJ.TrackSliderLeftHandle;
            else if (IsInside(FTrackSlideRightHandleBounds, p)) LastHitTested = KTL_GRIDTRACK_HITTEST_OBJ.TrackSliderRightHandle;
            else if (IsInside(FTrackSlideBounds, p)) LastHitTested = KTL_GRIDTRACK_HITTEST_OBJ.TrackSlider;
            else if (IsInside(FTrackBounds, p)) LastHitTested = KTL_GRIDTRACK_HITTEST_OBJ.TrackBar;
            else if (IsInside(FTrackLeftButtonBounds, p)) LastHitTested = KTL_GRIDTRACK_HITTEST_OBJ.LeftButton;
            else if (IsInside(FTrackRightButtonBounds, p)) LastHitTested = KTL_GRIDTRACK_HITTEST_OBJ.RightButton;
            else LastHitTested = KTL_GRIDTRACK_HITTEST_OBJ.None;
            */
            return LastHitTested;
        }

        private bool IsInside(Rectangle rect, Point p)
        {
            return (p.X >= rect.Left && p.X <= rect.Right && p.Y >= rect.Top && p.Y <= rect.Bottom);
        }



        protected override void InternalChangeNotification(enInternalChangeNotificationType cType)
        {
            switch (cType)
            {
                case enInternalChangeNotificationType.x:
                    break;
                case enInternalChangeNotificationType.y:
                    break;
                case enInternalChangeNotificationType.position:
                    break;
                case enInternalChangeNotificationType.width:
                    this.IsChanged = true;
                    Resize();
                    UpdateVisualUnit();
                    Render();
                    break;
                case enInternalChangeNotificationType.height:
                    this.IsChanged = true;
                    Resize();
                    UpdateVisualUnit();
                    Render();
                    break;
                case enInternalChangeNotificationType.bound:
                    this.IsChanged = true;
                    Resize();
                    UpdateVisualUnit();
                    Render();
                    break;
                case enInternalChangeNotificationType.isResizing:
                    if (!isResizing)
                    {
                        Render();
                        if (OnRender != null) OnRender(this);
                    }
                    break;
                case enInternalChangeNotificationType.none:
                default:
                    break;
            }
        }

        public override string Serialize()
        {
            return "";
        }

        public override void Deserialize(string xml)
        {
            throw new NotImplementedException();
        }



        #region PUBLIC METHODS

        public void SetZoom(long newZoomIn, long newZoomOut)
        {
            if (FZoomIn != newZoomIn || FZoomOut != newZoomOut)
            {
                FZoomIn = newZoomIn;
                FZoomOut = newZoomOut;
                UpdateVisualUnit();
                Render();
            }
        }

        #endregion PUBLIC METHODS
        



        #region PUBLIC PROPERTIES
 
        public Bitmap Image
        {
            get { return FRendered; }
        }

        public long TCin
        {
            get { return FZoomIn; }
            set 
            {
                if (FZoomIn != value)
                {
                    FZoomIn = value;
                    UpdateVisualUnit();
                    Render();
                }
            }
        }

        public long TCOut
        {
            get { return FZoomOut; }
            set 
            {
                if (FZoomOut != value)
                {
                    FZoomOut = value;
                    UpdateVisualUnit();
                    Render();
                }
            }
        }

        public float FPS
        {
            get { return fps; }
            set
            {
                if (fps != value)
                {
                    fps = value;

                    Render();
                }
            }
        }

        #endregion


    }
}
