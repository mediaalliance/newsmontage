﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
using System.ComponentModel;
using System.Runtime.InteropServices;


namespace MediaAlliance.Controls.MxpTimeLine.Engine
{
    public enum InternalEngineStatus
    {
        none,
        paused,
        play
    }

    public class CTimeLine_Engine :IDisposable
    {
        #region WIN32

        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("User32.dll", SetLastError = true)]
        public static extern bool PostMessage(IntPtr hWnd, uint uMsg, int wParam, int lParam);

        public const uint WM_USER       = 0x0400;
        public const uint TICK_MESSAGE  = WM_USER + 1234;

        #endregion WIN32



        // DELEGATES
        public delegate void Engine_OnChangePosition(long position);
        public delegate void Engine_OnEventHandle(object Sender, InternEngineEventHandle e);

        private delegate void dlgTick();


        // EVENTS
        public event Engine_OnChangePosition OnChangePosition   = null;
        public event Engine_OnEventHandle OnEventHandle         = null;


        private Thread th_engine            = null;
        private bool th_engine_stop         = false;


        private MxpTmeLine owner            = null;
        private volatile IntPtr OwnerHandle = IntPtr.Zero;

        private long tl_position            = 0;


        public InternalEngineStatus EngineStatus  = InternalEngineStatus.none;
        private int increment               = 1;





        public CTimeLine_Engine(MxpTmeLine owner)
        {
            this.owner          = owner;
            this.OwnerHandle    = owner.Handle;

            owner.OnChangePosition += new MXPTL_OnChangePosition(owner_OnChangePosition);

            th_engine                   = new Thread(new ThreadStart(th_engine_Task));
            th_engine.IsBackground      = true;
            th_engine.Priority          = ThreadPriority.Highest;
            th_engine.Start();
        }





        private void th_engine_Task()
        {
            dlgTick tick = new dlgTick(tick_task);

            while (!th_engine_stop)
            {
                switch (EngineStatus)
                {
                    case InternalEngineStatus.none:
                        break;
                    case InternalEngineStatus.paused:
                        break;
                    case InternalEngineStatus.play:
                        tl_position += increment;
                        //owner.Invoke(tick);

                        bool sent = PostMessage(OwnerHandle, TICK_MESSAGE, (int)tl_position, 0);

                        if (!sent)
                        {
                            // An error occured
                            Win32Exception wex = new Win32Exception(Marshal.GetLastWin32Error());

                        }
                        //Raise_OnChangePosition(tl_position);
                        break;
                    default:
                        break;
                }

                Thread.Sleep(40);
            }
        }

        private void tick_task()
        {
            if (EngineStatus == InternalEngineStatus.play)
            {
                if (owner.CurrentPosition != tl_position)
                {
                    owner.CurrentPosition = tl_position;
                }
            }
        }
        
        private void Raise_OnChangePosition(long position)
        {
            if (OnChangePosition != null)
            {
                foreach (Engine_OnChangePosition singleCast in OnChangePosition.GetInvocationList())
                {
                    ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;
                    try
                    {
                        if (syncInvoke != null && syncInvoke.InvokeRequired)
                        {
                            // Invokie the event on the main thread
                            syncInvoke.Invoke(OnChangePosition, new object[] { position });
                        }
                        else
                        {
                            // Raise the event
                            singleCast(position);
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }
        
        public void Dispose()
        {
            th_engine_stop = true;

            if (!th_engine.Join(200))
            {
                th_engine.Abort();
                th_engine = null;
            }
        }



        #region TIMELINE EVENTS

        void owner_OnChangePosition(object Sender, long position, bool Internal)
        {
            if (EngineStatus != InternalEngineStatus.play)
            {
                tl_position = position;
            }
        }
        
        #endregion TIMELINE EVENTS


        DateTime dt;

        #region PUBLIC METHODS

        public void Play()
        {
            if (owner.Enabled)
            {
                EngineStatus = InternalEngineStatus.play;
                dt = DateTime.Now;

                if (OnEventHandle != null) OnEventHandle(this, new InternEngineEventHandle(enIEngineEventType.onPlay, tl_position));
            }
        }

        public void PlayMark()
        {
            if (owner.Enabled)
            {
                owner.CurrentPosition = owner.CurrentMarkIn;
                tl_position = owner.CurrentPosition;
                EngineStatus = InternalEngineStatus.play;
                dt = DateTime.Now;

                //if (OnEventHandle != null) OnEventHandle(this, new InternEngineEventHandle(enIEngineEventType.onPlayMark, tl_position));
            }
        }

        public void Pause()
        {
            if (owner.Enabled)
            {
                EngineStatus = InternalEngineStatus.paused;

                TimeSpan ts = DateTime.Now - dt;


                string xx = ts.TotalSeconds.ToString() + "," + Convert.ToString(ts.Milliseconds * 10);

                if (OnEventHandle != null) OnEventHandle(this, new InternEngineEventHandle(enIEngineEventType.onPause, tl_position));
            }
        }

        #endregion PUBLIC METHODS
    }




    public enum enIEngineEventType
    {
        undefined       = 0,
        onPlay          = 1,
        onPause         = 2,
        onInitialized   = 3,
        onPlayMark      = 4
    }



    public class InternEngineEventHandle
    {
        public enIEngineEventType Type  = enIEngineEventType.undefined;
        public string Message           = "";
        public long CurrentPosition     = -1;
        
        public InternEngineEventHandle(enIEngineEventType type, long Position)
        {
            this.Type               = type;
            this.CurrentPosition    = Position;
        }

        public InternEngineEventHandle(enIEngineEventType type, string message)
        {
            this.Type               = type;
            this.Message            = message;
        }


    }
}
