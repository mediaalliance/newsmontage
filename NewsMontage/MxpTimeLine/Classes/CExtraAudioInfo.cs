﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Drawing;
using MediaAlliance.Controls.MxpTimeLine.Objects;



namespace MediaAlliance.Controls.MxpTimeLine.AudioInfo
{

    public class CMxpClip_ExtraAudioInfo
    {
        private CMxpClip owner = null;
        private CAudioNodeInfo_Collection audioNodes = new CAudioNodeInfo_Collection();

        private long lastDuration   = 0;
        private float main0gain     = 1;

        public float FMaxGainValue  = 0;
        public float FMinGainValue  = -1;



        public CMxpClip_ExtraAudioInfo(CMxpClip Owner)
        {
            this.owner = Owner;
            UpdateDuration(owner.TimecodeOut - owner.TimcodeIn);

            owner.OnChangedTC       += new CMxpClip.MxpClip_OnChangedTC(owner_OnChangedTC);
            owner.OnBoundsChanged   += new CMxpClip.MxpClip_OnBoundsChanged(owner_OnBoundsChanged);
        }



        #region CLIP PARENT EVENTS

        private void owner_OnBoundsChanged(object Sender, Rectangle oldBounds, Rectangle newBounds)
        {
            //throw new NotImplementedException();
        }

        private void owner_OnChangedTC(object Sender, long TCIn, long TCOut, enTcChangeType cType)
        {
            UpdateDuration(TCOut - TCIn);
        }
        
        #endregion CLIP PARENT EVENTS



        private void UpdateDuration(long newduration)
        {
            if (lastDuration != newduration && lastDuration > 0)
            {
                //for (int i = 0; i < timecodes.Length; i++)
                //{
                //    audioNodes.Add(new CAudioNodeInfo(timecodes[i] * newduration / lastDuration, values[i]);
                //}
            }
            else if (lastDuration == 0 && audioNodes.Count == 0 && newduration != 00)
            {
                // AGGIUNGO PER DEFAULT DUE VALORI DI GUADAGNO A 0 INIZIO E FINE
                // IL TIMECODE IN audioNodes E' RELATIVO

                //this.audioNodes.Add(new CAudioNodeInfo(0, 1F));
                //this.audioNodes.Add(new CAudioNodeInfo(newduration, 1F));
            }

            lastDuration = newduration;
        }

        private int CompareByTimecode(CAudioNodeInfo item1, CAudioNodeInfo item2)
        {
            if (item1.timecode > item2.timecode) return 1;
            if (item1.timecode < item2.timecode) return -1;
            return 0;
        }



        #region PUBLIC METHODS

        /// <summary>
        /// Aggiunge un nuovo nodo audio per la clip.
        /// </summary>
        /// <param name="tc">Timecode del punto rispetto il timecode-in della clip, non il clipping-in.</param>
        /// <param name="db">Valore dell'audio espresso in float a 1 a 0</param>
        public void AddNode(long tc, float db)
        {
            CAudioNodeInfo exist = audioNodes[tc];

            if (exist != null)
            {
                exist.timecode = tc - owner.TimcodeIn;
                exist.value = db;
            }
            else
            {
                //audioNodes.Add(new CAudioNodeInfo(tc - owner.TimcodeIn, db));
                audioNodes.Add(new CAudioNodeInfo(tc, db));
            }

            owner.NotifyRender();
        }

        public void Clear()
        {
            audioNodes.Clear();
            owner.NotifyRender();
        }

        public void RemoveNode(long tc)
        {
            CAudioNodeInfo to_remove = audioNodes[tc];
            if (to_remove != null) audioNodes.Remove(to_remove);
            
            owner.NotifyRender();
        }

        public void ChangeNode(CAudioNodeInfo nodeinfo, long newTc, float newValue)
        {
            long ntc = newTc - owner.TimcodeIn;

            nodeinfo.timecode = ntc;
            nodeinfo.value = newValue;

            owner.NotifyRender();
        }
        
        public List<CAudioNodeInfo> GetAllNodes()
        {
            audioNodes.Sort(CompareByTimecode);

            return audioNodes;
        }

        public float GetValueAt(long position)
        {
            CAudioNodeInfo exact = audioNodes[position];

            if (exact == null)
            {
                List<CAudioNodeInfo> before = new List<CAudioNodeInfo>(audioNodes.Where(x => x.timecode < position).OrderBy(x => x.timecode));
                List<CAudioNodeInfo> after  = new List<CAudioNodeInfo>(audioNodes.Where(x => x.timecode > position).OrderBy(x => x.timecode));

                float a = -30000;
                float b = -30000;

                if (before.Count > 0) a = before[before.Count - 1].value;
                if (after.Count > 0) b = after[0].value;

                if (a == -30000 && b == -30000) return this.Main0gain;
                if (a == -30000) return b * this.Main0gain;
                if (b == -30000) return a * this.Main0gain;
                if (a == 0 && b == 0) return this.Main0gain;

                long tc_diff = position - before[before.Count - 1].timecode;

                double ipo = PointsDistance((float)before[before.Count - 1].timecode, a, (float)after[0].timecode, b);
                double alp = Math.Asin((b - a) / ipo);

                double ip2 = tc_diff * Math.Cos(alp);

                float p_value = (float)(a + (ip2 * Math.Sin(alp)));

                p_value *= this.Main0gain;

                return p_value;
            }
            else
            {
                return exact.value * this.Main0gain;
            }
            
        }

        public float ConformValue(float value)
        {
            if (value > FMaxGainValue) return FMaxGainValue;
            if (value < FMinGainValue) return FMinGainValue;
            return value;
        }

        public CMxpClip_ExtraAudioInfo Clone(CMxpClip newowner)
        {
            CMxpClip_ExtraAudioInfo clone = new CMxpClip_ExtraAudioInfo(newowner);

            clone.FMaxGainValue = FMaxGainValue;
            clone.FMinGainValue = FMinGainValue;
            clone.lastDuration  = lastDuration;
            clone.main0gain     = main0gain;

            foreach (CAudioNodeInfo an in this.audioNodes)
            {
                clone.audioNodes.Add(new CAudioNodeInfo(an));
            }

            return clone;
        }

        #endregion PUBLIC METHODS



        #region PUBLIC PROPERTIES

        public float Main0gain
        {
            get { return main0gain; }
            set
            {
                if (main0gain != value)
                {
                    main0gain = value;
                    owner.NotifyRender();
                }
            }
        }

        #endregion PUBLIC PROPERTIES


        private double PointsDistance(float x1, float y1, float x2, float y2)
        {
            return Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
        }
    }

    public class CAudioNodeInfo_Collection : List<CAudioNodeInfo>
    {
        public CAudioNodeInfo this[long tc]
        {
            get
            {
                foreach (CAudioNodeInfo ai in this)
                {
                    if (ai.timecode == tc) return ai;
                }

                return null;
            }
        }
    }
}
