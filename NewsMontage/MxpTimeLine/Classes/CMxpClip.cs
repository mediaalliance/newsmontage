using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;


using System.Drawing;
using System.Drawing.Drawing2D;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Reflection;

using MediaAlliance.Controls.MxpTimeLine.Classes;
using MediaAlliance.Controls.MxpTimeLine.AudioInfo;
using MediaAlliance.Controls.MxpTimeLine.UndoDefinitions;
using MediaAlliance.Controls.MxpTimeLine.Objects;


namespace MediaAlliance.Controls.MxpTimeLine.Objects
{
    public delegate void MxpClip_OnVirtualObjectNeed(object Sender, Rectangle rect);

    public enum enCMxpClipType
    { 
        Video           = 0,
        Audio           = 1
    }

    [Flags]
    public enum MxpClip_HitTest_Info
    {
        none            = 0,
        leftHandle      = 1,
        rightHandle     = 2,
        thumb           = 3,
        audioNode       = 4
    }

    public enum MxpClip_ThumbPosition
    { 
        Absolute,
        Begining,
        Ending
    }

    public class CMxpClips_Collection : CAdvList<CMxpClip>
    {        
        public CMxpClips_Collection() : base() { }

        public CMxpClip Add(string Title, enCMxpClipType type, CMxpTL_ClipInfo clip_info, object Tag, long timecode_in, long timecode_out, CMxpTrack ParentTrack)
        {
            CMxpClip clip   = Add(Title, type, clip_info, Tag, timecode_in, timecode_out, 0, 0, ParentTrack);
            return clip;
        }

        public CMxpClip Add(string Title, enCMxpClipType type, CMxpTL_ClipInfo clip_info, object Tag, long timecode_in, long timecode_out, long clipping_in, long clipping_out, CMxpTrack ParentTrack)
        {
            CMxpClip clip       = new CMxpClip((CMxpTL_Object)ParentTrack, true);
            //clip.ID             = this.Count;
            clip.TimcodeIn      = timecode_in;
            clip.TimecodeOut    = timecode_out;
            clip.Title          = Title;
            clip.Tag            = Tag;
            clip.Type           = type;
            clip.ClipInfo       = clip_info;

            if (clipping_in == 0 && clipping_out == 0)
            {
                clip.IsFreeClip = true;
            }
            else
            {
                clip.ClippingIn     = (clipping_in >= 0) ? clipping_in : 0;
                //clip.ClippingOut    = (clipping_out > clipping_in && clipping_out <= timecode_out) ? clipping_out : 0;
                clip.ClippingOut = clipping_out;
            }

            this.Add(clip);

            clip.PreventRender = false;
            clip.NotifyRender();
            return clip;
        }

        public bool ContainClip(CMxpClip clip)
        {
            foreach (CMxpClip cl in this)
            {
                if (cl.ID == clip.ID)
                {
                    return true;
                }
            }

            return false;
        }

        public List<CMxpClip> ToList()
        { 
            return new List<CMxpClip>(this.ToArray());    
        }
    }

    public class CMxpClipThumb
    {
        private long timeCode = 0;
        private Bitmap bitmap = null;

        public CMxpClipThumb(long tc, Bitmap bmp)
        {
            this.timeCode   = tc;
            this.bitmap     = bmp;
        }

        public long Timecode { get { return timeCode; } set { timeCode = value; } }

        public Bitmap Bitmap 
        { 
            get { return bitmap; }
            set
            {
                if (value != bitmap)
                {
                    bitmap = value;
                }
            }
        }

        public CMxpClipThumb Clone()
        {
            return new CMxpClipThumb(timeCode, 
                                     (this.bitmap != null)?(Bitmap)this.bitmap.Clone() : null);            
        }
    }

    public class CMxpClipThumb_Colletion : CAdvList<CMxpClipThumb>
    {
        public bool Contain_TC(long tc)
        {
            List<CMxpClipThumb> th = this.Where(x => x.Timecode == tc).ToList();
            return th.Count > 0;
        }

        public CMxpClipThumb GetMinimumTC()
        {
            if (this.Count > 0)
            {
                List<CMxpClipThumb> th = this.OrderBy(x => x.Timecode, new TimecodeComparer()).ToList();
                return th[0];
            }
            return null;
        }

        public CMxpClipThumb GetMaximumTC()
        {
            if (this.Count > 0)
            {
                List<CMxpClipThumb> th = this.OrderByDescending(x => x.Timecode, new TimecodeComparer()).ToList();
                return th[0];
            }
            return null;
        }

        private class TimecodeComparer : IComparer<long>
        {
            public int Compare(long a, long b)
            {
                if (a > b) return 1;
                if (a < b) return -1;
                return 0;
            }
        }
    }


    public class CDisplayInfo
    {
        public bool Changed                 = false;

        private int firstVisible_X          = 0;
        private int lastVisible_X           = 0;
        private int visibleWidth            = 0;

        private long firstVisible_TC        = 0;
        private long lastVisible_TC         = 0;
        private long visible_Duration       = 0;

        private long m_firstVisibleTcOfClip = 0;
        private long m_lastVisibleTcOfClip  = 0;

        public int FirstVisible_X
        {
            get { return firstVisible_X; }
            set
            {
                if (value != firstVisible_X)
                {
                    firstVisible_X = value;
                    Changed = true;
                }
            }
        }

        public int LastVisible_X
        {
            get { return lastVisible_X; }
            set
            {
                if (value != lastVisible_X)
                {
                    lastVisible_X = value;
                    Changed = true;
                }
            }
        }

        public int VisibleWidth
        {
            get { return visibleWidth; }
            set
            {
                if (value != visibleWidth)
                {
                    visibleWidth = value;
                    Changed = true;
                }
            }
        }

        public long FirstVisible_TC
        {
            get { return firstVisible_TC; }
            set
            {
                if (value != firstVisible_TC)
                {
                    firstVisible_TC = value;
                    Changed = true;
                }
            }
        }

        public long LastVisible_TC
        {
            get { return lastVisible_TC; }
            set
            {
                if (value != lastVisible_TC)
                {
                    lastVisible_TC = value;
                    Changed = true;
                }
            }
        }

        public long FirstVisible_TC_OfClip
        {
            get { return m_firstVisibleTcOfClip; }
            set
            {
                if (value != m_firstVisibleTcOfClip)
                {
                    m_firstVisibleTcOfClip = value;
                }
            }
        }

        public long LastVisible_TC_OfClip
        {
            get { return m_lastVisibleTcOfClip; }
            set
            {
                if (value != m_lastVisibleTcOfClip)
                {
                    m_lastVisibleTcOfClip = value;
                }
            }
        }

        public long Visible_Duration
        {
            get { return visible_Duration; }
            set
            {
                if (value != visible_Duration)
                {
                    visible_Duration = value;
                    Changed = true;
                }
            }
        }
    }



    public class CMxpClip : CMxpTL_Object
    {
        // GRAPHICAL PUBLIC PROPERTIES
        private Color videoBackColor        = Color.LightSteelBlue;
        private Color audioBackColor        = Color.FromArgb(182, 207, 177);
        private Color backColor             = Color.LightSteelBlue;
        private Color FSelectedClipColor    = Color.LightSalmon;
        private Font FFontTitle             = new Font("Tahoma", 8); 

        // DELEGATES
        public delegate void MxpClip_OnChangeSelected(object Sender, bool Selected);
        public delegate void MxpClip_OnChangeTitle(object Sender);
        public delegate void MxpClip_OnEdit(object Sender, bool Start);
        public delegate void MxpClip_OnDeleteClip(object Sender);
        public delegate void MxpClip_OnBeforeRender(object Sender, Graphics graph, int width, int height, long tcin, long tcout);
        public delegate void MxpClip_OnRender(object Sender);
        public delegate void MxpClip_OnChangedTC(object Sender, long TCIn, long TCOut, enTcChangeType cType);
        public delegate void MxpClip_OnBoundsChanged(object Sender, Rectangle oldBounds, Rectangle newBounds);
        public delegate void MxpClip_OnDisplayInfoChange(object Sender);
        
        // EVENTS
        public event MxpClip_OnChangeSelected OnChangeSelected          = null;
        public event MxpClip_OnChangeTitle OnChangeTitle                = null;
        public event MxpClip_OnEdit OnEdit                              = null;
        public event MxpClip_OnDeleteClip OnDeleteClip                  = null;
        public event MxpClip_OnBeforeRender OnBeforeRender              = null;
        public event MxpClip_OnRender OnRender                          = null;
        public event MxpClip_OnChangedTC OnChangedTC                    = null;
        public event MxpClip_OnBoundsChanged OnBoundsChanged            = null;
        public event MxpClip_OnVirtualObjectNeed OnVirtualObjectNeed    = null;
        public event MxpClip_OnDisplayInfoChange OnDisplayInfoChange    = null;

        // CLASSES
        private ExtendedGraphics FExtendGraph = null;
        [XmlIgnore()]
        public CDisplayInfo DisplayInfo         = new CDisplayInfo();
        public CMxpTL_ClipInfo ClipInfo         = new CMxpTL_ClipInfo();

        // PUBLIC FIELDS
        public int Virtual_X            = 0;
        public int Virtual_Width        = 0;


        // PRIVATE FIELDS
        private Guid id                 = Guid.NewGuid();
        private long tcin               = 0;
        private long tcout              = 0;
        private long clipping_in        = 0;
        private long clipping_out       = 0;
        private bool selected           = false;
        private object tag              = null;
        private enCMxpClipType type     = enCMxpClipType.Video;
        private string title            = "";
        private MxpClipThumbLayout thumbLayout = MxpClipThumbLayout.firstAndLast;

        private CMxpClipThumb firstThumb    = null;         // Usato nel caso di MxpClipThumbLayout.firstAndLast        
        private CMxpClipThumb lastThumb     = null;         // Usato nel caso di MxpClipThumbLayout.firstAndLast        

        private CMxpClipThumb_Colletion FMyThumb = new CMxpClipThumb_Colletion();

        private bool showTitle                  = true;

        private bool isEditing                  = false;
        private bool isVisible                  = false;
        private bool isMoving                   = false;
        private bool isFreeClip                 = false;
        private bool visible                    = true;
        private bool isDeleted                  = false;
        private bool canEditAudio               = true;
        private bool m_invertedFieldOrder       = false;

        private Rectangle clipBounds            = new Rectangle(0, 0, 0, 0);
        private Rectangle audio0trackBar        = new Rectangle(0, 0, 0, 0);
        private Rectangle audio0trackCursor     = new Rectangle(0, 0, 0, 0);

        private CMxpClip_ExtraAudioInfo audioInfo = null;


        private Pen FBorderPen          = new Pen(Brushes.Black, 1);
        private Font FFont              = new Font(FontFamily.GenericSansSerif, 8, FontStyle.Regular);

        private Bitmap FRendered        = null;
        private Graphics FGrf           = null;

        private Bitmap backgraoundImage = null;


        // PUBLIC FIELDS

        [XmlIgnore()]
        public MxpClip_HitTest_Info LastHitTest = MxpClip_HitTest_Info.none;
        [XmlIgnore()]
        public object LastHitTestObject = null;
        [XmlIgnore()]
        public Point LastHitXPosition;
        [XmlIgnore()]
        public long LastHitTimecode;
        [XmlIgnore()]
        public long Last_magnet_tc      = 0;

        public CMxpClipGroup Group      = null;

        public bool PreventRender       = false;


        public enum MxpClipBgImageLayout
        { 
            onVisibleArea,
            fromPerformaceX,
            fromVirtualX
        }

        public MxpClipBgImageLayout BackgroungImageLayout = MxpClipBgImageLayout.onVisibleArea;


        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public CMxpClip()
        {
            this.Group = new CMxpClipGroup(this);

            foreach (MxpClip_HitTest_Info ht in Enum.GetValues(typeof(MxpClip_HitTest_Info)))
            {
                this.HitTest_Areas.Add(ht, new Rectangle());
            }

            this.width = 2;
            this.height = 2;


            FRendered = new Bitmap(width, height);
            FGrf = System.Drawing.Graphics.FromImage(FRendered);
            FGrf.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            FExtendGraph = new ExtendedGraphics(FGrf);

            FMyThumb.OnItemsCleared += new EventHandler<EventArgs>(FMyThumb_OnItemsCleared);
        }
        
        public CMxpClip(CMxpTL_Object parent, bool preventrender_onstart) : base(parent)
        {
            // INIZIALIZZO TUTTE LE HITTEST AREA POSSIBILI

            this.PreventRender = preventrender_onstart;

            foreach (MxpClip_HitTest_Info ht in Enum.GetValues(typeof(MxpClip_HitTest_Info)))
            {
                this.HitTest_Areas.Add(ht, new Rectangle());
            }


            this.Group          = new CMxpClipGroup(this);

            this.width          = 2;
            this.height         = parent.Height;

            FRendered           = new Bitmap(width, height);
            FGrf                = System.Drawing.Graphics.FromImage(FRendered);
            FGrf.SmoothingMode  = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            FExtendGraph        = new ExtendedGraphics(FGrf);

            FMyThumb.OnItemsCleared += new EventHandler<EventArgs>(FMyThumb_OnItemsCleared);
        }




        private Color GetDarkenColor(Color source, int factor)
        {
            int R = source.R;
            int G = source.G;
            int B = source.B;

            R -= factor;
            G -= factor;
            B -= factor;

            if (R < 0) R = 0;
            if (G < 0) G = 0;
            if (B < 0) B = 0;

            return Color.FromArgb(R, G, B);
        }

        private Color GetLightenColor(Color source, int factor)
        {
            int R = source.R;
            int G = source.G;
            int B = source.B;

            R += factor;
            G += factor;
            B += factor;

            if (R > 255) R = 255;
            if (G > 255) G = 255;
            if (B > 255) B = 255;

            return Color.FromArgb(R, G, B);
        }

        private void Render()
        {
            Render(true);
        }

        private void Render(bool raiseEvents)
        {
            UpdateDisplayedInfo();

            if (PreventRender) return;

            if (width == 2 && height == 2) return;

            if (width > 0 && height > 0)
            {
                if (FRendered.Width != width || FRendered.Height != height)
                {
                    if (width < 0 || height < 0) return;
                    FRendered.Dispose();
                    FGrf.Dispose();

                    FRendered = new Bitmap(width, height);
                    FGrf = System.Drawing.Graphics.FromImage(FRendered);
                    FGrf.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                }

                if (OnBeforeRender != null && raiseEvents) 
                {
                    long first_tc_visible   = this.clipping_in;
                    long last_tc_visinle    = this.clipping_out;
                    
                    OnBeforeRender(this, FGrf, this.width, this.height, first_tc_visible, last_tc_visinle);
                }

                switch (this.type)
                {
                    case enCMxpClipType.Audio:
                        Render_AudioType();
                        break;
                    default:
                        Render_VideoType();
                        break;
                }

                if (!this.isFreeClip)
                {
                    try
                    {
                        if (clipping_in > 0) FGrf.DrawImage(Properties.Resources.ClippedIn, 0, 4);
                        if (tcin + clipping_out < tcout) FGrf.DrawImage(Properties.Resources.ClippedOff, width - 4, 4);
                    }
                    catch (System.Exception ex)
                    {
                    	
                    }
                }

                if (m_invertedFieldOrder)
                {
                    FGrf.DrawImage(Properties.Resources.ICO_FieldsInverted, 
                                    10, 
                                    10, 
                                    Properties.Resources.ICO_FieldsInverted.Width,
                                    Properties.Resources.ICO_FieldsInverted.Height);
                }

                if (OnRender != null) OnRender(this);
            }
        }

        private void Render_VideoType()
        {
            if (width > 0 && height > 0)
            {
                // DISEGNO LA BASE

                int usable_width = width - 1;

                if (this.x + usable_width > Parent.Width + 40)
                {
                    usable_width = Parent.Width + 40 - X;
                }

                if (!isResizing)
                {
                    //GraphicsPath gp = DrawRoundRect(FGrf, FBorderPen, 0, 0, usable_width, height - 2, 2);
                    GraphicsPath gp = DrawRoundRect(FGrf, FBorderPen, 0, 0, width, height - 2, 2);

                    if (isEditing)
                    {
                        FGrf.FillPath(new SolidBrush(Color.BlueViolet), gp);
                    }
                    else if (selected)
                    {
                        FGrf.FillPath(new SolidBrush(GetLightenColor(backColor, 40)), gp);
                    }
                    else
                    {
                        FGrf.FillPath(new SolidBrush(backColor), gp);
                    }

                    if (BackgraoundImage != null)
                    {
                        FGrf.Clip = new Region(gp);

                        int bgpic_x = 0;

                        if (this.x < 0) bgpic_x = Math.Abs(this.x);

                        FGrf.DrawImage(BackgraoundImage, bgpic_x, 0);

                        FGrf.Clip = new Region(new Rectangle(0, 0, width, height));
                    }

                    if (selected) FGrf.DrawPath(new Pen(Color.LightCoral, 1), gp);
                }
                else
                {
                    // DISEGNO UN RETTANGOLO DI INGOMBRO SEMPLICE DURANTE IL RISCALO
                    //FGrf.FillRectangle(new SolidBrush(videoBackColor), new Rectangle(0, 0, usable_width, height - 2));
                    FGrf.FillRectangle(new SolidBrush(backColor), new Rectangle(0, 0, width, height - 2));

                    return;
                }
                

                int lastX = 5;

                // DISEGNO LA THUMB
                

                switch (thumbLayout)
                {
                    case MxpClipThumbLayout.none:
                        break;
                    case MxpClipThumbLayout.firstOnly:
                        break;
                    case MxpClipThumbLayout.firstAndLast:
                        {
                            #region FIRST AND LAST

                            CMxpClipThumb min = (firstThumb != null)? firstThumb.Clone() : null;
                            CMxpClipThumb max = (lastThumb != null) ? lastThumb.Clone() : null; 

                            int h = height - 4;
                            int w = h / 3 * 4;

                            if (max != null)
                            {
                                Console.WriteLine(" DRAWING LAST AS : " + lastThumb.Timecode);
                                Rectangle destThumb = new Rectangle(width - w - 5, 2, w, h);
                                Rectangle srcThumb = new Rectangle(0, 0, max.Bitmap.Width, max.Bitmap.Height);
                                        
                                if (destThumb.Width > width - 10)
                                {
                                    destThumb.Width     = width - 10;
                                    destThumb.Location  = new Point(width - 5 - destThumb.Width, destThumb.Y);
                                    srcThumb.Width      = destThumb.Width;
                                    srcThumb.Location   = new Point(max.Bitmap.Width - destThumb.Width, srcThumb.Y);
                                }

                                FGrf.DrawImage(max.Bitmap, destThumb, srcThumb, GraphicsUnit.Pixel);
                            }

                            if (min != null)
                            {
                                Rectangle destThumb = new Rectangle(5, 2, w, h);
                                Rectangle srcThumb = new Rectangle(0, 0, min.Bitmap.Width, min.Bitmap.Height);
                                        
                                if (destThumb.Width > width - 10)
                                {
                                    destThumb.Width = width - 10;
                                    srcThumb.Width  = width - 10;
                                }

                                FGrf.DrawImage(min.Bitmap, destThumb, srcThumb, GraphicsUnit.Pixel);
                            }

                            if(min != null && min.Bitmap != null) min.Bitmap.Dispose();
                            if(max != null && max.Bitmap != null) max.Bitmap.Dispose();
                            min = null;
                            max = null;

                            #endregion FIRST AND LAST
                        }
                        break;
                    case MxpClipThumbLayout.allPossible:
                        {
                            #region ALL POSSIBLE

                            double timeXpix = (double)tcout - (double)tcin / width;

                            //List<CMxpClipThumb> filteres_thumbs = FMyThumb.Where(x => x.Timecode >= clipping_in && x.Timecode <= clipping_out).ToList();
                            List<CMxpClipThumb> filteres_thumbs = FMyThumb.Where(x => x.Timecode >= DisplayInfo.FirstVisible_TC_OfClip && 
                                                                                      x.Timecode <= DisplayInfo.LastVisible_TC_OfClip).ToList();
                            List<CMxpClipThumb> visible_thumbs = filteres_thumbs.OrderByDescending(x => x.Timecode).ToList();

                            //List<CMxpClipThumb> visible_thumbs = FMyThumb.OrderByDescending(x => x.Timecode).ToList();

                            foreach (CMxpClipThumb thumb in visible_thumbs)
                            {
                                //Console.WriteLine("THUMB NUM : " + visible_thumbs.Count);
                                int px = ParentTrack.FrameToPix(thumb.Timecode + tcin);

                                if (px > -200)
                                {
                                    int thumb_height = thumb.Bitmap.Height;
                                    int thumb_width = thumb.Bitmap.Width;

                                    long ff = thumb.Timecode - clipping_in;

                                    //int px = (int)(ff * Virtual_Width / (clipping_out - clipping_in));

                                    Rectangle destThumb = new Rectangle(px + (0 - this.X), 5, thumb_width, thumb_height);
                                    FGrf.DrawImage(thumb.Bitmap, destThumb);
                                }

                                if (px > width + 10)
                                {
                                    break;
                                }
                            }

                            #endregion ALL POSSIBLE
                        }
                        break;
                }


                // DISEGNO IL TESTO DESCRITTIVO

                if (showTitle)
                {
                    FGrf.DrawString(title, FFontTitle, Brushes.Black, new Point(lastX, 5));
                }
            }

            //FGrf.DrawString(FDuration.ToString(), FFont, Brushes.Black, new Point(2, 2));
        }

        private void Render_AudioType()
        {
            if (width > 0 && height > 0)
            {
                // DISEGNO LA BASE

                if (!isResizing)
                {
                    GraphicsPath gp = DrawRoundRect(FGrf, FBorderPen, 0, 0, width - 1, height - 2, 2);


                    if (isEditing)
                    {
                        FGrf.FillPath(new SolidBrush(Color.BlueViolet), gp);
                    }
                    else if (selected)
                    {
                        FGrf.FillPath(new SolidBrush(GetLightenColor(backColor, 40)), gp);
                    }
                    else
                    {
                        FGrf.FillPath(new SolidBrush(backColor), gp);
                    }

                    if (BackgraoundImage != null)
                    {
                        if (timeline_control.FireDisplayClipEventOnPan ||
                            (!timeline_control.FireDisplayClipEventOnPan && (timeline_control.Actions & MxpTimelineAction.isPanning) != MxpTimelineAction.isPanning))
                        {
                            FGrf.Clip = new Region(gp);

                            switch (BackgroungImageLayout)
                            {
                                case MxpClipBgImageLayout.onVisibleArea:
                                    int sx = 0;
                                    if (this.X < 0) sx += Math.Abs(this.X);
                                    FGrf.DrawImage(BackgraoundImage, sx, 0);

                                    break;
                                case MxpClipBgImageLayout.fromPerformaceX:
                                    FGrf.DrawImage(BackgraoundImage, 0, 0);
                                    break;
                                case MxpClipBgImageLayout.fromVirtualX:
                                    // Todo : da implementare calcolo
                                    break;
                            }

                            FGrf.Clip = new Region(new Rectangle(0, 0, width, height));
                        }
                    }

                    if (selected) FGrf.DrawPath(new Pen(Color.LightCoral, 1), gp);
                }
                else
                {
                    // DISEGNO UN RETTANGOLO DI INGOMBRO SEMPLICE DURANTE IL RISCALO
                    FGrf.FillRectangle(new SolidBrush(backColor), new Rectangle(0, 0, width - 1, height - 2));

                    if (BackgraoundImage != null)
                    {
                        int bgpic_x = 0;

                        if (this.x < 0) bgpic_x = Math.Abs(this.x);

                        FGrf.DrawImage(BackgraoundImage, bgpic_x, 0);
                    }
                    return;                
                }

                //FGrf.DrawString("AUDIO", this.FFont, Brushes.Black, 10, 10);

                if (Duration > 0)
                {
                    Point lastpoint = new Point(-3000, -3000);
                    
                    // DISEGNO TUTTI I NODI AUDIO

                    if (canEditAudio)
                    {
                        List<CAudioNodeInfo> audio_nodes = audioInfo.GetAllNodes();

                        float half = height / 2;


                        #region MAIN GAIN VALUE LINE

                        Pen half_pen = new Pen(Brushes.BlueViolet, 1);
                        half_pen.DashStyle = DashStyle.Dash;

                        int m0g = ParentTrack.Height - (int)(ParentTrack.Height * AudioInfo.Main0gain);// (int)half;

                        FGrf.DrawLine(half_pen, 0, m0g, width, m0g);

                        #endregion MAIN GAIN VALUE LINE


                        for (int i = 0; i < audio_nodes.Count; i++)
                        {
                            CAudioNodeInfo audionode = audio_nodes[i];

                            int anX = (int)(width * (audionode.timecode - clipping_in) / Duration);
                            //int anX = (int)(width * (audionode.timecode) / Duration);

                            int anY = ParentTrack.Height - (int)(ParentTrack.Height * audionode.value);// (int)half;

                            if (i > 0)
                            {
                                FGrf.DrawLine(new Pen(Brushes.Blue, 1), anX, anY, lastpoint.X, lastpoint.Y);
                            }

                            if (i == 0 && anX > 0)                                  // LINEA APERTURA CONFORME A PRIMO NODO
                            {
                                FGrf.DrawLine(new Pen(Brushes.Blue, 1), 0, anY, anX, anY);
                            }

                            if (i == audio_nodes.Count - 1 && anX < width)          // LINEA CHIUSURA CONFORME A ULTIMO NODO
                            {
                                FGrf.DrawLine(new Pen(Brushes.Blue, 1), anX, anY, width, anY);
                            }

                            Rectangle nodearea = new Rectangle(anX - 3, anY - 3, 6, 6);
                            audionode.LastDrawedArea = nodearea;

                            if (this.selected) FGrf.FillEllipse(Brushes.Red, nodearea);

                            if (anX + X > Parent.Width + 40)
                            {
                                break;
                            }

                            lastpoint.X = anX;
                            lastpoint.Y = anY;
                        }
                    }
                }
            }
        }
        
        private void UpdateDisplayedInfo()
        {
            // AGGIORNO LE INFORMAZIONI RELATIVE ALLA RAPPRESENTAZIONE GRAFICA DELLA CLIP
            //
            // FirstVisible_X         : IL PRIMO PIXEL DISEGNATO A SX, NELLO SPAZIO DELLA TIMELINE (DA 0 A WIDTH DELLA TIMELINE)
            // LastVisible_X          : ULTIMO PIXEL DISEGNATO A DX, NELLO SPAZIO DELLA TIMELINE (DA 0 A WIDTH DELLA TIMELINE)
            // FirstVisible_TC        : CONVERSIONE IN TIMECODE DI FirstVisible_X
            // LastVisible_TC         : CONVERSIONE IN TIMECODE DI LastVisible_X
            // VisibleWidth           : MISURA IN PIXEL DELLA LARGHEZZA REALMENTE VISIBILE DELLA CLIP
            // Visible_Duration       : CONVERSIONE IN TIMECODE DI VisibleWidth (PORZIONE DI TEMPO VISIBILE A MONITOR DELLA CLIP)
            // FirstVisible_TC_OfClip : PRIMO TIMECODE RELATIVO VISIBILE DELLA CLIP
            // LastVisible_TC_OfClip  : ULTIMO TIMECODE RELATIVO VISIBILE DELLA CLIP

            DisplayInfo.Changed = false;


            if (!isVisible)
            {
                DisplayInfo.FirstVisible_X      = 0;
                DisplayInfo.LastVisible_X       = 0;
                DisplayInfo.FirstVisible_TC     = 0;
                DisplayInfo.LastVisible_TC      = 0;

                DisplayInfo.VisibleWidth        = 0;
                DisplayInfo.Visible_Duration    = 0;


                if (DisplayInfo.Changed)
                {
                    if (OnDisplayInfoChange != null) OnDisplayInfoChange(this);
                }

                return;
            }

            DisplayInfo.FirstVisible_X = (this.x < 0) ? 0 : x;

            int parent_width = (ParentTrack != null)? ParentTrack.Width : 0;

            DisplayInfo.LastVisible_X = (this.X + this.width > parent_width) ? parent_width : this.X + this.width;


            if (timeline_control != null)
            {
                DisplayInfo.FirstVisible_TC = timeline_control.GetTimecodeAt(DisplayInfo.FirstVisible_X);
                DisplayInfo.LastVisible_TC  = timeline_control.GetTimecodeAt(DisplayInfo.LastVisible_X);

                if (Virtual_X >= 0)
                {
                    // SE L'INIZIO DELLA CLIP E' OLTRE O SULLO ZERO DELLA TIMELINE
                    // IL PRIMO TIMECODE RAPPRESENTATO DELLA CLIP E' IL SUO 0

                    DisplayInfo.FirstVisible_TC_OfClip = clipping_in;
                }
                else
                {
                    // ALTRIMENTI CALCOLO IN BASE ALLA POSIZIONE DEL SUO ZERO REALE
                    // QUALE E' IL PRIMO VISIBILE

                    long out_margin = timeline_control.ZoomIn - (tcin + clipping_in);
                    DisplayInfo.FirstVisible_TC_OfClip = clipping_in + out_margin;

                    if (DisplayInfo.FirstVisible_TC_OfClip < clipping_in) DisplayInfo.FirstVisible_TC_OfClip = clipping_in;
                    if (DisplayInfo.FirstVisible_TC_OfClip > clipping_out) DisplayInfo.FirstVisible_TC_OfClip = clipping_out - 1;
                }

                if (this.ClientBounds.Right <= timeline_control.Width)
                {
                    // SE LA FINE DELLA CLIP E' INFERIORE ALLA FINE DELLA TIMELINE
                    // L'ULTIMO TIMECODE RAPPRESENTATO DELLA CLIP E' IL SUO TCOUT

                    DisplayInfo.LastVisible_TC_OfClip = clipping_out;
                }
                else
                {
                    // ALTRIMENTI CALCOLO IN BASE ALLA PROSIZIONE REALE DEL SUO OUT
                    // QUALE E' L'ULTIMO TIMECODE VISIBILE

                    long out_margin = (tcin + clipping_out) - timeline_control.ZoomOut;
                    DisplayInfo.LastVisible_TC_OfClip = clipping_out - out_margin;
                    if (DisplayInfo.LastVisible_TC_OfClip > clipping_out) DisplayInfo.LastVisible_TC_OfClip = clipping_out;
                }
            }

            DisplayInfo.VisibleWidth        = DisplayInfo.LastVisible_X - DisplayInfo.FirstVisible_X;
            DisplayInfo.Visible_Duration    = DisplayInfo.LastVisible_TC - DisplayInfo.FirstVisible_TC;



            if (DisplayInfo.Changed)
            {
                if (OnDisplayInfoChange != null) OnDisplayInfoChange(this);
            }
        }


        #region THUMB COLLECTION EVENTS

        private void FMyThumb_OnItemsCleared(object sender, EventArgs e)
        {
            Render();
        }
        
        #endregion THUMB COLLECTION EVENTS



        #region PUBLIC METHODS

        public void SetSize(int newWidth, int newHeight)
        {
            if (newWidth != width || newHeight != height)
            {
                width       = newWidth;
                height      = newHeight;
                Resize(newWidth, newHeight);
                Render();
            }
        }

        public MxpClip_HitTest_Info GetHitTest(Point p)
        {
            LastHitTest = MxpClip_HitTest_Info.none;
            LastHitTestObject = null;

            if (type == enCMxpClipType.Audio)
            {
                foreach (CAudioNodeInfo nodes in audioInfo.GetAllNodes())
                {
                    if (nodes.LastDrawedArea.Contains(p))
                    {
                        LastHitTest = MxpClip_HitTest_Info.audioNode;
                        LastHitTestObject = nodes;
                        break;
                    }
                }
            }
            else
            { 
            
            }

            if (p.X > width - 5 && p.X <= width)
            {
                if (LastHitTest == MxpClip_HitTest_Info.none)
                {
                    LastHitTest = MxpClip_HitTest_Info.rightHandle;
                }
                else
                {
                    LastHitTest |= MxpClip_HitTest_Info.rightHandle;
                }
            }

            if (p.X >= 0 && p.X <= 5)
            {
                if (LastHitTest == MxpClip_HitTest_Info.none)
                {
                    LastHitTest = MxpClip_HitTest_Info.leftHandle;
                }
                else
                {
                    LastHitTest |= MxpClip_HitTest_Info.leftHandle;
                }
            }

            return LastHitTest;
        }

        public void Delete()
        {
            if (OnDeleteClip != null)
            {
                OnDeleteClip(this);
            }
        }

        public void Edit()
        {
            isEditing = !isEditing;
            if (OnEdit != null) OnEdit(this, isEditing);
            Render();
        }

        public void NotifyRender()
        {
            Render();
            if (OnRender != null) OnRender(this);
        }

        public void NotifyDisplayedInfoChanged()
        {
            if (OnDisplayInfoChange != null) OnDisplayInfoChange(this);
        }

        public void RefreshDisplayedInfo()
        {
            UpdateDisplayedInfo();
        }

        public void NotifyRender_NoEVents()
        {
            Render(false);
        }

        /// <summary>
        /// Restituisce il la posizione e il valore dell'audio in una determinata X
        /// </summary>
        /// <param name="x">Valore in pixel della posizione X</param>
        /// <param name="y">Valore in pixel della posizione Y</param>
        /// <param name="tc">[OUT] valore restituito del Timecode rispetto lo zero della timeline</param>
        /// <param name="value">[OUT] valore restituito del volume da 1 a 0 in float</param>
        public void GetAudioValueAt(int x, int y, out long tc, out float value)
        {
            tc        = 0;
            value     = 1F;

            if (isFreeClip)
            {
                tc = x * Duration / this.ClientBounds.Width + TimcodeIn;
            }
            else
            {
                tc = (x * Duration / this.ClientBounds.Width) + TimcodeIn +clipping_in;
            }

            value = 1 - ((float)y / (float)ParentTrack.Height);// AudioInfo.ConformValue(value);

            if (value < 0) value = 0;
            if (value > 1) value = 1;

            Console.WriteLine("y : " + y + " - db : " + value + " - 0 : " + AudioInfo.Main0gain + " - t : " + tc);
        }

        public void Move(long tc, bool alone)
        {
            base.Notify_BeginEditing();

            this.tcin  += tc;
            this.tcout += tc;
            
            if (!alone && Group.Count > 0)
            {
                foreach (CMxpClip clip in this.Group)
                {
                    clip.Move(tc, true);
                    clip.IsMoving = true;
                }
                
            }
            if (!alone)
            {
                Render();
            }

            if (!isFreeClip)
            {
                if (OnChangedTC != null) OnChangedTC(this, tcin + clipping_in, tcin + clipping_out, enTcChangeType.both);
            }
            else
            {
                if (OnChangedTC != null) OnChangedTC(this, tcin, tcout, enTcChangeType.both); 
            }

            base.Notify_EndEditing();
        }

        public void SetThumb(long tc, Bitmap bitmap, MxpClip_ThumbPosition position)
        {
            CMxpClipThumb th = null;

            lock (FMyThumb)
            {
                if (thumbLayout == MxpClipThumbLayout.firstAndLast)
                {
                    if (position == MxpClip_ThumbPosition.Absolute)
                    {
                        #region TRANSLATE POSITION

                        if (Math.Abs(tc - this.clipping_in) < Math.Abs(tc - clipping_out))
                        {
                            position = MxpClip_ThumbPosition.Begining;
                        }
                        else
                        {
                            position = MxpClip_ThumbPosition.Ending;
                        }

                        #endregion TRANSLATE POSITION
                    }

                    switch (position)
                    {
                        case MxpClip_ThumbPosition.Begining:
                            if (firstThumb == null)
                            {
                                firstThumb = new CMxpClipThumb(tc, bitmap);
                            }
                            else
                            {
                                lock (firstThumb)
                                {
                                    firstThumb.Bitmap.Dispose();
                                    firstThumb = null;
                                    firstThumb = new CMxpClipThumb(tc, bitmap);
                                }
                            }
                            break;
                        case MxpClip_ThumbPosition.Ending:
                            if (lastThumb == null)
                            {
                                lastThumb = new CMxpClipThumb(tc, bitmap);
                            }
                            else
                            {
                                lock (lastThumb)
                                {
                                    lastThumb.Bitmap.Dispose();
                                    lastThumb = null;
                                    lastThumb = new CMxpClipThumb(tc, bitmap);
                                }
                            }
                            break;
                    }
                }
                else
                {
                    List<CMxpClipThumb> present = FMyThumb.Where(x => x.Timecode == tc).ToList();

                    //foreach (CMxpClipThumb pres in FMyThumb)
                    //{
                    //    if (pres.Timecode == tc)
                    //    {
                    //        th = pres;
                    //    }
                    //    break;
                    //}

                    if (present.Count > 0)
                    {
                        //present[0].Bitmap = bitmap;
                    }
                    else
                    {
                        FMyThumb.Add(new CMxpClipThumb(tc, bitmap));
                    }
                }
            }
            

            //switch (thumbLayout)
            //{
            //    case MxpClipThumbLayout.firstOnly:
            //        break;
            //    case MxpClipThumbLayout.firstAndLast:
            //        if (FMyThumb.Count > 2)
            //        {
            //            ThumbsKeepOnlyLimits();
            //        }
            //        break;
            //    case MxpClipThumbLayout.allPossible:
            //        break;
            //    default:
            //        break;
            //}

            //Render();
        }

        public void SetThumb(long tc, string thumb_file, MxpClip_ThumbPosition position)
        {
            if (File.Exists(thumb_file))
            {
                FileStream fs = new FileStream(thumb_file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                SetThumb(tc, (Bitmap)Bitmap.FromStream(fs), position);
                fs.Close();
                fs.Dispose();
            }
        }

        public float GetAudioAt(long position, bool isTimelinePosition)
        {
            if (isTimelinePosition)
            {
                if (!this.isFreeClip)
                {
                    position -= this.tcin + clipping_in;
                }
                else
                {
                    position -= this.tcin;
                }
            }

            if (this.type == enCMxpClipType.Audio)
            {
                if (isFreeClip)
                {
                    return AudioInfo.GetValueAt(position);           
                }
                else
                { 
                    return AudioInfo.GetValueAt(position + this.clipping_in);                
                }
            }

            return 0F;
        }

        public void DrawToBitmap(Bitmap bmp, Rectangle rect)
        {
            if (rect.Width > 0 && rect.Height > 0)
            {
                width = rect.Width;
                height = rect.Height;

                Render();
            }
            System.Drawing.Graphics grf = System.Drawing.Graphics.FromImage(bmp);
            grf.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            grf.DrawImage(FRendered, rect.Location);
            grf.Dispose();
        }

        public CMxpClip[] Cut(long timecode)
        {
            base.Notify_BeginEditing();

            CMxpClip[] divided = new CMxpClip[2];

            divided[0] = this;

            CMxpClip newmainclip = null;

            if (isFreeClip)
            {
                if (timecode > tcin && timecode < tcout)
                {
                    newmainclip = DoCut(timecode);
                }
            }
            else
            {
                if (timecode > tcin + clipping_in && timecode < tcin + clipping_out)
                {
                    newmainclip = DoCut(timecode);
                }
            }

            divided[1] = newmainclip;

            foreach (CMxpClip subclip in this.Group)
            {
                CMxpClip newsubclip = subclip.DoCut(timecode);

                if (newsubclip != null) newmainclip.Group.Add(newsubclip);
            }

            base.Notify_EndEditing();

            return divided;
        }

        public void Set_Parent(CMxpTL_Object new_parent, bool withUndo)
        {
            if(withUndo) base.Notify_BeginEditing();

            this.Parent = new_parent;

            if (withUndo) base.Notify_EndEditing();
        }

        public void Set_NewStartAt(long tc)
        {
            Set_NewStartAt(tc, true);

            foreach (CMxpClip subclip in Group)
            {
                subclip.Set_NewStartAt(tc, false);
            }
        }

        private void Set_NewStartAt(long tc, bool withundo)
        {
            if (withundo) base.Notify_BeginEditing();

            long movement = tc - (tcin + clipping_in);

            tcin += movement;
            tcout += movement;

            if (OnChangedTC != null) OnChangedTC(this, tcin, tcout, enTcChangeType.both);

            if (withundo) base.Notify_EndEditing();
        }

        public void Set_TimecodeIn(long tc, bool ContinueOnChild)
        {
            base.Notify_BeginEditing();

            if (tc != tcin)
            {
                tcin = tc;

                if (ContinueOnChild)
                {
                    foreach (CMxpClip subclip in this.Group)
                    {
                        subclip.Set_TimecodeIn(tc, false);
                    }
                }

                Render();
                if (OnChangedTC != null) OnChangedTC(this, tcin, tcout, enTcChangeType.timecode_in);
            }

            base.Notify_EndEditing();
        }

        public void Set_TimecodeOut(long tc, bool ContinueOnChild)
        {
            base.Notify_BeginEditing();

            if (tc != tcout)
            {
                tcout = tc;

                if (ContinueOnChild)
                {
                    foreach (CMxpClip subclip in this.Group)
                    {
                        subclip.Set_TimecodeOut(tc, false);
                    }
                }

                Render();
                if (OnChangedTC != null) OnChangedTC(this, tcin, tcout, enTcChangeType.timecode_out);
            }

            base.Notify_EndEditing();
        }

        public void Set_ClippingIn(long tc, bool ContinueOnChild)
        {
            base.Notify_BeginEditing();

            if (clipping_in != tc && tc >= 0)
            {
                clipping_in = tc;
                ClipInfo.Clipping_in = tc;        //tc - tcin;

                if (ContinueOnChild)
                {
                    foreach (CMxpClip subclip in this.Group)
                    {
                        subclip.Set_ClippingIn(tc, false);                    
                    }

                }

                if (!isFreeClip)
                {
                    if (OnChangedTC != null) OnChangedTC(this, tcin + clipping_in, tcin + clipping_out, enTcChangeType.timecode_in);
                }
                else
                {
                    if (OnChangedTC != null) OnChangedTC(this, tcin, tcin, enTcChangeType.timecode_in);
                }

                Render();
            }

            base.Notify_EndEditing();
        }

        public void Set_ClippingOut(long tc, bool ContinueOnChild)
        {
            base.Notify_BeginEditing();

            if (clipping_out != tc && tc >= 0)
            {
                clipping_out = tc;
                ClipInfo.Clipping_out = tc;     // tc - tcin;


                if (ContinueOnChild)
                {
                    foreach (CMxpClip subclip in this.Group)
                    {
                        subclip.Set_ClippingOut(tc, false);
                    }
                }

                if (!isFreeClip)
                {
                    if (OnChangedTC != null) OnChangedTC(this, tcin + clipping_in, tcin + clipping_out, enTcChangeType.timecode_out);
                }
                else
                {
                    if (OnChangedTC != null) OnChangedTC(this, tcin, tcin, enTcChangeType.timecode_out);
                }

                Render();
            }

            base.Notify_EndEditing();
        }

        public void Set_BackgraoundImage(Bitmap bmp, bool RaiseOnRenderEvent)
        {
            this.backgraoundImage = bmp;
            Render(RaiseOnRenderEvent);
        }

        public CMxpClip[] GetGroup()
        {
            List<CMxpClip> grp = new List<CMxpClip>();

            grp.Add(this);
            grp.AddRange(this.Group.ToArray());

            return grp.ToArray();
        }

        public CMxpClip Duplicate()
        {
            CMxpClip nclip = new CMxpClip();

            PropertyInfo[] pInfos = this.GetType().GetProperties();

            foreach (PropertyInfo pInfo in pInfos)
            {
                if (pInfo.CanWrite)
                {
                    pInfo.SetValue(nclip, pInfo.GetValue(this, null), null);
                }

            }


            FieldInfo[] fInfos = this.GetType().GetFields();

            foreach (FieldInfo fInfo in fInfos)
            {
                fInfo.SetValue(nclip, fInfo.GetValue(this));

            }

            return nclip;
        }

        public CMxpClip GetNextClip(out long tc_diff)
        {
            CMxpClip next = null;
            tc_diff = 100000;

            foreach (CMxpClip clip in ParentTrack.Clips)
            {
                long this_diff = (clip.ClippingIn + clip.tcin) - (tcin + clipping_out);

                if (this_diff >= 0 && this_diff < tc_diff)
                {
                    tc_diff = this_diff;
                    next = clip;
                }
            }

            return next;
        }

        public void AutoFadeAudio()
        {
            if (this.type != enCMxpClipType.Audio) return;

            List<CMxpClip> clips_ref = new List<CMxpClip>();
            List<CMxpTrack> tracks_ref = timeline_control.Tracks.Tracks.Where(trk => trk.Type == enMxpTrackType.Video).ToList();

            this.AudioInfo.Clear();

            foreach (CMxpTrack trk in tracks_ref)
            { 
                foreach (CMxpClip clip in trk.Clips)
                {
                    if (this.ContainTC(clip.tcin + clip.clipping_out) && !this.ContainTC(clip.tcin + clip.clipping_in))
                    {
                        // fade in

                        this.AudioInfo.AddNode(ClippingIn, 0);
                        this.AudioInfo.AddNode(clip.tcin + clip.clipping_out - tcin, 1);
                    }
                    if (this.ContainTC(clip.tcin + clip.clipping_in) && !this.ContainTC(clip.tcin + clip.clipping_out))
                    {
                        // fade out

                        this.AudioInfo.AddNode(ClippingOut, 0);
                        this.AudioInfo.AddNode(clip.tcin + clip.clipping_in - tcin, 1);
                    }
                }
            }
        }

        public bool ContainTC(long tc)
        {
            if (tc >= tcin + ClippingIn && tc <= tcin + clipping_out)
            {
                return true;
            }

            return false;
        }

        #endregion PUBLIC METHODS




        private CMxpClip DoCut(long timecode)
        {
            CMxpClip newclip = null;

            if (isFreeClip)
            {
                long oldend = tcout;

                if (timecode > tcin && timecode < tcout)
                {
                    this.tcout = timecode;

                    newclip = ParentTrack.AddClip(title, type, null, tag, tcin, tcout, clipping_out, oldend);
                    if (type == enCMxpClipType.Audio) newclip.audioInfo = audioInfo.Clone(newclip);

                    Render();

                    if (OnChangedTC != null) OnChangedTC(this, tcin, tcout, enTcChangeType.timecode_out);
                }
            }
            else
            {
                long oldend = clipping_out;

                if (timecode > tcin + clipping_in && timecode < tcin + clipping_out)
                {
                    this.clipping_out = timecode - tcin;

                    ClipInfo.Clipping_in  = clipping_in;
                    ClipInfo.Clipping_out = clipping_out;

                    CMxpTL_ClipInfo nCI = ClipInfo.Copy();

                    nCI.Clipping_in     = clipping_out;
                    nCI.Clipping_out    = oldend;


                    newclip = ParentTrack.AddClip(title, type, nCI, tag, tcin, tcout, clipping_out, oldend);
                    if(type == enCMxpClipType.Audio) newclip.audioInfo = audioInfo.Clone(newclip);

                    newclip.ClipInfo = ClipInfo.Copy();

                    newclip.ClipInfo.Clipping_in    = newclip.clipping_in;
                    newclip.ClipInfo.Clipping_out   = newclip.clipping_out;

                    newclip.NotifyRender();
                    Render();

                    //if (OnExitedFromActions != null) OnExitedFromActions(this, MxpTimelineAction.isEditingClipOut, clip);
                    if (OnChangedTC != null) OnChangedTC(this, tcin + clipping_in, tcin + clipping_out, enTcChangeType.timecode_out);
                }
            }

            return newclip;
        }

        public Bitmap Image
        {
            get 
            {
                if (FRendered != null && FGrf != null)
                {
                    return FRendered;
                }
                return null;
            }
        }


        protected override void InternalChangeNotification(enInternalChangeNotificationType cType)
        {
            switch (cType)
            {
                case enInternalChangeNotificationType.none:
                    break;
                case enInternalChangeNotificationType.x:
                    break;
                case enInternalChangeNotificationType.y:
                    break;
                case enInternalChangeNotificationType.position:
                    break;
                case enInternalChangeNotificationType.width:
                    Resize(width, height);
                    Render();
                    break;
                case enInternalChangeNotificationType.height:
                    Resize(width, height);
                    Render();
                    break;
                case enInternalChangeNotificationType.bound:
                    if (OnBoundsChanged != null) OnBoundsChanged(this, clipBounds, clipBounds);
                    break;
                case enInternalChangeNotificationType.isResizing:
                    if (!isResizing) Render();
                    break;
                default:
                    break;
            }
        }
        
        public override string Serialize()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(Properties.Resources.CMxpClip_Descriptor);

            doc["CMxpClip"]["ID"].InnerText             = this.id.ToString();
            doc["CMxpClip"]["TimecodeIn"].InnerText     = this.tcin.ToString();
            doc["CMxpClip"]["TimecodeOut"].InnerText    = this.TimecodeOut.ToString();
            doc["CMxpClip"]["ClippingIn"].InnerText     = this.clipping_in.ToString();
            doc["CMxpClip"]["ClippingOut"].InnerText    = this.clipping_out.ToString();
            doc["CMxpClip"]["TrackId"].InnerText        = this.ParentTrack.ID.ToString();
            doc["CMxpClip"]["Type"].InnerText           = this.type.ToString();
            doc["CMxpClip"]["SourceId"].InnerText       = ClipInfo.file_id.ToString();

            doc["CMxpClip"]["ClipInfo"]["ClippingIn"].InnerText     = this.ClipInfo.Clipping_in.ToString();
            doc["CMxpClip"]["ClipInfo"]["ClippingOut"].InnerText    = this.ClipInfo.Clipping_out.ToString();
            doc["CMxpClip"]["ClipInfo"]["description"].InnerText    = this.ClipInfo.description.ToString();
            doc["CMxpClip"]["ClipInfo"]["file_id"].InnerText        = this.ClipInfo.file_id.ToString();
            doc["CMxpClip"]["ClipInfo"]["filename"].InnerText       = this.ClipInfo.filename.ToString();
            doc["CMxpClip"]["ClipInfo"]["Tcin"].InnerText           = this.ClipInfo.Tcin.ToString();
            doc["CMxpClip"]["ClipInfo"]["Tcout"].InnerText          = this.ClipInfo.Tcout.ToString();

            return doc.InnerXml;
        }

        public override void Deserialize(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            this.tcin           = (doc["CMxpClip"]["TimecodeIn"].InnerText != null) ? long.Parse(doc["CMxpClip"]["TimecodeIn"].InnerText) : 0;
            this.tcout          = (doc["CMxpClip"]["TimecodeOut"].InnerText != null) ? long.Parse(doc["CMxpClip"]["TimecodeOut"].InnerText) : 0;
            this.clipping_in    = (doc["CMxpClip"]["ClippingIn"].InnerText != null) ? long.Parse(doc["CMxpClip"]["ClippingIn"].InnerText) : 0;
            this.clipping_out   = (doc["CMxpClip"]["ClippingOut"].InnerText != null) ? long.Parse(doc["CMxpClip"]["ClippingOut"].InnerText) : 0;
            this.type           = (doc["CMxpClip"]["Type"].InnerText != null) ? (enCMxpClipType)Enum.Parse(typeof(enCMxpClipType), doc["CMxpClip"]["Type"].InnerText) : enCMxpClipType.Video;

            this.ClipInfo.Clipping_in   = long.Parse(doc["CMxpClip"]["ClipInfo"]["ClippingIn"].InnerText);
            this.ClipInfo.Clipping_out  = long.Parse(doc["CMxpClip"]["ClipInfo"]["ClippingOut"].InnerText);
            this.ClipInfo.Tcin          = long.Parse(doc["CMxpClip"]["ClipInfo"]["Tcin"].InnerText);
            this.ClipInfo.Tcout         = long.Parse(doc["CMxpClip"]["ClipInfo"]["Tcout"].InnerText);
            this.ClipInfo.file_id       = int.Parse(doc["CMxpClip"]["ClipInfo"]["file_id"].InnerText);
            this.ClipInfo.description   = doc["CMxpClip"]["ClipInfo"]["description"].InnerText;
            this.ClipInfo.filename      = doc["CMxpClip"]["ClipInfo"]["filename"].InnerText;

            Render();

            if (OnChangedTC != null) OnChangedTC(this, tcin + clipping_in, tcin + clipping_out, enTcChangeType.both);
        }



        #region PUBLIC PROPERTIES

        public long Duration
        {
            get 
            {
                if (isFreeClip) return tcout - tcin;
                return clipping_out - clipping_in; 
            }
        }

        public long TotalDuration
        {
            get { return tcout - tcin; }
        }

        public long TimcodeIn
        {
            get { return tcin; }
            set
            {
                if (value != tcin)
                {
                    tcin        = value;

                    if (this.Group != null)
                    {
                        foreach (CMxpClip subclip in this.Group)
                        {
                            subclip.Set_TimecodeIn(value, false);
                        }
                    }

                    Render();
                    if (OnChangedTC != null) OnChangedTC(this, tcin, tcout, enTcChangeType.timecode_in);
                }
            }
        }

        public long TimecodeOut
        {
            get { return tcout; }
            set
            {
                if (value != tcout)
                {
                    tcout       = value;

                    foreach (CMxpClip subclip in this.Group)
                    {
                        subclip.Set_TimecodeOut(value, false);
                    }

                    Render();
                    if (OnChangedTC != null) OnChangedTC(this, tcin, tcout, enTcChangeType.timecode_out);
                }
            }
        }

        public long ClippingIn
        {
            get 
            {
                if (this.isFreeClip) return tcin;
                return clipping_in; 
            }
            set
            {
                if (clipping_in != value && value >= 0)
                {
                    clipping_in = value;
                    ClipInfo.Clipping_in = value;       //;value - tcin;
                    
                    foreach (CMxpClip subclip in this.Group)
                    {
                        subclip.Set_ClippingIn(value, false);
                    }

                    if (!isFreeClip)
                    {
                        if (OnChangedTC != null) OnChangedTC(this, tcin + clipping_in, tcin + clipping_out, enTcChangeType.timecode_in);
                    }
                    else
                    {
                        if (OnChangedTC != null) OnChangedTC(this, tcin, tcin, enTcChangeType.timecode_in);
                    }

                    Render();
                }
            }
        }

        public long ClippingOut
        {
            get 
            {
                if (isFreeClip) return tcout;
                return clipping_out; 
            }
            set
            {
                if (clipping_out != value && value <= tcout - tcin && value > clipping_in)
                {
                    clipping_out = value;
                    ClipInfo.Clipping_out = value;


                    foreach (CMxpClip subclip in this.Group)
                    {
                        subclip.Set_ClippingOut(value, false);
                    }

                    if (!isFreeClip)
                    {
                        if (OnChangedTC != null) OnChangedTC(this, tcin + clipping_in, tcin + clipping_out, enTcChangeType.timecode_out);
                    }
                    else
                    {
                        if (OnChangedTC != null) OnChangedTC(this, tcin, tcin, enTcChangeType.timecode_out);
                    }

                    Render();
                }
            }
        }

        public Guid ID
        {
            get { return id; }
            //set
            //{
            //    id = value;
            //}
        }
        
        public bool IsVisible
        {
            get { return isVisible; }
            set 
            {
                if (isVisible != value)
                {
                    isVisible = value;

                    if (isVisible)
                    {
                        if (OnDisplayInfoChange != null) OnDisplayInfoChange(this);
                        Render();
                    }
                }
            }
        }

        public bool IsClipped
        {
            get 
            {
                return (clipping_in > 0 || clipping_out < Duration);
            }
        }

        public bool IsMoving
        {
            get { return isMoving; }
            set
            {
                if (isMoving != value)
                {
                    isMoving = value;
                    if (OnVirtualObjectNeed != null) OnVirtualObjectNeed(null, new Rectangle(0, 0, 0, 0));
                    Render();
                }
            }
        }

        public bool IsSelected
        {
            get { return selected; }
            set 
            {
                if (selected != value)
                {
                    selected = value;
                    Render();
                    if (OnChangeSelected != null) OnChangeSelected(this, value);
                }
            }
        }

        public bool IsFreeClip
        {
            get { return isFreeClip; }
            set
            {
                if (isFreeClip != value)
                {
                    isFreeClip = value;
                    Render();
                }
            }
        }

        public bool IsDeleted
        {
            get { return isDeleted; }
            set
            {
                isDeleted = value;
                Render();
            }
        }

        public bool CanEditAudio
        {
            get { return canEditAudio; }
            set
            {
                if (canEditAudio != value)
                {
                    canEditAudio = value;
                    Render();
                }
            }
        }

        private void Resize(int width, int height)
        {
            if (width > 0 && height > 0)
            {
                if (FRendered != null)
                {
                    FRendered.Dispose();
                    FRendered = null;
                }
                if (FGrf != null)
                {
                    FGrf.Dispose();
                    FGrf = null;
                }

                FRendered = new Bitmap(width, height);
                if (FGrf != null) FGrf.Dispose();
                FGrf = System.Drawing.Graphics.FromImage(FRendered);
                FGrf.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                FExtendGraph = new ExtendedGraphics(FGrf);

                this.ClientBounds = new Rectangle(x, y, width, height);
            }
        }
        
        //public Color VideoClipBackcolor
        //{
        //    get { return videoBackColor; }
        //    set 
        //    {
        //        if (videoBackColor != value)
        //        {
        //            videoBackColor = value;
        //            Render();
        //        }
        //    }
        //}

        //public Color AudioClipBackcolor
        //{
        //    get { return audioBackColor; }
        //    set
        //    {
        //        if (audioBackColor != value)
        //        {
        //            audioBackColor = value;
        //            Render();
        //        }
        //    }
        //}

        public Color BackColor
        {
            get { return backColor; }
            set
            {
                if (backColor != value)
                {
                    backColor = value;
                    Render();
                }
            }
        }

        public Color SelectedClipColor
        {
            get { return FSelectedClipColor; }
            set
            {
                if (FSelectedClipColor != value)
                {
                    FSelectedClipColor = value;
                    Render();
                }
            }
        }
        
        [XmlIgnore()]
        public CMxpTrack ParentTrack
        {
            get { return (CMxpTrack)Parent; }
        }
        
        public string Title
        {
            get { return title; }
            set 
            {
                if (title != value)
                {
                    title = value;
                    
                    if (showTitle)
                    {
                        Render();
                        if (OnChangeTitle != null) OnChangeTitle(this);
                    }
                }
            }
        }

        public bool ShowTitle
        {
            get { return showTitle; }
            set 
            {
                if (showTitle != value)
                {
                    showTitle = value;
                    Render();
                    if (OnChangeTitle != null) OnChangeTitle(this);
                }
            }
        }

        public object Tag
        {
            get { return tag; }
            set { tag = value; }
        }

        public enCMxpClipType Type
        {
            get { return type; }
            set
            {
                if (value != type)
                {
                    if (value == enCMxpClipType.Audio)
                    {
                        audioInfo = new CMxpClip_ExtraAudioInfo(this);
                        backColor = audioBackColor;
                    }
                    else
                    {
                        audioInfo = null;
                        backColor = videoBackColor;
                    }

                    type = value;
                    Render();
                }
            }
        }

        public CMxpClip_ExtraAudioInfo AudioInfo
        {
            get
            {
                return audioInfo;
            }
        }

        [XmlIgnore()]
        public Bitmap BackgraoundImage
        {
            get { return backgraoundImage; }
            set
            {
                backgraoundImage = value;
                Render();
            }
        }

        [XmlIgnore()]
        public CMxpClipThumb_Colletion FrameThumbs
        {
            get { return FMyThumb; }
        }

        public MxpClipThumbLayout ThumbLayout
        {
            get { return thumbLayout; }
            set
            {
                if (thumbLayout != value)
                {
                    thumbLayout = value;
                    Render();
                }
            }
        }

        public bool Visible
        {
            get { return visible; }
            set
            {
                if (visible != value)
                {
                    visible = value;

                    foreach (CMxpClip subclip in Group)
                    {
                        subclip.visible = value;
                        subclip.ParentTrack.Render();
                    }

                    Render();
                }
            }
        }

        public long ThumbDuration
        {
            get
            {
                int h = height - 4;
                int w = h / 3 * 4;

                return this.ParentTrack.PixInFrame(w);
            }
        }

        public bool InvertedFieldOrder
        {
            get { return m_invertedFieldOrder; }
            set
            {
                if (m_invertedFieldOrder != value)
                {
                    m_invertedFieldOrder = value;
                    Render();
                }
            }
        }

        #endregion



        public static bool operator ==(CMxpClip clip1, CMxpClip clip2)
        {
            return !(clip1 != clip2);
        }

        public static bool operator !=(CMxpClip clip1, CMxpClip clip2)
        {
            object o1 = clip1;
            object o2 = clip2;
            if (o1 == null && o2 != null) return true;
            if (o1 != null && o2 == null) return true;
            if (o1 == null && o2 == null) return false;

            if (clip1.ID != clip2.ID) return true;

            return false;
        }

        private GraphicsPath DrawRoundRect(System.Drawing.Graphics g, Pen p, float X, float Y, float width, float height, float radius)
        {
            GraphicsPath gp = new GraphicsPath();

            gp.AddLine(X + radius, Y, X + width - (radius * 2), Y);
            gp.AddArc(X + width - (radius * 2), Y, radius * 2, radius * 2, 270, 90);
            gp.AddLine(X + width, Y + radius, X + width, Y + height - (radius * 2));
            gp.AddArc(X + width - (radius * 2), Y + height - (radius * 2), radius * 2, radius * 2, 0, 90);
            gp.AddLine(X + width - (radius * 2), Y + height, X + radius, Y + height);
            gp.AddArc(X, Y + height - (radius * 2), radius * 2, radius * 2, 90, 90);
            gp.AddLine(X, Y + height - (radius * 2), X, Y + radius);
            gp.AddArc(X, Y, radius * 2, radius * 2, 180, 90);
            gp.CloseFigure();

            g.DrawPath(p, gp);

            return gp;
        }
    }



    public class CAudioNodeInfo
    {
        public long timecode    = 0;
        public float value      = 0F;
        public Rectangle LastDrawedArea;

        public CAudioNodeInfo(long timecode, float val)
        {
            this.timecode = timecode;
            this.value = val;
        }

        public CAudioNodeInfo(CAudioNodeInfo source)
        {
            this.timecode       = source.timecode;
            this.value          = source.value;
            this.LastDrawedArea = source.LastDrawedArea;
        }
    }


    public class CMxpClipGroup : CAdvList<CMxpClip>
    {
        private CMxpClip owner = null;

        public CMxpClipGroup(CMxpClip Owner)
        {
            this.owner = Owner;

            base.OnBeforeItemAdded      += new EventHandler<GenericItemEventArgs<CMxpClip>>(CMxpClipGroup_OnBeforeItemAdded);
            base.OnItemAdded            += new EventHandler<GenericItemEventArgs<CMxpClip>>(Clips_OnItemAdded);
            base.OnItemRemoved          += new EventHandler<EventArgs>(Clips_OnItemRemoved);
            base.OnBeforeItemRemoved    += new EventHandler<GenericItemEventArgs<CMxpClip>>(Clips_OnBeforeItemRemoved);
        }

        void CMxpClipGroup_OnBeforeItemAdded(object sender, GenericItemEventArgs<CMxpClip> e)
        {
            if(e.Item == owner) e.Cancel = true;
            if(this.Contains(e.Item)) e.Cancel = true;
        }

        void Clips_OnItemAdded(object sender, GenericItemEventArgs<CMxpClip> e)
        {
            e.Item.Group.Add(owner);
        }

        void Clips_OnBeforeItemRemoved(object sender, GenericItemEventArgs<CMxpClip> e)
        {
            e.Item.Group.Remove(owner);
        }

        void Clips_OnItemRemoved(object sender, EventArgs e)
        {
            
        }
    }
}
