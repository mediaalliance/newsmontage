﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MediaAlliance.Controls.MxpTimeLine
{
    public class CTcTools
    {
        public static string Frame2TC(long frame, float fps)
        {
            string result = "";
            long ffTot  = frame;

            int ss      = (int)(frame / fps);
            int ff      = (int)(frame - (ss * fps));
            int mm      = (int)(ss / 60);
            ss          = ss - mm * 60;
            int hh      = (int)(mm / 60);
            mm          = mm - hh * 60;

            result += hh.ToString().PadLeft(2, '0') + ":";
            result += mm.ToString().PadLeft(2, '0') + ":";
            result += ss.ToString().PadLeft(2, '0') + ":";
            result += ff.ToString().PadLeft(2, '0');

            return result;
        }
    }
}
