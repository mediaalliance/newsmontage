using System;
using System.Collections.Generic;
using System.Text;

namespace MediaAlliance.Controls
{
    /// <summary>
    /// Classe che eredita la IList e aggiunge eventi durante l'inserimento e la rimozione degli items contenuti
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CAdvList<T> : IList<T>
        where T : class 
    {
        #region Members

        private List<T> m_pItems = null;

        #endregion

        #region Events

        public event EventHandler<GenericItemEventArgs<T>> OnItemAdded = null;
        public event EventHandler<GenericItemEventArgs<T>> OnBeforeItemAdded = null;
        public event EventHandler<EventArgs> OnItemRemoved = null;
        public event EventHandler<GenericItemEventArgs<T>> OnBeforeItemRemoved = null;
        public event EventHandler<EventArgs> OnItemsCleared = null;

        #endregion

        #region Protected Properties

        protected List<T> Items
        {
            get { return this.m_pItems; }
            private set { this.m_pItems = value; }
        }
        #endregion

        #region Constructor

        public CAdvList() : this(0)
        {
            //
        }

        public CAdvList(int capacity)
        {
            this.Items = new List<T>(capacity);
        }

        public CAdvList(IList<T> objects)
        {
            this.Items = new List<T>(objects);
        }

        #endregion

        #region IList Methods

        public T this[int index]
        {
            get 
            { 
                if(index < Count) return this.Items[index];
                return null;
            }
            set { this.Items[index] = value; }
        }

        public int IndexOf(T item)
        {
            return this.Items.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            if (OnBeforeItemAdded != null) OnBeforeItemAdded(this, new GenericItemEventArgs<T>(item));
            this.Items.Insert(index, item);
            if (OnItemAdded != null) OnItemAdded(this, new GenericItemEventArgs<T>(item));
        }

        public void RemoveAt(int index)
        {
            if (OnBeforeItemRemoved != null) OnBeforeItemRemoved(this, new GenericItemEventArgs<T>(this.Items[index]));
            this.Items.RemoveAt(index);
            if (OnItemRemoved != null) OnItemRemoved(this, new EventArgs());
        }
        #endregion



        #region ICollection Methods and Properties

        public int Count { get { return this.Items.Count; } }

        public bool IsReadOnly { get { return false; } }

        public void Add(T item)
        {
            GenericItemEventArgs<T> ev = new GenericItemEventArgs<T>(item);
            ev.Cancel = false;

            if (OnBeforeItemAdded != null) OnBeforeItemAdded(this, ev);// new GenericItemEventArgs<T>(item));

            if(!ev.Cancel)
            {
                this.Items.Add(item);
                if (OnItemAdded != null) OnItemAdded(this, new GenericItemEventArgs<T>(item));
            }
        }

        public void AddRange(IList<T> items)
        {        
            foreach(T item in items) this.Items.Add(item);
        }

        public void Clear()
        {
            this.Items.Clear();
            if (OnItemsCleared != null) OnItemsCleared(this, new EventArgs());
        }

        public bool Contains(T item)
        {
            return this.Items.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            this.Items.CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)
        {
            if (OnBeforeItemRemoved != null) OnBeforeItemRemoved(this, new GenericItemEventArgs<T>(item));
            bool happened = this.Items.Remove(item);
            if (OnItemRemoved != null) OnItemRemoved(this, new EventArgs());
            return happened;
        }

        #endregion



        #region IEnumerable<T> Methods

        public IEnumerator<T> GetEnumerator()
        {
            return this.Items.GetEnumerator();
        }

        public T[] ToArray()
        {
            return this.Items.ToArray();
        }
    
        #endregion

        #region IEnumerable Methods



        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        #endregion

    }

    public class GenericItemEventArgs<T> : EventArgs
    {
        private T MyItem;
        public bool Cancel = false;

        public T Item
        {
            get { return MyItem; }
            private set { MyItem = value; }
        }

        public GenericItemEventArgs(T item)
        {
            this.Item = item;
        }
    }

}
