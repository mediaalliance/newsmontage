using System;
using System.Collections.Generic;
using System.Text;

using System.Linq;
using System.Drawing;
using MediaAlliance.Controls.MxpTimeLine.Classes;
using System.Xml;
using System.Xml.Serialization;
using MediaAlliance.Controls.MxpTimeLine.Objects;
using System.ComponentModel;



namespace MediaAlliance.Controls.MxpTimeLine
{

    public enum Mxp_Tracks_HitTest_Info
    {
        None        = 0,
        OnTrack     = 1
    }

    public class CMxpTLTracks_Collection : CMxpTL_Object
    {
        // DELEGATES
        public delegate void MxpTrack_OnChangeClipSelection(object Sender, CMxpClip[] SelectedClips);
        public delegate void MxpTrack_OnEditClip(object Sender, CMxpClip clip, bool Edit);
        public delegate void MxpTrack_OnTrackAdded(object Sender, CMxpTrack track);

        public delegate void MxpTrack_OnTrackSelected(object Sender, CMxpTrack track);
        
        public delegate void MxpTrack_OnClipAdded(object Sender, CMxpClip clip);
        public delegate void MxpTrack_OnClipModified(object Sender, CMxpClip clip);
        public delegate void MxpTrack_OnClipRemoving(object Sender, CMxpClip clip);

        // EVENTs
        public event MxpTrack_OnChangeClipSelection OnChangeClipSelection   = null;
        public event MxpTrack_OnEditClip OnEditClip                         = null;
        public event MxpTrack_OnTrackAdded OnTrackAdded                     = null;

        public event MxpTrack_OnClipAdded OnClipAdded                       = null;
        public event MxpTrack_OnClipModified OnClipModified                 = null;
        public event MxpTrack_OnClipRemoving OnClipRemoving                 = null;
        public event MxpTrack_OnTrackSelected OnTrackSelected               = null;

        public event MxpClip_OnVirtualObjectNeed OnVirtualObjectNeed        = null;


        // GRAPHICAL PUBLIC PROPERTIES
        private Color FBackColor                = Color.FromArgb(62, 62, 62);
        private Color FTrackBackColor           = Color.FromArgb(80, 80, 80);
        private Pen FTimeLinesPen               = new Pen(Color.DimGray);
        private Color FClipColor                = Color.LightSteelBlue;
        private Color FSelectedClipColor        = Color.LightSalmon;

        // PRIVATE FIELDS
        private Bitmap FRendered                = null;
        private System.Drawing.Graphics FGrf    = null;

        private bool autoSizeHeight             = false;
        private int defaultTrackHeight          = 40;

        private CMxpTrack FSelectedTrack        = null;

        private Pen FBorderPen = new Pen(Brushes.Silver);

        private Point FStartDragPos = new Point(-1, -1);

        private long FTCin      = 0;
        private long FTCOut     = 0;
        private long FZoomIn    = 0;
        private long FZoomOut   = 0;

        private bool isRenderingWide = false;

        public Mxp_Tracks_HitTest_Info LastHitTested = Mxp_Tracks_HitTest_Info.None;
        public CMxpTrack LastClickedTrak = null;

        //public int ParentX      = 0;
        //public int ParentY      = 0;
        
        // CLASSES
        private CAdvList<CMxpTrack> tracks = new CAdvList<CMxpTrack>();

        private MxpTmeLine owner = null;

        public CAdvList<CMxpClip> DeletedClips = new CAdvList<CMxpClip>();

        // EVENTS
        public event MXPTL_OnRenderSubitem OnRender = null;
        public event MXPTL_ZoomAreaChanged OnZoomChanged = null;



        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public CMxpTLTracks_Collection() : base()
        { 
        
        }

        public CMxpTLTracks_Collection(MxpTmeLine Owner):base (Owner)
        {
            owner = Owner;
            FTimeLinesPen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;

            tracks.OnItemAdded += new EventHandler<GenericItemEventArgs<CMxpTrack>>(tracks_OnItemAdded);
        }



        void tracks_OnItemAdded(object sender, GenericItemEventArgs<CMxpTrack> e)
        {
            e.Item.OnRender                 += new CMxpTrack.MxpTrack_OnRender(track_OnRender);
            e.Item.OnSelectionChanged       += new CMxpTrack.MxpTrack_OnSelectionChanged(track_OnChangeSelection);
            e.Item.OnChangeClipSelection    += new CMxpTrack.MxpTrack_OnChangeClipSelection(track_OnChangeClipSelection);
            e.Item.OnEditClip               += new CMxpTrack.MxpTrack_OnEditClip(track_OnEditClip);
            e.Item.OnClipAdded              += new CMxpTrack.MxpTrack_OnClipAdded(track_OnClipAdded);
            e.Item.OnVirtualObjectNeed      += new MxpClip_OnVirtualObjectNeed(track_OnVirtualObjectNeed);
            e.Item.OnResize                 += new CMxpTrack.MxpTrack_OnResize(Item_OnResize);
            e.Item.OnClipRemoving           += new CMxpTrack.MxpTrack_OnClipRemoving(Item_OnClipRemoving);

            e.Item.Render();

            if (OnTrackAdded != null) OnTrackAdded(this, e.Item);
        }


        public void Render()
        {
            if (FRendered != null && FGrf != null)
            {
                FGrf.Clear(BackColor);

                DrawTracks();

                if (!isRenderingWide)
                {
                    if (OnRender != null) OnRender(this);
                }
            }
        }

        private void DrawTracks()
        {
            if (FGrf != null && FRendered != null && tracks.Count > 0)
            {
                isRenderingWide = true;

                for (int i = 0; i < tracks.Count; i++)
                {
                    CMxpTrack trk = tracks[i];

                    if (i == tracks.Count - 1)
                    {
                        isRenderingWide = false;
                    }

                    DrawTimeLine(trk);
                }
            }
        }

        private void DrawTimeLine(CMxpTrack track)
        {
            if (track.Image != null)
            {
                FGrf.DrawImage(track.Image, track.ClientBounds);// 0, track.Y, width, track.Height);
            }
        }

        public int FrameToPix(long frame)
        {
            if (FZoomOut > FZoomIn && width > 0)
            {
                return (int)(width * frame / (FZoomOut - FZoomIn));
            }
            return 0;
        }

        public long PixToFrame(int pixel)
        {
            if (FZoomOut > FZoomIn && width > 0)
            {
                return ((pixel) * (FZoomOut - FZoomIn) / width) + FZoomIn;
            }
            return 0;
        }



        #region PUBLIC METHODS

        public CMxpTrack AddTrack(string trackTitle, enMxpTrackType track_type, object Tag)
        {
            return AddTrack(trackTitle, track_type, defaultTrackHeight, Tag);
        }

        public CMxpTrack AddTrack(string trackTitle, enMxpTrackType track_type, int trackheight, object Tag)
        {
            CMxpTrack track      = new CMxpTrack(this, track_type);

            track.ID             = tracks.Count;

            track.X              = 0;
            track.Y              = 0;       // defaultTrackHeight * (tracks.Count - 1);
            track.Width          = Width;
            track.Height         = trackheight;
            track.ZoomIn         = FZoomIn;
            track.ZoomOut        = FZoomOut;
            track.Title          = trackTitle;
            track.Tag            = Tag;

            if (tracks.Count > 0)
            {
                track.Y = tracks[tracks.Count - 1].Y + tracks[tracks.Count - 1].Height;
            }

            tracks.Add(track);

            if (autoSizeHeight)
            {
                int yPos = 0;

                foreach (CMxpTrack xline in tracks)
                {
                    //xline.ContainerY = yPos;
                    xline.Y         = yPos;
                    xline.Height    = height / tracks.Count;
                    yPos            += xline.Height;
                }
            }

            return track;
        }

        public CMxpTrack GetTrack(int id)
        {
            foreach (CMxpTrack line in tracks)
            {
                if (line.ID == id)
                {
                    return line;
                }
            }
            return null;
        }

        public CMxpTrack GetTrackAt(int x, int y)
        {
            if (GetHitTest(new Point(x - this.x, y - this.y)) == Mxp_Tracks_HitTest_Info.OnTrack)
            {
                return LastClickedTrak;
            }

            return null;
        }
    
        #endregion PUBLIC METHODS




        #region TRACKS EVENTS

        private void track_OnRender(object Sender)
        {
            DrawTimeLine((CMxpTrack)Sender);
            
            if (!isRenderingWide)
            {
                Raise_OnRender();
            }
        }

        private void track_OnChangeSelection(object Sender, bool Selected)
        {
            CMxpTrack track = (CMxpTrack)Sender;

            if (Selected && OnTrackSelected != null) OnTrackSelected(this, track);

            if (Selected)
            {
                FSelectedTrack = track;

                foreach (CMxpTrack l in tracks)
                {
                    if (l != track)
                    {
                        if (l.Selected) l.Selected = false;
                    }
                }
            }
        }

        private void track_OnChangeClipSelection(object Sender, CMxpClip[] SelectedClips)
        {
            if (OnChangeClipSelection != null)
            {
                OnChangeClipSelection(this, SelectedClips);
            }
        }

        private void track_OnEditClip(object Sender, CMxpClip clip, bool Start)
        {
            if (OnEditClip != null) OnEditClip(this, clip, Start);
        }

        private void track_OnClipAdded(object Sender, CMxpClip clip)
        {
            //this.UndoManager.TakeSnapshot();
            if (OnClipAdded != null) OnClipAdded(this, clip);
        }

        private void track_OnVirtualObjectNeed(object Sender, Rectangle rect)
        {
            if (OnVirtualObjectNeed != null) OnVirtualObjectNeed(Sender, rect);
        }

        private void Item_OnResize(object Sender, Size NewSize)
        {
            CMxpTrack track = (CMxpTrack)Sender;

            for (int i = track.ID + 1; i < tracks.Count; i++)
            {
                tracks[i].Y = tracks[i - 1].ClientBounds.Bottom;
                tracks[i].Render();
            }

            Render();
        }

        private void Item_OnClipRemoving(object Sender, CMxpClip clip)
        {
            if (OnClipRemoving != null) OnClipRemoving(this, clip);
        }

        #endregion TRACKS EVENTS





        private void Resize()
        {
            if (width > 0 && height > 0)
            {
                if (FRendered != null)
                {
                    FRendered.Dispose();
                    FRendered = null;
                    FGrf.Dispose();
                    FGrf = null;
                }

                FRendered = new Bitmap(width, height);
                FGrf = System.Drawing.Graphics.FromImage(FRendered);

                Render();
            }
        }

        private void ConformeValues()
        {
            if (FZoomIn < FTCin) FZoomIn = FTCin;
            if (FZoomOut > FTCOut) FZoomOut = FTCOut;
        }

        public Mxp_Tracks_HitTest_Info GetHitTest(Point p)
        {
            LastHitTested = Mxp_Tracks_HitTest_Info.None;
            LastClickedTrak = null;

            foreach (CMxpTrack track in tracks)
            {
                if (track.ClientBounds.Contains(p))
                {
                    track.GetHitTest(new Point(p.X - track.ClientBounds.X, p.Y - track.ClientBounds.Y));
                    LastClickedTrak = track;
                    LastHitTested = Mxp_Tracks_HitTest_Info.OnTrack;
                    break;
                }
            }

            return LastHitTested;
        }

        private bool IsInside(Rectangle rect, Point p)
        {
            return (p.X >= rect.Left && p.X <= rect.Right && p.Y >= rect.Top && p.Y <= rect.Bottom);
        }

        public void SetZoom(long zoomIn, long zoomOut)
        {
            if (zoomIn != FZoomIn || zoomOut != FZoomOut)
            {
                FZoomIn  = zoomIn;
                FZoomOut = zoomOut;

                isRenderingWide = true;

                for (int i = 0; i < tracks.Count; i++)
                {
                    CMxpTrack trk = tracks[i];

                    trk.SetZoom(FZoomIn, FZoomOut, true);
                    trk.Render();
                    DrawTimeLine(trk);

                    if (i == tracks.Count - 1) isRenderingWide = false;
                }
            }
        }

        private void Raise_OnRender()
        {
            if (OnRender != null)
            {
                foreach (MXPTL_OnRenderSubitem singleCast in OnRender.GetInvocationList())
                {
                    ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;
                    try
                    {
                        if (syncInvoke != null && syncInvoke.InvokeRequired)
                        {
                            // Invokie the event on the main thread
                            syncInvoke.Invoke(OnRender, new object[] { this });
                        }
                        else
                        {
                            // Raise the event
                            singleCast(this);
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }


        protected override void InternalChangeNotification(enInternalChangeNotificationType cType)
        {
            switch (cType)
            {
                case enInternalChangeNotificationType.none:
                    break;
                case enInternalChangeNotificationType.x:
                    break;
                case enInternalChangeNotificationType.y:
                    break;
                case enInternalChangeNotificationType.position:
                    break;
                case enInternalChangeNotificationType.width:
                    foreach (CMxpTrack line in tracks) line.Width = this.width;
                    Resize();
                    Render();
                    break;
                case enInternalChangeNotificationType.height:
                    if (autoSizeHeight)
                    {
                        foreach (CMxpTrack line in tracks)
                        {
                            line.Height = height / tracks.Count;
                        }
                    }
                    Resize();
                    Render();
                    break;
                case enInternalChangeNotificationType.bound:

                    isRenderingWide = true;

                    for (int i = 0; i < tracks.Count; i++)
                    {
                        CMxpTrack trk = tracks[i];
                        trk.Width = this.width;

                        if (i == tracks.Count - 1) isRenderingWide = false;
                    }
                    Resize();
                    Render();
                    break;
                case enInternalChangeNotificationType.isResizing:
                    foreach (CMxpTrack track in this.tracks)
                    {
                        track.IsResizing = this.IsResizing;
                    }
                    break;
                default:
                    break;
            }
        }
        
        public override string Serialize()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(Properties.Resources.CMxpTracks_Decriptor);

            doc["CMxpTLTracks_Collection"]["ZoomIn"].InnerText  = ZoomIn.ToString();
            doc["CMxpTLTracks_Collection"]["ZoomOut"].InnerText = ZoomOut.ToString();

            foreach (CMxpTrack track in tracks)
            {
                doc["CMxpTLTracks_Collection"]["Tracks"].InnerXml += track.Serialize();
            }

            return doc.InnerXml;
        }

        public override void Deserialize(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            long zo = Convert.ToInt32(doc["CMxpTLTracks_Collection"]["ZoomOut"].InnerText);
            long zi = Convert.ToInt32(doc["CMxpTLTracks_Collection"]["ZoomIn"].InnerText);

            timeline_control.SetZoom(zi, zo);

            if (doc["CMxpTLTracks_Collection"]["Tracks"] != null)
            {
                foreach (XmlNode track_node in doc["CMxpTLTracks_Collection"]["Tracks"].ChildNodes)
                {
                    if (track_node["ID"] != null)
                    {
                        int id = int.Parse(track_node["ID"].InnerText);
                        bool found = false;

                        if (tracks.Count > id && id >= 0)                            
                        {
                            tracks[id].Deserialize(track_node.OuterXml);
                            found = true;
                        }

                        if (!found)
                        {
                            throw new Exception("Track not found");
                        }
                    }
                }


                #region TOLGO CLIP NUOVE NELLA TIMELINE RISPETTO IMMAGINE DI UNDO


                List<Guid> ClipOnImage = new List<Guid>();

                foreach (XmlNode track_node in doc["CMxpTLTracks_Collection"]["Tracks"].ChildNodes)
                {
                    if (track_node["ID"] != null)
                    {
                        if (track_node["Clips"] != null)
                        {
                            foreach (XmlNode clip_node in track_node["Clips"].ChildNodes)
                            {
                                if (clip_node["ID"] != null)
                                {
                                    Guid clip_guid = new Guid(clip_node["ID"].InnerText);
                                    ClipOnImage.Add(clip_guid);
                                }
                            }
                        }
                    }
                }


                foreach (CMxpClip clip in AllClips)
                {
                    // SE NELLA TIMELINE ESISTONO CLIP CHE NON ERANO PRESENTI NELL'IMMAGINE UNDO DESERIALIZZATA
                    // LE RIMUOVO

                    if (!ClipOnImage.Contains(clip.ID))
                    {
                        clip.Delete();
                    }
                }

                #endregion TOLGO CLIP NUOVE NELLA TIMELINE RISPETTO IMMAGINE DI UNDO
            }

            Render();
        }



        #region PUBLIC PROPERTIES

        public Bitmap Image
        {
            get { return FRendered; }
        }

        public long TCin
        {
            get { return FTCin; }
            set 
            {
                if (FTCin != value)
                {
                    FTCin = value;
                    ConformeValues();
                }
            }
        }

        public long TCOut
        {
            get { return FTCOut; }
            set 
            {
                if (FTCOut != value)
                {
                    FTCOut = value;
                    ConformeValues();
                }
            }
        }

        public long ZoomIn
        {
            get { return FZoomIn; }
            set 
            {
                if (FZoomIn != value && value < FZoomOut)
                {
                    FZoomIn = value;

                    this.isRenderingWide = true;

                    foreach (CMxpTrack trk in tracks)
                    {
                        trk.ZoomIn = value;
                        trk.Render();
                    }

                    Render();
                    if (OnZoomChanged != null) OnZoomChanged(this, FZoomIn, FZoomOut);
                }
            }
        }

        public long ZoomOut
        {
            get { return FZoomOut; }
            set 
            {
                if (FZoomOut != value && value > FZoomIn)
                {
                    FZoomOut = value;
                    //ConformeValues();

                    foreach (CMxpTrack trk in tracks)
                    {
                        trk.ZoomOut = value;
                        trk.Render();
                    }

                    Render();
                    if (OnZoomChanged != null) OnZoomChanged(this, FZoomIn, FZoomOut);
                }
            }
        }

        public int NumberOfLines
        {
            get { return tracks.Count; }
        }

        public bool AutoSizeHeight
        {
            get { return autoSizeHeight; }
            set 
            {
                if (autoSizeHeight != value)
                {
                    autoSizeHeight = value;
                    Render();
                }
            }
        }

        public int Count
        {
            get { return tracks.Count; }
        }

        public int LineHeight
        {
            get { return defaultTrackHeight; }
            set 
            {
                if (defaultTrackHeight != value)
                {
                    defaultTrackHeight = value;
                    Render();
                }
            }
        }

        public void Clear(bool remove_tracks)
        {
            foreach (CMxpTrack line in tracks)
            {
                foreach (CMxpClip clip in line.Clips)
                {
                    clip.Delete();
                }
            }

            if(remove_tracks) tracks.Clear();
        }

        public CMxpTrack SelectedTrack
        {
            get { return FSelectedTrack; }
        }

        public CMxpTrack this[int i]
        {
            get 
            {
                if (tracks.Count > i)
                {
                    return tracks[i];
                }
                return null;
            }
        }

        public List<CMxpClip> AllClips
        {
            get 
            {
                List<CMxpClip> lClips = new List<CMxpClip>();

                foreach (CMxpTrack trk in tracks)
                {
                    lClips.AddRange(trk.Clips);
                }

                return lClips;
            }
        }

        public List<CMxpClip> AllAudioClips
        {
            get
            {
                List<CMxpClip> lClips = new List<CMxpClip>();

                foreach (CMxpTrack trk in tracks)
                {
                    lClips.AddRange(trk.Clips.Where(x => x.Type == enCMxpClipType.Audio));
                }

                return lClips;
            }
        }

        public List<CMxpClip> AllVideoClips
        {
            get
            {
                List<CMxpClip> lClips = new List<CMxpClip>();

                foreach (CMxpTrack trk in tracks)
                {
                    lClips.AddRange(trk.Clips.Where(x => x.Type == enCMxpClipType.Video));
                }

                return lClips;
            }
        }

        public CAdvList<CMxpTrack> Tracks
        {
            get { return tracks; }
        }
    
        #endregion



        #region PUBLIC GRAPHICAL PROPERTIES
        
        public Color BackColor
        {
            get { return FBackColor; }
            set 
            {
                if (FBackColor != value)
                {
                    FBackColor = value;
                    Render();
                }
            }
        }
        
        public Color TrackBackColor
        {
            get { return FTrackBackColor; }
            set 
            {
                if (FTrackBackColor != value)
                {
                    FTrackBackColor = value;

                    foreach (CMxpTrack line in tracks) line.TimeLineBackColor = value;
                }
            }
        }

        public Color ClipColor
        {
            get { return FClipColor; }
            set
            {
                if (FClipColor != value)
                {
                    FClipColor = value;

                    foreach (CMxpTrack line in tracks) line.ClipColor = value;
                }
            }
        }

        public Color SelectedClipColor
        {
            get { return FSelectedClipColor; }
            set
            {
                if (FSelectedClipColor != value)
                {
                    FSelectedClipColor = value;

                    foreach (CMxpTrack line in tracks) line.SelectedClipColor = value;
                }
            }
        }

        #endregion
    }
}
