using System;
using System.Collections.Generic;
using System.Text;

using System.Drawing;
using System.Drawing.Drawing2D;
using MediaAlliance.Controls.MxpTimeLine.Classes;



namespace MediaAlliance.Controls.MxpTimeLine.Subcontrols
{
    public class CMxpTlZoomBar : CMxpTL_Object
    {
        // GRAPHICAL PUBLIC PROPERTIES
        private Color FTrackBackGround      = Color.FromArgb(77, 77, 77);//Color.FromArgb(77, 77, 77);
        private Color FTrackBarColor        = Color.FromArgb(119, 125, 127);//Color.FromArgb(59, 59, 59);
        private Color FTrackBarHandleColor  = Color.FromArgb(119, 125, 127);
        private Color FArrowColor           = Color.FromArgb(213, 213, 213);

        // VARIABLES
        //private int FWidth = 0;
        //private int FHeight = 0;
        private Bitmap FRendered             = null;
        private System.Drawing.Graphics FGrf = null;
        private int sideHandleSize           = 7;

        private Pen FBorderPen = new Pen(Brushes.Gray);

        private Rectangle FTrackBounds;
        private Rectangle FTrackSlideBounds;
        private Rectangle FTrackLeftButtonBounds;
        private Rectangle FTrackRightButtonBounds;
        private Rectangle FTrackSlideLeftHandleBounds;
        private Rectangle FTrackSlideRightHandleBounds;

        private Point FStartDragPos = new Point(-1, -1);
        private long FDragDiff      = 0;

        private long tcin           = 0;
        private long tcout          = 0;
        private long zoomIn         = 0;
        private long zoomOut        = 0;
        private float zoomMinLimit  = 1F;        // Pixel per frame

        public KTL_ZOOMTRACK_HITTEST_OBJ LastHitTested = KTL_ZOOMTRACK_HITTEST_OBJ.None;

        public enum KTL_ZOOMTRACK_HITTEST_OBJ
        { 
            None = 0,
            LeftButton = 1,
            RightButton = 2,
            TrackBar = 3,
            TrackSlider = 4,
            TrackSliderLeftHandle = 5,
            TrackSliderRightHandle = 6
        }

        // EVENTS
        public event MXPTL_OnRenderSubitem OnRender = null;
        public event MXPTL_ZoomAreaChanged OnZoomChanged = null;

        public CMxpTlZoomBar()
        {

        }

        public void Render()
        {
            DrawTrack();
            DrawButtons();

            if (OnRender != null) OnRender(this);
        }

        private void DrawButtons()
        {
            if (FRendered != null && FGrf != null)
            {
                FTrackLeftButtonBounds = new Rectangle(0, 0, height, height - 1);
                FTrackRightButtonBounds = new Rectangle(width - height - 1, 0, height, height - 1);

                // RIEMPIO LO SPAZIO DEI BOTTONI
                FGrf.FillRectangle(new SolidBrush(FTrackBackGround), FTrackLeftButtonBounds);
                FGrf.FillRectangle(new SolidBrush(FTrackBackGround), FTrackRightButtonBounds);
                
                FGrf.DrawRectangle(FBorderPen, FTrackLeftButtonBounds);
                FGrf.DrawRectangle(FBorderPen, FTrackRightButtonBounds);

                GraphicsPath gp = GetArrow(FTrackLeftButtonBounds, true);
                FGrf.FillPath(new SolidBrush(FArrowColor), gp);
                gp.Dispose();

                gp = GetArrow(FTrackRightButtonBounds, false);
                FGrf.FillPath(new SolidBrush(FArrowColor), gp);
                gp.Dispose();
            }
        }

        private GraphicsPath GetArrow(Rectangle rr, bool isLeft)
        {
            int border = 5;
            GraphicsPath gp = new GraphicsPath();

            if (isLeft)
            {
                gp.AddLine(new Point(rr.Right - border, rr.Y + border),
                            new Point(rr.X + border, rr.Top + rr.Height / 2));
                gp.AddLine(new Point(rr.X + border, rr.Top + rr.Height / 2),
                            new Point(rr.Right - border, rr.Top + rr.Height - border));
            }
            else
            {
                gp.AddLine(new Point(rr.X + border, rr.Y + border),
                            new Point(rr.Right - border, rr.Top + rr.Height / 2));
                gp.AddLine(new Point(rr.Right - border, rr.Top + rr.Height / 2),
                            new Point(rr.X + border, rr.Top + rr.Height - border));
            }

            gp.CloseFigure();

            return gp;
        }

        private void DrawTrack()
        {
            if (FRendered != null && FGrf != null)
            {
                // RIEMPIO LA BASE
                FTrackBounds = new Rectangle(height + 1, 0, width - (height * 2) - 2, height);
                FGrf.FillRectangle(new SolidBrush(FTrackBackGround), FTrackBounds);

                int pixIn   = FrameToPix(zoomIn);
                int pixOut  = FrameToPix(zoomOut);

                // CALCOLO GLI INGOMBRI
                FTrackSlideBounds               = new Rectangle(pixIn, 0, pixOut - pixIn, height);
                FTrackSlideLeftHandleBounds     = new Rectangle(FTrackSlideBounds.Left - sideHandleSize, 0, sideHandleSize, height);
                FTrackSlideRightHandleBounds    = new Rectangle(FTrackSlideBounds.Right, 0, sideHandleSize, height);

                Rectangle TrackBarMiddle        = new Rectangle(0, 0, Properties.Resources.ZoomHandle_Middle.Width, Properties.Resources.ZoomHandle_Middle.Height);

                TrackBarMiddle.X = FTrackSlideBounds.X + ((FTrackSlideBounds.Width / 2) - (TrackBarMiddle.Width / 2));
                TrackBarMiddle.Y = FTrackSlideBounds.Y + ((FTrackSlideBounds.Height / 2) - (TrackBarMiddle.Height / 2));

                // RIEMPIO LA TRACKBAR
                FGrf.FillRectangle(new SolidBrush(FTrackBarColor), FTrackSlideBounds);
                if (FTrackSlideBounds.Width < 5) FTrackSlideBounds.Width = 5;
                //FGrf.DrawRectangle(new Pen(Brushes.Black), FTrackSlideBounds.X, FTrackSlideBounds.Y, FTrackSlideBounds.Width - 1, FTrackSlideBounds.Height - 1);
                //DrawRoundRect(FGrf, new Pen(Brushes.Black), FTrackSlideBounds.X, FTrackSlideBounds.Y, FTrackSlideBounds.Width-1, FTrackSlideBounds.Height-1, 2);
                
                // RIEMPIO LE DUE MANIGLIE
                FGrf.DrawImage(Properties.Resources.ZoomHandle_Left, FTrackSlideLeftHandleBounds);
                FGrf.DrawImage(Properties.Resources.ZoomHandle_Right, FTrackSlideRightHandleBounds);

                // DISEGNO IL CENTRO DELLA BARRA
                if(TrackBarMiddle.Width < FTrackSlideBounds.Width) FGrf.DrawImage(Properties.Resources.ZoomHandle_Middle, TrackBarMiddle);
            }
        }

        public void MoveZoomArea(Point StartDragPos, Point Current)
        {
            long CurFrame = PixToFrame(Current.X);

            if(StartDragPos != FStartDragPos)
            {
                FStartDragPos = StartDragPos;
                FDragDiff = CurFrame - zoomIn;
            }

            long dur = zoomOut - zoomIn;
            long NewFrameIn = CurFrame - FDragDiff;

            if (NewFrameIn >= tcin && NewFrameIn + dur <= tcout)
            {
                zoomIn = CurFrame - FDragDiff;
                zoomOut = zoomIn + dur;
            }

            if (NewFrameIn < tcin)
            {
                zoomIn  = tcin;
                zoomOut = tcin + dur;
            }

            Render();

            if (OnZoomChanged != null) OnZoomChanged(this, zoomIn, zoomOut);
        }

        public void MoveZoomArea(Int32 frames)
        {
            if (zoomIn + frames >= 0 && zoomIn + frames <= TCOut && zoomOut + frames <= tcout && zoomOut + frames >= 0)
            {
                zoomIn += frames;
                zoomOut += frames;
            }
            else
            {
                if (zoomIn + frames < 0)
                {
                    long diff = zoomIn;
                    zoomIn = 0;
                    zoomOut -= diff;
                }
                else if (zoomOut + frames > tcout)
                {
                    long diff = tcout - zoomOut;
                    zoomOut = tcout;
                    zoomIn = zoomIn + diff; 
                }

            }
            Render();

            if (OnZoomChanged != null) OnZoomChanged(this, zoomIn, zoomOut);
        }

        public int FrameToPix(long frame)
        {
            if (tcout > tcin && FTrackBounds.Width > 0)
            {
                return (int)(FTrackBounds.Width * frame / (tcout - tcin)) + height;
            }
            return 0;
        }

        public long PixToFrame(int pixel)
        {
            if (tcout > tcin && FTrackBounds.Width > 0)
            {
                return (pixel - height) * (tcout - tcin) / FTrackBounds.Width;
            }
            return 0;
        }

        private void Resize()
        {
            if (width > 0 && height > 0)
            {
                if (FRendered != null)
                {
                    FRendered.Dispose();
                    FRendered = null;
                    FGrf.Dispose();
                    FGrf = null;
                }

                FRendered = new Bitmap(width, height);
                FGrf = System.Drawing.Graphics.FromImage(FRendered);
                FGrf.SmoothingMode = SmoothingMode.AntiAlias;
            }
        }

        private void ConformeValues()
        {
            if (zoomIn < tcin) zoomIn = tcin;
            if (zoomOut > tcout) zoomOut = tcout;
        }

        public KTL_ZOOMTRACK_HITTEST_OBJ GetHitTest(Point p)
        {
            if (FTrackSlideLeftHandleBounds.Contains(p)) LastHitTested = KTL_ZOOMTRACK_HITTEST_OBJ.TrackSliderLeftHandle;
            else if (FTrackSlideRightHandleBounds.Contains(p)) LastHitTested = KTL_ZOOMTRACK_HITTEST_OBJ.TrackSliderRightHandle;
            else if (FTrackSlideBounds.Contains(p)) LastHitTested = KTL_ZOOMTRACK_HITTEST_OBJ.TrackSlider;
            else if (FTrackBounds.Contains(p)) LastHitTested = KTL_ZOOMTRACK_HITTEST_OBJ.TrackBar;
            else if (FTrackLeftButtonBounds.Contains(p)) LastHitTested = KTL_ZOOMTRACK_HITTEST_OBJ.LeftButton;
            else if (FTrackRightButtonBounds.Contains(p)) LastHitTested = KTL_ZOOMTRACK_HITTEST_OBJ.RightButton;
            else LastHitTested = KTL_ZOOMTRACK_HITTEST_OBJ.None;

            return LastHitTested;
        }


        protected override void InternalChangeNotification(enInternalChangeNotificationType cType)
        {
            switch (cType)
            {
                case enInternalChangeNotificationType.none:
                    break;
                case enInternalChangeNotificationType.x:
                    break;
                case enInternalChangeNotificationType.y:
                    break;
                case enInternalChangeNotificationType.position:
                    break;
                case enInternalChangeNotificationType.width:
                    Resize();
                    Render();
                    break;
                case enInternalChangeNotificationType.height:
                    Resize();
                    Render();
                    break;
                case enInternalChangeNotificationType.bound:
                    Resize();
                    Render();
                    break;
                default:
                    break;
            }
        }


        public override string Serialize()
        {
            return "";
        }
        
        public override void Deserialize(string xml)
        {
            throw new NotImplementedException();
        }

        public void SetZoom(long zoomin, long zoomout)
        {
            zoomIn = zoomin;
            zoomOut = zoomout;
            Render();
            if (OnZoomChanged != null) OnZoomChanged(this, zoomIn, zoomOut);
        }


        #region PUBLIC PROPERTIES

        public Bitmap Image
        {
            get { return FRendered; }
        }

        public long TCin
        {
            get { return tcin; }
            set 
            {
                if (tcin != value)
                {
                    tcin = value;
                    if (tcout <= tcin) tcout = tcin + 1;

                    ConformeValues();
                }
            }
        }

        public long TCOut
        {
            get { return tcout; }
            set 
            {
                if (tcout != value)
                {
                    tcout = value;
                    if (tcin >= tcout) tcin = tcout - 1;

                    ConformeValues();
                }
            }
        }

        public long ZoomIn
        {
            get { return zoomIn; }
            set 
            {
                try
                {
                    //if (zoomIn != value && value < zoomOut)
                    if (value < zoomOut)
                    {
                        //float new_unit = (float)width / (float)(zoomOut - value);

                        if (value < 0) value = 0;

                        if (tcout - tcin <= 100)
                        {
                            zoomIn = 0;
                            zoomOut = tcout;
                            if (OnZoomChanged != null) OnZoomChanged(this, zoomIn, zoomOut);
                            Render();
                            return;
                        }

                        if (zoomOut - value >= 100)
                        {
                            zoomIn = value;
                            if (OnZoomChanged != null) OnZoomChanged(this, zoomIn, zoomOut);
                            Render();
                        }
                        else
                        { 
                        }

                        //if (new_unit <= zoomMinLimit || (TCOut - TCin * new_unit < width))
                        //{
                        //    zoomIn = value;
                        //    ConformeValues();
                        //    if (OnZoomChanged != null) OnZoomChanged(this, zoomIn, zoomOut);
                        //    Render();
                        //}
                        //else
                        //{
                        //    zoomIn = (long)(ZoomOut - (zoomMinLimit * width));

                        //    if (zoomIn < 0)
                        //    {
                        //        zoomIn = 0;
                        //        zoomOut = width;

                        //        if (zoomOut > TCOut)
                        //        {
                        //            zoomOut = TCOut;
                        //        }
                        //    }

                        //    if (OnZoomChanged != null) OnZoomChanged(this, zoomIn, zoomOut);
                        //    Render();
                        //}
                    }
                }
                catch
                { 
                }
            }
        }

        public long ZoomOut
        {
            get { return zoomOut; }
            set
            {
                try
                {

                    float new_unit = (float)width / (float)(value - zoomIn);

                    if (value > TCOut) value = TCOut;

                    if (tcout - tcin <= 100)
                    {
                        zoomIn = 0;
                        zoomOut = tcout;
                        if (OnZoomChanged != null) OnZoomChanged(this, zoomIn, zoomOut);
                        Render();
                        return;
                    }

                    if (value - zoomIn >= 100)
                    {
                        zoomOut = value;
                        if (OnZoomChanged != null) OnZoomChanged(this, zoomIn, zoomOut);
                        Render();
                    }
                    else
                    {
                    }

                    //if (value > zoomIn)
                    //{
                    //    if (new_unit <= zoomMinLimit || (TCOut - TCin * new_unit < width))
                    //    {
                    //        zoomOut = value;
                    //        ConformeValues();
                    //        Render();
                    //        if (OnZoomChanged != null) OnZoomChanged(this, zoomIn, zoomOut);
                    //    }
                    //    else
                    //    {
                    //        zoomOut = (long)(ZoomIn + (zoomMinLimit * width));

                    //        if (zoomOut > TCOut)
                    //        {
                    //            zoomOut = TCOut;
                    //            zoomIn = (long)(TCOut - (zoomMinLimit * width));
                    //        }

                    //        if (OnZoomChanged != null) OnZoomChanged(this, zoomIn, zoomOut);
                    //        Render();
                    //    }
                    //}
                }
                catch
                { 
                }
            }
        }

        /// <summary>
        /// Limite minimo dello zoom, espresso in numero di pixel per frame
        /// </summary>
        public float ZoomMinLimit
        {
            get { return zoomMinLimit; }
            set
            {
                if (zoomMinLimit != value)
                {
                    zoomMinLimit = value;
                }
            }
        }

        public float PixelPerFrame
        {
            get
            { 
                return width / (ZoomOut - ZoomIn);
            }
        }

        #endregion




        #region GRAPHICAL PUBLIC PROPERTIES

        public Color TrackBarColor
        {
            get { return FTrackBarColor; }
            set 
            {
                if (FTrackBarColor != value)
                {
                    FTrackBarColor = value;
                    Render();
                }
            }
        }

        public Color TrackBarHandleColor
        {
            get { return FTrackBarHandleColor; }
            set 
            {
                if (FTrackBarHandleColor != value)
                {
                    FTrackBarHandleColor = value;
                    Render();
                }
            }
        }

        public Color TrackBackColor
        {
            get { return FTrackBackGround; }
            set 
            {
                if (FTrackBackGround != value)
                {
                    FTrackBackGround = value;
                    Render();
                }
            }
        }

        #endregion



        public GraphicsPath DrawRoundRect(System.Drawing.Graphics g, Pen p, float X, float Y, float width, float height, float radius)
        {
            GraphicsPath gp = new GraphicsPath();

            gp.AddLine(X + radius, Y, X + width - (radius * 2), Y);
            gp.AddArc(X + width - (radius * 2), Y, radius * 2, radius * 2, 270, 90);
            gp.AddLine(X + width, Y + radius, X + width, Y + height - (radius * 2));
            gp.AddArc(X + width - (radius * 2), Y + height - (radius * 2), radius * 2, radius * 2, 0, 90);
            gp.AddLine(X + width - (radius * 2), Y + height, X + radius, Y + height);
            gp.AddArc(X, Y + height - (radius * 2), radius * 2, radius * 2, 90, 90);
            gp.AddLine(X, Y + height - (radius * 2), X, Y + radius);
            gp.AddArc(X, Y, radius * 2, radius * 2, 180, 90);
            gp.CloseFigure();

            g.DrawPath(p, gp);

            return gp;
        }
    }
}
