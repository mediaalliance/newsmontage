﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace MediaAlliance.Win32
{
    public class CWin32
    {
        [System.Obsolete("Work only on 32bit Systems > USE KGetAsyncKeyState()")]
        [DllImport("user32.dll")]
        private static extern int GetAsyncKeyState(int vKey);


        [System.Obsolete("Work only on 32bit Systems > USE KGetAsyncKeyState()")]
        [DllImport("user32.dll")]
        private static extern IntPtr GetAsyncKeyState(IntPtr vKey);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool PostMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool PostMessage(IntPtr hWnd, uint Msg, int wParam, int lParam);


        public static bool GetAsyncKeyState(Keys vKey)
        {
            return 0 != ((Int64)GetAsyncKeyState((IntPtr)vKey) & 0x8000);
        }
  
    }
}
