using System;
using System.Collections.Generic;
using System.Text;

using System.Drawing;

namespace MediaAlliance.Controls.MxpTimeLine.Classes
{
    public class CMxpTlCursor
    {
        // DELEGATES
        public delegate void KTLCursor_OnChangePosition(object Sender, int x, long frame);

        // EVENTS
        public event KTLCursor_OnChangePosition OnChangePosition = null;

        // VARIABLES 
        private Bitmap FRendered = null;
        private System.Drawing.Graphics FGrf = null;
        private Point[] FShape = null;

        private long FFramePosition = 0;
        private Int32 FPixelPosition = 0;

        private long FTCOut;

        public int CenterDiff = 0;

        // CLASSEs
        private CMxpTlGrid FMyGrid = null;

        public CMxpTlCursor(CMxpTlGrid MyGrid)
        {
            FMyGrid = MyGrid;

            FRendered = new Bitmap(14, 8);
            FGrf = System.Drawing.Graphics.FromImage(FRendered);

            CenterDiff = -(FRendered.Width / 2);

            FShape = new Point[3];
            FShape[0] = new Point(0, 0);
            FShape[1] = new Point(FRendered.Width, 0);
            FShape[2] = new Point(FRendered.Width / 2, FRendered.Height );

            Render();
        }

        public void Render()
        {
            FGrf.FillPolygon(Brushes.SteelBlue, FShape);
        }




        #region PUBLIC PROPERTIES


        public Bitmap Image
        {
            get { return FRendered; }
        }

        public long FramePosition
        {
            get { return FFramePosition; }
            set 
            {
                //if (FFramePosition != value)
                {
                    FFramePosition = value;

                    if (OnChangePosition != null)
                    {
                        FPixelPosition = FrameToPix(value);
                        OnChangePosition(this, FPixelPosition, FFramePosition);
                    }
                }
            }
        }

        public Int32 PixelPosition
        {
            get { return FPixelPosition; }
            set 
            {
                if (FPixelPosition != value)
                {
                    FPixelPosition = value;
                    if (OnChangePosition != null)
                    {
                        FFramePosition = PixToFrame(value);
                        OnChangePosition(this, FPixelPosition, FFramePosition);
                    }
                }
            }
        }

        public long TCOut
        {
            get { return FTCOut; }
            set { FTCOut = value; }
        }

        public int FrameToPix(long frame)
        {
            if (FMyGrid.TCOut > FMyGrid.TCin && FMyGrid.Width > 0)
            {
                double OneFFSize = (double)FMyGrid.Width / (FMyGrid.TCOut - FMyGrid.TCin);

                int ftp = (int)((frame - FMyGrid.TCin) * OneFFSize);
                //if (!Relative) ftp = (int)((frame) * OneFFSize);

                return (ftp == 0) ? (int)(OneFFSize) : ftp;
            }
            return 0;
        }

        public long PixInFrame(int pixel)
        {
            if (FMyGrid.TCOut > FMyGrid.TCin && FMyGrid.Width > 0)
            {
                int pix0 = FrameToPix(FMyGrid.TCin);
                long frame = (pixel * (FMyGrid.TCOut - FMyGrid.TCin) / FMyGrid.Width);// +FMyGrid.TCin;

                //if (frame < 0) frame = 0;
                if (frame > FTCOut) frame = (int)FTCOut;

                return frame;
            }
            return 0;
        }

        public long PixToFrame(int pixel)
        {
            return PixInFrame(pixel) + FMyGrid.TCin;
        }
	

        #endregion PUBLIC PROPERTIES



    }
}
