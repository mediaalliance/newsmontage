﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Drawing;
using System.Reflection;



namespace MediaAlliance.Controls.MxpTimeLine.Objects
{
    public class CMxpTL_ClipInfo
    {
        public int file_id              = 0;
        public int id_onprojectfile     = 0;
        public string filename          = "";
        public string description       = "";
        public long Tcin                = 0;
        public long Tcout               = 0;
        public long Clipping_in         = 0;
        public long Clipping_out        = 0;
        public int drag_point_x         = -1;
        public Dictionary<string, object> ClipAttributes = new Dictionary<string, object>();

        public Bitmap Thumbnail         = null;

        [XmlIgnore()]
        public CMxpClip timeline_clip   = null;
        public object Tag               = null;
        public object AudioClass        = null;

        public object CensorInfo        = null;



        public CMxpTL_ClipInfo()
        {
        }


        #region STATIC METHODS

        public static bool operator !=(CMxpTL_ClipInfo x, CMxpTL_ClipInfo y) 
        {
            object o1 = x;
            object o2 = y;
            if (o1 == null && o2 != null) return true;
            if (o1 != null && o2 == null) return true;
            if (o1 == null && o2 == null) return false;

            if(x.filename.Equals(y.filename, StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            return true;
        }

        public static bool operator ==(CMxpTL_ClipInfo x, CMxpTL_ClipInfo y) 
        {
            return !(x != y);
        }

        #endregion STATIC METHODS



        public CMxpTL_ClipInfo(string filename, int fileid, long tcin, long tcout, CMxpClip timeline_clip)
        {
            this.filename       = filename;
            this.file_id        = fileid;
            this.Tcin           = tcin;
            this.Tcout          = tcout;
            this.timeline_clip  = timeline_clip;
        }

        public bool IsFullClip
        {
            get 
            {
                if (Clipping_in > 0 || Clipping_out < Tcout) return false;
                return true;
            }
        }

        public CMxpTL_ClipInfo Copy()
        {
            CMxpTL_ClipInfo copy = new CMxpTL_ClipInfo();

            PropertyInfo[] pInfos = this.GetType().GetProperties();

            foreach (PropertyInfo pInfo in pInfos)
            {
                if (pInfo.CanWrite && pInfo.CanRead)
                {
                    pInfo.SetValue(copy, pInfo.GetValue(this, null), null);
                }
            }

            FieldInfo[] pFileds = this.GetType().GetFields();

            foreach (FieldInfo pFiled in pFileds)
            {
                pFiled.SetValue(copy, pFiled.GetValue(this));
            }

            return copy;
        }

    }
}
