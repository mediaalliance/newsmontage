﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Xml;
using System.IO;
using MediaAlliance.Controls.MxpTimeLine;
using MediaAlliance.Controls.MxpTimeLine.Objects;



namespace MediaAlliance.Controls.MxpTimeLine.Classes
{
    class CProjectManager
    {
        // DELEGATES

        // EVENTS
        public event MXPTL_OnProjectNeedClipInfo OnProjectNeedClipInfo  = null;
        public event MXPTL_OnProjectRequestClip OnProjectRequestClip    = null;

        private MxpTmeLine timeline_control = null;




        public CProjectManager(MxpTmeLine timeline)
        { 
            this.timeline_control = timeline;
        }

        public XmlDocument Save()
        {
            XmlDocument doc = new XmlDocument();

            doc.LoadXml(@"<?xml version=""1.0"" encoding=""UTF-8""?><project version=""1.0""></project>");

            XmlNode MAIN = doc["project"];

            XmlNode MEDIANODE = WriteMediaNode(MAIN);

            XmlNode CLIPSNODE = WriteClipNodes(MEDIANODE);

            XmlNode MONTAGENODE = WriteMontageNode(MAIN);

            

            return doc;
        }

        public void Load(string xml)
        {
            //XmlDocument doc = new XmlDocument();
            //doc.LoadXml(xml);

            //if (doc["project"] != null)
            //{
            //    XmlNodeList CLIPNODES = doc.GetElementsByTagName("clip");

            //    foreach (XmlNode clipnode in CLIPNODES)
            //    {
            //        string filename = clipnode.Attributes["file"].Value;
            //        string clipid = clipnode.Attributes["clipid"].Value;

            //        int id = Int32.Parse(clipid);
            //        id -= 1000;

            //        if (OnProjectRequestClip != null) OnProjectRequestClip(this, id, filename);
            //    }

            //    foreach (XmlNode tracknodes in doc["project"]["video_montage"].ChildNodes)
            //    {
            //        if (tracknodes.Name.StartsWith("video_"))
            //        {
            //            int idtrack = Int32.Parse(tracknodes.Name.Substring(tracknodes.Name.IndexOf("_") + 1));


            //        }
            //    }
            //}
        }


        #region READ METHODS



        #endregion READ METHODS


        #region WRITE METHODS

        private XmlNode WriteMediaNode(XmlNode main_node)
        {
            XmlNode media_node = main_node.OwnerDocument.CreateNode(XmlNodeType.Element, "media", "");

            media_node.Attributes.Append(media_node.OwnerDocument.CreateAttribute("id"));
            media_node.Attributes.Append(media_node.OwnerDocument.CreateAttribute("video_code"));
            media_node.Attributes.Append(media_node.OwnerDocument.CreateAttribute("title"));
            media_node.Attributes.Append(media_node.OwnerDocument.CreateAttribute("show"));
            media_node.Attributes.Append(media_node.OwnerDocument.CreateAttribute("datetime"));
            media_node.Attributes.Append(media_node.OwnerDocument.CreateAttribute("story_title"));
            media_node.Attributes.Append(media_node.OwnerDocument.CreateAttribute("author"));
            media_node.Attributes.Append(media_node.OwnerDocument.CreateAttribute("status"));

            main_node.AppendChild(media_node);

            return media_node;
        }

        private XmlNode WriteClipNodes(XmlNode main_node)
        {
            XmlNode clip_nodes = main_node.OwnerDocument.CreateNode(XmlNodeType.Element, "clips", "");

            List<object> clips_tags = new List<object>();
            List<string> clips_file = new List<string>();

            //int idclip = 0;

            foreach (CMxpTrack trk in timeline_control.Tracks.Tracks)
            {
                foreach (CMxpClip clip in trk.Clips)
                {
                    if (!clips_tags.Contains(clip.Tag))
                    {
                        clips_tags.Add(clip.Tag);

                        if (OnProjectNeedClipInfo != null)
                        {
                            CMxpTL_ClipInfo clipinfo = new CMxpTL_ClipInfo("", 0, 0, 0, clip);
                            OnProjectNeedClipInfo(this, clip, ref clipinfo);

                            if (clipinfo != null && clipinfo.filename.Trim() != "")
                            {
                                XmlNode clip_node = main_node.OwnerDocument.CreateNode(XmlNodeType.Element, "clip", "");

                                clip_node.Attributes.Append(clip_node.OwnerDocument.CreateAttribute("assetid"));
                                clip_node.Attributes.Append(clip_node.OwnerDocument.CreateAttribute("clipid"));
                                clip_node.Attributes.Append(clip_node.OwnerDocument.CreateAttribute("title"));
                                clip_node.Attributes.Append(clip_node.OwnerDocument.CreateAttribute("full"));
                                clip_node.Attributes.Append(clip_node.OwnerDocument.CreateAttribute("file"));
                                clip_node.Attributes.Append(clip_node.OwnerDocument.CreateAttribute("name"));
                                clip_node.Attributes.Append(clip_node.OwnerDocument.CreateAttribute("n_tracks"));
                                clip_node.Attributes.Append(clip_node.OwnerDocument.CreateAttribute("in"));
                                clip_node.Attributes.Append(clip_node.OwnerDocument.CreateAttribute("out"));
                                clip_node.Attributes.Append(clip_node.OwnerDocument.CreateAttribute("totaldur"));
                                clip_node.Attributes.Append(clip_node.OwnerDocument.CreateAttribute("clip_error"));

                                clip_node.Attributes["assetid"].Value       = "-";
                                clip_node.Attributes["clipid"].Value        = "" + (clipinfo.file_id + 1000);
                                clip_node.Attributes["title"].Value         = System.IO.Path.GetFileName(clipinfo.filename);
                                clip_node.Attributes["full"].Value          = "0";
                                clip_node.Attributes["file"].Value          = clipinfo.filename;
                                clip_node.Attributes["name"].Value          = "";
                                clip_node.Attributes["n_tracks"].Value      = "0";
                                clip_node.Attributes["in"].Value            = "0";
                                clip_node.Attributes["out"].Value           = clipinfo.tcout.ToString();
                                clip_node.Attributes["totaldur"].Value      = clipinfo.tcout.ToString();
                                clip_node.Attributes["clip_error"].Value    = "";

                                clip_nodes.AppendChild(clip_node);
                            }

                            //idclip++;
                        }
                    }
                }
            }
            

            //XmlNode clip_node = main_node.OwnerDocument.CreateNode(XmlNodeType.Element, "media", "");


            main_node.AppendChild(clip_nodes);

            return clip_nodes;
        }

        private XmlNode WriteMontageNode(XmlNode main_node)
        {
            XmlNode montage_node = main_node.OwnerDocument.CreateNode(XmlNodeType.Element, "video_montage", "");

            montage_node.Attributes.Append(main_node.OwnerDocument.CreateAttribute("zoom_in"));
            montage_node.Attributes.Append(main_node.OwnerDocument.CreateAttribute("zoom_out"));

            montage_node.Attributes["zoom_in"].Value    = timeline_control.ZoomIn.ToString();
            montage_node.Attributes["zoom_out"].Value   = timeline_control.ZoomOut.ToString();

            XmlNode tracks_node = main_node.OwnerDocument.CreateNode(XmlNodeType.Element, "videos", "");


            XmlNode track_video1_node = main_node.OwnerDocument.CreateNode(XmlNodeType.Element, "video_1", "");

            foreach (CMxpClip clip in timeline_control.Tracks[1].Clips)
            {
                WriteClipNode(track_video1_node, clip);
            }

            montage_node.AppendChild(track_video1_node);


            XmlNode track_video2_node = main_node.OwnerDocument.CreateNode(XmlNodeType.Element, "video_2", "");

            foreach (CMxpClip clip in timeline_control.Tracks[3].Clips)
            {
                WriteClipNode(track_video2_node, clip);
            }


            montage_node.AppendChild(track_video2_node);

            main_node.AppendChild(montage_node);

            return montage_node;
        }

        private XmlNode WriteTrackNode(XmlNode main_node, CMxpTrack track, string name_suffix)
        {
            XmlNode track_node = main_node.OwnerDocument.CreateNode(XmlNodeType.Element, name_suffix + "_" + track.ID, "");

            main_node.AppendChild(track_node);

            return track_node;
        }
        
        private XmlNode WriteClipNode(XmlNode main_node, CMxpClip clip)
        {
            XmlNode clip_node = main_node.OwnerDocument.CreateNode(XmlNodeType.Element, "videocliptl_" + clip.ID, "");
            
            clip_node.Attributes.Append(main_node.OwnerDocument.CreateAttribute("id"));
            clip_node.Attributes.Append(main_node.OwnerDocument.CreateAttribute("sourceclipid"));
            clip_node.Attributes.Append(main_node.OwnerDocument.CreateAttribute("In"));
            clip_node.Attributes.Append(main_node.OwnerDocument.CreateAttribute("Out"));
            clip_node.Attributes.Append(main_node.OwnerDocument.CreateAttribute("startTimeline"));

            CMxpTL_ClipInfo clipinfo = new CMxpTL_ClipInfo("", 0, 0, 0, clip);

            if (OnProjectNeedClipInfo != null) OnProjectNeedClipInfo(this, clip, ref clipinfo);

            clip_node.Attributes["id"].Value            = clip.ID.ToString();
            clip_node.Attributes["sourceclipid"].Value  = (clipinfo.file_id + 1000).ToString();
            clip_node.Attributes["In"].Value            = clip.ClippingIn.ToString();
            clip_node.Attributes["Out"].Value           = clip.ClippingOut.ToString() ;
            clip_node.Attributes["startTimeline"].Value = (clip.ClippingIn + clip.TimcodeIn).ToString();

            if (clip.Group.Count > 0)
            {
                if (clip.Group[0].Type == enCMxpClipType.Audio)
                {
                    CMxpClip clip_audio = clip.Group[0];

                    XmlNode volumes_node = main_node.OwnerDocument.CreateNode(XmlNodeType.Element, "volumes", "");

                    volumes_node.Attributes.Append(main_node.OwnerDocument.CreateAttribute("global"));
                    volumes_node.Attributes["global"].Value = clip_audio.AudioInfo.Main0gain.ToString();

                    foreach (CAudioNodeInfo ninfo in clip_audio.AudioInfo.GetAllNodes())
                    {
                        XmlNode volumepoint_node = main_node.OwnerDocument.CreateNode(XmlNodeType.Element, "volumepoints", "");

                        volumepoint_node.Attributes.Append(main_node.OwnerDocument.CreateAttribute("time"));
                        volumepoint_node.Attributes.Append(main_node.OwnerDocument.CreateAttribute("volume"));

                        volumepoint_node.Attributes["time"].Value = ninfo.timecode.ToString();
                        volumepoint_node.Attributes["volume"].Value = ninfo.value.ToString().Replace(",", "."); ;
                        
                        volumes_node.AppendChild(volumepoint_node);
                    }

                    clip_node.AppendChild(volumes_node);
                }
            }


            main_node.AppendChild(clip_node);

            return clip_node;
        }

        #endregion WRITE METHODS
    }
}
