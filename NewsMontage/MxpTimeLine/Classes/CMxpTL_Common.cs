﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Drawing;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.InteropServices;


using MediaAlliance.Controls.MxpTimeLine.UndoManager;



namespace MediaAlliance.Controls.MxpTimeLine.Classes
{
    public enum enInternalChangeNotificationType
    { 
        none,
        x,
        y,
        position,
        width,
        height,
        bound,
        isResizing
    }

    public abstract class CMxpTL_Object
    {
        // DELEGATES
        public delegate void MxpTL_OnBeginEditing(object Sender);
        public delegate void MxpTL_OnEndEditing(object Sender);

        // EVENTS
        public event MxpTL_OnBeginEditing OnBeginEditing    = null;
        public event MxpTL_OnEndEditing OnEndEditing        = null;



        protected int x                     = 0;
        protected int y                     = 0;
        protected int width                 = 0;
        protected int height                = 0;
        protected bool isResizing           = false;

        public bool IsChanged               = false;

        public CUndoManager UndoManager     = null;

        private Rectangle clientBounds      = new Rectangle();

        [XmlIgnore()]
        private CMxpTL_Object parent        = null;

        [XmlIgnore()]
        public MxpTmeLine timeline_control  = null;

        [XmlIgnore()]
        public Dictionary<object, Rectangle> HitTest_Areas = new Dictionary<object, Rectangle>();

        public CMxpTL_Object()
        {
        }

        public CMxpTL_Object(MxpTmeLine timeline_control)
        {
            this.timeline_control   = timeline_control;
            this.UndoManager        = timeline_control.Get_UndoManager();
        }

        public CMxpTL_Object(CMxpTL_Object parent)
        { 
            this.parent         = parent;
            this.UndoManager    = parent.UndoManager;

            CMxpTL_Object p     = parent;

            while(true)
            {
                if(p.parent == null) break;
                p = p.parent;                
            }

            this.timeline_control = (p.timeline_control != null) ? p.timeline_control : null;
        }

        
        
        protected abstract void InternalChangeNotification(enInternalChangeNotificationType cType);

        public abstract string Serialize();

        public abstract void Deserialize(string xml);

        //public abstract object GetUndo_Status();

        //public abstract void SetUndo_Status(object undo_info);



        protected void Notify_BeginEditing()
        {
            if (OnBeginEditing != null) OnBeginEditing(this);
        }

        protected void Notify_EndEditing()
        {
            if (OnEndEditing != null) OnEndEditing(this);
        }


        #region PUBLIC PROPERTIES
        
        public int X
        {
            get { return x; }
            set 
            {
                if (x != value)
                {
                    x = value;
                    this.clientBounds.X = x;

                    InternalChangeNotification(enInternalChangeNotificationType.x);
                }
            }
        }

        public int Y
        {
            get { return y; }
            set 
            {
                if (y != value)
                {
                    y = value;
                    this.clientBounds.Y = y;

                    InternalChangeNotification(enInternalChangeNotificationType.y);
                }
            }
        }

        public int Width
        {
            get { return width; }
            set 
            {
                if (width != value)
                {
                    width = value;
                    this.clientBounds.Width = value;

                    InternalChangeNotification(enInternalChangeNotificationType.width);
                }
            }
        }

        public int Height
        {
            get { return height; }
            set 
            {
                if (height != value)
                {
                    height = value;
                    this.clientBounds.Height = value;

                    InternalChangeNotification(enInternalChangeNotificationType.height);
                }
            }
        }
        
        [XmlIgnore()]
        public CMxpTL_Object Parent
        {
            get { return parent; }
            set
            {
                if (parent != value)
                {
                    this.parent = value;
                }
            }
        }

        [XmlIgnore()]
        public Rectangle ClientBounds
        {
            get { return clientBounds; }
            set 
            {
                if (clientBounds != value)
                {
                    clientBounds = value;

                    this.x      = value.X;
                    this.y      = value.Y;
                    this.width  = value.Width;
                    this.height = value.Height;

                    InternalChangeNotification(enInternalChangeNotificationType.bound);
                }
            }
        }

        public Point RootPosition
        {
            get
            {
                Point pos = new Point(0, 0);

                CMxpTL_Object obj = this;

                while (obj != null)
                {
                    pos.X += obj.x;
                    pos.Y += obj.y;
                    obj = obj.parent;
                }

                return pos;
            }
        }

        public bool IsResizing
        {
            get { return isResizing; }
            set
            {
                if (isResizing != value)
                {
                    isResizing = value;

                    InternalChangeNotification(enInternalChangeNotificationType.isResizing);
                }
            }
        }

        #endregion PUBLIC PROPERTIES
    }
}
