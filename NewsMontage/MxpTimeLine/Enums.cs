﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MediaAlliance.Controls.MxpTimeLine
{

    [Flags]
    public enum MxpTimelineAction
    {
        none                = 0x0,
        isZooming           = 0x1,
        isPanning           = 0x2,
        isEditingClipIn     = 0x4,
        isEditingClipOut    = 0x8,
        isScrubbing         = 0x10,
        isMarkingIn         = 0x20,
        isMarkingOut        = 0x40
    }

    public enum enTcChangeType
    { 
        timecode_in,
        timecode_out,
        both
    }

    public enum MxpClipThumbLayout
    { 
        none,
        firstOnly,
        firstAndLast,
        allPossible
    }

    public enum MxpMagnetPoint
    { 
        undefined,
        onStart,
        onEnd,
        onMiddle
    }

}
