﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Drawing.Drawing2D;



namespace MediaAlliance.Controls.MxpTimeLine.TrackInfo
{
    public partial class MxpTL_TrackInfo : UserControl
    {
        private MxpTimeLine.MxpTmeLine timeLineControl = null;
        private CMxpTrack selectedTrack = null;
        private bool canDisableTracks = true;

        private Bitmap BGCache = null;


        public MxpTL_TrackInfo()
        {
            InitializeComponent();

            this.DoubleBuffered = true;

        }

        private void Render()
        {
            RenderBackground();
            Refresh();
        }


        private void RenderBackground()
        {
            if (BGCache == null)
            {
                BGCache = new Bitmap(Width, Height);
            }



            if (timeLineControl != null)
            {
                Graphics grf = Graphics.FromImage(BGCache);

                grf.SmoothingMode = SmoothingMode.AntiAlias;

                //this.BackColor = timeLineControl.TrackBackColor;

                if (Parent != null) this.BackColor = Parent.BackColor;

                foreach (CMxpTrack track in timeLineControl.Tracks.Tracks)
                {
                    int y = track.Y + timeLineControl.Tracks.Y - 1;
                    int h = track.Height;

                    GraphicsPath gp = DrawRoundRect(20, y, Width, h, 4);

                    //e.Graphics.DrawLine(new Pen(Brushes.Yellow, 1), 0, track.Y + timeLineControl.Tracks.Y, Width, track.Y + timeLineControl.Tracks.Y);

                    Color bg = track.BackColor;

                    if (track == selectedTrack) bg = Color.FromArgb(220, 143, 42);      // GetLightenColor(bg, 20);

                    grf.FillPath(new SolidBrush(bg), gp);
                    grf.DrawPath(new Pen(Brushes.Gray, 1), gp);

                    grf.DrawString(track.Title, new Font(Font.FontFamily, 7), Brushes.Gray, 23, y + 3);

                    //switch (track.Type)
                    //{
                    //    case enMxpTrackType.Audio:
                    //        grf.DrawString("A", new Font(Font.FontFamily, 7), Brushes.Gray, 23, y + 3);
                    //        break;
                    //    case enMxpTrackType.Video:
                    //        grf.DrawString("V", new Font(Font.FontFamily, 7), Brushes.Gray, 23, y + 3);
                    //        break;
                    //    default:
                    //        break;
                    //}

                    if (track.TrackIcon != null)
                    {
                        int x_icon = ((Width - 23) / 2) - (track.TrackIcon.Width / 2) + 23;
                        int y_icon = (track.Height / 2) - (track.TrackIcon.Height / 2);

                        grf.DrawImage(track.TrackIcon,
                                        new Rectangle(x_icon, y + y_icon, track.TrackIcon.Width, track.TrackIcon.Height),
                                        new Rectangle(0, 0, track.TrackIcon.Width, track.TrackIcon.Height),
                                        GraphicsUnit.Pixel);
                    }

                    if (canDisableTracks)
                    {
                        CheckBox chk = new CheckBox();

                        chk.Parent  = this;
                        chk.Left    = 25;
                        chk.Top     = y + h - chk.Height;
                        chk.Tag     = track;
                        chk.Checked = track.Off;
                        chk.Click += new EventHandler(chk_CheckedChanged);
                    }
                }

                grf.FillRectangle(new SolidBrush(Color.FromArgb(59, 59, 59)), Width - 5, 0, Width, Height);

                grf.Dispose();

                this.Refresh();
            }
        }

        
        #region PUBLIC PROPERTIES

        public MxpTimeLine.MxpTmeLine TimeLineControl
        {
            get { return timeLineControl; }
            set
            {
                if (timeLineControl != value)
                {
                    timeLineControl = value;

                    timeLineControl.Tracks.OnTrackAdded     += new CMxpTLTracks_Collection.MxpTrack_OnTrackAdded(Tracks_OnTrackAdded);
                    timeLineControl.Tracks.OnTrackSelected  += new CMxpTLTracks_Collection.MxpTrack_OnTrackSelected(Tracks_OnTrackSelected);

                    foreach (CMxpTrack trk in timeLineControl.Tracks.Tracks)
                    {
                        trk.OnChangeOffState += new CMxpTrack.MxpTrack_OnChangeOffState(trk_OnChangeOffState);
                    }

                    Render();
                }
            }
        }

        public void trk_OnChangeOffState(object Sender, bool off)
        {
            foreach (Control ctl in this.Controls)
            {
                if (ctl is CheckBox)
                {
                    if (ctl.Tag is CMxpTrack)
                    {
                        CMxpTrack trk = (CMxpTrack)ctl.Tag;

                        ((CheckBox)ctl).Checked = trk.Off;
                    }
                }
            }
        }

        public void Tracks_OnTrackSelected(object Sender, CMxpTrack track)
        {
            selectedTrack = track;
            Render();
        }

        public void Tracks_OnTrackAdded(object Sender, CMxpTrack track)
        {
            track.OnChangeOffState += new CMxpTrack.MxpTrack_OnChangeOffState(trk_OnChangeOffState);
            Render();
        }
        
        public bool CanDisableTracks
        {
            get { return canDisableTracks; }
            set
            {
                if (canDisableTracks != value)
                {
                    canDisableTracks = value;
                    Render();
                }
            }
        }

        #endregion PUBLIC PROPERTIES





        private void MxpTL_TrackInfo_Paint(object sender, PaintEventArgs e)
        {
            if (BGCache != null)
            {
                e.Graphics.DrawImage(BGCache, 0, 0);
            }
        }

        void chk_CheckedChanged(object sender, EventArgs e)
        {
            CMxpTrack trk = (CMxpTrack)((CheckBox)sender).Tag;

            trk.Off = ((CheckBox)sender).Checked;

            trk.Selected = true;
        }





        #region GRAPHICS FUNCTIONS

        private GraphicsPath DrawRoundRect(float X, float Y, float width, float height, float radius)
        {
            GraphicsPath gp = new GraphicsPath();

            gp.AddLine(X + radius, Y, X + width - (radius * 2), Y);
            gp.AddArc(X + width - (radius * 2), Y, radius * 2, radius * 2, 270, 90);
            gp.AddLine(X + width, Y + radius, X + width, Y + height - (radius * 2));
            gp.AddArc(X + width - (radius * 2), Y + height - (radius * 2), radius * 2, radius * 2, 0, 90);
            gp.AddLine(X + width - (radius * 2), Y + height, X + radius, Y + height);
            gp.AddArc(X, Y + height - (radius * 2), radius * 2, radius * 2, 90, 90);
            gp.AddLine(X, Y + height - (radius * 2), X, Y + radius);
            gp.AddArc(X, Y, radius * 2, radius * 2, 180, 90);
            gp.CloseFigure();
            
            return gp;
        }
        
        private Color GetDarkenColor(Color source, int factor)
        {
            int R = source.R;
            int G = source.G;
            int B = source.B;

            R -= factor;
            G -= factor;
            B -= factor;

            if (R < 0) R = 0;
            if (G < 0) G = 0;
            if (B < 0) B = 0;

            return Color.FromArgb(R, G, B);
        }

        private Color GetLightenColor(Color source, int factor)
        {
            int R = source.R;
            int G = source.G;
            int B = source.B;

            R += factor;
            G += factor;
            B += factor;

            if (R > 255) R = 255;
            if (G > 255) G = 255;
            if (B > 255) B = 255;

            return Color.FromArgb(R, G, B);
        }

        #endregion GRAPHICS FUNCTIONS




        private void MxpTL_TrackInfo_MouseUp(object sender, MouseEventArgs e)
        {

        }

        private void MxpTL_TrackInfo_Resize(object sender, EventArgs e)
        {
            if (BGCache != null)
            {
                BGCache.Dispose();
                BGCache = null;
            }

            RenderBackground();        
        }
    }
}
