﻿namespace MediaAlliance.Controls.MxpTimeLine
{
    partial class MxpTmeLine
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Liberare le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            this.ThChangePos_Dispatcher_STOP();

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // MxpTmeLine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.DoubleBuffered = true;
            this.Name = "MxpTmeLine";
            this.Size = new System.Drawing.Size(581, 150);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.MxpTL_DragDrop);
            this.DragOver += new System.Windows.Forms.DragEventHandler(this.MxpTL_DragOver);
            this.GiveFeedback += new System.Windows.Forms.GiveFeedbackEventHandler(this.MxpTL_GiveFeedback);
            this.QueryContinueDrag += new System.Windows.Forms.QueryContinueDragEventHandler(this.MxpTL_QueryContinueDrag);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MxpTL_MouseDown);
            this.MouseLeave += new System.EventHandler(this.MxpTL_MouseLeave);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MxpTL_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MxpTL_MouseUp);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
