using System;
using System.Collections.Generic;
using System.Text;

using System.Linq;
using System.IO;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Threading;

using System.ComponentModel;

namespace MediaAlliance.AV
{
    public class CTimelinePreview : IDisposable
    {
        private delegate void dlgSetOnline(int id, bool play);
        private delegate void dlgSetOnlinePos(int id, long position, bool play);

        private enum enInternalCommandTypes
        { 
            none,
            setonline,
            seek
        }

        private class CCommand
        {
            public enInternalCommandTypes type;
            public object[] arguments;

            public CCommand(enInternalCommandTypes type, params object[] args)
            {
                this.type       = type;
                this.arguments  = args;
            }
        }

        private MediaAlliance.Controls.CAdvList<CCommand> CommandQueue = new MediaAlliance.Controls.CAdvList<CCommand>();

        private Thread ThCommandExe         = null;
        private bool ThCommandExe_Stop      = false;
        private ManualResetEvent mreCommnd  = new ManualResetEvent(false);

        public IntPtr OutputControl = IntPtr.Zero;

        private CPreview current                = null;
        private System.Windows.Forms.Panel pnl  = null;

        List<CPreview> previews = new List<CPreview>();


        public CTimelinePreview()
        {
            pnl             = new System.Windows.Forms.Panel();
            pnl.Visible     = false;

            CommandQueue.OnBeforeItemAdded  += new EventHandler<Controls.GenericItemEventArgs<CCommand>>(CommandQueue_OnBeforeItemAdded);
            CommandQueue.OnItemAdded        += new EventHandler<Controls.GenericItemEventArgs<CCommand>>(CommandQueue_OnItemAdded);

            ThCommandExe = new Thread(new ThreadStart(ThCommandExe_Task));
            ThCommandExe.IsBackground = true;
            ThCommandExe.Start();
        }

        void CommandQueue_OnBeforeItemAdded(object sender, Controls.GenericItemEventArgs<CTimelinePreview.CCommand> e)
        {
            lock (CommandQueue)
            {
                IEnumerable<CCommand> cmds = CommandQueue.Where(x => x.type == e.Item.type);

                List<CCommand> same_type = new List<CCommand>(cmds);

                if (same_type.Count > 0)
                {
                    // SE HO GIA' UN COMANDO DELLO STESSO TIPO IN LISTA SOSTITUISCO GLI ARGOMENTI CON 
                    // QUELLI ULTIMI ARRIVATI E CANCELLO L'INSERIMENTO DI QUELLO IN ARRIVO
                    same_type[0].arguments = e.Item.arguments;
                    e.Cancel = true;
                }
            }
        }

        void CommandQueue_OnItemAdded(object sender, Controls.GenericItemEventArgs<CTimelinePreview.CCommand> e)
        {
            mreCommnd.Set();
        }




        #region PUBLIC METHODS

        public int AddVideo(string filename)
        {
            CPreview preview = new CPreview();
            preview.ID = previews.Count;
            preview.OpenVideo(filename, pnl.Handle, true);
            previews.Add(preview);
            return preview.ID;
        }

        public void SetOnline(int id, bool play)
        {
            Console.WriteLine("ONLINE0 ID  : " + id);

            SetOnlinePos(id, -1, play);
        }

        public void SetOnlinePos(int id, long position, bool play)
        {
            //Console.WriteLine("ONLINE1 ID  : " + id +  " POS(" + position + ")");

            if (current != null && current.ID == id)
            {
                // L'ITEM RICHIESTO E' GIA' ATTIVO

                if (position >= 0) current.SetPosition(position);
                if (play) current.Play();
            }
            else
            {
                // SE GIA' C'ERA UN ELEMENTO VISUALIZZATO LO TOLGO

                if (current != null)
                {
                    current.Pause();
                    //current.ChangeMainWindowPreview(pnl.Handle);
                    current = null;
                }

                // RECUPERO L'ITEM CORRISPONDENTE

                List<CPreview> prev = new List<CPreview>(previews.Where(x => x.ID == id));

                if (prev.Count > 0)
                {
                    if (position >= 0) prev[0].SetPosition(position);
                    prev[0].ChangeMainWindowPreview(OutputControl);
                    if (play) prev[0].Play();

                    current = prev[0];
                }
            }
        }

        public void SetOnline_Async(int id, bool play)
        {
            //Console.WriteLine("ONLINE2 ID  : " + id);
            CCommand cmd = new CCommand(enInternalCommandTypes.setonline, id, -1, play);
            CommandQueue.Add(cmd);
        }

        public void SetOnline_Async(int id, long position, bool play)
        {
            //Console.WriteLine("ONLINE2 ID  : " + id + " POS(" + position + ")");
            CCommand cmd = new CCommand(enInternalCommandTypes.setonline, id, position, play);
            CommandQueue.Add(cmd);
        }

        public void SetNoVideo()
        {
            if (current != null)
            {
                current.Visible = false; //.ChangeMainWindowPreview(pnl.Handle);
                current = null;
            }
        }

        public void SetAudioVolume(float db)
        {
            if (current != null)
            {
                current.SetAudio(db);
            }
        }

        public void Play(int id)
        {
            Console.WriteLine("PLAY ID     : " + id);

            CPreview prev = GetById(id);

            if (prev != null)
            {
                prev.Play();
            }
        }

        public void Pause(int id)
        {
            Console.WriteLine("PAUSE ID    : " + id);

            CPreview prev = GetById(id);

            if (prev != null)
            {
                prev.Pause();
            }
        }

        public void Setposition(long position)
        {
            if (current != null)
            {
                current.SetPosition(position);
            }
        }

        #endregion PUBLIC METHODS




        private CPreview GetById(int id)
        {         
            List<CPreview> prev = new List<CPreview>(previews.Where(x => x.ID == id));

            if (prev.Count > 0)
            {
                return prev[0];
            }

            return null;
        }

        private void ThCommandExe_Task()
        {
            dlgSetOnline setol = new dlgSetOnline(SetOnline);
            dlgSetOnlinePos setolpos = new dlgSetOnlinePos(SetOnlinePos);

            while (!ThCommandExe_Stop)
            {
                mreCommnd.WaitOne();
                mreCommnd.Reset();

                while (CommandQueue.Count > 0)
                {
                    lock (CommandQueue)
                    {
                        CCommand cmd = CommandQueue[0];

                        switch (cmd.type)
                        {
                            case enInternalCommandTypes.setonline:
                                if (cmd.arguments.Length == 2)
                                {
                                    SetOnline((int)cmd.arguments[0], (bool)cmd.arguments[2]);
                                }
                                else if (cmd.arguments.Length == 3)
                                {
                                    SetOnlinePos((int)cmd.arguments[0], (long)cmd.arguments[1], (bool)cmd.arguments[2]);             
                                }
                                break;
                            case enInternalCommandTypes.seek:
                                break;
                        }

                        CommandQueue.RemoveAt(0);
                    }
                }
            }
        }



        ~CTimelinePreview()
        {
            Dispose();
        }

        public void Dispose()
        {
            foreach (CPreview prev in previews) prev.Dispose();
            previews.Clear();

            ThCommandExe_Stop = true;
            mreCommnd.Set();

            if (ThCommandExe.Join(200))
            {
                ThCommandExe.Abort();
            }
        }
    }








    public class CPreview : IDisposable 
    {
        // DELEGATES
        public delegate void KPreview_OnChangePosition(object Sender, long OldPos, long NewPos);
        public delegate void KPreview_OnChangeState(object Sender, KPreviewState State);
        public delegate void KPreview_OnFinish(object Sender);
        public delegate void KPreview_OnChangedFile(object Sender, string FileName);


        // EVENTs
        public event KPreview_OnChangePosition OnChangePosition = null;
        public event KPreview_OnFinish OnFinish                 = null;
        public event KPreview_OnChangeState OnChangeState       = null;
        public event KPreview_OnChangedFile OnChangedFile       = null;
        
        // DS OBJETS
        private IGraphBuilder graph     = null;
        private IMediaControl control   = null;
        private IMediaSeeking Seeking   = null;
        private IBaseFilter VMR9        = null;
        
        // THREADS
        private Thread FTimingThread = null;
        private bool FTimingThread_Stop = false;

        // VARIABLES
        private long unity              = 400000;
        private long FDuration          = 0;
        private bool FFinishRaised      = false;
        private long FCurrentPosition   = 0;
        private IntPtr FOutputWin       = IntPtr.Zero;
        private bool FAutoPauseOnSeek   = true;
        private bool FAutoRewindOnStop  = true;
        private long FMarkIn            = 0;
        private long FMarkOut           = 0;
        private bool FLoaded            = false;
        private string filename         = "";
        private bool visible            = false;
        private KPreviewState FState = KPreviewState.None;

        private PointF FCurrentMarked = new PointF(0, 0);


        public int ID = 0;

        // ENUM
        public enum KPreviewState
        { 
            None,
            Playing,
            PlayingMarked,
            Stoped,
            Cued,
            Paused,
            Finished
        }


        [DllImport("user32.dll")]
        static extern bool GetClientRect(IntPtr hWnd, out Rectangle lpRect);


        public CPreview()
        {
        }

        ~CPreview()
        {
            Dispose();
        }

        public void Dispose()
        {
            Clear();
        }



        #region PUBLIC METHODS

        public bool OpenVideo(string file, IntPtr Handle, bool StartPaused)
        {
            return OpenVideo(file, Handle, StartPaused, 0, 0);
        }

        public bool OpenVideo(string file, bool StartPaused, long Markin, long Markout)
        {
            return OpenVideo(file, FOutputWin, StartPaused, Markin, Markout);
        }

        public bool OpenVideo(string file, IntPtr Handle, bool StartPaused, long Markin, long Markout)
        {
            FLoaded = false;
            Clear();

            FMarkIn = 0;
            FMarkOut = 0;

            if (File.Exists(file))
            {
                filename    = file;
                graph       = (IGraphBuilder)new DirectShowLib.FilterGraph();
                control     = (IMediaControl)graph;
                Seeking     = (IMediaSeeking)control;
                
                FOutputWin  = Handle;

                IBaseFilter Source = null;
                try
                {
                    graph.AddSourceFilter(file, "video", out Source);
                }
                catch
                {
                   
                }

                if (Source == null)
                {
                    try
                    {
                        Source = FilterGraphTools.AddFilterFromClsid(graph, new Guid("{E436EBB5-524F-11CE-9F53-0020AF0BA770}"), "Source");
                        IFileSourceFilter s = (IFileSourceFilter)Source;
                        s.Load(file, null);
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.Write("KavTools->OpenVideo() Error : " + ex.Message);
                    }
                }

                // AGGIUNGO VMR9
                VMR9 = FilterGraphTools.AddFilterFromClsid(graph, new Guid("{51B4ABF3-748F-4E3B-A276-C828330E926A}"), "VMR9");

                if (VMR9 != null)
                {
                    ((IVMRFilterConfig9)VMR9).SetRenderingMode(VMR9Mode.Windowless);
                    ((IVMRFilterConfig9)VMR9).SetRenderingPrefs(VMR9RenderPrefs.DoNotRenderBorder);
                    //((IVMRMixerControl9)VMR9).SetAlpha(0, 0.5F);
                }

                List<IPin> listaPin = getPin(Source, PinDirection.Output);
                foreach (IPin var in listaPin)
                {
                    //IPin SPin = getPin(Source, PinDirection.Output)[0];
                    graph.Render(var);
                }

                //FilterGraphTools.SaveGraphFile(graph, "c:\\prova.grf");

                

                Refresh_Duration();                                 // AGGIORNO LA DURATA TOTALE DEL FILE

                // AGGIORNO EVENTUALE MARCATURA
                if (Markin >= 0 && Markin < FDuration - 1) FMarkIn = Markin;
                if (Markout > FMarkIn && Markout <= FDuration) FMarkOut = Markout;
                if (Markout == 0) FMarkOut = FDuration;

                int hr = control.Run();
                
                if (hr >= 0 && Handle != IntPtr.Zero)
                {
                    FState = KPreviewState.Cued;
                    if (OnChangeState != null) OnChangeState(this, FState);

                    if (OnChangedFile != null) OnChangedFile(this, file);

                    ChangeMainWindowPreview(Handle);
                    GotoStart();                                // VADO ALL'INIZIO
                    if (StartPaused) Pause();           // SE RICHIESTO METTO IN PAUSE
                    
                    FTimingThread_Stop = false;
                    FTimingThread = new Thread(new ThreadStart(FTimingThread_Task));
                    FTimingThread.IsBackground = true;
                    FTimingThread.Start();
                }

                FLoaded = true;

                return (hr >= 0);              
            }

            return false;
        }

        public void Clear()
        {
            try
            {
                System.Diagnostics.Debug.Write("KPreview : Clear()");

                if (graph != null)
                {
                    Stop();
                }

                if (FTimingThread != null)
                {
                    FTimingThread_Stop = true;
                    if (!FTimingThread.Join(100))
                    {
                        FTimingThread.Abort();
                    }
                    FTimingThread = null;
                }

                FCurrentPosition = 0;
                FDuration = 0;
                FMarkIn = 0;
                FMarkOut = 0;
                FOutputWin = IntPtr.Zero;

                if (graph != null)
                {
                    Stop();

                    // -----------------------------------------------------------------------------------
                    /* DISTRUGGI ELIMINA ERASA CANCELLA FINALIZZA SPEZZA FRAMMENTA TERMINA  il builder */

                    //System.Diagnostics.Debug.Write("    > Going to remove pin");
                    //FilterGraphTools.DisconnectAllPins(graph);
                    //FilterGraphTools.RemoveAllFilters(graph);
                    //Marshal.ReleaseComObject(graph);
                    //Marshal.FinalReleaseComObject(graph);

                    //GC.Collect();
                    //GC.WaitForPendingFinalizers();

                    Seeking = null;
                    control = null;
                    graph = null;
                    // -----------------------------------------------------------------------------------
                }

                FState = KPreviewState.None;
                if (OnChangeState != null) OnChangeState(this, FState);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write("KavTools->Clear() Error : " + ex.Message);
            }
        }

        public bool OpenVideo(string file, bool StartPaused)
        {
            return OpenVideo(file, IntPtr.Zero, StartPaused);
        }

        public bool OpenVideo(string file, IntPtr Handle)
        {
            return OpenVideo(file, Handle, false);
        }

        public bool OpenVideo(string file)
        {
            return OpenVideo(file, IntPtr.Zero, false);
        }

        public bool Play()
        {
            int hr = -1;
            //System.Diagnostics.Debug.Write("KPreview : Play()");
            if (graph != null)
            {
                hr = control.Run();
                FState = KPreviewState.Playing;
                if (OnChangeState != null) OnChangeState(this, FState);
            }

            return (hr >= 0);
        }

        public bool PlayMarked(long tcin, long tcout)
        {
            if (tcin >= 0 && tcout <= Duration && tcin < tcout)
            {
                SetPosition(tcin);

                FCurrentMarked.X = tcin;
                FCurrentMarked.Y = tcout;

                control.Run();
                FState = KPreviewState.PlayingMarked;

                return true;
            }
            else
            {
                FCurrentMarked.X = 0;
                FCurrentMarked.Y = 0;
                return false;
            }
        }

        public void Pause()
        {
            //System.Diagnostics.Debug.Write("KPreview : Pause()");
            if (graph != null)
            {
                control.Pause();
                FState = KPreviewState.Paused;
                if (OnChangeState != null) OnChangeState(this, FState);
            }
        }

        public void Stop()
        {
            //System.Diagnostics.Debug.Write("KPreview : Stop()");
            if (graph != null)
            {
                control.Stop();
                if (FAutoRewindOnStop)
                {
                    Seeking.SetPositions(FMarkIn * unity, AMSeekingSeekingFlags.AbsolutePositioning, 0, AMSeekingSeekingFlags.NoPositioning);
                }
                FState = KPreviewState.Stoped;
                if (OnChangeState != null) OnChangeState(this, FState);
            }
        }

        public void SetPosition(long frame)
        {
            long HoldPos = FCurrentPosition;
            if (Seeking != null)
            {
                Seeking.SetPositions(frame * unity, AMSeekingSeekingFlags.AbsolutePositioning, 0, AMSeekingSeekingFlags.NoPositioning);
                if (OnChangePosition != null) OnChangePosition(this, HoldPos, frame);
            }
        }

        public void GotoStart()
        {
            //System.Diagnostics.Debug.Write("KPreview : GotoStart()");
            Seeking.SetPositions(FMarkIn * unity, AMSeekingSeekingFlags.AbsolutePositioning, 0, AMSeekingSeekingFlags.NoPositioning);
        }

        #endregion PUBLIC METHODS



        #region PUBLIC PROPERTIES

        public long CurrentPosition
        {
            get
            { 
                long Frame = 0;
                if (graph != null)
                {
                    Seeking.GetCurrentPosition(out Frame);
                    Frame = Frame / unity;
                }
                return Frame - FMarkIn;
            }
            set
            {
                if (graph != null)
                {
                    if (value >= FMarkIn && value <= FMarkOut)
                    {
                        SetPosition(value);
                        if (FAutoPauseOnSeek) Pause();
                    }
                }
            }
        }

        public long Duration
        {
            get
            {
                if(FMarkIn == 0 && FMarkOut == 0) return FDuration;
                return FMarkOut - FMarkIn;
            }
        }

        public long FileDuration
        {
            get
            {
                return FDuration;
            }
        }

        public string Filename
        {
            get { return filename; }
        }

        public void ChangeMainWindowPreview(IntPtr handle)
        {
            //if (handle == FOutputWin) return;

            if (graph != null)
            {
                System.Drawing.Rectangle rect;
                GetClientRect(handle, out rect);
                IVideoWindow win = (IVideoWindow)graph;
                win.put_WindowStyle(DirectShowLib.WindowStyle.Child | DirectShowLib.WindowStyle.ClipChildren | DirectShowLib.WindowStyle.ClipSiblings);
                win.SetWindowPosition(0, 0, rect.Right, rect.Bottom);

                Visible = false;

                win.put_Owner(IntPtr.Zero);
                win.put_Owner(handle);

                Visible = true;
            }
        }

        public bool Visible
        {
            get { return visible; }
            set 
            {
                visible = value;

                IVideoWindow win = (IVideoWindow)graph;

                if (value)
                {
                    win.put_Visible(OABool.True);
                }
                else
                {
                    win.put_Visible(OABool.False); 
                }
            }
        }

        public bool AutoPauseOnSeek
        {
            get { return FAutoPauseOnSeek; }
            set { FAutoPauseOnSeek = value; }
        }

        public bool AutoRewindOnStop
        {
            get { return FAutoRewindOnStop; }
            set { FAutoRewindOnStop = value; }
        }

        public KPreviewState State
        {
            get { return FState; }
        }
        
        public void SetAudio(float db)
        {
            if(db > 0) db = 0;
            if(db < -100) db = -100;
            
            IBasicAudio audio = (IBasicAudio)graph;

            audio.put_Volume((int)db * 100);

            //IVMRFilterConfig vmr = (IVMRFilterConfig)graph;
        }
                
        #endregion PUBLIC PROPERTIES

        

        private bool IsInsideLimit(long Frame)
        {
            if (FMarkIn == 0 && FMarkOut == 0)
            {
                return (Frame > 0 && Frame <= FDuration);
            }
            else
            {
                return (Frame > FMarkIn && Frame <= FDuration);
            }
        }

        private void Refresh_Duration()
        {
            if (graph != null)
            {
                long Frame = 0;
                if (graph != null)
                {
                    Seeking.GetDuration(out Frame);
                    FDuration = Frame / unity;
                }
            }
        }

        private void FTimingThread_Task()
        {
            while (!FTimingThread_Stop)
            { 
                if (graph != null)
                {
                    long Frame = 0;
                    Seeking.GetCurrentPosition(out Frame);          // RECUPERO LA POSIZIONE DA DIRECTSHOW
                    Frame = Frame / unity;

                    bool Conformed = false;

                    if (Frame < FMarkIn)
                    {
                        System.Diagnostics.Debug.Write("KPreview : Conforming Position");
                        SetPosition(FMarkIn);
                        Conformed = true;
                    }

                    if (Frame > FMarkOut)
                    {
                        System.Diagnostics.Debug.Write("KPreview : Conforming Position");
                        SetPosition(FMarkOut);
                        Pause();
                        Conformed = true;
                    }

                    if (!Conformed)
                    {
                        if (Frame != FCurrentPosition)
                        {
                            FFinishRaised = false;

                            if (OnChangePosition != null)
                            {
                                OnChangePosition(this, FCurrentPosition - FMarkIn, Frame - FMarkIn);
                            }
                        }
                    }

                    if (Frame == FMarkOut && !FFinishRaised)
                    {
                        Pause();
                        FFinishRaised = true;
                        if (OnFinish != null) OnFinish(this);
                    }

                    if (State == KPreviewState.PlayingMarked)
                    {
                        if (Frame >= FCurrentMarked.Y)
                        {
                            Pause();
                        }
                    }

                    FCurrentPosition = Frame;

                    Thread.Sleep(40);
                }
            }
        }

        private static List<IPin> getPin(IBaseFilter filter, PinDirection direc)
        {
            IEnumPins epins;

            List<IPin> array = new List<IPin>();

            int hr = filter.EnumPins(out epins);
            IntPtr fetched = Marshal.AllocCoTaskMem(4);
            IPin[] pins = new IPin[1];
            while (epins.Next(1, pins, fetched) == 0)
            {
                PinInfo pinfo;
                pins[0].QueryPinInfo(out pinfo);
                bool found = (pinfo.dir == direc);
                DsUtils.FreePinInfo(pinfo);
                if (found)
                {
                    array.Add(pins[0]);
                }
            }
            return array;
        }
    }
}
