﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MediaAlliance.AV.MediaInfo;



namespace NewsMontange.LogHelper
{
    public class CLogHelper
    {
        public static void WriteClipInfo(MediaAlliance.Logger.ILogger logger, MediaAlliance.AV.MediaInfo.MediaInformations media_info)
        {
            logger.info("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
            logger.info("Loading clip infos :");
            logger.info("  > FILE                : " + media_info.Filename);

            logger.info("  > DURATION            : " + media_info.GetValue(GeneralInfoRequest.Duration));


            // VIDEO

            logger.info("  VIDEO STREAMS : [" + media_info.VideoCount + "]");

            for (int i = 0; i < media_info.VideoCount; i++)
            {
                logger.info("     > VIDEO [" + media_info.GetValue(VideoInfoRequest.ID, i) + "]");
                logger.info("       > Format              : " + media_info.GetValue(VideoInfoRequest.ID, i));
                logger.info("       > Format              : " + media_info.GetValue(VideoInfoRequest.Format, i));
                logger.info("       > CodecID             : " + media_info.GetValue(VideoInfoRequest.CodecID, i));
                logger.info("       > BitRate             : " + media_info.GetValue(VideoInfoRequest.BitRate, i));
                logger.info("       > BitRate_Mode        : " + media_info.GetValue(VideoInfoRequest.BitRate_Mode, i));
                logger.info("       > Resolution          : " + media_info.GetValue(VideoInfoRequest.Width, i) + " x " + media_info.GetValue(VideoInfoRequest.Height, i));
                logger.info("       > PixelAspectRatio    : " + media_info.GetValue(VideoInfoRequest.PixelAspectRatio, i));
                logger.info("       > DisplayAspectRatio  : " + media_info.GetValue(VideoInfoRequest.DisplayAspectRatio, i));
                logger.info("       > FrameRate_Mode      : " + media_info.GetValue(VideoInfoRequest.FrameRate_Mode, i));
                logger.info("       > FrameRate           : " + media_info.GetValue(VideoInfoRequest.FrameRate, i));
                logger.info("       > Standard            : " + media_info.GetValue(VideoInfoRequest.Standard, i));
                logger.info("       > Colorimetry         : " + media_info.GetValue(VideoInfoRequest.Colorimetry, i));
                logger.info("       > ScanType            : " + media_info.GetValue(VideoInfoRequest.ScanType, i));
                logger.info("       > aspectratio         : " + media_info.GetValue(VideoInfoRequest.DisplayAspectRatio, i));
            }

            // AUDIO

            logger.info("  AUDIO STREAMS : [" + media_info.AudioCount + "]");
            
            for (int i = 0; i < media_info.AudioCount; i++)
            {
                //foreach (AudioInfoRequest info in Enum.GetValues(typeof(AudioInfoRequest)))
                //{
                //    logger.info("     > " + info.ToString() + "               : " + media_info.GetValue(info, i));
                //}

                logger.info("     > AUDIO [" + media_info.GetValue(AudioInfoRequest.ID, i) + "]");
                logger.info("       > Format              : " + media_info.GetValue(AudioInfoRequest.ID, i));
                logger.info("       > Format              : " + media_info.GetValue(AudioInfoRequest.Format, i));
                logger.info("       > CodecID             : " + media_info.GetValue(AudioInfoRequest.CodecID, i));
                logger.info("       > BitRate             : " + media_info.GetValue(AudioInfoRequest.BitRate, i));
                logger.info("       > BitRate_Mode        : " + media_info.GetValue(AudioInfoRequest.BitRate_Mode, i));
                logger.info("       > Channel__s          : " + media_info.GetValue(AudioInfoRequest.Channel__s, i));
                logger.info("       > SamplingRate        : " + media_info.GetValue(AudioInfoRequest.SamplingRate, i));
                logger.info("       > SamplingCount       : " + media_info.GetValue(AudioInfoRequest.SamplingCount, i));
                logger.info("       > Resolution          : " + media_info.GetValue(AudioInfoRequest.Resolution, i));
                logger.info("       > Duration            : " + media_info.GetValue(AudioInfoRequest.Duration, i));
            }

            logger.info("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
        }
    }
}
