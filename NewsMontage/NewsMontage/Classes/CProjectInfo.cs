﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;




namespace NewsMontange.Helpers
{
    public class CProjectInfo
    {
        private DateTime creationTime = DateTime.Now;
        private string filename             = "";
        private string folder_Project       = "";

        private string project_Title        = "";
        private string project_Show         = "";
        private string project_Author       = "";
        private string project_StoryTitle   = "";

        private string newsNet_File         = "";

        private string script               = "";

        public CProjectsHelper Owner        = null;

        public List<string> ClipsNotFound = new List<string>();


        public CProjectInfo(CProjectsHelper owner)
        {
            this.Owner = owner;
        }

        public CProjectInfo(string ProjectFolder, CProjectsHelper owner)
        {
            folder_Project = Path.Combine(ProjectFolder, ID);
            this.Owner = owner;
            Update();
        }

        private void Update()
        {
            if (this.project_Title != "")
            {
                string id = "SA_" + creationTime.ToString("yyyyMMddTHHmmss");
            }
        }

        public void CheckAndCreateFolders()
        {
            check_folder(folder_Project);
            check_folder(Folder_VideoTemp);
            check_folder(Folder_FrameCache);
            check_folder(Folder_AudioTemp);
            check_folder(Folder_VoiceFiles);
            check_folder(Folder_ProjectBackups);
        }

        private void check_folder(string path)
        {
            if (path != "")
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
            }
        }


        #region PUBLIC PROPERTIES

        public DateTime CreationDate
        {
            get { return creationTime; }
            set
            {
                creationTime = value;
                Update();
            }
        }

        public string ID
        {
            get { return "SA_" + creationTime.ToString("yyyyMMddTHHmmss"); }
        }

        public string Filename
        {
            get { return filename; }
            set
            {
                filename = value;
                Update();
            }
        }

        public string Folder_Project
        {
            get { return folder_Project; }
            set
            {
                folder_Project = value;
                Update();
            }
        }

        public string Folder_VideoTemp
        {
            get { return Path.Combine(folder_Project, "VideoTMP"); }
        }

        public string Folder_VoiceFiles
        {
            get { return Path.Combine(folder_Project, "VoiceFiles"); }
        }

        public string Folder_FrameCache
        {
            get { return Path.Combine(folder_Project, "FrameCache"); }
        }

        public string Folder_AudioTemp
        {
            get { return Path.Combine(folder_Project, "AudioClipsTMP"); }
        }

        public string Folder_ProjectBackups
        {
            get { return Path.Combine(folder_Project, "ProjectBackups"); }
        }

        public string BackupsIndexFile
        {
            get { return Path.Combine(Folder_ProjectBackups, "Backups.index"); }
        }

        public string Project_Title
        {
            get { return project_Title; }
            set
            {
                project_Title = value;
                Update();
            }
        }

        public string NewsNet_File
        {
            get { return newsNet_File; }
            set
            {
                newsNet_File = value;
            }
        }

        public string Script
        {
            get { return script; }
            set
            {
                script = value;
            }
        }

        public string Project_Show
        {
            get { return project_Show; }
            set
            {
                project_Show = value;
                Update();
            }
        }

        public string Project_Author
        {
            get { return project_Author; }
            set
            {
                project_Author = value;
                Update();
            }
        }

        public string Project_StoryTitle
        {
            get { return project_StoryTitle; }
            set
            {
                project_StoryTitle = value;
                Update();
            }
        }

        #endregion PUBLIC PROPERTIES
    }
}
