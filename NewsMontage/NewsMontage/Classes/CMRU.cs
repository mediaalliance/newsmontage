﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Win32;
using System.IO;




namespace NewsMontange.Helpers
{
    public static class MRUTools
    {
        private static string basepath = @"Software\MediaAlliance\NewsMontage\MRU\";



        public class MRUItem
        {
            public string Filename;
            public DateTime date;
            public string Author = "";

            public MRUItem(string filename)
            {
                this.Filename = filename;
                this.date = DateTime.Now;
            }

            public MRUItem(string filename, DateTime date)
            {
                this.Filename = filename;
                this.date = date;
            }

            public MRUItem(string filename, DateTime date, string Author)
            {
                this.Filename = filename;
                this.date = date;
                this.Author = Author;
            }
        }





        public static MRUItem[] GetAll()
        {
            List<MRUItem> mrus = new List<MRUItem>();


            RegistryKey key = Registry.CurrentUser.CreateSubKey(basepath, RegistryKeyPermissionCheck.ReadWriteSubTree);

            if (key != null)
            {
                foreach (string sub_key_name in key.GetSubKeyNames())
                {
                    RegistryKey item_key = Registry.CurrentUser.OpenSubKey(basepath + "\\" + sub_key_name);

                    string path = (string)item_key.GetValue("path", "");
                    string auth = (string)item_key.GetValue("author", "");

                    if (path != "")
                    {
                        DateTime dt = DateTime.ParseExact(sub_key_name, "yyyyMMdd - HHmmss", null);

                        if (File.Exists(path))
                        {
                            mrus.Add(new MRUItem(path, dt, auth));
                        }
                        else
                        {
                            Remove(path);
                        }
                    }

                    item_key.Close();
                }
            }

            key.Close();

            mrus = mrus.OrderByDescending(x => x.date).ToList();

            return mrus.ToArray();
        }

        public static MRUItem FindByPath(string filename)
        {
            MRUItem result = null;

            RegistryKey key = Registry.CurrentUser.OpenSubKey(basepath);

            if (key != null)
            {
                foreach (string sub_key_name in key.GetSubKeyNames())
                {
                    RegistryKey sub_key = Registry.CurrentUser.OpenSubKey(basepath + "\\" + sub_key_name);

                    if (sub_key != null)
                    {
                        string path = (string)sub_key.GetValue("path", "");

                        if (path.Equals(filename, StringComparison.OrdinalIgnoreCase))
                        {
                            DateTime dt = DateTime.ParseExact(sub_key_name, "yyyyMMdd - HHmmss", null);

                            result = new MRUItem(filename, dt);
                            sub_key.Close();
                            break;
                        }
                    }
                }

                key.Close();
            }

            return result;
        }

        public static void Add(CProjectInfo prj, int maxItems)
        {
            MRUItem exists = FindByPath(prj.Filename);

            if (exists != null)
            {
                // ELEMENTO GIA' PRESENTE
                // MODIFICO DATA ITEM

                RegistryKey key = Registry.CurrentUser.CreateSubKey(basepath, RegistryKeyPermissionCheck.ReadWriteSubTree);

                string olddt = exists.date.ToString("yyyyMMdd - HHmmss");
                //item.date = DateTime.Now;
                string newdt = DateTime.Now.ToString("yyyyMMdd - HHmmss");

                RenameSubKey(key, olddt, newdt);

                key.Close();
            }
            else
            {
                RegistryKey key = Registry.CurrentUser.CreateSubKey(basepath, RegistryKeyPermissionCheck.ReadWriteSubTree);

                if (key != null)
                {
                    RegistryKey item_key = key.CreateSubKey(prj.CreationDate.ToString("yyyyMMdd - HHmmss"));
                    item_key.SetValue("path", prj.Filename);
                    item_key.SetValue("author", prj.Project_Author);
                    item_key.Close();
                }

                if (key.SubKeyCount > maxItems)
                {
                    List<string> names = new List<string>(key.GetSubKeyNames());
                    names.Sort();

                    while (key.SubKeyCount > maxItems)
                    {
                        key.DeleteSubKey(names[0]);
                    }
                }

                key.Close();
            }
        }

        public static void Remove(string filename)
        {
            MRUItem item = FindByPath(filename);

            if (item != null)
            {
                string keyname = basepath + item.date.ToString("yyyyMMdd - HHmmss");

                RegistryKey key = Registry.CurrentUser.OpenSubKey(basepath, RegistryKeyPermissionCheck.ReadWriteSubTree);

                key.DeleteSubKey(item.date.ToString("yyyyMMdd - HHmmss"));
            }
        }


        private static bool RenameSubKey(RegistryKey parentKey, string subKeyName, string newSubKeyName)
        {
            CopyKey(parentKey, subKeyName, newSubKeyName);
            parentKey.DeleteSubKeyTree(subKeyName);
            return true;
        }

        private static bool CopyKey(RegistryKey parentKey, string keyNameToCopy, string newKeyName)
        {
            RegistryKey destinationKey = parentKey.CreateSubKey(newKeyName);

            RegistryKey sourceKey = parentKey.OpenSubKey(keyNameToCopy);

            RecurseCopyKey(sourceKey, destinationKey);

            return true;
        }

        private static void RecurseCopyKey(RegistryKey sourceKey, RegistryKey destinationKey)
        {
            foreach (string valueName in sourceKey.GetValueNames())
            {
                object objValue = sourceKey.GetValue(valueName);
                RegistryValueKind valKind = sourceKey.GetValueKind(valueName);
                destinationKey.SetValue(valueName, objValue, valKind);
            }

            foreach (string sourceSubKeyName in sourceKey.GetSubKeyNames())
            {
                RegistryKey sourceSubKey = sourceKey.OpenSubKey(sourceSubKeyName);
                RegistryKey destSubKey = destinationKey.CreateSubKey(sourceSubKeyName);
                RecurseCopyKey(sourceSubKey, destSubKey);
            }
        }
    }

}
