﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ComponentModel;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;


namespace NewsMontange.Helpers
{
    public enum enRollbackType
    { 
        undefined,
        asOpened,
        asClosed,
        asRendered,
        beforeAddResource,
        autosave,
        beforeRestoreBackup
    }

    [Serializable()] 
    public class CRollbackInfo
    {
        public DateTime BackupDate;
        public string ProjectFilename   = "";
        public string BackupFilename    = "";
        public string Description       = "";
        public enRollbackType type      = enRollbackType.undefined;
    }

    [Serializable()] 
    public class CRollback_Collection : List<CRollbackInfo>
    {
        // DELEGATES
        [field: NonSerialized]
        public delegate void RB_OnAddedBackup(object Sender, CRollbackInfo rb_info);

        // EVENTS
        [field: NonSerialized]
        public event RB_OnAddedBackup OnAddedBackup = null;



        [NonSerialized]
        private CProjectInfo currentProjectInfo = null;

        public void Addbackup(string description, enRollbackType type)
        {
            if (currentProjectInfo == null) return;

            if (!currentProjectInfo.Owner.IsLoading)
            {
                Program.Logger.info("Adding rollback step type : " + type.ToString());

                CRollbackInfo bck = new CRollbackInfo();

                bck.BackupDate          = DateTime.Now;
                bck.Description         = description;
                bck.ProjectFilename     = currentProjectInfo.Filename;
                bck.type                = type;

                string fname = Path.GetFileName(currentProjectInfo.Filename);
                string bname = DateTime.Now.ToString("yyyyMMdd_HHmmss") + "_" + fname;

                bck.BackupFilename = bname;

                try
                {
                    currentProjectInfo.Owner.Save();

                    this.Add(bck);

                    File.Copy(currentProjectInfo.Filename, Path.Combine(currentProjectInfo.Folder_ProjectBackups, bname), true);


                    Stream stream = File.Open(currentProjectInfo.BackupsIndexFile, FileMode.Create);
                    BinaryFormatter bformatter = new BinaryFormatter();

                    bformatter.Serialize(stream, this);
                    stream.Close();

                    if (OnAddedBackup != null) OnAddedBackup(this, bck);

                    Load();
                }
                catch (Exception ex)
                { 
                }
            }
        }

        public void Load()
        {
            if (File.Exists(currentProjectInfo.BackupsIndexFile))
            {
                Stream stream = File.Open(currentProjectInfo.BackupsIndexFile, FileMode.Open);
                BinaryFormatter bformatter = new BinaryFormatter();

                CRollback_Collection obj = (CRollback_Collection)bformatter.Deserialize(stream);

                this.Clear();
                this.AddRange(obj.ToList());

                stream.Close();
            }
        }

        public void Restore(CRollbackInfo rollback_point)
        {
            string full_backup_filename = Path.Combine(currentProjectInfo.Folder_ProjectBackups, rollback_point.BackupFilename);

            Program.Logger.info("Rollback restore of :");
            Program.Logger.info("   > step description      : " + rollback_point.Description);
            Program.Logger.info("   > backup date           : " + rollback_point.BackupDate.ToString());
            Program.Logger.info("   > step type             : " + rollback_point.type.ToString());
            Program.Logger.info("   > backup file           : " + full_backup_filename);
            Program.Logger.info("   > project file          : " + currentProjectInfo.Filename);

            if (File.Exists(full_backup_filename))
            {
                Program.Logger.info("");
                Program.Logger.info("   > Restoring project file...");

                Addbackup("Before restore backup of " + rollback_point.BackupDate.ToString("dd/MM/yyyy HH.mm.ss"), enRollbackType.beforeRestoreBackup);

                try
                {
                    File.Copy(full_backup_filename, currentProjectInfo.Filename, true);
                    Program.Logger.debug("Backup file successfully restored");
                }
                catch (System.Exception ex)
                {
                    Program.Logger.error("Error during copy of backup to project file");
                    Program.Logger.error(ex);
                }

                currentProjectInfo.Owner.UnloadAll();

                currentProjectInfo.Owner.Load(currentProjectInfo.Filename, false);
            }
            else
            {
                Program.Logger.info("");
                Program.Logger.info("   > Backup file not found, procedure aborted");
            }
        }


        #region PUBLIC PROPETIES
        
        public CProjectInfo CurrentProjectInfo
        {
            get { return currentProjectInfo; }
            set
            {
                if (currentProjectInfo != value)
                {
                    currentProjectInfo = value;
                }
            }
        }

        #endregion PUBLIC PROPETIES
    }
}
