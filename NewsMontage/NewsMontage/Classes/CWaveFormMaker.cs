﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Drawing;
using System.IO;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;


using MediaAlliance.Controls.MxpTimeLine;
using MediaAlliance.Controls.MxpTimeLine.Objects;
using MediaAlliance.AV;
using MediaAlliance.Tools;
using System.Diagnostics;


namespace NewsMontange.Classes
{
    public class CWaveFormMaker
    {
        public const uint OnChangedWorking = 0x1000 + 123;

        // DELEGATES
        public delegate void wfmk_OnRendered(object Sender, CMxpTL_ClipInfo clip_info, CWaveFormClip wave_form, Bitmap bmp, int id);
        public delegate void wfmk_OnChangeWorking(object Sender, bool isWorking);

        // EVENTS
        public event wfmk_OnRendered OnRendered             = null;
        public event wfmk_OnChangeWorking OnChangeWorking   = null;


        public IntPtr MainFormHandle;
        
        private CWaveFormClip_Collection wavesCollection = new CWaveFormClip_Collection();
        private int smallPictureSize    = 400;
        private string imageCachePath   = "";

        private CWaveFormClip currentWorking = null;


        /// <summary>
        /// Waveform generator background process
        /// </summary>
        /// <param name="clip_info">CMxpTL_ClipInfo - Information about clip</param>
        /// <param name="picture_width">int - Image Render pic size</param>
        public void AddClip(CMxpTL_ClipInfo clip_info, int picture_width)
        {
            Raise_OnChangeWorking(this, true);

            CWaveFormClip item = new CWaveFormClip();

            bool loaded     = false;
            item.ClipInfo   = clip_info;

            if (imageCachePath != "")
            {
                loaded = item.Load(imageCachePath);          // TRY TO LOAD CACHE FROM FILE

                if (loaded)
                {
                    Program.Logger.debug(" Wave cache found");
                    CheckWorking();


                    //item.WaveFormPresent = true;
                }
                else
                {
                    item.GenerateWaveformCache();
                //    if (currentWorking == null)
                //    {
                //        currentWorking = item;

                //        Program.Logger.debug(" Wave cache not present, generate...");
                //        Program.Logger.debug("    > file    : " + item.ClipInfo.filename);
                //        item.GenerateWaveformCache();
                //    }
                }
            }

            item.SinglePictureWidth     = smallPictureSize;
            item.RenderWidth            = picture_width;

            item.OnRendered     += new CWaveFormClip.wfclip_OnRendered(item_OnRendered);
            item.OnRenderedAll  += new CWaveFormClip.wfclip_OnRenderedAll(item_OnRenderedAll);


            wavesCollection.Add(item);
        }

        void item_OnRenderedAll(object Sender)
        {
            CWaveFormClip clip = (CWaveFormClip)Sender;
            Program.Logger.debug(" Event for clip  : " + clip.ClipInfo.filename);

            if (imageCachePath != "" && Directory.Exists(imageCachePath))
            {

                Program.Logger.debug(" WaveForm saved for file  : " + clip.ClipInfo.filename);
                Program.Logger.debug("      on                  : " + imageCachePath);
                clip.Save(imageCachePath);
                clip.WaveFormPresent = true;
            }

            CheckWorking();
            
            currentWorking = null;

            //foreach (CWaveFormClip cclip in wavesCollection)
            //{
            //    if (cclip.WaveFormPresent == false)
            //    {
            //        Program.Logger.debug(" WaveForm going generate for : " + cclip.ClipInfo.filename);
            //        cclip.GenerateWaveformCache();
            //        break;
            //    }
            //}
        }

        private void CheckWorking()
        {
            bool working = false;

            foreach (CWaveFormClip clips in wavesCollection)
            {
                if (clips.WaitingRenders > 0)
                {
                    working = true;
                    break;
                }
            }

            if (!working)
            {
                Raise_OnChangeWorking(this, false);
            }
        }


        #region PUBLIC PROPERTIES
        
        public int SmallPictureSize
        {
            get { return smallPictureSize; }
            set
            {
                smallPictureSize = value;
            }
        }

        public string ImageCachePath
        {
            get { return imageCachePath; }
            set { imageCachePath = value; }
        }

        public CWaveFormClip this[CMxpTL_ClipInfo clip_info]
        {
            get
            {
                List<CWaveFormClip> item = wavesCollection.Where(x => x.ClipInfo == clip_info).ToList();

                if (item.Count > 0) return item[0];

                return null;
            }
        }

        #endregion PUBLIC PROPERTIES



        #region PUBLIC METHODS

        public Bitmap getWaveForm(CMxpTL_ClipInfo clip_info, long tcin, long tcout)
        {
#if DEBUG
            StackTrace stackTrace       = new StackTrace();
            StackFrame currunt_method   = stackTrace.GetFrames()[0];
            StackFrame caller_method    = stackTrace.GetFrames()[1];

            Console.WriteLine(currunt_method.GetMethod().Name.PadRight(20) +
                                "(clip_info, " + tcin + ", " + tcout + ") caller : " +
                                caller_method.GetMethod().Name + " -> " + ((System.Reflection.MethodInfo)caller_method.GetMethod()).DeclaringType.Name +
                                " Line(" + caller_method.GetFileLineNumber() + ")");
#endif

            //return new Bitmap(400, 70);


            CWaveFormClip wave_form = this[clip_info];
            Bitmap result = null;

            if (wave_form != null)
            {
                //if (tcout > clip_info.Tcout) tcout = clip_info.Tcout;

                int x0 = (int)(tcin * wave_form.RenderWidth / (clip_info.Tcout));
                int x1 = (int)(tcout * wave_form.RenderWidth / (clip_info.Tcout));

                if (x1 == x0) x1 = x0 + 1;

                if (x1 - x0 <= 0) return null;
                result = new Bitmap(x1 - x0, 70);

                Graphics gfx = Graphics.FromImage(result);

                int img_of_x0 = x0 / wave_form.SinglePictureWidth;
                int img_of_x1 = x1 / wave_form.SinglePictureWidth;

                int current_writing_x = 0;


                for (int i = img_of_x0; i <= img_of_x1; i++)
                {
                    int getting_x0  = 0;
                    int getting_w   = wave_form.SinglePictureWidth - getting_x0;

                    if (img_of_x0 == i)
                    {
                        getting_x0  = x0 % wave_form.SinglePictureWidth;
                        getting_w   = wave_form.SinglePictureWidth - getting_x0;
                    }

                    if (img_of_x1 == i)
                    {
                        getting_w = (x1 % wave_form.SinglePictureWidth) - getting_x0;
                    }

                    if (wave_form.items_cache.Count > i)
                    {
                        gfx.DrawImage(wave_form.items_cache[i].bitmap,
                                        new Rectangle(current_writing_x, 0, getting_w, result.Height),
                                        new Rectangle(getting_x0, 0, getting_w, 70),
                                        GraphicsUnit.Pixel);
                    }

                    current_writing_x += getting_w; 
                }

                gfx.Dispose();
            }

            return result;
        }

        #endregion PUBLIC METHODS



        #region EVENTS-RAISER

        private void Raise_OnChangeWorking(object Sender, bool working)
        {
            //MediaAlliance.Win32.CWin32.PostMessage(MainFormHandle, OnChangedWorking, (working)? 1 : 0, 0);
            if (OnChangeWorking != null)
            {
                foreach (wfmk_OnChangeWorking singleCast in OnChangeWorking.GetInvocationList())
                {
                    ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;

                    if (syncInvoke != null && syncInvoke.InvokeRequired)
                    {
                        syncInvoke.Invoke(OnChangeWorking, new object[] { Sender, working });
                    }
                    else
                    {
                        singleCast(Sender, working);
                    }
                }
            }
        }

        private void Raise_OnRendered(object Sender, CMxpTL_ClipInfo clip_info, CWaveFormClip wave_form, Bitmap bmp, int id)
        {
            if (OnRendered != null)
            {
                foreach (wfmk_OnRendered singleCast in OnRendered.GetInvocationList())
                {
                    ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;

                    try
                    {
                        if (syncInvoke != null && syncInvoke.InvokeRequired)
                        {
                            syncInvoke.Invoke(OnRendered, new object[] { Sender, clip_info, wave_form, bmp, id });
                        }
                        else
                        {
                            singleCast(Sender, clip_info, wave_form, bmp, id);
                        }
                    }
                    catch { }
                }
            }
        }

        #endregion EVENTS-RAISER



        #region ITEMS EVENTS

        void item_OnRendered(object Sender, Bitmap bmp, int id)
        {
            CWaveFormClip wfClip = (CWaveFormClip)Sender;
            Raise_OnRendered(this, wfClip.ClipInfo, wfClip, bmp, id);
        }

        #endregion ITEMS EVENTS

        

        #region INTERNAL CLASSES

        internal class CWaveFormClip_Collection : CAdvList<CWaveFormClip>
        {
            public CWaveFormClip_Collection()
                : base()
            { 
            
            }

        }

        [Serializable()] 
        public class CWaveform_cache_item : IDisposable
        {
            public string filename  = "";
            public int image_width  = 0;
            public long tc_in       = 0;
            public long tc_out      = 0;
            public int RenderWidth  = 0;
            public Bitmap bitmap    = null;

            public CWaveform_cache_item(string filename, int renderWidth, int id, Bitmap bmp)
            {
                this.filename       = filename;
                this.RenderWidth    = renderWidth;
                this.bitmap         = bmp;
            }

            public void Dispose()
            {
                if (this.bitmap != null) bitmap.Dispose();
            }
        }

        public class CWaveFormClip
        {
            // DELEGATES
            public delegate void wfclip_OnRendered(object Sender, Bitmap bmp, int id);
            public delegate void wfclip_OnRenderedAll(object Sender);

            private delegate void dlgLocalOnRendered(object Sender, Bitmap bmp, long tcin, long tcout, object Tag);

            // EVENTS
            public event wfclip_OnRendered OnRendered = null;
            public event wfclip_OnRenderedAll OnRenderedAll = null;



            private CMxpTL_ClipInfo clipInfo    = null;
            public CMxpClip clip                = null;
            public WaveFile waveFile            = null;
            //public CWaveformDS waveFileDS       = null;
            private int renderWidth             = -1;
            private int maxBmpWidth             = 400;
            private int picNumber               = 0;
            private int waiting_render          = 0;
            private bool loaded                 = false;
            public bool WaveFormPresent         = false;

            private int precision = 12;

            public List<CWaveform_cache_item> items_cache = new List<CWaveform_cache_item>();


            /// <summary>
            /// This class genarate and manage the image of waveforms
            /// </summary>
            public CWaveFormClip()
            {
            }


            public CMxpTL_ClipInfo ClipInfo
            {
                get { return this.clipInfo; }
                set
                {
                    if (this.clipInfo != value)
                    {
                        if (clipInfo != null)
                        {
                            ClearBitmaps();
                            waveFile.Dispose();
                        }

                        clipInfo = value;
                       
                    }
                }
            }



            #region PUBLIC PROPERTIES

            public int RenderWidth
            {
                get { return renderWidth; }
                set
                {
                    if (renderWidth != value)
                    {
                        renderWidth = value;
                        if (ClipInfo.filename.EndsWith("wav", StringComparison.OrdinalIgnoreCase))
                        {
                            //Update();
                        }
                    }
                }
            }

            public int SinglePictureWidth
            {
                get { return maxBmpWidth; }
                set
                {
                    maxBmpWidth = value;
                }
            }

            public int PicNumber
            {
                get { return picNumber; }
            }

            public int WaitingRenders
            {
                get { return waiting_render; }
            }

            #endregion PUBLIC PROPERTIES



            private void ClearBitmaps()
            {
                foreach (CWaveform_cache_item cache_item in items_cache) cache_item.Dispose();
                items_cache.Clear();
            }

            private void Update()
            {
                if (loaded) return;

                if (this.clipInfo != null && renderWidth > 0)
                {
                    if (waiting_render > 0)
                    {
                        waveFile.ClearAll();
                    }


                    picNumber = (int)Math.Round(((renderWidth / maxBmpWidth) + .5));
                    waiting_render = picNumber;

                    ClearBitmaps();

                    for (int i = 0; i < picNumber; i++)
                    {
                        CWaveform_cache_item item = new CWaveform_cache_item(clipInfo.filename, renderWidth, i, new Bitmap(SinglePictureWidth, 70));
                        this.items_cache.Add(item);
                    }

                    if (picNumber == 0) picNumber = 1;

                    for (int i = 0; i < picNumber; i++)
                    {
                        int current_bmp_width = maxBmpWidth;

                        if (i == picNumber - 1)
                        {
                            current_bmp_width = renderWidth % maxBmpWidth;
                            if (current_bmp_width == 0) current_bmp_width = maxBmpWidth;
                        }

                        Bitmap bmp = new Bitmap(current_bmp_width, 70);


                        int x0 = i * maxBmpWidth;                           // X START BLOCK
                        int x1 = x0 + current_bmp_width;                    // X END BLOCK

                        long tc0 = clipInfo.Tcout * x0 / renderWidth;       // FRAME START BLOCK
                        long tc1 = clipInfo.Tcout * x1 / renderWidth;       // FRAME END BLOCK

                        if(waveFile != null)
                            waveFile.Draw_async(bmp, Pens.Beige, tc0 * 40, tc1 * 40, precision, true, i);                      
                    }
                }
            }


            #region PUBLIC METHODS

            public void Save(string path)
            {
                path = Path.Combine(path, Path.GetFileName(clipInfo.filename));

                if (!Directory.Exists(path)) Directory.CreateDirectory(path);

                string filename = Path.GetFileName(clipInfo.filename) + ".wave";

                string fn = Path.Combine(path, filename);

                CWaveform_cache_item cache_item = new CWaveform_cache_item(clipInfo.filename, renderWidth, 0, items_cache[0].bitmap);
                
                Stream stream = File.Open(fn, FileMode.Create);
                BinaryFormatter bformatter = new BinaryFormatter();

                bformatter.Serialize(stream, items_cache);
                stream.Close();
            }

            public bool Load(string path)
            {
                path = Path.Combine(path, Path.GetFileName(clipInfo.filename));

                if (Directory.Exists(path))
                {
                    string filename = Path.GetFileName(clipInfo.filename) + ".wave";

                    string fn = Path.Combine(path, filename);

                    if (File.Exists(fn))
                    {
                        Program.Logger.info("Loading waveform cache :");
                        Program.Logger.info("   > file          : " + fn);

                        Stream stream = File.Open(fn, FileMode.Open);
                        BinaryFormatter bformatter = new BinaryFormatter();

                        List<CWaveform_cache_item> obj = (List<CWaveform_cache_item>)bformatter.Deserialize(stream);
                        
                        Program.Logger.info("   > items count   : " + obj.Count);

                        this.items_cache = obj;

                        stream.Close();

                        loaded = true;
                        return true;
                    }
                }

                return false;
            }

            public void GenerateWaveformCache()
            {
                if (clipInfo != null)
                {
                    if (Path.GetExtension(clipInfo.filename).ToLower() == ".wav")
                    {
                        // SE E' UN WAV ISTANZIO UN NUOVO OGGETTO waveFile CHE LEGGERA' I DATI
                        // E RENDERIZZERA' LE VARIE PORZIONI SU RICHIESTA

                        Program.Logger.debug(" WaveForm working on " + clipInfo.filename);

                        this.waveFile = new WaveFile(clipInfo.filename, null);

                        waveFile.OnDataRead += new WaveFile.WF_OnDataRead(waveFile_OnDataRead);
                        waveFile.OnRendered += new WaveFile.WF_OnRendered(waveFile_OnRendered);
                        waveFile.OnProgress += new WaveFile.WF_OnProgress(waveFile_OnProgress);
                        waveFile.Read();
                    }
                    else
                    {
                        //waveFileDS = new CWaveformDS(clipInfo.filename);

                        //waveFileDS
                    }
                }

                if (ClipInfo.filename.EndsWith("wav", StringComparison.OrdinalIgnoreCase))
                {
                    //Update();
                }
            }

            #endregion PUBLIC METHODS



            #region WAVEFILE EVENTS

            void waveFile_OnProgress(object Sender, Bitmap bmp, long tc, int perc, object Tag)
            {
                //Console.WriteLine("PROG : " + tc);
            }

            void waveFile_OnRendered(object Sender, Bitmap bmp, long tcin, long tcout, object Tag)
            {
                int img_index = (int)Tag;

                this.items_cache[img_index].bitmap = bmp;
                waiting_render--;

                Raise_OnRendered(bmp, (int)Tag);

                System.Threading.Thread.Sleep(1);

                if (waiting_render == 0)
                {
                    //waveFile.Dispose();
                    Raise_OnRenderedAll(this);
                }
            }

            void waveFile_OnDataRead(object Sender)
            {
                Update();
                //throw new NotImplementedException();
            }

            #endregion WAVEFILE EVENTS
            


            #region EVENTS-RAISER

            private void Raise_OnRendered(Bitmap bmp, int id)
            {
                if (OnRendered != null)
                {
                    foreach (wfclip_OnRendered singleCast in OnRendered.GetInvocationList())
                    {
                        ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;
                        
                        if (syncInvoke != null && syncInvoke.InvokeRequired)
                        {
                            syncInvoke.Invoke(OnRendered, new object[] { this, bmp, id });
                        }
                        else
                        {
                            singleCast(this, bmp, id);
                        }
                    }
                }
            }

            private void Raise_OnRenderedAll(object Sender)
            {
                if (OnRenderedAll != null)
                {
                    foreach (wfclip_OnRenderedAll singleCast in OnRenderedAll.GetInvocationList())
                    {
                        ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;

                        if (syncInvoke != null && syncInvoke.InvokeRequired)
                        {
                            syncInvoke.Invoke(OnRenderedAll, new object[] { Sender });
                        }
                        else
                        {
                            singleCast(Sender);
                        }
                    }
                }
            }
            
            #endregion EVENTS-RAISER

        }

        #endregion INTERNAL CLASSES
    }

    
}
