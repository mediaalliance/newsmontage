﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MediaAlliance.AV.MediaInfo;
using System.IO;



namespace NewsMontange.Helpers
{
    public class CSourceCheck
    {
        public bool Active              = false;

        private float m_framerate       = -1;
        private int m_minVideoStreams   = -1;
        private int m_minAudioStreams   = -1;
        private List<string> m_standard = new List<string>();



        public CSourceCheck(string[] standard, float framerate)
        {
            m_standard.AddRange(standard);
            m_framerate = framerate;
        }


        public CSourceCheckResult Check(string filename)
        {
            if (!Active) return new CSourceCheckResult(true, "Check is not active");

            CSourceCheckResult result = new CSourceCheckResult(true);

            if (File.Exists(filename))
            {
                Program.Logger.info(" [CHECKING SOURCE] " + filename);

                MediaInformations mi = new MediaInformations();

                try
                {
                    mi.Open(filename);
                    Program.Logger.info("   > File opened");
                }
                catch(Exception ex)
                {
                    Program.Logger.error("   > Error occurred opening file : ");
                    Program.Logger.error(ex);
                    return new CSourceCheckResult(false, "Impossible to analyze file", ex.Message);
                }

                if (m_minVideoStreams >= 0)
                {
                    if (mi.VideoCount < m_minVideoStreams)
                    {
                        result.Allowed = false;
                        result.ErrorMessages.Add("Video streams number error");

                        Program.Logger.error("   > Video streams number on file is incorrect");
                        Program.Logger.error("   > Video streams found " + mi.VideoCount + " required " + m_minVideoStreams);
                    }
                }

                if (m_minAudioStreams >= 0)
                {
                    if (mi.AudioCount < m_minAudioStreams)
                    {
                        result.Allowed = false;
                        result.ErrorMessages.Add("Audio streams number error");

                        Program.Logger.error("   > Audio streams number on file is incorrect");
                        Program.Logger.error("   > Audio streams found " + mi.AudioCount + " required " + m_minAudioStreams);
                    }
                }

                if (m_framerate >= 0)
                {
                    if (mi.VideoCount > 0)
                    {
                        string fr_str = mi.GetValue(VideoInfoRequest.FrameRate, 0);
                        fr_str = fr_str.Replace(",", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator);
                        fr_str = fr_str.Replace(".", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator);

                        float fr = 0;
                        Single.TryParse(fr_str, out fr);
                        
                        if (fr != m_framerate)
                        {
                            result.Allowed = false;
                            result.ErrorMessages.Add("Frame rate error (" + fr_str + " required " + m_framerate.ToString() + ")");

                            Program.Logger.error("   > Framerate on file is incorrect");
                            Program.Logger.error("   > Framerate " + fr  + " required " + m_framerate);
                        }
                    }
                }

                if (m_standard.Count > 0)
                {
                    if (mi.VideoCount > 0)
                    {
                        string standard = mi.GetValue(VideoInfoRequest.Standard, 0);

                        if (!m_standard.Contains(standard.ToUpper()))
                        {
                            result.Allowed = false;
                            result.ErrorMessages.Add("Video standard error (" + standard + " required " + m_standard + ")");

                            Program.Logger.error("   > Video standard on file is incorrect");
                            Program.Logger.error("   > Video standard " + standard + " required " + m_standard);
                        }
                        else
                        { 
                        }
                    }
                }

                mi.Close();
                mi.Dispose();
            }
            else
            {
                Program.Logger.error("   > Frame not found");
                return new CSourceCheckResult(false, "File not found");
            }

            return result;
        }


        #region PUBLIC PROPERTIES

        public float FrameRate
        {
            get { return m_framerate; }
            set
            {
                m_framerate = value;
            }
        }

        public int MinVideoStreams
        {
            get { return m_minVideoStreams; }
            set
            {
                m_minVideoStreams = value;
            }
        }

        public int MinAudioStreams
        {
            get { return m_minAudioStreams; }
            set
            {
                m_minAudioStreams = value;
            }
        }

        public List<string> VideoStandard
        {
            get { return m_standard; }
            set
            {
                m_standard = value;
            }
        }

        #endregion PUBLIC PROPERTIES
    }

    public class CSourceCheckResult
    {
        public bool Allowed = false;
        public List<string> ErrorMessages = new List<string>();

        public CSourceCheckResult(bool allowed, params string[] messages)
        {
            Allowed = allowed;
            ErrorMessages.AddRange(messages);
        }
    }
}
