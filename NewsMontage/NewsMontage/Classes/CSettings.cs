﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;
using System.IO;
using System.Reflection;




namespace NewsMontange.Classes
{
    public class CSettings
    {
        // DELEGATES
        public delegate void Setting_OnLoaded(object Sender);
        public delegate void Setting_OnSaved(object Sender);
        public delegate void Setting_OnSetupFormRequested(object Sender);

        // EVENTS
        public event Setting_OnLoaded OnLoaded                          = null;
        public event Setting_OnSaved OnSaved                            = null;
        public event Setting_OnSetupFormRequested OnSetupFormRequested  = null;


        private XmlDocument doc = null;
        
        
        public OPTIONS Options                   = null;
        public PROJECTS_OPTIONS Project_Options  = null;
        public GUI_OPTIONS GUI_Options           = null;
        public RENDER_OPTIONS RENDER_Options     = null;


        public static string NormalizePath(string path)
        {
            if (path.Trim() == "") return "";

            if (Path.IsPathRooted(path))
            {
                return path;
            }
            else
            {
                DirectoryInfo dir = new DirectoryInfo(path);
                return dir.FullName;
            }
        }


        public CSettings()
        {
            Options = new OPTIONS();
            Project_Options = new PROJECTS_OPTIONS();
            GUI_Options = new GUI_OPTIONS();
            RENDER_Options = new RENDER_OPTIONS();
        }

        public void RequestForm()
        {
            if (OnSetupFormRequested != null) OnSetupFormRequested(this);
        }

        public void Load()
        {
            Load("NM_Settings.config");
            if (OnLoaded != null) OnLoaded(this);
        }

        public void Load(string filename)
        {
            doc = new XmlDocument();
            doc.Load(filename);

            if (doc["NEWSMONTAGE_SETTINGS"] != null)
            {
                XmlNode MAIN = doc["NEWSMONTAGE_SETTINGS"];

                if (MAIN["EXTERNAL_SETTINGS"] != null)
                {
                    foreach (XmlNode file_node in MAIN["EXTERNAL_SETTINGS"].ChildNodes)
                    {
                        if (file_node.NodeType == XmlNodeType.Element && file_node.Name.Equals("file", StringComparison.OrdinalIgnoreCase))
                        {
                            if (File.Exists(file_node.InnerText))
                            {
                                try
                                {
                                    Load(file_node.InnerText);
                                }
                                catch { }
                            }
                        }
                    }
                }

                Options.Load(MAIN);
                Project_Options.Load(MAIN);
                GUI_Options.Load(MAIN);
                RENDER_Options.Load(MAIN);
            }
        }

        private string ArrayConverter(object obj)
        {
            return obj.ToString();
        }

        public void Save()
        {
            XmlNode MAIN = doc["NEWSMONTAGE_SETTINGS"];

            foreach (FieldInfo fInfo in this.GetType().GetFields())
            {
                object subclass = fInfo.GetValue(this);

                XmlNode curnode = MAIN[fInfo.FieldType.Name];

                if (curnode != null)
                {
                    foreach (FieldInfo fSubInfo in subclass.GetType().GetFields())
                    {
                        if (curnode[fSubInfo.Name] != null)
                        {
                            string valore = (string)fSubInfo.GetValue(subclass).ToString();

                            if (fSubInfo.FieldType.IsArray)
                            {
                                object[] array = (object[])fSubInfo.GetValue(subclass);
                                string[] destarray = Array.ConvertAll(array, new Converter<object, string>(ArrayConverter));

                                valore = string.Join(";", destarray);
                            }
                            curnode[fSubInfo.Name].InnerText = valore;
                        }
                    }
                }
            }

            doc.Save("NM_Settings.config");

            if (OnSaved != null) OnSaved(this);

            Load();
        }

        public class OPTIONS
        {
            public bool PREVIEW_ENGINE_DS_ONLY  = false;
            public bool TIMELINE_AUDIOSCRUB     = false;
            public bool CAN_IMPORT_VOICEFILE    = false;
            public int UNDO_TIMES               = 20;
            public bool VIDEO_INFO_OSD_VISIBLE  = false;

            public string[] VIDEOCLIP_FILTER    = new string[0];
            public string[] VOICE_FILTER        = new string[0];
            public string[] BACKGROUND_FILTER   = new string[0];

            public OPTIONS()
            {

            }

            public void Load(XmlNode main_node)
            {
                if (main_node["OPTIONS"] != null)
                {
                    XmlNode OPT = main_node["OPTIONS"];

                    if (OPT["PREVIEW_ENGINE_DS_ONLY"] != null)
                    {
                        bool.TryParse(OPT["PREVIEW_ENGINE_DS_ONLY"].InnerText, out PREVIEW_ENGINE_DS_ONLY);
                    }

                    if (OPT["TIMELINE_AUDIOSCRUB"] != null)
                    {
                        bool.TryParse(OPT["TIMELINE_AUDIOSCRUB"].InnerText, out TIMELINE_AUDIOSCRUB);
                    }

                    if (OPT["CAN_IMPORT_VOICEFILE"] != null)
                    {
                        bool.TryParse(OPT["CAN_IMPORT_VOICEFILE"].InnerText, out CAN_IMPORT_VOICEFILE);
                    }

                    if (OPT["UNDO_TIMES"] != null)
                    {
                        int.TryParse(OPT["UNDO_TIMES"].InnerText, out UNDO_TIMES);
                    }

                    if (OPT["VIDEOCLIP_FILTER"] != null)
                    {
                        VIDEOCLIP_FILTER = OPT["VIDEOCLIP_FILTER"].InnerText.Split(new char[]{ ',', ';'}, StringSplitOptions.RemoveEmptyEntries);
                    }

                    if (OPT["VOICE_FILTER"] != null)
                    {
                        VOICE_FILTER = OPT["VOICE_FILTER"].InnerText.Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                    }

                    if (OPT["BACKGROUND_FILTER"] != null)
                    {
                        BACKGROUND_FILTER = OPT["BACKGROUND_FILTER"].InnerText.Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                    }

                    if (OPT["VIDEO_INFO_OSD_VISIBLE"] != null)
                    {
                        bool.TryParse(OPT["VIDEO_INFO_OSD_VISIBLE"].InnerText, out VIDEO_INFO_OSD_VISIBLE);
                    }
                }
            }
        }

        public class PROJECTS_OPTIONS
        {
            public string PROJECTS_PATH     = ".\\Projects";
            public string NEWSNET_PRJ_PATH  = "";
            public int AUTOSAVE_TIME        = 0;
            public float FRAMERATE          = 25;
            public bool CHECK_SOURCE        = true;

            public PROJECTS_OPTIONS()
            {

            }

            public void Load(XmlNode main_node)
            {
                if (main_node["PROJECTS_OPTIONS"] != null)
                {
                    XmlNode OPT = main_node["PROJECTS_OPTIONS"];

                    if (OPT["PROJECTS_PATH"] != null)
                    {
                        PROJECTS_PATH = (OPT["PROJECTS_PATH"] != null)? OPT["PROJECTS_PATH"].InnerText : ".\\Projects";
                        PROJECTS_PATH = CSettings.NormalizePath(PROJECTS_PATH);

                        if (!System.IO.Directory.Exists(PROJECTS_PATH))
                        {
                            try
                            {
                                Directory.CreateDirectory(PROJECTS_PATH);
                                Program.Logger.info("Projects folder created : " + PROJECTS_PATH);
                            }
                            catch (Exception ex)
                            {
                                Program.Logger.error("Error creating projects folder : " + PROJECTS_PATH);
                                Program.Logger.error(ex);
                            }
                        }
                    }

                    if (OPT["NEWSNET_PRJ_PATH"] != null)
                    {
                        NEWSNET_PRJ_PATH = (OPT["NEWSNET_PRJ_PATH"] != null)? OPT["NEWSNET_PRJ_PATH"].InnerText : "";

                        if (NEWSNET_PRJ_PATH.Trim() != "")
                        {
                            NEWSNET_PRJ_PATH = CSettings.NormalizePath(NEWSNET_PRJ_PATH);
                        }
                    }

                    if (OPT["AUTOSAVE_TIME"] != null)
                    {
                        string auto_save_val_str = OPT["AUTOSAVE_TIME"].InnerText;

                        int.TryParse(auto_save_val_str, out this.AUTOSAVE_TIME);

                        if (AUTOSAVE_TIME < 0) AUTOSAVE_TIME = 0;
                    }

                    if (OPT["FRAMERATE"] != null)
                    {
                        string auto_save_val_str = OPT["FRAMERATE"].InnerText;
                        
                        auto_save_val_str = auto_save_val_str.Replace(",", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator);
                        auto_save_val_str = auto_save_val_str.Replace(".", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator);

                        Single.TryParse(auto_save_val_str, out this.FRAMERATE);

                        if (AUTOSAVE_TIME < 0) AUTOSAVE_TIME = 0;
                    }

                    if (OPT["CHECK_SOURCE"] != null)
                    {
                        string checksource_str = OPT["CHECK_SOURCE"].InnerText;
                        
                        bool.TryParse(checksource_str, out this.CHECK_SOURCE);
                    } 
                }
                
            }
        }

        public class GUI_OPTIONS
        {
            public bool DEL_CONFIRM = false;
            public string LANGUAGE = "it";

            public GUI_OPTIONS()
            {

            }

            public void Load(XmlNode main_node)
            {
                if (main_node["GUI_OPTIONS"] != null)
                {
                    XmlNode GUI_OPT = main_node["GUI_OPTIONS"];

                    if (GUI_OPT["DEL_CONFIRM"] != null)
                    {
                        bool.TryParse(GUI_OPT["DEL_CONFIRM"].InnerText, out DEL_CONFIRM);
                    }

                    if (GUI_OPT["LANGUAGE"] != null)
                    {
                        LANGUAGE = GUI_OPT["LANGUAGE"].InnerText;
                    }
                }
            }
        }

        public class RENDER_OPTIONS
        {
            // DELEGATES AND EVENTS
            public delegate void RENDEROPT_OnChangeProfile(object Sender, string profile);

            public event RENDEROPT_OnChangeProfile OnChangeProfile = null;


            // PUBLIC FIELDS

            public string RENDER_ENGINE_PATH    = "..\\NewsMontage Render Engine";
            public string EXPORT_FIX_PATH       = ".\\Export";
            public string FIX_PROFILE_AUDIOEXP  = "WAV";
            public bool EXPORT_AS_ENABLED       = false;

            // PRIVATE FIELDS
            private string m_FIX_PROFILE           = "";



            public RENDER_OPTIONS()
            {

            }

            public void Load(XmlNode main_node)
            {
                if (main_node["RENDER_OPTIONS"] != null)
                {
                    XmlNode RENDER_OPT = main_node["RENDER_OPTIONS"];

                    if (RENDER_OPT["RENDER_ENGINE_PATH"] != null)
                    {
                        RENDER_ENGINE_PATH = (RENDER_OPT["RENDER_ENGINE_PATH"] != null) ? RENDER_OPT["RENDER_ENGINE_PATH"].InnerText : "..\\NewsMontage Render Engine";
                        RENDER_ENGINE_PATH = CSettings.NormalizePath(RENDER_ENGINE_PATH);
                    }

                    if (RENDER_OPT["EXPORT_FIX_PATH"] != null)
                    {
                        EXPORT_FIX_PATH = (RENDER_OPT["EXPORT_FIX_PATH"] != null) ? RENDER_OPT["EXPORT_FIX_PATH"].InnerText : ".\\Export";
                        EXPORT_FIX_PATH = CSettings.NormalizePath(EXPORT_FIX_PATH);
                    }

                    if (RENDER_OPT["FIX_PROFILE"] != null)
                    {
                        FIX_PROFILE = (RENDER_OPT["FIX_PROFILE"] != null) ? RENDER_OPT["FIX_PROFILE"].InnerText : "";
                    }

                    if (RENDER_OPT["FIX_PROFILE_AUDIOEXP"] != null)
                    {
                        FIX_PROFILE_AUDIOEXP = (RENDER_OPT["FIX_PROFILE_AUDIOEXP"] != null) ? RENDER_OPT["FIX_PROFILE_AUDIOEXP"].InnerText : "WAV";
                    }

                    if (RENDER_OPT["EXPORT_AS_ENABLED"] != null)
                    {
                        bool.TryParse(RENDER_OPT["EXPORT_AS_ENABLED"].InnerText, out EXPORT_AS_ENABLED);
                    }
                }
            }


            public string FIX_PROFILE
            {
                get
                {
                    return m_FIX_PROFILE;
                }
                set
                {
                    m_FIX_PROFILE = value;
                    if (OnChangeProfile != null) OnChangeProfile(this, value);
                }
            }
        }
    }
}
