﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
using FFMpegHelper;
using System.ComponentModel;
using System.IO;


namespace NewsMontange.Classes
{
    public class CThumbMaker : IDisposable
    {
        // DELEGATES
        public delegate void ThM_OnDone(object Sender, string thumbReady, long position, object tag);

        // EVENTS
        public event ThM_OnDone OnDone = null;


        private class queue_item
        {
            public string fileSource        = "";
            public string pathDest          = "";
            public int sec                  = 0;
            public object Tag               = null;
            public bool done                = false;

            public queue_item(string filesource, string pathdest, int sec, object Tag)
            {
                this.fileSource = filesource;
                this.pathDest   = pathdest;
                this.sec        = sec;
                this.Tag        = Tag;
            }
        }

        private Thread TMaker               = null;
        private bool TMaker_Stop            = false;
        private ManualResetEvent mreTMaker  = new ManualResetEvent(false);
        private List<queue_item> Queue      = new List<queue_item>();
        private List<queue_item> QueueDone  = new List<queue_item>();
        FFMpegHelper.SceneChange ffmpeg_h   = new FFMpegHelper.SceneChange();

        private string projectsPath         = "";
        private System.IO.FileSystemWatcher fsw = null;



        public CThumbMaker()
        {
        }


        private void START_WATCHING()
        {
            fsw.Path                    = projectsPath;
            fsw.IncludeSubdirectories   = true;
            fsw.Filter                  = "*.jpg";
            fsw.Created                 += new System.IO.FileSystemEventHandler(fsw_Created);
            fsw.Error                   += new ErrorEventHandler(fsw_Error);
            fsw.EnableRaisingEvents     = true;
        }


        #region PUBLIC METHODS

        public void START()
        {
            if (TMaker == null)
            {
                TMaker = new Thread(new ThreadStart(TMaker_Task));
                TMaker.IsBackground = true;
                TMaker.Start();
            }
        }

        public void STOP()
        {
            if (TMaker != null)
            {
                TMaker_Stop = true;
                mreTMaker.Set();

                if (!TMaker.Join(200))
                {
                    TMaker.Abort();
                }

                TMaker_Stop = false;
                TMaker = null;
            }
        }

        public void Reset()
        {
            QueueDone.Clear();
            Queue.Clear();
            mreTMaker.Reset();
        }

        public void AddRequest(string FileSource, string DestPath, int sec, object Tag)
        {
            Queue.Insert(0, new queue_item(FileSource, DestPath, sec, Tag));
            mreTMaker.Set();
        }

        public string GetThumbFilename(string thumbsPath, string clipfile, int second, int frame)
        {
            thumbsPath = Path.Combine(thumbsPath, clipfile);

            return Path.Combine(thumbsPath, "frame_sec" + second + "_" + frame.ToString().PadLeft(6, '0') + ".jpg");
        }

        public bool ExtistThumbFor(string thumbsPath, string clipfile, int second, int frame, out string filename)
        {
            filename = "";
            if (Path.IsPathRooted(clipfile)) clipfile = Path.GetFileName(clipfile);

            string thumb_filename = GetThumbFilename(thumbsPath, clipfile, second, frame);

            if(File.Exists(thumb_filename))
            {
                filename = thumb_filename;
                return true;
            }

            return false;
        }

        public void Dispose()
        {
            STOP();
            ffmpeg_h.Stop();
            ffmpeg_h = null;

            if (fsw != null)
            {
                fsw.EnableRaisingEvents = false;
                fsw.Dispose();
            }
        }

        #endregion PUBLIC METHODS



        #region PUBLIC PROPERTIES

        public string ProjectPath
        {
            get { return projectsPath; }
            set
            {
                if (projectsPath != value)
                {
                    projectsPath = value;

                    if (fsw != null)
                    {
                        fsw.EnableRaisingEvents = false;
                        fsw.Dispose();
                    }
                    
                    //fsw                         = new System.IO.FileSystemWatcher();
                    //START_WATCHING();
                }
            }
        }

        #endregion PUBLIC PROPERTIES



        #region FILESYSTEMWATCHER EVENTS

        void fsw_Created(object sender, System.IO.FileSystemEventArgs e)
        {
            System.IO.FileAttributes att = System.IO.File.GetAttributes(e.FullPath);

            if ((att & System.IO.FileAttributes.Directory) == System.IO.FileAttributes.Directory)
            {
                return;
            }
                        
            string destpath = e.FullPath;
            string clipname = destpath.Substring(e.FullPath.LastIndexOf('\\') + 1);
            int sec = -1;

            if (e.Name.IndexOf("_") > 0)
            {
                string sec_str = clipname.Split('_')[1].Replace("sec", "");

                int.TryParse(sec_str, out sec);
            }

            if (sec >= 0)
            {
                queue_item item = null;

                foreach (queue_item it in QueueDone)
                {
                    if (it.sec == sec && it.pathDest == System.IO.Path.GetDirectoryName(e.FullPath))
                    {
                        item = it;
                        break;
                    }
                }

                if (item != null)
                {
                    Raise_OnDone(destpath, sec, item.Tag);// Queue[0].Tag);
                    QueueDone.Remove(item);
                }
                else
                {
                    //Raise_OnDone(destpath, sec, item.Tag);// Queue[0].Tag);                
                }
            }
        }

        void fsw_Error(object sender, ErrorEventArgs e)
        {
            fsw.EnableRaisingEvents = false;
            fsw.Dispose();

            fsw = new FileSystemWatcher();

            while (!fsw.EnableRaisingEvents)
            {
                try
                {
                    START_WATCHING();
                }
                catch
                {
                    System.Threading.Thread.Sleep(200);
                }
            }
        }

        #endregion FILESYSTEMWATCHER EVENTS



        private void TMaker_Task()
        {
            while (!TMaker_Stop)
            {
                queue_item item = null;

                if (Queue.Count > 0)
                {
                    item = Queue[0];

                    try
                    {
                        lock (Queue)
                        {
                            Console.WriteLine(" TH-TASK FOR : " + Path.GetFileName(item.fileSource));

                            bool done = false;
                            int times = 3;

                            while (!done && times >= 0)
                            {
                                int exit = ffmpeg_h.StartAndWait(item.fileSource,
                                                      item.sec,
                                                      1,
                                                      item.pathDest);

                                string clipname = item.pathDest.Substring(item.fileSource.LastIndexOf('\\') + 1);

                                clipname = GetThumbFilename(item.pathDest, "", item.sec, 1);

                                if (!File.Exists(clipname) || exit < 0)
                                {
                                    Console.WriteLine("   > Error : " + times);
                                    done = false;
                                    times--;
                                }
                                else
                                {
                                    done = true;
                                    //QueueDone.Add(item);
                                    Raise_OnDone(clipname, item.sec, item.Tag);
                                    //item.done = true;

                                    Queue.RemoveAt(0);
                                }
                            }
                        }
                    }
                    catch (System.Exception ex)
                    {

                    }             
                }


                if (Queue.Count == 0)
                {
                    //if (item != null) 
                    mreTMaker.WaitOne();
                    mreTMaker.Reset();
                }
                else
                {
                    Thread.Sleep(100);
                }
            }
        }


        #region RAISE METHODS

        private void Raise_OnDone(string thumbfile, long position, object tag)
        {
            if (OnDone != null)
            {
                foreach (ThM_OnDone singleCast in OnDone.GetInvocationList())
                {
                    ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;

                    if (syncInvoke != null && syncInvoke.InvokeRequired)
                    {
                        // Invokie the event on the main thread
                        syncInvoke.Invoke(OnDone, new object[] { this, thumbfile, position, tag });
                    }
                    else
                    {
                        // Raise the event
                        singleCast(this, thumbfile, position, tag);
                    }
                }
            }
        }

        #endregion RAISE METHODS

    }
}
