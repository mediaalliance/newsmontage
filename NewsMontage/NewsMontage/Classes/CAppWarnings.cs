﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MediaAlliance.Tools;



namespace NewsMontange.Warnings
{
    public class CAppWarnings
    {
        // DELEGATES
        public delegate void AW_OnWarningAdded(object Sender, CAppWarnInfo warn);
        public delegate void AW_OnWarningRemoved(object Sender);

        // EVENTS
        public event AW_OnWarningAdded OnWarningAdded       = null;
        public event AW_OnWarningRemoved OnWarningRemoved   = null;


        public CAdvList<CAppWarnInfo> Warnings = new CAdvList<CAppWarnInfo>();


        public CAppWarnings()
        {
            Warnings.OnItemAdded += new EventHandler<GenericItemEventArgs<CAppWarnInfo>>(Wanings_OnItemAdded);
            Warnings.OnItemRemoved += new EventHandler<EventArgs>(Warnings_OnItemRemoved);
        }


        #region PUBLIC METHODS

        public void AddWarn(WarnType type, string title, string message, WarnContext context)
        {
            Warnings.Add(new CAppWarnInfo(type, title, message, context));
        }

        public void Remove(WarnContext context)
        {
            List<CAppWarnInfo> toRemove = new List<CAppWarnInfo>();

            foreach (CAppWarnInfo warn in Warnings)
            {
                if (warn.context == context) toRemove.Add(warn);
            }

            foreach (CAppWarnInfo warn in toRemove)
            {
                Warnings.Remove(warn);
            }
        }

        #endregion PUBLIC METHODS



        void Wanings_OnItemAdded(object sender, GenericItemEventArgs<CAppWarnInfo> e)
        {
            if (OnWarningAdded != null) OnWarningAdded(this, e.Item);
        }

        void Warnings_OnItemRemoved(object sender, EventArgs e)
        {
            if (OnWarningRemoved != null) OnWarningRemoved(this);
        }

    }

    public enum WarnType
    { 
        generic,
        fileNotFound,
        pathNotFound,
        invalidValue
    }

    public enum WarnContext
    { 
        undefined,
        ini_configuration,
        external,
        project,
        application_input
    }

    public class CAppWarnInfo
    {
        public string Title             = "";
        public string Message           = "";
        public WarnType type            = WarnType.generic;
        public WarnContext context      = WarnContext.undefined;

        public CAppWarnInfo(WarnType type, string Title, string Message, WarnContext context)
        {
            this.type       = type;
            this.Title      = Title;
            this.Message    = Message;
            this.context    = context;
        }
    }
}
