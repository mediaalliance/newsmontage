﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MediaAlliance.Controls.MxpTimeLine;
using MediaAlliance.Controls.MxpTimeLine.Objects;

using System.Runtime.InteropServices;
using NewsMontange.Language;
using System.IO;


namespace NewsMontange.Classes
{
    /// <summary>
    /// Classe che gestisce gli eventi della tastiera e relativi Shurtcut della form principale
    /// </summary>
    public class CKeyboardManager
    {
        private MxpTmeLine mxptl                = null;
        private frmMain main_form               = null;

        public CKeyboardManager(frmMain main_form, MxpTmeLine main_timeline)
        {
            this.mxptl              = main_timeline;
            this.main_form          = main_form;
            
            if (File.Exists(Program.LanguageFile))
            {
                CDict.AddFromFile(Program.LanguageFile);
            }

        }



        [DllImport("user32.dll")]
        static extern IntPtr GetTopWindow(IntPtr hWnd);
        
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Winapi)]
        internal static extern IntPtr GetFocus();

        [DllImport("user32.dll", ExactSpelling = true, CharSet = CharSet.Auto)]
        public static extern IntPtr GetParent(IntPtr hWnd);

        [DllImport("user32.dll")]
        static extern IntPtr SetFocus(IntPtr hWnd);



        public bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            bool isTextEditorFocused = false;

            IntPtr focused_control = GetFocus();

            if (focused_control != IntPtr.Zero)
            {
                StringBuilder cName = new StringBuilder(100);
                GetClassName(focused_control, cName, cName.Capacity);

                if (cName.ToString().IndexOf("RichEdit", StringComparison.OrdinalIgnoreCase) >= 0 ||
                    cName.ToString().IndexOf("Textbox", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    isTextEditorFocused = true;
                }
            }

            bool handled = false;

            bool shift  = MediaAlliance.Win32.CWin32.GetAsyncKeyState(Keys.ShiftKey);    // (keyData & Keys.ShiftKey) == Keys.ShiftKey;
            bool ctrl   = MediaAlliance.Win32.CWin32.GetAsyncKeyState(Keys.ControlKey);    // (keyData & Keys.ControlKey) == Keys.ControlKey;

            if (keyData == Keys.Tab)
            {
                #region TAB

                handled = true;

                switch (main_form.FocusedArea)
                {
                    case enFocusedArea.MontageArea:
                    case enFocusedArea.OutputPreview:
                        handled = true;
                        main_form.SourceTimeline.Parent.Focus();
                        break;
                    case enFocusedArea.SourcePreview:
                    case enFocusedArea.SourceManager:
                        handled = true;
                        mxptl.Focus();
                        break;
                }

                #endregion TAB
            }
            else if ((keyData & Keys.Z) == Keys.Z)
            {
                if (shift && ctrl)
                {
                    mxptl.Redo();
                    return true;
                }
                else if (ctrl)
                {
                    mxptl.Undo();
                    return true;
                }
            }


            MxpTmeLine timeline = null;

            switch (main_form.FocusedArea)
            {
                case enFocusedArea.MontageArea:
                case enFocusedArea.OutputPreview:
                    timeline = mxptl;
                    break;
                case enFocusedArea.SourcePreview:
                case enFocusedArea.SourceManager:
                    timeline = main_form.SourceTimeline;
                    break;
            }

            if (timeline != null)
            {
                switch (keyData)
                {
                    case Keys.Space:
                        {
                            if (!isTextEditorFocused)
                            {
                                Program.Logger.debug("[PRESS SPACEBAR]");
                                handled = true;

                                #region SPACEBAR

                                if ((main_form.AppStatus & enApplicationStatus.recordingAudio) == enApplicationStatus.recordingAudio)
                                {
                                    // SE PREMO LA BARRASPAZIO MENTRE STO REGISTRANDO NON ESEGUO IL PLAY MA SOPPO LA REGISTRAZIONE

                                    main_form.StopVoiceRecording();
                                }
                                else
                                {
                                    if (timeline.InternalEngine.EngineStatus != MediaAlliance.Controls.MxpTimeLine.Engine.InternalEngineStatus.play)
                                    {
                                        timeline.InternalEngine.Play();
                                    }
                                    else
                                    {
                                        timeline.InternalEngine.Pause();
                                    }
                                }

                                #endregion SPACEBAR
                            }
                        }
                        break;
                    case Keys.Home:
                        {
                            if (!isTextEditorFocused)
                            {
                                Program.Logger.debug("[PRESS HOME]");
                                handled = true;

                                #region HOME

                                if (timeline.InternalEngine.EngineStatus == MediaAlliance.Controls.MxpTimeLine.Engine.InternalEngineStatus.play)
                                {
                                    // SE LA TIMELINE E' IN PLAY FACCIO PAUSE
                                    timeline.InternalEngine.Pause();
                                }

                                timeline.SetCurrentPositionAsExternal(0);

                                #endregion HOME
                            }
                        }
                        break;
                    case Keys.End:
                        {
                            if (!isTextEditorFocused)
                            {
                                Program.Logger.debug("[PRESS END]");
                                handled = true;

                                #region END

                                CMxpClip[] lastest_clips = timeline.LastestClips;
                                
                                if (timeline.InternalEngine.EngineStatus == MediaAlliance.Controls.MxpTimeLine.Engine.InternalEngineStatus.play)
                                {
                                    // SE LA TIMELINE E' IN PLAY FACCIO PAUSE
                                    timeline.InternalEngine.Pause();
                                }

                                if (lastest_clips != null && lastest_clips.Length > 0)
                                {
                                    timeline.SetCurrentPositionAsExternal(lastest_clips[0].TimcodeIn + lastest_clips[0].ClippingOut);
                                }
                                else
                                {
                                    timeline.SetCurrentPositionAsExternal(0);
                                }

                                #endregion END
                            }
                        }
                        break;
                    case Keys.PageUp:
                        {
                            if (!isTextEditorFocused)
                            {
                                #region PAGE-UP

                                Program.Logger.debug("[PRESS PAGEUP]");
                                handled = true;

                                if (timeline.InternalEngine.EngineStatus == MediaAlliance.Controls.MxpTimeLine.Engine.InternalEngineStatus.play)
                                {
                                    // SE LA TIMELINE E' IN PLAY FACCIO PAUSE
                                    timeline.InternalEngine.Pause();
                                }

                                timeline.MoveNextStep();

                                #endregion PAGE-UP
                            }
                        }
                        break;
                    case Keys.PageDown:
                        {
                            if (!isTextEditorFocused)
                            {
                                #region PAGE-DOWN

                                Program.Logger.debug("[PRESS PAGEDOWN]");
                                handled = true;

                                if (timeline.InternalEngine.EngineStatus == MediaAlliance.Controls.MxpTimeLine.Engine.InternalEngineStatus.play)
                                {
                                    // SE LA TIMELINE E' IN PLAY FACCIO PAUSE
                                    timeline.InternalEngine.Pause();
                                }

                                timeline.MovePreviousStep();

                                #endregion PAGE-DOWN
                            }
                        }
                        break;
                    case Keys.Space| Keys.Control:
                        {
                            if (!isTextEditorFocused)
                            {
                                Program.Logger.debug("[PRESS CTRL+SPACEBAR]");

                                #region CTRL + SPACEBAR

                                if (timeline.InternalEngine.EngineStatus == MediaAlliance.Controls.MxpTimeLine.Engine.InternalEngineStatus.play)
                                {
                                    timeline.InternalEngine.Pause();
                                }

                                timeline.Request_PlayMarked();

                                #endregion CTRL + SPACEBAR
                            }
                        }
                        break;
                    case Keys.Right | Keys.Shift:
                        {
                            if (!isTextEditorFocused)
                            {
                                #region RIGHT+SHIFT

                                Program.Logger.debug("[PRESS SHIFT+RIGHT]");
                                handled = true;

                                if (timeline.InternalEngine.EngineStatus == MediaAlliance.Controls.MxpTimeLine.Engine.InternalEngineStatus.play)
                                {
                                    timeline.InternalEngine.Pause();
                                }

                                timeline.SetCurrentPositionAsExternal(timeline.CurrentPosition + (int)timeline.FPS);

                                #endregion RIGHT+SHIFT
                            }
                        }
                        break;
                    case Keys.Right:
                        {
                            if (!isTextEditorFocused)
                            {
                                #region RIGHT

                                Program.Logger.debug("[PRESS RIGHT]");
                                handled = true;

                                if (timeline.InternalEngine.EngineStatus == MediaAlliance.Controls.MxpTimeLine.Engine.InternalEngineStatus.play)
                                {
                                    timeline.InternalEngine.Pause();
                                }

                                timeline.SetCurrentPositionAsExternal(timeline.CurrentPosition + 1);

                                #endregion RIGHT
                            }
                        }
                        break;
                    case Keys.Left | Keys.Shift:
                        {
                            if (!isTextEditorFocused)
                            {
                                #region SHIFT+LEFT

                                Program.Logger.debug("[PRESS SHIFT+LEFT]");
                                handled = true;

                                if (timeline.InternalEngine.EngineStatus == MediaAlliance.Controls.MxpTimeLine.Engine.InternalEngineStatus.play)
                                {
                                    timeline.InternalEngine.Pause();
                                }

                                timeline.SetCurrentPositionAsExternal(timeline.CurrentPosition - (int)timeline.FPS);

                                #endregion SHIFT+LEFT
                            }
                        }

                        break;
                    case Keys.Left:
                        {
                            if (!isTextEditorFocused)
                            {
                                #region LEFT

                                Program.Logger.debug("[PRESS LEFT]");
                                handled = true;

                                if (timeline.InternalEngine.EngineStatus == MediaAlliance.Controls.MxpTimeLine.Engine.InternalEngineStatus.play)
                                {
                                    timeline.InternalEngine.Pause();
                                }

                                timeline.SetCurrentPositionAsExternal(timeline.CurrentPosition - 1);

                                #endregion LEFT
                            }
                        }
                        break;
                    case Keys.C:
                        {
                            if (!isTextEditorFocused)
                            {
                                #region Key-C

                                Program.Logger.debug("[PRESS C]");
                                if (timeline == mxptl)
                                {
                                    handled = true;
                                    mxptl.CLIP_CUTTER();
                                }

                                #endregion Key-C
                            }
                        }
                        break;
                    case Keys.I:
                        {
                            if (!isTextEditorFocused)
                            {
                                #region Key-I

                                Program.Logger.debug("[PRESS I]");

                                handled = true;
                                timeline.MarkIn();

                                #endregion Key-I
                            }
                        }
                        break;
                    case Keys.O:
                        {
                            if (!isTextEditorFocused)
                            {
                                #region Key-O

                                Program.Logger.debug("[PRESS O]");

                                handled = true;
                                timeline.MarkOut();

                                #endregion Key-O
                            }
                        }
                        break;
                    case Keys.Escape:
                        {
                            if (!isTextEditorFocused)
                            {
                                #region Escape

                                Program.Logger.debug("[PRESS ESCAPE]");

                                handled = true;
                                timeline.HideMarks();

                                #endregion Escape
                            }
                            else
                            {
                                IntPtr focused_parent = GetParent(focused_control);
                                
                                if (focused_parent != IntPtr.Zero)
                                {
                                    SetFocus(focused_parent);
                                }
                            }
                        }
                        break;
                    case Keys.Up:
                        {
                            if (!isTextEditorFocused)
                            {
                                #region Key-Up

                                Program.Logger.debug("[PRESS UP]");

                                if (timeline == mxptl)
                                {
                                    CMxpTrack sel = timeline.Tracks.SelectedTrack;

                                    if (sel == null) sel = timeline.Tracks[0];

                                    if (sel != null && sel.ID > 0)
                                    {
                                        timeline.Tracks[sel.ID - 1].Selected = true;
                                    }
                                }

                                #endregion Key-Up
                            }
                        }
                        break;
                    case Keys.Down:
                        {
                            if (!isTextEditorFocused)
                            {
                                #region Key-Down

                                Program.Logger.debug("[PRESS DOWN]");

                                if (timeline == mxptl)
                                {
                                    CMxpTrack sel = timeline.Tracks.SelectedTrack;

                                    if (sel == null) sel = timeline.Tracks[0];

                                    if (sel != null && sel.ID < timeline.Tracks.Count - 1)
                                    {
                                        timeline.Tracks[sel.ID + 1].Selected = true;
                                    }
                                }

                                #endregion Key-Down
                            }
                        }
                        break;
                    case Keys.Back:
                        {
                            if (!isTextEditorFocused)
                            {
                                #region Key-Back

                                Program.Logger.debug("[PRESS BACKSPACE]");

                                if (timeline == mxptl)
                                {
                                    handled = true;
                                    CLIP_BLACKER();
                                }

                                #endregion Key-Back
                            }
                        }
                        break;

                    case Keys.Delete:
                        {
                            if (!isTextEditorFocused)
                            {
                                #region Key-Delete

                                Program.Logger.debug("[PRESS DELETE]");

                                if (timeline == mxptl)
                                {
                                    handled = true;
                                    CLIP_CUTTER();
                                }

                                #endregion Key-Delete
                            }
                        }
                        break;
                }
            }

            return handled;
        }

        public void CLIP_BLACKER()
        {
            CMxpTrack sel = mxptl.Tracks.SelectedTrack;

            if (sel == null)
            {
                MessageBox.Show(CDict.GetValue("MainForm.Mbox.SelectTrack", "Select a track where apply this command!"));
            }
            else if (!mxptl.IsMarked)
            {
                MessageBox.Show(CDict.GetValue("MainForm.Mbox.SelectClipPortion", "Select a clip portion before!"));
            }
            else
            {
                CMxpClip clip_to_cut = null;

                long ti = mxptl.CurrentMarkIn;
                long to = mxptl.CurrentMarkOut;

                foreach (CMxpClip clip in sel.Clips)
                {
                    if (clip.TimcodeIn + clip.ClippingIn <= ti && clip.TimcodeIn + clip.ClippingOut >= to)
                    {
                        clip_to_cut = clip;
                        break;
                    }
                }

                if (clip_to_cut != null)
                {
                    if (Program.Settings.GUI_Options.DEL_CONFIRM)
                    {
                        Program.Logger.info("Asking confirmation about Blanking command on clip " + clip_to_cut.ID);

                        
                        if (MessageBox.Show(null,
                                            CDict.GetValue("MainForm.Mbox.ConfirmBlank", "Are you sure to blank the selection?"),
                                            "Continue",
                                            MessageBoxButtons.YesNo,
                                            MessageBoxIcon.Question,
                                            MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                        {
                            Program.Logger.info("   > Operation aborted");
                            return;
                        }
                    }

                    Program.Logger.info("   > Operation confirmed");

                    Program.Logger.info("Blanking clip : " + clip_to_cut.ID);
                    Program.Logger.info("   > In      : " + ti);
                    Program.Logger.info("   > Out     : " + (to - clip_to_cut.TimcodeIn).ToString());

                    Program.Logger.info("   > File    : " + clip_to_cut.ClipInfo.filename);

                    try
                    {
                        mxptl.SaveUndo();

                        CMxpClip[] splice0 = clip_to_cut.Cut(ti);           
                        splice0[1].Set_ClippingIn(to - clip_to_cut.TimcodeIn, true);
                        splice0[1].Set_ClippingOut(splice0[1].ClippingOut, true);
                        splice0[1].NotifyRender();
                        foreach (CMxpClip clip in splice0[1].Group) clip.NotifyRender();

                        Program.Logger.info("   > Done");
                    }
                    catch (Exception ex)
                    {
                        Program.Logger.info("   > Blanking clip error");
                        Program.Logger.info(ex);
                    }

                    mxptl.HideMarks();
                }
                else
                {
                    string trackname = "\"" + mxptl.Tracks.SelectedTrack.Description + "\"" + " (" + mxptl.Tracks.SelectedTrack.Title + ")";
                    string message = CDict.GetValue("MainForm.Mbox.NoClipToApplyBlanker",
                                                    "There are no valid clips where apply commands, |RETURN|" +
                                                    "you need to select right Markin/Markout and click over the track of the clip you need to edit...|RETURN|" +
                                                    "now is selected |TRACKNAME| track.");

                    message = message.Replace("|TRACKNAME|", trackname);
                    message = message.Replace("|RETURN|", "\r\n");

                    MessageBox.Show(null, message,
                                    "BLANCKER COMMAND",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);
                }
            }
        }

        public void CLIP_CUTTER()
        {
            if (mxptl.IsMarked)
            {
                CMxpTrack sel = mxptl.Tracks.SelectedTrack;

                if (sel == null)
                {
                    MessageBox.Show(CDict.GetValue("MainForm.Mbox.SelectTrack", "Select a track where apply this command!"));
                }
                else
                {
                    CMxpClip clip_to_cut = null;

                    long ti = mxptl.CurrentMarkIn;
                    long to = mxptl.CurrentMarkOut;

                    foreach (CMxpClip clip in sel.Clips)
                    {
                        if (clip.TimcodeIn + clip.ClippingIn <= ti && clip.TimcodeIn + clip.ClippingOut >= to)
                        {
                            clip_to_cut = clip;
                            break;
                        }
                    }

                    if (clip_to_cut != null)
                    {
                        if (Program.Settings.GUI_Options.DEL_CONFIRM)
                        {
                            Program.Logger.info("Asking confirmation about Erase command on clip " + clip_to_cut.ID);

                            if (MessageBox.Show(null,
                                                CDict.GetValue("MainForm.Mbox.ConfirmErase", "Are you sure to erase the selection?"),
                                                "Continue",
                                                MessageBoxButtons.YesNo,
                                                MessageBoxIcon.Question,
                                                MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                            {
                                Program.Logger.info("   > Operation aborted");
                                return;
                            }
                        }

                        Program.Logger.info("   > Operation confirmed");

                        Program.Logger.info("Erasing clip : " + clip_to_cut.ID);
                        Program.Logger.info("   > In      : " + ti);
                        Program.Logger.info("   > Out     : " + (to - clip_to_cut.TimcodeIn).ToString());

                        try
                        {
                            mxptl.SaveUndo();

                            long space = mxptl.CurrentMarkOut - mxptl.CurrentMarkIn;

                            CMxpClip[] splice0 = clip_to_cut.Cut(to);
                            splice0[0].ClippingOut = ti - clip_to_cut.TimcodeIn;
                            splice0[1].Set_NewStartAt(ti + 1);

                            Program.Logger.info("   > Done");
                        }
                        catch (Exception ex)
                        {
                            Program.Logger.info("   > Cutting clip error");
                            Program.Logger.info(ex);
                        }

                        mxptl.HideMarks();
                    }
                    else
                    {
                        string trackname = "\"" + mxptl.Tracks.SelectedTrack.Description  + "\"" + " (" + mxptl.Tracks.SelectedTrack.Title + ")";
                        string message = CDict.GetValue("MainForm.Mbox.NoClipToApplyEraser",
                                                        "There are no valid clips where apply commands,  |RETURN|" +
                                                        "you need to select right Markin/Markout and click over the track of the clip you need to edit... |RETURN|" +
                                                        "now is selected |TRACKNAME| track.");

                        message = message.Replace("|TRACKNAME|", trackname);
                        message = message.Replace("|RETURN|", "\r\n");

                        MessageBox.Show(null, message,
                                        "ERASER COMMAND",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Exclamation);
                    }
                }
            }
            else if (mxptl.SelectedClip != null)
            {
                if (Program.Settings.GUI_Options.DEL_CONFIRM)
                {
                    Program.Logger.info("Asking confirmation about Remove clip " + mxptl.SelectedClip.ID);

                    if (MessageBox.Show(null,
                                        CDict.GetValue("MainForm.Mbox.ConfirmRemoveClip", "Are you sure to remove selected clip?"),
                                        "Are you sure to remove selected clip?",
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Question,
                                        MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                    {
                        Program.Logger.info("   > Operation aborted");
                        return;
                    }
                }


                Program.Logger.info("   > Operation confirmed");

                mxptl.SaveUndo();

                CMxpClip[] clips = mxptl.SelectedClip.GetGroup();

                foreach (CMxpClip clip in clips) clip.Delete();
            }
        }
    }
}
