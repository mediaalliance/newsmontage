﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

using System.IO;
using System.Xml;
using System.Reflection;
using Microsoft.Win32;

using MediaAlliance.Controls.MxpTimeLine;
using MediaAlliance.Controls.MxpTimeLine.Objects;
using MediaAlliance.AV;
using System.Windows.Forms;
using NewsMontange.LogHelper;
using NewsMontange.Language;
using MediaAlliance.AV.CensorTools;
using System.ComponentModel;




namespace NewsMontange.Helpers
{
    public class CProjectsHelper
    {
        // DELEGATES
        public delegate void Prj_OnCurrentProjectChanged(object Sender, CProjectInfo project);
        public delegate void Prj_OnAddedSource(object Sender, CMxpTL_ClipInfo clipinfo, bool isProjectLoading);
        public delegate void Prj_OnRemovingSource(object Sender, CMxpTL_ClipInfo clipinfo, ref bool Cancel);
        public delegate void Prj_OnSourceNotFound(object Sender, string filename);
        public delegate void Prj_OnStartLoading(object Sender, string OperationTitle);
        public delegate void Prj_OnFinishLoading(object Sender);
        public delegate void Prj_OnProgress(object Sender, string step_description, int perc);
        public delegate void Prj_OnUnloadedAll(object Sender);

        // EVENTS
        public event Prj_OnAddedSource OnAddedSource                        = null;
        public event Prj_OnRemovingSource OnRemovingSource                  = null;
        public event Prj_OnSourceNotFound OnSourceNotFound                  = null;
        public event Prj_OnProgress OnProgress                              = null;
        public event Prj_OnStartLoading OnStartLoading                      = null;
        public event Prj_OnFinishLoading OnFinishLoading                    = null;
        public event Prj_OnCurrentProjectChanged OnCurrentProjectChanged    = null;
        public event Prj_OnUnloadedAll OnUnloadedAll                        = null;


        // CLASSES 
        private CRollback_Collection rollback_manager   = new CRollback_Collection();
        private CProjectInfo currentProject             = null;
        private CSourceCheck m_sourceCheck              = null;
        

        public enum enSouceType
        { 
            video,
            voice,
            background
        }
        
        private MxpTmeLine timeline_control         = null;
        private VideoPreview video_preview          = null;
        private List<CMxpTL_ClipInfo> m_sourcesList = new List<CMxpTL_ClipInfo>();
        private string projectPath                  = "";
        private bool isLoading                      = false;



        public CProjectsHelper(string ProjectPath, MxpTmeLine timeline_control, VideoPreview video_preview)
        {
            currentProject          = new CProjectInfo(this);

            this.timeline_control   = timeline_control;
            this.video_preview      = video_preview;
            this.projectPath        = ProjectPath;

            m_sourceCheck = new CSourceCheck(new string[0], -1);


            if (File.Exists(Program.LanguageFile))
            {
                CDict.AddFromFile(Program.LanguageFile);
            }
        }



        public void New(CProjectInfo prj)
        {
            Program.Logger.info("Creating new project : " + prj.Filename);

            currentProject = prj;
            rollback_manager.CurrentProjectInfo = currentProject;
            currentProject.CheckAndCreateFolders();
        }


        public void Load(string prjfilename, bool SaveRollbackPoint)
        {
            Program.Logger.info("Loading project : " + prjfilename);

            if (OnStartLoading != null) OnStartLoading(this, CDict.GetValue("LoadingForm.LoadingProject", "Loading Project..."));

            XmlDocument doc = new XmlDocument();

            if (!File.Exists(prjfilename))
            {
                Program.Logger.error("    > File not found");
                throw new FileNotFoundException();
            }

            try
            {
                doc.Load(prjfilename);
            }
            catch { }

            if (doc["project"] != null)
            {
                if (OnProgress != null) OnProgress(this, "Parsing project file...", 6);

                currentProject                  = new CProjectInfo(this);
                currentProject.Filename         = prjfilename;
                currentProject.Folder_Project   = new FileInfo(prjfilename).DirectoryName;

                currentProject.CheckAndCreateFolders();

                rollback_manager.CurrentProjectInfo  = currentProject;

                if (OnProgress != null) OnProgress(this, "Backup project...", 10);

                

                rollback_manager.Load();

                this.isLoading = true;

                if (OnCurrentProjectChanged != null) OnCurrentProjectChanged(this, currentProject);

                
                #region LOAD

                if (doc["project"]["GenericInfo"] != null)
                {
                    XmlNode generic_node = doc["project"]["GenericInfo"];

                    currentProject.NewsNet_File = (generic_node["FromNewsnetFile"] != null) ? generic_node["FromNewsnetFile"].InnerText : "";                
                }

                if (doc["project"]["media"] != null)
                {
                    XmlNode media_node = doc["project"]["media"];

                    currentProject.Project_Title        = (media_node.Attributes["title"] != null) ? media_node.Attributes["title"].Value : "";
                    currentProject.Project_Show         = (media_node.Attributes["show"] != null) ? media_node.Attributes["show"].Value : "";
                    currentProject.Project_StoryTitle   = (media_node.Attributes["story_title"] != null) ? media_node.Attributes["story_title"].Value : "";
                    currentProject.Project_Author       = (media_node.Attributes["author"] != null) ? media_node.Attributes["author"].Value : "";

                    currentProject.Script = media_node.InnerText;
                }


                XmlNodeList CLIPNODES = doc.GetElementsByTagName("clip");

                int perc = 0;
                int curr = 0;

                foreach (XmlNode clipnode in CLIPNODES)
                {
                    perc = (curr + 1)  * 60 / CLIPNODES.Count;
                    if (OnProgress != null) OnProgress(this, "Loading footage...", perc +  10);

                    string filename     = clipnode.Attributes["file"].Value;
                    string clipid       = clipnode.Attributes["clipid"].Value;
                    string description  = (clipnode.Attributes["description"] != null) ? clipnode.Attributes["description"].Value : "";
                    
                    enSouceType type = enSouceType.video;

                    // VERIFICO IL TIPO DI SORGENTE RISPETTO IL PARENTNODE

                    if (clipnode.ParentNode.Name.Equals("clips"))
                    {
                        type = enSouceType.video;
                    }
                    else if (clipnode.ParentNode.Name.Equals("voices"))
                    {
                        type = enSouceType.voice;
                    }
                    else if (clipnode.ParentNode.Name.Equals("background"))
                    {
                        type = enSouceType.background;
                    }


                    int id = Int32.Parse(clipid);
                    id -= 1000;

                    // DECIDO QUANTE TRACCE DI SOURCE AGGIUNGERE RISPETTO IL TYPE
                    int tracksnumber = 1;
                    if (type == enSouceType.video) tracksnumber = 2;


                    // AGGIUNGO LA CLIP A RUNTIME E NELLE LISTE DI FILE USATI

                    CMxpTL_ClipInfo clip_info = AddSource(filename, description, tracksnumber, type, id);

                    curr++;
                }

                long zi = long.Parse(doc["project"]["video_montage"].Attributes["zoom_in"].Value);
                long zo = long.Parse(doc["project"]["video_montage"].Attributes["zoom_out"].Value);




                foreach (XmlNode tracknodes in doc["project"]["video_montage"].ChildNodes)
                {
                    // TRACCE AUDIO-VIDEO

                    if (tracknodes.Name.Equals("tracks_info"))
                    {
                        foreach (XmlNode trk_node in tracknodes.ChildNodes)
                        {
                            if (trk_node.Name.Equals("track") && trk_node.NodeType == XmlNodeType.Element)
                            {
                                int id = (trk_node.Attributes["id"] != null) ? int.Parse(trk_node.Attributes["id"].Value) : 0;
                                bool off = (trk_node.Attributes["off"] != null) ? bool.Parse(trk_node.Attributes["off"].Value) : false;

                                if(timeline_control.Tracks.Count > id)
                                {
                                    timeline_control.Tracks[id].Off = off;
                                }
                            }
                        }
                    }
                    else
                    {
                        int idtrack = 0;

                        enSouceType src_type = enSouceType.video;

                        if (tracknodes.Name.StartsWith("video_"))
                        {
                            idtrack = Int32.Parse(tracknodes.Name.Substring(tracknodes.Name.IndexOf("_") + 1));
                            if (idtrack == 2) idtrack++;
                            src_type = enSouceType.video;
                        }
                        else if (tracknodes.Name.StartsWith("voice"))
                        {
                            src_type = enSouceType.voice;
                        }
                        else if (tracknodes.Name.StartsWith("background"))
                        {
                            idtrack = 5;
                            src_type = enSouceType.background;
                        }

                        curr = 0;
                        perc = 70;

                        foreach (XmlNode clipnode in tracknodes.ChildNodes)
                        {
                            perc = (curr + 1) * 30 / tracknodes.ChildNodes.Count;
                            if (OnProgress != null) OnProgress(this, "Creating timeline...", perc + 70);

                            if (clipnode.NodeType == XmlNodeType.Element && clipnode.Name.StartsWith("videocliptl"))
                            {
                                Guid id                 = new Guid(clipnode.Attributes["id"].Value);
                                int sourceclipid        = Int32.Parse(clipnode.Attributes["sourceclipid"].Value);
                                long In                 = long.Parse(clipnode.Attributes["In"].Value);
                                long Out                = long.Parse(clipnode.Attributes["Out"].Value);
                                long startTimeline      = long.Parse(clipnode.Attributes["startTimeline"].Value);

                                List<CMxpTL_ClipInfo> src = m_sourcesList.Where(x => x.id_onprojectfile == sourceclipid - 1000).ToList();

                                if (src.Count > 0)
                                {
                                    CMxpTL_ClipInfo clip_info = src[0].Copy();

                                    long tc = startTimeline;
                                    long duration = clip_info.Tcout;
                                                                        
                                    clip_info.Tcin          = 0;
                                    clip_info.Tcout         = duration;
                                    clip_info.Clipping_in   = In;
                                    clip_info.Clipping_out  = Out;


                                    CMxpClip clipV = null;
                                    CMxpClip clipA = null;

                                    if (src_type == enSouceType.video)
                                    {
                                        clipV = timeline_control.Tracks[idtrack].AddClip("",
                                                                                            enCMxpClipType.Video,
                                                                                            clip_info.Copy(),
                                                                                            null,
                                                                                            tc - In,
                                                                                            tc - In  + duration,
                                                                                            In,
                                                                                            Out);

                                        clipA = timeline_control.Tracks[idtrack + 1].AddClip("",
                                                                                            enCMxpClipType.Audio,
                                                                                            clip_info.Copy(),
                                                                                            null,
                                                                                            tc - In,
                                                                                            tc - In + duration,
                                                                                            In,
                                                                                            Out);


                                        clipV.Group.Add(clipA);
                                    }
                                    else
                                    {
                                        clipA = timeline_control.Tracks[idtrack].AddClip("",
                                                                                            enCMxpClipType.Audio,
                                                                                            clip_info.Copy(),
                                                                                            null,
                                                                                            tc - In,
                                                                                            tc + duration,
                                                                                            In,
                                                                                            Out);

                                    }



                                    if (clipnode["volumes"] != null)
                                    {
                                        clipA.AudioInfo.Main0gain = Convert.ToSingle(clipnode["volumes"].Attributes["global"].Value, System.Globalization.CultureInfo.InvariantCulture);

                                        foreach (XmlNode audionode in clipnode["volumes"].ChildNodes)
                                        {
                                            if (audionode.NodeType == XmlNodeType.Element && audionode.Name.Equals("volumepoints"))
                                            {
                                                long time = long.Parse(audionode.Attributes["time"].Value);
                                                float volume = Convert.ToSingle(audionode.Attributes["volume"].Value, System.Globalization.CultureInfo.InvariantCulture);

                                                //time -= clipA.ClippingIn;

                                                clipA.AudioInfo.AddNode(time, volume);
                                            }
                                        }
                                    }
                                }
                            }

                            curr++;
                        }
                    }
                }

                timeline_control.ZoomOut    = zo;
                timeline_control.ZoomIn     = zi;
                timeline_control.Refresh();

                foreach (CMxpClip clip in timeline_control.Tracks.AllClips)
                {
                    clip.NotifyRender();
                }

                this.isLoading = false;

                #endregion LOAD


                if (SaveRollbackPoint)
                {
                    // FACCIO BACKUP DEL PRJ
                    rollback_manager.Addbackup("As opened at : " + DateTime.Now.ToString("HH.mm.ss dd/MM/yyyy"), enRollbackType.asOpened);
                }
            }

            MRUTools.Add(currentProject, 10);

            if (OnProgress != null) OnProgress(this, "Project successfully loaded", 100);

            if (OnFinishLoading != null) OnFinishLoading(this);
        }

        public void UnloadAll()
        {
            Program.Logger.info("Unloading current project");
            timeline_control.ClearAllClips();

            video_preview.UnloadAll();
            m_sourcesList.Clear();

            if (OnUnloadedAll != null) OnUnloadedAll(this);
        }
        
        public void Save()
        {

            Program.Logger.info("Saving current project");

            if (currentProject.Filename == "")
            {
                throw new Exception("Project never saved before!");
            }
            else
            {
                XmlDocument doc = new XmlDocument();

                doc.LoadXml(@"<?xml version=""1.0"" encoding=""UTF-8""?><project version=""1.0""></project>");

                XmlNode MAIN = doc["project"];

                WriteGenericInfoNode(MAIN);

                XmlNode MEDIANODE = WriteMediaNode(MAIN);

                XmlNode CLIPSNODE_AV = WriteClipNodes(MEDIANODE);

                XmlNode MONTAGENODE = WriteMontageNode(MAIN);

                //doc.Save(prjfilename);
                if (System.IO.File.Exists(currentProject.Filename))
                {
                    System.IO.File.Delete(currentProject.Filename);
                }

                XmlTextWriter writer = new XmlTextWriter(currentProject.Filename, null);
                writer.Formatting = Formatting.Indented;
                doc.Save(writer);
                writer.Close();

                MRUTools.Add(currentProject, 10);
            }
        }
                
        public void NewsNet_Import(string filename)
        {
            if (File.Exists(filename))
            {
                if (OnStartLoading != null) OnStartLoading(this, "Importing NewsNet Project...");

                currentProject.NewsNet_File = filename;

                XmlDocument doc = new XmlDocument();
                doc.Load(filename);

                if (doc["media"] != null)
                {
                    if (OnProgress != null) OnProgress(this, "Parsing NewsNet project...", 5);

                    #region STORY INFORMATION

                    XmlNode MAIN_NODE = doc["media"];

                    currentProject.Project_Title        = (MAIN_NODE.Attributes["title"] != null) ? MAIN_NODE.Attributes["title"].Value : "";
                    currentProject.Project_Show         = (MAIN_NODE.Attributes["show"] != null) ? MAIN_NODE.Attributes["show"].Value : "";
                    currentProject.Project_StoryTitle   = (MAIN_NODE.Attributes["story_title"] != null) ? MAIN_NODE.Attributes["story_title"].Value : "";
                    currentProject.Project_Author       = (MAIN_NODE.Attributes["author"] != null) ? MAIN_NODE.Attributes["author"].Value : "";
                    currentProject.Script               = MAIN_NODE.InnerText;

                    #endregion STORY INFORMATION


                    #region CLIP INFORMATION

                    if (MAIN_NODE["clips"] != null)
                    {
                        XmlNode CLIPS_NODE = MAIN_NODE["clips"];

                        long start_tc       = 0;
                        int current_track   = 1;
                        int prog            = 0;
                        int current         = 0;

                        foreach (XmlNode clip_node in CLIPS_NODE.ChildNodes)
                        {
                            prog = (int)((current + 1) * 95 / CLIPS_NODE.ChildNodes.Count);
                            if (OnProgress != null) OnProgress(this, "Importing footage...", 5 + prog);

                            if (clip_node.NodeType == XmlNodeType.Element && clip_node.Name.Equals("clip", StringComparison.OrdinalIgnoreCase))
                            {
                                if (clip_node["file"] != null)
                                {
                                    string source_file = clip_node["file"].InnerText;
                                    string title = (clip_node["title"] != null)? clip_node["title"].InnerText : "";

                                    if (File.Exists(source_file))
                                    {
                                        CMxpTL_ClipInfo src_clip_info = AddSource(source_file, title, 2, enSouceType.video, false);

                                        Guid id                 = Guid.NewGuid();
                                        long In                 = /*start_tc + */long.Parse(clip_node["in"].InnerText) / 40;
                                        long Out                = /*start_tc + */long.Parse(clip_node["out"].InnerText) / 40;
                                        long Duration           = Out - In;

                                        CMxpTL_ClipInfo clip_info = src_clip_info.Copy();

                                        clip_info.Tcin          = start_tc - In;
                                        clip_info.Tcout         = clip_info.Tcin + src_clip_info.Tcout;
                                        clip_info.Clipping_in   = In;
                                        clip_info.Clipping_out  = Out;

                                        start_tc += Duration;

                                        CMxpClip clipV = null;
                                        CMxpClip clipA = null;

                                        clipV = timeline_control.Tracks[current_track].AddClip("",
                                                                                            enCMxpClipType.Video,
                                                                                            clip_info.Copy(),
                                                                                            null,
                                                                                            clip_info.Tcin,
                                                                                            clip_info.Tcout,
                                                                                            In,
                                                                                            Out);

                                        clipA = timeline_control.Tracks[current_track + 1].AddClip("",
                                                                                            enCMxpClipType.Audio,
                                                                                            clip_info.Copy(),
                                                                                            null,
                                                                                            clip_info.Tcin,
                                                                                            clip_info.Tcout,
                                                                                            In,
                                                                                            Out);


                                        clipV.Group.Add(clipA);

                                        // ALTERNO LA TRACCE USATE
                                        current_track = (current_track == 1) ? 3 : 1;
                                    }
                                }
                            }

                            Application.DoEvents();


                            current++;
                        }
                    }

                    #endregion CLIP INFORMATION

                    timeline_control.ZoomAll();
                    timeline_control.Refresh();

                    foreach (CMxpClip clip in timeline_control.Tracks.AllClips)
                    {
                        clip.NotifyRender();
                    }

                }
                else
                {
                    throw new Exception("Invalid file!");
                }

                if (OnProgress != null) OnProgress(this, "Project successfully imported", 100);

                if (OnFinishLoading != null) OnFinishLoading(this);
            }
        }



        public void InitSourceChecker()
        {
            m_sourceCheck.Active = true;
            m_sourceCheck.FrameRate = Program.Settings.Project_Options.FRAMERATE;

            if (Program.Settings.Project_Options.FRAMERATE == 25F)
            {
                m_sourceCheck.VideoStandard.Add("PAL");
                m_sourceCheck.VideoStandard.Add("COMPONENT");
            }
            else if (Program.Settings.Project_Options.FRAMERATE == 29.97F)
            {
                m_sourceCheck.VideoStandard.Add("NTSC");
            }
        }

        public CMxpTL_ClipInfo AddSource(string filename, string description, int track_number, enSouceType type)
        {
            return AddSource(filename, description, track_number, type, -1, true);
        }

        public CMxpTL_ClipInfo AddSource(string filename, string description, int track_number, enSouceType type, bool saveRollbackStep)
        {
            return AddSource(filename, description, track_number, type, -1, saveRollbackStep);
        }

        public CMxpTL_ClipInfo AddSource(string filename, string description, int track_number, enSouceType type, int id_onprjfile)
        {
            return AddSource(filename, description, track_number, type, id_onprjfile, true);
        }

        public CMxpTL_ClipInfo AddSource(string filename, string description, int track_number, enSouceType type, int id_onprjfile, bool saveRollbackStep)
        {
            CMxpTL_ClipInfo clipinfo = new CMxpTL_ClipInfo();


            Program.Logger.info(" Check source file before import on project");

            CSourceCheckResult result = m_sourceCheck.Check(filename);

            if (result.Allowed)
            {
                if (saveRollbackStep)
                {
                    rollback_manager.Addbackup("Before adding resource : " + Path.GetFileName(filename), enRollbackType.beforeAddResource);
                }

                CMxpTL_ClipInfo just_present = null;

                foreach (CMxpTL_ClipInfo cInfo in m_sourcesList)
                {
                    if (cInfo.filename.Equals(filename, StringComparison.OrdinalIgnoreCase))
                    {
                        just_present = cInfo;
                        break;
                    }
                }

                if (just_present == null)
                {
                    int idsource = -1;

                    try
                    {
                        if (!File.Exists(filename))
                        {
                            currentProject.ClipsNotFound.Add(filename);
                            if (OnSourceNotFound != null) OnSourceNotFound(this, filename);
                        }
                        else
                        {
                            idsource = video_preview.Add(filename, track_number, clipinfo);
                        }
                    }
                    catch (Exception ex)
                    {
                        Program.Logger.error("Error loading source : " + filename);
                        Program.Logger.error(ex);
                    }

                    if (idsource >= 0)
                    {
                        if (!File.Exists(filename))
                        {
                            if (OnSourceNotFound != null) OnSourceNotFound(this, filename);
                        }
                        else
                        {
                            //clipinfo = new CMxpTL_ClipInfo();

                            clipinfo.filename           = filename;
                            clipinfo.file_id            = idsource;
                            clipinfo.Tcin               = 0;
                            clipinfo.Tcout              = 0;
                            clipinfo.Tag                = type;
                            clipinfo.description        = description;
                            clipinfo.id_onprojectfile   = id_onprjfile;

                            if (File.Exists(filename + ".censor"))
                            {
                                CCensorTimeline ctl = new CCensorTimeline();
                                ctl.LoadFile(filename + ".censor");
                                clipinfo.CensorInfo = ctl;
                            }
                            

                            // SCRIVO NEL LOG LE INFORMAZIONI SULLA CLIP

                            MediaAlliance.AV.MediaInfo.MediaInformations mi = new MediaAlliance.AV.MediaInfo.MediaInformations();
                            mi.Open(filename);

                            CLogHelper.WriteClipInfo(Program.Logger, mi);

                            // RECUPERO LA DURATA DEL FILE

                            if (mi.VideoCount > 0)
                            {
                                string dStr = mi.GetValue(MediaAlliance.AV.MediaInfo.VideoInfoRequest.Duration, 0);
                                clipinfo.Tcout = Convert.ToInt64(dStr) / 40;
                                clipinfo.Clipping_out = clipinfo.Tcout;
                                clipinfo.ClipAttributes.Add("ScanOrder", mi.GetValue(MediaAlliance.AV.MediaInfo.VideoInfoRequest.ScanOrder, 0));
                            }
                            else
                            {
                                string dStr = mi.GetValue(MediaAlliance.AV.MediaInfo.GeneralInfoRequest.Duration);
                                clipinfo.Tcout = Convert.ToInt64(dStr) / 40;
                                clipinfo.Clipping_out = clipinfo.Tcout;
                            }

                            mi.Dispose();

                            // 


                            m_sourcesList.Add(clipinfo);

                            //if (OnAddedSource != null) OnAddedSource(this, clipinfo, this.isLoading);
                            Raise_OnAddedSource(clipinfo, this.isLoading);

                            Program.Logger.info("   > Source added successfully");
                        }
                    }

                }
                else
                {
                    Program.Logger.info("   > Clip already present");
                    clipinfo = just_present;
                }
            }
            else
            {
                string msg = "This clip could not be used in this project :";

                foreach (string suberror in result.ErrorMessages)
                {
                    msg += "\r\n" + suberror;
                }

                MessageBox.Show(msg);
            }

            return clipinfo;
        }

        public void RemoveSource(string filename)
        {
            CMxpTL_ClipInfo toRemove = null;

            foreach (CMxpTL_ClipInfo info in m_sourcesList)
            {
                if (info.filename.Equals(filename, StringComparison.OrdinalIgnoreCase))
                {
                    toRemove = info;
                    break;
                }
            }

            if (toRemove != null)
            {
                bool cancel = false;
                if (OnRemovingSource != null) OnRemovingSource(this, toRemove, ref cancel);

                if (!cancel)
                {
                    m_sourcesList.Remove(toRemove);
                }
            }
        }



        public string ProjectPath
        {
            get { return projectPath; }
            set
            {
                projectPath = value;
            }
        }

        public CProjectInfo CurrentProject
        {
            get { return currentProject; }
        }

        public CRollback_Collection RollbackManager
        {
            get { return rollback_manager; }
        }

        public bool IsLoading
        {
            get { return isLoading; }
            set 
            {
                isLoading = value;
            }
        }


        private XmlNode AddSubnode(XmlNode parent, string name, string innertext)
        {
            XmlNode nn = parent.OwnerDocument.CreateNode(XmlNodeType.Element, name, "");

            if (innertext.Trim() != "")
            {
                nn.InnerText = innertext;
            }

            parent.AppendChild(nn);

            return nn;
        }


        #region PUBLIC PROPERTIES

        public List<CMxpTL_ClipInfo> SourcesList
        {
            get { return m_sourcesList; }
        }

        #endregion PUBLIC PROPERTIES



        #region WRITE METHODS

        private XmlNode WriteGenericInfoNode(XmlNode main_node)
        {
            XmlNode geninfo_node = AddSubnode(main_node, "GenericInfo", "");

            Assembly assem  = Assembly.GetEntryAssembly();
            AssemblyName assemName = assem.GetName();
            Version ver = assemName.Version;

            AddSubnode(geninfo_node, "ApplicationVersion", ver.ToString());
            AddSubnode(geninfo_node, "TLControlVersion", timeline_control.GetType().Assembly.GetName().Version.ToString());
            AddSubnode(geninfo_node, "OSVersion", Environment.OSVersion.VersionString);
            AddSubnode(geninfo_node, "MachineName", Environment.MachineName);
            AddSubnode(geninfo_node, "UserName", Environment.UserName);
            AddSubnode(geninfo_node, "Domain", Environment.UserDomainName);
            AddSubnode(geninfo_node, "FromNewsnetFile", currentProject.NewsNet_File); 

            main_node.AppendChild(geninfo_node);

            return geninfo_node;
        }

        private XmlNode WriteMediaNode(XmlNode main_node)
        {
            XmlNode media_node = main_node.OwnerDocument.CreateNode(XmlNodeType.Element, "media", "");

            media_node.Attributes.Append(media_node.OwnerDocument.CreateAttribute("id"));
            media_node.Attributes.Append(media_node.OwnerDocument.CreateAttribute("video_code"));
            media_node.Attributes.Append(media_node.OwnerDocument.CreateAttribute("title"));
            media_node.Attributes.Append(media_node.OwnerDocument.CreateAttribute("show"));
            media_node.Attributes.Append(media_node.OwnerDocument.CreateAttribute("datetime"));
            media_node.Attributes.Append(media_node.OwnerDocument.CreateAttribute("story_title"));
            media_node.Attributes.Append(media_node.OwnerDocument.CreateAttribute("author"));
            media_node.Attributes.Append(media_node.OwnerDocument.CreateAttribute("status"));

            media_node.Attributes["id"].Value           = new FileInfo(currentProject.Filename).Directory.Name;
            media_node.Attributes["video_code"].Value   = new FileInfo(currentProject.Filename).Directory.Name;
            media_node.Attributes["title"].Value        = currentProject.Project_Title;
            media_node.Attributes["show"].Value         = currentProject.Project_Show;
            media_node.Attributes["datetime"].Value     = new FileInfo(currentProject.Filename).Directory.Name;
            media_node.Attributes["author"].Value       = currentProject.Project_Author;
            media_node.Attributes["story_title"].Value  = currentProject.Project_StoryTitle;
            media_node.Attributes["status"].Value       = "0";

            media_node.InnerText = currentProject.Script;

            main_node.AppendChild(media_node);

            return media_node;
        }

        private XmlNode WriteClipNodes(XmlNode main_node)
        {
            XmlNode clip_nodes = null;

            AddSubnode(main_node, "clips", "");
            AddSubnode(main_node, "voices", "");
            AddSubnode(main_node, "background", "");
        
            foreach (CMxpTL_ClipInfo cInfo in m_sourcesList)
            {
                int used_counter = 0;

                List<object> clips_tags = new List<object>();

                switch ((enSouceType)cInfo.Tag)
                {
                    case enSouceType.video:
                        {
                            #region AUDIO/VIDEO CLIPS

                            CMxpTrack[] trkToCheck = new CMxpTrack[] { timeline_control.Tracks[1], timeline_control.Tracks[3] };
                            
                            foreach (CMxpTrack trk in trkToCheck)
                            {
                                foreach (CMxpClip clip in trk.Clips)
                                {
                                    // SE QUESTA SOURCE NON ERA MAI STATA AGGIUNTA
                                    
                                    if (clip.ClipInfo.filename.Equals(cInfo.filename, StringComparison.OrdinalIgnoreCase))
                                    {
                                        used_counter++;
                                    }
                                }
                            }

                            #endregion AUDIO/VIDEO CLIPS
                        }
                        break;
                    case enSouceType.voice:
                        {
                            #region VOICE CLIPS

                            clip_nodes = main_node["voices"];
                            
                            CMxpTrack[] trkToCheck = new CMxpTrack[] { timeline_control.Tracks[0] };
                            
                            foreach (CMxpTrack trk in trkToCheck)
                            {
                                foreach (CMxpClip clip in trk.Clips)
                                {
                                    // SE QUESTA SOURCE NON ERA MAI STATA AGGIUNTA

                                    CMxpTL_ClipInfo clipinfo = clip.ClipInfo;

                                    if (clipinfo.filename.Equals(cInfo.filename, StringComparison.OrdinalIgnoreCase))
                                    {
                                        used_counter++;
                                    }
                                }
                            }

                            #endregion VOICE CLIPS
                        }
                        break;
                    case enSouceType.background:
                        {
                            #region BACKGROUND AUDIO

                            clip_nodes = main_node["background"];

                            CMxpTrack[] trkToCheck = new CMxpTrack[] { timeline_control.Tracks[5] };

                            foreach (CMxpTrack trk in trkToCheck)
                            {
                                foreach (CMxpClip clip in trk.Clips)
                                {
                                    // SE QUESTA SOURCE NON ERA MAI STATA AGGIUNTA

                                    CMxpTL_ClipInfo clipinfo = clip.ClipInfo;

                                    if (clipinfo.filename.Equals(cInfo.filename, StringComparison.OrdinalIgnoreCase))
                                    {
                                        used_counter++;
                                    }
                                }
                            }

                            #endregion BACKGROUND AUDIO
                        }
                        break;
                    default:
                        break;
                }

                if (cInfo != null && cInfo.filename.Trim() != "")
                {
                    XmlNode clip_node = main_node.OwnerDocument.CreateNode(XmlNodeType.Element, "clip", "");

                    clip_node.Attributes.Append(clip_node.OwnerDocument.CreateAttribute("assetid"));
                    clip_node.Attributes.Append(clip_node.OwnerDocument.CreateAttribute("clipid"));
                    clip_node.Attributes.Append(clip_node.OwnerDocument.CreateAttribute("title"));
                    clip_node.Attributes.Append(clip_node.OwnerDocument.CreateAttribute("full"));
                    clip_node.Attributes.Append(clip_node.OwnerDocument.CreateAttribute("file"));
                    clip_node.Attributes.Append(clip_node.OwnerDocument.CreateAttribute("name"));
                    clip_node.Attributes.Append(clip_node.OwnerDocument.CreateAttribute("n_tracks"));
                    clip_node.Attributes.Append(clip_node.OwnerDocument.CreateAttribute("in"));
                    clip_node.Attributes.Append(clip_node.OwnerDocument.CreateAttribute("out"));
                    clip_node.Attributes.Append(clip_node.OwnerDocument.CreateAttribute("totaldur"));
                    clip_node.Attributes.Append(clip_node.OwnerDocument.CreateAttribute("clip_error"));
                    clip_node.Attributes.Append(clip_node.OwnerDocument.CreateAttribute("description"));
                    clip_node.Attributes.Append(clip_node.OwnerDocument.CreateAttribute("used"));

                    clip_node.Attributes["assetid"].Value       = "-";
                    clip_node.Attributes["clipid"].Value        = "" + (cInfo.file_id + 1000);
                    clip_node.Attributes["title"].Value         = System.IO.Path.GetFileName(cInfo.filename);
                    clip_node.Attributes["full"].Value          = "0";
                    clip_node.Attributes["file"].Value          = cInfo.filename;
                    clip_node.Attributes["name"].Value          = "";
                    clip_node.Attributes["n_tracks"].Value      = "0";
                    clip_node.Attributes["in"].Value            = "0";
                    clip_node.Attributes["out"].Value           = cInfo.Tcout.ToString();
                    clip_node.Attributes["totaldur"].Value      = cInfo.Tcout.ToString();
                    clip_node.Attributes["clip_error"].Value    = "";
                    clip_node.Attributes["description"].Value   = cInfo.description;
                    clip_node.Attributes["used"].Value          = used_counter.ToString();

                    switch ((enSouceType)cInfo.Tag)
                    {
                        case enSouceType.video:
                            clip_nodes = main_node["clips"];
                            break;
                        case enSouceType.voice:
                            clip_nodes = main_node["voices"];
                            break;
                        case enSouceType.background:
                            clip_nodes = main_node["background"];
                            break;
                        default:
                            break;
                    }

                    clip_nodes.AppendChild(clip_node);
                    main_node.AppendChild(clip_nodes);
                }
            }
            return clip_nodes;
        }

        private XmlNode WriteMontageNode(XmlNode main_node)
        {
            XmlNode montage_node = main_node.OwnerDocument.CreateNode(XmlNodeType.Element, "video_montage", "");

            montage_node.Attributes.Append(main_node.OwnerDocument.CreateAttribute("zoom_in"));
            montage_node.Attributes.Append(main_node.OwnerDocument.CreateAttribute("zoom_out"));

            montage_node.Attributes["zoom_in"].Value    = timeline_control.ZoomIn.ToString();
            montage_node.Attributes["zoom_out"].Value   = timeline_control.ZoomOut.ToString();

            // ===================================================================

            XmlNode tracks_info_node = main_node.OwnerDocument.CreateNode(XmlNodeType.Element, "tracks_info", "");

            foreach (CMxpTrack trk in timeline_control.Tracks.Tracks)
            {
                XmlNode trk_node = main_node.OwnerDocument.CreateNode(XmlNodeType.Element, "track", "");
                trk_node.Attributes.Append(main_node.OwnerDocument.CreateAttribute("title")).Value = trk.Title;
                trk_node.Attributes.Append(main_node.OwnerDocument.CreateAttribute("id")).Value = trk.ID.ToString();
                trk_node.Attributes.Append(main_node.OwnerDocument.CreateAttribute("off")).Value = trk.Off.ToString();

                tracks_info_node.AppendChild(trk_node);
            }

            montage_node.AppendChild(tracks_info_node);

            // ===================================================================

            XmlNode track_voice_node = main_node.OwnerDocument.CreateNode(XmlNodeType.Element, "voice", "");

            foreach (CMxpClip clip in timeline_control.Tracks[0].Clips)
            {
                WriteClipNode(track_voice_node, clip);
            }

            montage_node.AppendChild(track_voice_node);

            // ===================================================================

            XmlNode track_video1_node = main_node.OwnerDocument.CreateNode(XmlNodeType.Element, "video_1", "");

            foreach (CMxpClip clip in timeline_control.Tracks[1].Clips)
            {
                WriteClipNode(track_video1_node, clip);
            }

            montage_node.AppendChild(track_video1_node);

            // ===================================================================

            XmlNode track_video2_node = main_node.OwnerDocument.CreateNode(XmlNodeType.Element, "video_2", "");

            foreach (CMxpClip clip in timeline_control.Tracks[3].Clips)
            {
                WriteClipNode(track_video2_node, clip);
            }

            montage_node.AppendChild(track_video2_node);

            // ===================================================================

            XmlNode track_background_node = main_node.OwnerDocument.CreateNode(XmlNodeType.Element, "background", "");

            foreach (CMxpClip clip in timeline_control.Tracks[5].Clips)
            {
                WriteClipNode(track_background_node, clip);
            }

            montage_node.AppendChild(track_background_node);

            // ===================================================================

            main_node.AppendChild(montage_node);

            return montage_node;
        }

        private XmlNode WriteTrackNode(XmlNode main_node, CMxpTrack track, string name_suffix)
        {
            XmlNode track_node = main_node.OwnerDocument.CreateNode(XmlNodeType.Element, name_suffix + "_" + track.ID, "");

            main_node.AppendChild(track_node);

            return track_node;
        }
        
        private XmlNode WriteClipNode(XmlNode main_node, CMxpClip clip)
        {
            XmlNode clip_node = main_node.OwnerDocument.CreateNode(XmlNodeType.Element, "videocliptl", "");
            
            clip_node.Attributes.Append(main_node.OwnerDocument.CreateAttribute("id"));
            clip_node.Attributes.Append(main_node.OwnerDocument.CreateAttribute("sourceclipid"));
            clip_node.Attributes.Append(main_node.OwnerDocument.CreateAttribute("In"));
            clip_node.Attributes.Append(main_node.OwnerDocument.CreateAttribute("Out"));
            clip_node.Attributes.Append(main_node.OwnerDocument.CreateAttribute("startTimeline"));

            CMxpTL_ClipInfo clipinfo = clip.ClipInfo;
        
            clip_node.Attributes["id"].Value            = clip.ID.ToString();
            clip_node.Attributes["sourceclipid"].Value  = (clipinfo.file_id + 1000).ToString();
            clip_node.Attributes["In"].Value            = clip.ClippingIn.ToString();
            clip_node.Attributes["Out"].Value           = clip.ClippingOut.ToString() ;
            clip_node.Attributes["startTimeline"].Value = (clip.ClippingIn + clip.TimcodeIn).ToString();

            CMxpClip audio_clip = (clip.Type == enCMxpClipType.Audio) ? clip : null;

            if (audio_clip == null && clip.Group.Count > 0)
            {
                if (clip.Group[0].Type == enCMxpClipType.Audio) audio_clip = clip.Group[0];
            }

            if (audio_clip != null)
            {
                XmlNode volumes_node = main_node.OwnerDocument.CreateNode(XmlNodeType.Element, "volumes", "");

                volumes_node.Attributes.Append(main_node.OwnerDocument.CreateAttribute("global"));
                volumes_node.Attributes["global"].Value = audio_clip.AudioInfo.Main0gain.ToString().Replace(",", ".");

                foreach (CAudioNodeInfo ninfo in audio_clip.AudioInfo.GetAllNodes())
                {
                    XmlNode volumepoint_node = main_node.OwnerDocument.CreateNode(XmlNodeType.Element, "volumepoints", "");

                    volumepoint_node.Attributes.Append(main_node.OwnerDocument.CreateAttribute("time"));
                    volumepoint_node.Attributes.Append(main_node.OwnerDocument.CreateAttribute("volume"));

                    volumepoint_node.Attributes["time"].Value = ninfo.timecode.ToString();
                    volumepoint_node.Attributes["volume"].Value = ninfo.value.ToString().Replace(",", "."); ;
                        
                    volumes_node.AppendChild(volumepoint_node);
                }

                clip_node.AppendChild(volumes_node);
            }


            main_node.AppendChild(clip_node);

            return clip_node;
        }

        #endregion WRITE METHODS



        #region RAISE EVENTS

        private void Raise_OnAddedSource(CMxpTL_ClipInfo clipinfo, bool isProjectLoading)
        {
            if (OnAddedSource != null)
            {
                foreach (Prj_OnAddedSource singleCast in OnAddedSource.GetInvocationList())
                {
                    ISynchronizeInvoke syncInvoke = singleCast.Target as ISynchronizeInvoke;
                    try
                    {
                        if (syncInvoke != null && syncInvoke.InvokeRequired)
                        {
                            // Invokie the event on the main thread
                            syncInvoke.Invoke(OnAddedSource, new object[] { this, clipinfo, isProjectLoading });
                        }
                        else
                        {
                            // Raise the event
                            singleCast(this, clipinfo, isProjectLoading);
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }

        #endregion RAISE EVENTS
    }
}
