﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace NewsMontange
{
    public class CNM_Tools
    {

        public static string Frame2TC(long frame, float fps)
        {
            string result   = "";
            long ffTot      = frame;

            int ss      = (int)(frame / fps);
            int ff      = (int)(frame - (ss * fps));
            int mm      = (int)(ss / 60);
            ss          = ss - mm * 60;
            int hh      = (int)(mm / 60);
            mm          = mm - hh * 60;

            result += hh.ToString().PadLeft(2, '0') + ":";
            result += mm.ToString().PadLeft(2, '0') + ":";
            result += ss.ToString().PadLeft(2, '0') + ":";
            result += ff.ToString().PadLeft(2, '0');

            return result;
        }

        public static Color GetDarkenColor(Color source, int factor)
        {
            int R = source.R;
            int G = source.G;
            int B = source.B;

            R -= factor;
            G -= factor;
            B -= factor;

            if (R < 0) R = 0;
            if (G < 0) G = 0;
            if (B < 0) B = 0;

            return Color.FromArgb(R, G, B);
        }

        public static Color GetLightenColor(Color source, int factor)
        {
            int R = source.R;
            int G = source.G;
            int B = source.B;

            R += factor;
            G += factor;
            B += factor;

            if (R > 255) R = 255;
            if (G > 255) G = 255;
            if (B > 255) B = 255;

            return Color.FromArgb(R, G, B);
        }
    }
}
