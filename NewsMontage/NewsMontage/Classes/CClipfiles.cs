﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MediaAlliance.AV;

namespace TestMxpTimeline.ClipFiles
{
    public class CClipfiles
    {
        private string filename     = "";
        private long length         = 0;
        private long tcin           = 0;
        private long tcout          = 0;
        private bool isSubclip      = false;
        private CClipfiles parent   = null;

        public int idPreview        = 0;

        public MediaAlliance.Controls.CAdvList<CClipfiles> Subclip = new MediaAlliance.Controls.CAdvList<CClipfiles>();

        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public CClipfiles()
        {

        }

        public CClipfiles(string filename)
        {
            MediaAlliance.AV.MediaInfo.MediaInformations md = new MediaAlliance.AV.MediaInfo.MediaInformations();
            md.Open(filename);
            string duration_str = md.GetValue(MediaAlliance.AV.MediaInfo.GeneralInfoRequest.Duration);

            long duration = long.Parse(duration_str);

            this.tcout = duration / 40;

            md.Dispose();

            this.filename = filename;

            Subclip.OnItemAdded += new EventHandler<MediaAlliance.Controls.GenericItemEventArgs<CClipfiles>>(Subclip_OnItemAdded);
        }

        void Subclip_OnItemAdded(object sender, MediaAlliance.Controls.GenericItemEventArgs<CClipfiles> e)
        {
            e.Item.IsSubclip    = true;
            e.Item.Parent       = this;
        }



        public void AddSubclip(long tcin, long tcout)
        {
            CClipfiles sclip    = new CClipfiles();
            sclip.filename      = filename;
            sclip.TimecodeIn    = tcin;
            sclip.tcout         = tcout;

            this.Subclip.Add(sclip);
        }


        #region PUBLIC PROPERTIES

        public long TimecodeIn
        {
            get { return tcin; }
            set
            {
                if (tcin != value)
                {
                    tcin = value;
                }
            }
        }

        public long TimecodeOut
        {
            get { return tcout; }
            set
            {
                if (tcout != value)
                {
                    tcout = value;
                }
            }
        }

        public string Filename
        {
            get { return filename; }
            set
            {
                if (filename != value)
                {
                    filename = value;
                }
            }
        }

        public bool IsSubclip
        {
            get { return isSubclip; }
            set 
            {
                isSubclip = value;
            }
        }

        public CClipfiles Parent
        {
            get { return parent; }
            set
            {
                if (parent != value)
                {
                    parent = value;
                }
            }
        }

        #endregion PUBLIC PROPERTIES
    }
}
