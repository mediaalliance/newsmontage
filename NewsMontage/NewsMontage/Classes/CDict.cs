using System;
using System.Collections;
using System.IO;
using System.Xml;
using System.Diagnostics;

namespace NewsMontange.Language
{

	/// <summary>
	/// Summary description for StringDictionary.
	/// </summary>
	internal class StringDictionary : DictionaryBase
	{
		public String this[String key]
		{
			get
			{
				return ((String) Dictionary[key]);
			}
			set
			{
				Dictionary[key] = value;
			}
		}

		public ICollection Keys
		{
			get
			{
				return (Dictionary.Keys);
			}
		}

		public ICollection Values
		{
			get
			{
				return (Dictionary.Values);
			}
		}

		public void Add(String key, String value)
		{
			Dictionary.Add(key, value);
		}

		public bool Contains(String key)
		{
			return (Dictionary.Contains(key));
		}

		public void Remove(String key)
		{
			Dictionary.Remove(key);
		}
	}


	/* classe base che implementa un dizionario di stringhe
	 * */


	/// <summary>
	/// Classe per la memorizzazione di stringhe
	/// prelevate da un file config in standard xml - app.config.
	/// Consente il caricamento ripetuto di elementi sostituendo 
	/// il valore degli elementi con la chiave gi� presente
	/// </summary>
	public class CDict
	{
		private static StringDictionary dict = new StringDictionary();
		private static string fileName = "";
		
		public static void Clear()  
		{
			dict.Clear();
		}

		public static int AddFromFile(string fileName)
		{
			int	errors = 0;
			CDict.fileName = fileName;

			try 
			{
				XmlTextReader reader = new XmlTextReader(fileName);
				reader.WhitespaceHandling = WhitespaceHandling.None;
				string skey = "";
				string svalue = "";
				try 
				{
					while ( reader.Read() ) 
					{
						//legge sia Element che EndElement, perci� serve node type
						if (reader.NodeType == XmlNodeType.Element &&
							reader.Name == "add") 
						{
							skey = reader.GetAttribute("key");
							if (skey != null && skey.Length > 0)
							{
								svalue = reader.GetAttribute("value");
								if (svalue != null && svalue.Length > 0)
								{
									if (dict.Contains(skey))
									{
										Debug.WriteLine("replacing: " + skey + " = " + svalue);
										dict[skey] = svalue;
									}
									else
									{
										//Debug.WriteLine("adding: " + skey + " = " + svalue);
										dict.Add(skey, svalue);
									}
								}
							}
						}
					}
					errors = 0;
				}
				catch(Exception ex1)
				{
					errors = 2;
					Trace.WriteLine(
						"Exception reading [" 
						+ fileName 
						+ "]: " 
						+ ex1.ToString());
				}
				reader.Close();
			}
			catch (Exception ex)
			{
				errors = 1;
				Trace.WriteLine(
					"Exception accessing [" 
					+ fileName 
					+ "]: " 
					+ ex.ToString());
			}
			return errors;
		}

		public static string GetValue(string key)
		{
			if (key.Length>0 && dict.Contains(key))
				return dict[key];
			else
				return "";
		}

		public static string GetValue(string key, string defaultValue)
		{
			if (key.Length>0 && dict.Contains(key))
				return dict[key];
			else
				return defaultValue;
		}

		// Uses the enumerator. 
		public static void PrintKeysAndValues()  
		{
			DictionaryEntry myDE;
			System.Collections.IEnumerator myEnumerator = dict.GetEnumerator();
			while ( myEnumerator.MoveNext() )
				if ( myEnumerator.Current != null )  
				{
					myDE = (DictionaryEntry) myEnumerator.Current;
					Debug.WriteLine( "[" + myDE.Key.ToString() 
						+ "]:[" + myDE.Value.ToString() + "]");
				}
			Debug.WriteLine("");
		}

	}


}
