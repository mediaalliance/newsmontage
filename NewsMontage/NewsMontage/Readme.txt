﻿================================================
Media Alliance NewsMontage 1.0.2.13  2011-12-19
================================================

[Install Instruction]
- Replace NewsMontage.exe and also Dlls and Configuration files listed below in the application folder. 

[What's new] 
- Improved : Improved play on timeline od Audio Tracks

[Modules updated]
MAVideoPreview.dll                              Ver:[1.0.0.11      ]
FFMpegHelper.dll                                Ver:[1.1.0.5       ]
MediaAlliance.Controls.MxpTimeLine.dll			Ver:[1.0.1.12      ]
MediaAlliance.AV.dll							Ver:[1.0.0.6       ]

================================================
Media Alliance NewsMontage 1.0.2.12  2011-12-10
================================================

[Install Instruction]
- Replace NewsMontage.exe and also Dlls and Configuration files listed below in the application folder. 

[What's new] 
- Fixed	 : Thumb generation over clip representation on Timeline
- Fixed  : ThumbFrame on Timeline clips now Draw FirstFrame over Last, whene clip is too small

[Modules updated]
MediaAlliance.Controls.MxpTimeLine.dll			Ver:[1.0.1.11   ]


================================================
Media Alliance NewsMontage 1.0.2.10  2011-12-08
================================================

[Install Instruction]
- Replace NewsMontage.exe and also Dlls and Configuration files listed below in the application folder. 

[What's new] 
- Fixed	 : Improved play clip BackToBack on timeline

[Modules updated]
MAVideoPreview.dll                              Ver:[1.0.0.9       ]



================================================
Media Alliance NewsMontage 1.0.2.9  2011-11-17
================================================

[Install Instruction]
- Replace NewsMontage.exe and also Dlls and Configuration files listed below in the application folder. 

[What's new] 
- Added	 : Added datetime on automatic file name for audio export
- Fixed	 : When a clip is dragged over left Timeline limit (that is not TC 0), clip is moved at start of timeline-view automatically

[Modules updated]
MediaAlliance.Controls.MxpTimeLine.dll			Ver:[1.0.1.10   ]


================================================
Media Alliance NewsMontage 1.0.2.8  2011-11-04
================================================

[Install Instruction]
- Replace NewsMontage.exe and also Dlls and Configuration files listed below in the application folder. 

[What's new] 
- Fixed	 : Some drawing issues on timeline clips

[Modules updated]
MediaAlliance.Controls.MxpTimeLine.dll			Ver:[1.0.1.9	   ]



================================================
Media Alliance NewsMontage 1.0.2.7  2011-10-31
================================================

[Install Instruction]
- Replace NewsMontage.exe and also Dlls and Configuration files listed below in the application folder. 

[What's new] 
- Fixed	 : During clips re-trimming you clould cross over other existing clips
- Added  : Copy to on Items menu of VoiceOver Gallery
- Added  : Suggested name during exporting audio tracks

[Modules updated]
MediaAlliance.Controls.MxpTimeLine.dll			Ver:[1.0.1.8	   ]


================================================
Media Alliance NewsMontage 1.0.2.6  2011-10-28
================================================

[Install Instruction]
- Replace NewsMontage.exe and also Dlls and Configuration files listed below in the application folder. 

[What's new] 
- Fixed	 : When main form is Maximized or Minimized, timeline loose the real cursor position

[Modules updated]


================================================
Media Alliance NewsMontage 1.0.2.5  2011-10-26
================================================

[Install Instruction]
- Replace NewsMontage.exe and also Dlls and Configuration files listed below in the application folder. 

[What's new] 
- Fixed	 : Import from Newsnet
- Fixed  : Removed Administrator Account Request
- Added  : Author on NewsNet Project Import

[Modules updated]


================================================
Media Alliance NewsMontage 1.0.2.3  2011-09-21
================================================

[Install Instruction]
- Replace NewsMontage.exe and also Dlls and Configuration files listed below in the application folder. 

[What's new] 
- Fixed	 : When a new project is creating from an already opened one, time-line will be not empty... last projects work still be...
- Fixed  : External text windows as not setted as topmost form..
- Added  : Now audio export tool propose a default export name, (project name + short name of selected tracks)
- Modify : Now Timeline duration is 20 minutes
- Fixed  : On Loading form Author of project is not visible

[Modules updated]
MediaAlliance.Controls.MxpTimeLine.dll			Ver:[1.0.1.7	   ]



================================================
Media Alliance NewsMontage 1.0.2.1  2011-08-29
================================================

[Install Instruction]
- Replace NewsMontage.exe and also Dlls and Configuration files listed below in the application folder. 

[What's new] 
- Fixed	 : Completed GUI translation
- Added	 : Zoom-Fit button on main timeline
- Added	 : New NMButtons control with visible click effetc


[Modules updated]
MediaAlliance.Controls.dll						Ver:[1.0.2.2	   ]

================================================
Media Alliance NewsMontage 1.0.1.54  2011-08-09
================================================

[Install Instruction]
- Replace NewsMontage.exe and also Dlls and Configuration files listed below in the application folder. 

[What's new] 
- Fixed	 : Drag over zero on Voice track and BG could generate a refresh error (if drag start directly from source-gallery)


[Modules updated]



================================================
Media Alliance NewsMontage 1.0.1.53  2011-08-09
================================================

[Install Instruction]
- Replace NewsMontage.exe and also Dlls and Configuration files listed below in the application folder. 

[What's new] 
- Added  : On adding new description of recorded voice file, you can confirm with enter
- Added  : Remove project features on "Startup Form" on right-click
- Added  : Double click to import NewsNet EDL


[Modules updated]
MediaAlliance.Controls.MxpTimeLine.dll			Ver:[1.0.1.5	   ]
MAVideoPreview.dll                              Ver:[1.0.0.8       ]
MediaAlliance.Controls							Ver:[1.0.1.2	   ]
language.en.config
language.it.config
language.ru.config




================================================
Media Alliance NewsMontage 1.0.1.50  2011-08-08
================================================

[Install Instruction]
- Replace NewsMontage.exe and also Dlls and Configuration files listed below in the application folder. 

[What's new] 
- Added  : Alert messagebox when Eraser or Blanker command has no clip where works...
- Fixed  : Drag over zero on Voice track and BG could generate a refresh error


[Modules updated]
language.en.config
language.it.config
language.ru.config



================================================
Media Alliance NewsMontage 1.0.1.46- 2011-08-07
================================================

[Install Instruction]
- Replace NewsMontage.exe and also Dlls and Configuration files listed below in the application folder. 

[What's new] 
- Fixed  : Play command issue called by graphical button 
- Added  : Some tooltip added with relative language file


[Modules updated]
language.en.config
language.it.config
language.ru.config



================================================
Media Alliance NewsMontage 1.0.1.41- 2011-08-07
================================================

[Install Instruction]
- Replace NewsMontage.exe and also Dlls and Configuration files listed below in the application folder. 

[What's new] 
- Fixed  : If fast click were done over clip gallery on Sources Tabs, more than two, application try to reopen clip more times
- Fixed  : Some TC convertion issues on NewsNet Project importing procedures
- Added	 : "Potential Errors" logs on ClipInfo form


[Modules updated]
MediaAlliance.Controls.MxpTimeLine.dll			Ver:[1.0.1.4	   ]
MAVideoPreview.dll                              Ver:[1.0.0.7       ]
MediaAlliance.Controls							Ver:[1.0.1.2	   ]


================================================
Media Alliance NewsMontage 1.0.1.38- 2011-08-06
================================================

[Install Instruction]
- Replace NewsMontage.exe and also Dlls and Configuration files listed below in the application folder. 

[What's new] 
- Fixed  : When fast movements were done on timeline, current position indication could be wrong
- Fixed  : Exporting audio track on new project, doesn't works, project are not saved
- Fixed  : On new project if you ad and audio track and try to export audio, track will not be on audio file.
- Added  : Refresh button on NewsNet project list
- Fixed	 : Mouse cursor is not visible when pass-over Jurnalist text box
- Fixed  : Tracks name on mainform and adio-export form
- Fixed  : On "ClipInfo" form, mouse is not visibile over file information textbox
- Added  : Possibility to hide information over preview windows, now default is unvisible
- Fixed  : Movements on timeline by PagUp and PagDwn doesn't perform right seek whene clip is marked


[Modules updated]
MediaAlliance.Controls.MxpTimeLine.dll			Ver:[1.0.1.4	   ]
MAVideoPreview.dll                              Ver:[1.0.0.7       ]

================================================
Media Alliance NewsMontage 1.0.1.29- 2011-07-26
================================================

[Install Instruction]
- Replace NewsMontage.exe and also Dlls and Configuration files listed below in the application folder. 

[What's new] 
- Fixed  : Clip back to back insertion on same timeline overlapped, the second one doesn't play
- Fixed  : Full project info form doesn't appears on click
- Fixes	 : ZoomAll button on Voice Tab doesn't works
- Fixed  : When user navigate by PageUp & PageDown preview doesn't display frames
- Fixed  : Editing position by TC Current position Contols on Video,Voice,BG Tabs doesn't take effects on preview
- Fixed	 : "Record time elapsed" display a wrong value, different from timeline duration
- Added  : Autopause timeline on Tabs, when changed selected tab
- Added  : Timeline Cursor follow play on PlayMark action
- Added	 : Tooltips
- Added  : Markin & Markout displayed on selection
- Added  : Graphical buttons to navigate betweend Starts and Ends of Clips (same ad pageup & pagedown)

[Modules updated]
MediaAlliance.Controls.MxpTimeLine.dll			Ver:[1.0.1.3	   ]
MAVideoPreview.dll                              Ver:[1.0.0.6       ]
language.en.config
language.it.config
language.ru.config

================================================
Media Alliance NewsMontage 1.0.1.21- 2011-06-23
================================================

[Install Instruction]
- Replace NewsMontage.exe in the application folder.

[What's new] 
- Fixed  : NewsNet basket import, wrong clips duration
- Added	 : Definition reader for output ScanOrder type on RendersProfiles
- Added  : Multimedia dettails for clip files (on Clip info form)

[Modules updated]
MediaAlliance.NewsMontage.RenderTM 1.0.1.0



================================================
Media Alliance NewsMontage 1.0.1.21- 2011-06-07
================================================

[Install Instruction]
- Replace NewsMontage.exe in the application folder.

[What's new] 
- Fixed  : NewsNet basket import, wrong clips duration


[Modules updated]




================================================
Media Alliance NewsMontage 1.0.1.19- 2011-05-27
================================================

[Install Instruction]
- Replace NewsMontage.exe and the modules updated listed below in the application's folder.
- Old projects that already has WaveForms cached, must be cleared by deleting file inside ProjectFolder\FrameCache... 
		all subfolders, and reload..

[What's new] 
- Fixed  : Decreased waveform generation priority
- Fixed	 : Zoom all on Background tab doesn't works
- Fixed	 : When cancel had been pressed after that "Startup form" has been opened, application exit

[Modules updated]
MediaAlliance.AV.dll                            Ver:[1.0.0.5       ]  Created:[2011-05-06T10:55:19]


================================================
Media Alliance NewsMontage 1.0.1.14- 2011-05-24
================================================

[Install Instruction]
- Replace NewsMontage.exe and the modules updated listed below in the application's folder.

[What's new] 
- Added  : Settings variable to set default-fix profile for audio export

[Modules updated]



===============================================
Media Alliance NewsMontage 1.0.1.2- 2011-05-19
===============================================

[Install Instruction]
- Replace NewsMontage.exe and the modules updated listed below in the application's folder.

[What's new] 
- Fixed  : Some timeline-zoom minor bugs
- Fixed	 : Waveform representation
- Fixed	 : Home/End go at frame 1 and last frame -1 instead of 0 and lastframe
- Fixed  : NextFrame command continue after timeline end
- Fixed  : During voice recording "space-bar" fire a play on timeline, instead record stop
- Fixed  : Undo operation issues after CUT/BLANK/ERASE
- Fixed  : AudioPoint could be moved after clip margin, so become unreachable
- Added	 : PagUp/PagDwn if timeline is marked goes to Markin/Markout, otherwise "PageDown" jump
		   between all clip-start before current position, and "PageUp" jump between all 
		   clip-end after current position
- Fixed  : Quicktime clips on Main timeline doesn't show output frame during scrub, until first
		   play/pause command.


[Modules updated]
FFMpegHelper.dll                                Ver:[1.1.0.4       ]  Created:[2011-05-06T10:55:19]
MAVideoPreview.dll                              Ver:[1.0.0.5       ]  Created:[2011-05-06T10:55:19]
MediaAlliance.AV.dll                            Ver:[1.0.0.3       ]  Created:[2011-05-06T10:55:19]
MediaAlliance.Controls.MxpTimeLine.dll          Ver:[1.0.0.5       ]  Created:[2011-05-06T10:55:19]
MediaAlliance.NewsMontage.RenderTM.dll          Ver:[1.0.0.3       ]  Created:[2011-05-06T10:55:19]


===============================================
Media Alliance NewsMontage 1.0.1.1- 2011-05-16
===============================================

[Install Instruction]
- Replace NewsMontage.exe and the modules updated listed below in the application's folder.

[What's new] 
- Added	 : Audio export tools

[Modules updated]
MediaAlliance.NewsMontage.RenderTM.dll          Ver:[1.0.0.1       ]  Created:[2011-05-06T10:55:19]



===============================================
Media Alliance NewsMontage 1.0.0.31- 2011-05-15
===============================================

[Install Instruction]
- Replace NewsMontage.exe and the modules updated listed below in the application's folder.

[What's new] 
- Fixed   : Some issues on genereting/showing thumbnails frame on source timelime
- Added	  : SyncPlay during Audio recording
- Fixed	  : Zoom issues (also increased zoom limit)

[Modules updated]
MediaAlliance.Controls.MxpTimeLine.dll          Ver:[1.0.0.4       ]  Created:[2011-05-06T10:55:19]


===============================================
Media Alliance NewsMontage 1.0.0.28- 2011-05-11
===============================================

[Install Instruction]
- Replace NewsMontage.exe and the modules updated listed below in the application's folder.

[What's new] 
- Added   : Possibility to remove unused clip from Sources
- Added   : Scrub audio in both Timelines, Main and Source 
- Added   : Mapped Home and End key, "Home" take position to timeline start "End" to last clip Tcout
		    if timeline is marked : "Home" take postion to MarkIn, "End" to markout
- Changed : When a clip portion was dragged or sent to timeline project, the selection on source 
			timeline is still available
- Fixed   : High CPU Usage on audio-clip instance
- Fixed	  : Audio functionality issue on source timeline
- Fixed	  : If cursor is inside clip and its track has disabled/enabled, clip still visible/unvisibile 
			until move cursor out
- Fixed	  : Some graphics issues on gallerie lists
- Fixed   : Drag clip over and before timeline-zero issues

[Modules updated]
MAVideoPreview.dll                              Ver:[1.0.0.4       ]  Created:[2011-05-06T10:55:19]
MediaAlliance.AV.dll                            Ver:[1.0.0.2       ]  Created:[2011-05-06T10:55:19]
MediaAlliance.Controls.MxpTimeLine.dll          Ver:[1.0.0.3       ]  Created:[2011-05-06T10:55:19]


===============================================
Media Alliance NewsMontage 1.0.0.5 - 2011-05-05
===============================================

[Install Instruction]
- Replace NewsMontage.exe and the modules updated listed below in the application's folder.

[What's new]
- Added : 
- Fixed : When any configuration saved, some parameters will be corrupted on ini file
- Fixed : Both timeline could be in play simultaneously
- Fixed : When new clip portion is dropped over cursor postion, clip is not visible until exit e ri-enter inside it.
- Fixed : When the current-online clip is removed, it is still visibile until cursor exit for the first time.
- Fixed : When dragging clip portion is dropped out of the timeline, green area still visibile until next drag
- Fixed : When clips from MOV file were added, they are not visible on seek until first play command
- Fixed : When clips has been resized and it was across cursor position, sometime its duration was not updated until first play
- Fixed : A new project could be created with no title
- Fixed : Sources buttons works on timeline also if no clip has loaded
- Fixed : Some undo issues
- Fixed : Context clips menu does not request confirmation on delete

[Modules updated]
MAVideoPreview.dll                              Ver:[1.0.0.2       ]  Created:[2011-04-30T13:28:43]
MediaAlliance.Controls.MxpTimeLine.dll          Ver:[1.0.0.2       ]  Created:[2011-04-30T13:28:44]



===============================================
Media Alliance NewsMontage 1.0.X.X - 2011-05-02
===============================================

[Install Instruction]
- First released version, install it by NewsMontage2011_Setup.exe

[What's new]
- 

[Modules updated]