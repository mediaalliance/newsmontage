using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Linq;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;

using MediaAlliance.Controls.MxpTimeLine;
using MediaAlliance.Controls.MxpTimeLine.Classes;
using MediaAlliance.Controls.MxpTimeLine.Objects;
using NewsMontange.Helpers;
using NewsMontange.Classes;
using MediaAlliance.AV;
using MediaAlliance.NewsMontage.RenderTM;

using Telerik.WinControls;
using NewsMontange.Forms;
using NewsMontange.Warnings;
using MediaAlliance.Controls.MxpTimeLine.Engine;
using MediaAlliance.Controls.MxpTimeLine.UndoManager;
using NewsMontange.Language;
using MediaAlliance.AV.DS.SubClasses;
using MediaAlliance.AV.CensorTools;

using MediaAlliance.ProductKey;
using System.Collections;



[assembly: Obfuscation(Feature = "code control flow obfuscation", Exclude = false)]
[assembly: Obfuscation(Feature = "encrypt resources", Exclude = false)]
[assembly: Obfuscation(Feature = "rename symbol names with printable characters", Exclude = false)]
//[assembly: Obfuscation(Feature = "string encryption", Exclude = true)]
[assembly: Obfuscation(Feature = "all", Exclude = true)]

[assembly: Obfuscation(Feature = "embed MediaAlliance.ProductKey.dll", Exclude = false)]


namespace NewsMontange
{
    public partial class frmMain : Form, ILicense
    {
        // CLASSES        
        private CProjectsHelper projectHelper                   = null;
        private VideoPreview vp                                 = null;
        private CThumbMaker thumbMaker                          = null;
        private CKeyboardManager keyboard_manager               = null;
        private CWaveFormMaker waveform_maker                   = null;
        private CRenderTM RenderTM                              = null;
        private CAppWarnings warnings                           = null;   

        // FORMS

        private frmLog frmdbg_internalLogs                      = null;
        private frmClipDebug frmdbg_clip                        = null;
        private Forms.frmStartup startup_form                   = null;
        private Forms.frmClipInfo frm_ClipInfo                  = null;
        private Forms.frmLoading frm_Loading                    = new Forms.frmLoading();
        private Forms.frmWarningList frm_WarningList            = null;
        private frmAbout frm_about                              = null;

        // CONTROLS

        private UserControls.SRC_VIDEO SRC_VIDEO                = null;
        private UserControls.SRC_VOICE SRC_VOICE                = null;
        private UserControls.SRC_BACKGROUND SRC_BACKGROUND      = null;

        private Timer tmrAutoSave                               = new Timer();

        
        private CMxpClip lastEditedClip                         = null;
        private CMxpClip onlineClip                             = null;


        private enFocusedArea focusedArea                       = enFocusedArea.Undefined;
        private Dictionary<Control, enFocusedArea> app_areas    = new Dictionary<Control, enFocusedArea>();
        
        
        private enApplicationStatus appStatus                   = enApplicationStatus.undifined;


        // PRIVATE FIELDS

        private string m_current_renderprofile = "";
        private string m_current_renderExtension = "";
        private string m_current_renderscanorder = "";

        private FormWindowState m_lastWindowState = FormWindowState.Normal;

        private string m_productkey = "";

        private bool m_runningdebug = false;


        public Dictionary<MediaAlliance.ProductKey.LocalizedStrings, string> Formlicense_language = new Dictionary<MediaAlliance.ProductKey.LocalizedStrings, string>();



        public frmMain()
        {
            CommonCTor();
        }

        public frmMain(string[] args)
        {
            InitializeComponent();

            CommonCTor();
#if DEBUG
            m_runningdebug = true;
#endif

            #region CHECK ARGUMENTS

            string j_args = string.Join(" ", args);
            if (j_args.IndexOf("/debug", StringComparison.OrdinalIgnoreCase) >= 0 ||
                j_args.IndexOf("-debug", StringComparison.OrdinalIgnoreCase) >= 0)
            {
                m_runningdebug = true;
            }

            #endregion CHECK ARGUMENTS

            if (m_runningdebug)
            {
                frmdbg_clip = new frmClipDebug(mxptl);

                frmdbg_internalLogs = new frmLog();

                internalLogToolStripMenuItem.Visible = true;
            }
            else
            {
                internalLogToolStripMenuItem.Visible = false; 
            }

            this.SetStyle(ControlStyles.ResizeRedraw, false);


            // WARNINGS
            warnings = new CAppWarnings();
            warnings.OnWarningAdded += new CAppWarnings.AW_OnWarningAdded(warnings_OnWarningAdded);

            frm_WarningList = new frmWarningList(warnings);
            //



            Text += " ver. " + Application.ProductVersion;// +" beta";

            //ThemeResolutionService.LoadPackageFile("NewsMontage_Theme.tssp");

            lblCurrentTC0.TimeCode  = 0;
            lblDuration.TimeCode    = 0;


            mxptl.TCOUT = 25 * 60 * 20;
            CMxpTrack track;

            //

            track = mxptl.Tracks.AddTrack("VOICE", enMxpTrackType.Audio, 70, null);
            track.TrackIcon = Properties.Resources.VoiceOver;
            track.Description = "Voiceover Track";
            //

            track = mxptl.Tracks.AddTrack("VIDEO-A", enMxpTrackType.Video, null);
            track.OnClipChangedTC += new CMxpTrack.MxpTrack_OnClipChangedTC(track_OnClipChangedTC);
            track.TrackIcon = Properties.Resources.Video;
            track.Description = "Video track A";

            track = mxptl.Tracks.AddTrack("AUDIO-A", enMxpTrackType.Audio, 50, null);
            track.Description = "Audio track A";

            //

            track = mxptl.Tracks.AddTrack("VIDEO-B", enMxpTrackType.Video, null);
            track.OnClipChangedTC += new CMxpTrack.MxpTrack_OnClipChangedTC(track_OnClipChangedTC);
            track.TrackIcon = Properties.Resources.Video;
            track.Description = "Video track B";

            track = mxptl.Tracks.AddTrack("AUDIO-B", enMxpTrackType.Audio, 50, null);
            track.Description = "Audio track B";

            //

            track = mxptl.Tracks.AddTrack("BG", enMxpTrackType.Audio, 70, null);
            track.TrackIcon = Properties.Resources.Background;
            track.Description = "Background Audio track";

        }

        private void CommonCTor()
        {
            System.Diagnostics.Debug.Write("NM START");
            //m_formlicense_language.Add(LocalizedStrings.FormCaption, 
        }



        public void Init(object[] args)
        {
            // INIZILIZZAZIONE DATA DALLA PRODUCTKEY

            m_productkey = (string)args[0];
            BitArray mask = (BitArray)args[1];
        }


        private void DeepFocusHandling(Control ctl)
        {
            foreach (Control subctl in ctl.Controls)
            {
                subctl.GotFocus += new EventHandler(subctl_GotFocus);
                DeepFocusHandling(subctl);
            }
        }

        void subctl_GotFocus(object sender, EventArgs e)
        {
            Control ctl = (Control)sender;

            while (ctl != null)
            {
                if (ctl is MA_NmPanel)
                {                    
                    if (app_areas.ContainsKey(ctl))
                    {
                        this.FocusedArea = app_areas[ctl];
                    }

                    break;
                }

                ctl = ctl.Parent;
            }
        }






        void source_tab_OnChange(object Sender, int index, object Tag)
        {
            if (Tag != null)
            {
                if (lblGeneratingWaves.Controls.Count > 0 && lblGeneratingWaves.Controls[0] == Tag)
                {
                    return;
                }

                Program.Logger.debug("Switched source area to " + Tag.GetType().ToString());

                if (lblGeneratingWaves.Controls.Count > 0)
                {
                    // VEDO SE LA TIMELINE DEL CONTROLLO E' IN PLAY ED ESEGUO UN PAUSE

                    FieldInfo fi = lblGeneratingWaves.Controls[0].GetType().GetField("Timeline");

                    if (fi != null)
                    {
                        MxpTmeLine tl = (MxpTmeLine)fi.GetValue(lblGeneratingWaves.Controls[0]);
                        if (tl.InternalEngine.EngineStatus == InternalEngineStatus.play)
                        {
                            tl.InternalEngine.Pause();
                        }
                    }

                    // NASCONDO QUELLO ATTUALE

                    lblGeneratingWaves.Controls[0].Parent = null;
                }

                if (Tag != null)
                {
                    lblGeneratingWaves.Controls.Add((Control)Tag);
                }
            }
        }



        #region MAINFORM EVENTS

        private void OpenProductKeyForm()
        {
            MediaAlliance.ProductKey.CProductKey.CallMethod("ShowLicenceForm", new object[] { m_productkey, Formlicense_language, this });

            //frmProductKey frmPK= new frmProductKey();
            //frmPK.ProductKey = m_productkey;
            //frmPK.ShowDialog();

            //if (!frmPK.IsOK)
            //{
            //    MessageBox.Show(Application.ProductName + " is now closing");
            //    Environment.Exit(2);
            //}
            //else
            //{
            //    StreamWriter sw = new StreamWriter("productkey.dat", false);
            //    sw.WriteLine(frmPK.ProductKey);
            //    sw.Close();
            //    sw.Dispose();

            //    m_productkey = frmPK.ProductKey;
            //}

            //frmPK.Dispose();

        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            //DateTime realdatetime = DateTime.Now;

            //if (File.Exists("MACore.dll"))
            //{
            //    try
            //    {
            //        Program.Logger.info(" [MACore] Initing...");
            //        Assembly core = Assembly.LoadFile(new FileInfo("MACore.dll").FullName);
            //        Type t = core.GetType("MACore.CMACore");
            //        object core_class = Activator.CreateInstance(t);
            //        realdatetime = (DateTime)t.GetMethod("CallMethod").Invoke(core_class, new object[] { "GetRealDatetimeAprox", new object[] { "NewsMontage" } });

            //        string le = "";

            //        if (t.GetProperty("LastError") != null)
            //        {
            //            PropertyInfo pi = t.GetProperty("LastError");
            //            le = (string)pi.GetValue(core_class, null);
            //        }

            //        if (le != "")
            //        {
            //            Program.Logger.error("  > " + le);
            //        }

            //        core_class = null;

            //        Program.Logger.info(" RD " + realdatetime.ToString("ddMM"));
            //    }
            //    catch (Exception ex)
            //    {
            //        Program.Logger.error(" [MACore] Error : " + ex.Message);
            //    }
            //}

            //if (File.Exists("productkey.dat"))
            //{
            //    StreamReader sr = new StreamReader("productkey.dat");
            //    m_productkey = sr.ReadLine();
            //    sr.Close();
            //    sr.Dispose();

            //    if (m_productkey.Trim() != "")
            //    {
            //        MediaAlliance.ProductKey.CProductKey.enProductKeyCheckResult res = CProductKey.enProductKeyCheckResult.ok;

            //        if (m_productkey.StartsWith(Application.ProductName, StringComparison.OrdinalIgnoreCase))
            //        {
            //            try
            //            {
            //                res = MediaAlliance.ProductKey.CProductKey.check(Application.ProductName, m_productkey, realdatetime);
            //                Program.Logger.info(" RES " + Enum.GetName(typeof(MediaAlliance.ProductKey.CProductKey.enProductKeyCheckResult), res));
            //            }
            //            catch (Exception ex)
            //            {
            //                Program.Logger.error(" PK : " + ex.Message);
            //            }
            //        }
            //        else
            //        {
            //            res = CProductKey.enProductKeyCheckResult.invalid;
            //        }

            //        switch (res)
            //        {
            //            case CProductKey.enProductKeyCheckResult.expired:
            //                MessageBox.Show("Your software has been expired !");
            //                OpenProductKeyForm();
            //                break;
            //            case CProductKey.enProductKeyCheckResult.invalid:
            //                MessageBox.Show("Your software ProductKey is not valid !");
            //                OpenProductKeyForm();
            //                break;
            //            case CProductKey.enProductKeyCheckResult.notfound:
            //                MessageBox.Show("Your software is not active !");
            //                OpenProductKeyForm();
            //                break;
            //            case CProductKey.enProductKeyCheckResult.ok:
            //                // OK SOFTWARE ACTIVE ACTIVE
            //                break;
            //        }
            //    }
            //    else
            //    {
            //        MessageBox.Show("Your software is not active !");
            //        OpenProductKeyForm();
            //    }
            //}
            //else
            //{
            //    MessageBox.Show("Your software is not active !");
            //    OpenProductKeyForm();
            //}




            Dictionary<int, System.Runtime.InteropServices.ComTypes.IMoniker> moniker = new Dictionary<int, System.Runtime.InteropServices.ComTypes.IMoniker>();
            
            moniker = CEnumerate.Get_ListOfVideoAcquireHardware(CEnumerate.FilterType.VideoCompressorCategory);
            Dictionary<int, MediaAlliance.AV.DS.SubClasses.CEnumerate.LibInfo> filters = CEnumerate.Get_FriendlyListOfVideoAcquireHardware(ref moniker);

            // STARTUP FORM
            startup_form = new Forms.frmStartup();

            // AUTOSAVE

            tmrAutoSave.Tick += new EventHandler(tmrAutoSave_Tick);

            if (Program.Settings.Project_Options.AUTOSAVE_TIME > 0)
            {
                tmrAutoSave.Interval = Program.Settings.Project_Options.AUTOSAVE_TIME * 1000;
                tmrAutoSave.Enabled = true;

                lblAutosave.Text = CDict.GetValue("MainForm.StatusBar.Autosave", "Autosave : ") + Program.Settings.Project_Options.AUTOSAVE_TIME + " s";
            }
            else
            {
                lblAutosave.Text = CDict.GetValue("MainForm.StatusBar.Autosave", "Autosave : ") + " off";
            }

            // KEYBOARD MANAGER
            keyboard_manager = new CKeyboardManager(this, mxptl);

            // RENDER TASK MANAGER
            RenderTM = new CRenderTM();
            RenderTM.OnRun                  += new CRenderTM.Render_OnRun(RenderTM_OnRun);
            RenderTM.OnProgress             += new CRenderTM.Render_OnProgress(RenderTM_OnProgress);
            RenderTM.OnInfo                 += new CRenderTM.Render_OnInfo(RenderTM_OnInfo);
            RenderTM.OnVerboseInfo          += new CRenderTM.Render_OnVerboseInfo(RenderTM_OnVerboseInfo);
            RenderTM.OnFinish               += new CRenderTM.Render_OnFinish(RenderTM_OnFinish);
            RenderTM.OnEngineNotFound       += new CRenderTM.Render_OnEngineNotFound(RenderTM_OnEngineNotFound);

            RenderTM.RenderEnginePath = Program.Settings.RENDER_Options.RENDER_ENGINE_PATH;

            if (m_current_renderprofile != "")
            {
                // LA PRIMA VOLTA DALL'AVVIO LEGGO LE INFO SUL RENDER PROFILE QUI

                this.ChangedRenderProfile(this, m_current_renderprofile);
            }

            mediaAsToolStripMenuItem.Visible = Program.Settings.RENDER_Options.EXPORT_AS_ENABLED;

            // WAVE FORM MAKER
            waveform_maker                  = new CWaveFormMaker();
            waveform_maker.MainFormHandle   = this.Handle;
            waveform_maker.OnRendered       += new CWaveFormMaker.wfmk_OnRendered(waveform_maker_OnRendered);
            waveform_maker.OnChangeWorking  += new CWaveFormMaker.wfmk_OnChangeWorking(waveform_maker_OnChangeWorking);

            // AREE GRAFICHE APPLICATIVO

            app_areas.Add(mA_NmPanel2, enFocusedArea.MontageArea);
            app_areas.Add(mA_NmPanel1, enFocusedArea.OutputPreview);
            app_areas.Add(mA_NmPanel3, enFocusedArea.SourcePreview);
            app_areas.Add(mA_NmPanel4, enFocusedArea.SourceManager);

            
            // INIZIALIZZO ENGINE INTERNO DELLA TIMELINE
            mxptl.InitializeInternalEngine();
  

            // CLASSE PREVIEW
            vp = new VideoPreview(  picPreview1, 
                                    Program.Settings.Options.PREVIEW_ENGINE_DS_ONLY, 
                                    Program.Settings.Options.TIMELINE_AUDIOSCRUB);

            vp.InfoVisible = Program.Settings.Options.VIDEO_INFO_OSD_VISIBLE;

            // INIZIALIZZO IL THUMBMAKER
            thumbMaker = new CThumbMaker();
            thumbMaker.OnDone += new CThumbMaker.ThM_OnDone(thumbMaker_OnDone);
            thumbMaker.START();


            string prjpath = Program.Settings.Project_Options.PROJECTS_PATH; // @"D:\! MyExtra\Controls\MxpTimeLine\Test_Projects"; //new FileInfo(Application.ExecutablePath).DirectoryName;

            if (prjpath.Trim() == ".")
            {
                prjpath = new FileInfo(Application.ExecutablePath).Directory.FullName;
                prjpath = Path.Combine(prjpath, "Projects");
            }
            
            Program.Logger.debug("Assigned projects path : " + prjpath);

            if (!Directory.Exists(prjpath))
            {
                try
                {
                    if (Directory.Exists(prjpath)) Directory.CreateDirectory(prjpath);
                }
                catch (Exception ex)
                {
                    Program.Logger.error("Unable to create projects folder " + prjpath);
                    Program.Logger.error(ex);
                    MessageBox.Show(CDict.GetValue("MainForm.Mbox.UnableCreatePrjForm", "Unable to create projects folder, please check NM_Settings.config"));
                    Environment.Exit(1);
                }
            }

            projectHelper = new CProjectsHelper(prjpath, mxptl, vp);
            projectHelper.OnStartLoading                += new CProjectsHelper.Prj_OnStartLoading(projectHelper_OnStartLoading);
            projectHelper.OnFinishLoading               += new CProjectsHelper.Prj_OnFinishLoading(projectHelper_OnFinishLoading);
            projectHelper.OnProgress                    += new CProjectsHelper.Prj_OnProgress(projectHelper_OnProgress);
            projectHelper.OnAddedSource                 += new CProjectsHelper.Prj_OnAddedSource(projectHelper_OnAddedSource);
            projectHelper.OnRemovingSource              += new CProjectsHelper.Prj_OnRemovingSource(projectHelper_OnRemovingSource);
            projectHelper.OnUnloadedAll                 += new CProjectsHelper.Prj_OnUnloadedAll(projectHelper_OnUnloadedAll);
            projectHelper.OnSourceNotFound              += new CProjectsHelper.Prj_OnSourceNotFound(projectHelper_OnSourceNotFound);
            projectHelper.OnCurrentProjectChanged       += new CProjectsHelper.Prj_OnCurrentProjectChanged(projectHelper_OnCurrentProjectChanged);
            projectHelper.RollbackManager.OnAddedBackup += new CRollback_Collection.RB_OnAddedBackup(RollbackManager_OnAddedBackup);

            if (Program.Settings.Project_Options.CHECK_SOURCE)
            {
                Program.Logger.info(" Initializing Source Checker because required");
                projectHelper.InitSourceChecker();
            }
            else
            {
                Program.Logger.info(" No Source Checker required"); 
            }



            // COSTRUISCO I VARI CONTENITORI PER I SOURCE

            // VIDEO
            SRC_VIDEO                       = new UserControls.SRC_VIDEO();
            SRC_VIDEO.Name                  = "SRC_VIDEO";
            SRC_VIDEO.Dock                  = DockStyle.Fill;
            SRC_VIDEO.ProjectHelper         = projectHelper;
            SRC_VIDEO.PreviewMonitor        = preview_monitor_src;
            SRC_VIDEO.Montage_timeline      = mxptl;
            SRC_VIDEO.CurrentPosition_Controls.Add(lblCurrentTC_SRC);
            lblGeneratingWaves.Controls.Add(SRC_VIDEO);

            // VOICE
            SRC_VOICE                       = new UserControls.SRC_VOICE();
            SRC_VOICE.Name                  = "SRC_VOICE";
            SRC_VOICE.Dock                  = DockStyle.Fill;
            SRC_VOICE.ProjectHelper         = projectHelper;
            SRC_VOICE.Waveform_maker        = waveform_maker;
            SRC_VOICE.Montage_timeline      = mxptl;
            SRC_VOICE.PreviewMonitor        = preview_monitor_src;
            SRC_VOICE.CurrentPosition_Controls.Add(lblCurrentTC_SRC);
            lblGeneratingWaves.Controls.Add(SRC_VOICE);
            SRC_VOICE.Parent = null;

            // BACKGROUND

            SRC_BACKGROUND                  = new UserControls.SRC_BACKGROUND();
            SRC_BACKGROUND.Name             = "SRC_BACKGROUND";
            SRC_BACKGROUND.Dock             = DockStyle.Fill;
            SRC_BACKGROUND.ProjectHelper    = projectHelper;
            SRC_BACKGROUND.Waveform_maker   = waveform_maker;
            SRC_BACKGROUND.Montage_timeline = mxptl;
            SRC_BACKGROUND.PreviewMonitor   = preview_monitor_src;
            SRC_BACKGROUND.CurrentPosition_Controls.Add(lblCurrentTC_SRC);
            lblGeneratingWaves.Controls.Add(SRC_BACKGROUND);
            SRC_BACKGROUND.Parent = null;



            source_tab.AddButton(CDict.GetValue("MainForm.VideoSource.Title", "VIDEO"), Properties.Resources.tabsrc_video_icon, SRC_VIDEO).Selected = true;
            source_tab.AddButton(CDict.GetValue("MainForm.VoiceSource.Title", "VOICE"), Properties.Resources.tabsrc_voice_icon, SRC_VOICE);
            source_tab.AddButton(CDict.GetValue("MainForm.BackgroundSource.Title", "BACKGROUND"), Properties.Resources.tabsrc_back_icon, SRC_BACKGROUND);


            source_tab.OnChange += new MA_NmPanel_Tabs.NMTabs_OnChange(source_tab_OnChange);


            DeepFocusHandling(this);
            DeepFocusHandling(SRC_VIDEO);
            DeepFocusHandling(SRC_VOICE);
            DeepFocusHandling(SRC_BACKGROUND);

            StarupPage(true);
        }


        private void LoadLanguage()
        {
            Program.LanguageFile = "language." + Program.Settings.GUI_Options.LANGUAGE + ".config";


            if (!File.Exists(Program.LanguageFile))
            {
                Program.Logger.error(" Language file not found : " + Program.LanguageFile);
            }
            else
            {
                Program.Logger.info(" Loading language from file : " + Program.LanguageFile);


                CDict.AddFromFile("language." + Program.Settings.GUI_Options.LANGUAGE + ".config");

                fileToolStripMenuItem.Text = CDict.GetValue("MainForm.MainMenu.File", fileToolStripMenuItem.Text);
                {
                    startupMenuToolStripMenuItem.Text = CDict.GetValue("MainForm.MainMenu.StartupMenu", startupMenuToolStripMenuItem.Text);
                    loadToolStripMenuItem.Text = CDict.GetValue("MainForm.MainMenu.Load", loadToolStripMenuItem.Text);
                    saveToolStripMenuItem.Text = CDict.GetValue("MainForm.MainMenu.Save", saveToolStripMenuItem.Text);
                    rollbackToolStripMenuItem.Text = CDict.GetValue("MainForm.MainMenu.Rollback", rollbackToolStripMenuItem.Text);
                    exportToolStripMenuItem.Text = CDict.GetValue("MainForm.MainMenu.Export", exportToolStripMenuItem.Text);
                    mediaToolStripMenuItem.Text = CDict.GetValue("MainForm.MainMenu.Export.Media", mediaToolStripMenuItem.Text);
                    mediaAsToolStripMenuItem.Text = CDict.GetValue("MainForm.MainMenu.Export.MediaAs", mediaAsToolStripMenuItem.Text);
                    exportAudioToolStripMenuItem.Text = CDict.GetValue("MainForm.MainMenu.Export.Audio", exportAudioToolStripMenuItem.Text);
                }

                editToolStripMenuItem.Text = CDict.GetValue("MainForm.MainMenu.Edit", editToolStripMenuItem.Text);
                {
                    undoToolStripMenuItem.Text = CDict.GetValue("MainForm.MainMenu.Edit.Undo", undoToolStripMenuItem.Text);
                    preferenceToolStripMenuItem.Text = CDict.GetValue("MainForm.MainMenu.Edit.Preference", preferenceToolStripMenuItem.Text);
                }

                toolStripMenuItem1.Text = CDict.GetValue("MainForm.MainMenu.Options", toolStripMenuItem1.Text);
                {
                    clipInfoToolStripMenuItem.Text = CDict.GetValue("MainForm.MainMenu.Options.ClipInfo", clipInfoToolStripMenuItem.Text);
                }

                projectToolStripMenuItem.Text = CDict.GetValue("MainForm.MainMenu.Project", projectToolStripMenuItem.Text);
                {
                    recoverBlankOnStartToolStripMenuItem.Text = CDict.GetValue("MainForm.MainMenu.Project.RecoverBlanckOnStart", recoverBlankOnStartToolStripMenuItem.Text);
                }

                aboutToolStripMenuItem.Text = CDict.GetValue("MainForm.MainMenu.About", aboutToolStripMenuItem.Text);


                // OUTPUT WINDOW
                {
                    label1.Text = CDict.GetValue("MainForm.OutputWin.CurrentPosition", label1.Text);
                    label3.Text = CDict.GetValue("MainForm.OutputWin.Duration", label3.Text);
                }

                // STATUS BAR
                {
                    lblAutosave.Text = CDict.GetValue("MainForm.StatusBar.Autosave", lblAutosave.Text);
                    label2.Text = CDict.GetValue("MainForm.StatusBar.WaveformGenerating", label2.Text);
                }

                // RIGHT CLIPS MENU
                {
                    eliminaToolStripMenuItem.Text = CDict.GetValue("MainForm.MainMenu.ClipMenu.Remove", eliminaToolStripMenuItem.Text);
                    clipInfoToolStripMenuItem1.Text = CDict.GetValue("MainForm.MainMenu.ClipMenu.ClipInfo", clipInfoToolStripMenuItem1.Text);
                    autoFadeAudioToolStripMenuItem.Text = CDict.GetValue("MainForm.MainMenu.ClipMenu.AutoFade", autoFadeAudioToolStripMenuItem.Text);
                }

                // PROJECT INFO CONTROL

                ProjectInfoUC.UpdateLanguage();
            }
        }

        private void InitTooltips()
        {
            // TOOLTIPS

            //ttip.SetToolTip(btnFrameFFW, CDict.GetValue("MainForm.Tooltip.FrameForward", "Frame forward"));
            btnFrameFFW.Tooltip_Text = CDict.GetValue("MainForm.Tooltip.FrameForward", "Frame forward");
            //ttip.SetToolTip(btnFrameREW, CDict.GetValue("MainForm.Tooltip.FrameBackward", "Frame backward"));
            btnFrameREW.Tooltip_Text = CDict.GetValue("MainForm.Tooltip.FrameBackward", "Frame backward");
            //ttip.SetToolTip(btnPlay, CDict.GetValue("MainForm.Tooltip.Play", "Play"));
            btnPlay.Tooltip_Text = CDict.GetValue("MainForm.Tooltip.Play", "Play");
            //ttip.SetToolTip(btnPause, CDict.GetValue("MainForm.Tooltip.Pause", "Pause"));
            btnPause.Tooltip_Text = CDict.GetValue("MainForm.Tooltip.Pause", "Pause");

            //ttip.SetToolTip(btn_FrameFFwSrc, CDict.GetValue("MainForm.Tooltip.FrameForward", "Frame forward"));
            btn_FrameFFwSrc.Tooltip_Text = CDict.GetValue("MainForm.Tooltip.FrameForward", "Frame forward");
            //ttip.SetToolTip(btn_FrameREWSrc, CDict.GetValue("MainForm.Tooltip.FrameBackward", "Frame backward"));
            btn_FrameREWSrc.Tooltip_Text = CDict.GetValue("MainForm.Tooltip.FrameBackward", "Frame backward");
            //ttip.SetToolTip(btn_PlaySrc, CDict.GetValue("MainForm.Tooltip.Play", "Play"));
            btn_PlaySrc.Tooltip_Text = CDict.GetValue("MainForm.Tooltip.Play", "Play");
            //ttip.SetToolTip(btn_PauseSrc, CDict.GetValue("MainForm.Tooltip.Pause", "Pause"));
            btn_PauseSrc.Tooltip_Text = CDict.GetValue("MainForm.Tooltip.Pause", "Pause");

            //ttip.SetToolTip(btnClipCut2, CDict.GetValue("MainForm.Tooltip.RazorTool", "Razor tool"));
            btnClipCut2.Tooltip_Text = CDict.GetValue("MainForm.Tooltip.RazorTool", "Razor tool");
            //ttip.SetToolTip(btnBlank, CDict.GetValue("MainForm.Tooltip.BlankerTool", "Blanker tool"));
            btnBlank.Tooltip_Text = CDict.GetValue("MainForm.Tooltip.BlankerTool", "Blanker tool");
            //ttip.SetToolTip(btnClipRemover, CDict.GetValue("MainForm.Tooltip.EraserTool", "Eraser tool"));
            btnClipRemover.Tooltip_Text = CDict.GetValue("MainForm.Tooltip.EraserTool", "Eraser tool");
            //ttip.SetToolTip(btnZAll2, CDict.GetValue("MainForm.Tooltip.Zoomall", "Zoom All"));
            btnZAll2.Tooltip_Text = CDict.GetValue("MainForm.Tooltip.Zoomall", "Zoom All");
            btnZoomFit.Tooltip_Text = CDict.GetValue("MainForm.Tooltip.ZoomFit", "Zoom Fit");

            btnStepBW.Tooltip_Text = CDict.GetValue("MainForm.Tooltip.StepBackward", "Go to previous start");
            btnStepFW.Tooltip_Text = CDict.GetValue("MainForm.Tooltip.StepForward", "Go to next end");

            ttip.SetToolTip(lblCurrentClip, CDict.GetValue("MainForm.Tooltip.CurrentClipName", "Current displayed clip"));
            ttip.SetToolTip(lblCurrentTC0, CDict.GetValue("MainForm.Tooltip.CursorPosition", "Cursor position"));
            ttip.SetToolTip(lblDuration, CDict.GetValue("MainForm.Tooltip.ProjectDuration", "Project duration"));

            // VIDEO SOURCE CONTROL

            ttip.SetToolTip(SRC_VIDEO.btnSendToA, CDict.GetValue("SRC_VIDEO.Tooltip.SendToTrack_A", "Send selection to Track A"));
            ttip.SetToolTip(SRC_VIDEO.btnSendToB, CDict.GetValue("SRC_VIDEO.Tooltip.SendToTrack_B", "Send selection to Track B"));
            ttip.SetToolTip(SRC_VIDEO.picPlayVirtualClip, CDict.GetValue("SRC_VIDEO.Tooltip.PlayMarked", "Play marked area"));
            ttip.SetToolTip(SRC_VIDEO.picZoomAllSrc2, CDict.GetValue("SRC_VIDEO.Tooltip.Zoomall", "Zoom All"));
            ttip.SetToolTip(SRC_VIDEO.picVirtualClip, CDict.GetValue("SRC_VIDEO.Tooltip.DragMarkedArea", "Drag marked area"));
            ttip.SetToolTip(SRC_VIDEO.picOpen, CDict.GetValue("SRC_VIDEO.Tooltip.ImportFile", "Import new file"));

            // VOICE SOURCE CONTROL

            ttip.SetToolTip(SRC_VOICE.btnPlayMarker, CDict.GetValue("SRC_VOICE.Tooltip.PlayMarked", "Play marked area"));
            ttip.SetToolTip(SRC_VOICE.btnVirtualClip, CDict.GetValue("SRC_VOICE.Tooltip.DragMarkedArea", "Drag marked area"));
            ttip.SetToolTip(SRC_VOICE.btnZoomAll, CDict.GetValue("SRC_VOICE.Tooltip.Zoomall", "Zoom All"));
            ttip.SetToolTip(SRC_VOICE.btnRec, CDict.GetValue("SRC_VOICE.Tooltip.RecButton", "Start Recording"));
            ttip.SetToolTip(SRC_VOICE.chkPlayOnRec, CDict.GetValue("SRC_VOICE.Tooltip.PlayOnRec", "Play project while Reconding"));
            ttip.SetToolTip(SRC_VOICE.picOpen, CDict.GetValue("SRC_VOICE.Tooltip.ImportFile", "Import new file"));
            ttip.SetToolTip(SRC_VOICE.richEnlarge, CDict.GetValue("SRC_VOICE.Tooltip.ShowEditorOnExernalWindow", "Show edito on external window"));

            // BACKGROUND SOURCE CONTROL

            ttip.SetToolTip(SRC_BACKGROUND.btnVirtualClip, CDict.GetValue("SRC_BACKGROUND.Tooltip.DragMarkedArea", "Drag marked area"));
            ttip.SetToolTip(SRC_BACKGROUND.btnPlayMarker, CDict.GetValue("SRC_BACKGROUND.Tooltip.PlayMarked", "Play marked area"));
            ttip.SetToolTip(SRC_BACKGROUND.btnZoomAll, CDict.GetValue("SRC_BACKGROUND.Tooltip.Zoomall", "Zoom All"));
            ttip.SetToolTip(SRC_BACKGROUND.picOpen, CDict.GetValue("SRC_BACKGROUND.Tooltip.ImportFile", "Import new file"));
        }

        public void Settings_OnLoaded(object Sender)
        {
            LoadLanguage();

            // IMPOSTO I COMPONENTI CON IL FRAMERATE LETTO DALL'INI
            mxptl.FPS               = Program.Settings.Project_Options.FRAMERATE;
            lblCurrentTC0.FPS       = Program.Settings.Project_Options.FRAMERATE;
            lblDuration.FPS         = Program.Settings.Project_Options.FRAMERATE;
            lblCurrentTC_SRC.FPS    = Program.Settings.Project_Options.FRAMERATE;

            if (vp != null)
            {
                vp.InfoVisible = Program.Settings.Options.VIDEO_INFO_OSD_VISIBLE;
            }
            
            if (Program.Settings.Project_Options.FRAMERATE == 25)
            {
                Program.Standard = enVideoStandards.PAL;
            }
            else if (Program.Settings.Project_Options.FRAMERATE == 29.97f)
            {
                Program.Standard = enVideoStandards.NTSC;
            }

            lblProjectStandard.Text = Program.Standard.ToString();

            mxptl.UndoTimes = Program.Settings.Options.UNDO_TIMES;

            CheckWarningOnSettings();
        }

        public void Settings_OnSetupFormRequested(object Sender)
        {
            frmPreference pref = new frmPreference();
            pref.ShowDialog();
            pref.Dispose();
            Program.Settings.Save();
        }

        private void CheckWarningOnSettings()
        {
            warnings.Remove(WarnContext.ini_configuration);

            if (Program.Settings.Project_Options.NEWSNET_PRJ_PATH.Trim() != "")
            {
                if (!Directory.Exists(Program.Settings.Project_Options.NEWSNET_PRJ_PATH.Trim()))
                {
                    warnings.AddWarn(WarnType.pathNotFound,
                                    "Path not found",
                                    "NewsNet Projects path not found on : " + Program.Settings.Project_Options.NEWSNET_PRJ_PATH,
                                    WarnContext.ini_configuration);
                }
            }

            if (Program.Settings.RENDER_Options.EXPORT_FIX_PATH.Trim() != "")
            {
                if (!Directory.Exists(Program.Settings.RENDER_Options.EXPORT_FIX_PATH.Trim()))
                {
                    warnings.AddWarn(WarnType.pathNotFound,
                                    "Path not found",
                                    "Render output folder not found on : " + Program.Settings.RENDER_Options.EXPORT_FIX_PATH,
                                    WarnContext.ini_configuration);
                }
            }
            else
            {
                warnings.AddWarn(WarnType.pathNotFound,
                                "Path not setted",
                                "Render output folder is not configured",
                                WarnContext.ini_configuration); 
            }

            if (warnings.Warnings.Count == 0)
            {
                tsmAppAlert.Visible = false;
            }
        }


        void tmrAutoSave_Tick(object sender, EventArgs e)
        {
            if (projectHelper.CurrentProject != null)
            {
                projectHelper.RollbackManager.Addbackup("AUTOSAVE " + DateTime.Now.ToString("dd/MM/yyyy HH.mm"), enRollbackType.autosave);
            }
        }

        public class WM_SYSCOMMAND
        {
            public const uint SC_CLOSE = 0xF060;            // Closes the window.
            public const uint SC_CONTEXTHELP = 0xF180;
            public const uint SC_DEFAULT = 0xF160;          // Selects the default item; the user double-clicked the window menu.
            public const uint SC_HOTKEY = 0xF150;           // Activates the window associated with the application-specified hot key. The lParam parameter identifies the window to activate.
            public const uint SC_HSCROLL = 0xF080;          // Scrolls horizontally.
            public const uint SCF_ISSECURE = 0x00000001;    // Indicates whether the screen saver is secure. 
            public const uint SC_KEYMENU = 0xF100;          // Retrieves the window menu as a result of a keystroke. For more information, see the Remarks section.
            public const uint SC_MAXIMIZE = 0xF030;         // Maximizes the window.
            public const uint SC_MINIMIZE = 0xF020;         // Minimizes the window.
            public const uint SC_MONITORPOWER = 0xF170;     // Sets the state of the display : (-1 (the display is powering on), 1 (the display is going to low power), 2 (the display is being shut off)
            public const uint SC_MOUSEMENU = 0xF090;        // Retrieves the window menu as a result of a mouse click.
            public const uint SC_MOVE = 0xF010;             // Moves the window.
            public const uint SC_NEXTWINDOW = 0xF040;       // Moves to the next window.
            public const uint SC_PREVWINDOW = 0xF050;       // Moves to the previous window.
            public const uint SC_RESTORE = 0xF120;          // Restores the window to its normal position and size.
            public const uint SC_SCREENSAVE = 0xF140;       // Executes the screen saver application specified in the [boot] section of the System.ini file.
            public const uint SC_SIZE = 0xF000;             // Sizes the window.
            public const uint SC_TASKLIST = 0xF130;         // Activates the Start menu.
            public const uint SC_VSCROLL = 0xF070;          // Scrolls vertically.
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == CWaveFormMaker.OnChangedWorking)
            {
                bool working = (m.WParam.ToInt32() == 1) ? true : false;
            }

            if (m.Msg == 0x0112) // WM_SYSCOMMAND
            {
                // Check your window state here

                if (m.WParam == new IntPtr(WM_SYSCOMMAND.SC_MAXIMIZE) || m.WParam == new IntPtr(WM_SYSCOMMAND.SC_RESTORE)) 
        
                {
                    mxptl.CurrentPosition = lblCurrentTC0.TimeCode;
                }
            }


            base.WndProc(ref m);
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (projectHelper.CurrentProject != null)
            {
                projectHelper.RollbackManager.Addbackup("As closed at " + DateTime.Now.ToString("dd/MM/yyyy HH.mm.ss"), enRollbackType.asClosed);
            }

            vp.Dispose();

            SRC_VIDEO.Dispose();
            SRC_VOICE.Dispose();
            SRC_BACKGROUND.Dispose();

            if (thumbMaker != null)
            {
                thumbMaker.Dispose();
            }
        }
        
        private void frmMain_Shown(object sender, EventArgs e)
        {
            InitTooltips();

            if (m_runningdebug)
            {
                frmdbg_clip.Show();
            }

            mxptl.Focus();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (!keyboard_manager.ProcessCmdKey(ref msg, keyData))
            {
                return base.ProcessCmdKey(ref msg, keyData);
            }
            else
            {
                return true;
            }
        }

        private void frmMain_ResizeBegin(object sender, EventArgs e)
        {
            if (lblGeneratingWaves.Controls.Count > 0) lblGeneratingWaves.Controls[0].Hide();
        }

        private void frmMain_ResizeEnd(object sender, EventArgs e)
        {
            if (lblGeneratingWaves.Controls.Count > 0) lblGeneratingWaves.Controls[0].Show();
            mxptl.CurrentPosition = lblCurrentTC0.TimeCode;
        }
        
        private void frmMain_SizeChanged(object sender, EventArgs e)
        {
            if (m_lastWindowState != this.WindowState)
            {
                m_lastWindowState = this.WindowState;
                mxptl.CurrentPosition = lblCurrentTC0.TimeCode;
            }
        }

        #endregion MAINFORM EVENTS
        


        #region WARNINGS EVENTS
        
        private void tsmAppAlert_Click(object sender, EventArgs e)
        {
            frm_WarningList.Visible = !frm_WarningList.Visible;
        }

        void warnings_OnWarningAdded(object Sender, CAppWarnInfo warn)
        {
            tsmAppAlert.Visible = true;
            Program.Logger.warning("Warning added :");
            Program.Logger.warning("    > type          : " + warn.type.ToString());
            Program.Logger.warning("    > title         : " + warn.Title);
            Program.Logger.warning("    > message       : " + warn.Message);
            Program.Logger.warning("    > context       : " + warn.context.ToString());
        }

        #endregion WARNINGS EVENTS



        #region RENDER TASKMANAGER EVENTS
        
        void RenderTM_OnRun(object Sender, string command_exec)
        {
            Program.Logger.debug("Executing process : " + command_exec);
        }

        void RenderTM_OnInfo(object Sender, MediaAlliance.NewsMontage.RenderTM.Classes.CRenderOutputInfo output)
        {
            Program.Logger.info("RENDERTASK [" + output.TaskInfo.MyProcess.Id + "] : Info " + output.Message);

            frm_Loading.AddMessage(output.Message);
        }

        void RenderTM_OnProgress(object Sender, MediaAlliance.NewsMontage.RenderTM.Classes.CRenderOutputInfo output)
        {
            Program.Logger.verbose("RENDERTASK [" + output.TaskInfo.MyProcess.Id + "] : Progress " + output.ProgressValue);

            frm_Loading.Percent = output.ProgressValue;
        }

        void RenderTM_OnFinish(object Sender, MediaAlliance.NewsMontage.RenderTM.Classes.CRenderOutputInfo output, enRenderCmdType commandType)
        {
            if (commandType == enRenderCmdType.Render)
            {
                Program.Logger.info("RENDERTASK [" + output.TaskInfo.MyProcess.Id + "] : RENDER FINISHED");

                frm_Loading.Hide();
                MessageBox.Show(CDict.GetValue("MainForm.Mbox.RenderComplete", "Render complete!"));
            }
            else if (commandType == enRenderCmdType.ProfileList)
            {
                Program.Logger.info("RENDER PROFILES : ");
                foreach (string prof in RenderTM.Profiles)
                {
                    Program.Logger.info("   > " + prof);
                } 
            }
        }

        void RenderTM_OnVerboseInfo(object Sender, MediaAlliance.NewsMontage.RenderTM.Classes.CRenderOutputInfo output)
        {
            Program.Logger.verbose("RENDERTASK [" + output.TaskInfo.MyProcess.Id + "] : " + output.Message);
        }

        void RenderTM_OnEngineNotFound(object Sender)
        {
            Program.Logger.error("Render-Engine exe not found");
            warnings.AddWarn(WarnType.fileNotFound, "Render engine not found", "Render engine path is wrong or engine is not installed", WarnContext.ini_configuration);

            tsmAppAlert.Image = Properties.Resources.AlertAnimate;
            tsmAppAlert.Invalidate();

            exportToolStripMenuItem.Enabled = false;
        }


        #endregion RENDER TASKMANAGER EVENTS



        #region WAVEFORM MAKER EVENTS

        void waveform_maker_OnChangeWorking(object Sender, bool working)
        {
            pnlWaveWorking.Visible = working;
        }

        void waveform_maker_OnRendered(object Sender, CMxpTL_ClipInfo clip_info, CWaveFormMaker.CWaveFormClip wave_form, Bitmap bmp, int id)
        {

        }

        #endregion WAVEFORM MAKER EVENTS
        


        void thumbMaker_OnDone(object Sender, string thumbReady, long position, object tag)
        {
            if (tag is CMxpClip)
            {
                CMxpClip clip = (CMxpClip)tag;

                string file = thumbReady;// Path.Combine(thumbReady, "frame_sec" + position + "_000001.jpg");

                //Console.WriteLine(" ONDONE : " + clip.ID + " -> " + position + " -> (" + clip.ClippingIn + " - " + clip.ClippingOut + ")");

                if (File.Exists(file))
                {
                    //FileStream fs = new FileStream(thumbReady, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                    //clip.SetThumb(position * (int)mxptl.FPS, (Bitmap)Bitmap.FromStream(fs));
                    //fs.Close();
                    //fs.Dispose();
                    clip.SetThumb(position * (int)mxptl.FPS, file, MxpClip_ThumbPosition.Absolute);
                }

                clip.NotifyRender_NoEVents();
            }
        }



        #region PUBLIC METHODS

        public void StarupPage(bool exitOnCancel)
        {
            // MRU

            Program.Logger.info("OPENING STARTUP PAGE");

            if (startup_form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                #region OK ACTION

                switch (startup_form.StartUpAction)
                {
                    case NewsMontange.Forms.frmStartup.enStartUp_Action.newProject:
                        {
                            Program.Logger.info("   > New project choosed");

                            //string main_prj_folder = Path.Combine(projectHelper.ProjectPath, "SA_" + DateTime.Now.ToString("yyyyMMddTHHmmss"));

                            CProjectInfo proj = new CProjectInfo(projectHelper.ProjectPath, projectHelper);

                            proj.Project_Author = startup_form.txtAuthor.Text;
                            proj.Project_Show = startup_form.txtShow.Text;
                            proj.Project_StoryTitle = startup_form.txtStoryTitle.Text;
                            proj.Project_Title = startup_form.lblRealTitle.Text;
                            proj.Filename = Path.Combine(Path.Combine(projectHelper.ProjectPath, proj.ID), startup_form.txtTitle.Text + ".nmprj");

                            projectHelper.New(proj);
                            try
                            {
                                projectHelper.Save();
                            }
                            catch { }
                            projectHelper.UnloadAll();

                            ProjectInfoUC.ProjectInfo = projectHelper.CurrentProject;

                            mxptl.ZoomAll();
                        }
                        break;
                    case NewsMontange.Forms.frmStartup.enStartUp_Action.openRecent:
                        {
                            Program.Logger.info("   > Recent project choosed");

                            if (startup_form.RecentProject != null)
                            {
                                if (projectHelper.CurrentProject.Filename != "")
                                {
                                    if (MessageBox.Show(null,
                                                        CDict.GetValue("MainForm.Mbox.SaveOnClose", "Do you want to save current project before close it?"),
                                                        "Save current",
                                                        MessageBoxButtons.YesNo,
                                                        MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                                    {
                                        try
                                        {
                                            projectHelper.Save();
                                        }
                                        catch { }
                                        projectHelper.UnloadAll();
                                    }
                                }

                                projectHelper.Load(startup_form.RecentProject.Filename, true);


                                SRC_VOICE.Script = projectHelper.CurrentProject.Script;


                                ProjectInfoUC.ProjectInfo = projectHelper.CurrentProject;

                                Program.Logger.info("      > file : " + startup_form.RecentProject.Filename);
                            }
                        }
                        break;
                    case NewsMontange.Forms.frmStartup.enStartUp_Action.newsnetProject:
                        {
                            CProjectInfo proj = new CProjectInfo(projectHelper.ProjectPath, projectHelper);

                            projectHelper.UnloadAll();

                            projectHelper.New(proj);
                            projectHelper.NewsNet_Import(startup_form.NewsNet_Selected_Project);

                            string prj_name = Path.GetFileName(startup_form.NewsNet_Selected_Project);
                            prj_name = prj_name.Replace(Path.GetExtension(startup_form.NewsNet_Selected_Project), "");

                            proj.Filename = Path.Combine(Path.Combine(projectHelper.ProjectPath, proj.ID), prj_name + ".nmprj");


                            SRC_VOICE.Script = projectHelper.CurrentProject.Script;

                            try
                            {
                                projectHelper.Save();
                            }
                            catch { }
                            Program.Logger.info("   > NewsNet source choosed");

                            ProjectInfoUC.ProjectInfo = projectHelper.CurrentProject;
                        }
                        break;
                    case Forms.frmStartup.enStartUp_Action.customFile:
                        {
                            Program.Logger.info("   > Custom file open choosed");

                            if (projectHelper.CurrentProject.Filename != "")
                            {
                                if (MessageBox.Show(null,
                                                    CDict.GetValue("MainForm.Mbox.SaveOnClose", "Do you want to save current project before close it?"),
                                                    "Save current",
                                                    MessageBoxButtons.YesNo,
                                                    MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                                {
                                    try
                                    {
                                        projectHelper.Save();
                                    }
                                    catch { }
                                    projectHelper.UnloadAll();
                                }
                            }

                            projectHelper.Load(startup_form.CustomProjectFile, true);


                            SRC_VOICE.Script = projectHelper.CurrentProject.Script;

                            Program.Logger.info("      > file : " + startup_form.CustomProjectFile);

                            ProjectInfoUC.ProjectInfo = projectHelper.CurrentProject;
                        }
                        break;
                } 

                #endregion
            }
            else
            {
                Program.Logger.info("   > Operation cancelled");

                if(exitOnCancel) Environment.Exit(1);
            }
        }

        public void UpdateProjectDuration()
        { 
            CMxpClip[] lastest_clips = mxptl.LastestClips;

            if (lastest_clips != null && lastest_clips.Length > 0)
            {
                lblDuration.TimeCode = lastest_clips[0].TimcodeIn + lastest_clips[0].ClippingOut;
            }
            else
            {
                lblDuration.TimeCode = 0;
            }
        }

        public void AddStatus(params enApplicationStatus[] add_status)
        {
            enApplicationStatus new_status = appStatus;

            foreach (enApplicationStatus act in add_status)
            {
                new_status = new_status | act;
            }

            appStatus = new_status;
        }

        public void RemoveStatus(params enApplicationStatus[] remove_actions)
        {
            enApplicationStatus new_status = appStatus;

            foreach (enApplicationStatus act in remove_actions)
            {
                if ((new_status & act) == act)
                {
                    //if (OnExitedFromActions != null) OnExitedFromActions(this, act, objref);
                    new_status = new_status ^ act;
                }
            }

            appStatus = new_status;
        }

        public void StopVoiceRecording()
        {
            SRC_VOICE.STOP_RECORDING();
        }

        public void ChangedRenderProfile(object Sender, string profilename)
        {
            Program.Logger.info(" Render profile changed to : " + profilename);

            m_current_renderprofile = profilename;

            if (RenderTM != null)
            {
                RenderScanOrder = RenderTM.GetProfileInfo(profilename, "OutputScanOrderInfo");
                m_current_renderExtension = RenderTM.GetProfileInfo(profilename, "OutputExtension");
                Program.Logger.info(" Render profile OutputScanOrderInfo : " + RenderScanOrder);
            }
        }

        #endregion PUBLIC METHODS



        private void AddInfo(string message)
        {
            if (m_runningdebug)
            {
                frmdbg_internalLogs.lstInfos.Items.Insert(0, DateTime.Now.ToString("HH.mm.ss") + " : " + message);
                while (frmdbg_internalLogs.lstInfos.Items.Count > 200) frmdbg_internalLogs.lstInfos.Items.RemoveAt(frmdbg_internalLogs.lstInfos.Items.Count - 1);
            }
        }
        


        #region PROJECT HELPER

        void projectHelper_OnAddedSource(object Sender, CMxpTL_ClipInfo clipinfo, bool isProjectLoading)
        {
            //if (!m_censorStore.ContainsKey(clipinfo.filename))
            //{
            //    CTimelineMasks CTL = new CTimelineMasks();
            //    CTL.Duration = clipinfo.Tcout;
            //    CTL.AddLayer(CensorMaskType.pixelMed);
            //    CTLM_Keyframe kf0 = CTL[0].AddKeyframe(10);
            //    kf0.Bounds = new Rectangle(10, 10, 100, 100);

            //    m_censorStore.Add(clipinfo.filename, CTL);
            //}

            string type_str = (clipinfo.Tag is CProjectsHelper.enSouceType)? ((CProjectsHelper.enSouceType)clipinfo.Tag).ToString() : "Unknown";

            if (isProjectLoading)
            {
                Program.Logger.info("Loading project resource : ");
            }
            else
            {
                Program.Logger.info("Adding new resource to project : ");            
            }

            Program.Logger.info("   > Type              : " + type_str);
            Program.Logger.info("   > File              : " + clipinfo.filename);
            

            waveform_maker.ImageCachePath = projectHelper.CurrentProject.Folder_FrameCache;


            switch ((CProjectsHelper.enSouceType)clipinfo.Tag)
            {
                case CProjectsHelper.enSouceType.video:
                    {

                        if (clipinfo.Thumbnail == null)
                        {
                            // CONTROLLO SE CI SONO THUMB GENERATE

                            string thumbpath = Path.Combine(projectHelper.CurrentProject.Folder_FrameCache, Path.GetFileName(clipinfo.filename));

                            if (Directory.Exists(thumbpath))
                            {
                                string[] thumbs = Directory.GetFiles(thumbpath, "*.jpg");

                                if (thumbs.Length > 0)
                                {
                                    FileStream fs = new FileStream(thumbs[0], FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                                    clipinfo.Thumbnail = (Bitmap)Image.FromStream(fs);
                                    fs.Close();
                                    fs.Dispose();
                                }
                            }
                            else
                            {
                                SRC_VIDEO.CreateMainThumbnail(clipinfo);
                            }
                        }

                        try
                        {
                            int IN = SRC_VIDEO.ListSources0.Items.Count;

                            SRC_VIDEO.ListSources0.Items.Add(clipinfo);
                            SRC_VIDEO.ListSources0.Refresh();

                            IN = SRC_VIDEO.ListSources0.Items.Count;
                        }
                        catch (Exception ex)
                        { 
                        }
                    }
                    break;
                case CProjectsHelper.enSouceType.voice:
                    {
                        SRC_VOICE.ListSources0.Items.Add(clipinfo);
                        SRC_VOICE.ListSources0.Refresh();
                        waveform_maker.AddClip(clipinfo, 28000);
                    }
                    break;
                case CProjectsHelper.enSouceType.background:
                    {
                        SRC_BACKGROUND.ListSources0.Items.Add(clipinfo);
                        SRC_BACKGROUND.ListSources0.Refresh();
                        waveform_maker.AddClip(clipinfo, 28000);
                    }
                    break;
                default:
                    break;
            }
        }

        void projectHelper_OnRemovingSource(object Sender, CMxpTL_ClipInfo clipinfo, ref bool Cancel)
        {
            Program.Logger.info(" Removing source from project");
            Program.Logger.info("   > Source filename       : " + clipinfo.filename);
            Program.Logger.info("   > Source type           : " + ((CProjectsHelper.enSouceType)clipinfo.Tag).ToString());

            bool removed = false;

            switch ((CProjectsHelper.enSouceType)clipinfo.Tag)
            {
                case CProjectsHelper.enSouceType.video:
                    {
                        for (int i = 0; i < SRC_VIDEO.ListSources0.Items.Count; i++)
                        {
                            CMxpTL_ClipInfo cInfo = (CMxpTL_ClipInfo)SRC_VIDEO.ListSources0.Items[i];

                            if (cInfo.filename.Equals(clipinfo.filename, StringComparison.OrdinalIgnoreCase))
                            {
                                SRC_VIDEO.ListSources0.Items.RemoveAt(i);
                                removed = true;
                                break;
                            }
                        }
                    }
                    break;
                case CProjectsHelper.enSouceType.voice:
                    {
                        for (int i = 0; i < SRC_VOICE.ListSources0.Items.Count; i++)
                        {
                            CMxpTL_ClipInfo cInfo = (CMxpTL_ClipInfo)SRC_VOICE.ListSources0.Items[i];

                            if (cInfo.filename.Equals(clipinfo.filename, StringComparison.OrdinalIgnoreCase))
                            {
                                SRC_VOICE.ListSources0.Items.RemoveAt(i);
                                removed = true;
                                break;
                            }
                        }
                    }
                    break;
                case CProjectsHelper.enSouceType.background:
                    {
                        for (int i = 0; i < SRC_BACKGROUND.ListSources0.Items.Count; i++)
                        {
                            CMxpTL_ClipInfo cInfo = (CMxpTL_ClipInfo)SRC_BACKGROUND.ListSources0.Items[i];

                            if (cInfo.filename.Equals(clipinfo.filename, StringComparison.OrdinalIgnoreCase))
                            {
                                SRC_BACKGROUND.ListSources0.Items.RemoveAt(i);
                                removed = true;
                                break;
                            }
                        }
                    }
                    break;
            }
        }

        void projectHelper_OnUnloadedAll(object Sender)
        {
            SRC_VIDEO.ListSources0.Items.Clear();
            SRC_VOICE.ListSources0.Items.Clear();
            SRC_BACKGROUND.ListSources0.Items.Clear();
        }

        void projectHelper_OnSourceNotFound(object Sender, string filename)
        {
            if (frm_Loading.Visible)
            {
                frm_Loading.AddMessage("File not found : " + filename);
                Application.DoEvents();
            }

            warnings.AddWarn(WarnType.fileNotFound, "Resource not found", filename + " is not still available", WarnContext.project);

            Program.Logger.error("File not found : " + filename);
        }
        
        void projectHelper_OnStartLoading(object Sender, string OperationTitle)
        {
            tsmFieldsAlert.Visible = false;

            frm_Loading.Title = OperationTitle;
            frm_Loading.Percent = 0;
            frm_Loading.ClearMessages();
            frm_Loading.Show();
            Application.DoEvents();
            System.Threading.Thread.Sleep(40);
        }

        void projectHelper_OnProgress(object Sender, string step_message, int perc)
        {
            frm_Loading.AddMessage(step_message);
            frm_Loading.Percent = perc;
            Application.DoEvents();
        }

        void projectHelper_OnFinishLoading(object Sender)
        {
            System.Threading.Thread.Sleep(1500);
            frm_Loading.Hide();

            if (projectHelper.CurrentProject.ClipsNotFound.Count > 0)
            {
                tsmAppAlert.Image = Properties.Resources.AlertAnimate;
                MessageBox.Show(CDict.GetValue("MainForm.Mbox.SourceClipNotFound", "One or more clip/s source of project is not still available"));
                frm_WarningList.Show();
            }
            else
            {
                tsmAppAlert.Image = Properties.Resources.alert_red;
            }
        }

        void projectHelper_OnCurrentProjectChanged(object Sender, CProjectInfo project)
        {
            // CONFIGURO IL THUMBMAKER CON IL PERCORSO DEL PROGETTO
            thumbMaker.ProjectPath = project.Folder_FrameCache;
        }

        void RollbackManager_OnAddedBackup(object Sender, CRollbackInfo rb_info)
        {
            //Program.Logger.info("Rollback image added [" + rb_info.type.ToString() + "]");
            //Program.Logger.verbose("    > type          : " + rb_info.type.ToString());
            //Program.Logger.verbose("    > date          : " + rb_info.BackupDate.ToString());
            //Program.Logger.verbose("    > description   : " + rb_info.Description.ToString());
        }
        
        #endregion PROJECT HELPER



        #region TIMELINE EVENTS

        private void track_OnClipChangedTC(object Sender, CMxpClip clip, enTcChangeType cType)
        {
            long tcin   = clip.TimcodeIn + clip.ClippingIn;
            long tcout  = clip.TimcodeIn + clip.ClippingOut;

            lastEditedClip = clip;
        }

        private void mxptl_OnChangePosition(object Sender, long position, bool Internal)
        {
            OnChangePosition_Notifyed(Sender, position, Internal);
        }

        private void OnChangePosition_Notifyed(object Sender, long position, bool Internal)
        {
            Application.DoEvents();

            lblCurrentTC0.TimeCode = position;

            if (Internal)
            {
                #region INTERNAL

                // QUANDO IL CAMBIAMENTO VIENE FATTO DALL'OPERATORE SUL CONTROLLO

                if (mxptl.InternalEngine.EngineStatus == MediaAlliance.Controls.MxpTimeLine.Engine.InternalEngineStatus.play)
                {
                    mxptl.InternalEngine.Pause();
                }

                if (mxptl.CurrentOnlineClips.Count > 0)
                {
                    CMxpClip top_videoclip = null;
                    int trk = -1;

                    List<CMxpClip> olc = new List<CMxpClip>(mxptl.CurrentOnlineClips);

                    lock (olc)
                    {
                        foreach (CMxpClip cclip in olc)
                        {
                            CMxpTL_ClipInfo clip_info = cclip.ClipInfo;

                            if (cclip.Type == enCMxpClipType.Video && top_videoclip == null && !cclip.ParentTrack.Off)
                            {
                                // SE E DI TIPO VIDEO E LA PRIMA DALL'ALTRO LA SETTO ONLINE

                                top_videoclip = cclip;

                                if (clip_info != null)
                                {
                                    OnlineClip = cclip;

                                    trk = (top_videoclip.ParentTrack.ID == 1) ? 0 : 1;

                                    vp.SetPosition(trk, clip_info.file_id, (int)(position - top_videoclip.TimcodeIn));

                                    if (Program.Settings.Options.TIMELINE_AUDIOSCRUB)
                                    {
                                        vp.PlayScrub(trk, clip_info.file_id);
                                    }
                                    //vp.SetOnLine(trk, clip_info.file_id);
                                }
                            }

                            if ((mxptl.Actions & MxpTimelineAction.isZooming) != MxpTimelineAction.isZooming &&
                                (mxptl.Actions & MxpTimelineAction.isPanning) != MxpTimelineAction.isPanning)
                            {
                                if (cclip != top_videoclip)
                                {
                                    switch (cclip.Type)
                                    {
                                        case enCMxpClipType.Video:
                                            {
                                                trk = (cclip.ParentTrack.ID == 1) ? 0 : 1;

                                                vp.SetPosition(trk, clip_info.file_id, (int)(position - cclip.TimcodeIn));

                                                if (Program.Settings.Options.TIMELINE_AUDIOSCRUB)
                                                {
                                                    vp.PlayScrub(trk, clip_info.file_id);
                                                }
                                            }
                                            break;
                                        case enCMxpClipType.Audio:
                                            {
                                                if (cclip.Group.Count == 0)
                                                {
                                                    // CLIP AUDIO SU TRACCIA SINGOLA

                                                    vp.SetPosition(0, clip_info.file_id, (int)(position - cclip.TimcodeIn));

                                                    if (Program.Settings.Options.TIMELINE_AUDIOSCRUB)
                                                    {
                                                        vp.PlayScrub(0, clip_info.file_id);
                                                    }
                                                }
                                            }
                                            break;
                                    }
                                }
                            }

                        }
                    }
                }

                #endregion INTERNAL
            }
            else
            {
            }

            if (mxptl.CurrentOnlineClips.Count > 0)
            {
                // IN OGNI CASO

                foreach (CMxpClip audioclip in mxptl.CurrentOnlineClips.Where(x => x.Type == enCMxpClipType.Audio))
                {
                    // PER TUTTE LE CLIP DI TIPO AUDIO

                    if (audioclip.Group.Count > 0)
                    {
                        // SE SONO TRACCE AUDIO DI UN VIDEO

                        CMxpClip vclip = audioclip.Group[0];
                        CMxpTL_ClipInfo fclip = vclip.ClipInfo;

                        float volume = 0;

                        if (!audioclip.ParentTrack.Off)
                        {
                            volume = audioclip.GetAudioAt(position, true);

                            #if DEBUG
                            //Console.WriteLine("VALUE AT : " + db);
                            #endif
                        }

                        int trk = (vclip.ParentTrack.ID == 1) ? 0 : 1;
                        
                        if ((this.appStatus & enApplicationStatus.recordingAudio) == enApplicationStatus.recordingAudio)
                        {
                            volume = 0;
                        }

                        vp.SetVolume(trk, fclip.file_id, volume);

                    }
                    else
                    { 
                        // SE SONO TRACCE AUDIO SINGOLE

                        CMxpTL_ClipInfo fclip = audioclip.ClipInfo;

                        float volume = 0; 

                        if (!audioclip.ParentTrack.Off)
                        {
                            volume = audioclip.GetAudioAt(position, true);
                        }

                        if ((this.appStatus & enApplicationStatus.recordingAudio) == enApplicationStatus.recordingAudio)
                        {
                            volume = 0;
                        }

                        vp.SetVolume(0, fclip.file_id, volume);
                    }
                }
            }
        }

        private void mxptl_OnChangeMarker(object Sender, long FrameIn, long FrameOut, enTcChangeType change_type)
        {
            //switch (change_type)
            //{
            //    case enTcChangeType.timecode_in:
            //        OnChangePosition_Notifyed(Sender, FrameIn, true);
            //        break;
            //    case enTcChangeType.timecode_out:
            //        OnChangePosition_Notifyed(Sender, FrameIn, true);
            //        break;
            //}

            if (mxptl.IsMarked)
            {
                btnBlank.Enabled = true;
                btnClipRemover.Enabled = true;
            }
            else
            {
                btnBlank.Enabled = false;
                btnClipRemover.Enabled = false; 
            }
        }

        private void mxptl_OnOnlineItemsTableChanged(object Sender, long tc, List<CTimeTableItem> online_items)
        {
            try
            {
                if (online_items.Count > 0)
                {
                    bool hasVideoOnline = false;
                    bool isFirstVideoTrack = true;

                    foreach (CTimeTableItem item in online_items)
                    {
                        CMxpClip clip = (CMxpClip)item.Tag;

                        CMxpTL_ClipInfo fclip = clip.ClipInfo;

                        if (isFirstVideoTrack && clip.Type == enCMxpClipType.Video && !clip.ParentTrack.Off)
                        {
                            hasVideoOnline = true;

                            OnlineClip = clip;

                            int trk = (clip.ParentTrack.ID == 1) ? 0 : 1;
                            //vp.SetOnLine(trk, fclip.file_id);

                            isFirstVideoTrack = false;
                        }

                        if (mxptl.InternalEngine.EngineStatus == MediaAlliance.Controls.MxpTimeLine.Engine.InternalEngineStatus.play && clip.Type == enCMxpClipType.Video)
                        {
                            int trk = (clip.ParentTrack.ID == 1) ? 0 : 1;
                            vp.Play(trk, fclip.file_id);
                        }
                        else
                        {
                            //vp.SetPosition(0, fclip.idPreview, (int)(tc - clip.TimcodeIn));
                        }
                    }

                    if (!hasVideoOnline)
                    {
                        OnlineClip = null;
                    }
                    //TLMonitor.SetOnline(fclip.Filename, mxptl.InternalEngine.Status == MediaAlliance.Controls.MxpTimeLine.Engine.InternalEngineStatus.play);
                }
                else
                {
                    // SE LA TABELLA DELLE CLIP ATTUALMENTE ONLINE E' VUOTA

                    OnlineClip = null;
                }

                if (m_runningdebug) AddInfo("Online items : " + online_items.Count);
            }
            catch
            {
            }
        }

        private void mxptl_OnItemChangeOnlineStatus(object Sender, CTimeTableItem Item, bool online)
        {
            if (Item.Tag is CMxpClip)
            {
                CMxpClip clip = (CMxpClip)Item.Tag;
                CMxpTL_ClipInfo fClip = clip.ClipInfo;

                if (clip.Group.Count == 0 || clip.Type == enCMxpClipType.Video)
                {
                    if (online)
                    {
                        //Console.WriteLine(clip.ID + "-" + clip.ParentTrack.ID + " go online");
                        if (mxptl.InternalEngine.EngineStatus == MediaAlliance.Controls.MxpTimeLine.Engine.InternalEngineStatus.play)
                        {
                            int trk = 0;

                             if (clip.Type == enCMxpClipType.Video)
                            {
                                trk = (clip.ParentTrack.ID == 1) ? 0 : 1;
                            }


                            vp.Play(trk, fClip.file_id);
                        }
                    }
                    else
                    {
                        int trk = 0;

                        if (clip.Type == enCMxpClipType.Video)
                        {
                            trk = (clip.ParentTrack.ID == 1) ? 0 : 1;
                        }

                        long tc_diff = 100000;
                        CMxpClip next_clip = clip.GetNextClip(out tc_diff);


                        if (next_clip != null && tc_diff < 2 && next_clip.ClipInfo == clip.ClipInfo)
                         {
                            // FIX : PER LE TRACCE SINGOLE, SE DUE SPEZZONI DELLA STESSA CLIP SONO TROPPO VIGINI
                            // NON RIESCE A METTERE IN PAUSA FARE IL SEEK E RIPARTIRE, QUINDI EVITO DI FARE LA 
                            // PAUSA NEL CASO SIANO A DISANZA UGUALE O INFERIORE A 2 FRAME
                        }
                        else
                        {
                             vp.Pause(trk, fClip.file_id);
                            Console.WriteLine("PAUSE " + clip.ID + " ON_CHANGE_STATUS");
                        }
                    }
                }
            }
        }

        private void mxptl_OnTimecodeEvent(object Sender, long tc, CTimeTableItem item, enTimeTableItemType itemposition)
        {
            string arg = item.Tag.GetType().ToString();

            if (item.Tag is CMxpClip)
            {
                arg = "CLIP " + ((CMxpClip)item.Tag).ParentTrack.ID + ((CMxpClip)item.Tag).ID;
            }

            switch (itemposition)
            {
                case enTimeTableItemType.uncknow:
                    break;
                case enTimeTableItemType.item_start:
                    {
                        if(m_runningdebug) AddInfo("START " + arg);

                        CMxpClip clip = (CMxpClip)item.Tag;

                        CMxpTL_ClipInfo fClip = clip.ClipInfo;

                        if (clip.Group.Count == 0 || clip.Type == enCMxpClipType.Video)
                        {
                            int trk = 0;

                            if (clip.Type == enCMxpClipType.Video)
                            {
                                trk = (clip.ParentTrack.ID == 1) ? 0 : 1;
                            }
                            vp.SetPosition(trk, fClip.file_id, (int)(mxptl.CurrentPosition - clip.TimcodeIn));

                            if (mxptl.InternalEngine.EngineStatus == MediaAlliance.Controls.MxpTimeLine.Engine.InternalEngineStatus.play)
                            {
                                vp.Play(trk, fClip.file_id);
                                Console.WriteLine("PLAYT " + clip.ID + " ON_TimecodeEvent");
                            }
                        }
                    }
                    break;
                case enTimeTableItemType.item_end:
                    if (item.Tag is CMxpClip)
                    {
                        CMxpClip clip = (CMxpClip)item.Tag;
                        CMxpTL_ClipInfo fClip = ((CMxpClip)item.Tag).ClipInfo;

                        //if (clip.Group.Count == 0 || clip.Type == enCMxpClipType.Video)
                        //{
                        //    int trk = 0;

                        //    if (clip.Type == enCMxpClipType.Video)
                        //    {
                        //        trk = (clip.ParentTrack.ID == 1) ? 0 : 1;
                        //    }

                        //    vp.Pause(trk, fClip.file_id);
                        //}
                        //mxptl.InternalEngine.Pause();
                    }

                    if (m_runningdebug) AddInfo("END   " + arg);
                    break;
                case enTimeTableItemType.item_inside:
                    break;
                default:
                    break;
            }
        }

        private void mxptl_OnInternalEngineEvent(object Sender, MediaAlliance.Controls.MxpTimeLine.Engine.InternEngineEventHandle e)
        {
            switch (e.Type)
            {
                case MediaAlliance.Controls.MxpTimeLine.Engine.enIEngineEventType.onPlay:
                    if (SourceTimeline != null)
                    {
                        if (SourceTimeline.InternalEngine.EngineStatus == InternalEngineStatus.play)
                        {
                            // EVITO IL PLAY CONTEMPORANEO SULLE DUE TIMELINE
                            SourceTimeline.InternalEngine.Pause();
                        }
                    }

                    foreach (CMxpClip clip in mxptl.CurrentOnlineClips)
                    {
                        if (clip.Group.Count == 0 || clip.Type == enCMxpClipType.Video)
                        {
                            // TUTTE LE CLIP SOLO AUDIO O QUELLE VIDEO DI TRACCE AUDIOVIDEO

                            CMxpTL_ClipInfo fClip = clip.ClipInfo;

                            int trk = 0;

                            if (clip.Type == enCMxpClipType.Video)
                            {
                                trk = (clip.ParentTrack.ID == 1) ? 0 : 1;
                            }
                            
                            if ((this.appStatus & enApplicationStatus.recordingAudio) == enApplicationStatus.recordingAudio)
                            {
                                vp.SetVolume(trk, fClip.file_id, 0);
                            }

                            vp.SetPosition(trk, fClip.file_id, (int)(mxptl.CurrentPosition - clip.TimcodeIn));
                            vp.Play(trk, fClip.file_id);
                        }


                        //if ((this.appStatus & enApplicationStatus.recordingAudio) == enApplicationStatus.recordingAudio)
                        //{
                        //    CMxpTL_ClipInfo clip_info = clip.ClipInfo;
                        //    vp.SetVolume(
                        //}
                    }
                    break;
                case MediaAlliance.Controls.MxpTimeLine.Engine.enIEngineEventType.onPause:
                    foreach (CMxpClip clip in mxptl.CurrentOnlineClips)
                    {
                        if (clip.Group.Count == 0 || clip.Type == enCMxpClipType.Video)
                        {
                            CMxpTL_ClipInfo fClip = clip.ClipInfo;

                            int trk = 0;

                            if (clip.Type == enCMxpClipType.Video)
                            {
                                trk = (clip.ParentTrack.ID == 1) ? 0 : 1;
                            }

                            vp.Pause(trk, fClip.file_id);
                        }
                    }
                    break;
            }
        }

        private void mxptl_OnChangeClipSelection(object Sender, CMxpClip[] SelectedClips)
        {
            if (SelectedClips.Length > 0)
            {
                if (frm_ClipInfo != null && frm_ClipInfo.Visible)
                {
                    frm_ClipInfo.Clip = SelectedClips[0];
                }
            }
            //lstSelection.Items.Clear();

            //foreach (CMxpClip clip in SelectedClips)
            //{
            //    AddInfo("Selezionata clip " + clip.ID + " su track : " + clip.ParentTrack.ID);
            //    lstSelection.Items.Add(clip.ParentTrack.ID + " - " + clip.ID);
            //}
        }

        private void mxptl_OnEndingClipEditing(object Sender, CMxpClip Clip, ref bool Cancel)
        {
            if (m_runningdebug) AddInfo("Editing clip : " + Clip.ID);
        }

        private void mxptl_OnExitedFromActions(object Sender, MxpTimelineAction Action, object objref)
        {
            switch (Action)
            {
                case MxpTimelineAction.isZooming:
                    break;
                case MxpTimelineAction.isPanning:
                    break;
                case MxpTimelineAction.isEditingClipIn:
                case MxpTimelineAction.isEditingClipOut:
                    {
                        CMxpClip workinclip = mxptl.SelectedClip;
                        if (objref is CMxpClip) workinclip = (CMxpClip)objref;

                        if (workinclip == null) return;

                        #region EXIT FROM EDIT TCIN OR TCOUT

                        // USCENDO DALL'EDITING DI MARK-IN O MARK-OUT DI UNA CLIP
                        // AGGIORNO LE THUMB RAPPRESENTATE GRAFICAMENTE
                        
                        if (workinclip.Type == enCMxpClipType.Audio)
                        {
                            // SE LA CLIP MODIFICATA E' DI TIPO AUDIO
                            if (workinclip.Group.Count > 0)
                            {
                                // SE LA CLIP E' COLLEGATA AD UNA CLIP VIDEO
                                // ALTRIMENTI SCARTO LA RICHIESTA
                                workinclip = (workinclip.Group[0].Type == enCMxpClipType.Video) ? workinclip.Group[0] : null;
                            }
                            else
                            {
                            }
                        }
                        else if (workinclip != null)
                        {
                            CMxpTL_ClipInfo fclip = workinclip.ClipInfo;
                            
                            #region THUMB

                            int second = 0;

                            if (Action == MxpTimelineAction.isEditingClipIn)
                            {
                                second = (int)(workinclip.ClippingIn) / (int)mxptl.FPS;
                            }
                            else
                            {
                                second = (int)(workinclip.ClippingOut) / (int)mxptl.FPS;
                            }

                            if (!workinclip.FrameThumbs.Contain_TC(second * (int)mxptl.FPS))
                            {
                                string thumbpath = Path.Combine(projectHelper.CurrentProject.Folder_FrameCache, Path.GetFileName(fclip.filename));

                                if (fclip.filename.Trim() != "" && workinclip.Type == enCMxpClipType.Video)
                                {
                                    if (!Directory.Exists(thumbpath)) Directory.CreateDirectory(thumbpath);

                                    string thumb_filename = "";

                                    // SE LA THUMB E' GIA' SALVATA SU DISCO LA CARICO
                                    if (thumbMaker.ExtistThumbFor(projectHelper.CurrentProject.Folder_FrameCache,
                                                                    fclip.filename,
                                                                    second,
                                                                    1,
                                                                    out thumb_filename))
                                    {
                                        workinclip.SetThumb(second * (int)mxptl.FPS, 
                                                            thumb_filename,
                                                            (Action == MxpTimelineAction.isEditingClipIn)? MxpClip_ThumbPosition.Begining : MxpClip_ThumbPosition.Ending);
                                        workinclip.NotifyRender();
                                    }
                                    // ALTRIMENTI RICHIEDO AL THUMBMEKER DI GENERARLA
                                    else
                                    {
                                        thumbMaker.AddRequest(fclip.filename, thumbpath, second, workinclip);
                                    }
                                }
                            }

                            #endregion THUMB
                        }

                        #endregion EXIT FROM EDIT TCIN
                    }
                    break;
                case MxpTimelineAction.isMarkingIn:
                case MxpTimelineAction.isMarkingOut:
                    //OnChangePosition_Notifyed(Sender, mxptl.CurrentPosition, true);
                    break;
                case MxpTimelineAction.isScrubbing:
                    break;
            }
        }

        private void mxptl_OnClipAdded(object Sender, CMxpClip clip)
        {
            if (clip.Type == enCMxpClipType.Audio)
            {
                if (clip.ParentTrack.ID == 0 || clip.ParentTrack.ID == 5)
                {
                    // VOICE && BACKGROUND CLIP

                    CMxpTL_ClipInfo clip_info = clip.ClipInfo;
                    CWaveFormMaker.CWaveFormClip wave_info = waveform_maker[clip_info];

                    if (wave_info != null)
                    {
                        clip.BackgraoundImage = waveform_maker.getWaveForm(clip_info, clip.ClippingIn, clip.ClippingOut);
                    }
                }
                else if (clip.ParentTrack.ID == 2 || clip.ParentTrack.ID == 4)
                {
                    // AUDIO TRACK OF VIDEOCLIP

                }
            }
            else if (clip.Type == enCMxpClipType.Video)
            {
                #if FIELDCHECK
                
                clip.InvertedFieldOrder = HasInvertedField(clip);

                if (clip.InvertedFieldOrder && !tsmFieldsAlert.Visible) tsmFieldsAlert.Visible = true;

                #endif
            }
        }

        private void mxptl_OnClipRemoving(object Sender, CMxpClip clip)
        {
            if (clip.Type == enCMxpClipType.Video)
            {
                if (clip.InvertedFieldOrder)
                {
                    // SE LA CLIP AVEVA I FIELD INVERTITI RISPETTO IL PROGETTO RICONTROLLO TUTTE LE CLIP
                    // PER AGGIORNARE LO STATO DELL'ALLERT GRAFICO

                    #if FIELDCHECK

                    tsmFieldsAlert.Visible = false;
                    
                    foreach (CMxpClip cclip in mxptl.Tracks.AllVideoClips)
                    {
                        if (cclip.InvertedFieldOrder)
                        {
                            tsmFieldsAlert.Visible = true;
                            break;
                        }
                    }

                    #endif
                }
            }
        }

        private void mxptl_OnClipDisplayInfoChanged(object Sender, CMxpClip clip)
        {
            // VOICE || BACKGROUND TRACKS
            
            #region WAVEFORM

            if (clip.ParentTrack.ID == 0 || clip.ParentTrack.ID == 5)
            {
                //Console.WriteLine("MARGIN -> " + clip.DisplayInfo.FirstVisible_TC_OfClip.ToString().PadLeft(10) + " <- " + clip.DisplayInfo.LastVisible_TC_OfClip);
                //return;
                ////clip = mxptl.SelectedClip;

                if (clip.DisplayInfo.VisibleWidth > 0)
                {
                    CMxpTL_ClipInfo clip_info = clip.ClipInfo;

                    clip_info.Clipping_in = clip.ClippingIn;
                    clip_info.Clipping_out = clip.ClippingOut;

                    CWaveFormMaker.CWaveFormClip wave_info = waveform_maker[clip_info];

                    if (wave_info != null)
                    {
                        Bitmap wfb = waveform_maker.getWaveForm(clip_info,
                                                                clip.DisplayInfo.FirstVisible_TC_OfClip,// clip.ClippingIn,
                                                                clip.DisplayInfo.LastVisible_TC_OfClip);// clip.ClippingOut);
                        if (wfb == null) return;

                        int pic_width = clip.Virtual_Width + clip.Virtual_X;
                        if (clip.Virtual_X > 0) pic_width = clip.Virtual_Width;
                        pic_width = clip.DisplayInfo.VisibleWidth;
                        if (pic_width > 0)
                        {
                            Bitmap dest = new Bitmap(pic_width, clip.ParentTrack.Height);

                            //Console.WriteLine("clip.DisplayInfo.VisibleWidth : " + clip.DisplayInfo.VisibleWidth);

                            Graphics grf = Graphics.FromImage(dest);
                            grf.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                            grf.DrawImage(wfb,
                                            new Rectangle(0, 0, dest.Width, dest.Height),
                                            new Rectangle(0, 0, wfb.Width, wfb.Height),
                                            GraphicsUnit.Pixel);

                            if (m_runningdebug)
                            {
                                grf.FillRectangle(Brushes.Black, new Rectangle(0, 0, 10, 10));
                                grf.FillRectangle(Brushes.Black, new Rectangle(dest.Width - 10, 0, 10, 10));
                            }

                            grf.Dispose();
                            wfb.Dispose();

                            clip.BackgraoundImage = dest;
                        }
                    }
                }
            }

            #endregion WAVEFORM
        }


        
        private void mxptl_OnAdvanceDraggingEvent(object Sender, IDataObject DragData, CMxpTrack over_track, ref int[] excluded_tracks, ref long rappresent_lenght, ref bool cancel)
        {
            // EVENTO SCATENATO DURANTE IL DRAG DI UN OGGETTO SOPRA LA TIMELINE
            // QUESTO EVENTO RIPORTA INFORMAZIONI SPECIFICHE DELLA TIMELINE OLTRE ALL'OGGETTO DragData ORIGINALE DEL DRAG
            //
            // over_track               : Traccia sotto il cursore del mouse
            // rappresentation_length   : Valore da settare per evidenziare la durata dell'oggetto draggato
            // excluded_tracks          : int[] con le tracce da non considerare valide al drop

            if (DragData.GetData(typeof(CMxpTL_ClipInfo)) != null)
            {
                CMxpTL_ClipInfo fClip = (CMxpTL_ClipInfo)DragData.GetData(typeof(CMxpTL_ClipInfo));

                if (fClip.Tag is CProjectsHelper.enSouceType)
                {
                    // DRAG DI OGGETTI DALLE SOURCES

                    CProjectsHelper.enSouceType src_type = (CProjectsHelper.enSouceType)fClip.Tag;

                    switch (src_type)
                    {
                        case CProjectsHelper.enSouceType.video:
                            excluded_tracks = new int[] { 0, 2, 4, 5 };
                            break;
                        case CProjectsHelper.enSouceType.voice:
                            excluded_tracks = new int[] { 1, 2, 3, 4, 5 };
                            break;
                        case CProjectsHelper.enSouceType.background:
                            excluded_tracks = new int[] { 0, 1, 2, 3, 4 };
                            break;
                        default:
                            break;
                    }

                    if (fClip.IsFullClip)
                    {
                        rappresent_lenght = fClip.Tcout;
                    }
                    else
                    {
                        rappresent_lenght = fClip.Clipping_out - fClip.Clipping_in;
                        Console.WriteLine("rappresent_lenght : " + rappresent_lenght + "        ( " + fClip.Clipping_out + " - " + fClip.Clipping_in + " )");
                    }
                }
                cancel = false;
            }
        }
        
        private void mxptl_OnAdvenceDragDrop(object Sender, DragEventArgs e, Point ManagedDrop)
        {
            try
            {
                Point tlp = mxptl.PointToClient(new Point(ManagedDrop.X, ManagedDrop.Y));

                if (tlp.X < 0) tlp.X = 0;

                CMxpTrack trk = mxptl.Tracks.GetTrackAt(tlp.X, tlp.Y);

                if (trk != null)
                {
                    CMxpTL_ClipInfo clip_info = (CMxpTL_ClipInfo)e.Data.GetData(typeof(CMxpTL_ClipInfo));

                    if (clip_info != null)
                    {
                        // AGGIUNGO LA CLIP AL GESTORE PREVIEW DELLA TIMELINE
                        clip_info.file_id = vp.Add(clip_info.filename, 1, clip_info);

                        if (clip_info.Tag is CProjectsHelper.enSouceType)
                        {
                            // DROP DI OGGETTI DALLE SOURCES

                            CProjectsHelper.enSouceType src_type = (CProjectsHelper.enSouceType)clip_info.Tag;

                            mxptl.SaveUndo();

                            switch (src_type)
                            {
                                case CProjectsHelper.enSouceType.video:
                                    {
                                        #region VIDEO CLIP

                                        if (clip_info.IsFullClip)
                                        {
                                            long tc = mxptl.GetTimecodeAt(tlp.X);

                                            CMxpClip clipV = mxptl.Tracks[trk.ID].AddClip("",
                                                                                            enCMxpClipType.Video,
                                                                                            clip_info,
                                                                                            null,
                                                                                            tc, 
                                                                                            tc + clip_info.Tcout,
                                                                                            0,
                                                                                            clip_info.Tcout);

                                            CMxpClip clipA = mxptl.Tracks[trk.ID + 1].AddClip("",
                                                                                            enCMxpClipType.Audio,
                                                                                            clip_info,
                                                                                            null,
                                                                                            tc,
                                                                                            tc + clip_info.Tcout,
                                                                                            0,
                                                                                            clip_info.Tcout);


                                            clipV.Group.Add(clipA);
                                        }
                                        else
                                        {
                                            long tc = mxptl.GetTimecodeAt(tlp.X);

                                            CMxpClip clipV = mxptl.Tracks[trk.ID].AddClip("",
                                                                                            enCMxpClipType.Video,
                                                                                            clip_info.Copy(),
                                                                                            null,
                                                                                            tc - clip_info.Clipping_in,
                                                                                            tc - clip_info.Clipping_in + clip_info.Tcout,
                                                                                            clip_info.Clipping_in,
                                                                                            clip_info.Clipping_out);

                                            CMxpClip clipA = mxptl.Tracks[trk.ID + 1].AddClip("",
                                                                                            enCMxpClipType.Audio,
                                                                                            clip_info.Copy(),
                                                                                            null,
                                                                                            tc - clip_info.Clipping_in,
                                                                                            tc - clip_info.Clipping_in + clip_info.Tcout,
                                                                                            clip_info.Clipping_in,
                                                                                            clip_info.Clipping_out);


                                            clipV.Group.Add(clipA);
                                        }

                                        #endregion VIDEO CLIP
                                    }
                                    break;
                                case CProjectsHelper.enSouceType.voice:

                                    #region VOICE CLIP

                                    if (clip_info.IsFullClip)
                                    {
                                        long tc = mxptl.GetTimecodeAt(tlp.X);

                                        CMxpClip clipA = mxptl.Tracks[trk.ID].AddClip("",
                                                                                        enCMxpClipType.Audio,
                                                                                        clip_info,
                                                                                        null,
                                                                                        tc,
                                                                                        tc + clip_info.Tcout,
                                                                                        0,
                                                                                        clip_info.Tcout);
                                    }
                                    else
                                    {
                                        long tc = mxptl.GetTimecodeAt(tlp.X);
                                        CMxpClip clipA = mxptl.Tracks[trk.ID].AddClip("",
                                                                                        enCMxpClipType.Audio,
                                                                                        clip_info,
                                                                                        null,
                                                                                        tc - clip_info.Clipping_in,
                                                                                        tc - clip_info.Clipping_in + clip_info.Tcout,
                                                                                        clip_info.Clipping_in,
                                                                                        clip_info.Clipping_out);
                                    }

                                    #endregion VOICE CLIP

                                    break;
                                case CProjectsHelper.enSouceType.background:

                                    #region BACKGROUND CLIP

                                    if (clip_info.IsFullClip)
                                    {
                                        long tc = mxptl.GetTimecodeAt(tlp.X);

                                        CMxpClip clipA = mxptl.Tracks[trk.ID].AddClip("",
                                                                                        enCMxpClipType.Audio,
                                                                                        clip_info,
                                                                                        null,
                                                                                        tc,
                                                                                        tc + clip_info.Tcout,
                                                                                        0,
                                                                                        clip_info.Tcout);
                                    }
                                    else
                                    {
                                        long tc = mxptl.GetTimecodeAt(tlp.X);
                                        CMxpClip clipA = mxptl.Tracks[trk.ID].AddClip("",
                                                                                        enCMxpClipType.Audio,
                                                                                        clip_info,
                                                                                        null,
                                                                                        tc - clip_info.Clipping_in,
                                                                                        tc - clip_info.Clipping_in + clip_info.Tcout,
                                                                                        clip_info.Clipping_in,
                                                                                        clip_info.Clipping_out);
                                    }

                                    #endregion BACKGROUND CLIP

                                    break;
                                default:
                                    break;
                            }


                            //clip.SetThumb(0, (Bitmap)pc.Image);
                            //clip.SetThumb(clip_out, (Bitmap)pc.Image);
                            //clip.SetThumb(clip_duration, (Bitmap)pc.Image);
                        }
                    }
                }

                //SourceTimeline.HideMarks();
            }
            catch { }
        }

        
       
        private void mxptl_OnMouseWheel(object Sender, MouseEventArgs e)
        {
            this.Text = e.Delta.ToString();
        }
        
        private void mxptl_OnZoomChanged(object Sender, long ZoomIn, long ZoomOut)
        {
            //foreach (CMxpClip audio_clip in mxptl.Tracks[0].Clips)
            //{
            //    if (audio_clip.IsVisible)
            //    {
            //        CMxpTL_ClipInfo clip_info = null;
            //        WaveFile wf = null;
                    
            //        if(audio_clip.Tag is WaveFile)
            //        {
            //            wf = (WaveFile)audio_clip.Tag;
            //        }
            //        else
            //        {
            //            clip_info = audio_clip.ClipInfo;

            //            if (clip_info.AudioClass != null && clip_info.AudioClass is WaveFile)
            //            {
            //                wf = (WaveFile)clip_info.AudioClass;
            //            }
            //        }

            //        if(wf != null)
            //        {
            //            if (audio_clip.DisplayInfo.VisibleWidth > 0)
            //            {
            //                Bitmap wfb = new Bitmap(audio_clip.DisplayInfo.VisibleWidth, audio_clip.Height);

            //                int precision = 3000;
            //                //if (mxptl.IsOnAction(MxpTimelineAction.isPanning)) precision = 2000;

            //                wf.Draw_async(wfb,
            //                                new Pen(new SolidBrush(Color.FromArgb(134, 134, 134)), 1),
            //                                audio_clip.DisplayInfo.FirstVisible_TC * 40,
            //                                audio_clip.DisplayInfo.LastVisible_TC * 40,
            //                                precision,
            //                                false,
            //                                audio_clip);
            //            }
            //        }
            //    }
            //}
            //foreach (CMxpClip audioclip in mxptl.Tracks.AllAudioClips)
            //{
            //    if (audioclip.IsVisible && audioclip.Tag is WaveFile)
            //    {
            //    }
            //}

        }

        private void mxptl_OnChangeActiveActions(object Sender, MxpTimelineAction Actions)
        {
            //if ((Actions & MxpTimelineAction.isEditingClipIn) == MxpTimelineAction.isEditingClipIn)
            //{
            //    Console.WriteLine("IS EDITING IN");
            //}
            //else
            //{
            //    Console.WriteLine("IS NOT EDITING IN");
            //}

        }

        private void mxptl_OnOnlineItemsTableChanged(object Sender, long tc)
        {

        }

        private void mxptl_OnUndoEvent(object Sender, UndoEventHandler e)
        {
            switch (e.type)
            {
                case UndoEventType.unknown:
                    break;
                case UndoEventType.newSnapshowTaken:
                    #if DEBUG
                    Console.WriteLine("[NEW UNDO POINT]");
                    #endif
                    
                    Program.Logger.debug("[NEW UNDO POINT]");

                    break;
                case UndoEventType.undoDone:
                    #if DEBUG
                    Console.WriteLine("[UNDO DONE] > Remaining undo image : " + e.UndoManager.RemainingUndo);
                    #endif

                    Program.Logger.info("[UNDO DONE] > Remaining undo image : " + e.UndoManager.RemainingUndo);

                    break;
                case UndoEventType.redoDone:
                    break;
                default:
                    break;
            }
        }

        #endregion TIMELINE EVENTS



        #region PRIVATE PROPERTIES

        private CMxpClip OnlineClip
        {
            get { return onlineClip; }
            set
            {
                if (onlineClip != value)
                {
                    onlineClip = value;

                    if (onlineClip == null)
                    {
                        vp.SetAllOffline();
                        lblCurrentClip.Text = "no clip";
                    }
                    else
                    {
                        CMxpTL_ClipInfo fclip = onlineClip.ClipInfo;
                        lblCurrentClip.Text = Path.GetFileName(fclip.filename);

                        int trk = (onlineClip.ParentTrack.ID == 1) ? 0 : 1;

                        lblOnlineTrack.Text = trk.ToString();

                        vp.SetOnLine(trk, fclip.file_id);
                    }
                }
            }
        }

        #endregion PRIVATE PROPERTIES



        #region PUBLIC PROPERTIES

        public enFocusedArea FocusedArea
        {
            get { return focusedArea; }
            set
            {
                if (focusedArea != value)
                {
                    Control ctl_new = null;
                    Control ctl_old = null;

                    foreach (KeyValuePair<Control, enFocusedArea> areas in app_areas)
                    {
                        if (areas.Value == value)
                        {
                            ctl_new = areas.Key;
                            if (ctl_new is MA_NmPanel) ((MA_NmPanel)ctl_new).IsFocused = true;
                            break;
                        }
                    }

                    foreach (KeyValuePair<Control, enFocusedArea> areas in app_areas)
                    {
                        if (areas.Value == focusedArea)
                        {
                            ctl_old = areas.Key;
                            if (ctl_old is MA_NmPanel) ((MA_NmPanel)ctl_old).IsFocused = false;
                            break;
                        }
                    }


                    focusedArea = value;
                }
            }
        }

        public MxpTmeLine SourceTimeline
        {
            get
            {
                Control ctl = lblGeneratingWaves.Controls[0];

                FieldInfo pInfo = ctl.GetType().GetField("Timeline");

                if (pInfo != null)
                {
                    return (MxpTmeLine)pInfo.GetValue(ctl);
                }

                return null;
            }
        }

        public enApplicationStatus AppStatus
        {
            get { return appStatus; }
            set
            {
                appStatus = value;
            }
        }

        public string RenderScanOrder
        {
            get { return m_current_renderscanorder; }
            set
            {
                m_current_renderscanorder = value;

                #if FIELDCHECK
                
                tsmFieldsAlert.Visible = false;

                foreach (CMxpClip clip in mxptl.Tracks.AllVideoClips)
                {
                    bool invert = HasInvertedField(clip);
                    clip.InvertedFieldOrder = invert;
                    if (invert && !tsmFieldsAlert.Visible) tsmFieldsAlert.Visible = true;
                }

                #endif
            }
        }

        public VideoPreview Videopreview
        {
            get { return vp; }
        }

        #endregion PUBLIC PROPERTIES



        #region MENU CLIPS EVENTS

        private void mnuClip_Opening(object sender, CancelEventArgs e)
        {
            if (mxptl.SelectedClip != null && mxptl.SelectedClip.Type == enCMxpClipType.Audio)
            {
                autoFadeAudioToolStripMenuItem.Visible = true;
            }
            else
            {
                autoFadeAudioToolStripMenuItem.Visible = false;
            }
            //ToolStripMenuItem me = (ToolStripMenuItem)sender;

            //CMxpClip clip = (CMxpClip)((Control)me.GetCurrentParent()).Tag;

            //autoFadeAudioToolStripMenuItem.Visible = (clip.Type == enCMxpClipType.Audio);
        }        

        private void eliminaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem me = (ToolStripMenuItem)sender;

            CMxpClip clip = (CMxpClip)((Control)me.GetCurrentParent()).Tag;


            if (Program.Settings.GUI_Options.DEL_CONFIRM)
            {
                Program.Logger.info("Asking confirmation about Remove clip " + mxptl.SelectedClip.ID);

                if (MessageBox.Show(null,
                                    CDict.GetValue("MainForm.Mbox.ConfirmRemoveClip", "Are you sure to remove selected clip?"),
                                    "Continue",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question,
                                    MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                {
                    Program.Logger.info("   > Operation aborted");
                    return;
                }
            }
            
            Program.Logger.info("   > Operation confirmed");
            
            mxptl.SaveUndo();

            CMxpClip[] clips = mxptl.SelectedClip.GetGroup();

            foreach (CMxpClip sclip in clips) sclip.Delete();
        }

        private void clipInfoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem me = (ToolStripMenuItem)sender;

            CMxpClip clip = (CMxpClip)((Control)me.GetCurrentParent()).Tag;

            if (frm_ClipInfo == null) frm_ClipInfo = new frmClipInfo(mxptl.FPS);

            frm_ClipInfo.Clip       = clip;
            frm_ClipInfo.Visible    = true;
            frm_ClipInfo.BringToFront();
        }

        private void autoFadeAudioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem me = (ToolStripMenuItem)sender;

            CMxpClip clip = (CMxpClip)((Control)me.GetCurrentParent()).Tag;

            clip.AutoFadeAudio();
        }

        #endregion MENU CLIPS EVENTS
        


        #region MAINMENU ITEMS EVENTS

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mxptl.Undo();
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mxptl.Redo();
        }

        private void startupMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StarupPage(false);
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                projectHelper.Save();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Program.Logger.error(" [saveToolStripMenuItem_Click] Error : "+ ex.Message);
            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFilesource.Title    = "Open an existing project";
            openFilesource.Filter   = "NMProject|*.nmprj";

            if (openFilesource.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                projectHelper.Load(openFilesource.FileName, true);
            }
        }

        private void internalLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmdbg_internalLogs.Visible = !frmdbg_internalLogs.Visible;
        }

        private void recoverBlankOnStartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CMxpClip first = null;

            foreach (CMxpClip clip in mxptl.Tracks.AllClips)
            {
                if (first == null)
                {
                    first = clip;
                }
                else if(first.ClippingIn + first.TimcodeIn > clip.ClippingIn + clip.TimcodeIn)
                {
                    first = clip;
                }
            }

            if (first != null)
            {
                mxptl.SaveUndo();

                long diff = first.ClippingIn + first.TimcodeIn;

                foreach (CMxpClip clip in mxptl.Tracks.AllClips)
                {
                    if (clip.Group.Count == 0 || clip.Type == enCMxpClipType.Video)
                    {
                        clip.TimcodeIn -= diff;
                    }
                }
            }
        }

        private void rollbackToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRollback frm = new frmRollback();
            frm.ProjectHelper = this.projectHelper;

            frm.ShowDialog();
        }

        private void mediaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.Logger.info("Rendering request");

            projectHelper.RollbackManager.Addbackup("Before render to media-file", enRollbackType.asRendered);

            frm_Loading.Title = CDict.GetValue("LoadingForm.Rendering", "Rendering");
            frm_Loading.ClearMessages();
            frm_Loading.Percent = 0;
            frm_Loading.Show();

            Application.DoEvents();

            // CONTROLLO E CREO LA CARTELLA DI DESTINAZIONE DEI RENDERS

            if (!Directory.Exists(Program.Settings.RENDER_Options.EXPORT_FIX_PATH))
            {
                try
                {
                    Directory.CreateDirectory(Program.Settings.RENDER_Options.EXPORT_FIX_PATH);
                }
                catch (System.Exception ex)
                {
                    Program.Logger.error("   > Error creating destination folder : " + Program.Settings.RENDER_Options.EXPORT_FIX_PATH);
                    Program.Logger.error(ex);

                    MessageBox.Show(CDict.GetValue("MainForm.Mbox.ErrorCreatingRenderFolder", "Error creating destination folder, render aborted..."));
                    return;
                }
            }

            // SE NELLA CONFIGURAZIONE SPECIFICO IL PROFILO DA UTILIZZARE PER IL RENDER, CONTROLLO SE
            // E' TRA QUELLI A DISPOSIZIONE NEL RENDER-ENGINE

            string RenderProfile = "";

            if (Program.Settings.RENDER_Options.FIX_PROFILE != "")
            { 
                Program.Logger.info("   > Render configuation required Profile : " + Program.Settings.RENDER_Options.FIX_PROFILE);

                if (RenderTM.Profiles.Contains(Program.Settings.RENDER_Options.FIX_PROFILE.ToUpper()))
                {
                    RenderProfile = Program.Settings.RENDER_Options.FIX_PROFILE;
                    Program.Logger.info("   > Profile found");
                }
                else
                {
                    Program.Logger.error("   > Profile not found");
                }
            }

            if (m_current_renderExtension.Trim().Equals(""))
            {
                m_current_renderExtension = ".avi";
            }

            string output_file = Path.GetFileName(projectHelper.CurrentProject.Filename).Replace(Path.GetExtension(projectHelper.CurrentProject.Filename), m_current_renderExtension);
            string dest_filename = Path.Combine(Program.Settings.RENDER_Options.EXPORT_FIX_PATH, output_file);


            Program.Logger.info("   > Render destination : " + dest_filename);

            RenderTM.Render(projectHelper.CurrentProject.Filename, dest_filename, RenderProfile);

        }

        private void mediaAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            projectHelper.RollbackManager.Addbackup("Before render to media-file", enRollbackType.asRendered);

            frm_Loading.Title = CDict.GetValue("LoadingForm.Rendering", "Rendering");
            frm_Loading.ClearMessages();
            frm_Loading.Percent = 0;
            frm_Loading.Show();

            Application.DoEvents();
            //RenderTM.Render(projectHelper.CurrentProject.Filename, "c:\\prova.avi");

        }

        private void exportAudioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Forms.frmAudioExport audioexp = new frmAudioExport();

            audioexp.ProjectHelper = this.projectHelper;

            audioexp.Addtrack("Voiceover track (VOICE)", "VO", 0, true);
            audioexp.Addtrack("Track A (AUDIO-A)", "VA", 1, false);
            audioexp.Addtrack("Track B (AUDIO-B)", "VB", 2, false);
            audioexp.Addtrack("Background (BG)", "BG", 3, false);

            audioexp.DefaultPath = Program.Settings.RENDER_Options.EXPORT_FIX_PATH;

            if (audioexp.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                int[] tracks = audioexp.GetSelectedIndex();

                if (tracks.Length > 0)
                {
                    Program.Logger.info(" Exporting audio");
                    
                    projectHelper.RollbackManager.Addbackup("Before Exporting Audio", enRollbackType.asRendered);

                    frm_Loading.Title = CDict.GetValue("LoadingForm.Rendering", "Rendering");
                    frm_Loading.ClearMessages();
                    frm_Loading.Percent = 0;
                    frm_Loading.Show();

                    Application.DoEvents();

                    // CONTROLLO E CREO LA CARTELLA DI DESTINAZIONE DEI RENDERS

                    if (!Directory.Exists(Program.Settings.RENDER_Options.EXPORT_FIX_PATH))
                    {
                        try
                        {
                            Directory.CreateDirectory(Program.Settings.RENDER_Options.EXPORT_FIX_PATH);
                        }
                        catch (System.Exception ex)
                        {
                            Program.Logger.error("   > Error creating destination folder : " + Program.Settings.RENDER_Options.EXPORT_FIX_PATH);
                            Program.Logger.error(ex);

                            MessageBox.Show(CDict.GetValue("MainForm.Mbox.ErrorCreatingRenderFolder", "Error on destination folder, render aborted..."));
                            return;
                        }
                    }

                    // SE NELLA CONFIGURAZIONE SPECIFICO IL PROFILO DA UTILIZZARE PER IL RENDER, CONTROLLO SE
                    // E' TRA QUELLI A DISPOSIZIONE NEL RENDER-ENGINE

                    string RenderProfile = Program.Settings.RENDER_Options.FIX_PROFILE_AUDIOEXP;

                    Program.Logger.info("   > Render destination : " + audioexp.Filename);

                    RenderTM.Render(projectHelper.CurrentProject.Filename, audioexp.Filename, RenderProfile, tracks);
                }
            }

            audioexp.Dispose();
        }

        private void preferenceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.Settings.RequestForm();
        }
        
        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (frm_about == null) frm_about = new frmAbout();
            frm_about.ShowDialog();
        }

        private void activationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenProductKeyForm();
        }

        #endregion MAINMENU ITEMS EVENTS

        

        #region MAINTIMELINE BUTTON

        private void btnPlay_Click_1(object sender, EventArgs e)
        {
            if (mxptl.InternalEngine.EngineStatus != MediaAlliance.Controls.MxpTimeLine.Engine.InternalEngineStatus.play)
            {
                mxptl.InternalEngine.Play();

                //foreach (CMxpClip clip in mxptl.CurrentOnlineClips)
                //{
                //    CMxpTL_ClipInfo fClip = clip.ClipInfo;

                //    int trk = 0;

                //    if (clip.Type == enCMxpClipType.Video)
                //    {
                //        trk = (clip.ParentTrack.ID == 1) ? 0 : 1;
                //    }

                //    vp.Play(trk, fClip.file_id);
                //}
            }
        }
        
        private void btnPause_Click(object sender, EventArgs e)
        {
            if (mxptl.InternalEngine.EngineStatus == MediaAlliance.Controls.MxpTimeLine.Engine.InternalEngineStatus.play)
            {
                mxptl.InternalEngine.Pause();
            }
        }
        
        private void btnFrameFFW_Click(object sender, EventArgs e)
        {
            mxptl.InternalEngine.Pause();

            if (MediaAlliance.Win32.CWin32.GetAsyncKeyState(Keys.ShiftKey))
            {
                mxptl.SetCurrentPositionAsExternal(mxptl.CurrentPosition + (int)mxptl.FPS);
            }
            else
            {
                mxptl.SetCurrentPositionAsExternal(mxptl.CurrentPosition + 1);
            }
        }

        private void btnFrameREW_Click(object sender, EventArgs e)
        {
            mxptl.InternalEngine.Pause();

            if (MediaAlliance.Win32.CWin32.GetAsyncKeyState(Keys.ShiftKey))
            {
                mxptl.SetCurrentPositionAsExternal(mxptl.CurrentPosition - (int)mxptl.FPS);
            }
            else
            {
                mxptl.SetCurrentPositionAsExternal(mxptl.CurrentPosition - 1);
            }
        }

        private void btnBlank_Click(object sender, EventArgs e)
        {
            Program.Logger.info("[PRESSED BUTTON] Blanker");
            keyboard_manager.CLIP_BLACKER();
        }

        private void btnClipRemover_Click(object sender, EventArgs e)
        {
            Program.Logger.info("[PRESSED BUTTON] Eraser");
            keyboard_manager.CLIP_CUTTER();
        }
        
        private void picSplit_Click(object sender, EventArgs e)
        {
            Program.Logger.info("[PRESSED BUTTON] Split");
            mxptl.CLIP_CUTTER();
        }

        private void picZoomAll_Click(object sender, EventArgs e)
        {
            Program.Logger.info("[PRESSED BUTTON] Zoom all main timeline");
            mxptl.ZoomAll();
        }

        private void btnZoomFit_Click(object sender, EventArgs e)
        {
            Program.Logger.info("[PRESSED BUTTON] Zoom fit main timeline");

            long tcin   = mxptl.TCOUT;
            long tcout  = 0;

            foreach (CMxpClip clip in mxptl.Tracks.AllClips)
            {
                if (!clip.IsFreeClip)
                {
                    if (clip.TimcodeIn + clip.ClippingIn < tcin) tcin = clip.TimcodeIn + clip.ClippingIn;
                    if (clip.TimcodeIn + clip.ClippingOut > tcout) tcout = clip.TimcodeIn + clip.ClippingOut;
                }
                else
                {
                    if (clip.TimcodeIn < tcin) tcin = clip.TimcodeIn;
                    if (clip.TimecodeOut > tcout) tcout = clip.TimecodeOut;
                }
            }

            if (tcout - tcin > 50)
            {
                mxptl.ZoomIn = tcin;
                mxptl.ZoomOut = tcout;
            }
        }

        private void btnStepBW_Click(object sender, EventArgs e)
        {
            mxptl.MovePreviousStep();
        }

        private void btnStepFW_Click(object sender, EventArgs e)
        {
            mxptl.MoveNextStep();
        }

        #endregion MAINTIMELINE BUTTON
        


        #region PLAY BAR SOURCE

        private void btn_PlaySrc_Click(object sender, EventArgs e)
        {
            if (SourceTimeline != null)
            {
                if(SourceTimeline.Enabled) SourceTimeline.InternalEngine.Play();
            }
        }

        private void btn_FrameREWSrc_Click(object sender, EventArgs e)
        {
            if (SourceTimeline != null)
            {
                if (SourceTimeline.Enabled) SourceTimeline.SetCurrentPositionAsExternal(SourceTimeline.CurrentPosition - 1);
            }
        }

        private void btn_PauseSrc_Click(object sender, EventArgs e)
        {
            if (SourceTimeline != null)
            {
                if (SourceTimeline.Enabled) SourceTimeline.InternalEngine.Pause();
            }
        }

        private void btn_FrameFFwSrc_Click(object sender, EventArgs e)
        {
            if (SourceTimeline != null)
            {
                if (SourceTimeline.Enabled) SourceTimeline.SetCurrentPositionAsExternal(SourceTimeline.CurrentPosition + 1);
            }
        }

        #endregion PLAY BAR SOURCE




        private void lblCurrentTC0_OnTimecodeChanged(object Sender, long timecode)
        {
            mxptl.CurrentPosition = timecode;
        }
        
        private void toolStripMenuItem1_DropDownOpening(object sender, EventArgs e)
        {
            if (frm_ClipInfo != null)
            {
                clipInfoToolStripMenuItem.Checked = frm_ClipInfo.Visible;
            }
        }

        private void clipInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (frm_ClipInfo == null)
            {
                frm_ClipInfo = new frmClipInfo(mxptl.FPS);
            }

            frm_ClipInfo.Visible = !frm_ClipInfo.Visible;
        }

        private void tmrProjectDurationUpdater_Tick(object sender, EventArgs e)
        {
            UpdateProjectDuration();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            mxptl.Tracks[0].Height = 100;

            mxptl.Tracks[3].Height = 100;
        }

        private void ProjectInfoUC_OnFullinfoClick(object Sender)
        {
            frmProjectInfo frm  = new frmProjectInfo();
            frm.MainTimeline    = mxptl;
            frm.ProjectHelper   = this.projectHelper;
            frm.ShowDialog();
            frm.Dispose();
        }

        private bool HasInvertedField(CMxpClip clip)
        {
            bool result = false;

            if (clip.ClipInfo.ClipAttributes.ContainsKey("ScanOrder"))
            {
                if (clip.ClipInfo.ClipAttributes["ScanOrder"].ToString() != "")
                {
                    //clip.InvertedFieldOrder = clip.ClipInfo.ClipAttributes["ScanOrder"].ToString().ToUpper().Trim() != m_current_renderscanorder.ToUpper();
                    result = clip.ClipInfo.ClipAttributes["ScanOrder"].ToString().ToUpper().Trim() != m_current_renderscanorder.ToUpper();
                }
            }

            return result;
        }

        private void markInToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mxptl.MarkIn();
        }

        private void markOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mxptl.MarkOut();
        }

        private void btnFrameREW_MouseEnter(object sender, EventArgs e)
        {
            ttip.Active = false;
            ttip.Active = true;
        }

        private void lblCurrentClip_MouseMove(object sender, MouseEventArgs e)
        {
            ttip.Active = false;
            ttip.Active = true;
        }
    }
}