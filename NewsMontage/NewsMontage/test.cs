﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MediaAlliance.Controls.MxpTimeLine.Objects;
using NewsMontange.Classes;
using System.IO;

namespace NewsMontange
{
    public partial class test : Form
    {
        delegate void dlgUpdate(CWaveFormMaker.CWaveFormClip wc, Bitmap bmp, int id);
        private CWaveFormMaker wfMaker = new CWaveFormMaker();

        CMxpTL_ClipInfo clip_info;
        int pic_size = 400;

        public test()
        {
            InitializeComponent();

            wfMaker.SmallPictureSize = pic_size;
            wfMaker.ImageCachePath = new System.IO.FileInfo(Application.ExecutablePath).Directory.FullName;
            wfMaker.ImageCachePath = Path.Combine(wfMaker.ImageCachePath, "temp");
            wfMaker.OnRendered += new CWaveFormMaker.wfmk_OnRendered(wfMaker_OnRendered);

            clip_info = new CMxpTL_ClipInfo();

            //clip_info.filename   = @"D:\! MyExtra\Controls\MxpTimeLine\Test_Projects\SA_20110414T195411\VoiceFiles\V_20110414_203251.wav";
            clip_info.filename = @"C:\WORKS\VIDEO_TEST\TEST WAVEFORM_01_mono.wav";
            clip_info.Tcin      = 0;
            clip_info.Tcout     = 0;



            MediaAlliance.AV.MediaInfo.MediaInformations mi = new MediaAlliance.AV.MediaInfo.MediaInformations();
            mi.Open(clip_info.filename);

            Program.Logger.debug("Loading clip infos :");
            Program.Logger.debug("  > video stream/s      : " + mi.VideoCount);
            Program.Logger.debug("  > audio stream/s      : " + mi.AudioCount);
            Program.Logger.debug("  > frame rate          : " + mi.FrameRate);

            if (mi.VideoCount > 0)
            {
                string dStr = mi.GetValue(MediaAlliance.AV.MediaInfo.VideoInfoRequest.Duration, 0);
                clip_info.Tcout = Convert.ToInt64(dStr) / 40;
                clip_info.Clipping_out = clip_info.Tcout;
            }
            else
            {
                string dStr = mi.GetValue(MediaAlliance.AV.MediaInfo.GeneralInfoRequest.Duration);
                clip_info.Tcout = Convert.ToInt64(dStr) / 40;
                clip_info.Clipping_out = clip_info.Tcout;
            }


            mi.Dispose();

            wfMaker.AddClip(clip_info, 9000);
        }

        void wfMaker_OnRendered(object Sender, CMxpTL_ClipInfo clip_info, CWaveFormMaker.CWaveFormClip wave_form, Bitmap bmp, int id)
        {
            dlgUpdate upd = new dlgUpdate(update_wc);

            pictureBox2.Image = wfMaker.getWaveForm(clip_info, 0, clip_info.Tcout);

            this.Invoke(upd, wave_form, bmp, id);
        }

        private void update_wc(CWaveFormMaker.CWaveFormClip wave_form, Bitmap bitmap, int id)
        {
            //listBox1.Items.Clear();

            //foreach (Bitmap bmp in wave_form.bitmapsCache)
            {
                bitmap.Tag = id;
                listBox1.Items.Add(bitmap);
            }

            pictureBox2_Click(null, null);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            pictureBox1.Image = (Bitmap)listBox1.SelectedItem;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Bitmap bmpmain = new Bitmap(pic_size * wfMaker[clip_info].PicNumber, 70);
            Graphics g = Graphics.FromImage(bmpmain);

            foreach (Bitmap bmp in listBox1.Items)
            {
                int id = (int)bmp.Tag;
                g.DrawImage(bmp, id * pic_size, 0);
            }

            g.Dispose();
            pictureBox2.Image = bmpmain;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            wfMaker[clip_info].RenderWidth = wfMaker[clip_info].RenderWidth * 2;
        }

        private void pictureBox2_MouseMove(object sender, MouseEventArgs e)
        {
            long tc = e.X * clip_info.Tcout / pictureBox2.Width;
            pictureBox1.Image = wfMaker.getWaveForm(clip_info, tc, tc + (25 * 4));
        }

        private void radMenuItem2_Click(object sender, EventArgs e)
        {

        }
    }
}
