﻿namespace NewsMontange.Forms
{
    partial class frmWarningList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWarningList));
            this.lst = new System.Windows.Forms.ListBox();
            this.pnlDown = new System.Windows.Forms.Panel();
            this.pnlExit = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlPref = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnExit = new MediaAlliance.Controls.MAButton();
            this.btnSettingPage = new MediaAlliance.Controls.MAButton();
            this.pnlDown.SuspendLayout();
            this.pnlExit.SuspendLayout();
            this.pnlPref.SuspendLayout();
            this.SuspendLayout();
            // 
            // lst
            // 
            this.lst.BackColor = System.Drawing.Color.Gray;
            this.lst.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lst.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lst.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.lst.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lst.FormattingEnabled = true;
            this.lst.IntegralHeight = false;
            this.lst.ItemHeight = 52;
            this.lst.Location = new System.Drawing.Point(0, 0);
            this.lst.Name = "lst";
            this.lst.Size = new System.Drawing.Size(343, 387);
            this.lst.TabIndex = 0;
            this.lst.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.lst_DrawItem);
            // 
            // pnlDown
            // 
            this.pnlDown.BackColor = System.Drawing.Color.DarkGray;
            this.pnlDown.Controls.Add(this.pnlExit);
            this.pnlDown.Controls.Add(this.pnlPref);
            this.pnlDown.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlDown.Location = new System.Drawing.Point(0, 387);
            this.pnlDown.Name = "pnlDown";
            this.pnlDown.Size = new System.Drawing.Size(343, 65);
            this.pnlDown.TabIndex = 1;
            // 
            // pnlExit
            // 
            this.pnlExit.Controls.Add(this.btnExit);
            this.pnlExit.Controls.Add(this.label2);
            this.pnlExit.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlExit.Location = new System.Drawing.Point(264, 0);
            this.pnlExit.Name = "pnlExit";
            this.pnlExit.Size = new System.Drawing.Size(79, 65);
            this.pnlExit.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label2.Location = new System.Drawing.Point(0, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 43;
            this.label2.Text = "Close";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlPref
            // 
            this.pnlPref.Controls.Add(this.btnSettingPage);
            this.pnlPref.Controls.Add(this.label1);
            this.pnlPref.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlPref.Location = new System.Drawing.Point(0, 0);
            this.pnlPref.Name = "pnlPref";
            this.pnlPref.Size = new System.Drawing.Size(79, 65);
            this.pnlPref.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label1.Location = new System.Drawing.Point(0, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 43;
            this.label1.Text = "Preferences";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Transparent;
            this.btnExit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnExit.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnExit.Image_Hover")));
            this.btnExit.Image_Normal = null;
            this.btnExit.Image_Type = MediaAlliance.Controls.NmButtonIcon.Exit;
            this.btnExit.Location = new System.Drawing.Point(0, 0);
            this.btnExit.Margin = new System.Windows.Forms.Padding(6, 13, 6, 13);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(79, 52);
            this.btnExit.TabIndex = 42;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnSettingPage
            // 
            this.btnSettingPage.BackColor = System.Drawing.Color.Transparent;
            this.btnSettingPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSettingPage.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnSettingPage.Image_Hover")));
            this.btnSettingPage.Image_Normal = null;
            this.btnSettingPage.Image_Type = MediaAlliance.Controls.NmButtonIcon.Settings;
            this.btnSettingPage.Location = new System.Drawing.Point(0, 0);
            this.btnSettingPage.Margin = new System.Windows.Forms.Padding(6, 13, 6, 13);
            this.btnSettingPage.Name = "btnSettingPage";
            this.btnSettingPage.Size = new System.Drawing.Size(79, 52);
            this.btnSettingPage.TabIndex = 42;
            this.btnSettingPage.Click += new System.EventHandler(this.btnSettingPage_Click);
            // 
            // frmWarningList
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(343, 452);
            this.Controls.Add(this.lst);
            this.Controls.Add(this.pnlDown);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmWarningList";
            this.ShowIcon = false;
            this.Text = "NewsMontage WARNINGS";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmWarningList_FormClosing);
            this.pnlDown.ResumeLayout(false);
            this.pnlExit.ResumeLayout(false);
            this.pnlPref.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lst;
        private System.Windows.Forms.Panel pnlDown;
        private MediaAlliance.Controls.MAButton btnSettingPage;
        private System.Windows.Forms.Panel pnlPref;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlExit;
        private MediaAlliance.Controls.MAButton btnExit;
        private System.Windows.Forms.Label label2;
    }
}