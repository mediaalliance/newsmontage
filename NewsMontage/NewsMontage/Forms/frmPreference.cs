﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Xml;
using System.Reflection;
using NewsMontange.Language;
using System.IO;



namespace NewsMontange.Forms
{
    public partial class frmPreference : Form
    {
        public frmPreference()
        {
            InitializeComponent();
        }


        private void frmPreference_Load(object sender, EventArgs e)
        {
            if (File.Exists(Program.LanguageFile))
            {
                CDict.AddFromFile(Program.LanguageFile);
            }

            label1.Text = CDict.GetValue("FormPreference.Title", label1.Text);

            AddPathItem(CDict.GetValue("FormPreference.NewsNetPrjPath", "NewsNet projects path"), 
                        Program.Settings.Project_Options.NEWSNET_PRJ_PATH).TextChanged += new EventHandler(NewsNet_PathChanged);
            AddPathItem(CDict.GetValue("FormPreference.RenderPath", "Render output path"), 
                        Program.Settings.RENDER_Options.EXPORT_FIX_PATH).TextChanged += new EventHandler(RenderOutput_PathChanged);

            lblClose.Text = CDict.GetValue("FormPreference.Close", "Close");
        }



        private TextBox AddPathItem(string name, string path)
        {
            SplitContainer me = new SplitContainer();
            me.Parent = pnlMain;
            me.Dock = DockStyle.Top;
            me.Height = 45;
            me.SplitterDistance = Width / 4;
            me.IsSplitterFixed = true;
            me.FixedPanel = FixedPanel.Panel1;

            Label lbl = new Label();
            lbl.Parent = me.Panel1;
            lbl.Dock = DockStyle.Fill;
            lbl.AutoSize = false;
            lbl.TextAlign = ContentAlignment.MiddleLeft;
            lbl.Text = name;
            lbl.ForeColor = Color.Silver;

            TextBox txt = new TextBox();
            txt.Parent = me.Panel2;
            txt.Top = (me.Height / 2) - (txt.Height / 2);
            txt.Left = 5;
            txt.Width = me.Panel2.Width - 35;
            txt.Text = path;
            txt.ReadOnly = true;
            txt.BorderStyle = BorderStyle.FixedSingle;
            txt.BackColor = Color.DarkGray;


            Button btn = new Button();
            btn.Parent = me.Panel2;
            btn.Top = txt.Top;
            btn.Left = txt.Right + 4;
            btn.Width = me.Panel2.Width - btn.Left;
            btn.Height = txt.Height;
            btn.Text = "...";
            btn.Tag = txt;
            btn.Click += new EventHandler(choosePath_Click);

            me.BringToFront();

            return txt;
        }



        void choosePath_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                ((TextBox)((Control)sender).Tag).Text = folderBrowserDialog1.SelectedPath;
            }
        }



        private void NewsNet_PathChanged(object Sender, EventArgs e)
        {
            Program.Settings.Project_Options.NEWSNET_PRJ_PATH = ((TextBox)Sender).Text;
        }

        private void RenderOutput_PathChanged(object Sender, EventArgs e)
        {
            Program.Settings.RENDER_Options.EXPORT_FIX_PATH = ((TextBox)Sender).Text;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
