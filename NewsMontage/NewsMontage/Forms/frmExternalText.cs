﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using NewsMontange.UserControls;
using System.IO;
using NewsMontange.Language;

namespace NewsMontange.Forms
{
    public partial class frmExternalText : Form
    {
        public frmExternalText()
        {
            InitializeComponent();
            //AddTabStrip();
        }

        private void richText_TextChanged(object sender, EventArgs e)
        {
            if (richText.Tag != null && richText.Tag is MA_NMRichTextUC)
            {
                ((MA_NMRichTextUC)richText.Tag).Rtf = richText.Rtf;
            }
        }

        private void frmExternalText_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                this.Hide();
            }
        }


        private void zoomTrack_ValueChanged(object sender, EventArgs e)
        {
            float valore = .6F + (.2F * zoomTrack2.Value);
            richText.ZoomFactor = valore;
        }

        private void richText_OnZoomChanged(object Sender, float zoom_level, bool ChangedByGui)
        {
            if (ChangedByGui)
            {
                int newvalue = (int)((zoom_level / .2F) - .6F);
                if (newvalue < zoomTrack2.Minimum) newvalue = zoomTrack2.Minimum;
                if (newvalue > zoomTrack2.Maximum) newvalue = zoomTrack2.Maximum;
                zoomTrack2.Value = newvalue;
            }
        }

        private void btnZoom1_Click(object sender, EventArgs e)
        {
            richText.ZoomFactor = 1.0F;
        }

        private void btnContrast_Click(object sender, EventArgs e)
        {
            if (richText.BackColor == Color.DimGray)
            {
                richText.BackColor = Color.WhiteSmoke;
            }
            else
            {
                richText.BackColor = Color.DimGray;
            }
        }

        private void btnBold_Click(object sender, EventArgs e)
        {
            bool isBold     = richText.SelectionFont.Bold;
            bool isItalic   = richText.SelectionFont.Italic;

            isBold = !isBold;

            FontStyle sty = FontStyle.Regular;

            if (isBold) sty |= FontStyle.Bold;
            if (isItalic) sty |= FontStyle.Italic;

            richText.SelectionFont = new Font(richText.SelectionFont, sty);
        }

        private void btnItalic_Click(object sender, EventArgs e)
        {
            bool isBold = richText.SelectionFont.Bold;
            bool isItalic = richText.SelectionFont.Italic;

            isItalic = !isItalic;

            FontStyle sty = FontStyle.Regular;

            if (isBold) sty |= FontStyle.Bold;
            if (isItalic) sty |= FontStyle.Italic;

            richText.SelectionFont = new Font(richText.SelectionFont, sty);
        }

        private void frmExternalText_Load(object sender, EventArgs e)
        {
            if (File.Exists(Program.LanguageFile))
            {
                CDict.AddFromFile(Program.LanguageFile);
            }

            lblZoomLevel.Text = CDict.GetValue("FormExternalText.ZoomLevel", lblZoomLevel.Text);
            lblContrast.Text = CDict.GetValue("FormExternalText.Contrast", lblContrast.Text);
            lblBold.Text = CDict.GetValue("FormExternalText.Bold", lblBold.Text);
            lblItalic.Text = CDict.GetValue("FormExternalText.Italic", lblItalic.Text);
        }

        //void AddTabStrip()
        //{
        //    RadMenuContentItem contentItem = new RadMenuContentItem();
        //    RadTabStripElement tabStrip = new RadTabStripElement(); 
        //    tabStrip.UseNewLayoutSystem     = true; 
        //    tabStrip.StretchBaseMode        = TabBaseStretchMode.NoStretch; 
        //    tabStrip.ShowOverFlowButton     = false; 
        //    contentItem.ContentElement      = tabStrip; 

        //    TabItem fontItem = new TabItem("Font"); 
        //    TabItem sizeItem = new TabItem("Size"); 
        //    //sizeItem.Click += new EventHandler(sizeItem_Click); 
        //    //fontItem.Click += new EventHandler(fontItem_Click); 
        //    tabStrip.Items.Add(fontItem); 
        //    tabStrip.Items.Add(sizeItem);
        //    this.radToolStripItem4.Items.Insert(6, contentItem); 
        //    //SetArrowButton(fontCombo); 
        //    //SetArrowButton(sizeCombo);
        //}
    }
}
