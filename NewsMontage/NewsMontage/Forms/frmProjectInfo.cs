﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using NewsMontange.Helpers;
using MediaAlliance.Controls;

using MediaAlliance.Controls.MxpTimeLine.Objects;
using System.IO;
using MediaAlliance.Controls.MxpTimeLine;
using System.Diagnostics;
using NewsMontange.Language;


namespace NewsMontange.Forms
{
    public partial class frmProjectInfo : Form
    {
        private CProjectsHelper m_projectHelper = null;
        private MxpTmeLine m_mainTimeLine = null;


        public frmProjectInfo()
        {
            InitializeComponent();
            UpdateLanguage();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private int UsedTimes(CMxpTL_ClipInfo clip_info)
        {
            if (m_projectHelper != null && m_mainTimeLine != null)
            {
                int used_times = 0;

                foreach (CMxpClip clip in m_mainTimeLine.Tracks.AllClips)
                {
                    if (clip.ClipInfo.filename.Equals(clip_info.filename, StringComparison.OrdinalIgnoreCase))
                    {
                        if (clip.Type == enCMxpClipType.Video || clip.Group.Count == 0)
                        {
                            used_times++;
                        }
                    }
                }

                return used_times;
            }

            return 0;
        }

        private void Update()
        {
            if (m_projectHelper != null)
            {
                treeInfo.Nodes.Clear();

                TreeNode prj_info = treeInfo.Nodes.Add(CDict.GetValue("FullProjectInfoForm.TreeViewItem.ProjectInfo", "Project Info"));
                prj_info.Expand();
                prj_info.ForeColor = Color.YellowGreen;

                prj_info.Nodes.Add(new TreeNode(CDict.GetValue("FullProjectInfoForm.TreeViewItem.Title", "Title") + " : " + m_projectHelper.CurrentProject.Project_Title));
                prj_info.Nodes.Add(new TreeNode(CDict.GetValue("FullProjectInfoForm.TreeViewItem.Author", "Author") + " : " + m_projectHelper.CurrentProject.Project_Author));
                prj_info.Nodes.Add(new TreeNode(CDict.GetValue("FullProjectInfoForm.TreeViewItem.CreationDate", "Creation date") + " : " + m_projectHelper.CurrentProject.CreationDate.ToString()));
                TreeNode pfilenode = new TreeNode(CDict.GetValue("FullProjectInfoForm.TreeViewItem.Filename", "File name") + " : " + m_projectHelper.CurrentProject.Filename);
                prj_info.Nodes.Add(pfilenode);
                pfilenode.Tag = new FileInfo(m_projectHelper.CurrentProject.Filename);

                TreeNode src_info = treeInfo.Nodes.Add(CDict.GetValue("FullProjectInfoForm.TreeViewItem.Sources", "Sources"));
                src_info.Expand();
                src_info.ForeColor = Color.YellowGreen;

                TreeNode src_v_info = src_info.Nodes.Add(CDict.GetValue("FullProjectInfoForm.TreeViewItem.Videoclips", "Video Clips"));
                TreeNode src_a_info = src_info.Nodes.Add(CDict.GetValue("FullProjectInfoForm.TreeViewItem.Voicefiles", "Voice files"));
                TreeNode src_b_info = src_info.Nodes.Add(CDict.GetValue("FullProjectInfoForm.TreeViewItem.Backgroundclips", "Background clips"));

                foreach (CMxpTL_ClipInfo clip_info in m_projectHelper.SourcesList)
                {
                    TreeNode clip_node = null;

                    switch ((CProjectsHelper.enSouceType)clip_info.Tag)
                    {
                        case CProjectsHelper.enSouceType.video:
                            clip_node = src_v_info;
                            break;
                        case CProjectsHelper.enSouceType.voice:
                            clip_node = src_a_info;
                            break;
                        case CProjectsHelper.enSouceType.background:
                            clip_node = src_b_info;
                            break;
                    }

                    if (clip_node != null)
                    {
                        TreeNode vNode = new TreeNode(Path.GetFileName(clip_info.filename));
                        TreeNode filenode = new TreeNode("File : " + clip_info.filename);
                        vNode.Nodes.Add(filenode);
                        filenode.Tag = new FileInfo(clip_info.filename);
                        vNode.Nodes.Add(new TreeNode("Used : " + UsedTimes(clip_info)));

                        foreach (TreeNode subnode in vNode.Nodes) subnode.ForeColor = Color.LightGray;

                        clip_node.Nodes.Add(vNode);
                    }
                }
            }
        }
        
        public void UpdateLanguage()
        {
            label1.Text = CDict.GetValue("MainForm.ProjectInfo", label1.Text);
            label2.Text = CDict.GetValue("Generic.Button.Close", label2.Text);
            openLocationToolStripMenuItem.Text = CDict.GetValue("FullProjectInfoForm.OpenLocation", openLocationToolStripMenuItem.Text);
            Update();
        }


        #region PUBLIC PROPERTIES

        public CProjectsHelper ProjectHelper
        {
            get { return m_projectHelper; }
            set
            {
                if (m_projectHelper != value)
                {
                    m_projectHelper = value;
                    Update();
                }
            }
        }

        public MxpTmeLine MainTimeline
        {
            get { return m_mainTimeLine; }
            set { m_mainTimeLine = value; }
        }

        #endregion PUBLIC PROPERTIES



        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmsTreeItems_Opening(object sender, CancelEventArgs e)
        {
            if (treeInfo.SelectedNode == null)
            {
                e.Cancel = true;
            }
            else
            {
                openLocationToolStripMenuItem.Enabled = (treeInfo.SelectedNode.Tag is FileInfo);
            }
        }

        private void openLocationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (treeInfo.SelectedNode == null) return;

            if (treeInfo.SelectedNode.Tag is FileInfo)
            {
                Process.Start(((FileInfo)treeInfo.SelectedNode.Tag).DirectoryName);
            }
        }
    }
}
