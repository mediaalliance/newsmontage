﻿namespace NewsMontange.Forms
{
    partial class frmCensorEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCensorEditor));
            this.pnlMonitor = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Timeline = new MediaAlliance.Controls.MxpTimeLine.MxpTmeLine();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.pnlTools = new System.Windows.Forms.Panel();
            this.pnlMainButtons = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.addKF = new System.Windows.Forms.PictureBox();
            this.btnFrameFFW = new MediaAlliance.Controls.MAButton();
            this.btnPlay = new MediaAlliance.Controls.MAButton();
            this.btnPause = new MediaAlliance.Controls.MAButton();
            this.btnFrameREW = new MediaAlliance.Controls.MAButton();
            this.btnAddMask = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.pnlTop.SuspendLayout();
            this.pnlTools.SuspendLayout();
            this.pnlMainButtons.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.addKF)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlMonitor
            // 
            this.pnlMonitor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pnlMonitor.Location = new System.Drawing.Point(137, 56);
            this.pnlMonitor.Name = "pnlMonitor";
            this.pnlMonitor.Size = new System.Drawing.Size(561, 397);
            this.pnlMonitor.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Timeline);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 602);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(839, 109);
            this.panel1.TabIndex = 1;
            // 
            // Timeline
            // 
            this.Timeline.AutoAdaptLines = false;
            this.Timeline.CanDisableTracks = false;
            this.Timeline.CanDragClip = false;
            this.Timeline.CanDragoutClips = true;
            this.Timeline.CanDragoutMarkers = false;
            this.Timeline.CanEditAudioNodes = true;
            this.Timeline.CanMarkOverRectime = true;
            this.Timeline.CanSeekOverRectime = true;
            this.Timeline.ClipColor = System.Drawing.Color.LightSteelBlue;
            this.Timeline.ClipOfContextMenu = null;
            this.Timeline.ColorFocus = System.Drawing.Color.LightSalmon;
            this.Timeline.CurrentPosition = ((long)(0));
            this.Timeline.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Timeline.Enabled = false;
            this.Timeline.FireDisplayClipEventOnPan = true;
            this.Timeline.FPS = 25F;
            this.Timeline.Location = new System.Drawing.Point(0, 0);
            this.Timeline.Margin = new System.Windows.Forms.Padding(604214, 472870333, 604214, 472870333);
            this.Timeline.Menu_Clips = null;
            this.Timeline.Menu_General = null;
            this.Timeline.Name = "Timeline";
            this.Timeline.RecTime = ((long)(0));
            this.Timeline.RecTimeBarHeight = 0;
            this.Timeline.SelectedClipColor = System.Drawing.Color.LightSalmon;
            this.Timeline.ShowFocus = true;
            this.Timeline.ShowFocusBorder = false;
            this.Timeline.Size = new System.Drawing.Size(839, 109);
            this.Timeline.TabIndex = 1;
            this.Timeline.TCIN = ((long)(0));
            this.Timeline.TCOUT = ((long)(500));
            this.Timeline.TimeBarHeight = 20;
            this.Timeline.TimeLineBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.Timeline.TimeLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.Timeline.TrackBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))));
            this.Timeline.TrackColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(125)))), ((int)(((byte)(127)))));
            this.Timeline.TrackHandleColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(125)))), ((int)(((byte)(127)))));
            this.Timeline.TrackHeight = 40;
            this.Timeline.TrackInfoControl_Container = null;
            this.Timeline.UndoTimes = 10;
            this.Timeline.ZoomBarHeight = 20;
            this.Timeline.ZoomIn = ((long)(0));
            this.Timeline.ZoomLimitMin = 1F;
            this.Timeline.ZoomOut = ((long)(500));
            this.Timeline.OnChangePosition += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnChangePosition(this.Timeline_OnChangePosition);
            // 
            // pnlTop
            // 
            this.pnlTop.Controls.Add(this.pnlMonitor);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(839, 505);
            this.pnlTop.TabIndex = 2;
            // 
            // pnlTools
            // 
            this.pnlTools.Controls.Add(this.button3);
            this.pnlTools.Controls.Add(this.button2);
            this.pnlTools.Controls.Add(this.button1);
            this.pnlTools.Controls.Add(this.addKF);
            this.pnlTools.Controls.Add(this.pnlMainButtons);
            this.pnlTools.Controls.Add(this.btnAddMask);
            this.pnlTools.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlTools.Location = new System.Drawing.Point(0, 505);
            this.pnlTools.Name = "pnlTools";
            this.pnlTools.Size = new System.Drawing.Size(839, 97);
            this.pnlTools.TabIndex = 1;
            // 
            // pnlMainButtons
            // 
            this.pnlMainButtons.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlMainButtons.Controls.Add(this.tableLayoutPanel1);
            this.pnlMainButtons.Location = new System.Drawing.Point(606, 8);
            this.pnlMainButtons.Name = "pnlMainButtons";
            this.pnlMainButtons.Size = new System.Drawing.Size(191, 42);
            this.pnlMainButtons.TabIndex = 44;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.btnFrameFFW, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnPlay, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnPause, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnFrameREW, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(191, 42);
            this.tableLayoutPanel1.TabIndex = 43;
            // 
            // addKF
            // 
            this.addKF.Image = global::NewsMontange.Properties.Resources.keyframe;
            this.addKF.Location = new System.Drawing.Point(426, 11);
            this.addKF.Name = "addKF";
            this.addKF.Size = new System.Drawing.Size(40, 24);
            this.addKF.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.addKF.TabIndex = 45;
            this.addKF.TabStop = false;
            this.addKF.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // btnFrameFFW
            // 
            this.btnFrameFFW.BackColor = System.Drawing.Color.Transparent;
            this.btnFrameFFW.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFrameFFW.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnFrameFFW.Image_Hover")));
            this.btnFrameFFW.Image_Normal = null;
            this.btnFrameFFW.Image_Type = MediaAlliance.Controls.NmButtonIcon.NextFrame;
            this.btnFrameFFW.Location = new System.Drawing.Point(143, 3);
            this.btnFrameFFW.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnFrameFFW.Name = "btnFrameFFW";
            this.btnFrameFFW.Size = new System.Drawing.Size(46, 36);
            this.btnFrameFFW.TabIndex = 41;
            this.btnFrameFFW.Click += new System.EventHandler(this.btnFrameFFW_Click);
            // 
            // btnPlay
            // 
            this.btnPlay.BackColor = System.Drawing.Color.Transparent;
            this.btnPlay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPlay.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnPlay.Image_Hover")));
            this.btnPlay.Image_Normal = null;
            this.btnPlay.Image_Type = MediaAlliance.Controls.NmButtonIcon.Play;
            this.btnPlay.Location = new System.Drawing.Point(49, 3);
            this.btnPlay.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(43, 36);
            this.btnPlay.TabIndex = 39;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // btnPause
            // 
            this.btnPause.BackColor = System.Drawing.Color.Transparent;
            this.btnPause.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPause.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnPause.Image_Hover")));
            this.btnPause.Image_Normal = null;
            this.btnPause.Image_Type = MediaAlliance.Controls.NmButtonIcon.Pause;
            this.btnPause.Location = new System.Drawing.Point(96, 3);
            this.btnPause.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(43, 36);
            this.btnPause.TabIndex = 40;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // btnFrameREW
            // 
            this.btnFrameREW.BackColor = System.Drawing.Color.Transparent;
            this.btnFrameREW.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFrameREW.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnFrameREW.Image_Hover")));
            this.btnFrameREW.Image_Normal = null;
            this.btnFrameREW.Image_Type = MediaAlliance.Controls.NmButtonIcon.BackFrame;
            this.btnFrameREW.Location = new System.Drawing.Point(2, 3);
            this.btnFrameREW.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnFrameREW.Name = "btnFrameREW";
            this.btnFrameREW.Size = new System.Drawing.Size(43, 36);
            this.btnFrameREW.TabIndex = 42;
            this.btnFrameREW.Click += new System.EventHandler(this.btnFrameREW_Click);
            // 
            // btnAddMask
            // 
            this.btnAddMask.Image = global::NewsMontange.Properties.Resources.CMT_PixelBig;
            this.btnAddMask.Location = new System.Drawing.Point(12, 8);
            this.btnAddMask.Name = "btnAddMask";
            this.btnAddMask.Size = new System.Drawing.Size(87, 70);
            this.btnAddMask.TabIndex = 0;
            this.btnAddMask.UseVisualStyleBackColor = true;
            this.btnAddMask.Click += new System.EventHandler(this.btnAddMask_Click);
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Image = global::NewsMontange.Properties.Resources.CMT_PixelSmall;
            this.button1.Location = new System.Drawing.Point(105, 8);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(87, 70);
            this.button1.TabIndex = 46;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Image = global::NewsMontange.Properties.Resources.CMT_Solarize;
            this.button2.Location = new System.Drawing.Point(198, 8);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(87, 70);
            this.button2.TabIndex = 47;
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Enabled = false;
            this.button3.Image = global::NewsMontange.Properties.Resources.CMT_Threshold;
            this.button3.Location = new System.Drawing.Point(291, 8);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(87, 70);
            this.button3.TabIndex = 48;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // frmCensorEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(92)))), ((int)(((byte)(92)))));
            this.ClientSize = new System.Drawing.Size(839, 711);
            this.Controls.Add(this.pnlTop);
            this.Controls.Add(this.pnlTools);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "frmCensorEditor";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CENSOR EDITOR";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmCensorEditor_FormClosing);
            this.Shown += new System.EventHandler(this.frmCensorEditor_Shown);
            this.panel1.ResumeLayout(false);
            this.pnlTop.ResumeLayout(false);
            this.pnlTools.ResumeLayout(false);
            this.pnlTools.PerformLayout();
            this.pnlMainButtons.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.addKF)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel pnlMonitor;
        private System.Windows.Forms.Panel panel1;
        public MediaAlliance.Controls.MxpTimeLine.MxpTmeLine Timeline;
        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Panel pnlTools;
        private System.Windows.Forms.Button btnAddMask;
        private System.Windows.Forms.Panel pnlMainButtons;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MediaAlliance.Controls.MAButton btnFrameFFW;
        private MediaAlliance.Controls.MAButton btnPlay;
        private MediaAlliance.Controls.MAButton btnPause;
        private MediaAlliance.Controls.MAButton btnFrameREW;
        private System.Windows.Forms.PictureBox addKF;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;

    }
}