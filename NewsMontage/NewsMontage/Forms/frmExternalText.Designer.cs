﻿namespace NewsMontange.Forms
{
    partial class frmExternalText
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmExternalText));
            this.radToolStripItem1 = new Telerik.WinControls.UI.RadToolStripItem();
            this.radToolStripItem2 = new Telerik.WinControls.UI.RadToolStripItem();
            this.radToolStripElement1 = new Telerik.WinControls.UI.RadToolStripElement();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblItalic = new System.Windows.Forms.Label();
            this.lblBold = new System.Windows.Forms.Label();
            this.btnItalic = new MediaAlliance.Controls.MAButton();
            this.btnBold = new MediaAlliance.Controls.MAButton();
            this.lblZoomLevel = new System.Windows.Forms.Label();
            this.btnContrast = new MediaAlliance.Controls.MAButton();
            this.btnZoom1 = new MediaAlliance.Controls.MAButton();
            this.lblContrast = new System.Windows.Forms.Label();
            this.lblZoom1 = new System.Windows.Forms.Label();
            this.zoomTrack2 = new System.Windows.Forms.TrackBar();
            this.richText = new NewsMontange.UserControls.MA_NMRichTextUC();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrack2)).BeginInit();
            this.SuspendLayout();
            // 
            // radToolStripItem1
            // 
            this.radToolStripItem1.Key = "0";
            this.radToolStripItem1.Name = "radToolStripItem1";
            this.radToolStripItem1.Text = "radToolStripItem1";
            // 
            // radToolStripItem2
            // 
            this.radToolStripItem2.Key = "1";
            this.radToolStripItem2.Name = "radToolStripItem2";
            this.radToolStripItem2.Text = "radToolStripItem2";
            // 
            // radToolStripElement1
            // 
            this.radToolStripElement1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radToolStripItem1});
            this.radToolStripElement1.Name = "radToolStripElement1";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gray;
            this.panel1.Controls.Add(this.zoomTrack2);
            this.panel1.Controls.Add(this.lblItalic);
            this.panel1.Controls.Add(this.lblBold);
            this.panel1.Controls.Add(this.btnItalic);
            this.panel1.Controls.Add(this.btnBold);
            this.panel1.Controls.Add(this.lblZoomLevel);
            this.panel1.Controls.Add(this.btnContrast);
            this.panel1.Controls.Add(this.btnZoom1);
            this.panel1.Controls.Add(this.lblContrast);
            this.panel1.Controls.Add(this.lblZoom1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(588, 70);
            this.panel1.TabIndex = 69;
            // 
            // lblItalic
            // 
            this.lblItalic.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblItalic.ForeColor = System.Drawing.Color.LightGray;
            this.lblItalic.Location = new System.Drawing.Point(186, 50);
            this.lblItalic.Name = "lblItalic";
            this.lblItalic.Size = new System.Drawing.Size(58, 13);
            this.lblItalic.TabIndex = 11;
            this.lblItalic.Text = "Italic";
            this.lblItalic.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblBold
            // 
            this.lblBold.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBold.ForeColor = System.Drawing.Color.LightGray;
            this.lblBold.Location = new System.Drawing.Point(128, 50);
            this.lblBold.Name = "lblBold";
            this.lblBold.Size = new System.Drawing.Size(59, 13);
            this.lblBold.TabIndex = 10;
            this.lblBold.Text = "Bold";
            this.lblBold.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnItalic
            // 
            this.btnItalic.AutoSize = true;
            this.btnItalic.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.btnItalic.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.none;
            this.btnItalic.Image_Click = null;
            this.btnItalic.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnItalic.Image_Hover")));
            this.btnItalic.Image_Normal = global::NewsMontange.Properties.Resources.BTN_FontItalic;
            this.btnItalic.Image_Type = MediaAlliance.Controls.NmButtonIcon.Custom;
            this.btnItalic.Location = new System.Drawing.Point(195, 8);
            this.btnItalic.Margin = new System.Windows.Forms.Padding(3, 7, 3, 7);
            this.btnItalic.Name = "btnItalic";
            this.btnItalic.Shortcut = System.Windows.Forms.Keys.None;
            this.btnItalic.ShortcutTextColor = System.Drawing.Color.Silver;
            this.btnItalic.Size = new System.Drawing.Size(42, 42);
            this.btnItalic.TabIndex = 9;
            this.btnItalic.Tooltip_Text = "Tooltip text";
            this.btnItalic.UseInternalToolTip = false;
            this.btnItalic.Click += new System.EventHandler(this.btnItalic_Click);
            // 
            // btnBold
            // 
            this.btnBold.AutoSize = true;
            this.btnBold.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.btnBold.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.none;
            this.btnBold.Image_Click = null;
            this.btnBold.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnBold.Image_Hover")));
            this.btnBold.Image_Normal = global::NewsMontange.Properties.Resources.BTN_FontBold;
            this.btnBold.Image_Type = MediaAlliance.Controls.NmButtonIcon.Custom;
            this.btnBold.Location = new System.Drawing.Point(138, 8);
            this.btnBold.Margin = new System.Windows.Forms.Padding(3, 7, 3, 7);
            this.btnBold.Name = "btnBold";
            this.btnBold.Shortcut = System.Windows.Forms.Keys.None;
            this.btnBold.ShortcutTextColor = System.Drawing.Color.Silver;
            this.btnBold.Size = new System.Drawing.Size(42, 42);
            this.btnBold.TabIndex = 8;
            this.btnBold.Tooltip_Text = "Tooltip text";
            this.btnBold.UseInternalToolTip = false;
            this.btnBold.Click += new System.EventHandler(this.btnBold_Click);
            // 
            // lblZoomLevel
            // 
            this.lblZoomLevel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblZoomLevel.AutoSize = true;
            this.lblZoomLevel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblZoomLevel.ForeColor = System.Drawing.Color.LightGray;
            this.lblZoomLevel.Location = new System.Drawing.Point(445, 19);
            this.lblZoomLevel.Name = "lblZoomLevel";
            this.lblZoomLevel.Size = new System.Drawing.Size(62, 13);
            this.lblZoomLevel.TabIndex = 7;
            this.lblZoomLevel.Text = "Zoom level";
            // 
            // btnContrast
            // 
            this.btnContrast.AutoSize = true;
            this.btnContrast.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.btnContrast.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.none;
            this.btnContrast.Image_Click = null;
            this.btnContrast.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnContrast.Image_Hover")));
            this.btnContrast.Image_Normal = null;
            this.btnContrast.Image_Type = MediaAlliance.Controls.NmButtonIcon.Contrast;
            this.btnContrast.Location = new System.Drawing.Point(68, 9);
            this.btnContrast.Margin = new System.Windows.Forms.Padding(3, 12, 3, 12);
            this.btnContrast.Name = "btnContrast";
            this.btnContrast.Shortcut = System.Windows.Forms.Keys.None;
            this.btnContrast.ShortcutTextColor = System.Drawing.Color.Silver;
            this.btnContrast.Size = new System.Drawing.Size(42, 42);
            this.btnContrast.TabIndex = 6;
            this.btnContrast.Tooltip_Text = "Tooltip text";
            this.btnContrast.UseInternalToolTip = false;
            this.btnContrast.Click += new System.EventHandler(this.btnContrast_Click);
            // 
            // btnZoom1
            // 
            this.btnZoom1.AutoSize = true;
            this.btnZoom1.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.btnZoom1.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.none;
            this.btnZoom1.Image_Click = null;
            this.btnZoom1.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnZoom1.Image_Hover")));
            this.btnZoom1.Image_Normal = null;
            this.btnZoom1.Image_Type = MediaAlliance.Controls.NmButtonIcon.View;
            this.btnZoom1.Location = new System.Drawing.Point(12, 9);
            this.btnZoom1.Margin = new System.Windows.Forms.Padding(3, 12, 3, 12);
            this.btnZoom1.Name = "btnZoom1";
            this.btnZoom1.Shortcut = System.Windows.Forms.Keys.None;
            this.btnZoom1.ShortcutTextColor = System.Drawing.Color.Silver;
            this.btnZoom1.Size = new System.Drawing.Size(42, 42);
            this.btnZoom1.TabIndex = 5;
            this.btnZoom1.Tooltip_Text = "Tooltip text";
            this.btnZoom1.UseInternalToolTip = false;
            this.btnZoom1.Click += new System.EventHandler(this.btnZoom1_Click);
            // 
            // lblContrast
            // 
            this.lblContrast.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContrast.ForeColor = System.Drawing.Color.LightGray;
            this.lblContrast.Location = new System.Drawing.Point(61, 50);
            this.lblContrast.Name = "lblContrast";
            this.lblContrast.Size = new System.Drawing.Size(58, 13);
            this.lblContrast.TabIndex = 4;
            this.lblContrast.Text = "Contrast";
            this.lblContrast.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblContrast.Click += new System.EventHandler(this.btnContrast_Click);
            // 
            // lblZoom1
            // 
            this.lblZoom1.AutoSize = true;
            this.lblZoom1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblZoom1.ForeColor = System.Drawing.Color.LightGray;
            this.lblZoom1.Location = new System.Drawing.Point(10, 50);
            this.lblZoom1.Name = "lblZoom1";
            this.lblZoom1.Size = new System.Drawing.Size(45, 13);
            this.lblZoom1.TabIndex = 2;
            this.lblZoom1.Text = "Zoom 1";
            this.lblZoom1.Click += new System.EventHandler(this.btnZoom1_Click);
            // 
            // zoomTrack2
            // 
            this.zoomTrack2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.zoomTrack2.Location = new System.Drawing.Point(372, 35);
            this.zoomTrack2.Maximum = 50;
            this.zoomTrack2.Minimum = 2;
            this.zoomTrack2.Name = "zoomTrack2";
            this.zoomTrack2.Size = new System.Drawing.Size(204, 45);
            this.zoomTrack2.TabIndex = 12;
            this.zoomTrack2.TickFrequency = 5;
            this.zoomTrack2.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.zoomTrack2.Value = 2;
            this.zoomTrack2.ValueChanged += new System.EventHandler(this.zoomTrack_ValueChanged);
            // 
            // richText
            // 
            this.richText.BackColor = System.Drawing.Color.DimGray;
            this.richText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richText.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richText.ForeColor = System.Drawing.Color.Gainsboro;
            this.richText.Location = new System.Drawing.Point(0, 70);
            this.richText.Name = "richText";
            this.richText.Padding = new System.Windows.Forms.Padding(2);
            this.richText.Rtf = "{\\rtf1\\ansi\\ansicpg1252\\deff0\\deflang1040{\\fonttbl{\\f0\\fnil\\fcharset0 Segoe UI;}}" +
    "\r\n\\viewkind4\\uc1\\pard\\f0\\fs20\\par\r\n}\r\n";
            this.richText.SelectionFont = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richText.Size = new System.Drawing.Size(588, 460);
            this.richText.TabIndex = 67;
            this.richText.ZoomFactor = 1F;
            this.richText.OnZoomChanged += new NewsMontange.UserControls.MA_NMRichTextUC.CRichText.Rich_OnZoomChanged(this.richText_OnZoomChanged);
            this.richText.TextChanged += new System.EventHandler(this.richText_TextChanged);
            // 
            // frmExternalText
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(588, 530);
            this.Controls.Add(this.richText);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(493, 479);
            this.Name = "frmExternalText";
            this.ShowIcon = false;
            this.Text = "News Text";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmExternalText_FormClosing);
            this.Load += new System.EventHandler(this.frmExternalText_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrack2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public UserControls.MA_NMRichTextUC richText;
        private Telerik.WinControls.UI.RadToolStripItem radToolStripItem1;
        private Telerik.WinControls.UI.RadToolStripItem radToolStripItem2;
        private Telerik.WinControls.UI.RadToolStripElement radToolStripElement1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblZoom1;
        private System.Windows.Forms.Label lblContrast;
        private MediaAlliance.Controls.MAButton btnContrast;
        private MediaAlliance.Controls.MAButton btnZoom1;
        private System.Windows.Forms.Label lblZoomLevel;
        private MediaAlliance.Controls.MAButton btnBold;
        private MediaAlliance.Controls.MAButton btnItalic;
        private System.Windows.Forms.Label lblItalic;
        private System.Windows.Forms.Label lblBold;
        private System.Windows.Forms.TrackBar zoomTrack2;


    }
}