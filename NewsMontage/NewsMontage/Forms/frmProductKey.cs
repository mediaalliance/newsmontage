﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MediaAlliance.ProductKey;
using System.Reflection;
using System.IO;



namespace NewsMontange
{
    public partial class frmProductKey : Form
    {
        private MediaAlliance.ProductKey.CProductKey pKey;


        public frmProductKey()
        {
            InitializeComponent();
        }

        private void frmCode_Load(object sender, EventArgs e)
        {
            lblProductName.Text = Application.ProductName;
            lblReqCode.Text = CProductKey.getHardwareId(0);
        }
        
        private void txtProdKey_TextChanged(object sender, EventArgs e)
        {
            lblExpiration.Text = "...";

            bool isCodeOk = this.IsOK;

            lblCheck.BackColor = (isCodeOk) ? Color.Green : Color.Red;

            try
            {
                CProductKey pk = new CProductKey(txtProdKey.Text);

                if (!pk.Expire)
                {

                    lblExpiration.Text = "Software never expire";
                }
                else
                {
                    lblExpiration.Text = "Software will expire on next " + pk.ExpirationDate.Value.ToString("dd/MM/yyyy");
                }
            }
            catch
            { 
            }
        }



        #region PUBLIC PROPERTIES

        public string ProductKey
        {
            get { return txtProdKey.Text; }
            set
            {
                txtProdKey.Text = value;
            }
        }

        public bool IsOK
        {
            get 
            {
                bool ok = false;
                try
                {
                    DateTime realdatetime = DateTime.Now;

                    if (File.Exists("MACore.dll"))
                    {
                        Assembly core = Assembly.LoadFile(new FileInfo("MACore.dll").FullName);
                        Type t = core.GetType("MACore.CMACore");
                        object core_class = Activator.CreateInstance(t);
                        realdatetime = (DateTime)t.GetMethod("CallMethod").Invoke(core_class, new object[] { "GetRealDatetimeAprox", new object[] { Application.ProductName } });
                    }

                    ok = CProductKey.check(Application.ProductName, txtProdKey.Text, realdatetime) == CProductKey.enProductKeyCheckResult.ok;                    
                }
                catch { }

                return ok;
            }
        }

        #endregion PUBLIC PROPERTIES

        private void btnCopyReqCode_Click(object sender, EventArgs e)
        {
            Clipboard.Clear();
            Clipboard.SetText(lblReqCode.Text);
        }
        
        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
