﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NewsMontange.Language;
using System.IO;
using NewsMontange.Helpers;

namespace NewsMontange.Forms
{
    public partial class frmAudioExport : Form
    {
        private CProjectsHelper m_projectsHelper = null;

        private class trkItem
        {
            public int index;
            public string title;
            public string shortname;

            public trkItem(string title, string shortname, int index)
            {
                this.index = index;
                this.title = title;
                this.shortname = shortname;
            }

            public override string ToString()
            {
                return title;
            }
        }

        public frmAudioExport()
        {
            InitializeComponent();

            

        }

        public void Addtrack(string title, string shortname, int index, bool Checked)
        {
            chks.Items.Add(new trkItem(title, shortname, index), Checked);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        public int[] GetSelectedIndex()
        {
            List<int> index = new List<int>();

            foreach (trkItem trk in chks.CheckedItems)
            {
                index.Add(trk.index);
            }

            return index.ToArray();
        }

        private void btnChooseFile_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Title = "Audio export";
            saveFileDialog1.Filter = "Wave file|*.wav";

            if( m_projectsHelper != null)
            {
                string extraname = "";

                foreach (trkItem trk in chks.CheckedItems)
                {
                    extraname += trk.shortname + "_";
                }

                extraname = extraname.Substring(0, extraname.Length - 1);

                saveFileDialog1.FileName = Path.GetFileName(m_projectsHelper.CurrentProject.Project_Title + "(" + extraname + ").wav");
            }

            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                ttOutputFile.SetToolTip(txtFile, saveFileDialog1.FileName);
                txtFile.Tag = saveFileDialog1.FileName;
                txtFile.Text = Path.GetFileName(saveFileDialog1.FileName);
            }
        }



        #region PUBLIC PROPERTIES

        public string Filename
        {
            get { return (txtFile.Tag != null)? (string)txtFile.Tag : ""; }
        }

        public string DefaultPath
        {
            get { return saveFileDialog1.InitialDirectory; }
            set
            {
                saveFileDialog1.InitialDirectory = value;
            }
        }

        public CProjectsHelper ProjectHelper
        {
            get { return m_projectsHelper; }
            set { m_projectsHelper = value; }
        }

        #endregion PUBLIC PROPERTIES



        private void UpdateBtnState()
        {
            btnOk.Enabled = false;

            if (chks.CheckedItems.Count > 0 && txtFile.Tag != null)
            {
                btnOk.Enabled = true;
            }
        }

        private void txtFile_TextChanged(object sender, EventArgs e)
        {
            UpdateBtnState();
        }

        private void chks_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateBtnState();
        }

        private void frmAudioExport_Load(object sender, EventArgs e)
        {
            if (File.Exists(Program.LanguageFile))
            {
                CDict.AddFromFile(Program.LanguageFile);
            }

            label1.Text = CDict.GetValue("FormAudioExport.Title", label1.Text);
            label2.Text = CDict.GetValue("FormAudioExport.TracksToExport", label2.Text);
            label3.Text = CDict.GetValue("FormAudioExport.OutputFile", label3.Text);
            btnOk.Text = CDict.GetValue("Generic.Button.OK", btnOk.Text);
            btnCancel.Text = CDict.GetValue("Generic.Button.Cancel", btnCancel.Text);
        }

        private void frmAudioExport_Shown(object sender, EventArgs e)
        {
            if (m_projectsHelper != null)
            {
                string extraname = "";

                foreach (trkItem trk in chks.CheckedItems)
                {
                    extraname += trk.shortname + "_";
                }

                extraname = extraname.Substring(0, extraname.Length - 1);

                string fn = Path.Combine( saveFileDialog1.InitialDirectory, 
                                          DateTime.Now.ToString("yyyyMMdd_HH-mm") + "_" +
                                          Path.GetFileName(m_projectsHelper.CurrentProject.Project_Title + "(" + extraname + ").wav"));

                ttOutputFile.SetToolTip(txtFile, fn);
                txtFile.Tag = fn;
                txtFile.Text = Path.GetFileName(fn);
            }
        }
    }
}
