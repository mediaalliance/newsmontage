﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NewsMontange.Warnings;

namespace NewsMontange.Forms
{
    public partial class frmWarningList : Form
    {
        private CAppWarnings warnings = null;
        private Font TitleFont;
        private Font MessageFont;


        public frmWarningList(CAppWarnings warnings)
        {
            InitializeComponent();

            this.warnings = warnings;

            warnings.OnWarningAdded     += new CAppWarnings.AW_OnWarningAdded(warnings_OnWarningAdded);
            warnings.OnWarningRemoved   += new CAppWarnings.AW_OnWarningRemoved(warnings_OnWarningRemoved);

            TitleFont = new Font(Font.FontFamily, 8, FontStyle.Bold);
            MessageFont = new Font(Font.FontFamily, 9, FontStyle.Italic);

            RefreshList();
        }





        void warnings_OnWarningAdded(object Sender, CAppWarnInfo warn)
        {
            RefreshList();
        }

        void warnings_OnWarningRemoved(object Sender)
        {
            RefreshList();
        }



        private void RefreshList()
        {
            lst.Items.Clear();
            lst.Items.AddRange(warnings.Warnings.ToArray());

            btnSettingPage.Visible = false;

            if (warnings.Warnings.Where(X => X.context == WarnContext.ini_configuration).ToList().Count > 0)
            {
                btnSettingPage.Visible = true;
            }

            if (warnings.Warnings.Count == 0)
            {
                //this.Hide();
            }
        }



        private void lst_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index >= 0)
            {
                e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

                CAppWarnInfo me = (CAppWarnInfo)lst.Items[e.Index];

                e.DrawBackground();

                int x_pos = 1;

                switch (me.context)
                {
                    case WarnContext.ini_configuration:
                        e.Graphics.DrawImage(Properties.Resources.WarnContext_Ini,
                                            new Rectangle(e.Bounds.Location, Properties.Resources.WarnContext_Ini.Size),
                                            new Rectangle(new Point(0, 0), Properties.Resources.WarnContext_Ini.Size),
                                            GraphicsUnit.Pixel);
                                            
                        break;
                    case WarnContext.external:
                        e.Graphics.DrawImage(Properties.Resources.WarnContext_External,
                                            new Rectangle(e.Bounds.Location, Properties.Resources.WarnContext_Ini.Size),
                                            new Rectangle(new Point(0, 0), Properties.Resources.WarnContext_Ini.Size),
                                            GraphicsUnit.Pixel);
                        break;
                    case WarnContext.application_input:
                        e.Graphics.DrawImage(Properties.Resources.WarnContext_Gui,
                                            new Rectangle(e.Bounds.Location, Properties.Resources.WarnContext_Ini.Size),
                                            new Rectangle(new Point(0, 0), Properties.Resources.WarnContext_Ini.Size),
                                            GraphicsUnit.Pixel);
                        break;
                    case WarnContext.project:
                        e.Graphics.DrawImage(Properties.Resources.WarnContext_Project,
                                            new Rectangle(e.Bounds.Location, Properties.Resources.WarnContext_Ini.Size),
                                            new Rectangle(new Point(0, 0), Properties.Resources.WarnContext_Ini.Size),
                                            GraphicsUnit.Pixel);
                        break;
                }

                switch (me.type)
                {
                    case WarnType.generic:
                        break;
                    case WarnType.fileNotFound:
                        break;
                    case WarnType.pathNotFound:
                        break;
                    case WarnType.invalidValue:
                        break;
                }

                x_pos += Properties.Resources.WarnContext_Ini.Width + 4;

                e.Graphics.DrawString(me.Title, TitleFont, Brushes.LightYellow, x_pos, e.Bounds.Y + 2);

                e.Graphics.DrawString(me.Message, MessageFont, Brushes.Silver, new RectangleF(e.Bounds.X + 47, e.Bounds.Y + 16, Width - x_pos, 50 - 16));


                e.DrawFocusRectangle();
            }
        }
        
        private void frmWarningList_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                this.Hide();
            }
        }


        private void btnSettingPage_Click(object sender, EventArgs e)
        {
            Program.Settings.RequestForm();
        }
        
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
