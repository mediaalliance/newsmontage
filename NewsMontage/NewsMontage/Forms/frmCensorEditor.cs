﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MediaAlliance.AV;
using MediaAlliance.Controls.MxpTimeLine.Objects;
using System.IO;
using MediaAlliance.Controls.MxpTimeLine;
using MediaAlliance.AV.CensorTools;

namespace NewsMontange.Forms
{
    public partial class frmCensorEditor : Form
    {
        private CMxpTL_ClipInfo m_clipInfo = null;
        private Source m_previewsrc = null;

        private frmCensorArea m_selectedArea = null;
        private List<frmCensorArea> area_objects = new List<frmCensorArea>();

        private CCensorTimeline m_censorTimeline = new CCensorTimeline();

        public frmCensorEditor()
        {
            InitializeComponent();
        }


        #region PUBLIC PROPERTIES

        public CMxpTL_ClipInfo PreviewSource
        {
            get { return m_clipInfo; }
            set
            {
                if (m_clipInfo != value)
                {
                    m_clipInfo = value;

                    if (m_clipInfo.CensorInfo != null)
                    {
                        this.m_censorTimeline = (CCensorTimeline)m_clipInfo.CensorInfo;
                        Update();
                    }
                    else
                    {
                        this.m_censorTimeline = new CCensorTimeline();
                    }
                }
            }
        }

        //public CCensorTimeline CensorTimeline
        //{
        //    get
        //    {
        //        return m_censorTimeline;
        //    }
        //}

        #endregion PUBLIC PROPERTIES



        private void Update()
        {
            foreach (CTLM_Layer layer in m_censorTimeline.Layers)
            {
                frmCensorArea area = ADD_MASK(true);

                area.CensorLayer = layer;

                foreach (CTLM_Keyframe kf in layer.AllKeyframes)
                { 
                    
                }
            }
        }

        //private string GetCensorTimeline()
        //{
        //    foreach (CTLM_Layer layer in m_censorTimeline.Layers)
        //    {
        //        for (int i = 0; i < layer.AllKeyframes.Count; i++)
        //        {
        //            CTLM_Keyframe kf = layer.AllKeyframes[i];

        //            kf.Bounds = new Rectangle(  kf.Bounds.Location.X - pnlMonitor.Left, 
        //                                        kf.Bounds.Y - pnlMonitor.Top,
        //                                        kf.Bounds.Width,
        //                                        kf.Bounds.Height);
        //        }
        //    }

        //    return CensorTimeline.Save();
        //}


        #region FORM EVENTS

        private void frmCensorEditor_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (CTLM_Layer layer in m_censorTimeline.Layers)
            {
                int x = 0;
                int y = 0;
                int w = 0;
                int h = 0;

                foreach (CTLM_Keyframe kf in layer.AllKeyframes)
                { 
                    x = kf.Bounds.X - pnlMonitor.Left;
                    y = kf.Bounds.Y - pnlMonitor.Top;

                    x = x * 720 / pnlMonitor.Width;
                    y = y * 576 / pnlMonitor.Height;
                    w = kf.Bounds.Width * 720 / pnlMonitor.Width;
                    h = kf.Bounds.Height * 576 / pnlMonitor.Height;

                    Rectangle realrect = new Rectangle(x, y, w, h);

                    kf.Bounds = realrect;
                }
            }


            this.m_censorTimeline.SaveFile(m_clipInfo.filename + ".censor");

            m_previewsrc.Close();
            m_previewsrc = null;
        }

        private void frmCensorEditor_Shown(object sender, EventArgs e)
        {
            if (m_clipInfo != null)
            {
                if (!Program.Settings.Options.PREVIEW_ENGINE_DS_ONLY)
                {
                    if (Path.GetExtension(m_clipInfo.filename).ToLower().Contains("mov"))
                        m_previewsrc = new QTPreview(Program.Settings.Options.TIMELINE_AUDIOSCRUB);
                    else
                        m_previewsrc = new DSPreview(Program.Settings.Options.TIMELINE_AUDIOSCRUB);
                }
                else
                {
                    m_previewsrc = new DSPreview(Program.Settings.Options.TIMELINE_AUDIOSCRUB);
                }

                pnlMonitor.Controls.Add(m_previewsrc);
                m_previewsrc.NewInstance(m_clipInfo.filename);
                m_previewsrc.Visible = true;


                Timeline.Enabled = true;
                Timeline.HideMarks();
                Timeline.TCOUT = m_clipInfo.Tcout;
                Timeline.ZoomAll();

                Timeline.Tracks.AddTrack("MAIN", enMxpTrackType.Video, 85, null);
                Timeline.Tracks[0].ThumbLayout = MxpClipThumbLayout.allPossible;
                Timeline.Tracks[0].AddClip("", enCMxpClipType.Video, m_clipInfo, null, 0, m_clipInfo.Tcout, 0, m_clipInfo.Tcout);
                //Timeline_OnZoomChanged(Timeline, Timeline.ZoomIn, Timeline.ZoomOut);
                Timeline.OnInternalEngineEvent += new MXPTL_OnInternalEngineEvent(Timeline_OnInternalEngineEvent);

                Timeline.ZoomAll();

                Timeline.InitializeInternalEngine();
            }
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            Timeline.InternalEngine.Play();
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            Timeline.InternalEngine.Pause();
        }

        private void btnFrameFFW_Click(object sender, EventArgs e)
        {
            Timeline.InternalEngine.Pause();
            Timeline.CurrentPosition++;
        }

        private void btnFrameREW_Click(object sender, EventArgs e)
        {
            Timeline.InternalEngine.Pause();
            Timeline.CurrentPosition--;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (m_selectedArea != null)
            {
                if (m_selectedArea.CensorLayer != null)
                {
                    CTLM_Keyframe kf = m_selectedArea.CensorLayer.AddKeyframe(Timeline.CurrentPosition);
                    kf.Bounds = m_selectedArea.Bounds;
                }
            }
        }

        private void btnAddMask_Click(object sender, EventArgs e)
        {
            ADD_MASK(false);

            addKF.Enabled = true;
        }

        void frm1_GotFocus(object sender, EventArgs e)
        {
            m_selectedArea = (frmCensorArea)sender;
        }

        #endregion FORM EVENTS



        #region TIMELINE EVENTS

        private void Timeline_OnChangePosition(object Sender, long position, bool Internal)
        {
            if (Internal)
            {
                m_previewsrc.Seek(position);

                try
                {

                    List<CTLM_Keyframe> kfs = m_censorTimeline.GetKeyAt(position);

                    foreach (CTLM_Keyframe kf in kfs)
                    {
                        foreach (Control ctl in pnlTop.Controls)
                        {
                            if (ctl is frmCensorArea)
                            {
                                frmCensorArea area = (frmCensorArea)ctl;

                                if (area.CensorLayer == kf.Owner)
                                {
                                    area.Bounds = kf.Bounds;
                                    break;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                { 
                
                }
            }
        }

        private void Timeline_OnInternalEngineEvent(object Sender, MediaAlliance.Controls.MxpTimeLine.Engine.InternEngineEventHandle e)
        {
            switch (e.Type)
            {
                case MediaAlliance.Controls.MxpTimeLine.Engine.enIEngineEventType.undefined:
                    break;
                case MediaAlliance.Controls.MxpTimeLine.Engine.enIEngineEventType.onPlay:
                    m_previewsrc.Play();
                    break;
                case MediaAlliance.Controls.MxpTimeLine.Engine.enIEngineEventType.onPause:
                    m_previewsrc.Pause();
                    break;
                case MediaAlliance.Controls.MxpTimeLine.Engine.enIEngineEventType.onInitialized:
                    break;
                default:
                    break;
            }
        }

        #endregion TIMELINE EVENTS



        private frmCensorArea ADD_MASK(bool loading)
        {
            frmCensorArea frm1 = new frmCensorArea();

            if(!loading)
                frm1.CensorLayer = m_censorTimeline.AddLayer(CensorMaskType.pixelMed);

            frm1.TopLevel = false;
            frm1.Bounds = new Rectangle(20, 20, 200, 100);
            pnlTop.Controls.Add(frm1);
            frm1.Visible = true;
            frm1.BringToFront();
            frm1.Left = 10;
            frm1.Top = 10;
            frm1.Width = 100;
            frm1.Height = 100;
            frm1.Opacity = 0.7d;

            frm1.GotFocus += new EventHandler(frm1_GotFocus);

            area_objects.Add(frm1);

            m_selectedArea = frm1;

            return frm1;
        }

    }
}
