﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MediaAlliance.Controls.MxpTimeLine;
using MediaAlliance.Controls.MxpTimeLine.Objects;

namespace NewsMontange.Forms
{
    public partial class frmClipDebug : Form
    {
        private MxpTmeLine m_timeline = null;
        private CMxpClip m_currentclip = null;

        public frmClipDebug(MxpTmeLine timeline)
        {
            m_timeline = timeline;
            InitializeComponent();

            m_timeline.OnChangeClipSelection += new MXPTL_OnChangeClipSelection(m_timeline_OnChangeClipSelection);
        }

        private void UpdateInfo()
        {
            try
            {
                grid.Rows.Clear();

                if (m_currentclip != null)
                {
                    grid.Rows.Add("TimcodeIn", m_currentclip.TimcodeIn + " (" + Frame2TC(m_currentclip.TimcodeIn, m_timeline.FPS) + ")");
                    grid.Rows.Add("ClippingIn", m_currentclip.ClippingIn + " (" + Frame2TC(m_currentclip.ClippingIn, m_timeline.FPS) + ")");
                    grid.Rows.Add("TimecodeOut", m_currentclip.TimecodeOut + " (" + Frame2TC(m_currentclip.TimecodeOut, m_timeline.FPS) + ")");
                    grid.Rows.Add("ClippingOut", m_currentclip.ClippingOut + " (" + Frame2TC(m_currentclip.ClippingOut, m_timeline.FPS) + ")");
                    grid.Rows.Add("X", m_currentclip.X.ToString());
                    grid.Rows.Add("Virtual_X", m_currentclip.Virtual_X.ToString());
                    grid.Rows.Add("Width", m_currentclip.Width.ToString());
                    grid.Rows.Add("Virtual_Width", m_currentclip.Virtual_Width.ToString());
                    grid.Rows.Add("FirstVisible_X", m_currentclip.DisplayInfo.FirstVisible_X.ToString());
                    grid.Rows.Add("LastVisible_X", m_currentclip.DisplayInfo.LastVisible_X.ToString());
                    grid.Rows.Add("FirstVisible_TC", m_currentclip.DisplayInfo.FirstVisible_TC.ToString());
                    grid.Rows.Add("FirstVisible_TC_OfClip", m_currentclip.DisplayInfo.FirstVisible_TC_OfClip.ToString());
                    grid.Rows.Add("LastVisible_TC", m_currentclip.DisplayInfo.LastVisible_TC.ToString());
                    grid.Rows.Add("LastVisible_TC_OfClip", m_currentclip.DisplayInfo.LastVisible_TC_OfClip.ToString());
                }
            }
            catch { }
        }

        void m_timeline_OnChangeClipSelection(object Sender, MediaAlliance.Controls.MxpTimeLine.Objects.CMxpClip[] SelectedClips)
        {
            if (m_currentclip != null)
            {
                m_currentclip.OnDisplayInfoChange -= m_currentclip_OnDisplayInfoChange;
                m_currentclip.OnChangeTitle -= m_currentclip_OnChangeTitle;
            }

            if (SelectedClips.Length > 0)
            {
                m_currentclip = SelectedClips[0];

                m_currentclip.OnDisplayInfoChange += new CMxpClip.MxpClip_OnDisplayInfoChange(m_currentclip_OnDisplayInfoChange);
                m_currentclip.OnChangeTitle += new CMxpClip.MxpClip_OnChangeTitle(m_currentclip_OnChangeTitle);
            }
            else
            {
                m_currentclip = null;
            }

            UpdateInfo();
        }

        void m_currentclip_OnChangeTitle(object Sender)
        {
            UpdateInfo();
        }

        void m_currentclip_OnDisplayInfoChange(object Sender)
        {
            UpdateInfo();
        }

        private void frmClipDebug_Shown(object sender, EventArgs e)
        {
            this.Left = Screen.PrimaryScreen.Bounds.Right - Width - 5;
            this.Top = Screen.PrimaryScreen.WorkingArea.Bottom - Height - 5;
        }
        
        public string Frame2TC(long frame, float fps)
        {
            string result = "";

            long frames = Math.Abs(frame);
            long ffTot = Math.Abs(frames);

            int ss  = (int)(frames / fps);
            int ff  = (int)(frames - (ss * fps));
            int mm  = (int)(ss / 60);
            ss      = ss - mm * 60;
            int hh  = (int)(mm / 60);
            mm      = mm - hh * 60;

            result += hh.ToString().PadLeft(2, '0') + ":";
            result += mm.ToString().PadLeft(2, '0') + ":";
            result += ss.ToString().PadLeft(2, '0') + ":";
            result += ff.ToString().PadLeft(2, '0');

            if (frame < 0) result = "-" + result;


            return result;
        }
    }
}
