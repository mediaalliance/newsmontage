﻿namespace NewsMontange.Forms
{
    partial class frmStartup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("LAST 2 DAYS", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("Oldest", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup3 = new System.Windows.Forms.ListViewGroup("LAST 2 DAYS", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup4 = new System.Windows.Forms.ListViewGroup("Oldest", System.Windows.Forms.HorizontalAlignment.Left);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStartup));
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.radPageView1 = new Telerik.WinControls.UI.RadPageView();
            this.pageMRU = new Telerik.WinControls.UI.RadPageViewPage();
            this.lstMRU = new System.Windows.Forms.ListView();
            this.clm_filename = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clm_data = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmAuthor = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mnuProjects = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.removeProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.picListSrc_Style = new System.Windows.Forms.PictureBox();
            this.pageNEW = new Telerik.WinControls.UI.RadPageViewPage();
            this.picAlert = new System.Windows.Forms.PictureBox();
            this.lblRealTitle = new System.Windows.Forms.Label();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.txtScript = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.txtStoryTitle = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.txtAuthor = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.txtShow = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.txtTitle = new Telerik.WinControls.UI.RadTextBox();
            this.pageNEWSNET = new Telerik.WinControls.UI.RadPageViewPage();
            this.nnList = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel2 = new System.Windows.Forms.Panel();
            this.picRefresh = new System.Windows.Forms.PictureBox();
            this.lblNNProjectMessage = new System.Windows.Forms.Label();
            this.picNNProjectAlert = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnOk = new Telerik.WinControls.UI.RadButton();
            this.radButton2 = new Telerik.WinControls.UI.RadButton();
            this.btnLoad = new Telerik.WinControls.UI.RadButton();
            this.openFile = new System.Windows.Forms.OpenFileDialog();
            this.lblVer = new System.Windows.Forms.Label();
            this.toolTip_alert = new System.Windows.Forms.ToolTip(this.components);
            this.lblFullFilename = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).BeginInit();
            this.radPageView1.SuspendLayout();
            this.pageMRU.SuspendLayout();
            this.mnuProjects.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picListSrc_Style)).BeginInit();
            this.pageNEW.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAlert)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtScript)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStoryTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAuthor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
            this.pageNEWSNET.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picRefresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picNNProjectAlert)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnLoad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // radButton1
            // 
            this.radButton1.Location = new System.Drawing.Point(91, 280);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(106, 53);
            this.radButton1.TabIndex = 0;
            this.radButton1.Text = "radButton1";
            // 
            // radPageView1
            // 
            this.radPageView1.BackColor = System.Drawing.Color.Transparent;
            this.radPageView1.Controls.Add(this.pageMRU);
            this.radPageView1.Controls.Add(this.pageNEW);
            this.radPageView1.Controls.Add(this.pageNEWSNET);
            this.radPageView1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radPageView1.Location = new System.Drawing.Point(12, 178);
            this.radPageView1.Name = "radPageView1";
            this.radPageView1.SelectedPage = this.pageMRU;
            this.radPageView1.Size = new System.Drawing.Size(707, 345);
            this.radPageView1.TabIndex = 1;
            this.radPageView1.ThemeName = "ControlDefault";
            this.radPageView1.SelectedPageChanged += new System.EventHandler(this.radPageView1_SelectedPageChanged);
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.radPageView1.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.radPageView1.GetChildAt(0))).BackColor = System.Drawing.Color.Transparent;
            // 
            // pageMRU
            // 
            this.pageMRU.Controls.Add(this.lstMRU);
            this.pageMRU.Controls.Add(this.panel1);
            this.pageMRU.Location = new System.Drawing.Point(10, 40);
            this.pageMRU.Name = "pageMRU";
            this.pageMRU.Size = new System.Drawing.Size(686, 294);
            this.pageMRU.Text = "Recent Projects";
            // 
            // lstMRU
            // 
            this.lstMRU.BackColor = System.Drawing.Color.DarkGray;
            this.lstMRU.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstMRU.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clm_filename,
            this.clm_data,
            this.clmAuthor});
            this.lstMRU.ContextMenuStrip = this.mnuProjects;
            this.lstMRU.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstMRU.FullRowSelect = true;
            listViewGroup1.Header = "LAST 2 DAYS";
            listViewGroup1.Name = "grpLast2days";
            listViewGroup2.Header = "Oldest";
            listViewGroup2.Name = "grpOldest";
            this.lstMRU.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2});
            this.lstMRU.HideSelection = false;
            this.lstMRU.Location = new System.Drawing.Point(0, 30);
            this.lstMRU.MultiSelect = false;
            this.lstMRU.Name = "lstMRU";
            this.lstMRU.Size = new System.Drawing.Size(686, 264);
            this.lstMRU.TabIndex = 0;
            this.lstMRU.UseCompatibleStateImageBehavior = false;
            this.lstMRU.View = System.Windows.Forms.View.Tile;
            this.lstMRU.SelectedIndexChanged += new System.EventHandler(this.lstMRU_SelectedIndexChanged);
            this.lstMRU.DoubleClick += new System.EventHandler(this.lstMRU_DoubleClick);
            this.lstMRU.KeyUp += new System.Windows.Forms.KeyEventHandler(this.lstMRU_KeyUp);
            // 
            // clm_filename
            // 
            this.clm_filename.Text = "File";
            this.clm_filename.Width = 250;
            // 
            // clm_data
            // 
            this.clm_data.Text = "Date";
            this.clm_data.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.clm_data.Width = 140;
            // 
            // clmAuthor
            // 
            this.clmAuthor.Text = "Author";
            this.clmAuthor.Width = 140;
            // 
            // mnuProjects
            // 
            this.mnuProjects.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeProjectToolStripMenuItem});
            this.mnuProjects.Name = "mnuProjects";
            this.mnuProjects.Size = new System.Drawing.Size(158, 26);
            this.mnuProjects.Opening += new System.ComponentModel.CancelEventHandler(this.mnuProjects_Opening);
            // 
            // removeProjectToolStripMenuItem
            // 
            this.removeProjectToolStripMenuItem.Image = global::NewsMontange.Properties.Resources.Delete_icon_small;
            this.removeProjectToolStripMenuItem.Name = "removeProjectToolStripMenuItem";
            this.removeProjectToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.removeProjectToolStripMenuItem.Text = "Remove project";
            this.removeProjectToolStripMenuItem.Click += new System.EventHandler(this.removeProjectToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gray;
            this.panel1.Controls.Add(this.picListSrc_Style);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(686, 30);
            this.panel1.TabIndex = 1;
            // 
            // picListSrc_Style
            // 
            this.picListSrc_Style.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picListSrc_Style.Image = global::NewsMontange.Properties.Resources.ListSrc_ico_icon;
            this.picListSrc_Style.Location = new System.Drawing.Point(663, 4);
            this.picListSrc_Style.Name = "picListSrc_Style";
            this.picListSrc_Style.Size = new System.Drawing.Size(20, 20);
            this.picListSrc_Style.TabIndex = 1;
            this.picListSrc_Style.TabStop = false;
            this.picListSrc_Style.Click += new System.EventHandler(this.picListSrc_Style_Click);
            // 
            // pageNEW
            // 
            this.pageNEW.Controls.Add(this.picAlert);
            this.pageNEW.Controls.Add(this.lblRealTitle);
            this.pageNEW.Controls.Add(this.radLabel5);
            this.pageNEW.Controls.Add(this.txtScript);
            this.pageNEW.Controls.Add(this.radLabel4);
            this.pageNEW.Controls.Add(this.txtStoryTitle);
            this.pageNEW.Controls.Add(this.radLabel3);
            this.pageNEW.Controls.Add(this.txtAuthor);
            this.pageNEW.Controls.Add(this.radLabel2);
            this.pageNEW.Controls.Add(this.txtShow);
            this.pageNEW.Controls.Add(this.radLabel1);
            this.pageNEW.Controls.Add(this.txtTitle);
            this.pageNEW.Location = new System.Drawing.Point(10, 40);
            this.pageNEW.Name = "pageNEW";
            this.pageNEW.Size = new System.Drawing.Size(686, 294);
            this.pageNEW.Text = "New Project";
            // 
            // picAlert
            // 
            this.picAlert.Image = global::NewsMontange.Properties.Resources.alert_red;
            this.picAlert.Location = new System.Drawing.Point(23, 70);
            this.picAlert.Name = "picAlert";
            this.picAlert.Size = new System.Drawing.Size(20, 19);
            this.picAlert.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picAlert.TabIndex = 9;
            this.picAlert.TabStop = false;
            this.picAlert.Visible = false;
            this.picAlert.MouseEnter += new System.EventHandler(this.picAlert_MouseEnter);
            // 
            // lblRealTitle
            // 
            this.lblRealTitle.AutoSize = true;
            this.lblRealTitle.Font = new System.Drawing.Font("Segoe UI", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRealTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblRealTitle.Location = new System.Drawing.Point(48, 71);
            this.lblRealTitle.Name = "lblRealTitle";
            this.lblRealTitle.Size = new System.Drawing.Size(20, 17);
            this.lblRealTitle.TabIndex = 8;
            this.lblRealTitle.Text = "...";
            this.lblRealTitle.Visible = false;
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel5.Location = new System.Drawing.Point(27, 169);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(49, 25);
            this.radLabel5.TabIndex = 7;
            this.radLabel5.Text = "Script";
            // 
            // txtScript
            // 
            this.txtScript.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtScript.Location = new System.Drawing.Point(24, 195);
            this.txtScript.Multiline = true;
            this.txtScript.Name = "txtScript";
            // 
            // 
            // 
            this.txtScript.RootElement.StretchVertically = true;
            this.txtScript.Size = new System.Drawing.Size(638, 74);
            this.txtScript.TabIndex = 4;
            this.txtScript.TabStop = false;
            this.txtScript.ThemeName = "Office2007Black";
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel4.Location = new System.Drawing.Point(359, 104);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(81, 25);
            this.radLabel4.TabIndex = 6;
            this.radLabel4.Text = "Story Title";
            // 
            // txtStoryTitle
            // 
            this.txtStoryTitle.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStoryTitle.Location = new System.Drawing.Point(359, 135);
            this.txtStoryTitle.MaxLength = 255;
            this.txtStoryTitle.Name = "txtStoryTitle";
            this.txtStoryTitle.Size = new System.Drawing.Size(302, 23);
            this.txtStoryTitle.TabIndex = 3;
            this.txtStoryTitle.TabStop = false;
            this.txtStoryTitle.ThemeName = "Office2007Black";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.Location = new System.Drawing.Point(24, 104);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(58, 25);
            this.radLabel3.TabIndex = 4;
            this.radLabel3.Text = "Author";
            // 
            // txtAuthor
            // 
            this.txtAuthor.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAuthor.Location = new System.Drawing.Point(24, 135);
            this.txtAuthor.MaxLength = 255;
            this.txtAuthor.Name = "txtAuthor";
            this.txtAuthor.Size = new System.Drawing.Size(304, 23);
            this.txtAuthor.TabIndex = 2;
            this.txtAuthor.TabStop = false;
            this.txtAuthor.ThemeName = "Office2007Black";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(359, 12);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(47, 25);
            this.radLabel2.TabIndex = 2;
            this.radLabel2.Text = "Show";
            // 
            // txtShow
            // 
            this.txtShow.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtShow.Location = new System.Drawing.Point(359, 43);
            this.txtShow.MaxLength = 255;
            this.txtShow.Name = "txtShow";
            this.txtShow.Size = new System.Drawing.Size(302, 23);
            this.txtShow.TabIndex = 1;
            this.txtShow.TabStop = false;
            this.txtShow.ThemeName = "Office2007Black";
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.Location = new System.Drawing.Point(24, 12);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(39, 25);
            this.radLabel1.TabIndex = 1;
            this.radLabel1.Text = "Title";
            // 
            // txtTitle
            // 
            this.txtTitle.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitle.Location = new System.Drawing.Point(24, 43);
            this.txtTitle.MaxLength = 255;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(304, 23);
            this.txtTitle.TabIndex = 0;
            this.txtTitle.TabStop = false;
            this.txtTitle.ThemeName = "Office2007Black";
            this.txtTitle.TextChanged += new System.EventHandler(this.txtTitle_TextChanged);
            // 
            // pageNEWSNET
            // 
            this.pageNEWSNET.Controls.Add(this.nnList);
            this.pageNEWSNET.Controls.Add(this.panel2);
            this.pageNEWSNET.Location = new System.Drawing.Point(10, 40);
            this.pageNEWSNET.Name = "pageNEWSNET";
            this.pageNEWSNET.Size = new System.Drawing.Size(686, 294);
            this.pageNEWSNET.Text = "NewsNet Stories";
            // 
            // nnList
            // 
            this.nnList.BackColor = System.Drawing.Color.DarkGray;
            this.nnList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nnList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.nnList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nnList.FullRowSelect = true;
            listViewGroup3.Header = "LAST 2 DAYS";
            listViewGroup3.Name = "grpLast2days";
            listViewGroup4.Header = "Oldest";
            listViewGroup4.Name = "grpOldest";
            this.nnList.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup3,
            listViewGroup4});
            this.nnList.HideSelection = false;
            this.nnList.Location = new System.Drawing.Point(0, 30);
            this.nnList.MultiSelect = false;
            this.nnList.Name = "nnList";
            this.nnList.Size = new System.Drawing.Size(686, 264);
            this.nnList.TabIndex = 2;
            this.nnList.UseCompatibleStateImageBehavior = false;
            this.nnList.View = System.Windows.Forms.View.Tile;
            this.nnList.SelectedIndexChanged += new System.EventHandler(this.nnList_SelectedIndexChanged);
            this.nnList.DoubleClick += new System.EventHandler(this.nnList_DoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "File";
            this.columnHeader1.Width = 250;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Date";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader2.Width = 140;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Author";
            this.columnHeader3.Width = 140;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gray;
            this.panel2.Controls.Add(this.picRefresh);
            this.panel2.Controls.Add(this.lblNNProjectMessage);
            this.panel2.Controls.Add(this.picNNProjectAlert);
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(686, 30);
            this.panel2.TabIndex = 3;
            // 
            // picRefresh
            // 
            this.picRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picRefresh.Image = global::NewsMontange.Properties.Resources.view_refresh_24;
            this.picRefresh.Location = new System.Drawing.Point(637, 4);
            this.picRefresh.Name = "picRefresh";
            this.picRefresh.Size = new System.Drawing.Size(20, 20);
            this.picRefresh.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picRefresh.TabIndex = 4;
            this.picRefresh.TabStop = false;
            this.picRefresh.Click += new System.EventHandler(this.picRefresh_Click);
            // 
            // lblNNProjectMessage
            // 
            this.lblNNProjectMessage.AutoSize = true;
            this.lblNNProjectMessage.Location = new System.Drawing.Point(36, 7);
            this.lblNNProjectMessage.Name = "lblNNProjectMessage";
            this.lblNNProjectMessage.Size = new System.Drawing.Size(43, 17);
            this.lblNNProjectMessage.TabIndex = 3;
            this.lblNNProjectMessage.Text = "label1";
            // 
            // picNNProjectAlert
            // 
            this.picNNProjectAlert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picNNProjectAlert.Image = global::NewsMontange.Properties.Resources.alert_red;
            this.picNNProjectAlert.Location = new System.Drawing.Point(2, 1);
            this.picNNProjectAlert.Name = "picNNProjectAlert";
            this.picNNProjectAlert.Size = new System.Drawing.Size(28, 27);
            this.picNNProjectAlert.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picNNProjectAlert.TabIndex = 2;
            this.picNNProjectAlert.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.Image = global::NewsMontange.Properties.Resources.ListSrc_ico_icon;
            this.pictureBox2.Location = new System.Drawing.Point(663, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(20, 20);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(592, 545);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(119, 36);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "OPEN";
            this.btnOk.ThemeName = "Office2007Black";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnOk.GetChildAt(0))).Text = "OPEN";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnOk.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(223)))), ((int)(((byte)(224)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnOk.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(146)))), ((int)(((byte)(146)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnOk.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(103)))), ((int)(((byte)(103)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnOk.GetChildAt(0).GetChildAt(0))).GradientStyle = Telerik.WinControls.GradientStyles.Linear;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnOk.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnOk.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnOk.GetChildAt(0).GetChildAt(1).GetChildAt(1))).TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnOk.GetChildAt(0).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(64)))), ((int)(((byte)(104)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnOk.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.btnOk.GetChildAt(0).GetChildAt(2))).GradientAngle = 270F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.btnOk.GetChildAt(0).GetChildAt(2))).GradientStyle = Telerik.WinControls.GradientStyles.Linear;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.btnOk.GetChildAt(0).GetChildAt(2))).ForeColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.btnOk.GetChildAt(0).GetChildAt(2))).ForeColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(79)))), ((int)(((byte)(79)))));
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.btnOk.GetChildAt(0).GetChildAt(2))).ForeColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(174)))), ((int)(((byte)(174)))));
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.btnOk.GetChildAt(0).GetChildAt(2))).DefaultSize = new System.Drawing.Size(1, 1);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.btnOk.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(72)))), ((int)(((byte)(70)))));
            ((Telerik.WinControls.Primitives.FocusPrimitive)(this.btnOk.GetChildAt(0).GetChildAt(3))).InnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(186)))), ((int)(((byte)(186)))));
            // 
            // radButton2
            // 
            this.radButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton2.Location = new System.Drawing.Point(467, 545);
            this.radButton2.Name = "radButton2";
            this.radButton2.Size = new System.Drawing.Size(119, 36);
            this.radButton2.TabIndex = 3;
            this.radButton2.Text = "CANCEL";
            this.radButton2.ThemeName = "Office2007Black";
            this.radButton2.Click += new System.EventHandler(this.radButton2_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton2.GetChildAt(0))).Text = "CANCEL";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton2.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(223)))), ((int)(((byte)(224)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton2.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(146)))), ((int)(((byte)(146)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton2.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(103)))), ((int)(((byte)(103)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton2.GetChildAt(0).GetChildAt(0))).GradientStyle = Telerik.WinControls.GradientStyles.Linear;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton2.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton2.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radButton2.GetChildAt(0).GetChildAt(1).GetChildAt(1))).TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radButton2.GetChildAt(0).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(84)))), ((int)(((byte)(23)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radButton2.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radButton2.GetChildAt(0).GetChildAt(2))).GradientAngle = 270F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radButton2.GetChildAt(0).GetChildAt(2))).GradientStyle = Telerik.WinControls.GradientStyles.Linear;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radButton2.GetChildAt(0).GetChildAt(2))).ForeColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radButton2.GetChildAt(0).GetChildAt(2))).ForeColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(79)))), ((int)(((byte)(79)))));
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radButton2.GetChildAt(0).GetChildAt(2))).ForeColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(174)))), ((int)(((byte)(174)))));
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radButton2.GetChildAt(0).GetChildAt(2))).DefaultSize = new System.Drawing.Size(1, 1);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radButton2.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(72)))), ((int)(((byte)(70)))));
            ((Telerik.WinControls.Primitives.FocusPrimitive)(this.radButton2.GetChildAt(0).GetChildAt(3))).InnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(186)))), ((int)(((byte)(186)))));
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(22, 545);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(175, 36);
            this.btnLoad.TabIndex = 5;
            this.btnLoad.Text = "LOAD PROJECT";
            this.btnLoad.ThemeName = "ControlDefault";
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnLoad.GetChildAt(0))).Text = "LOAD PROJECT";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnLoad.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(223)))), ((int)(((byte)(224)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnLoad.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(146)))), ((int)(((byte)(146)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnLoad.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(103)))), ((int)(((byte)(103)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnLoad.GetChildAt(0).GetChildAt(0))).GradientStyle = Telerik.WinControls.GradientStyles.Linear;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnLoad.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnLoad.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnLoad.GetChildAt(0).GetChildAt(1).GetChildAt(1))).TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnLoad.GetChildAt(0).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(64)))), ((int)(((byte)(104)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnLoad.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.btnLoad.GetChildAt(0).GetChildAt(2))).GradientAngle = 270F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.btnLoad.GetChildAt(0).GetChildAt(2))).GradientStyle = Telerik.WinControls.GradientStyles.Linear;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.btnLoad.GetChildAt(0).GetChildAt(2))).ForeColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.btnLoad.GetChildAt(0).GetChildAt(2))).ForeColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(79)))), ((int)(((byte)(79)))));
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.btnLoad.GetChildAt(0).GetChildAt(2))).ForeColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(174)))), ((int)(((byte)(174)))));
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.btnLoad.GetChildAt(0).GetChildAt(2))).DefaultSize = new System.Drawing.Size(1, 1);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.btnLoad.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(72)))), ((int)(((byte)(70)))));
            ((Telerik.WinControls.Primitives.FocusPrimitive)(this.btnLoad.GetChildAt(0).GetChildAt(3))).InnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(186)))), ((int)(((byte)(186)))));
            // 
            // lblVer
            // 
            this.lblVer.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVer.Location = new System.Drawing.Point(444, 149);
            this.lblVer.Name = "lblVer";
            this.lblVer.Size = new System.Drawing.Size(268, 23);
            this.lblVer.TabIndex = 6;
            this.lblVer.Text = "ver. beta";
            this.lblVer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblFullFilename
            // 
            this.lblFullFilename.AutoSize = true;
            this.lblFullFilename.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFullFilename.ForeColor = System.Drawing.Color.Silver;
            this.lblFullFilename.Location = new System.Drawing.Point(18, 521);
            this.lblFullFilename.Name = "lblFullFilename";
            this.lblFullFilename.Size = new System.Drawing.Size(16, 15);
            this.lblFullFilename.TabIndex = 7;
            this.lblFullFilename.Text = "...";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::NewsMontange.Properties.Resources.NM_StartupScreen1;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(707, 160);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // frmStartup
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(731, 593);
            this.Controls.Add(this.lblFullFilename);
            this.Controls.Add(this.lblVer);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.radButton2);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.radPageView1);
            this.Controls.Add(this.radButton1);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmStartup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Media Alliance - News Montage Startup";
            this.Load += new System.EventHandler(this.frmStartup_Load);
            this.Shown += new System.EventHandler(this.frmStartup_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).EndInit();
            this.radPageView1.ResumeLayout(false);
            this.pageMRU.ResumeLayout(false);
            this.mnuProjects.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picListSrc_Style)).EndInit();
            this.pageNEW.ResumeLayout(false);
            this.pageNEW.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAlert)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtScript)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStoryTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAuthor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
            this.pageNEWSNET.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picRefresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picNNProjectAlert)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnLoad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private Telerik.WinControls.UI.RadPageView radPageView1;
        private Telerik.WinControls.UI.RadPageViewPage pageMRU;
        private Telerik.WinControls.UI.RadPageViewPage pageNEW;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadPageViewPage pageNEWSNET;
        private Telerik.WinControls.UI.RadButton btnOk;
        private Telerik.WinControls.UI.RadButton radButton2;
        private System.Windows.Forms.ListView lstMRU;
        private System.Windows.Forms.PictureBox pictureBox1;
        public Telerik.WinControls.UI.RadTextBox txtScript;
        public Telerik.WinControls.UI.RadTextBox txtStoryTitle;
        public Telerik.WinControls.UI.RadTextBox txtAuthor;
        public Telerik.WinControls.UI.RadTextBox txtShow;
        public Telerik.WinControls.UI.RadTextBox txtTitle;
        private Telerik.WinControls.UI.RadButton btnLoad;
        private System.Windows.Forms.OpenFileDialog openFile;
        private System.Windows.Forms.ColumnHeader clm_filename;
        private System.Windows.Forms.ColumnHeader clm_data;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox picListSrc_Style;
        private System.Windows.Forms.ColumnHeader clmAuthor;
        private System.Windows.Forms.Label lblVer;
        private System.Windows.Forms.PictureBox picAlert;
        private System.Windows.Forms.ToolTip toolTip_alert;
        public System.Windows.Forms.Label lblRealTitle;
        private System.Windows.Forms.ListView nnList;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblFullFilename;
        private System.Windows.Forms.Label lblNNProjectMessage;
        private System.Windows.Forms.PictureBox picNNProjectAlert;
        private System.Windows.Forms.PictureBox picRefresh;
        private System.Windows.Forms.ContextMenuStrip mnuProjects;
        private System.Windows.Forms.ToolStripMenuItem removeProjectToolStripMenuItem;

    }
}