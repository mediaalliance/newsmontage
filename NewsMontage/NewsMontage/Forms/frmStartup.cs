﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;



using NewsMontange.Helpers;
using System.IO;
using NewsMontange.Language;
using System.Xml;


namespace NewsMontange.Forms
{
    public partial class frmStartup : Form
    {
        public enum enStartUp_Action
        { 
            newProject,
            openRecent,
            newsnetProject,
            customFile
        }

        public enStartUp_Action StartUpAction = enStartUp_Action.openRecent;

        public string CustomProjectFile = "";
        public string newsNet_Selected_Project = "";


        public frmStartup()
        {
            InitializeComponent();

            lblVer.Text = CDict.GetValue("LoadingForm.SoftwareVersion", "Version") + " " + Application.ProductVersion;// +" beta";

            lblRealTitle.Text = "";

            ImageList ims = new ImageList();

            ims.TransparentColor = Color.Transparent;
            ims.ColorDepth = ColorDepth.Depth32Bit;
            ims.ImageSize = new System.Drawing.Size(43, 64);
            ims.Images.Add(Properties.Resources.NM_PRJ_01);
            ims.Images.Add(Properties.Resources.NN_ICON_01);


            lstMRU.LargeImageList = ims;
            lstMRU.SmallImageList = ims;

            nnList.LargeImageList = ims;
            nnList.SmallImageList = ims;

            RefreshMRU();
            RefreshNewsNet();

            pageMRU.Select();
        }
        
        private void frmStartup_Load(object sender, EventArgs e)
        {
            if (File.Exists(Program.LanguageFile))
            {
                CDict.AddFromFile(Program.LanguageFile);
            }

            pageMRU.Text = CDict.GetValue("FormStartup.RecentProject", pageMRU.Text);
            pageNEW.Text = CDict.GetValue("FormStartup.NewProject", pageNEW.Text);
            pageNEWSNET.Text = CDict.GetValue("FormStartup.NewsNetStory", pageNEWSNET.Text);
            btnLoad.Text = CDict.GetValue("FormStartup.ButtonLoadProject", btnLoad.Text);

            radLabel1.Text = CDict.GetValue("Generic.Text.Title", radLabel1.Text);
            radLabel2.Text = CDict.GetValue("Generic.Text.Show", radLabel2.Text);
            radLabel3.Text = CDict.GetValue("Generic.Text.Author", radLabel3.Text);
            radLabel4.Text = CDict.GetValue("Generic.Text.StoryTitle", radLabel4.Text);
            radLabel5.Text = CDict.GetValue("Generic.Text.Script", radLabel5.Text);

            radButton2.Text = CDict.GetValue("Generic.Button.Cancel", radButton2.Text);
            btnOk.Text = CDict.GetValue("Generic.Button.Open", btnOk.Text);

        }



        private void RefreshMRU()
        {
            lstMRU.Items.Clear();
            lstMRU.Groups.Clear();

            MRUTools.MRUItem[] items = MRUTools.GetAll();

            ListViewGroup grp_old = new ListViewGroup(CDict.GetValue("FormStartup.MRUListGroup.Oldest", "Oldest"));
            ListViewGroup grp_rec = new ListViewGroup(CDict.GetValue("FormStartup.MRUListGroup.Last2days", "Last two days"));

            lstMRU.Groups.Add(grp_rec);
            lstMRU.Groups.Add(grp_old);

            foreach (MRUTools.MRUItem item in items)
            {
                ListViewGroup grp = grp_old;

                if (((TimeSpan)(DateTime.Now - item.date)).TotalHours < 48)
                {
                    grp = grp_rec;
                }

                ListViewItem it     = new ListViewItem(new string[] 
                                                                    { 
                                                                        Path.GetFileName(item.Filename), 
                                                                        item.date.ToString("dd/MM/yyyy HH.mm"),
                                                                        (item.Author.Trim() != "")? item.Author : CDict.GetValue("FormStartup.MRUListGroup.noAuthor", "no author")
                                                                    });
                it.ImageIndex       = 0;
                it.BackColor        = lstMRU.BackColor;
                it.Tag              = item;
                it.Group            = grp;

                lstMRU.Items.Add(it);
            }

            if (lstMRU.Items.Count > 0)
            {
                lstMRU.Focus();
                lstMRU.Items[0].Selected = true;
            }
        }

        private void RefreshNewsNet()
        {
            nnList.Items.Clear();
            nnList.Groups.Clear();

            ListViewGroup grp_old = new ListViewGroup(CDict.GetValue("FormStartup.MRUListGroup.Oldest", "Oldest"));
            ListViewGroup grp_rec = new ListViewGroup(CDict.GetValue("FormStartup.MRUListGroup.Last2days", "Last two days"));

            if (Directory.Exists(Program.Settings.Project_Options.NEWSNET_PRJ_PATH))
            {
                string[] proj_files = Directory.GetFiles(Program.Settings.Project_Options.NEWSNET_PRJ_PATH, "*.todo", SearchOption.AllDirectories);

                foreach (string proj_file in proj_files)
                {
                    FileInfo fi = new FileInfo(proj_file);
                    string author = "No Author";

                    try
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(proj_file);

                        if (doc["media"] != null)
                        {
                            if (doc["media"].Attributes["author"] != null)
                            {
                                author = doc["media"].Attributes["author"].Value;
                            }
                        }

                    }
                    catch
                    { }

                    ListViewGroup grp = grp_old;

                    if (((TimeSpan)(DateTime.Now - fi.LastWriteTime)).TotalHours < 48)
                    {
                        grp = grp_rec;
                    }

                    ListViewItem it = new ListViewItem(new string[] 
                                                        { 
                                                            fi.Name, 
                                                            fi.LastWriteTime.ToString("dd/MM/yyyy HH.mm"),
                                                            (author.Trim() != "")? author : CDict.GetValue("FormStartup.MRUListGroup.noAuthor", "no author")
                                                        });
                    it.ImageIndex = 1;
                    it.BackColor = lstMRU.BackColor;
                    it.Tag = proj_file;
                    it.Group = grp;
                    

                    nnList.Items.Add(it);

                }
            }
        }


        private void lstMRU_DoubleClick(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void lstMRU_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && lstMRU.SelectedItems.Count > 0)
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void lstMRU_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstMRU.SelectedItems.Count > 0)
            {
                lblFullFilename.Text = ((MRUTools.MRUItem)lstMRU.SelectedItems[0].Tag).Filename;
            }
        }



        private void radPageView1_SelectedPageChanged(object sender, EventArgs e)
        {
            lblFullFilename.Text = "";
            btnOk.Enabled = true;

            if (radPageView1.SelectedPage == pageMRU)
            {
                StartUpAction = enStartUp_Action.openRecent;

                RefreshMRU();
                RefreshNewsNet();
                
                btnOk.Enabled = lstMRU.SelectedItems.Count > 0;
            }
            else if (radPageView1.SelectedPage == pageNEW)
            {
                StartUpAction = enStartUp_Action.newProject;
            }
            else
            {
                StartUpAction = enStartUp_Action.newsnetProject;
            }
        }



        public MRUTools.MRUItem RecentProject
        {
            get
            {
                if (lstMRU.SelectedItems.Count > 0)
                {
                    return (MRUTools.MRUItem)lstMRU.SelectedItems[0].Tag;
                }

                return null;
            }
        }

        public string NewsNet_Selected_Project
        {
            get 
            {
                return newsNet_Selected_Project;
            }
        }


        private void btnOk_Click(object sender, EventArgs e)
        {
            if (radPageView1.SelectedPage == pageMRU)
            {
                StartUpAction = enStartUp_Action.openRecent;

                if (lstMRU.SelectedItems.Count == 0)
                {
                    return;
                }
            }
            else if (radPageView1.SelectedPage == pageNEW)
            {
                StartUpAction = enStartUp_Action.newProject;

                if (lblRealTitle.Text.Trim() == "")
                {
                    MessageBox.Show(CDict.GetValue("FormStartup.MBox.InsertTitle", "Please insert a title before continue!"));
                    return;
                }
            }
            else
            {
                StartUpAction = enStartUp_Action.newsnetProject;
            }

            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            openFile.Filter = "NM Project|*.nmprj";
            openFile.InitialDirectory = Program.Settings.Project_Options.PROJECTS_PATH;

            if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                CustomProjectFile = openFile.FileName;
                StartUpAction = enStartUp_Action.customFile;
                DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void picListSrc_Style_Click(object sender, EventArgs e)
        {
            switch (lstMRU.View)
            {
                case View.Details:
                    lstMRU.View = View.Tile;
                    break;                
                case View.Tile:
                    lstMRU.View = View.Details;
                    break;
            }
        }


        private char[] invalid_char = Path.GetInvalidFileNameChars();

        private void txtTitle_TextChanged(object sender, EventArgs e)
        {
            if (txtTitle.Text.IndexOfAny(invalid_char) >= 0)
            {
                picAlert.Visible        = true;
                lblRealTitle.Visible    = true;

                string real_title       = "";

                foreach (char ch in txtTitle.Text)
                {
                    if (!invalid_char.Contains<char>(ch))
                    {
                        real_title += ch;
                    }
                }

                lblRealTitle.Text = real_title;
            }
            else
            {
                lblRealTitle.Text       = txtTitle.Text;
                picAlert.Visible        = false;
                lblRealTitle.Visible    = false;
            }
        }

        private void picAlert_MouseEnter(object sender, EventArgs e)
        {
            toolTip_alert.Show(CDict.GetValue("FormStartup.ToolTip.TitleCharsFix", 
                                                "Title will be the name of project file, so some chars is not allowed... Title become :"),
                                this,
                                this.PointToClient(Cursor.Position),
                                4000);

        }

        private void nnList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (nnList.SelectedItems.Count > 0)
            {
                newsNet_Selected_Project = (string)(nnList.SelectedItems[0].Tag);
            }
            else
            {
                newsNet_Selected_Project = "";
            }
        }

        private void frmStartup_Shown(object sender, EventArgs e)
        {
            picNNProjectAlert.Visible = false;
            lblNNProjectMessage.Text = "";
            picRefresh.Enabled = true;

            if (Program.Settings.Project_Options.NEWSNET_PRJ_PATH.Trim() == "")
            {
                if (radPageView1.Pages.Contains(pageNEWSNET))
                {
                    radPageView1.Pages.Remove(pageNEWSNET);
                }
            }
            else
            {
                if (!Directory.Exists(Program.Settings.Project_Options.NEWSNET_PRJ_PATH.Trim()))
                {
                    picRefresh.Enabled = false;
                    picNNProjectAlert.Visible = true;
                    lblNNProjectMessage.Text = CDict.GetValue("FormStartup.NewsNetPrjFolderNotFound", "NewsNet Project folder not found");
                }
                else
                {
                }
            }
        }

        private void radButton2_Click(object sender, EventArgs e)
        {

        }

        private void picRefresh_Click(object sender, EventArgs e)
        {
            nnList.Items.Clear();
            nnList.Groups.Clear();
            nnList.Refresh();
            System.Threading.Thread.Sleep(200);

            RefreshNewsNet();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            switch (nnList.View)
            {
                case View.Details:
                    nnList.View = View.Tile;
                    break;
                case View.Tile:
                    nnList.View = View.Details;
                    break;
            }
        }

        private void mnuProjects_Opening(object sender, CancelEventArgs e)
        {
            removeProjectToolStripMenuItem.Enabled = false;

            if (lstMRU.SelectedItems.Count > 0)
            {
                removeProjectToolStripMenuItem.Enabled = true;
            }
        }

        private void removeProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string msg = CDict.GetValue("MainForm.Mbox.RemoveProjectConfirmation", 
                                        "Are you sure to remove selected project/s and all project's files and folders?\r\n"+
                                        "(this operation does not remove videoclip and backgrounds sources, only recorded voice files)");

            msg = msg.Replace("|RETURN|", "\r\n");

            if (MessageBox.Show(null,
                                msg,
                                "Remove project/s",
                                MessageBoxButtons.OKCancel,
                                MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.OK)
            {
                Program.Logger.info(" [DELETE PROJECTS] Operation confirmed :");

                List<ListViewItem> ToRemove = new List<ListViewItem>();

                foreach(ListViewItem item in lstMRU.SelectedItems)
                {
                    MRUTools.MRUItem mru = (MRUTools.MRUItem)item.Tag;
                    DirectoryInfo dir = new FileInfo(mru.Filename).Directory;

                    Program.Logger.info("    > Project  : " + dir.FullName);

                    if (dir.Exists)
                    {
                        try
                        {
                            FileOperationAPIWrapper.MoveToRecycleBin(dir.FullName);
                            ToRemove.Add(item);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Error removing project : " + dir.FullName);
                            Program.Logger.error("       > Error removing project : " + ex.Message);
                        }
                    }
                }

                foreach (ListViewItem item in ToRemove)
                {
                    lstMRU.Items.Remove(item);
                }
            }
        }

        private void nnList_DoubleClick(object sender, EventArgs e)
        {
            if (nnList.SelectedItems.Count > 0)
            {
                newsNet_Selected_Project = (string)(nnList.SelectedItems[0].Tag);
                btnOk_Click(this, null);
            }
            else
            {
                newsNet_Selected_Project = "";
            }
        }

    }
}
