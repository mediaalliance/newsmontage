﻿namespace NewsMontange.Forms
{
    partial class frmClipInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            this.pnlClipMain = new System.Windows.Forms.Panel();
            this.pnlClippedArea = new System.Windows.Forms.Panel();
            this.lblTcin = new System.Windows.Forms.Label();
            this.lblTcout = new System.Windows.Forms.Label();
            this.lblClippingIn = new System.Windows.Forms.Label();
            this.lblClippingOut = new System.Windows.Forms.Label();
            this.chkTopmost = new Telerik.WinControls.UI.RadCheckBox();
            this.pnlClippingIn = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlClippingOut = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tab = new Telerik.WinControls.UI.RadGridView();
            this.btnSerialize = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txt_mediainfo = new System.Windows.Forms.TextBox();
            this.pnlClipMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTopmost)).BeginInit();
            this.pnlClippingIn.SuspendLayout();
            this.pnlClippingOut.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tab)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlClipMain
            // 
            this.pnlClipMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlClipMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pnlClipMain.Controls.Add(this.pnlClippedArea);
            this.pnlClipMain.Location = new System.Drawing.Point(6, 63);
            this.pnlClipMain.Name = "pnlClipMain";
            this.pnlClipMain.Size = new System.Drawing.Size(374, 63);
            this.pnlClipMain.TabIndex = 2;
            // 
            // pnlClippedArea
            // 
            this.pnlClippedArea.BackColor = System.Drawing.Color.Gray;
            this.pnlClippedArea.Location = new System.Drawing.Point(73, 3);
            this.pnlClippedArea.Name = "pnlClippedArea";
            this.pnlClippedArea.Size = new System.Drawing.Size(116, 57);
            this.pnlClippedArea.TabIndex = 0;
            // 
            // lblTcin
            // 
            this.lblTcin.AutoSize = true;
            this.lblTcin.Location = new System.Drawing.Point(3, 47);
            this.lblTcin.Name = "lblTcin";
            this.lblTcin.Size = new System.Drawing.Size(64, 13);
            this.lblTcin.TabIndex = 3;
            this.lblTcin.Text = "00:00:00:00";
            // 
            // lblTcout
            // 
            this.lblTcout.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTcout.AutoSize = true;
            this.lblTcout.Location = new System.Drawing.Point(314, 47);
            this.lblTcout.Name = "lblTcout";
            this.lblTcout.Size = new System.Drawing.Size(64, 13);
            this.lblTcout.TabIndex = 4;
            this.lblTcout.Text = "00:00:00:00";
            // 
            // lblClippingIn
            // 
            this.lblClippingIn.AutoSize = true;
            this.lblClippingIn.Location = new System.Drawing.Point(9, 29);
            this.lblClippingIn.Name = "lblClippingIn";
            this.lblClippingIn.Size = new System.Drawing.Size(64, 13);
            this.lblClippingIn.TabIndex = 5;
            this.lblClippingIn.Text = "00:00:00:00";
            this.lblClippingIn.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblClippingOut
            // 
            this.lblClippingOut.AutoSize = true;
            this.lblClippingOut.Location = new System.Drawing.Point(2, 29);
            this.lblClippingOut.Name = "lblClippingOut";
            this.lblClippingOut.Size = new System.Drawing.Size(64, 13);
            this.lblClippingOut.TabIndex = 6;
            this.lblClippingOut.Text = "00:00:00:00";
            // 
            // chkTopmost
            // 
            this.chkTopmost.Location = new System.Drawing.Point(300, 0);
            this.chkTopmost.Name = "chkTopmost";
            this.chkTopmost.Size = new System.Drawing.Size(91, 18);
            this.chkTopmost.TabIndex = 7;
            this.chkTopmost.Text = "Always on top";
            this.chkTopmost.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkTopmost_ToggleStateChanged);
            // 
            // pnlClippingIn
            // 
            this.pnlClippingIn.Controls.Add(this.panel2);
            this.pnlClippingIn.Controls.Add(this.lblClippingIn);
            this.pnlClippingIn.Location = new System.Drawing.Point(6, 126);
            this.pnlClippingIn.Name = "pnlClippingIn";
            this.pnlClippingIn.Size = new System.Drawing.Size(73, 45);
            this.pnlClippingIn.TabIndex = 8;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(71, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(2, 45);
            this.panel2.TabIndex = 0;
            // 
            // pnlClippingOut
            // 
            this.pnlClippingOut.Controls.Add(this.panel3);
            this.pnlClippingOut.Controls.Add(this.lblClippingOut);
            this.pnlClippingOut.Location = new System.Drawing.Point(192, 126);
            this.pnlClippingOut.Name = "pnlClippingOut";
            this.pnlClippingOut.Size = new System.Drawing.Size(73, 45);
            this.pnlClippingOut.TabIndex = 9;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(2, 45);
            this.panel3.TabIndex = 0;
            // 
            // tab
            // 
            this.tab.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tab.Location = new System.Drawing.Point(6, 197);
            // 
            // tab
            // 
            this.tab.MasterTemplate.AllowAddNewRow = false;
            this.tab.MasterTemplate.AllowColumnReorder = false;
            this.tab.MasterTemplate.AllowColumnResize = false;
            this.tab.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn1.FormatString = "";
            gridViewTextBoxColumn1.HeaderText = "Name";
            gridViewTextBoxColumn1.Name = "clmName";
            gridViewTextBoxColumn1.RowSpan = 15;
            gridViewTextBoxColumn1.Width = 130;
            gridViewTextBoxColumn2.FormatString = "";
            gridViewTextBoxColumn2.HeaderText = "Value";
            gridViewTextBoxColumn2.Name = "clmValue";
            gridViewTextBoxColumn2.RowSpan = 15;
            gridViewTextBoxColumn2.Width = 240;
            this.tab.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.tab.MasterTemplate.EnableGrouping = false;
            this.tab.MasterTemplate.EnableSorting = false;
            this.tab.MasterTemplate.ShowRowHeaderColumn = false;
            this.tab.Name = "tab";
            this.tab.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.tab.ReadOnly = true;
            // 
            // 
            // 
            this.tab.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.tab.Size = new System.Drawing.Size(372, 288);
            this.tab.TabIndex = 10;
            this.tab.Text = "radGridView1";
            this.tab.ThemeName = "Office2007Black";
            // 
            // btnSerialize
            // 
            this.btnSerialize.Location = new System.Drawing.Point(3, 6);
            this.btnSerialize.Name = "btnSerialize";
            this.btnSerialize.Size = new System.Drawing.Size(75, 23);
            this.btnSerialize.TabIndex = 11;
            this.btnSerialize.Text = "button1";
            this.btnSerialize.UseVisualStyleBackColor = true;
            this.btnSerialize.Visible = false;
            this.btnSerialize.Click += new System.EventHandler(this.btnSerialize_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControl1.Location = new System.Drawing.Point(0, 23);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(394, 517);
            this.tabControl1.TabIndex = 12;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Gray;
            this.tabPage1.Controls.Add(this.btnSerialize);
            this.tabPage1.Controls.Add(this.pnlClipMain);
            this.tabPage1.Controls.Add(this.tab);
            this.tabPage1.Controls.Add(this.lblTcin);
            this.tabPage1.Controls.Add(this.pnlClippingOut);
            this.tabPage1.Controls.Add(this.lblTcout);
            this.tabPage1.Controls.Add(this.pnlClippingIn);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(386, 491);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "TIMELINE CLIP INFO";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Gray;
            this.tabPage2.Controls.Add(this.txt_mediainfo);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(386, 491);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "FILE INFO";
            // 
            // txt_mediainfo
            // 
            this.txt_mediainfo.BackColor = System.Drawing.Color.DimGray;
            this.txt_mediainfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_mediainfo.Cursor = System.Windows.Forms.Cursors.Default;
            this.txt_mediainfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_mediainfo.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_mediainfo.Location = new System.Drawing.Point(3, 3);
            this.txt_mediainfo.Multiline = true;
            this.txt_mediainfo.Name = "txt_mediainfo";
            this.txt_mediainfo.Size = new System.Drawing.Size(380, 485);
            this.txt_mediainfo.TabIndex = 0;
            // 
            // frmClipInfo
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(394, 540);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.chkTopmost);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmClipInfo";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Clip Information";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmClipInfo_FormClosing);
            this.pnlClipMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkTopmost)).EndInit();
            this.pnlClippingIn.ResumeLayout(false);
            this.pnlClippingIn.PerformLayout();
            this.pnlClippingOut.ResumeLayout(false);
            this.pnlClippingOut.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tab)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlClipMain;
        private System.Windows.Forms.Panel pnlClippedArea;
        private System.Windows.Forms.Label lblTcin;
        private System.Windows.Forms.Label lblTcout;
        private System.Windows.Forms.Label lblClippingIn;
        private System.Windows.Forms.Label lblClippingOut;
        private Telerik.WinControls.UI.RadCheckBox chkTopmost;
        private System.Windows.Forms.Panel pnlClippingIn;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlClippingOut;
        private System.Windows.Forms.Panel panel3;
        private Telerik.WinControls.UI.RadGridView tab;
        private System.Windows.Forms.Button btnSerialize;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txt_mediainfo;

    }
}