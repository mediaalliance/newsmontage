﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using NewsMontange.Language;

namespace NewsMontange.Forms
{
    public partial class frmAbout : Form
    {
        public frmAbout()
        {
            InitializeComponent();

            lblClose.Text = CDict.GetValue("FormAbout.Close", lblClose.Text);

            lblAppVersion.Text = Application.ProductName + " v. " + Application.ProductVersion;

            AppDomain MyDomain = AppDomain.CurrentDomain;

            Assembly[] AssembliesLoaded = MyDomain.GetAssemblies();

            foreach (Assembly MyAssembly in AssembliesLoaded)
            {
                object[] companyInfo = MyAssembly.GetCustomAttributes(typeof(AssemblyCompanyAttribute), true);
                string company = "";

                if (companyInfo.Length > 0)
                {
                    company = ((AssemblyCompanyAttribute)companyInfo[0]).Company;
                }

                if (company.StartsWith("Media", StringComparison.OrdinalIgnoreCase))
                {
                    lstLoadedAssemblies.Items.Add(MyAssembly.GetName().Name + " - [" + MyAssembly.GetName().Version + "]");
                }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
