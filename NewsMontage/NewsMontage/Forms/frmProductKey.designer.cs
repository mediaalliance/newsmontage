﻿namespace NewsMontange
{
    partial class frmProductKey
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProductKey));
            this.lblSoftwares = new System.Windows.Forms.Label();
            this.lblReqCode = new System.Windows.Forms.Label();
            this.btnCopyReqCode = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtProdKey = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblProductName = new System.Windows.Forms.Label();
            this.lblCheck = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblExpiration = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlExit = new System.Windows.Forms.Panel();
            this.btnExit = new MediaAlliance.Controls.MAButton();
            this.lblClose = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlExit.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSoftwares
            // 
            this.lblSoftwares.AutoSize = true;
            this.lblSoftwares.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoftwares.ForeColor = System.Drawing.Color.DarkGray;
            this.lblSoftwares.Location = new System.Drawing.Point(462, 99);
            this.lblSoftwares.Name = "lblSoftwares";
            this.lblSoftwares.Size = new System.Drawing.Size(70, 20);
            this.lblSoftwares.TabIndex = 1;
            this.lblSoftwares.Text = "Softwares";
            // 
            // lblReqCode
            // 
            this.lblReqCode.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReqCode.Location = new System.Drawing.Point(39, 234);
            this.lblReqCode.Name = "lblReqCode";
            this.lblReqCode.Size = new System.Drawing.Size(502, 38);
            this.lblReqCode.TabIndex = 2;
            this.lblReqCode.Text = "007c913a6d7ee4c1ed393d628d68dfd8c5";
            this.lblReqCode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCopyReqCode
            // 
            this.btnCopyReqCode.Location = new System.Drawing.Point(466, 240);
            this.btnCopyReqCode.Name = "btnCopyReqCode";
            this.btnCopyReqCode.Size = new System.Drawing.Size(75, 29);
            this.btnCopyReqCode.TabIndex = 3;
            this.btnCopyReqCode.Text = "Copy";
            this.btnCopyReqCode.UseVisualStyleBackColor = true;
            this.btnCopyReqCode.Click += new System.EventHandler(this.btnCopyReqCode_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label1.Location = new System.Drawing.Point(229, 213);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 21);
            this.label1.TabIndex = 4;
            this.label1.Text = "REQUEST CODE";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label2.Location = new System.Drawing.Point(247, 317);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 21);
            this.label2.TabIndex = 5;
            this.label2.Text = "Product key";
            // 
            // txtProdKey
            // 
            this.txtProdKey.BackColor = System.Drawing.Color.Silver;
            this.txtProdKey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtProdKey.Location = new System.Drawing.Point(47, 353);
            this.txtProdKey.Name = "txtProdKey";
            this.txtProdKey.Size = new System.Drawing.Size(494, 27);
            this.txtProdKey.TabIndex = 6;
            this.txtProdKey.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtProdKey.TextChanged += new System.EventHandler(this.txtProdKey_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label3.Location = new System.Drawing.Point(224, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 21);
            this.label3.TabIndex = 7;
            this.label3.Text = "PRODUCT NAME";
            // 
            // lblProductName
            // 
            this.lblProductName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProductName.Location = new System.Drawing.Point(39, 159);
            this.lblProductName.Name = "lblProductName";
            this.lblProductName.Size = new System.Drawing.Size(498, 38);
            this.lblProductName.TabIndex = 8;
            this.lblProductName.Text = "Product Name";
            this.lblProductName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCheck
            // 
            this.lblCheck.BackColor = System.Drawing.Color.Red;
            this.lblCheck.Location = new System.Drawing.Point(21, 354);
            this.lblCheck.Name = "lblCheck";
            this.lblCheck.Size = new System.Drawing.Size(23, 25);
            this.lblCheck.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label4.Location = new System.Drawing.Point(251, 404);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 21);
            this.label4.TabIndex = 11;
            this.label4.Text = "Expiration";
            // 
            // lblExpiration
            // 
            this.lblExpiration.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExpiration.Location = new System.Drawing.Point(39, 425);
            this.lblExpiration.Name = "lblExpiration";
            this.lblExpiration.Size = new System.Drawing.Size(502, 38);
            this.lblExpiration.TabIndex = 12;
            this.lblExpiration.Text = "Never Expire";
            this.lblExpiration.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::NewsMontange.Properties.Resources.NewsMontage_Icon_02;
            this.pictureBox2.Location = new System.Drawing.Point(25, 39);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(80, 80);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 13;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::NewsMontange.Properties.Resources.MediaAlliance_Scritta_Internal;
            this.pictureBox1.Location = new System.Drawing.Point(116, 50);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(420, 55);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pnlExit
            // 
            this.pnlExit.Controls.Add(this.btnExit);
            this.pnlExit.Controls.Add(this.lblClose);
            this.pnlExit.Location = new System.Drawing.Point(462, 466);
            this.pnlExit.Name = "pnlExit";
            this.pnlExit.Size = new System.Drawing.Size(79, 55);
            this.pnlExit.TabIndex = 14;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Transparent;
            this.btnExit.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.btnExit.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.none;
            this.btnExit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnExit.Image_Click = null;
            this.btnExit.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnExit.Image_Hover")));
            this.btnExit.Image_Normal = null;
            this.btnExit.Image_Type = MediaAlliance.Controls.NmButtonIcon.Exit;
            this.btnExit.Location = new System.Drawing.Point(0, 0);
            this.btnExit.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnExit.Name = "btnExit";
            this.btnExit.Shortcut = System.Windows.Forms.Keys.None;
            this.btnExit.ShortcutTextColor = System.Drawing.Color.Silver;
            this.btnExit.Size = new System.Drawing.Size(79, 42);
            this.btnExit.TabIndex = 42;
            this.btnExit.Tooltip_Text = "Tooltip text";
            this.btnExit.UseInternalToolTip = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lblClose
            // 
            this.lblClose.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblClose.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClose.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblClose.Location = new System.Drawing.Point(0, 42);
            this.lblClose.Name = "lblClose";
            this.lblClose.Size = new System.Drawing.Size(79, 13);
            this.lblClose.TabIndex = 43;
            this.lblClose.Text = "Close";
            this.lblClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmProductKey
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(574, 538);
            this.Controls.Add(this.pnlExit);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.lblExpiration);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblCheck);
            this.Controls.Add(this.lblProductName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtProdKey);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCopyReqCode);
            this.Controls.Add(this.lblReqCode);
            this.Controls.Add(this.lblSoftwares);
            this.Controls.Add(this.pictureBox1);
            this.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmProductKey";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Product Key";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frmCode_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlExit.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblSoftwares;
        private System.Windows.Forms.Label lblReqCode;
        private System.Windows.Forms.Button btnCopyReqCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtProdKey;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblProductName;
        private System.Windows.Forms.Label lblCheck;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblExpiration;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel pnlExit;
        private MediaAlliance.Controls.MAButton btnExit;
        private System.Windows.Forms.Label lblClose;
    }
}