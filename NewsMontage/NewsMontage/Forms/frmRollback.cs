﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using NewsMontange.Helpers;





namespace NewsMontange.Forms
{
    public partial class frmRollback : Form
    {
        private CProjectsHelper projectHelper = null;
        private ImageList tvImagelist = null;

        private CRollbackInfo current_selection = null;

        public frmRollback()
        {
            InitializeComponent();

            tvImagelist = new ImageList();
            tvImagelist.ColorDepth = ColorDepth.Depth32Bit;
            tvImagelist.ImageSize = new Size(24, 24);

            tvImagelist.Images.Add(Properties.Resources.Rollback_Calendar);         // 0
            tvImagelist.Images.Add(Properties.Resources.Rollback_asLoaded);         // 1
            tvImagelist.Images.Add(Properties.Resources.Rollback_asRendered);       // 2
            tvImagelist.Images.Add(Properties.Resources.Rollback_autosave);         // 3
            tvImagelist.Images.Add(Properties.Resources.Rollback_beforeRestore);    // 4

            tv.ImageList = tvImagelist;
        }

        public CProjectsHelper ProjectHelper
        { 
            get { return projectHelper; }
            set
            {
                projectHelper = value;

                bool isFirst = true;
                DateTime cur_date = DateTime.Now;
                
                Telerik.WinControls.UI.RadTreeNode parent_node = null;
                Telerik.WinControls.UI.RadTreeNode parent_node_autosave = null;


                List<CRollbackInfo> current_rollsback = value.RollbackManager.OrderByDescending(X => X.BackupDate).ToList();

                foreach (CRollbackInfo rbInfo in current_rollsback)
                {
                    if (isFirst || rbInfo.BackupDate.Date != cur_date.Date)
                    {
                        parent_node             = new Telerik.WinControls.UI.RadTreeNode(rbInfo.Description);
                        parent_node.Text        = rbInfo.BackupDate.ToString("dd/MM/yyyy");
                        parent_node.ImageIndex  = 0;

                        if (isFirst)
                        {
                            parent_node.Expand();
                        }

                        tv.Nodes.Add(parent_node);


                        isFirst = false;
                        cur_date = rbInfo.BackupDate;
                    }

                    if (rbInfo.type == enRollbackType.autosave)
                    {
                        if (parent_node_autosave == null)
                        {
                            parent_node_autosave = new Telerik.WinControls.UI.RadTreeNode();
                            parent_node_autosave.Text = "Autosave group";
                            parent_node.Nodes.Add(parent_node_autosave);
                        }
                    }
                    else
                    {
                        parent_node_autosave = null;
                    }


                    Telerik.WinControls.UI.RadTreeNode n = new Telerik.WinControls.UI.RadTreeNode(rbInfo.Description);

                    switch (rbInfo.type)
                    {
                        case enRollbackType.undefined:
                            break;
                        case enRollbackType.asOpened:
                            n.ImageIndex = 1;
                            break;
                        case enRollbackType.asRendered:
                            n.ImageIndex = 2;
                            break;
                        case enRollbackType.beforeAddResource:
                            break;
                        case enRollbackType.autosave:
                            n.ImageIndex = 3;
                            break;
                        case enRollbackType.beforeRestoreBackup:
                            n.ImageIndex = 4;
                            break;
                        default:
                            break;
                    }

                    n.Tag = rbInfo;

                    if (parent_node_autosave != null)
                    {
                        parent_node_autosave.Nodes.Add(n);
                    }
                    else
                    {
                        parent_node.Nodes.Add(n);
                    }
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnRestore_Click(object sender, EventArgs e)
        {
            if (current_selection != null)
            {
                string msg = "You are restoring this rollback step : \r\n " + current_selection.Description + "\r\n are you sure?";

                if (MessageBox.Show(null, msg, "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    Program.Logger.info("User confirmed restore of rollback step");
                    projectHelper.RollbackManager.Restore(current_selection);
                }
            }
        }

        private void tv_DoubleClick(object sender, EventArgs e)
        {
            btnRestore_Click(null, null);
        }



        private void frmRollback_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                this.Hide();
            }
        }

        private void tv_SelectedNodeChanging(object sender, Telerik.WinControls.UI.RadTreeViewCancelEventArgs e)
        {
            current_selection = null;

            if (e.Node != null)
            {
                if (e.Node.Tag != null)
                {
                    btnRestore.Enabled = true;
                    current_selection = (CRollbackInfo)e.Node.Tag;
                }
            }
        }

    }
}
