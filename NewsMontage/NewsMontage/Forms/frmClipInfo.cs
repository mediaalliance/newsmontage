﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Xml;
using System.Xml.Serialization;

using MediaAlliance.Controls.MxpTimeLine;
using MediaAlliance.Controls.MxpTimeLine.Objects;
using System.IO;
using NewsMontange.Language;




namespace NewsMontange.Forms
{
    public partial class frmClipInfo : Form
    {
        private CMxpClip clip   = null;
        private bool realClose  = false;
        private float fps       = 25;

        public frmClipInfo(float fps)
        {
            InitializeComponent();

            this.Text = CDict.GetValue("ClipInformationForm.Title", this.Text);
            chkTopmost.Text = CDict.GetValue("ClipInformationForm.TopMost", chkTopmost.Text);
            tabPage1.Text = CDict.GetValue("ClipInformationForm.TimeLineInfoTab", tabPage1.Text);
            tabPage2.Text = CDict.GetValue("ClipInformationForm.FileInfoTab", tabPage2.Text);

            this.fps = fps;
        }

        private string FileLength(long len)
        {
            return FileSizeToString((double)len);
        }

        private enum byteUnit
        {
            buKiloByte = 1,       // kilo  Byte
            buMegaByte = 2,       // Mega  Byte
            buGigaByte = 3,       // Giga  Byte
            buTeraByte = 4,       // Tera  Byte
            buPetaByte = 5,       // Peta  Byte
            buExaByte = 6,       // Exa   Byte
            buZettaByte = 7,       // Zetta Byte
            buYottaByte = 8        // Yotta Byte
        };

        private string FileSizeToString(double size)
        {
            double reff = 1;
            int i = 0;

            while (reff < size)
            {
                i++;
                reff = Math.Pow(1000, i);
            }
            i--;
            int r = (int)(size / Math.Pow(1000, i));

            switch ((byteUnit)i)
            {
                case byteUnit.buKiloByte: return r.ToString() + " kB";
                case byteUnit.buMegaByte: return r.ToString() + " MB";
                case byteUnit.buGigaByte: return r.ToString() + " GB";
                case byteUnit.buTeraByte: return r.ToString() + " TB";
                case byteUnit.buPetaByte: return r.ToString() + " PB";
                case byteUnit.buExaByte: return r.ToString() + " EB";
                case byteUnit.buZettaByte: return r.ToString() + " ZB";
                case byteUnit.buYottaByte: return r.ToString() + " YB";
            }

            return "";
        }

        private void Update()
        {
            tab.Rows.Clear();

            if (clip != null)
            {
                lblTcin.Text        = GetTimecode(clip.TimcodeIn);
                lblTcout.Text       = GetTimecode(clip.TimecodeOut);
                lblClippingIn.Text  = GetTimecode(clip.ClippingIn);
                lblClippingOut.Text = GetTimecode(clip.ClippingOut);

                // tcout - tcin : width = tcin : x

                pnlClippedArea.Left     = (int)(pnlClipMain.Width * clip.ClippingIn / (clip.TimecodeOut - clip.TimcodeIn));
                pnlClippedArea.Width    = (int)(pnlClipMain.Width * clip.ClippingOut / (clip.TimecodeOut - clip.TimcodeIn)) - pnlClippedArea.Left;

                pnlClippingIn.Left      = pnlClippedArea.Left + pnlClipMain.Left - pnlClippingIn.Width;
                pnlClippingOut.Left     = pnlClippedArea.Right + pnlClipMain.Left;

                tab.Rows.Add("id", clip.ID);
                tab.Rows.Add(CDict.GetValue("ClipInformationForm.VarsName.track", "track"), clip.ParentTrack.ID);
                tab.Rows.Add(CDict.GetValue("ClipInformationForm.VarsName.type", "type"), clip.Type.ToString());
                tab.Rows.Add(CDict.GetValue("ClipInformationForm.VarsName.duration", "duration"), clip.Duration);
                tab.Rows.Add(CDict.GetValue("ClipInformationForm.VarsName.totalDuration", "total duration"), clip.TotalDuration);
                tab.Rows.Add(CDict.GetValue("ClipInformationForm.VarsName.clippingIn", "clipping in"), clip.ClippingIn);
                tab.Rows.Add(CDict.GetValue("ClipInformationForm.VarsName.clippingOut", "clipping out"), clip.ClippingOut);
                tab.Rows.Add(CDict.GetValue("ClipInformationForm.VarsName.timecodeIn", "timecode in"), clip.TimcodeIn);
                tab.Rows.Add(CDict.GetValue("ClipInformationForm.VarsName.timecodeOut", "timecode out"), clip.TimecodeOut);

                if (File.Exists(clip.ClipInfo.filename))
                {
                    try
                    {
                        MediaAlliance.AV.MediaInfo.MediaInformations mi = new MediaAlliance.AV.MediaInfo.MediaInformations();
                        mi.Open(clip.ClipInfo.filename);
                        FileInfo fi = new FileInfo(clip.ClipInfo.filename);

                        txt_mediainfo.Clear();

                        txt_mediainfo.Text += "GENERIC INFO";

                        txt_mediainfo.Text += " > File              : " + Path.GetFileName(clip.ClipInfo.filename) + "\r\n";
                        txt_mediainfo.Text += " > Duration          : " + mi.GetValue(MediaAlliance.AV.MediaInfo.GeneralInfoRequest.Duration) + "\r\n";

                        txt_mediainfo.Text += " > CreationTime      : " + fi.CreationTime.ToString() + "\r\n";
                        txt_mediainfo.Text += " > LastAccessTime    : " + fi.LastAccessTime.ToString() + "\r\n";
                        txt_mediainfo.Text += " > LastWriteTime     : " + fi.LastWriteTime.ToString() + "\r\n";
                        txt_mediainfo.Text += " > Length            : " + FileLength(fi.Length) + "\r\n";
                        txt_mediainfo.Text += " > Video tracks      : " + mi.VideoCount + "\r\n";
                        txt_mediainfo.Text += " > Audio tracks      : " + mi.AudioCount + "\r\n";


                        if (mi.VideoCount > 0)
                        {
                            for (int i = 0; i < mi.VideoCount; i++)
                            {
                                txt_mediainfo.Text += "VIDEO INFO #" + i +"\r\n";

                                txt_mediainfo.Text += " > Duration           : " + mi.GetValue(MediaAlliance.AV.MediaInfo.VideoInfoRequest.Duration, i) + "\r\n";
                                txt_mediainfo.Text += " > Format             : " + mi.GetValue(MediaAlliance.AV.MediaInfo.VideoInfoRequest.Format, i) + "\r\n";
                                txt_mediainfo.Text += " > Standard           : " + mi.GetValue(MediaAlliance.AV.MediaInfo.VideoInfoRequest.Standard, i) + "\r\n";
                                txt_mediainfo.Text += " > CodecID            : " + mi.GetValue(MediaAlliance.AV.MediaInfo.VideoInfoRequest.CodecID, i) + "\r\n";
                                txt_mediainfo.Text += " > Colorimetry        : " + mi.GetValue(MediaAlliance.AV.MediaInfo.VideoInfoRequest.Colorimetry, i) + "\r\n";
                                txt_mediainfo.Text += " > FrameRate          : " + mi.GetValue(MediaAlliance.AV.MediaInfo.VideoInfoRequest.FrameRate, i) + "\r\n";
                                txt_mediainfo.Text += " > Width              : " + mi.GetValue(MediaAlliance.AV.MediaInfo.VideoInfoRequest.Width, i) + "\r\n";
                                txt_mediainfo.Text += " > Height             : " + mi.GetValue(MediaAlliance.AV.MediaInfo.VideoInfoRequest.Height, i) + "\r\n";
                                txt_mediainfo.Text += " > DisplayAspectRatio : " + mi.GetValue(MediaAlliance.AV.MediaInfo.VideoInfoRequest.DisplayAspectRatio, i) + "\r\n";
                                txt_mediainfo.Text += " > PixelAspectRatio   : " + mi.GetValue(MediaAlliance.AV.MediaInfo.VideoInfoRequest.PixelAspectRatio, i) + "\r\n";
                                txt_mediainfo.Text += " > ScanType           : " + mi.GetValue(MediaAlliance.AV.MediaInfo.VideoInfoRequest.ScanType, i) + "\r\n";
                                txt_mediainfo.Text += " > ScanOrder          : " + mi.GetValue(MediaAlliance.AV.MediaInfo.VideoInfoRequest.ScanOrder, i) + "\r\n";
                                txt_mediainfo.Text += " > BitRate            : " + mi.GetValue(MediaAlliance.AV.MediaInfo.VideoInfoRequest.BitRate, i) + "\r\n";
                            }
                        }

                        if (mi.AudioCount > 0)
                        {
                            for (int i = 0; i < mi.AudioCount; i++)
                            {
                                txt_mediainfo.Text += "AUDIO INFO #" + i + "\r\n";

                                txt_mediainfo.Text += " > Duration           : " + mi.GetValue(MediaAlliance.AV.MediaInfo.AudioInfoRequest.Duration, i) + "\r\n";
                                txt_mediainfo.Text += " > Format             : " + mi.GetValue(MediaAlliance.AV.MediaInfo.AudioInfoRequest.Format, i) + "\r\n";
                                txt_mediainfo.Text += " > BitRate            : " + mi.GetValue(MediaAlliance.AV.MediaInfo.AudioInfoRequest.BitRate, i) + "\r\n";
                                txt_mediainfo.Text += " > BitRate Mode       : " + mi.GetValue(MediaAlliance.AV.MediaInfo.AudioInfoRequest.BitRate_Mode, i) + "\r\n";
                                txt_mediainfo.Text += " > CodecID            : " + mi.GetValue(MediaAlliance.AV.MediaInfo.AudioInfoRequest.CodecID, i) + "\r\n";
                                txt_mediainfo.Text += " > CompressionRatio   : " + mi.GetValue(MediaAlliance.AV.MediaInfo.AudioInfoRequest.CompressionRatio, i) + "\r\n";
                                txt_mediainfo.Text += " > Channel__s         : " + mi.GetValue(MediaAlliance.AV.MediaInfo.AudioInfoRequest.Channel__s, i) + "\r\n";
                                txt_mediainfo.Text += " > Encoded_Library    : " + mi.GetValue(MediaAlliance.AV.MediaInfo.AudioInfoRequest.Encoded_Library, i) + "\r\n";
                                txt_mediainfo.Text += " > Interleave_Dur     : " + mi.GetValue(MediaAlliance.AV.MediaInfo.AudioInfoRequest.Interleave_Duration, i) + "\r\n";
                                txt_mediainfo.Text += " > SamplingRate       : " + mi.GetValue(MediaAlliance.AV.MediaInfo.AudioInfoRequest.SamplingRate, i) + "\r\n";
                                txt_mediainfo.Text += " > Language           : " + mi.GetValue(MediaAlliance.AV.MediaInfo.AudioInfoRequest.Language, i) + "\r\n";
                            }
                        }

                        mi.Dispose();
                    }
                    catch
                    { 
                    }
                }
            }
        }


        public CMxpClip Clip
        {
            get { return clip; }
            set
            {
                if (clip != value)
                {
                    if (clip != null)
                    {
                        clip.OnChangedTC -= clip_OnChangedTC;
                    }

                    clip = value;

                    try
                    {
                        if (clip != null)
                        {
                            clip.OnChangedTC += new CMxpClip.MxpClip_OnChangedTC(clip_OnChangedTC);
                            Update();
                        }
                    }
                    catch (Exception ex)
                    {
                        Program.Logger.error(" [ClipInfo] Error : ");
                        Program.Logger.error(ex, true);
                    }
                }
            }
        }

        void clip_OnChangedTC(object Sender, long TCIn, long TCOut, enTcChangeType cType)
        {
            Update();
        }

        private string GetTimecode(long tc)
        {
            string tcstr = CTcTools.Frame2TC(Math.Abs(tc), fps); // Frame2TC(Math.Abs(tc));

            if (tc < 0) tcstr += "-";

            return tcstr;
        }

        private void chkTopmost_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            TopMost = (args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off) ? false : true;
        }

        private void frmClipInfo_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!realClose)
            {
                e.Cancel = true;
                this.Visible = false;
            }
        }

        public void RealClose()
        {
            realClose = true;
            Close();
        }

        private void btnSerialize_Click(object sender, EventArgs e)
        {
            if (clip != null)
            {
                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                XmlSerializer s = new XmlSerializer(typeof(CMxpClip));
                s.Serialize(ms, clip);

                string xml = Encoding.ASCII.GetString(ms.ToArray());
                ms.Close();
                ms.Dispose();

                MessageBox.Show(xml);
            }
        }
    }
}
