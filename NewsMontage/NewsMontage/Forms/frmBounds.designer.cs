namespace NewsMontange.Forms
{
    partial class frmCensorArea
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // frmBounds
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 268);
            this.Name = "frmBounds";
            this.Text = "Borderless Form";
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.frmBounds_MouseUp);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.frmBounds_Paint);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmBounds_MouseDown);
            this.Resize += new System.EventHandler(this.frmBounds_Resize);
            this.ResizeEnd += new System.EventHandler(this.frmBounds_Resized);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmBounds_MouseMove);
            this.ResumeLayout(false);

        }

        #endregion
    }
}

