﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using NewsMontange.Language;

namespace NewsMontange.Forms
{
    public partial class frmRecDesciption : Form
    {
        public frmRecDesciption()
        {
            InitializeComponent();
        }

        private void frmRecDesciption_Load(object sender, EventArgs e)
        {
            if (File.Exists(Program.LanguageFile))
            {
                CDict.AddFromFile(Program.LanguageFile);
            }

            radLabel1.Text = CDict.GetValue("FormRecDescription.Description", radLabel1.Text);
            btnOk.Text = CDict.GetValue("Generic.Button.OK", btnOk.Text);
        }



        public string Description
        {
            get { return txtTitle.Text; }
            set
            {
                txtTitle.Text = value;
            }
        }

        private void txtTitle_TextChanged(object sender, EventArgs e)
        {
            btnOk.Enabled = txtTitle.Text.Trim() != "";
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void txtTitle_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return && txtTitle.Text.Trim() != "")
            {
                btnOk_Click(this, null);
            }
        }
    }
}
