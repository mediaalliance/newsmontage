﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Threading;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;



namespace NewsMontange.Forms
{
    public partial class frmLoading : Form
    {
        #region WIN32

        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("User32.dll", SetLastError = true)]
        public static extern bool PostMessage(IntPtr hWnd, uint uMsg, int wParam, int lParam);

        
        [DllImport("User32.dll")]
        public static extern IntPtr GetDC(IntPtr hwnd);

        [DllImport("User32.dll")]
        public static extern void ReleaseDC(IntPtr dc);




        public const uint WM_USER = 0x0400;
        public const uint TICK_MESSAGE = WM_USER + 1234;

        #endregion WIN32



        private float percent           = 0;
        private bool animated           = true;


        private IntPtr MainHandle       = IntPtr.Zero;
        private float current_value     = 0;
        private int max_increment       = 5;
        private Thread thAnimation      = null;
        private bool thAnimation_Stop   = false;
        private ManualResetEvent mre    = new ManualResetEvent(false);



        public frmLoading()
        {
            InitializeComponent();

            //this.AllowTransparency = true;
            //this.DoubleBuffered = true;
            //this.Opacity = .99;

            this.Region = new System.Drawing.Region(GetRoundedRect(new Rectangle(0,0,Width-1, Height-1), 4));

            bar.Width = 1;

            Animated = true;

            update();
        }

        private void frmLoading_Load(object sender, EventArgs e)
        {
            MainHandle = this.Handle;
            splitContainer1.Panel1Collapsed = true;
        }


        #region GRAPHICS FUNCTIONS

        private GraphicsPath GetRoundedRect(RectangleF baseRect, float radius)
        {
            if (radius <= 0.0F)
            {
                GraphicsPath mPath = new GraphicsPath();
                mPath.AddRectangle(baseRect);
                mPath.CloseFigure();
                return mPath;
            }

            if (radius >= (Math.Min(baseRect.Width, baseRect.Height)) / 2.0)
                return GetCapsule(baseRect);

            float diameter = radius * 2.0F;
            SizeF sizeF = new SizeF(diameter, diameter);
            RectangleF arc = new RectangleF(baseRect.Location, sizeF);
            GraphicsPath path = new System.Drawing.Drawing2D.GraphicsPath();

            path.AddArc(arc, 180, 90);

            arc.X = baseRect.Right - diameter;
            path.AddArc(arc, 270, 90);

            arc.Y = baseRect.Bottom - diameter;
            path.AddArc(arc, 0, 90);

            arc.X = baseRect.Left;
            path.AddArc(arc, 90, 90);

            path.CloseFigure();
            return path;
        }

        private GraphicsPath GetCapsule(RectangleF baseRect)
        {
            float diameter;
            RectangleF arc;
            GraphicsPath path = new System.Drawing.Drawing2D.GraphicsPath();
            try
            {
                if (baseRect.Width > baseRect.Height)
                {
                    // return horizontal capsule 
                    diameter = baseRect.Height;
                    SizeF sizeF = new SizeF(diameter, diameter);
                    arc = new RectangleF(baseRect.Location, sizeF);
                    path.AddArc(arc, 90, 180);
                    arc.X = baseRect.Right - diameter;
                    path.AddArc(arc, 270, 180);
                }
                else if (baseRect.Width < baseRect.Height)
                {
                    // return vertical capsule 
                    diameter = baseRect.Width;
                    SizeF sizeF = new SizeF(diameter, diameter);
                    arc = new RectangleF(baseRect.Location, sizeF);
                    path.AddArc(arc, 180, 180);
                    arc.Y = baseRect.Bottom - diameter;
                    path.AddArc(arc, 0, 180);
                }
                else
                {
                    // return circle 
                    path.AddEllipse(baseRect);
                }
            }
            catch (Exception ex)
            {
                path.AddEllipse(baseRect);
            }
            finally
            {
                path.CloseFigure();
            }
            return path;
        }

        #endregion GRAPHICS FUNCTIONS



        protected override void WndProc(ref Message m)
        {
            if (m.Msg == TICK_MESSAGE)
            {
                bar.Width = (int)(out_bar.Width * current_value / 100);
                this.Refresh();
                Application.DoEvents();
            }

            base.WndProc(ref m);
        }



        public void AddMessage(string msg)
        {
            if ((lstMessages.Items.Count > 0 && (string)lstMessages.Items[0] != msg) || lstMessages.Items.Count == 0)
            {
                lstMessages.Items.Insert(0, msg);
            }
        }

        public void ClearMessages()
        {
            lstMessages.Items.Clear();
        }


        private void START_THREAD()
        {
            if (thAnimation == null)
            {
                mre.Reset();
                thAnimation = new Thread(new ThreadStart(thAnimation_task));
                thAnimation.IsBackground = true;
                thAnimation.Start();
            }
        }

        private void STOP_THREAD()
        {
            thAnimation_Stop = true;
            mre.Set();

            if (thAnimation != null)
            {
                if (!thAnimation.Join(200))
                {
                    thAnimation.Abort();
                }

                thAnimation = null;
                thAnimation_Stop = false;
            }
        }

        private void SafeClose()
        {
            STOP_THREAD();
        }

        
        private void thAnimation_task()
        {
            Random rnd = new Random(DateTime.Now.Millisecond);

            while (!thAnimation_Stop)
            {
                while (current_value != percent)
                {
                    float inc = rnd.Next(0, max_increment);

                    if (current_value + inc > percent)
                    {
                        inc = percent - current_value;
                    }

                    current_value += inc;
                    PostMessage(MainHandle, TICK_MESSAGE, 0, 0);

                    Thread.Sleep(10);
                }

                mre.WaitOne();
                mre.Reset();
            }
        }



        private void update()
        {
            if (!animated)
            {
                bar.Width = (int)(out_bar.Width * percent / 100);
                pic.Refresh();
            }
            else
            {
                mre.Set();
            }
        }

        public string Title
        {
            get { return lblTitle.Text; }
            set { lblTitle.Text = value; }
        }

        public float Percent
        {
            get { return percent; }
            set
            {
                if (value < 0) value = 0;
                if (value > 100) value = 100;

                if (value != percent)
                {
                    percent = value;
                    update();
                }
            }
        }

        public bool Animated
        {
            get { return animated; }
            set 
            {
                animated = value;

                if (animated)
                {
                    START_THREAD();
                }
                else
                {
                    STOP_THREAD();
                }
            }
        }

        public Image image
        {
            get { return pic.Image; }
            set
            {
                if (pic.Image != value)
                {
                    pic.Image = value;
                    CheckPicVisibility();
                }
            }
        }

        private void CheckPicVisibility()
        {

            if (pic.Image == null)
            {
                splitContainer1.Panel1Collapsed = true;
            }
            else
            {
                splitContainer1.Panel1Collapsed = false;
            }
        }

        private void frmLoading_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                this.Hide();
            }
        }

        private void frmLoading_Paint(object sender, PaintEventArgs e)
        {



        }

        private void frmLoading_Shown(object sender, EventArgs e)
        {
            CheckPicVisibility();
        }
    }
}
