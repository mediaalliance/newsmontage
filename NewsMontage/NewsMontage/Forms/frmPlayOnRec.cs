﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;

namespace NewsMontange.Forms
{
    public partial class frmPlayOnRec : Form
    {
        public frmPlayOnRec()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            
            RegistryKey key = Registry.CurrentUser.CreateSubKey(@"Software\MediaAlliance\NewsMontage\Options");
            
            if (key != null)
            {
                key.SetValue("ShowPlayOnRecAlert", (chkShowAgain.Checked)? 0 : 1, RegistryValueKind.DWord);
            }

            key.Close();

            this.Close();
        }

        private void chkShowAgain_CheckedChanged(object sender, EventArgs e)
        {
        }
    }
}
