﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Runtime.InteropServices;

namespace NewsMontange.UserControls
{
    public partial class MA_NMRichTextUC : UserControl
    {
        // EVENTS
        public event CRichText.Rich_OnZoomChanged OnZoomChanged = null;


        private bool isFocused = false;

        [Browsable(true)]
        public new event System.EventHandler TextChanged = null;

        private const uint WM_PAINT = 0x000F;

        public CRichText RichTextBox = new CRichText();



        public MA_NMRichTextUC()
        {
            InitializeComponent();

            this.Padding = new System.Windows.Forms.Padding(2);

            RichTextBox.Parent      = this;
            RichTextBox.BorderStyle = BorderStyle.None;
            RichTextBox.Dock        = DockStyle.Fill;
            RichTextBox.BackColor   = this.BackColor;
            RichTextBox.TextChanged += new EventHandler(RichTextBox_TextChanged);
            RichTextBox.KeyUp += new KeyEventHandler(RichTextBox_KeyUp);


            this.Enter                  += new EventHandler(MA_NMRichTextUC_Enter);
            this.Leave                  += new EventHandler(MA_NMRichTextUC_Leave);
            RichTextBox.Enter           += new EventHandler(MA_NMRichTextUC_Enter);
            RichTextBox.Leave           += new EventHandler(MA_NMRichTextUC_Leave);
            RichTextBox.OnZoomChanged   += new CRichText.Rich_OnZoomChanged(RichTextBox_OnZoomChanged);
        }



        void RichTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Parent.Focus();
            }
        }

        void RichTextBox_TextChanged(object sender, EventArgs e)
        {
            if (TextChanged != null) TextChanged(this, e);
        }

        void RichTextBox_OnZoomChanged(object Sender, float zoom_level, bool ChangedByGui)
        {
            if (OnZoomChanged != null) OnZoomChanged(this, zoom_level, true);
        }


        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_PAINT)
            {
                // raise the paint event
                base.WndProc(ref m);
                using (Graphics graphic = base.CreateGraphics())
                {
                    OnPaint(new PaintEventArgs(graphic, base.ClientRectangle));
                }
            }
            base.WndProc(ref m);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            if (isFocused)
            {
                e.Graphics.DrawRectangle(Pens.Orange, 0, 0, Width - 1, Height - 1);
            }

            //if (isFocused)
            //{
            //    Bitmap bufferImage = new Bitmap(base.ClientSize.Width, base.ClientSize.Height);
            //    Graphics grf = Graphics.FromImage(bufferImage);
            //    e.Graphics.DrawImage(bufferImage, 0, 0);
            //    bufferImage.Dispose();
            //}
            //else
            //{
            //}

            //using (Graphics bufferDC = Graphics.FromImage(bufferImage))
            //{
            //    foreach (Word word in _spellChecker.MisspelledWords.Values)
            //    {
            //        // code to find word here ...

            //        // draw wavy line
            //        for (int offset = startPoint.X;
            //            offset <= (endPoint.X - 2);
            //            offset += 4)
            //        {
            //            bufferDC.DrawLine(_errorPen,
            //                offset, startPoint.Y,
            //                (offset + 2), (startPoint.Y + 2));

            //            if ((offset + 2) < (endPoint.X - 2))
            //            {
            //                bufferDC.DrawLine(_errorPen,
            //                    (offset + 2), (startPoint.Y + 2),
            //                    (offset + 4), startPoint.Y);
            //            }
            //        } // for
            //    }// foreach word
            //} // using graphics
            //e.Graphics.DrawImage(bufferImage, 0, 0);
            //bufferImage.Dispose();
        }

        protected override void OnBackColorChanged(EventArgs e)
        {
            RichTextBox.BackColor = this.BackColor;
            base.OnBackColorChanged(e);
        }

        void MA_NMRichTextUC_Leave(object sender, EventArgs e)
        {
            isFocused = false;
            Refresh();
        }

        void MA_NMRichTextUC_Enter(object sender, EventArgs e)
        {
            isFocused = true;
            Refresh();
        }


        #region PUBLIC PROPERTIES

        public string Rtf
        {
            get { return RichTextBox.Rtf; }
            set { RichTextBox.Rtf = value; }    
        }

        public float ZoomFactor
        {
            get { return RichTextBox.ZoomFactor; }
            set 
            { 
                RichTextBox.ZoomFactor = value;
                if (OnZoomChanged != null) OnZoomChanged(this, value, false);
            }
        }

        public Font SelectionFont
        {
            get { return RichTextBox.SelectionFont; }
            set { RichTextBox.SelectionFont = value; }
        }

        #endregion PUBLIC PROPERTIES


        public class CRichText : RichTextBox
        {
            [System.Obsolete("Work only on 32bit Systems > USE KGetAsyncKeyState()")]
            [DllImport("user32.dll")]
            private static extern IntPtr GetAsyncKeyState(IntPtr vKey);


            // DELEGATE
            public delegate void Rich_OnZoomChanged(object Sender, float zoom_level, bool ChangedByGui);

            // EVENTS
            public event Rich_OnZoomChanged OnZoomChanged = null;



            private bool GetAsyncKeyState(Keys vKey)
            {
                return 0 != ((Int64)GetAsyncKeyState((IntPtr)vKey) & 0x8000);
            }

            private float min_zoom      = .6F;
            private float max_zoom      = 10;
            private float zoom_unit     = .2F;


            protected override void OnMouseWheel(MouseEventArgs e)
            {
                if (GetAsyncKeyState(Keys.ControlKey))
                {
                    float new_zoom = this.ZoomFactor + ((e.Delta / 40) * zoom_unit);

                    new_zoom = new_zoom - (new_zoom % .2F);

                    if (new_zoom > max_zoom) new_zoom = max_zoom;
                    if (new_zoom < min_zoom) new_zoom = min_zoom;

                    this.ZoomFactor = new_zoom;

                    if (OnZoomChanged != null) OnZoomChanged(this, this.ZoomFactor, true);
                }
                else
                {
                    base.OnMouseWheel(e);
                }
            }

        }
    }
}
