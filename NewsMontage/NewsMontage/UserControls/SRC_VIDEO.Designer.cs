﻿using MediaAlliance.Controls;
namespace NewsMontange.UserControls
{
    partial class SRC_VIDEO
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            this.thumbMaker.Dispose();

            if (sourcePreview != null) sourcePreview.Close();

            if (disposing && (components != null))
            {
                components.Dispose();
            }



            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SRC_VIDEO));
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.pnlSourceViewer = new System.Windows.Forms.Panel();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.label6 = new System.Windows.Forms.Label();
            this.picOpen = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlMarkersTC = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tc_markout = new NewsMontange.UserControls.MA_TimeCodeUC();
            this.tc_markin = new NewsMontange.UserControls.MA_TimeCodeUC();
            this.pnlLine = new System.Windows.Forms.Panel();
            this.lblZoomAllSrc = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblCurrentPosition = new System.Windows.Forms.Label();
            this.TimeCode_Position = new NewsMontange.UserControls.MA_TimeCodeUC();
            this.picZoomAllSrc2 = new MediaAlliance.Controls.MAButton();
            this.picPlayVirtualClip = new MediaAlliance.Controls.MAButton();
            this.btnSendToA = new MediaAlliance.Controls.MAButton();
            this.btnSendToB = new MediaAlliance.Controls.MAButton();
            this.picVirtualClip = new MediaAlliance.Controls.MAButton();
            this.label4 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.ListSources0 = new NewsMontange.UserControls.MA_NMListSources();
            this.mnuGallery = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.censorEditorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlLblSource = new System.Windows.Forms.Panel();
            this.picListSrc_Style = new System.Windows.Forms.PictureBox();
            this.lblSources = new System.Windows.Forms.Label();
            this.pnlTLSrc = new System.Windows.Forms.Panel();
            this.Timeline = new MediaAlliance.Controls.MxpTimeLine.MxpTmeLine();
            this.pnlTrackInfo_Src = new System.Windows.Forms.Panel();
            this.imgsSrcThumb = new System.Windows.Forms.ImageList(this.components);
            this.openFilesource = new System.Windows.Forms.OpenFileDialog();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.pnlSourceViewer.SuspendLayout();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picOpen)).BeginInit();
            this.panel1.SuspendLayout();
            this.pnlMarkersTC.SuspendLayout();
            this.mnuGallery.SuspendLayout();
            this.pnlLblSource.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picListSrc_Style)).BeginInit();
            this.pnlTLSrc.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer3.IsSplitterFixed = true;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.pnlSourceViewer);
            this.splitContainer3.Panel1.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.pnlTLSrc);
            this.splitContainer3.Panel2.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.splitContainer3.Size = new System.Drawing.Size(1034, 372);
            this.splitContainer3.SplitterDistance = 238;
            this.splitContainer3.TabIndex = 39;
            // 
            // pnlSourceViewer
            // 
            this.pnlSourceViewer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(91)))), ((int)(((byte)(91)))), ((int)(((byte)(91)))));
            this.pnlSourceViewer.Controls.Add(this.splitContainer4);
            this.pnlSourceViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSourceViewer.Location = new System.Drawing.Point(0, 0);
            this.pnlSourceViewer.Margin = new System.Windows.Forms.Padding(5, 3, 3, 3);
            this.pnlSourceViewer.Name = "pnlSourceViewer";
            this.pnlSourceViewer.Size = new System.Drawing.Size(1029, 238);
            this.pnlSourceViewer.TabIndex = 0;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer4.IsSplitterFixed = true;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.label6);
            this.splitContainer4.Panel1.Controls.Add(this.picOpen);
            this.splitContainer4.Panel1.Controls.Add(this.panel1);
            this.splitContainer4.Panel1.Controls.Add(this.button3);
            this.splitContainer4.Panel1.Controls.Add(this.button2);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.ListSources0);
            this.splitContainer4.Panel2.Controls.Add(this.pnlLblSource);
            this.splitContainer4.Panel2.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.splitContainer4.Size = new System.Drawing.Size(1029, 238);
            this.splitContainer4.SplitterDistance = 750;
            this.splitContainer4.TabIndex = 49;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label6.Location = new System.Drawing.Point(695, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 57;
            this.label6.Text = "OPEN";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // picOpen
            // 
            this.picOpen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picOpen.Image = global::NewsMontange.Properties.Resources.Folder_01;
            this.picOpen.Location = new System.Drawing.Point(695, 5);
            this.picOpen.Name = "picOpen";
            this.picOpen.Size = new System.Drawing.Size(52, 39);
            this.picOpen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picOpen.TabIndex = 51;
            this.picOpen.TabStop = false;
            this.picOpen.Click += new System.EventHandler(this.picOpen_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pnlMarkersTC);
            this.panel1.Controls.Add(this.lblZoomAllSrc);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.lblCurrentPosition);
            this.panel1.Controls.Add(this.TimeCode_Position);
            this.panel1.Controls.Add(this.picZoomAllSrc2);
            this.panel1.Controls.Add(this.picPlayVirtualClip);
            this.panel1.Controls.Add(this.btnSendToA);
            this.panel1.Controls.Add(this.btnSendToB);
            this.panel1.Controls.Add(this.picVirtualClip);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 175);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(750, 63);
            this.panel1.TabIndex = 50;
            // 
            // pnlMarkersTC
            // 
            this.pnlMarkersTC.Controls.Add(this.label2);
            this.pnlMarkersTC.Controls.Add(this.label3);
            this.pnlMarkersTC.Controls.Add(this.tc_markout);
            this.pnlMarkersTC.Controls.Add(this.tc_markin);
            this.pnlMarkersTC.Controls.Add(this.pnlLine);
            this.pnlMarkersTC.Location = new System.Drawing.Point(165, 0);
            this.pnlMarkersTC.Name = "pnlMarkersTC";
            this.pnlMarkersTC.Size = new System.Drawing.Size(307, 29);
            this.pnlMarkersTC.TabIndex = 67;
            this.pnlMarkersTC.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DarkGray;
            this.label2.Location = new System.Drawing.Point(6, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(14, 12);
            this.label2.TabIndex = 60;
            this.label2.Text = "IN";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DarkGray;
            this.label3.Location = new System.Drawing.Point(276, 1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 12);
            this.label3.TabIndex = 61;
            this.label3.Text = "OUT";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tc_markout
            // 
            this.tc_markout.BackColor_Selected = System.Drawing.Color.DimGray;
            this.tc_markout.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.tc_markout.BorderFocusesColor = System.Drawing.Color.LightSalmon;
            this.tc_markout.Font = new System.Drawing.Font("LCD_PS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tc_markout.ForeColor = System.Drawing.Color.Gray;
            this.tc_markout.FPS = 25F;
            this.tc_markout.Location = new System.Drawing.Point(168, 2);
            this.tc_markout.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tc_markout.Name = "tc_markout";
            this.tc_markout.Size = new System.Drawing.Size(102, 20);
            this.tc_markout.TabIndex = 59;
            this.tc_markout.TimeCode = ((long)(0));
            this.tc_markout.OnTimecodeChanged += new NewsMontange.UserControls.MA_TimeCodeUC.MATC_OnTimecodeChanged(this.tc_markout_OnTimecodeChanged);
            // 
            // tc_markin
            // 
            this.tc_markin.BackColor_Selected = System.Drawing.Color.DimGray;
            this.tc_markin.BorderColor = System.Drawing.Color.Green;
            this.tc_markin.BorderFocusesColor = System.Drawing.Color.LightSalmon;
            this.tc_markin.Font = new System.Drawing.Font("LCD_PS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tc_markin.ForeColor = System.Drawing.Color.Gray;
            this.tc_markin.FPS = 25F;
            this.tc_markin.Location = new System.Drawing.Point(22, 2);
            this.tc_markin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tc_markin.Name = "tc_markin";
            this.tc_markin.Size = new System.Drawing.Size(102, 20);
            this.tc_markin.TabIndex = 58;
            this.tc_markin.TimeCode = ((long)(0));
            this.tc_markin.OnTimecodeChanged += new NewsMontange.UserControls.MA_TimeCodeUC.MATC_OnTimecodeChanged(this.tc_markin_OnTimecodeChanged);
            // 
            // pnlLine
            // 
            this.pnlLine.Location = new System.Drawing.Point(74, 16);
            this.pnlLine.Name = "pnlLine";
            this.pnlLine.Size = new System.Drawing.Size(144, 11);
            this.pnlLine.TabIndex = 60;
            this.pnlLine.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlLine_Paint);
            // 
            // lblZoomAllSrc
            // 
            this.lblZoomAllSrc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblZoomAllSrc.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblZoomAllSrc.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lblZoomAllSrc.Location = new System.Drawing.Point(705, 47);
            this.lblZoomAllSrc.Name = "lblZoomAllSrc";
            this.lblZoomAllSrc.Size = new System.Drawing.Size(48, 14);
            this.lblZoomAllSrc.TabIndex = 59;
            this.lblZoomAllSrc.Text = "ZOOM ALL";
            this.lblZoomAllSrc.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label5.Location = new System.Drawing.Point(647, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 14);
            this.label5.TabIndex = 58;
            this.label5.Text = "PLAY MARK";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblCurrentPosition
            // 
            this.lblCurrentPosition.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentPosition.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lblCurrentPosition.Location = new System.Drawing.Point(165, 11);
            this.lblCurrentPosition.Name = "lblCurrentPosition";
            this.lblCurrentPosition.Size = new System.Drawing.Size(307, 13);
            this.lblCurrentPosition.TabIndex = 53;
            this.lblCurrentPosition.Text = "CURRENT POSITION";
            this.lblCurrentPosition.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // TimeCode_Position
            // 
            this.TimeCode_Position.BackColor_Selected = System.Drawing.Color.DimGray;
            this.TimeCode_Position.BorderColor = System.Drawing.Color.DarkGoldenrod;
            this.TimeCode_Position.BorderFocusesColor = System.Drawing.Color.LightSalmon;
            this.TimeCode_Position.Font = new System.Drawing.Font("LCD_PS", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TimeCode_Position.ForeColor = System.Drawing.Color.Gray;
            this.TimeCode_Position.FPS = 25F;
            this.TimeCode_Position.Location = new System.Drawing.Point(249, 31);
            this.TimeCode_Position.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TimeCode_Position.Name = "TimeCode_Position";
            this.TimeCode_Position.Size = new System.Drawing.Size(128, 30);
            this.TimeCode_Position.TabIndex = 52;
            this.TimeCode_Position.TimeCode = ((long)(0));
            this.TimeCode_Position.OnTimecodeChanged += new NewsMontange.UserControls.MA_TimeCodeUC.MATC_OnTimecodeChanged(this.TimeCode_Position_OnTimecodeChanged);
            // 
            // picZoomAllSrc2
            // 
            this.picZoomAllSrc2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picZoomAllSrc2.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.picZoomAllSrc2.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.automatic;
            this.picZoomAllSrc2.Image_Click = null;
            this.picZoomAllSrc2.Image_Hover = ((System.Drawing.Image)(resources.GetObject("picZoomAllSrc2.Image_Hover")));
            this.picZoomAllSrc2.Image_Normal = null;
            this.picZoomAllSrc2.Image_Type = MediaAlliance.Controls.NmButtonIcon.ZoomAll;
            this.picZoomAllSrc2.Location = new System.Drawing.Point(707, 4);
            this.picZoomAllSrc2.Name = "picZoomAllSrc2";
            this.picZoomAllSrc2.Shortcut = System.Windows.Forms.Keys.None;
            this.picZoomAllSrc2.ShortcutTextColor = System.Drawing.Color.Silver;
            this.picZoomAllSrc2.Size = new System.Drawing.Size(40, 40);
            this.picZoomAllSrc2.TabIndex = 51;
            this.picZoomAllSrc2.Tooltip_Text = "Tooltip text";
            this.picZoomAllSrc2.UseInternalToolTip = false;
            this.picZoomAllSrc2.Click += new System.EventHandler(this.picZoomAllSrc_Click);
            // 
            // picPlayVirtualClip
            // 
            this.picPlayVirtualClip.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picPlayVirtualClip.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.picPlayVirtualClip.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.automatic;
            this.picPlayVirtualClip.Enabled = false;
            this.picPlayVirtualClip.Image_Click = null;
            this.picPlayVirtualClip.Image_Hover = ((System.Drawing.Image)(resources.GetObject("picPlayVirtualClip.Image_Hover")));
            this.picPlayVirtualClip.Image_Normal = null;
            this.picPlayVirtualClip.Image_Type = MediaAlliance.Controls.NmButtonIcon.PlayMarked;
            this.picPlayVirtualClip.Location = new System.Drawing.Point(658, 4);
            this.picPlayVirtualClip.Name = "picPlayVirtualClip";
            this.picPlayVirtualClip.Shortcut = System.Windows.Forms.Keys.None;
            this.picPlayVirtualClip.ShortcutTextColor = System.Drawing.Color.Silver;
            this.picPlayVirtualClip.Size = new System.Drawing.Size(40, 40);
            this.picPlayVirtualClip.TabIndex = 50;
            this.picPlayVirtualClip.Tooltip_Text = "Tooltip text";
            this.picPlayVirtualClip.UseInternalToolTip = false;
            this.picPlayVirtualClip.Load += new System.EventHandler(this.picPlayVirtualClip_Load);
            this.picPlayVirtualClip.Click += new System.EventHandler(this.picPlayVirtualClip_Click);
            // 
            // btnSendToA
            // 
            this.btnSendToA.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.btnSendToA.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.automatic;
            this.btnSendToA.Enabled = false;
            this.btnSendToA.Image_Click = null;
            this.btnSendToA.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnSendToA.Image_Hover")));
            this.btnSendToA.Image_Normal = global::NewsMontange.Properties.Resources.BTN_SendToA;
            this.btnSendToA.Image_Type = MediaAlliance.Controls.NmButtonIcon.Custom;
            this.btnSendToA.Location = new System.Drawing.Point(62, 4);
            this.btnSendToA.Name = "btnSendToA";
            this.btnSendToA.Shortcut = System.Windows.Forms.Keys.None;
            this.btnSendToA.ShortcutTextColor = System.Drawing.Color.Silver;
            this.btnSendToA.Size = new System.Drawing.Size(40, 40);
            this.btnSendToA.TabIndex = 49;
            this.btnSendToA.Tooltip_Text = "Tooltip text";
            this.btnSendToA.UseInternalToolTip = false;
            this.btnSendToA.Click += new System.EventHandler(this.btnSendToA_Click);
            // 
            // btnSendToB
            // 
            this.btnSendToB.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.btnSendToB.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.automatic;
            this.btnSendToB.Enabled = false;
            this.btnSendToB.Image_Click = null;
            this.btnSendToB.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnSendToB.Image_Hover")));
            this.btnSendToB.Image_Normal = global::NewsMontange.Properties.Resources.BTN_SendToB;
            this.btnSendToB.Image_Type = MediaAlliance.Controls.NmButtonIcon.Custom;
            this.btnSendToB.Location = new System.Drawing.Point(108, 4);
            this.btnSendToB.Name = "btnSendToB";
            this.btnSendToB.Shortcut = System.Windows.Forms.Keys.None;
            this.btnSendToB.ShortcutTextColor = System.Drawing.Color.Silver;
            this.btnSendToB.Size = new System.Drawing.Size(40, 40);
            this.btnSendToB.TabIndex = 47;
            this.btnSendToB.TabStop = false;
            this.btnSendToB.Tooltip_Text = "Tooltip text";
            this.btnSendToB.UseInternalToolTip = false;
            this.btnSendToB.Click += new System.EventHandler(this.btnSendToB_Click);
            // 
            // picVirtualClip
            // 
            this.picVirtualClip.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.picVirtualClip.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.automatic;
            this.picVirtualClip.Enabled = false;
            this.picVirtualClip.Image_Click = null;
            this.picVirtualClip.Image_Hover = ((System.Drawing.Image)(resources.GetObject("picVirtualClip.Image_Hover")));
            this.picVirtualClip.Image_Normal = null;
            this.picVirtualClip.Image_Type = MediaAlliance.Controls.NmButtonIcon.VirtualClip;
            this.picVirtualClip.Location = new System.Drawing.Point(15, 4);
            this.picVirtualClip.Name = "picVirtualClip";
            this.picVirtualClip.Shortcut = System.Windows.Forms.Keys.None;
            this.picVirtualClip.ShortcutTextColor = System.Drawing.Color.Silver;
            this.picVirtualClip.Size = new System.Drawing.Size(40, 40);
            this.picVirtualClip.TabIndex = 37;
            this.picVirtualClip.TabStop = false;
            this.picVirtualClip.Tooltip_Text = "Tooltip text";
            this.picVirtualClip.UseInternalToolTip = false;
            this.picVirtualClip.QueryContinueDrag += new System.Windows.Forms.QueryContinueDragEventHandler(this.picVirtualClip_QueryContinueDrag);
            this.picVirtualClip.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picVirtualClip_MouseDown);
            this.picVirtualClip.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picVirtualClip_MouseMove);
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label4.Location = new System.Drawing.Point(13, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 17);
            this.label4.TabIndex = 41;
            this.label4.Text = "CLIP DRAG";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(192, 13);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 48;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(110, 13);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 47;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ListSources0
            // 
            this.ListSources0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.ListSources0.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ListSources0.ContextMenuStrip = this.mnuGallery;
            this.ListSources0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListSources0.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ListSources0.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListSources0.FormattingEnabled = true;
            this.ListSources0.IntegralHeight = false;
            this.ListSources0.ItemHeight = 40;
            this.ListSources0.Location = new System.Drawing.Point(0, 27);
            this.ListSources0.Name = "ListSources0";
            this.ListSources0.Size = new System.Drawing.Size(275, 211);
            this.ListSources0.TabIndex = 51;
            this.ListSources0.DoubleClick += new System.EventHandler(this.ListSources0_DoubleClick);
            this.ListSources0.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ListSources0_MouseDown);
            this.ListSources0.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ListSources0_MouseMove);
            // 
            // mnuGallery
            // 
            this.mnuGallery.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeToolStripMenuItem,
            this.censorEditorToolStripMenuItem});
            this.mnuGallery.Name = "mnuGallery";
            this.mnuGallery.Size = new System.Drawing.Size(153, 70);
            this.mnuGallery.Opening += new System.ComponentModel.CancelEventHandler(this.mnuGallery_Opening);
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Image = global::NewsMontange.Properties.Resources.Delete_icon_small;
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.removeToolStripMenuItem.Text = "Remove";
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.removeToolStripMenuItem_Click);
            // 
            // censorEditorToolStripMenuItem
            // 
            this.censorEditorToolStripMenuItem.Image = global::NewsMontange.Properties.Resources.ICO_Censor_Small;
            this.censorEditorToolStripMenuItem.Name = "censorEditorToolStripMenuItem";
            this.censorEditorToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.censorEditorToolStripMenuItem.Text = "Censor Editor";
            this.censorEditorToolStripMenuItem.Click += new System.EventHandler(this.censorEditorToolStripMenuItem_Click);
            // 
            // pnlLblSource
            // 
            this.pnlLblSource.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pnlLblSource.Controls.Add(this.picListSrc_Style);
            this.pnlLblSource.Controls.Add(this.lblSources);
            this.pnlLblSource.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlLblSource.Location = new System.Drawing.Point(0, 5);
            this.pnlLblSource.Name = "pnlLblSource";
            this.pnlLblSource.Size = new System.Drawing.Size(275, 22);
            this.pnlLblSource.TabIndex = 0;
            // 
            // picListSrc_Style
            // 
            this.picListSrc_Style.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picListSrc_Style.Image = global::NewsMontange.Properties.Resources.ListSrc_ico_icon;
            this.picListSrc_Style.Location = new System.Drawing.Point(252, 1);
            this.picListSrc_Style.Name = "picListSrc_Style";
            this.picListSrc_Style.Size = new System.Drawing.Size(20, 20);
            this.picListSrc_Style.TabIndex = 0;
            this.picListSrc_Style.TabStop = false;
            this.picListSrc_Style.Visible = false;
            // 
            // lblSources
            // 
            this.lblSources.AutoSize = true;
            this.lblSources.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSources.ForeColor = System.Drawing.Color.Silver;
            this.lblSources.Location = new System.Drawing.Point(3, 3);
            this.lblSources.Name = "lblSources";
            this.lblSources.Size = new System.Drawing.Size(100, 15);
            this.lblSources.TabIndex = 0;
            this.lblSources.Text = "Videoclips Gallery";
            // 
            // pnlTLSrc
            // 
            this.pnlTLSrc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(91)))), ((int)(((byte)(91)))), ((int)(((byte)(91)))));
            this.pnlTLSrc.Controls.Add(this.Timeline);
            this.pnlTLSrc.Controls.Add(this.pnlTrackInfo_Src);
            this.pnlTLSrc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTLSrc.Location = new System.Drawing.Point(0, 0);
            this.pnlTLSrc.Name = "pnlTLSrc";
            this.pnlTLSrc.Size = new System.Drawing.Size(1029, 130);
            this.pnlTLSrc.TabIndex = 38;
            // 
            // Timeline
            // 
            this.Timeline.AutoAdaptLines = false;
            this.Timeline.CanDisableTracks = false;
            this.Timeline.CanDragClip = false;
            this.Timeline.CanDragoutClips = true;
            this.Timeline.CanDragoutMarkers = false;
            this.Timeline.CanEditAudioNodes = true;
            this.Timeline.CanMarkOverRectime = true;
            this.Timeline.CanSeekOverClip = true;
            this.Timeline.CanSeekOverRectime = true;
            this.Timeline.ClipColor = System.Drawing.Color.LightSteelBlue;
            this.Timeline.ClipOfContextMenu = null;
            this.Timeline.ColorFocus = System.Drawing.Color.LightSalmon;
            this.Timeline.CurrentPosition = ((long)(0));
            this.Timeline.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Timeline.Enabled = false;
            this.Timeline.FireDisplayClipEventOnPan = true;
            this.Timeline.FPS = 25F;
            this.Timeline.Location = new System.Drawing.Point(72, 0);
            this.Timeline.Margin = new System.Windows.Forms.Padding(604214, 472870333, 604214, 472870333);
            this.Timeline.Menu_Clips = null;
            this.Timeline.Menu_General = null;
            this.Timeline.Name = "Timeline";
            this.Timeline.RecTime = ((long)(0));
            this.Timeline.RecTimeBarHeight = 0;
            this.Timeline.SelectedClipColor = System.Drawing.Color.LightSalmon;
            this.Timeline.ShowFocus = true;
            this.Timeline.ShowFocusBorder = false;
            this.Timeline.Size = new System.Drawing.Size(957, 130);
            this.Timeline.TabIndex = 0;
            this.Timeline.TCIN = ((long)(0));
            this.Timeline.TCOUT = ((long)(500));
            this.Timeline.TimeBarHeight = 20;
            this.Timeline.TimeLineBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.Timeline.TimeLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.Timeline.TrackBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))));
            this.Timeline.TrackColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(125)))), ((int)(((byte)(127)))));
            this.Timeline.TrackHandleColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(125)))), ((int)(((byte)(127)))));
            this.Timeline.TrackHeight = 40;
            this.Timeline.TrackInfoControl_Container = this.pnlTrackInfo_Src;
            this.Timeline.UndoTimes = 10;
            this.Timeline.ZoomBarHeight = 20;
            this.Timeline.ZoomIn = ((long)(0));
            this.Timeline.ZoomLimitMin = 1F;
            this.Timeline.ZoomOut = ((long)(0));
            this.Timeline.OnZoomChanged += new MediaAlliance.Controls.MxpTimeLine.MXPTL_ZoomAreaChanged(this.Timeline_OnZoomChanged);
            this.Timeline.OnChangePosition += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnChangePosition(this.Timeline_OnChangePosition);
            this.Timeline.OnChangeMarker += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnChangeMarker(this.Timeline_OnChangeMarker);
            this.Timeline.OnInternalEngineEvent += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnInternalEngineEvent(this.Timeline_OnInternalEngineEvent);
            this.Timeline.OnExitedFromActions += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnExitedFromActions(this.Timeline_OnExitedFromActions);
            this.Timeline.OnRequestPlaymarked += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnRequestPlaymarked(this.Timeline_OnRequestPlaymarked);
            // 
            // pnlTrackInfo_Src
            // 
            this.pnlTrackInfo_Src.BackColor = System.Drawing.Color.Transparent;
            this.pnlTrackInfo_Src.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlTrackInfo_Src.Location = new System.Drawing.Point(0, 0);
            this.pnlTrackInfo_Src.Name = "pnlTrackInfo_Src";
            this.pnlTrackInfo_Src.Size = new System.Drawing.Size(72, 130);
            this.pnlTrackInfo_Src.TabIndex = 2;
            // 
            // imgsSrcThumb
            // 
            this.imgsSrcThumb.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imgsSrcThumb.ImageSize = new System.Drawing.Size(80, 50);
            this.imgsSrcThumb.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // SRC_VIDEO
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.splitContainer3);
            this.Name = "SRC_VIDEO";
            this.Size = new System.Drawing.Size(1034, 372);
            this.Load += new System.EventHandler(this.SRC_VIDEO_Load);
            this.QueryContinueDrag += new System.Windows.Forms.QueryContinueDragEventHandler(this.SRC_VIDEO_QueryContinueDrag);
            this.ParentChanged += new System.EventHandler(this.SRC_VIDEO_ParentChanged);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.ResumeLayout(false);
            this.pnlSourceViewer.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            this.splitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picOpen)).EndInit();
            this.panel1.ResumeLayout(false);
            this.pnlMarkersTC.ResumeLayout(false);
            this.pnlMarkersTC.PerformLayout();
            this.mnuGallery.ResumeLayout(false);
            this.pnlLblSource.ResumeLayout(false);
            this.pnlLblSource.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picListSrc_Style)).EndInit();
            this.pnlTLSrc.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.Panel pnlSourceViewer;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel pnlLblSource;
        private System.Windows.Forms.PictureBox picListSrc_Style;
        private System.Windows.Forms.Label lblSources;
        private System.Windows.Forms.Panel pnlTLSrc;
        private System.Windows.Forms.Panel pnlTrackInfo_Src;
        public MediaAlliance.Controls.MxpTimeLine.MxpTmeLine Timeline;
        private System.Windows.Forms.OpenFileDialog openFilesource;
        private System.Windows.Forms.ImageList imgsSrcThumb;
        private System.Windows.Forms.Panel panel1;
        private MA_TimeCodeUC TimeCode_Position;
        private System.Windows.Forms.Label lblCurrentPosition;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblZoomAllSrc;
        public UserControls.MA_NMListSources ListSources0;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel pnlMarkersTC;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private MA_TimeCodeUC tc_markout;
        private MA_TimeCodeUC tc_markin;
        private System.Windows.Forms.Panel pnlLine;
        private System.Windows.Forms.ContextMenuStrip mnuGallery;
        private System.Windows.Forms.ToolStripMenuItem removeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem censorEditorToolStripMenuItem;
        public MAButton picVirtualClip;
        public MAButton btnSendToB;
        public MAButton btnSendToA;
        public MAButton picPlayVirtualClip;
        public MAButton picZoomAllSrc2;
        public System.Windows.Forms.PictureBox picOpen;
    }
}
