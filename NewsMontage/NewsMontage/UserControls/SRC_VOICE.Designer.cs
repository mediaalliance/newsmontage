﻿using MediaAlliance.Controls;
namespace NewsMontange.UserControls
{
    partial class SRC_VOICE
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SRC_VOICE));
            Telerik.WinControls.ThemeSource themeSource1 = new Telerik.WinControls.ThemeSource();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.pnlSourceViewer = new System.Windows.Forms.Panel();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.richEnlarge = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.richText = new NewsMontange.UserControls.MA_NMRichTextUC();
            this.lblOpen = new System.Windows.Forms.Label();
            this.picOpen = new System.Windows.Forms.PictureBox();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.chkPlayOnRec = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tc_rectime = new NewsMontange.UserControls.MA_TimeCodeUC();
            this.pnlVUMeter = new System.Windows.Forms.Panel();
            this.voiceRecorderUC1 = new MediaAlliance.AV.VoiceRecorderUC();
            this.cmbAudioHwSource = new Telerik.WinControls.UI.RadDropDownButton();
            this.radMenuButtonItem1 = new Telerik.WinControls.UI.RadMenuButtonItem();
            this.btnRec = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlMarkersTC = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tc_markout = new NewsMontange.UserControls.MA_TimeCodeUC();
            this.tc_markin = new NewsMontange.UserControls.MA_TimeCodeUC();
            this.pnlLine = new System.Windows.Forms.Panel();
            this.btnZoomAll = new MediaAlliance.Controls.MAButton();
            this.btnPlayMarker = new MediaAlliance.Controls.MAButton();
            this.btnVirtualClip = new MediaAlliance.Controls.MAButton();
            this.label6 = new System.Windows.Forms.Label();
            this.lblCurrentPosition = new System.Windows.Forms.Label();
            this.TimeCode_Position = new NewsMontange.UserControls.MA_TimeCodeUC();
            this.lblZoomAllSrc = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ListSources0 = new NewsMontange.UserControls.MA_NMListSources();
            this.mnuGallery = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlLblSource = new System.Windows.Forms.Panel();
            this.picListSrc_Style = new System.Windows.Forms.PictureBox();
            this.lblSources = new System.Windows.Forms.Label();
            this.pnlTLSrc = new System.Windows.Forms.Panel();
            this.Timeline = new MediaAlliance.Controls.MxpTimeLine.MxpTmeLine();
            this.pnlTrackInfo_Src = new System.Windows.Forms.Panel();
            this.imgsSrcThumb = new System.Windows.Forms.ImageList(this.components);
            this.openFilesource = new System.Windows.Forms.OpenFileDialog();
            this.radThemeManager1 = new Telerik.WinControls.RadThemeManager();
            this.copyToToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToDialog = new System.Windows.Forms.SaveFileDialog();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.pnlSourceViewer.SuspendLayout();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.richEnlarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOpen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            this.pnlVUMeter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbAudioHwSource)).BeginInit();
            this.panel1.SuspendLayout();
            this.pnlMarkersTC.SuspendLayout();
            this.mnuGallery.SuspendLayout();
            this.pnlLblSource.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picListSrc_Style)).BeginInit();
            this.pnlTLSrc.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer3.IsSplitterFixed = true;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.pnlSourceViewer);
            this.splitContainer3.Panel1.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.pnlTLSrc);
            this.splitContainer3.Panel2.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.splitContainer3.Size = new System.Drawing.Size(1034, 372);
            this.splitContainer3.SplitterDistance = 238;
            this.splitContainer3.TabIndex = 39;
            // 
            // pnlSourceViewer
            // 
            this.pnlSourceViewer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(91)))), ((int)(((byte)(91)))), ((int)(((byte)(91)))));
            this.pnlSourceViewer.Controls.Add(this.splitContainer4);
            this.pnlSourceViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSourceViewer.Location = new System.Drawing.Point(0, 0);
            this.pnlSourceViewer.Margin = new System.Windows.Forms.Padding(5, 3, 3, 3);
            this.pnlSourceViewer.Name = "pnlSourceViewer";
            this.pnlSourceViewer.Size = new System.Drawing.Size(1029, 238);
            this.pnlSourceViewer.TabIndex = 0;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer4.IsSplitterFixed = true;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.richEnlarge);
            this.splitContainer4.Panel1.Controls.Add(this.label1);
            this.splitContainer4.Panel1.Controls.Add(this.richText);
            this.splitContainer4.Panel1.Controls.Add(this.lblOpen);
            this.splitContainer4.Panel1.Controls.Add(this.picOpen);
            this.splitContainer4.Panel1.Controls.Add(this.radPanel1);
            this.splitContainer4.Panel1.Controls.Add(this.label4);
            this.splitContainer4.Panel1.Controls.Add(this.panel1);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.ListSources0);
            this.splitContainer4.Panel2.Controls.Add(this.pnlLblSource);
            this.splitContainer4.Panel2.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.splitContainer4.Size = new System.Drawing.Size(1029, 238);
            this.splitContainer4.SplitterDistance = 750;
            this.splitContainer4.TabIndex = 49;
            // 
            // richEnlarge
            // 
            this.richEnlarge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.richEnlarge.Image = global::NewsMontange.Properties.Resources.icon_enlarge;
            this.richEnlarge.Location = new System.Drawing.Point(691, 154);
            this.richEnlarge.Name = "richEnlarge";
            this.richEnlarge.Size = new System.Drawing.Size(15, 15);
            this.richEnlarge.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.richEnlarge.TabIndex = 68;
            this.richEnlarge.TabStop = false;
            this.richEnlarge.Click += new System.EventHandler(this.richEnlarge_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label1.Location = new System.Drawing.Point(284, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(403, 13);
            this.label1.TabIndex = 67;
            this.label1.Text = "TEXT";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // richText
            // 
            this.richText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richText.BackColor = System.Drawing.Color.DimGray;
            this.richText.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richText.Location = new System.Drawing.Point(284, 52);
            this.richText.Name = "richText";
            this.richText.Padding = new System.Windows.Forms.Padding(2);
            this.richText.Rtf = "{\\rtf1\\ansi\\ansicpg1252\\deff0\\deflang1040{\\fonttbl{\\f0\\fnil\\fcharset0 Segoe UI;}}" +
    "\r\n\\viewkind4\\uc1\\pard\\f0\\fs20\\par\r\n}\r\n";
            this.richText.SelectionFont = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richText.Size = new System.Drawing.Size(403, 117);
            this.richText.TabIndex = 66;
            this.richText.ZoomFactor = 1F;
            this.richText.TextChanged += new System.EventHandler(this.richText_TextChanged);
            // 
            // lblOpen
            // 
            this.lblOpen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOpen.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOpen.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lblOpen.Location = new System.Drawing.Point(695, 47);
            this.lblOpen.Name = "lblOpen";
            this.lblOpen.Size = new System.Drawing.Size(54, 13);
            this.lblOpen.TabIndex = 65;
            this.lblOpen.Text = "OPEN";
            this.lblOpen.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblOpen.Visible = false;
            // 
            // picOpen
            // 
            this.picOpen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picOpen.Image = global::NewsMontange.Properties.Resources.Folder_01;
            this.picOpen.Location = new System.Drawing.Point(695, 5);
            this.picOpen.Name = "picOpen";
            this.picOpen.Size = new System.Drawing.Size(52, 39);
            this.picOpen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picOpen.TabIndex = 64;
            this.picOpen.TabStop = false;
            this.picOpen.Visible = false;
            this.picOpen.Click += new System.EventHandler(this.picOpen_Click);
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.chkPlayOnRec);
            this.radPanel1.Controls.Add(this.label7);
            this.radPanel1.Controls.Add(this.tc_rectime);
            this.radPanel1.Controls.Add(this.pnlVUMeter);
            this.radPanel1.Controls.Add(this.cmbAudioHwSource);
            this.radPanel1.Controls.Add(this.btnRec);
            this.radPanel1.Location = new System.Drawing.Point(9, 52);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(269, 117);
            this.radPanel1.TabIndex = 62;
            ((Telerik.WinControls.UI.RadPanelElement)(this.radPanel1.GetChildAt(0))).Text = "";
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radPanel1.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(116)))), ((int)(((byte)(32)))));
            // 
            // chkPlayOnRec
            // 
            this.chkPlayOnRec.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chkPlayOnRec.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkPlayOnRec.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPlayOnRec.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.chkPlayOnRec.Location = new System.Drawing.Point(82, 97);
            this.chkPlayOnRec.Name = "chkPlayOnRec";
            this.chkPlayOnRec.Size = new System.Drawing.Size(181, 17);
            this.chkPlayOnRec.TabIndex = 62;
            this.chkPlayOnRec.Text = "Play on Rec";
            this.chkPlayOnRec.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkPlayOnRec.UseVisualStyleBackColor = false;
            this.chkPlayOnRec.CheckedChanged += new System.EventHandler(this.chkPlayOnRec_CheckedChanged);
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label7.Location = new System.Drawing.Point(161, 40);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 13);
            this.label7.TabIndex = 61;
            this.label7.Text = "RECORD TIME ELAPSED";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tc_rectime
            // 
            this.tc_rectime.BackColor_Selected = System.Drawing.Color.DimGray;
            this.tc_rectime.BorderColor = System.Drawing.Color.Gray;
            this.tc_rectime.BorderFocusesColor = System.Drawing.Color.LightSalmon;
            this.tc_rectime.Font = new System.Drawing.Font("LCD_PS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tc_rectime.ForeColor = System.Drawing.Color.Gray;
            this.tc_rectime.FPS = 25F;
            this.tc_rectime.Location = new System.Drawing.Point(161, 57);
            this.tc_rectime.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tc_rectime.Name = "tc_rectime";
            this.tc_rectime.Size = new System.Drawing.Size(102, 23);
            this.tc_rectime.TabIndex = 59;
            this.tc_rectime.TimeCode = ((long)(0));
            // 
            // pnlVUMeter
            // 
            this.pnlVUMeter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlVUMeter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pnlVUMeter.Controls.Add(this.voiceRecorderUC1);
            this.pnlVUMeter.Location = new System.Drawing.Point(5, 5);
            this.pnlVUMeter.Name = "pnlVUMeter";
            this.pnlVUMeter.Size = new System.Drawing.Size(69, 109);
            this.pnlVUMeter.TabIndex = 51;
            // 
            // voiceRecorderUC1
            // 
            this.voiceRecorderUC1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.voiceRecorderUC1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.voiceRecorderUC1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.voiceRecorderUC1.FileName = "C:\\Program Files (x86)\\Microsoft Visual Studio 10.0\\Common7\\IDE\\temp.wav";
            this.voiceRecorderUC1.ID = 0;
            this.voiceRecorderUC1.Location = new System.Drawing.Point(0, 0);
            this.voiceRecorderUC1.Name = "voiceRecorderUC1";
            this.voiceRecorderUC1.Size = new System.Drawing.Size(69, 109);
            this.voiceRecorderUC1.TabIndex = 52;
            // 
            // cmbAudioHwSource
            // 
            this.cmbAudioHwSource.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbAudioHwSource.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuButtonItem1});
            this.cmbAudioHwSource.Location = new System.Drawing.Point(82, 5);
            this.cmbAudioHwSource.Name = "cmbAudioHwSource";
            this.cmbAudioHwSource.Size = new System.Drawing.Size(181, 24);
            this.cmbAudioHwSource.TabIndex = 55;
            this.cmbAudioHwSource.Text = "radDropDownButton1";
            // 
            // radMenuButtonItem1
            // 
            this.radMenuButtonItem1.Name = "radMenuButtonItem1";
            this.radMenuButtonItem1.Text = "radMenuButtonItem1";
            // 
            // btnRec
            // 
            this.btnRec.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnRec.Enabled = false;
            this.btnRec.Location = new System.Drawing.Point(82, 43);
            this.btnRec.Name = "btnRec";
            this.btnRec.Size = new System.Drawing.Size(73, 38);
            this.btnRec.TabIndex = 53;
            this.btnRec.Text = "REC";
            this.btnRec.UseVisualStyleBackColor = false;
            this.btnRec.Click += new System.EventHandler(this.btnRec_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label4.Location = new System.Drawing.Point(9, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(269, 13);
            this.label4.TabIndex = 61;
            this.label4.Text = "AUDIO SOURCES";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pnlMarkersTC);
            this.panel1.Controls.Add(this.btnZoomAll);
            this.panel1.Controls.Add(this.btnPlayMarker);
            this.panel1.Controls.Add(this.btnVirtualClip);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.lblCurrentPosition);
            this.panel1.Controls.Add(this.TimeCode_Position);
            this.panel1.Controls.Add(this.lblZoomAllSrc);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 175);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(750, 63);
            this.panel1.TabIndex = 50;
            // 
            // pnlMarkersTC
            // 
            this.pnlMarkersTC.Controls.Add(this.label2);
            this.pnlMarkersTC.Controls.Add(this.label3);
            this.pnlMarkersTC.Controls.Add(this.tc_markout);
            this.pnlMarkersTC.Controls.Add(this.tc_markin);
            this.pnlMarkersTC.Controls.Add(this.pnlLine);
            this.pnlMarkersTC.Location = new System.Drawing.Point(165, 0);
            this.pnlMarkersTC.Name = "pnlMarkersTC";
            this.pnlMarkersTC.Size = new System.Drawing.Size(303, 29);
            this.pnlMarkersTC.TabIndex = 66;
            this.pnlMarkersTC.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DarkGray;
            this.label2.Location = new System.Drawing.Point(6, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(14, 12);
            this.label2.TabIndex = 60;
            this.label2.Text = "IN";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DarkGray;
            this.label3.Location = new System.Drawing.Point(276, 1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 12);
            this.label3.TabIndex = 61;
            this.label3.Text = "OUT";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tc_markout
            // 
            this.tc_markout.BackColor_Selected = System.Drawing.Color.DimGray;
            this.tc_markout.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.tc_markout.BorderFocusesColor = System.Drawing.Color.LightSalmon;
            this.tc_markout.Font = new System.Drawing.Font("LCD_PS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tc_markout.ForeColor = System.Drawing.Color.Gray;
            this.tc_markout.FPS = 25F;
            this.tc_markout.Location = new System.Drawing.Point(168, 2);
            this.tc_markout.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tc_markout.Name = "tc_markout";
            this.tc_markout.Size = new System.Drawing.Size(102, 20);
            this.tc_markout.TabIndex = 59;
            this.tc_markout.TimeCode = ((long)(0));
            this.tc_markout.OnTimecodeChanged += new NewsMontange.UserControls.MA_TimeCodeUC.MATC_OnTimecodeChanged(this.tc_markout_OnTimecodeChanged);
            // 
            // tc_markin
            // 
            this.tc_markin.BackColor_Selected = System.Drawing.Color.DimGray;
            this.tc_markin.BorderColor = System.Drawing.Color.Green;
            this.tc_markin.BorderFocusesColor = System.Drawing.Color.LightSalmon;
            this.tc_markin.Font = new System.Drawing.Font("LCD_PS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tc_markin.ForeColor = System.Drawing.Color.Gray;
            this.tc_markin.FPS = 25F;
            this.tc_markin.Location = new System.Drawing.Point(22, 2);
            this.tc_markin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tc_markin.Name = "tc_markin";
            this.tc_markin.Size = new System.Drawing.Size(102, 20);
            this.tc_markin.TabIndex = 58;
            this.tc_markin.TimeCode = ((long)(0));
            this.tc_markin.OnTimecodeChanged += new NewsMontange.UserControls.MA_TimeCodeUC.MATC_OnTimecodeChanged(this.tc_markin_OnTimecodeChanged);
            // 
            // pnlLine
            // 
            this.pnlLine.Location = new System.Drawing.Point(74, 16);
            this.pnlLine.Name = "pnlLine";
            this.pnlLine.Size = new System.Drawing.Size(149, 11);
            this.pnlLine.TabIndex = 60;
            this.pnlLine.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlLine_Paint);
            // 
            // btnZoomAll
            // 
            this.btnZoomAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnZoomAll.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.btnZoomAll.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.automatic;
            this.btnZoomAll.Image_Click = null;
            this.btnZoomAll.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnZoomAll.Image_Hover")));
            this.btnZoomAll.Image_Normal = null;
            this.btnZoomAll.Image_Type = MediaAlliance.Controls.NmButtonIcon.ZoomAll;
            this.btnZoomAll.Location = new System.Drawing.Point(705, 4);
            this.btnZoomAll.Name = "btnZoomAll";
            this.btnZoomAll.Shortcut = System.Windows.Forms.Keys.None;
            this.btnZoomAll.ShortcutTextColor = System.Drawing.Color.Silver;
            this.btnZoomAll.Size = new System.Drawing.Size(40, 40);
            this.btnZoomAll.TabIndex = 65;
            this.btnZoomAll.Tooltip_Text = "Tooltip text";
            this.btnZoomAll.UseInternalToolTip = false;
            this.btnZoomAll.Load += new System.EventHandler(this.btnZoomAll_Click);
            this.btnZoomAll.Click += new System.EventHandler(this.btnZoomAll_Click);
            // 
            // btnPlayMarker
            // 
            this.btnPlayMarker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPlayMarker.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.btnPlayMarker.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.automatic;
            this.btnPlayMarker.Enabled = false;
            this.btnPlayMarker.Image_Click = null;
            this.btnPlayMarker.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnPlayMarker.Image_Hover")));
            this.btnPlayMarker.Image_Normal = null;
            this.btnPlayMarker.Image_Type = MediaAlliance.Controls.NmButtonIcon.PlayMarked;
            this.btnPlayMarker.Location = new System.Drawing.Point(656, 4);
            this.btnPlayMarker.Name = "btnPlayMarker";
            this.btnPlayMarker.Shortcut = System.Windows.Forms.Keys.None;
            this.btnPlayMarker.ShortcutTextColor = System.Drawing.Color.Silver;
            this.btnPlayMarker.Size = new System.Drawing.Size(40, 40);
            this.btnPlayMarker.TabIndex = 64;
            this.btnPlayMarker.Tooltip_Text = "Tooltip text";
            this.btnPlayMarker.UseInternalToolTip = false;
            this.btnPlayMarker.Click += new System.EventHandler(this.btnPlayMarker_Click);
            // 
            // btnVirtualClip
            // 
            this.btnVirtualClip.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.btnVirtualClip.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.automatic;
            this.btnVirtualClip.Enabled = false;
            this.btnVirtualClip.Image_Click = null;
            this.btnVirtualClip.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnVirtualClip.Image_Hover")));
            this.btnVirtualClip.Image_Normal = null;
            this.btnVirtualClip.Image_Type = MediaAlliance.Controls.NmButtonIcon.VirtualClip;
            this.btnVirtualClip.Location = new System.Drawing.Point(15, 4);
            this.btnVirtualClip.Name = "btnVirtualClip";
            this.btnVirtualClip.Shortcut = System.Windows.Forms.Keys.None;
            this.btnVirtualClip.ShortcutTextColor = System.Drawing.Color.Silver;
            this.btnVirtualClip.Size = new System.Drawing.Size(40, 40);
            this.btnVirtualClip.TabIndex = 62;
            this.btnVirtualClip.TabStop = false;
            this.btnVirtualClip.Tooltip_Text = "Tooltip text";
            this.btnVirtualClip.UseInternalToolTip = false;
            this.btnVirtualClip.QueryContinueDrag += new System.Windows.Forms.QueryContinueDragEventHandler(this.btnVirtualClip_QueryContinueDrag);
            this.btnVirtualClip.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picVirtualClip_MouseDown);
            this.btnVirtualClip.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picVirtualClip_MouseMove);
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label6.Location = new System.Drawing.Point(13, 46);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 17);
            this.label6.TabIndex = 63;
            this.label6.Text = "CLIP DRAG";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblCurrentPosition
            // 
            this.lblCurrentPosition.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentPosition.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lblCurrentPosition.Location = new System.Drawing.Point(165, 16);
            this.lblCurrentPosition.Name = "lblCurrentPosition";
            this.lblCurrentPosition.Size = new System.Drawing.Size(303, 13);
            this.lblCurrentPosition.TabIndex = 55;
            this.lblCurrentPosition.Text = "CURRENT POSITION";
            this.lblCurrentPosition.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // TimeCode_Position
            // 
            this.TimeCode_Position.BackColor_Selected = System.Drawing.Color.DimGray;
            this.TimeCode_Position.BorderColor = System.Drawing.Color.DarkGoldenrod;
            this.TimeCode_Position.BorderFocusesColor = System.Drawing.Color.LightSalmon;
            this.TimeCode_Position.Font = new System.Drawing.Font("LCD_PS", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TimeCode_Position.ForeColor = System.Drawing.Color.Gray;
            this.TimeCode_Position.FPS = 25F;
            this.TimeCode_Position.Location = new System.Drawing.Point(249, 31);
            this.TimeCode_Position.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TimeCode_Position.Name = "TimeCode_Position";
            this.TimeCode_Position.Size = new System.Drawing.Size(128, 30);
            this.TimeCode_Position.TabIndex = 54;
            this.TimeCode_Position.TimeCode = ((long)(0));
            this.TimeCode_Position.OnTimecodeChanged += new NewsMontange.UserControls.MA_TimeCodeUC.MATC_OnTimecodeChanged(this.TimeCode_Position_OnTimecodeChanged);
            // 
            // lblZoomAllSrc
            // 
            this.lblZoomAllSrc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblZoomAllSrc.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblZoomAllSrc.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lblZoomAllSrc.Location = new System.Drawing.Point(703, 47);
            this.lblZoomAllSrc.Name = "lblZoomAllSrc";
            this.lblZoomAllSrc.Size = new System.Drawing.Size(48, 14);
            this.lblZoomAllSrc.TabIndex = 44;
            this.lblZoomAllSrc.Text = "ZOOM ALL";
            this.lblZoomAllSrc.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label5.Location = new System.Drawing.Point(645, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 14);
            this.label5.TabIndex = 42;
            this.label5.Text = "PLAY MARK";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ListSources0
            // 
            this.ListSources0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.ListSources0.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ListSources0.ContextMenuStrip = this.mnuGallery;
            this.ListSources0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListSources0.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ListSources0.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListSources0.FormattingEnabled = true;
            this.ListSources0.IntegralHeight = false;
            this.ListSources0.ItemHeight = 40;
            this.ListSources0.Location = new System.Drawing.Point(0, 27);
            this.ListSources0.Name = "ListSources0";
            this.ListSources0.Size = new System.Drawing.Size(275, 211);
            this.ListSources0.TabIndex = 53;
            this.ListSources0.DoubleClick += new System.EventHandler(this.ListSources0_DoubleClick);
            this.ListSources0.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ListSources0_MouseDown);
            this.ListSources0.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ListSources0_MouseMove);
            // 
            // mnuGallery
            // 
            this.mnuGallery.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeToolStripMenuItem,
            this.copyToToolStripMenuItem});
            this.mnuGallery.Name = "mnuGallery";
            this.mnuGallery.Size = new System.Drawing.Size(118, 48);
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Image = global::NewsMontange.Properties.Resources.Delete_icon_small;
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.removeToolStripMenuItem.Text = "Remove";
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.removeToolStripMenuItem_Click);
            // 
            // pnlLblSource
            // 
            this.pnlLblSource.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pnlLblSource.Controls.Add(this.picListSrc_Style);
            this.pnlLblSource.Controls.Add(this.lblSources);
            this.pnlLblSource.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlLblSource.Location = new System.Drawing.Point(0, 5);
            this.pnlLblSource.Name = "pnlLblSource";
            this.pnlLblSource.Size = new System.Drawing.Size(275, 22);
            this.pnlLblSource.TabIndex = 52;
            // 
            // picListSrc_Style
            // 
            this.picListSrc_Style.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picListSrc_Style.Image = global::NewsMontange.Properties.Resources.ListSrc_ico_icon;
            this.picListSrc_Style.Location = new System.Drawing.Point(252, 1);
            this.picListSrc_Style.Name = "picListSrc_Style";
            this.picListSrc_Style.Size = new System.Drawing.Size(20, 20);
            this.picListSrc_Style.TabIndex = 0;
            this.picListSrc_Style.TabStop = false;
            this.picListSrc_Style.Visible = false;
            // 
            // lblSources
            // 
            this.lblSources.AutoSize = true;
            this.lblSources.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSources.ForeColor = System.Drawing.Color.Silver;
            this.lblSources.Location = new System.Drawing.Point(3, 3);
            this.lblSources.Name = "lblSources";
            this.lblSources.Size = new System.Drawing.Size(80, 15);
            this.lblSources.TabIndex = 0;
            this.lblSources.Text = "Voices Gallery";
            // 
            // pnlTLSrc
            // 
            this.pnlTLSrc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(91)))), ((int)(((byte)(91)))), ((int)(((byte)(91)))));
            this.pnlTLSrc.Controls.Add(this.Timeline);
            this.pnlTLSrc.Controls.Add(this.pnlTrackInfo_Src);
            this.pnlTLSrc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTLSrc.Location = new System.Drawing.Point(0, 0);
            this.pnlTLSrc.Name = "pnlTLSrc";
            this.pnlTLSrc.Size = new System.Drawing.Size(1029, 130);
            this.pnlTLSrc.TabIndex = 38;
            // 
            // Timeline
            // 
            this.Timeline.AutoAdaptLines = false;
            this.Timeline.CanDisableTracks = false;
            this.Timeline.CanDragClip = false;
            this.Timeline.CanDragoutClips = true;
            this.Timeline.CanDragoutMarkers = false;
            this.Timeline.CanEditAudioNodes = false;
            this.Timeline.CanMarkOverRectime = true;
            this.Timeline.CanSeekOverClip = true;
            this.Timeline.CanSeekOverRectime = true;
            this.Timeline.ClipColor = System.Drawing.Color.LightSteelBlue;
            this.Timeline.ClipOfContextMenu = null;
            this.Timeline.ColorFocus = System.Drawing.Color.LightSalmon;
            this.Timeline.CurrentPosition = ((long)(0));
            this.Timeline.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Timeline.Enabled = false;
            this.Timeline.FireDisplayClipEventOnPan = true;
            this.Timeline.FPS = 25F;
            this.Timeline.Location = new System.Drawing.Point(72, 0);
            this.Timeline.Margin = new System.Windows.Forms.Padding(604214, 472870333, 604214, 472870333);
            this.Timeline.Menu_Clips = null;
            this.Timeline.Menu_General = null;
            this.Timeline.Name = "Timeline";
            this.Timeline.RecTime = ((long)(0));
            this.Timeline.RecTimeBarHeight = 0;
            this.Timeline.SelectedClipColor = System.Drawing.Color.LightSalmon;
            this.Timeline.ShowFocus = true;
            this.Timeline.ShowFocusBorder = false;
            this.Timeline.Size = new System.Drawing.Size(957, 130);
            this.Timeline.TabIndex = 0;
            this.Timeline.TCIN = ((long)(0));
            this.Timeline.TCOUT = ((long)(500));
            this.Timeline.TimeBarHeight = 20;
            this.Timeline.TimeLineBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.Timeline.TimeLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.Timeline.TrackBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))));
            this.Timeline.TrackColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(125)))), ((int)(((byte)(127)))));
            this.Timeline.TrackHandleColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(125)))), ((int)(((byte)(127)))));
            this.Timeline.TrackHeight = 40;
            this.Timeline.TrackInfoControl_Container = this.pnlTrackInfo_Src;
            this.Timeline.UndoTimes = 10;
            this.Timeline.ZoomBarHeight = 20;
            this.Timeline.ZoomIn = ((long)(0));
            this.Timeline.ZoomLimitMin = 1F;
            this.Timeline.ZoomOut = ((long)(0));
            this.Timeline.OnZoomChanged += new MediaAlliance.Controls.MxpTimeLine.MXPTL_ZoomAreaChanged(this.Timeline_OnZoomChanged);
            this.Timeline.OnChangePosition += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnChangePosition(this.Timeline_OnChangePosition);
            this.Timeline.OnChangeMarker += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnChangeMarker(this.Timeline_OnChangeMarker);
            this.Timeline.OnInternalEngineEvent += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnInternalEngineEvent(this.Timeline_OnInternalEngineEvent);
            this.Timeline.OnExitedFromActions += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnExitedFromActions(this.Timeline_OnExitedFromActions);
            this.Timeline.OnClipDisplayInfoChanged += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnClipDisplayInfoChanged(this.Timeline_OnClipDisplayInfoChanged);
            this.Timeline.OnRequestPlaymarked += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnRequestPlaymarked(this.Timeline_OnRequestPlaymarked);
            // 
            // pnlTrackInfo_Src
            // 
            this.pnlTrackInfo_Src.BackColor = System.Drawing.Color.Transparent;
            this.pnlTrackInfo_Src.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlTrackInfo_Src.Location = new System.Drawing.Point(0, 0);
            this.pnlTrackInfo_Src.Name = "pnlTrackInfo_Src";
            this.pnlTrackInfo_Src.Size = new System.Drawing.Size(72, 130);
            this.pnlTrackInfo_Src.TabIndex = 2;
            // 
            // imgsSrcThumb
            // 
            this.imgsSrcThumb.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imgsSrcThumb.ImageSize = new System.Drawing.Size(80, 50);
            this.imgsSrcThumb.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // radThemeManager1
            // 
            themeSource1.ThemeLocation = "D:\\! MyExtra\\Telerik_Themes\\NewsMontage_Theme_Telerik_WinControls_UI_RadGridView." +
    "xml";
            this.radThemeManager1.LoadedThemes.AddRange(new Telerik.WinControls.ThemeSource[] {
            themeSource1});
            // 
            // copyToToolStripMenuItem
            // 
            this.copyToToolStripMenuItem.Name = "copyToToolStripMenuItem";
            this.copyToToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.copyToToolStripMenuItem.Text = "Copy to";
            this.copyToToolStripMenuItem.Click += new System.EventHandler(this.copyToToolStripMenuItem_Click);
            // 
            // copyToDialog
            // 
            this.copyToDialog.Title = "Copy To...";
            // 
            // SRC_VOICE
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.splitContainer3);
            this.Name = "SRC_VOICE";
            this.Size = new System.Drawing.Size(1034, 372);
            this.Load += new System.EventHandler(this.SRC_VOICE_Load);
            this.GiveFeedback += new System.Windows.Forms.GiveFeedbackEventHandler(this.SRC_VOICE_GiveFeedback);
            this.QueryContinueDrag += new System.Windows.Forms.QueryContinueDragEventHandler(this.SRC_VOICE_QueryContinueDrag);
            this.ParentChanged += new System.EventHandler(this.SRC_VOICE_ParentChanged);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.ResumeLayout(false);
            this.pnlSourceViewer.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel1.PerformLayout();
            this.splitContainer4.Panel2.ResumeLayout(false);
            this.splitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.richEnlarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOpen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            this.pnlVUMeter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbAudioHwSource)).EndInit();
            this.panel1.ResumeLayout(false);
            this.pnlMarkersTC.ResumeLayout(false);
            this.pnlMarkersTC.PerformLayout();
            this.mnuGallery.ResumeLayout(false);
            this.pnlLblSource.ResumeLayout(false);
            this.pnlLblSource.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picListSrc_Style)).EndInit();
            this.pnlTLSrc.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.Panel pnlSourceViewer;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblZoomAllSrc;
        private System.Windows.Forms.Panel pnlTLSrc;
        private System.Windows.Forms.Panel pnlTrackInfo_Src;
        public MediaAlliance.Controls.MxpTimeLine.MxpTmeLine Timeline;
        private System.Windows.Forms.OpenFileDialog openFilesource;
        private System.Windows.Forms.ImageList imgsSrcThumb;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlVUMeter;
        private MediaAlliance.AV.VoiceRecorderUC voiceRecorderUC1;
        private System.Windows.Forms.Label lblCurrentPosition;
        private MA_TimeCodeUC TimeCode_Position;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private MA_TimeCodeUC tc_markout;
        private MA_TimeCodeUC tc_markin;
        private System.Windows.Forms.Label label6;
        private Telerik.WinControls.UI.RadDropDownButton cmbAudioHwSource;
        private Telerik.WinControls.UI.RadMenuButtonItem radMenuButtonItem1;
        private System.Windows.Forms.Label label4;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private MA_TimeCodeUC tc_rectime;
        private System.Windows.Forms.Label label7;
        private Telerik.WinControls.RadThemeManager radThemeManager1;
        public MA_NMListSources ListSources0;
        private System.Windows.Forms.Panel pnlLblSource;
        private System.Windows.Forms.PictureBox picListSrc_Style;
        private System.Windows.Forms.Label lblSources;
        private System.Windows.Forms.Panel pnlMarkersTC;
        private System.Windows.Forms.Label lblOpen;
        private UserControls.MA_NMRichTextUC richText;
        private System.Windows.Forms.Panel pnlLine;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button btnRec;
        private System.Windows.Forms.ContextMenuStrip mnuGallery;
        private System.Windows.Forms.ToolStripMenuItem removeToolStripMenuItem;
        public MAButton btnVirtualClip;
        public MAButton btnPlayMarker;
        public MAButton btnZoomAll;
        public System.Windows.Forms.CheckBox chkPlayOnRec;
        public System.Windows.Forms.PictureBox picOpen;
        public System.Windows.Forms.PictureBox richEnlarge;
        private System.Windows.Forms.ToolStripMenuItem copyToToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog copyToDialog;
    }
}
