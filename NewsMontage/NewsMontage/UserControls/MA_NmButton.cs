﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Threading;
using System.Drawing.Imaging;


namespace MediaAlliance.Controls
{
    public partial class MA_NmButton : UserControl
    {
        private delegate void dlg_refresh();

        private Image picNormal             = null;
        private Image picHover              = null;
        private bool mouseHover             = false;

        private Thread th_fade              = null;
        private ManualResetEvent fade_mre   = new ManualResetEvent(false);
        private bool fade_stop              = false;
        private int fade_inc                = 50;
        private int fade_val                = 0;


        public MA_NmButton()
        {
            InitializeComponent();

            this.DoubleBuffered = true;
        }

        private void MA_NmButton_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                th_fade = new Thread(new ThreadStart(th_fade_task));
                th_fade.IsBackground = true;
                th_fade.Start();
            }
        }


        private void th_fade_task()
        {
            dlg_refresh refresh = new dlg_refresh(this.Refresh);

            while (!fade_stop)
            {
                if(fade_val == 0 || fade_val == 255)
                {
                    fade_mre.WaitOne();
                    fade_mre.Reset();
                }

                fade_val += fade_inc;

                if(fade_val < 0) fade_val = 0;
                if(fade_val > 255) fade_val = 255;

                if(!fade_stop) this.Invoke(refresh);

                Thread.Sleep(40);
            }

            fade_stop = false;
        }



        private void HOVER()
        {
            if (!this.DesignMode)
            {
                fade_inc = +25;
                fade_mre.Set();
            }
        }

        private void NORMAL()
        {
            if (!this.DesignMode)
            {
                fade_inc = -25;
                fade_mre.Set();
            }
        }



        private void SAFECLOSE()
        {
            if (th_fade != null)
            {
                fade_stop = true;
                fade_mre.Set();

                if (!th_fade.Join(60))
                {
                    th_fade.Abort();
                }
            }
        }



        protected override void OnMouseEnter(EventArgs e)
        {
            HOVER();
            mouseHover = true;
            Refresh();
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            NORMAL();
            mouseHover = false;
            Refresh();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (picNormal != null)
            {
                Rectangle drop_rect = new Rectangle((Width / 2) - (picNormal.Width / 2),
                                                    (Height / 2) - (picNormal.Height / 2),
                                                    picNormal.Width,
                                                    picNormal.Height);

                if (this.Enabled)
                {
                    if (mouseHover && picHover != null && picNormal != null)
                    {
                        e.Graphics.DrawImage(picNormal,
                                                drop_rect,
                                                new Rectangle(0, 0, picNormal.Width, picNormal.Height),
                                                GraphicsUnit.Pixel);


                        ImageAttributes ia = new ImageAttributes();

                        ColorMatrix cm = new ColorMatrix();

                        cm.Matrix33 = 1.0f / 255 * fade_val;

                        ia.SetColorMatrix(cm);

                        e.Graphics.DrawImage(picHover,
                                                drop_rect,
                                                0,
                                                0,
                                                picHover.Width,
                                                picHover.Height,
                                                GraphicsUnit.Pixel,
                                                ia);

                    }
                    else if (picNormal != null)
                    {
                        e.Graphics.DrawImage(picNormal,
                                                drop_rect,
                                                new Rectangle(0, 0, picNormal.Width, picNormal.Height),
                                                GraphicsUnit.Pixel);
                    }
                }
                else
                {
                    if (picNormal != null)
                    {
                        ImageAttributes ia = new ImageAttributes();

                        ColorMatrix cm = new ColorMatrix();

                        cm.Matrix33 = 1.0f / 255 * 150;

                        ia.SetColorMatrix(cm);

                        e.Graphics.DrawImage(picNormal,
                                                drop_rect,
                                                0,
                                                0,
                                                picNormal.Width,
                                                picNormal.Height,
                                                GraphicsUnit.Pixel,
                                                ia);
                    }
                }
            }
            base.OnPaint(e);
        }

        protected override void OnEnabledChanged(EventArgs e)
        {
            base.OnEnabledChanged(e);

            Refresh();
        }



        #region PUBLIC PROPERTIES

        public Image Image_Normal
        {
            get { return picNormal; }
            set
            {
                picNormal = value;
                Refresh();
            }
        }

        public Image Image_Hover
        {
            get { return picHover; }
            set
            {
                picHover = value;
                Refresh();
            }
        }

        #endregion PUBLIC PROPERTIES
    }
}
