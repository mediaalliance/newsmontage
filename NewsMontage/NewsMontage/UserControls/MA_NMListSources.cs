﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MediaAlliance.Controls.MxpTimeLine.Objects;
using System.IO;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace NewsMontange.UserControls
{
    [System.Reflection.ObfuscationAttribute(Feature = "all", Exclude = true)]
    public partial class MA_NMListSources : ListBox
    {
        private Font m_firstline_font;
        private Font m_secondline_font; 

        public MA_NMListSources() : base()
        {
            InitializeComponent();

            m_firstline_font    = new Font(Font.FontFamily, 8, FontStyle.Italic);
            m_secondline_font   = new Font(Font.FontFamily, 8, FontStyle.Bold);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            base.OnPaintBackground(pevent);
            pevent.Graphics.DrawLine(new Pen(Brushes.Silver, 1), 1, 0, 0, Height);
        }
        
        protected override void OnDrawItem(DrawItemEventArgs e)
        {
            if (this.DesignMode) return;

            if (e.Index >= 0)
            {
                bool selected = ((e.State & DrawItemState.Selected) == DrawItemState.Selected);

                if(this.Items[e.Index] is CMxpTL_ClipInfo)
                {
                    CMxpTL_ClipInfo me = (CMxpTL_ClipInfo)Items[e.Index];

                    e.Graphics.SmoothingMode = SmoothingMode.HighQuality;

                    if (selected)
                    {
                        e.Graphics.FillRectangle(new SolidBrush(GetLightenColor(BackColor, 20)), e.Bounds);
                    }
                    else
                    {
                        e.Graphics.FillRectangle(new SolidBrush(BackColor), e.Bounds);
                    }

                    Font lcd_font = new Font("LCD_PS", 9);
                    int tc_widht = (int)e.Graphics.MeasureString("00:00:00:00", lcd_font).Width;

                    Rectangle rThumb = new Rectangle(e.Bounds.X, e.Bounds.Y + 2, (e.Bounds.Height - 4) / 3 * 4, e.Bounds.Height - 4);

                    int alpha = (selected)? 255 : 150;

                    if ((NewsMontange.Helpers.CProjectsHelper.enSouceType)me.Tag == NewsMontange.Helpers.CProjectsHelper.enSouceType.video)
                    {
                        Image thumb = me.Thumbnail;
                        if (thumb == null) thumb = Properties.Resources.NoThumb;

                        ImageAttributes ia      = new ImageAttributes();
                        ColorMatrix cm          = new ColorMatrix();
                        cm.Matrix33             = 1.0f / 255 * alpha;
                        ia.SetColorMatrix(cm);

                        e.Graphics.DrawImage(thumb,
                                                rThumb,
                                                0,
                                                0,
                                                thumb.Width,
                                                thumb.Height,
                                                GraphicsUnit.Pixel,
                                                ia);
                        ia.Dispose();
                    }
                    else
                    {
                        ImageAttributes ia      = new ImageAttributes();
                        ColorMatrix cm          = new ColorMatrix();
                        cm.Matrix33             = 1.0f / 255 * alpha;
                        ia.SetColorMatrix(cm);

                        e.Graphics.DrawImage(Properties.Resources.AudioClipsIcon,
                                                rThumb,
                                                0,
                                                0,
                                                Properties.Resources.AudioClipsIcon.Width,
                                                Properties.Resources.AudioClipsIcon.Height,
                                                GraphicsUnit.Pixel,
                                                ia);
                        ia.Dispose();
                    }

#if CENSURA
                    if (me.CensorInfo != null)
                    {
                        e.Graphics.DrawImage(Properties.Resources.ICO_Censor_Small,
                                                new Rectangle(3, 3, Properties.Resources.ICO_Censor_Small.Width / 2, Properties.Resources.ICO_Censor_Small.Height / 2),
                                                new Rectangle(0, 0, Properties.Resources.ICO_Censor_Small.Width, Properties.Resources.ICO_Censor_Small.Height),
                                                GraphicsUnit.Pixel);
                    }
#endif

                    Rectangle rText0 = new Rectangle(rThumb.Right + 2, rThumb.Y, e.Bounds.Width - rThumb.Width - 4 - tc_widht - 4, 12);
                    e.Graphics.DrawString(Path.GetFileName(me.filename), m_firstline_font, Brushes.Silver, rText0);

                    Rectangle rText1 = new Rectangle(rThumb.Right + 2, rThumb.Y + 16, e.Bounds.Width - rThumb.Width - 4 - tc_widht - 4, rThumb.Height);
                    e.Graphics.DrawString(me.description, m_secondline_font, Brushes.Silver, rText1);

                    Rectangle rTC = new Rectangle(rText0.Right + 2, rText0.Y, tc_widht, rText0.Height - 2);
                    e.Graphics.DrawString(Frame2TC(me.Tcout), lcd_font, Brushes.Silver, rTC.Location);

                    Pen div = new Pen(Brushes.Gray, 1);
                    div.DashStyle = DashStyle.Dash;
                    e.Graphics.DrawLine(div, e.Bounds.X, e.Bounds.Y + e.Bounds.Height - 1, e.Bounds.Width, e.Bounds.Y + e.Bounds.Height - 1);

                    e.DrawFocusRectangle();
                }
            }
        }

        private Color GetDarkenColor(Color source, int factor)
        {
            int R = source.R;
            int G = source.G;
            int B = source.B;

            R -= factor;
            G -= factor;
            B -= factor;

            if (R < 0) R = 0;
            if (G < 0) G = 0;
            if (B < 0) B = 0;

            return Color.FromArgb(R, G, B);
        }

        private Color GetLightenColor(Color source, int factor)
        {
            int R = source.R;
            int G = source.G;
            int B = source.B;

            R += factor;
            G += factor;
            B += factor;

            if (R > 255) R = 255;
            if (G > 255) G = 255;
            if (B > 255) B = 255;

            return Color.FromArgb(R, G, B);
        }

        private string Frame2TC(long frame)
        {
            string result = "";
            long ffTot = frame;

            int FFrameRate = 25;

            int ss = (int)(frame / FFrameRate);
            int ff = (int)(frame - ss * FFrameRate);
            int mm = (int)(ss / 60);
            ss = ss - mm * 60;
            int hh = (int)(mm / 60);
            mm = mm - hh * 60;

            result += hh.ToString().PadLeft(2, '0') + ":";
            result += mm.ToString().PadLeft(2, '0') + ":";
            result += ss.ToString().PadLeft(2, '0') + ":";
            result += ff.ToString().PadLeft(2, '0');

            return result;
        }
    }
}
