﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Drawing.Drawing2D;
using System.IO;

using MediaAlliance.Controls.MxpTimeLine;
using MediaAlliance.Controls.MxpTimeLine.Objects;
using MediaAlliance.Controls.MxpTimeLine.Classes;
using NewsMontange.Helpers;
using MediaAlliance.AV;
using NewsMontange.Classes;
using MediaAlliance.Controls.MxpTimeLine.Engine;
using NewsMontange.Language;
using NewsMontange.Forms;
using MediaAlliance.AV.CensorTools;


namespace NewsMontange.UserControls
{
    [System.Reflection.ObfuscationAttribute(Feature = "all", Exclude = true)]
    public partial class SRC_VIDEO : UserControl
    {
        private CMxpTL_ClipInfo currentOpened_source    = null;
        private CMxpTL_ClipInfo currentDragging_source  = null;
        private Source sourcePreview                    = null;
        private Source censorPreview                    = null;
        private Control previewMonitor                  = null;
        private CProjectsHelper projectHelper           = null;

        private Point cliplist_downpos;
        private Point virtualClip_downpos;
        private MxpTmeLine montage_timeline             = null;

        private CThumbMaker thumbMaker                  = null;
        private long last_thumb_duration                = 0;

        private int LastThumbSecInterval                = -1;
        private int CurrentThumbSecInterval             = 0;

        private bool FirstStepOnLoaded                  = true;
        
        public List<MA_TimeCodeUC> CurrentPosition_Controls = new List<MA_TimeCodeUC>();



        public SRC_VIDEO()
        {
            InitializeComponent();

            removeToolStripMenuItem.Text = CDict.GetValue("MainForm.VideoSource.RemoveGalleryItem", "Remove");

            Timeline.InitializeInternalEngine();

            Timeline.Tracks.AddTrack("MAIN", enMxpTrackType.Video, 85, null);
            Timeline.Tracks[0].ThumbLayout = MxpClipThumbLayout.allPossible;
            Timeline.TCOUT = 25 * 60 * 1;
            Timeline.ZoomAll();

            TimeCode_Position.TimeCode = 0;

            imgsSrcThumb.Images.Add(Properties.Resources.NoThumb);

            //DeepFocusHandling(this);

#if CENSURA
#else
            censorEditorToolStripMenuItem.Visible = false;
#endif


        }




        //private void DeepFocusHandling(Control ctl)
        //{
        //    foreach (Control subctl in ctl.Controls)
        //    {
        //        subctl.GotFocus += new EventHandler(subctl_GotFocus);
        //        DeepFocusHandling(subctl);
        //    }
        //}
        
        private void SRC_VIDEO_Load(object sender, EventArgs e)
        {
            if (File.Exists(Program.LanguageFile))
            {
                CDict.AddFromFile(Program.LanguageFile);
            }

            lblSources.Text = CDict.GetValue("MainForm.VideoSource.GalleryTitle", lblSources.Text);
            label4.Text = CDict.GetValue("MainForm.VideoSource.ClipDrag", label4.Text);
            label6.Text = CDict.GetValue("Generic.Button.Open", label6.Text);
            label5.Text = CDict.GetValue("Generic.Button.PlayMark", label5.Text);
            lblZoomAllSrc.Text = CDict.GetValue("Generic.Button.ZoomAll", lblZoomAllSrc.Text);
            lblCurrentPosition.Text = CDict.GetValue("MainForm.VideoSource.CurrentPosition", lblCurrentPosition.Text);



            // INIZIALIZZO IL THUMBMAKER
            thumbMaker = new CThumbMaker();
            thumbMaker.OnDone += new CThumbMaker.ThM_OnDone(thumbMaker_OnDone);
            thumbMaker.START();
        }



        void thumbMaker_OnDone(object Sender, string thumbReady, long position, object tag)
        {
            if (tag != null && tag is CMxpTL_ClipInfo)
            {
                CMxpTL_ClipInfo clip_info = (CMxpTL_ClipInfo)tag;

                if (clip_info.Thumbnail == null)
                {
                    // SE LA CLIPINFO RELATIVA ALLA CLIP NON HA ANCORA UNA THUMBNAIL ASSEGNATA, PRENDO QUELLA APPENA GENERATA
                    // E AGGIORNO L'ELENCO DELLE CLIP PER FARLA VEDERE

                    FileStream fs = new FileStream(thumbReady, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                    clip_info.Thumbnail = (Bitmap)Bitmap.FromStream(fs);
                    fs.Close();
                    fs.Dispose();
                    ListSources0.Refresh();
                }
            }

            if (Timeline.Tracks[0].Clips.Count > 0)
            {
                if (tag is CMxpClip)
                {
                    CMxpClip clip = (CMxpClip)tag;

                    if (clip.ClipInfo.filename.Equals(Timeline.Tracks[0].Clips[0].ClipInfo.filename, StringComparison.OrdinalIgnoreCase))
                    {
                        // SE IL FILE DI QUI SI E' PRODOTTA LA THUMB E' LO STELLO DELLA CLIP IN TIMELINE

                        string file = thumbReady;// Path.Combine(thumbReady, "frame_sec" + position + "_000001.jpg");

                        if (File.Exists(file))
                        {
                            FileStream fs = new FileStream(thumbReady, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                            clip.SetThumb(position * 25, (Bitmap)Bitmap.FromStream(fs), MxpClip_ThumbPosition.Absolute);
                            fs.Close();
                            fs.Dispose();
                        }

                        clip.NotifyRender();
                    }
                }
            }

        }



        #region TIMELINE EVENTS

        private void Timeline_OnChangeMarker(object Sender, long FrameIn, long FrameOut, enTcChangeType change_type)
        {
            if (Timeline.InternalEngine.EngineStatus == MediaAlliance.Controls.MxpTimeLine.Engine.InternalEngineStatus.play)
            {
                Timeline.InternalEngine.Pause();
            }

            if (sourcePreview != null)
            {
                if (change_type == enTcChangeType.timecode_in)
                {
                    sourcePreview.Seek(FrameIn);
                    if (Program.Settings.Options.TIMELINE_AUDIOSCRUB)
                    {
                        sourcePreview.PlayScrub();
                    }
                }
                else if (change_type == enTcChangeType.timecode_out)
                {
                    sourcePreview.Seek(FrameOut);
                    if (Program.Settings.Options.TIMELINE_AUDIOSCRUB)
                    {
                        sourcePreview.PlayScrub();
                    }
                }
            }

            picVirtualClip.Enabled      = Timeline.IsMarked;
            btnSendToA.Enabled          = Timeline.IsMarked;
            btnSendToB.Enabled          = Timeline.IsMarked;
            picPlayVirtualClip.Enabled  = Timeline.IsMarked;

            pnlMarkersTC.Visible        = Timeline.IsMarked;
            lblCurrentPosition.Visible  = !Timeline.IsMarked;

            if (Timeline.IsMarked)
            {
                tc_markin.TimeCode          = Timeline.CurrentMarkIn;
                tc_markout.TimeCode         = Timeline.CurrentMarkOut;
                //tc_mark_duration.TimeCode   = Timeline.CurrentMarkOut - Timeline.CurrentMarkIn;
            }
            else
            {
                tc_markin.TimeCode = 0;
                tc_markout.TimeCode = 0;
            }
        }

        private void Timeline_OnChangePosition(object Sender, long position, bool Internal)
        {
            TimeCode_Position.TimeCode = position;

            foreach (MA_TimeCodeUC tc_ctl in CurrentPosition_Controls) tc_ctl.TimeCode = position;

            
            if (Internal)
            {
                if (Timeline.InternalEngine.EngineStatus == MediaAlliance.Controls.MxpTimeLine.Engine.InternalEngineStatus.play)
                {
                    Timeline.InternalEngine.Pause();
                }

                if (sourcePreview != null)
                {
                    sourcePreview.Seek(position);
                    if (Program.Settings.Options.TIMELINE_AUDIOSCRUB)
                    {
                        sourcePreview.PlayScrub();
                    }
                }
            }
        }

        private void Timeline_OnInternalEngineEvent(object Sender, MediaAlliance.Controls.MxpTimeLine.Engine.InternEngineEventHandle e)
        {
            switch (e.Type)
            {
                case MediaAlliance.Controls.MxpTimeLine.Engine.enIEngineEventType.undefined:
                    break;
                case MediaAlliance.Controls.MxpTimeLine.Engine.enIEngineEventType.onPlay:
                    if (montage_timeline != null)
                    {
                        if (montage_timeline.InternalEngine.EngineStatus == InternalEngineStatus.play)
                        {
                            // EVITO IL PLAY CONTEMPORANEO SULLE DUE TIMELINE
                            montage_timeline.InternalEngine.Pause();
                        }
                    }
                    if(sourcePreview != null) sourcePreview.Play();
                    break;
                case MediaAlliance.Controls.MxpTimeLine.Engine.enIEngineEventType.onPause:
                    if (sourcePreview != null) sourcePreview.Pause();
                    break;
                case MediaAlliance.Controls.MxpTimeLine.Engine.enIEngineEventType.onInitialized:
                    break;
                default:
                    break;
            }

        }

        private void Timeline_OnExitedFromActions(object Sender, MxpTimelineAction Action, object objref)
        {
            switch (Action)
            {
                case MxpTimelineAction.isZooming:
                    break;
                case MxpTimelineAction.isPanning:
                    break;
                case MxpTimelineAction.isEditingClipIn:
                    break;
                case MxpTimelineAction.isEditingClipOut:
                    if (FirstStepOnLoaded)
                    {
                        FirstStepOnLoaded = false;
                    }
                    break;
                case MxpTimelineAction.isMarkingIn:
                case MxpTimelineAction.isMarkingOut:
                    if (sourcePreview != null)
                    {
                        sourcePreview.Seek(Timeline.CurrentPosition);
                    }
                    break;
                case MxpTimelineAction.isScrubbing:
                    break;
            }   
        }

        private void Timeline_OnZoomChanged(object Sender, long ZoomIn, long ZoomOut)
        {
            if (Timeline.Tracks.Count > 0)
            {
                if (Timeline.Tracks[0].Clips.Count > 0)
                {
                    long thumb_duration = Timeline.Tracks[0].Clips[0].ThumbDuration;

                    //int img_width = 74;
                    //long view_duration = ZoomOut - ZoomIn;
                    //int frame_interlval = (int)(view_duration / 74);
                    int sec_interval = (int)((thumb_duration / 25) + .5);

                    if (sec_interval != CurrentThumbSecInterval)
                    {
                        CurrentThumbSecInterval = sec_interval;
                    }
                }
            }
            GENERATE_THUMB_MACRO();
        }

        private void Timeline_OnRequestPlaymarked(object Sender)
        {
            if (sourcePreview != null)
            {
                System.Threading.Thread.Sleep(200);
                sourcePreview.PlayMarked(Timeline.CurrentMarkIn, Timeline.CurrentMarkOut);
                Timeline.InternalEngine.PlayMark();
            }
        }

        #endregion TIMELINE EVENTS


        /// <summary>
        /// MACRO DA ESEGUIRE PER GENERARE/RECUPERARE LE THUMB DELLA CLIP APERTA
        /// </summary>
        private void GENERATE_THUMB_MACRO()
        {
            try
            {
                if (Timeline.Tracks.Count > 0)
                {
                    if (Timeline.Tracks[0].Clips.Count > 0)
                    {
                        CMxpClip clip = Timeline.Tracks[0].Clips[0];
                        CMxpTL_ClipInfo fClip = clip.ClipInfo;

                        long thumb_duration = Timeline.Tracks[0].Clips[0].ThumbDuration;

                        //if (thumb_duration < 40) return;

                        Console.WriteLine("TH-DURATION : " + thumb_duration);
                        


                        string thumbpath = Path.Combine(projectHelper.CurrentProject.Folder_FrameCache, Path.GetFileName(fClip.filename));
                        if (!Directory.Exists(thumbpath)) Directory.CreateDirectory(thumbpath);

                        if (CurrentThumbSecInterval == 0) CurrentThumbSecInterval = 1;


                        if (LastThumbSecInterval != CurrentThumbSecInterval)
                        {
                            Console.WriteLine(" THUMB INTERVAL CHANGED");

                            LastThumbSecInterval = CurrentThumbSecInterval;

                            clip.FrameThumbs.Clear();
                        }

                        for (int t = 0; t < fClip.Tcout; t += (CurrentThumbSecInterval * 25))
                        {
                            string thumb_filename = "";

                            if (thumbMaker.ExtistThumbFor(projectHelper.CurrentProject.Folder_FrameCache,
                                                            fClip.filename,
                                                            t / 25,
                                                            1,
                                                            out thumb_filename))
                            {
                                try
                                {
                                    clip.SetThumb(t, thumb_filename, MxpClip_ThumbPosition.Absolute);
                                }
                                catch { }
                            }
                            else
                            {
                                thumbMaker.AddRequest(fClip.filename, thumbpath, t / 25, clip);
                            }
                        }

                        clip.PreventRender = false;
                        clip.NotifyRender();
                    }
                }
            }
            catch
            {
            }
        }

        
        private void picOpen_Click(object sender, EventArgs e)
        {
            openFilesource.Title    = "Import an existing videoclip";
            openFilesource.Filter   = "VIDEOCLIP FILE|*.mov;*.avi";


            if (Program.Settings.Options.VIDEOCLIP_FILTER.Length > 0)
            {
                openFilesource.Filter = "VIDEOCLIP FILE|" + string.Join(";", Program.Settings.Options.VIDEOCLIP_FILTER);
            }

            if (openFilesource.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Program.Logger.info("Adding new Video resource : ");
                Program.Logger.info("   > Filename       : " + openFilesource.FileName);
                projectHelper.AddSource(openFilesource.FileName, "", 2, CProjectsHelper.enSouceType.video);
            }
        }

        private void picPlayVirtualClip_Click(object sender, EventArgs e)
        {
            Timeline.Request_PlayMarked();
        }

        private void picZoomAllSrc_Click(object sender, EventArgs e)
        {
            Timeline.ZoomAll();
        }
        
        private void picVirtualClip_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                virtualClip_downpos = Cursor.Position;
            }
        }

        private void picVirtualClip_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                int xdiff = Math.Abs(Cursor.Position.X - virtualClip_downpos.X);
                int ydiff = Math.Abs(Cursor.Position.Y - virtualClip_downpos.Y);

                if (xdiff > 15 || ydiff > 15)
                {
                    currentOpened_source.Tag            = NewsMontange.Helpers.CProjectsHelper.enSouceType.video;
                    currentDragging_source              = currentOpened_source;
                    currentOpened_source.Clipping_in    = Timeline.CurrentMarkIn;
                    currentOpened_source.Clipping_out   = Timeline.CurrentMarkOut;

                    picVirtualClip.DoDragDrop(currentOpened_source, DragDropEffects.Copy);
                }
            }
        }


        private void button2_Click(object sender, EventArgs e)
        {
            ththumb = new System.Threading.Thread(new System.Threading.ThreadStart(thth_Task));
            ththumb.IsBackground = true;
            ththumb.Start();


            //for (int i = 0; i < 1000; i += 100)
            //{
            //    cli.SetThumb(i, Properties.Resources.NoThumb);
            //}
        }


        private void fileSystemWatcher1_Created(object sender, FileSystemEventArgs e)
        {


            string fn = e.Name;
            int close_pos = fn.LastIndexOf('_');
            string numstr = fn.Substring(9, close_pos - 9);

            int sec = Convert.ToInt32(numstr);

            // FRAME_SECxxxxx_000001.BMP

            System.Threading.Thread.Sleep(250);
            cli.SetThumb((long)(sec * 25), (Bitmap)Bitmap.FromFile(e.FullPath), MxpClip_ThumbPosition.Absolute);

        }

        private void fileSystemWatcher1_Changed(object sender, FileSystemEventArgs e)
        {

        }



        CMxpClip cli = null;



        System.Threading.Thread ththumb = null;
        int times = 0;


        public void thth_Task()
        {
            while (times < 100)
            {
                FFMpegHelper.SceneChange sc = new FFMpegHelper.SceneChange();
                //sc.StartAndWait(@"C:\WORKS\VIDEO_TEST\Zastavka_OMN_SD_PAL_DV.mov", times, 1, @"D:\! MyExtra\Controls\MxpTimeLine\TEST_THUMB");
                sc.StartAndWait(@"C:\WORKS\VIDEO_TEST\test1.mov", times, 1, @"D:\! MyExtra\Controls\MxpTimeLine\TEST_THUMB");

                times += 3;
            }
        }




        #region PUBLIC PROPERTIES

        public Control PreviewMonitor
        {
            get { return previewMonitor; }
            set { previewMonitor = value; }
        }

        public CProjectsHelper ProjectHelper
        {
            get { return projectHelper; }
            set 
            { 
                projectHelper = value; 
            }
        }

        public CMxpTL_ClipInfo CurrentOpened
        {
            get { return currentOpened_source; }
        }

        public CMxpTL_ClipInfo CurrentDragging
        {
            get { return currentDragging_source; }
        }

        public MxpTmeLine Montage_timeline
        {
            get { return montage_timeline; }
            set
            {
                montage_timeline = value;
                Timeline.FPS = value.FPS;
            }
        }

        #endregion PUBLIC PROPERTIES


        #region PUBLIC METHODS

        public void UnloadOpenedClips()
        {
            CloseCurrentPreview();
        }

        public void CreateMainThumbnail(CMxpTL_ClipInfo clip_info)
        {
            string thumb_filename = "";

            if (thumbMaker.ExtistThumbFor(projectHelper.CurrentProject.Folder_FrameCache,
                                            clip_info.filename,
                                            1,
                                            1,
                                            out thumb_filename))
            {
                try
                {
                    FileStream fs = new FileStream(thumb_filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                    clip_info.Thumbnail = (Bitmap)Bitmap.FromStream(fs);
                    fs.Close();
                    fs.Dispose();
                    ListSources0.Refresh();
                }
                catch { }
            }
            else
            {
                string thumbpath = Path.Combine(projectHelper.CurrentProject.Folder_FrameCache, Path.GetFileName(clip_info.filename));
                if (!Directory.Exists(thumbpath)) Directory.CreateDirectory(thumbpath);
                thumbMaker.AddRequest(clip_info.filename, thumbpath, 1, clip_info);
            }
        }

        #endregion PUBLIC METHODS


        private void CloseCurrentPreview()
        {
            if (sourcePreview != null) sourcePreview.Close();

            thumbMaker.Reset();

            if (Timeline.Tracks[0].Clips.Count > 0)
            {
                Timeline.Tracks[0].Clips[0].Delete();
                Timeline.TCOUT = (long)Timeline.FPS * 60 * 5;
                Timeline.ZoomAll();
                Timeline.Enabled = false;
            }

        }


        #region SOURCE LIST EVENTS

        private void ListSources0_DoubleClick(object sender, EventArgs e)
        {
            if (ListSources0.SelectedItem != null)
            {
                FirstStepOnLoaded = true;

                CMxpTL_ClipInfo fClip = (CMxpTL_ClipInfo)ListSources0.SelectedItem;

                if (currentOpened_source != fClip)
                {
                    currentOpened_source = fClip;

                    CloseCurrentPreview();

                    if (!Program.Settings.Options.PREVIEW_ENGINE_DS_ONLY)
                    {
                        if (Path.GetExtension(fClip.filename).ToLower().Contains("mov"))
                        {
                            sourcePreview = new QTPreview(Program.Settings.Options.TIMELINE_AUDIOSCRUB);
                        }
                        else
                        {
                            sourcePreview = new DSPreview(Program.Settings.Options.TIMELINE_AUDIOSCRUB);
                            ((DSPreview)sourcePreview).OnChangeState += new DSPreview.DSPreview_OnChangeState(SRC_VIDEO_OnChangeState);
                        }
                    }
                    else
                    {
                        sourcePreview = new DSPreview(Program.Settings.Options.TIMELINE_AUDIOSCRUB);
                        ((DSPreview)sourcePreview).OnChangeState += new DSPreview.DSPreview_OnChangeState(SRC_VIDEO_OnChangeState);
                    }

                    if (previewMonitor != null) previewMonitor.Controls.Add(sourcePreview);

                    sourcePreview.NewInstance(fClip.filename);
                    sourcePreview.ViewInfo = Program.Settings.Options.VIDEO_INFO_OSD_VISIBLE;
                    sourcePreview.Visible = true;


                    Timeline.Enabled = true;
                    Timeline.HideMarks();
                    Timeline.TCOUT = fClip.Tcout;
                    Timeline.ZoomAll();

                    Timeline.Tracks[0].AddClip("", enCMxpClipType.Video, fClip, null, 0, fClip.Tcout, 0, fClip.Tcout);
                    Timeline_OnZoomChanged(Timeline, Timeline.ZoomIn, Timeline.ZoomOut);
                }
            }
        }

        void SRC_VIDEO_OnChangeState(object Sender, DSPreview.MAPreviewState State)
        {
            switch (State)
            {
                case DSPreview.MAPreviewState.None:
                    break;
                case DSPreview.MAPreviewState.Playing:
                    break;
                case DSPreview.MAPreviewState.PlayingMarked:
                    break;
                case DSPreview.MAPreviewState.Stoped:
                    break;
                case DSPreview.MAPreviewState.Cued:
                    break;
                case DSPreview.MAPreviewState.Paused:
                    if (Timeline.InternalEngine.EngineStatus == InternalEngineStatus.play)
                    {
                        Timeline.InternalEngine.Pause();
                        
                        if (Timeline.IsMarked)
                        {
                            // SE LO STATO E' PLAY E C'E' UNA MARCATURA VUOL DIRE CHE SONO IN PLAYMARK
                            // IL THREAD FA FINIRE A VOLTE IL CURSORE OLTRE LA MARCATURA, SOLO GRAFICAMENTE
                            // QUESTO CORREGGE IL DIFETTO
                            Timeline.CurrentPosition = Timeline.CurrentMarkOut;
                        }
                    }
                    break;
                case DSPreview.MAPreviewState.Finished:
                    break;
                default:
                    break;
            }
        }

        private void ListSources0_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                cliplist_downpos = e.Location;
            }
        }

        private void ListSources0_MouseMove(object sender, MouseEventArgs e)
        {
            if (ListSources0.SelectedItem != null)
            {
                CMxpTL_ClipInfo fClip = (CMxpTL_ClipInfo)ListSources0.SelectedItem;

                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    int xdiff = Math.Abs(e.Location.X - cliplist_downpos.X);
                    int ydiff = Math.Abs(e.Location.Y - cliplist_downpos.Y);

                    if (xdiff > 15 || ydiff > 15)
                    {
                        fClip.Clipping_in           = 0;
                        fClip.Clipping_out          = fClip.Tcout;
                        currentDragging_source      = fClip;
                        this.DoDragDrop(fClip, DragDropEffects.Copy);
                    }
                }
            }
        }

        #endregion SOURCE LIST EVENTS



        private void btnSendToA_Click(object sender, EventArgs e)
        {
            SendToTrack(1);
        }

        private void btnSendToB_Click(object sender, EventArgs e)
        {
            SendToTrack(3);
        }

        private void SendToTrack(int trackid)
        {
            if (montage_timeline != null)
            {
                CMxpClip last_clip = montage_timeline.Tracks[trackid].LastClip;

                long tcin = 0;

                if (last_clip != null) tcin = last_clip.TimcodeIn + last_clip.ClippingOut + 1;

                currentOpened_source.Tag            = NewsMontange.Helpers.CProjectsHelper.enSouceType.video;
                currentDragging_source              = currentOpened_source;
                currentOpened_source.Clipping_in    = Timeline.CurrentMarkIn;
                currentOpened_source.Clipping_out   = Timeline.CurrentMarkOut;


                CMxpClip clipV = montage_timeline.Tracks[trackid].AddClip("",
                                                                enCMxpClipType.Video,
                                                                currentOpened_source,
                                                                null,
                                                                tcin - Timeline.CurrentMarkIn,
                                                                tcin - Timeline.CurrentMarkIn + currentOpened_source.Tcout,
                                                                Timeline.CurrentMarkIn,
                                                                Timeline.CurrentMarkOut);

                CMxpClip clipA = montage_timeline.Tracks[trackid + 1].AddClip("",
                                                                enCMxpClipType.Audio,
                                                                currentOpened_source,
                                                                null,
                                                                tcin - Timeline.CurrentMarkIn,
                                                                tcin - Timeline.CurrentMarkIn + currentOpened_source.Tcout,
                                                                Timeline.CurrentMarkIn,
                                                                Timeline.CurrentMarkOut);

                Program.Logger.info("Sending clip to track " + ((trackid == 1) ? "A" : "B"));
                Program.Logger.debug("  > Source            : " + Path.GetFileName((currentOpened_source.filename)));
                Program.Logger.debug("  > Clippin In        : " + Timeline.CurrentMarkIn);
                Program.Logger.debug("  > Clippin Out       : " + Timeline.CurrentMarkOut);
                Program.Logger.debug("  > Add to TC         : " + tcin + "(" + CNM_Tools.Frame2TC(tcin, Timeline.FPS) + ")");

                clipV.Group.Add(clipA);

                clipV.NotifyRender();
                clipA.NotifyRender();

                //Timeline.HideMarks();
            }
        }



        private void TimeCode_Position_OnTimecodeChanged(object Sender, long timecode)
        {
            if (Timeline.Enabled) Timeline.SetCurrentPositionAsExternal(timecode);
        }

        private void tc_markin_OnTimecodeChanged(object Sender, long timecode)
        {
            if (Timeline.Enabled)
            {
                Timeline.MarkIn(timecode);
            }
        }

        private void tc_markout_OnTimecodeChanged(object Sender, long timecode)
        {
            if (Timeline.Enabled)
            {
                Timeline.MarkOut(timecode);
            }
        }



        private void SRC_VIDEO_ParentChanged(object sender, EventArgs e)
        {
            if (this.Parent == null)
            {
                if (previewMonitor.Controls.Count > 0)
                {
                    previewMonitor.Controls[0].Parent = null;
                }
            }
            else
            {
                if (sourcePreview != null)
                {
                    previewMonitor.Controls.Add(sourcePreview);
                    sourcePreview.Play();
                    sourcePreview.Pause();
                }
            }
        }

        private void ListItems0_DrawItem(object sender, DrawItemEventArgs e)
        {

        }

        private void picPlayVirtualClip_Load(object sender, EventArgs e)
        {

        }
        
        private void pnlLine_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawLine(Pens.Silver, 0, 0, 0, e.ClipRectangle.Height - 1);
            e.Graphics.DrawLine(Pens.Silver, 0, e.ClipRectangle.Height - 1, e.ClipRectangle.Width - 1, e.ClipRectangle.Height - 1);
            e.Graphics.DrawLine(Pens.Silver, e.ClipRectangle.Width - 1, e.ClipRectangle.Height - 1, e.ClipRectangle.Width - 1, 0);

            base.OnPaint(e);
        }

        private void picVirtualClip_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {
            if (e.Action == DragAction.Drop)
            {
                montage_timeline.HideDragObject();
            }
        }

        private void removeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ListSources0.SelectedIndex >= 0)
            {
                CMxpTL_ClipInfo fClip = (CMxpTL_ClipInfo)ListSources0.SelectedItem;

                Program.Logger.info(" [REMOVING SOURCE CLIP] VIDEO : " + fClip.filename);

                int used_times = 0;

                foreach (CMxpClip clip in montage_timeline.Tracks.AllClips)
                {
                    if (clip.ClipInfo.filename.Equals(fClip.filename, StringComparison.OrdinalIgnoreCase))
                    {
                        if (clip.Type == enCMxpClipType.Video || clip.Group.Count == 0)
                        {
                            used_times++;
                        }
                    }
                }

                if (used_times > 0)
                {
                    Program.Logger.info("   > Clip user " + used_times + " times, operation aborted...");
                    MessageBox.Show("This clip is used " + used_times + " in current project, so is not removable...");
                }
                else
                {
                    projectHelper.RemoveSource(fClip.filename);
                    Program.Logger.info("   > Clip removed");
                }
            }
        }

        private void censorEditorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ListSources0.SelectedIndex >= 0)
            {
                CMxpTL_ClipInfo fClip = (CMxpTL_ClipInfo)ListSources0.SelectedItem;

                frmCensorEditor frm = new frmCensorEditor();

                frm.PreviewSource = fClip;

                frm.ShowDialog();

                if (File.Exists(fClip.filename + ".censor"))
                {
                    CCensorTimeline tl = new CCensorTimeline();
                    tl.LoadFile(fClip.filename + ".censor");
                    fClip.CensorInfo = tl;
                }
                else
                {
                    if (fClip.CensorInfo != null) fClip.CensorInfo = null;
                }
                //string xml = frm.CensorTimeline.Save();

                frm.Dispose();

                
            }
        }

        private void mnuGallery_Opening(object sender, CancelEventArgs e)
        {
            if (ListSources0.SelectedIndex >= 0)
            {
                CMxpTL_ClipInfo fClip = (CMxpTL_ClipInfo)ListSources0.SelectedItem;

                censorEditorToolStripMenuItem.Text = (fClip.CensorInfo != null)? "Modify Censor" : "Create Censor";
            }
        }

        private void SRC_VIDEO_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {
            switch (e.Action)
            {
                case DragAction.Cancel:
                    break;
                case DragAction.Continue:
                    break;
                case DragAction.Drop:
                    montage_timeline.HideDragObject();
                    break;
                default:
                    break;
            }
        }
    }

}
