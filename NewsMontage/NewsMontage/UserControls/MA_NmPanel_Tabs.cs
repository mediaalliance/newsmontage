﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Drawing.Drawing2D;
using System.ComponentModel.Design;


namespace NewsMontange
{
    [Designer("System.Windows.Forms.Design.ParentControlDesigner, System.Design", typeof(IDesigner))]
    public partial class MA_NmPanel_Tabs : Panel
    {
        // DELEGATES
        public delegate void NMTabs_OnChange(object Sender, int index, object Tag);

        // EVENTS
        public event NMTabs_OnChange OnChange = null;




        private GraphicsPath base_gp  = null;
        private string title = "";

        private Color borderColor           = Color.FromArgb(68, 68, 68);
        private Color borderFocusesColor    = Color.LightSalmon;
        private bool isFocused              = false;

        private int vertical_aperture       = 30;
        private int horizontal_aperture     = 350;

        private Color clear_color           = DefaultBackColor;

        private int selectedIndex           = 0;



        public MA_NmPanel_Tabs()
        {
            InitializeComponent();

            this.DoubleBuffered = true;
            this.AutoSize       = false;
        }

        public MA_NmPanel_Button AddButton(string title, Image icon, object Tag)
        {
            Bitmap bmp = new Bitmap(115, vertical_aperture - 3);
            Graphics g = Graphics.FromImage(bmp);
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;

            SizeF textMeasure = g.MeasureString(title, new Font(this.Font.FontFamily, 9, FontStyle.Bold));

            if (icon != null)
            {
                g.DrawImage(icon, 3, bmp.Height / 2 - icon.Height / 2 - 2);
            }

            g.DrawString(title,
                        new Font(this.Font.FontFamily, 9, FontStyle.Bold), 
                        Brushes.Silver, 
                        22 + ((bmp.Width - 22) / 2 - (textMeasure.Width / 2)), 
                        bmp.Height / 2 - (int)(textMeasure.Height / 2));            
            g.Dispose();


            int number = this.Controls.Count;
            int picwidth = bmp.Width;

            MA_NmPanel_Button btn = new MA_NmPanel_Button();
            btn.Name = number.ToString();
            btn.Tag = Tag;
            btn.ButtonPic = bmp;
            btn.Location = new Point(4 + (number * (picwidth + 4)), 2);
            btn.Width = picwidth;
            btn.Height = vertical_aperture - 2 - 1;

            btn.Click += new EventHandler(pic_Click);

            this.Controls.Add(btn);
            this.horizontal_aperture = btn.Right + 15;

            return btn;
        }

        public MA_NmPanel_Button AddButton(Image pic, object Tag)
        {
            int number              = this.Controls.Count;
            int picwidth            = 115;

            MA_NmPanel_Button btn   = new MA_NmPanel_Button();
            btn.Name                = number.ToString();
            btn.Tag                 = Tag;
            btn.ButtonPic           = pic;
            btn.Location            = new Point(4 + (number * (picwidth + 4)), 2);
            btn.Width               = picwidth;
            btn.Height              = vertical_aperture - 2 - 1;

            btn.Click += new EventHandler(pic_Click);

            this.Controls.Add(btn);
            this.horizontal_aperture = btn.Right + 15;

            return btn;
        }

        void pic_Click(object sender, EventArgs e)
        {
            MA_NmPanel_Button me = (MA_NmPanel_Button)sender;

            foreach (MA_NmPanel_Button btn in Controls)
            {
                btn.Selected = false;
            }

            selectedIndex = Convert.ToInt32(me.Name);

            if (OnChange != null) OnChange(this, selectedIndex, me.Tag);

            me.Selected = true;
        }



        protected override void OnParentChanged(EventArgs e)
        {
            if (Parent is MA_NmPanel)
            {
                this.borderColor            = ((MA_NmPanel)Parent).BorderColor;
                this.BackColor              = ((MA_NmPanel)Parent).BackColor;
                this.borderFocusesColor     = ((MA_NmPanel)Parent).BorderFocusesColor;


                if (((MA_NmPanel)Parent).Parent != null) this.clear_color = ((MA_NmPanel)Parent).Parent.BackColor;
            }

            Refresh();

            base.OnParentChanged(e);
        }

        protected override void OnVisibleChanged(EventArgs e)
        {
            if (Parent != null)
            {
                if (Parent.Parent != null)
                {
                    this.clear_color = Parent.Parent.BackColor;
                }

                if (Parent is MA_NmPanel)
                {
                    ((MA_NmPanel)Parent).OnFocusChanged += new MA_NmPanel.NMP_OnFocusChanged(MA_NmPanel_Tabs_OnFocusChanged);
                }
            }

            base.OnVisibleChanged(e);
        }

        void MA_NmPanel_Tabs_OnFocusChanged(object Sender, bool isFocused)
        {
            this.isFocused = isFocused;
            Refresh();
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            int aperture = horizontal_aperture;
            int height = vertical_aperture;

            if (Parent != null)
            {
                e.Graphics.Clear(clear_color);
            }

            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

            Color bColor = borderColor;
            if (isFocused) bColor = this.borderFocusesColor;

            GraphicsPath gp = new GraphicsPath();

            e.Graphics.FillPath(new SolidBrush(this.BackColor), base_gp);
            e.Graphics.DrawPath(new Pen(new SolidBrush(borderColor)), base_gp);


            gp.AddLine(new Point(0, Height), new Point(0, height + 4));
            gp.AddArc(new Rectangle(0, height, 8, 8), 180, 90);
            gp.AddLine(new Point(4, height), new Point(aperture, height));
            gp.AddArc(new Rectangle(aperture - 8, height - 16, 16, 16), 90, -45);
            gp.AddLine(gp.GetLastPoint(), new Point(aperture + height, 8));
            gp.AddArc(new Rectangle(aperture + height + 4, 0, 16, 16), 220, 50);

            if ((this.Dock & DockStyle.Top) == DockStyle.Top)
            {
                gp.AddLine(gp.GetLastPoint(), new Point(Width - 4, 0));
                gp.AddArc(new Rectangle(Width - 9, 0, 8, 8), 270, 90);
                gp.AddLine(gp.GetLastPoint(), new Point(Width - 1, Height + 1));
            }
            else
            {
                gp.AddLine(gp.GetLastPoint(), new Point(Width, 0)); 
            }

            e.Graphics.DrawPath(new Pen(new SolidBrush(bColor)), gp);
            gp.Dispose();

        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            this.Focus();
            base.OnMouseDown(e);
        }


        protected override void OnResize(EventArgs e)
        {
            if (base_gp != null) base_gp.Dispose();
            base_gp = DrawRoundRect(0, 0, Width - 1, Height - 1, 4);
            Refresh();
            base.OnResize(e);
        }



        #region PUBLIC PROPERTIES

        public Color BorderColor
        {
            get { return borderColor; }
            set
            {
                if (borderColor != value)
                {
                    borderColor = value;
                    Refresh();
                }
            }
        }

        public Color BorderFocusesColor
        {
            get { return borderFocusesColor; }
            set
            {
                if (borderFocusesColor != value)
                {
                    borderFocusesColor = value;
                    Refresh();
                }
            }
        }

        public string Title
        {
            get { return title; }
            set
            {
                if (title != value)
                {
                    title = value;
                    Refresh();
                }
            }
        }

        public bool IsFocused
        {
            get { return isFocused; }
            set
            {
                if (isFocused != value)
                {
                    isFocused = value;
                    Refresh();
                }
            }
        }

        public int Horizontal_aperture
        {
            get { return horizontal_aperture; }
            set
            {
                if (horizontal_aperture != value)
                {
                    horizontal_aperture = value;
                    Refresh();
                }
            }
        }

        public int Verical_aperture
        {
            get { return vertical_aperture; }
            set
            {
                if (vertical_aperture != value)
                {
                    vertical_aperture = value;
                    Refresh();
                }
            }
        }

        #endregion PUBLIC PROPERTIES




        private GraphicsPath DrawRoundRect(float X, float Y, float width, float height, float radius)
        {
            GraphicsPath gp = new GraphicsPath();

            gp.AddLine(X + width + 1, Y + height + 1, X, Y + height +1);
            gp.AddLine(X, Y + height + 1, X, Y + radius);
            gp.AddArc(X, Y, radius * 2, radius * 2, 180, 90);


            if ((this.Dock & DockStyle.Top) == DockStyle.Top)
            {
                gp.AddLine(X + radius, Y, X + width - (radius * 2), Y);
                gp.AddArc(X + width - (radius * 2), Y, radius * 2, radius * 2, 270, 90);
                gp.AddLine(X + width, Y + radius, X + width, Y + height + 1);
            }
            else
            {
                gp.AddLine(X + radius, Y, X + width + 1, Y);
            }
            //gp.AddArc(X + width - (radius * 2), Y + height - (radius * 2), radius * 2, radius * 2, 0, 90);
            //gp.AddLine(X + width - (radius * 2), Y + height, X + radius, Y + height);
            //gp.AddArc(X, Y + height - (radius * 2), radius * 2, radius * 2, 90, 90);
            //gp.CloseFigure();

            return gp;
        }

    }


    public class MA_NmPanel_Button : Panel
    {
        public new event EventHandler Click = null;

        private PictureBox pic              = null;
        private Color selectedButtonColor   = Color.FromArgb(147, 118, 58);
        private Color hoverButtonColor      = Color.Gray;
        private bool selected               = false;

        public MA_NmPanel_Button()
        {
            pic             = new PictureBox();
            pic.SizeMode    = PictureBoxSizeMode.CenterImage;
            pic.Dock        = DockStyle.Fill;

            pic.Click += new EventHandler(pic_Click);

            Controls.Add(pic);

            pic.MouseEnter += new EventHandler(pic_MouseEnter);
            pic.MouseLeave += new EventHandler(pic_MouseLeave);
        }

        void pic_Click(object sender, EventArgs e)
        {
            if (Click != null) Click(this, e);
        }



        void pic_MouseLeave(object sender, EventArgs e)
        {
            PictureBox me = (PictureBox)sender;
            me.BackColor = (selected)? selectedButtonColor : Color.Transparent;
        }

        void pic_MouseEnter(object sender, EventArgs e)
        {
            PictureBox me = (PictureBox)sender;
            me.BackColor = Color.Gray;
        }

        public bool Selected
        {
            get { return selected; }
            set
            {
                selected = value;

                if (value)
                {
                    pic.BackColor = selectedButtonColor;
                }
                else
                {
                    pic.BackColor = Color.Transparent;
                }
            }
        }


        public Image ButtonPic
        {
            get { return pic.Image; }
            set { pic.Image = value; }
        }
    }
}
