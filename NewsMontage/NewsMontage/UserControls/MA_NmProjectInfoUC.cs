﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using NewsMontange.Helpers;
using System.IO;
using NewsMontange.Language;

namespace NewsMontange.UserControls
{
    public partial class MA_NmProjectInfo: UserControl
    {
        // DELEGATES
        public delegate void PrjInfo_OnFullinfoClick(object Sender);

        // EVENTS
        public event PrjInfo_OnFullinfoClick OnFullinfoClick = null;


        private CProjectInfo projectInfo = null;



        public MA_NmProjectInfo()
        {
            InitializeComponent();
        }

        public void UpdateLanguage()
        {
            mA_NmPanel3.Title = CDict.GetValue("MainForm.ProjectInfo", mA_NmPanel3.Title);
            label1.Text = CDict.GetValue("MainForm.ProjectName", label1.Text);
            label2.Text = CDict.GetValue("MainForm.ProjectAuthor", label2.Text);
            label5.Text = CDict.GetValue("MainForm.ProjectDate", label5.Text);
            lblFullinfo.Text = CDict.GetValue("MainForm.FullInfo", lblFullinfo.Text);
        }

        private void lblFullinfo_Click(object sender, EventArgs e)
        {
            if (OnFullinfoClick != null) OnFullinfoClick(this);
        }

        public CProjectInfo ProjectInfo
        {
            get { return projectInfo; }
            set 
            {
                if (projectInfo != value)
                {
                    projectInfo = value;

                    lblProjName.Text        = Path.GetFileName(projectInfo.Filename);
                    lblAuthor.Text          = projectInfo.Project_Author;
                    lblCreationDate.Text    = projectInfo.CreationDate.ToString("dd/MM/yyyy HH.mm");
                }
            }
        }
    }
}
