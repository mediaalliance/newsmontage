﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO;
using System.Runtime.InteropServices;

using MediaAlliance.Controls.MxpTimeLine;
using MediaAlliance.Controls.MxpTimeLine.Objects;
using MediaAlliance.Controls.MxpTimeLine.Classes;
using NewsMontange.Helpers;
using MediaAlliance.AV;
using NewsMontange.Forms;
using NewsMontange.Classes;
using MediaAlliance.Controls.MxpTimeLine.Engine;
using Microsoft.Win32;
using NewsMontange.Language;


namespace NewsMontange.UserControls
{
    [System.Reflection.ObfuscationAttribute(Feature = "all", Exclude = true)]
    public partial class SRC_VOICE : UserControl
    {
        // FORMS

        private frmExternalText externalText = null;
        private frmPlayOnRec frmPlayOnRec = null;


        private CMxpTL_ClipInfo currentOpened_source    = null;
        private CMxpTL_ClipInfo currentDragging_source  = null;
        private Source sourcePreview                    = null;
        private Control previewMonitor                  = null;
        private CProjectsHelper projectHelper           = null;
        private CWaveFormMaker waveform_maker           = null;

        private MxpTmeLine montage_timeline             = null;

        private int audio_device_id                     = -1;

        private Point cliplist_downpos;
        private Point virtualClip_downpos;


        private DateTime m_recstarttime;
        private System.Timers.Timer tmrRecording = new System.Timers.Timer(40);

        public List<MA_TimeCodeUC> CurrentPosition_Controls = new List<MA_TimeCodeUC>();



        public SRC_VOICE()
        {
            InitializeComponent();

            removeToolStripMenuItem.Text = CDict.GetValue("MainForm.VoiceSource.RemoveGalleryItem", "Remove");
            copyToToolStripMenuItem.Text = CDict.GetValue("MainForm.VoiceSource.CopyToGalleryItem", "Copy To");
        }
        
        private void SRC_VOICE_Load(object sender, EventArgs e)
        {

            Timeline.InitializeInternalEngine();

            Timeline.Tracks.AddTrack("MAIN", enMxpTrackType.Audio, 85, null);
            Timeline.TCOUT = 25 * 60 * 1;
            Timeline.ZoomAll();


            richText.Cursor = Cursor_Beam();

            imgsSrcThumb.Images.Add(Properties.Resources.NoThumb);


            picOpen.Visible = Program.Settings.Options.CAN_IMPORT_VOICEFILE;
            lblOpen.Visible = Program.Settings.Options.CAN_IMPORT_VOICEFILE;

            try
            {
                AudioDevice.DeviceInfo[] DeviceList = AudioDevice.GetDevicesList();

                cmbAudioHwSource.Items.Clear();
                cmbAudioHwSource.Text = "None";

                foreach (AudioDevice.DeviceInfo info in DeviceList)
                {
                    Telerik.WinControls.RadItem item = new Telerik.WinControls.UI.RadMenuItem();
                    item.Text = info.Name;
                    item.Tag = info;
                    item.Click += new EventHandler(item_Click);
                    cmbAudioHwSource.Items.Add(item);
                }

            }
            catch 
            { 
            }

            tmrRecording.Elapsed += new System.Timers.ElapsedEventHandler(tmrRecording_Elapsed);

            //grid_source.Rows.Add(new Telerik.WinControls.UI.RadListDataItem("<html>text1<br>text2</html>"), "prova");


            if (File.Exists(Program.LanguageFile))
            {
                CDict.AddFromFile(Program.LanguageFile);
            }

            lblSources.Text = CDict.GetValue("MainForm.VoiceSource.GalleryTitle", lblSources.Text);
            label4.Text = CDict.GetValue("MainForm.VoiceSource.AudioSources", label4.Text);
            lblOpen.Text = CDict.GetValue("Generic.Button.Open", lblOpen.Text);
            label5.Text = CDict.GetValue("Generic.Button.PlayMark", label5.Text);
            lblZoomAllSrc.Text = CDict.GetValue("Generic.Button.ZoomAll", lblZoomAllSrc.Text);
            label6.Text = CDict.GetValue("MainForm.VoiceSource.ClipDrag", label6.Text);
            chkPlayOnRec.Text = CDict.GetValue("MainForm.VoiceSource.PlayOnRec", chkPlayOnRec.Text);
            label7.Text = CDict.GetValue("MainForm.VoiceSource.RecTime", label7.Text);
            label1.Text = CDict.GetValue("Generic.Text.Text", label1.Text);
            lblCurrentPosition.Text = CDict.GetValue("MainForm.VoiceSource.CurrentPosition", lblCurrentPosition.Text);
        }



        void tmrRecording_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            TimeSpan ts = DateTime.Now - m_recstarttime;

            this.Invoke((MethodInvoker)delegate
            {
                set_rec((long)ts.TotalMilliseconds / (1000 / (long)Timeline.FPS)); 
            });
        }

        void item_Click(object sender, EventArgs e)
        {
            Telerik.WinControls.UI.RadMenuItem me = (Telerik.WinControls.UI.RadMenuItem)sender;

            cmbAudioHwSource.Text = ((AudioDevice.DeviceInfo)me.Tag).Name;

            AudioDeviceID = ((AudioDevice.DeviceInfo)me.Tag).ID;
        }


        #region TIMELINE EVENTS

        private void Timeline_OnChangeMarker(object Sender, long FrameIn, long FrameOut, enTcChangeType change_type)
        {
            if (Timeline.IsMarked)
            {
                tc_markin.TimeCode          = Timeline.CurrentMarkIn;
                tc_markout.TimeCode         = Timeline.CurrentMarkOut;
                //tc_mark_duration.TimeCode   = Timeline.CurrentMarkOut - Timeline.CurrentMarkIn;
            }
            else
            {
                tc_markin.TimeCode = 0;
                tc_markout.TimeCode = 0;
            }

            //pnlMarkDuration.Visible   = Timeline.IsMarked;
            //pnlMarkOut.Visible        = Timeline.IsMarked;
            pnlMarkersTC.Visible        = Timeline.IsMarked;
            pnlMarkersTC.Visible        = Timeline.IsMarked;
            btnVirtualClip.Enabled      = Timeline.IsMarked;
            btnPlayMarker.Enabled       = Timeline.IsMarked;
            lblCurrentPosition.Visible  = !Timeline.IsMarked;
        }

        private void Timeline_OnChangePosition(object Sender, long position, bool Internal)
        {
            TimeCode_Position.TimeCode = position;

            foreach (MA_TimeCodeUC tc_ctl in CurrentPosition_Controls) tc_ctl.TimeCode = position;

            if (Internal)
            {
                if (Timeline.InternalEngine.EngineStatus == MediaAlliance.Controls.MxpTimeLine.Engine.InternalEngineStatus.play)
                {
                    Timeline.InternalEngine.Pause();
                }

                if ((Timeline.Actions & MxpTimelineAction.isZooming) != MxpTimelineAction.isZooming &&
                    (Timeline.Actions & MxpTimelineAction.isPanning) != MxpTimelineAction.isPanning)
                {
                    if (sourcePreview != null)
                    {
                        sourcePreview.Seek(position);
                        if (Program.Settings.Options.TIMELINE_AUDIOSCRUB)
                        {
                            sourcePreview.PlayScrub();
                        }
                    }
                }
            }
        }

        private void Timeline_OnZoomChanged(object Sender, long ZoomIn, long ZoomOut)
        {
            return;

            if(Timeline.Tracks[0].Clips.Count > 0)
            {
                CMxpClip audioclip = Timeline.Tracks[0].Clips[0];

                Bitmap wfb = waveform_maker.getWaveForm(CurrentOpened, Timeline.ZoomIn, Timeline.ZoomOut);
                
                Bitmap dest = new Bitmap(audioclip.DisplayInfo.VisibleWidth, Timeline.Tracks[0].Height);

                Graphics grf = Graphics.FromImage(dest);

                grf.DrawImage(wfb,
                                new Rectangle(0, 0, dest.Width, dest.Height),
                                new Rectangle(0, 0, wfb.Width, wfb.Height),
                                GraphicsUnit.Pixel);

                grf.Dispose();
                wfb.Dispose();

                Timeline.Tracks[0].Clips[0].BackgraoundImage = dest;


                //int precision = 3000;

                //wf.Draw_async(wfb, 
                //                new Pen(Brushes.Yellow, 1), 
                //                audioclip.DisplayInfo.FirstVisible_TC * 40, 
                //                audioclip.DisplayInfo.LastVisible_TC * 40, 
                //                precision, 
                //                false, 
                //                audioclip);
            }
        }

        private void Timeline_OnClipDisplayInfoChanged(object Sender, CMxpClip clip)
        {
            CMxpTL_ClipInfo clip_info = clip.ClipInfo;

            clip_info.Clipping_in = clip.ClippingIn;
            clip_info.Clipping_out = clip.ClippingOut;

            CWaveFormMaker.CWaveFormClip wave_info = waveform_maker[clip_info];

            if (clip.Virtual_Width == 0) return;

            if (wave_info != null)
            {
                Bitmap wfb = waveform_maker.getWaveForm(clip_info,
                                                        clip.DisplayInfo.FirstVisible_TC_OfClip,// clip.ClippingIn,
                                                        clip.DisplayInfo.LastVisible_TC_OfClip);// clip.ClippingOut);

                Bitmap dest = new Bitmap(clip.DisplayInfo.VisibleWidth, clip.ParentTrack.Height);

                //Console.WriteLine("clip.DisplayInfo.VisibleWidth : " + clip.DisplayInfo.VisibleWidth);

                Graphics grf = Graphics.FromImage(dest);
                grf.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                grf.DrawImage(wfb,
                                new Rectangle(0, 0, dest.Width, dest.Height),
                                new Rectangle(0, 0, wfb.Width, wfb.Height),
                                GraphicsUnit.Pixel);

                //grf.FillRectangle(Brushes.Black, new Rectangle(0, 0, 10, 10));
                //grf.FillRectangle(Brushes.Black, new Rectangle(dest.Width-10, 0, 10, 10));

                grf.Dispose();
                wfb.Dispose();

                clip.BackgraoundImage = dest;
            }
        }

        private void Timeline_OnInternalEngineEvent(object Sender, MediaAlliance.Controls.MxpTimeLine.Engine.InternEngineEventHandle e)
        {
            if (sourcePreview != null)
            {
                switch (e.Type)
                {
                    case MediaAlliance.Controls.MxpTimeLine.Engine.enIEngineEventType.onPlay:
                        if (montage_timeline != null)
                        {
                            if (montage_timeline.InternalEngine.EngineStatus == InternalEngineStatus.play)
                            {
                                // EVITO IL PLAY CONTEMPORANEO SULLE DUE TIMELINE
                                montage_timeline.InternalEngine.Pause();
                            }
                        }
                        sourcePreview.Play();
                        break;
                    case MediaAlliance.Controls.MxpTimeLine.Engine.enIEngineEventType.onPause:
                        sourcePreview.Pause();
                        break;
                }
            }
        }

        private void Timeline_OnExitedFromActions(object Sender, MxpTimelineAction Action, object objref)
        {
            if (Action == MxpTimelineAction.isScrubbing)
            {
                //sourcePreview.Pause();
            }
        }

        private void Timeline_OnRequestPlaymarked(object Sender)
        {
            if (sourcePreview != null)
            {
                System.Threading.Thread.Sleep(200);
                sourcePreview.PlayMarked(Timeline.CurrentMarkIn, Timeline.CurrentMarkOut);
                Timeline.InternalEngine.PlayMark();
            }
        }
        
        #endregion TIMELINE EVENTS

        
        #region SOURCE LIST EVENTS

        private void ListSources0_DoubleClick(object sender, EventArgs e)
        {
            if (ListSources0.SelectedItem != null)
            {
                CMxpTL_ClipInfo fClip = (CMxpTL_ClipInfo)ListSources0.SelectedItem;

                if (currentOpened_source != fClip)
                {
                    currentOpened_source = fClip;

                    CloseCurrentPreview();

                    if (!Program.Settings.Options.PREVIEW_ENGINE_DS_ONLY)
                    {
                        if (Path.GetExtension(fClip.filename).ToLower().Contains("mov") && !Program.Settings.Options.PREVIEW_ENGINE_DS_ONLY)
                        {
                            sourcePreview = new QTPreview(Program.Settings.Options.TIMELINE_AUDIOSCRUB);
                        }
                        else
                        {
                            sourcePreview = new DSPreview(Program.Settings.Options.TIMELINE_AUDIOSCRUB);
                            ((DSPreview)sourcePreview).OnChangeState += new DSPreview.DSPreview_OnChangeState(SRC_VOICE_OnChangeState);
                        }
                    }
                    else
                    {
                        sourcePreview = new DSPreview(Program.Settings.Options.TIMELINE_AUDIOSCRUB);
                        ((DSPreview)sourcePreview).OnChangeState += new DSPreview.DSPreview_OnChangeState(SRC_VOICE_OnChangeState);
                    }

                    sourcePreview.NewInstance(fClip.filename);
                    sourcePreview.Visible = true;
                    sourcePreview.Dock = DockStyle.Fill;

                    if (previewMonitor != null) previewMonitor.Controls.Add(sourcePreview);


                    LOAD_AUDIOFILE_MACRO(fClip);

                    Timeline.HideMarks();
                    Timeline.Enabled = true;
                }
            }
        }

        private void ListSources0_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                cliplist_downpos = e.Location;
            }
        }

        private void ListSources0_MouseMove(object sender, MouseEventArgs e)
        {
            if (ListSources0.SelectedItem != null)
            {
                CMxpTL_ClipInfo fClip = (CMxpTL_ClipInfo)ListSources0.SelectedItem;

                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    int xdiff = Math.Abs(e.Location.X - cliplist_downpos.X);
                    int ydiff = Math.Abs(e.Location.Y - cliplist_downpos.Y);

                    if (xdiff > 15 || ydiff > 15)
                    {
                        fClip.Clipping_in       = 0;
                        fClip.Clipping_out      = fClip.Tcout;
                        currentDragging_source  = fClip;
                        this.DoDragDrop(fClip, DragDropEffects.Copy);
                    }
                }
            }
        }

        #endregion SOURCE LIST EVENTS
        

        
        private void picVirtualClip_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                virtualClip_downpos = Cursor.Position;
            }
        }

        private void picVirtualClip_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                int xdiff = Math.Abs(Cursor.Position.X - virtualClip_downpos.X);
                int ydiff = Math.Abs(Cursor.Position.Y - virtualClip_downpos.Y);

                if (xdiff > 15 || ydiff > 15)
                {
                    currentOpened_source.Tag            = NewsMontange.Helpers.CProjectsHelper.enSouceType.voice;
                    currentDragging_source              = currentOpened_source;
                    currentOpened_source.Clipping_in    = Timeline.CurrentMarkIn;
                    currentOpened_source.Clipping_out   = Timeline.CurrentMarkOut;

                    btnVirtualClip.DoDragDrop(currentOpened_source, DragDropEffects.Copy);
                }
            }
        }

        

        #region PUBLIC PROPERTIES

        public Source SourcePreview
        {
            get { return sourcePreview; }
            set
            {
                sourcePreview = value;
            }
        }

        public Control PreviewMonitor
        {
            get { return previewMonitor; }
            set { previewMonitor = value; }
        }

        public CProjectsHelper ProjectHelper
        {
            get { return projectHelper; }
            set
            { 
                projectHelper = value;
            }
        }

        public CMxpTL_ClipInfo CurrentOpened
        {
            get { return currentOpened_source; }
        }

        public CMxpTL_ClipInfo CurrentDragging
        {
            get { return currentDragging_source; }
        }

        public CWaveFormMaker Waveform_maker
        {
            get { return waveform_maker; }
            set 
            { 
                waveform_maker = value;
                waveform_maker.OnRendered += new CWaveFormMaker.wfmk_OnRendered(waveform_maker_OnRendered);
            }
        }

        public MxpTmeLine Montage_timeline
        {
            get { return montage_timeline; }
            set
            {
                montage_timeline = value;
            }
        }

        public int AudioDeviceID
        {
            get { return audio_device_id; }
            set
            {
                audio_device_id = value;

                voiceRecorderUC1.StartUp(value);

                btnRec.Enabled = true;
            }
        }

        public string Script
        {
            get { return richText.Rtf; }
            set
            {
                richText.Rtf = value;
            }
        }

        #endregion PUBLIC PROPERTIES


        #region PUBLIC METHODS

        public void UnloadOpenedClips()
        {
            CloseCurrentPreview();
        }

        public void START_RECORDING()
        {
            btnRec.Text = "STOP";
            btnRec.BackColor = Color.Silver;

            string filename = "V_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".wav";
            voiceRecorderUC1.FileName = Path.Combine(projectHelper.CurrentProject.Folder_VoiceFiles, filename);
            voiceRecorderUC1.StartRecording();

            m_recstarttime = DateTime.Now;
            tc_rectime.TimeCode = 0;
            tmrRecording.Start();

            Program.MainForm.AddStatus(enApplicationStatus.recordingAudio);

            if (chkPlayOnRec.Checked)
            {
                montage_timeline.InternalEngine.Play();
            }
            chkPlayOnRec.Enabled = false;
        }

        public void STOP_RECORDING()
        {
            btnRec.Text = "REC";
            btnRec.BackColor = Color.FromArgb(255, 192, 192);
            btnRec.Enabled = false;

            tmrRecording.Stop();

            Timeline.Enabled = true;


            voiceRecorderUC1.StopRecording();

            if (chkPlayOnRec.Checked)
            {
                montage_timeline.InternalEngine.Pause();
            }
            chkPlayOnRec.Enabled = true;

            if (sourcePreview != null) sourcePreview.Dispose();

            Forms.frmRecDesciption frm = new Forms.frmRecDesciption();

            frm.ShowDialog();

            CMxpTL_ClipInfo clip_info = projectHelper.AddSource(voiceRecorderUC1.FileName, frm.Description, 1, CProjectsHelper.enSouceType.voice);

            System.Threading.Thread.Sleep(200);

            Program.MainForm.RemoveStatus(enApplicationStatus.recordingAudio);
            btnRec.Enabled = true;


            //if (Program.Settings.Options.PREVIEW_ENGINE_DS_ONLY)
            //{
            //    sourcePreview = new DSPreview(Program.Settings.Options.TIMELINE_AUDIOSCRUB);
            //}
            //else
            //{
            //    if (Path.GetExtension(voiceRecorderUC1.FileName).ToLower().Contains("mov") && !Program.Settings.Options.PREVIEW_ENGINE_DS_ONLY)
            //        sourcePreview = new QTPreview(Program.Settings.Options.TIMELINE_AUDIOSCRUB);
            //    else
            //        sourcePreview = new DSPreview(Program.Settings.Options.TIMELINE_AUDIOSCRUB);

            //}

            //sourcePreview.NewInstance(voiceRecorderUC1.FileName);
            //sourcePreview.Visible = true;
            //sourcePreview.Dock = DockStyle.Fill;

            //if (previewMonitor != null) previewMonitor.Controls.Add(sourcePreview);


            //currentDragging_source = clip_info;

            //CloseCurrentPreview();

            //LOAD_AUDIOFILE_MACRO(clip_info);

        }

        #endregion PUBLIC METHODS
        

        #region PRIVATE METHODS

        private void CloseCurrentPreview()
        {
            if (sourcePreview != null) sourcePreview.Close();

            if (Timeline.Tracks[0].Clips.Count > 0)
            {
                Timeline.Tracks[0].Clips[0].Delete();
                Timeline.TCOUT = (long)Timeline.FPS * 60 * 5;
                Timeline.ZoomAll();
                //Timeline.Enabled = false;
            }
        }

        private void UpdateWaveform()
        {
            if (CurrentOpened != null)
            {
                if (Timeline.Tracks.Count > 0 && Timeline.Tracks[0].Clips.Count > 0)
                {
                    CMxpClip audioclip = Timeline.Tracks[0].Clips[0];

                    Bitmap wfb = waveform_maker.getWaveForm(CurrentOpened, Timeline.ZoomIn, Timeline.ZoomOut);

                    Bitmap dest = new Bitmap(audioclip.DisplayInfo.VisibleWidth, Timeline.Tracks[0].Height);

                    Graphics grf = Graphics.FromImage(dest);

                    grf.DrawImage(wfb,
                                    new Rectangle(0, 0, dest.Width, dest.Height),
                                    new Rectangle(0, 0, wfb.Width, wfb.Height),
                                    GraphicsUnit.Pixel);

                    grf.Dispose();
                    wfb.Dispose();

                    Timeline.Tracks[0].Clips[0].BackgraoundImage = dest;
                }
            }
        }

        private void set_rec(long tc)
        {
            tc_rectime.TimeCode = tc;
        }
        
        private void LOAD_AUDIOFILE_MACRO(CMxpTL_ClipInfo clip_info)
        {
            // SETUP TIMELINE DURATION AND ZOOM
            Timeline.TCOUT      = clip_info.Tcout;
            Timeline.ZoomIn     = 0;
            Timeline.ZoomOut    = Timeline.TCOUT;

            // CREATING NEW CLIP ON TRACK
            CMxpClip voice_over_clip = Timeline.Tracks[0].AddClip("VOICE OVER", enCMxpClipType.Audio, clip_info, null, 0, clip_info.Tcout, 0, clip_info.Tcout);
            Timeline.Tracks[0].Clips[0].BackColor       = Color.DimGray;
            Timeline.Tracks[0].Clips[0].CanEditAudio    = false;

            UpdateWaveform();
        }

        #endregion PRIVATE METHODS
        

        #region VOICECONTROL EVENTS

        private void SRC_VOICE_GiveFeedback(object sender, GiveFeedbackEventArgs e)
        {

        }

        private void SRC_VOICE_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {
            if (e.Action == DragAction.Drop)
            {
                montage_timeline.HideDragObject();
                //Timeline.HideMarks();
                //tc_markin.TimeCode = 0;
                //tc_markout.TimeCode = 0;
            }
        }

        private void SRC_VOICE_ParentChanged(object sender, EventArgs e)
        {
            if (this.Parent == null)
            {
                if (previewMonitor.Controls.Count > 0)
                {
                    previewMonitor.Controls[0].Parent = null;
                }
            }
            else
            {
                if (sourcePreview != null)
                {
                    previewMonitor.Controls.Add(sourcePreview);
                    sourcePreview.Play();
                    sourcePreview.Pause();
                }
            }
        }

        #endregion VOICECONTROL EVENTS



        void waveform_maker_OnRendered(object Sender, CMxpTL_ClipInfo clip_info, CWaveFormMaker.CWaveFormClip wave_form, Bitmap bmp, int id)
        {
            UpdateWaveform();
        }


        
        private void picOpen_Click(object sender, EventArgs e)
        {
            openFilesource.Title = "Import an existing voice file";
            openFilesource.Filter = "WAV|*.wav";

            if (Program.Settings.Options.VOICE_FILTER.Length > 0)
            {
                openFilesource.Filter = "VOICE FILE|" + string.Join(";", Program.Settings.Options.VOICE_FILTER);
            }

            if (openFilesource.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string prjfile = Path.Combine(projectHelper.CurrentProject.Folder_VoiceFiles, Path.GetFileName(openFilesource.FileName));

                projectHelper.AddSource(openFilesource.FileName, "", 2, CProjectsHelper.enSouceType.voice);
            }
        }

        private void btnRec_Click(object sender, EventArgs e)
        {
            if (btnRec.Text == "REC")
            {
                START_RECORDING();
            }
            else
            {
                STOP_RECORDING();
            }
        }


        private void btnPlayMarker_Click(object sender, EventArgs e)
        {
            Timeline.Request_PlayMarked();
        }

        private void btnZoomAll_Click(object sender, EventArgs e)
        {
            if (Timeline.Enabled) Timeline.ZoomAll();
        }

        private void pnlLine_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawLine(Pens.Silver, 0, 0, 0, e.ClipRectangle.Height - 1);
            e.Graphics.DrawLine(Pens.Silver, 0, e.ClipRectangle.Height - 1, e.ClipRectangle.Width - 1, e.ClipRectangle.Height - 1);
            e.Graphics.DrawLine(Pens.Silver, e.ClipRectangle.Width - 1, e.ClipRectangle.Height - 1, e.ClipRectangle.Width - 1, 0);

            base.OnPaint(e);
        }

        private void richEnlarge_Click(object sender, EventArgs e)
        {
            if (externalText == null)
            {
                externalText = new frmExternalText();
                externalText.richText.Rtf = richText.Rtf;
                externalText.richText.Tag = richText;
                externalText.Show();
            }
            else
            {
                externalText.Show();
            }
        }


        private void tc_markin_OnTimecodeChanged(object Sender, long timecode)
        {
            if (Timeline.Enabled)
            {
                Timeline.MarkIn(timecode);
            }
        }

        private void tc_markout_OnTimecodeChanged(object Sender, long timecode)
        {
            if (Timeline.Enabled)
            {
                Timeline.MarkOut(timecode);
            }
        }

        public Cursor Cursor_Beam()
        {
            return CreateCursor(Properties.Resources.Cursor_NM_Beam, 16, 16);
        }

        [DllImport("user32.dll")]
        private static extern IntPtr CreateIconIndirect(ref IconInfo icon);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetIconInfo(IntPtr hIcon, ref IconInfo pIconInfo);

        private static Cursor CreateCursor(Bitmap bmp, int xHotSpot, int yHotSpot)
        {
            IconInfo tmp = new IconInfo();
            GetIconInfo(bmp.GetHicon(), ref tmp);
            tmp.xHotspot = xHotSpot;
            tmp.yHotspot = yHotSpot;
            tmp.fIcon = false;
            return new Cursor(CreateIconIndirect(ref tmp));
        }

        private void richText_TextChanged(object sender, EventArgs e)
        {
            projectHelper.CurrentProject.Script = richText.Rtf;

            if (externalText != null)
            {
                externalText.richText.Rtf = richText.Rtf;
            }
        }


        private void removeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ListSources0.SelectedIndex >= 0)
            {
                CMxpTL_ClipInfo fClip = (CMxpTL_ClipInfo)ListSources0.SelectedItem;

                Program.Logger.info(" [REMOVING SOURCE CLIP] VOICE : " + fClip.filename);

                int used_times = 0;

                foreach (CMxpClip clip in montage_timeline.Tracks.AllClips)
                {
                    if (clip.ClipInfo.filename.Equals(fClip.filename, StringComparison.OrdinalIgnoreCase))
                    {
                        used_times++;
                    }
                }

                if (used_times > 0)
                {
                    Program.Logger.info("   > Clip user " + used_times + " times, operation aborted...");
                    MessageBox.Show("This clip is used " + used_times + " in current project, so is not removable...");
                }
                else
                {
                    string file_path = new FileInfo(fClip.filename).Directory.FullName.TrimEnd('\\');

                    if (file_path.Equals(new DirectoryInfo(projectHelper.CurrentProject.Folder_VoiceFiles).FullName.TrimEnd('\\')))
                    {
                        Program.Logger.info("   > File is generated by NewsMontage, asking confirmation for delete");

                        if (MessageBox.Show(null,
                                            "This file was generated by NewsMontage so removing this clip will also delete file, are you sure?",
                                            "Attention",
                                            MessageBoxButtons.YesNo,
                                            MessageBoxIcon.Question,
                                            MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                        {
                            Program.Logger.info("   > Refused");
                            return;
                        }
                    }

                    Program.Logger.info("   > Operation confirmed");

                    try
                    {
                        File.Delete(fClip.filename);
                        Program.Logger.info("   > File deleted");
                    }
                    catch (Exception ex)
                    {
                        Program.Logger.error("   > File delete error : " + ex.Message);
                    }

                    projectHelper.RemoveSource(fClip.filename);
                    Program.Logger.info("   > Clip removed");
                }
            }
        }
        
        private void copyToToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ListSources0.SelectedIndex >= 0)
            {
                CMxpTL_ClipInfo fClip = (CMxpTL_ClipInfo)ListSources0.SelectedItem;

                Program.Logger.info(" [COPY TO CLIP] VOICE : " + fClip.filename);

                copyToDialog.FileName = Path.GetFileName(fClip.filename);
                copyToDialog.Filter = Path.GetExtension(fClip.filename) + "|*." + Path.GetExtension(fClip.filename);

                if (copyToDialog.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        File.Copy(fClip.filename, copyToDialog.FileName);
                        Program.Logger.info("   > File saved to : " + copyToDialog.FileName);
                        MessageBox.Show("File saved");
                    }
                    catch (Exception ex)
                    {
                        Program.Logger.error("   > Error saving file : " + copyToDialog.FileName);
                        Program.Logger.error("   > Error             : " + ex.Message);
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    Program.Logger.info("   > Action cancelled by user");
                }
            }
        }




        private void chkPlayOnRec_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPlayOnRec.Checked)
            {
                int val = 1;

                RegistryKey key = Registry.CurrentUser.CreateSubKey(@"Software\MediaAlliance\NewsMontage\Options");

                if (key != null)
                {
                    val = (int)key.GetValue("ShowPlayOnRecAlert", 1);
                }

                if (val > 0)
                {
                    frmPlayOnRec = new frmPlayOnRec();
                    frmPlayOnRec.Show();
                }
            }
            else
            {
                if (frmPlayOnRec != null)
                {
                    frmPlayOnRec.Close();
                    frmPlayOnRec = null;
                }
            }
        }

        private void TimeCode_Position_OnTimecodeChanged(object Sender, long timecode)
        {
            if (Timeline.Enabled) Timeline.SetCurrentPositionAsExternal(timecode);
        }
        
        void SRC_VOICE_OnChangeState(object Sender, DSPreview.MAPreviewState State)
        {
            switch (State)
            {
                case DSPreview.MAPreviewState.None:
                    break;
                case DSPreview.MAPreviewState.Playing:
                    break;
                case DSPreview.MAPreviewState.PlayingMarked:
                    break;
                case DSPreview.MAPreviewState.Stoped:
                    break;
                case DSPreview.MAPreviewState.Cued:
                    break;
                case DSPreview.MAPreviewState.Paused:
                    if (Timeline.InternalEngine.EngineStatus == InternalEngineStatus.play)
                    {
                        Timeline.InternalEngine.Pause();

                        if (Timeline.IsMarked)
                        {
                            // SE LO STATO E' PLAY E C'E' UNA MARCATURA VUOL DIRE CHE SONO IN PLAYMARK
                            // IL THREAD FA FINIRE A VOLTE IL CURSORE OLTRE LA MARCATURA, SOLO GRAFICAMENTE
                            // QUESTO CORREGGE IL DIFETTO
                            Timeline.CurrentPosition = Timeline.CurrentMarkOut;
                        }
                    }
                    break;
                case DSPreview.MAPreviewState.Finished:
                    break;
                default:
                    break;
            }
        }

        private void btnVirtualClip_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {
            if (e.Action == DragAction.Drop)
            {
                montage_timeline.HideDragObject();
            }
        }
    }

}
