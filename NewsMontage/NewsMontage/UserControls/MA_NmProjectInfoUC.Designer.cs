﻿namespace NewsMontange.UserControls
{
    partial class MA_NmProjectInfo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.lblCreationDate = new System.Windows.Forms.Label();
            this.lblAuthor = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblProjName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblFullinfo = new System.Windows.Forms.Label();
            this.mA_NmPanel3 = new NewsMontange.MA_NmPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.mA_NmPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblCreationDate, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblAuthor, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblProjName, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(8, 15);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(382, 40);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(13, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(180, 14);
            this.label5.TabIndex = 5;
            this.label5.Text = "Date";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCreationDate
            // 
            this.lblCreationDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCreationDate.ForeColor = System.Drawing.Color.LightSteelBlue;
            this.lblCreationDate.Location = new System.Drawing.Point(199, 26);
            this.lblCreationDate.Name = "lblCreationDate";
            this.lblCreationDate.Size = new System.Drawing.Size(180, 14);
            this.lblCreationDate.TabIndex = 4;
            this.lblCreationDate.Text = "xx/xx/xxxx xx.xx";
            this.lblCreationDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAuthor
            // 
            this.lblAuthor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAuthor.ForeColor = System.Drawing.Color.LightGray;
            this.lblAuthor.Location = new System.Drawing.Point(199, 13);
            this.lblAuthor.Name = "lblAuthor";
            this.lblAuthor.Size = new System.Drawing.Size(180, 13);
            this.lblAuthor.TabIndex = 3;
            this.lblAuthor.Text = "author";
            this.lblAuthor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(13, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(180, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Author";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblProjName
            // 
            this.lblProjName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblProjName.ForeColor = System.Drawing.Color.LightGray;
            this.lblProjName.Location = new System.Drawing.Point(199, 0);
            this.lblProjName.Name = "lblProjName";
            this.lblProjName.Size = new System.Drawing.Size(180, 13);
            this.lblProjName.TabIndex = 1;
            this.lblProjName.Text = "name.nmprj";
            this.lblProjName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(13, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Project name";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFullinfo
            // 
            this.lblFullinfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblFullinfo.Font = new System.Drawing.Font("Segoe UI", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFullinfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblFullinfo.Location = new System.Drawing.Point(8, 58);
            this.lblFullinfo.Name = "lblFullinfo";
            this.lblFullinfo.Size = new System.Drawing.Size(382, 14);
            this.lblFullinfo.TabIndex = 2;
            this.lblFullinfo.Text = "Full info >>";
            this.lblFullinfo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblFullinfo.Click += new System.EventHandler(this.lblFullinfo_Click);
            // 
            // mA_NmPanel3
            // 
            this.mA_NmPanel3.BackColor = System.Drawing.Color.Gray;
            this.mA_NmPanel3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.mA_NmPanel3.BorderFocusesColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(185)))), ((int)(((byte)(45)))));
            this.mA_NmPanel3.Controls.Add(this.tableLayoutPanel1);
            this.mA_NmPanel3.Controls.Add(this.lblFullinfo);
            this.mA_NmPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mA_NmPanel3.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mA_NmPanel3.ForeColor = System.Drawing.Color.DarkGray;
            this.mA_NmPanel3.IsFocused = false;
            this.mA_NmPanel3.Location = new System.Drawing.Point(0, 0);
            this.mA_NmPanel3.Name = "mA_NmPanel3";
            this.mA_NmPanel3.Padding = new System.Windows.Forms.Padding(8, 15, 8, 8);
            this.mA_NmPanel3.Size = new System.Drawing.Size(398, 80);
            this.mA_NmPanel3.TabIndex = 35;
            this.mA_NmPanel3.Title = "PROJECT INFO";
            // 
            // MA_NmProjectInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mA_NmPanel3);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "MA_NmProjectInfo";
            this.Size = new System.Drawing.Size(398, 80);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.mA_NmPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MA_NmPanel mA_NmPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblCreationDate;
        private System.Windows.Forms.Label lblAuthor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblProjName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblFullinfo;

    }
}
