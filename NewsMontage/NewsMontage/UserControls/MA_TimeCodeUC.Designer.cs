﻿namespace NewsMontange.UserControls
{
    partial class MA_TimeCodeUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mA_NmPanel1 = new NewsMontange.MA_NmPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.mA_NmPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mA_NmPanel1
            // 
            this.mA_NmPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.mA_NmPanel1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.mA_NmPanel1.BorderFocusesColor = System.Drawing.Color.LightSalmon;
            this.mA_NmPanel1.Controls.Add(this.panel1);
            this.mA_NmPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mA_NmPanel1.IsFocused = false;
            this.mA_NmPanel1.Location = new System.Drawing.Point(0, 0);
            this.mA_NmPanel1.Name = "mA_NmPanel1";
            this.mA_NmPanel1.Padding = new System.Windows.Forms.Padding(5, 1, 5, 1);
            this.mA_NmPanel1.Size = new System.Drawing.Size(223, 62);
            this.mA_NmPanel1.TabIndex = 0;
            this.mA_NmPanel1.Title = "";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.maskedTextBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(5, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(213, 60);
            this.panel1.TabIndex = 54;
            this.panel1.BackColorChanged += new System.EventHandler(this.panel1_BackColorChanged);
            // 
            // maskedTextBox1
            // 
            this.maskedTextBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.maskedTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.maskedTextBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.maskedTextBox1.Font = new System.Drawing.Font("LCD_PS", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maskedTextBox1.ForeColor = System.Drawing.SystemColors.ScrollBar;
            this.maskedTextBox1.HidePromptOnLeave = true;
            this.maskedTextBox1.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this.maskedTextBox1.Location = new System.Drawing.Point(0, 36);
            this.maskedTextBox1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 1);
            this.maskedTextBox1.Mask = "00:00:00:00";
            this.maskedTextBox1.Name = "maskedTextBox1";
            this.maskedTextBox1.ResetOnSpace = false;
            this.maskedTextBox1.Size = new System.Drawing.Size(213, 24);
            this.maskedTextBox1.TabIndex = 53;
            this.maskedTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.maskedTextBox1.Enter += new System.EventHandler(this.maskedTextBox1_Enter);
            this.maskedTextBox1.Leave += new System.EventHandler(this.maskedTextBox1_Leave);
            // 
            // MA_TimeCodeUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mA_NmPanel1);
            this.Name = "MA_TimeCodeUC";
            this.Size = new System.Drawing.Size(223, 62);
            this.mA_NmPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MA_NmPanel mA_NmPanel1;
        private System.Windows.Forms.MaskedTextBox maskedTextBox1;
        private System.Windows.Forms.Panel panel1;
    }
}
