﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Drawing.Drawing2D;
using System.ComponentModel.Design;


namespace NewsMontange
{
    [System.Reflection.ObfuscationAttribute(Feature = "all", Exclude = true)]
    [Designer("System.Windows.Forms.Design.ParentControlDesigner, System.Design", typeof(IDesigner))]
    public partial class MA_NmPanel : Panel
    {
        // DELEGATES
        public delegate void NMP_OnFocusChanged(object Sender, bool isFocused);

        // EVENTS
        public event NMP_OnFocusChanged OnFocusChanged = null;


        private GraphicsPath base_gp  = null;
        private string title = "";

        private Color borderColor           = Color.FromArgb(68, 68, 68);
        private Color borderFocusesColor    = Color.LightSalmon;
        private bool isFocused              = false;



        public MA_NmPanel()
        {
            InitializeComponent();

            this.SetStyle(ControlStyles.Selectable, true);

            this.DoubleBuffered = true;
            this.AutoSize       = false;
        }


            
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            if (Parent != null)
            {
                e.Graphics.Clear(Parent.BackColor);
            }

            Color bColor = borderColor;
            if (isFocused) bColor = this.borderFocusesColor;


            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            e.Graphics.FillPath(new SolidBrush(this.BackColor), base_gp);
            e.Graphics.DrawPath(new Pen(new SolidBrush(bColor)), base_gp);

            int x_title = 3;
            if (this.Padding.Left != 0) x_title = this.Padding.Left;


            e.Graphics.DrawString(this.title, Font, new SolidBrush(this.ForeColor), x_title, 2);

            //base.OnPaintBackground(e);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            this.Focus();
            base.OnMouseDown(e);
        }


        private void MA_NmPanel_Paint(object sender, PaintEventArgs e)
        {
        }

        protected override void OnResize(EventArgs e)
        {
            if (base_gp != null) base_gp.Dispose();
            base_gp = DrawRoundRect(0, 0, Width - 1, Height - 1, 4);
            Refresh();
            base.OnResize(e);
        }



        #region PUBLIC PROPERTIES

        public Color BorderColor
        {
            get { return borderColor; }
            set
            {
                if (borderColor != value)
                {
                    borderColor = value;
                    Refresh();
                }
            }
        }

        public Color BorderFocusesColor
        {
            get { return borderFocusesColor; }
            set
            {
                if (borderFocusesColor != value)
                {
                    borderFocusesColor = value;
                    Refresh();
                }
            }
        }

        public string Title
        {
            get { return title; }
            set
            {
                if (title != value)
                {
                    title = value;
                    Refresh();
                }
            }
        }

        public bool IsFocused
        {
            get { return isFocused; }
            set
            {
                if (isFocused != value)
                {
                    isFocused = value;
                    if (OnFocusChanged != null) OnFocusChanged(this, value);
                    Refresh();
                }
            }
        }

        #endregion PUBLIC PROPERTIES




        private GraphicsPath DrawRoundRect(float X, float Y, float width, float height, float radius)
        {
            GraphicsPath gp = new GraphicsPath();

            gp.AddLine(X + radius, Y, X + width - (radius * 2), Y);
            gp.AddArc(X + width - (radius * 2), Y, radius * 2, radius * 2, 270, 90);
            gp.AddLine(X + width, Y + radius, X + width, Y + height - (radius * 2));
            gp.AddArc(X + width - (radius * 2), Y + height - (radius * 2), radius * 2, radius * 2, 0, 90);
            gp.AddLine(X + width - (radius * 2), Y + height, X + radius, Y + height);
            gp.AddArc(X, Y + height - (radius * 2), radius * 2, radius * 2, 90, 90);
            gp.AddLine(X, Y + height - (radius * 2), X, Y + radius);
            gp.AddArc(X, Y, radius * 2, radius * 2, 180, 90);
            gp.CloseFigure();

            return gp;
        }

    }

}
