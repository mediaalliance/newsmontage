﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO;

using MediaAlliance.Controls.MxpTimeLine;
using MediaAlliance.Controls.MxpTimeLine.Objects;
using MediaAlliance.Controls.MxpTimeLine.Classes;
using NewsMontange.Classes;
using NewsMontange.Helpers;
using MediaAlliance.AV;
using MediaAlliance.Controls.MxpTimeLine.Engine;
using NewsMontange.Language;


namespace NewsMontange.UserControls
{
    [System.Reflection.ObfuscationAttribute(Feature = "all", Exclude = true)]
    public partial class SRC_BACKGROUND : UserControl
    {
        // DELEGATES 
        private delegate void SetRecTc(long tc);


        private CMxpTL_ClipInfo currentOpened_source    = null;
        private CMxpTL_ClipInfo currentDragging_source  = null;
        private Source sourcePreview                    = null;
        private Control previewMonitor                  = null;
        private CProjectsHelper projectHelper           = null;
        private CWaveFormMaker waveform_maker           = null;

        private MxpTmeLine montage_timeline             = null;

        private int audio_device_id                     = 0;

        private Point cliplist_downpos;
        private Point virtualClip_downpos;

        //private WaveFile wf = null;

        private System.Timers.Timer tmrRecording = new System.Timers.Timer(40);

        public List<MA_TimeCodeUC> CurrentPosition_Controls = new List<MA_TimeCodeUC>();



        public SRC_BACKGROUND()
        {
            InitializeComponent();

            removeToolStripMenuItem.Text = CDict.GetValue("MainForm.BackgroundSource.RemoveGalleryItem", "Remove");

            Timeline.InitializeInternalEngine();

            Timeline.Tracks.AddTrack("MAIN", enMxpTrackType.Audio, 85, null);
            Timeline.TCOUT = 25 * 60 * 1;
            Timeline.ZoomAll();


            imgsSrcThumb.Images.Add(Properties.Resources.NoThumb);

        }

        private void SRC_BACKGROUND_Load(object sender, EventArgs e)
        {

            if (File.Exists(Program.LanguageFile))
            {
                CDict.AddFromFile(Program.LanguageFile);
            }

            lblSources.Text = CDict.GetValue("MainForm.BackgroundSource.GalleryTitle", lblSources.Text);
            label6.Text = CDict.GetValue("MainForm.BackgroundSource.ClipDrag", label6.Text);
            label4.Text = CDict.GetValue("Generic.Button.Open", label4.Text);
            label5.Text = CDict.GetValue("Generic.Button.PlayMark", label5.Text);
            lblZoomAllSrc.Text = CDict.GetValue("Generic.Button.ZoomAll", lblZoomAllSrc.Text);
            lblCurrentPosition.Text = CDict.GetValue("MainForm.BackgroundSource.CurrentPosition", lblCurrentPosition.Text);
        }





        #region TIMELINE EVENTS

        private void Timeline_OnChangeMarker(object Sender, long FrameIn, long FrameOut, enTcChangeType change_type)
        {
            if (Timeline.IsMarked)
            {
                tc_markin.TimeCode          = Timeline.CurrentMarkIn;
                tc_markout.TimeCode         = Timeline.CurrentMarkOut;
                //tc_mark_duration.TimeCode   = Timeline.CurrentMarkOut - Timeline.CurrentMarkIn;
            }
            else
            {
                tc_markin.TimeCode      = 0;
                tc_markout.TimeCode     = 0;
            }

            pnlMarkersTC.Visible        = Timeline.IsMarked;
            btnVirtualClip.Enabled      = Timeline.IsMarked;
            btnPlayMarker.Enabled       = Timeline.IsMarked;
            lblCurrentPosition.Visible  = !Timeline.IsMarked;
        }

        private void Timeline_OnChangePosition(object Sender, long position, bool Internal)
        {
            TimeCode_Position.TimeCode = position;

            foreach (MA_TimeCodeUC tc_ctl in CurrentPosition_Controls) tc_ctl.TimeCode = position;

            if (Internal)
            {
                if (Timeline.InternalEngine.EngineStatus == MediaAlliance.Controls.MxpTimeLine.Engine.InternalEngineStatus.play)
                {
                    Timeline.InternalEngine.Pause();
                }

                if ((Timeline.Actions & MxpTimelineAction.isZooming) != MxpTimelineAction.isZooming &&
                    (Timeline.Actions & MxpTimelineAction.isPanning) != MxpTimelineAction.isPanning)
                {
                    if (sourcePreview != null)
                    {
                        if (Program.Settings.Options.TIMELINE_AUDIOSCRUB)
                        {
                            sourcePreview.PlayScrub();
                        }
                        sourcePreview.Seek(position);
                    }
                }
            }
        }

        private void Timeline_OnZoomChanged(object Sender, long ZoomIn, long ZoomOut)
        {
            if (Timeline.Tracks[0].Clips.Count > 0)
            {
                CMxpClip audioclip = Timeline.Tracks[0].Clips[0];

                Bitmap wfb = waveform_maker.getWaveForm(CurrentOpened, Timeline.ZoomIn, Timeline.ZoomOut);

                Bitmap dest = new Bitmap(audioclip.DisplayInfo.VisibleWidth, Timeline.Tracks[0].Height);

                Graphics grf = Graphics.FromImage(dest);

                grf.DrawImage(wfb,
                                new Rectangle(0, 0, dest.Width, dest.Height),
                                new Rectangle(0, 0, wfb.Width, wfb.Height),
                                GraphicsUnit.Pixel);

                grf.Dispose();
                wfb.Dispose();

                Timeline.Tracks[0].Clips[0].BackgraoundImage = dest;
            }
        }

        private void Timeline_OnInternalEngineEvent(object Sender, MediaAlliance.Controls.MxpTimeLine.Engine.InternEngineEventHandle e)
        {
            if (sourcePreview != null)
            {
                switch (e.Type)
                {
                    case MediaAlliance.Controls.MxpTimeLine.Engine.enIEngineEventType.onPlay:
                        if (montage_timeline != null)
                        {
                            if (montage_timeline.InternalEngine.EngineStatus == InternalEngineStatus.play)
                            {
                                // EVITO IL PLAY CONTEMPORANEO SULLE DUE TIMELINE
                                montage_timeline.InternalEngine.Pause();
                            }
                        }
                        sourcePreview.Play();
                        break;
                    case MediaAlliance.Controls.MxpTimeLine.Engine.enIEngineEventType.onPause:
                        sourcePreview.Pause();
                        break;
                }
            }
        }

        private void Timeline_OnExitedFromActions(object Sender, MxpTimelineAction Action, object objref)
        {
            if (Action == MxpTimelineAction.isScrubbing)
            {
                //sourcePreview.Pause();
            }
        }

        private void Timeline_OnRequestPlaymarked(object Sender)
        {
            if (sourcePreview != null)
            {
                System.Threading.Thread.Sleep(200);
                sourcePreview.PlayMarked(Timeline.CurrentMarkIn, Timeline.CurrentMarkOut);
                Timeline.InternalEngine.PlayMark();
            }
        }
        
        #endregion TIMELINE EVENTS



        #region SOURCE LIST EVENTS

        private void ListSources0_DoubleClick(object sender, EventArgs e)
        {
            if (ListSources0.SelectedItem != null)
            {
                Timeline.Enabled = true;

                CMxpTL_ClipInfo fClip = (CMxpTL_ClipInfo)ListSources0.SelectedItem;

                if (currentOpened_source != fClip)
                {
                    currentOpened_source = fClip;

                    CloseCurrentPreview();

                    if (!Program.Settings.Options.PREVIEW_ENGINE_DS_ONLY)
                    {
                        if (Path.GetExtension(fClip.filename).ToLower().Contains("mov") && !Program.Settings.Options.PREVIEW_ENGINE_DS_ONLY)
                        {
                            sourcePreview = new QTPreview(Program.Settings.Options.TIMELINE_AUDIOSCRUB);
                        }
                        else
                        {
                            sourcePreview = new DSPreview(Program.Settings.Options.TIMELINE_AUDIOSCRUB);
                            ((DSPreview)sourcePreview).OnChangeState += new DSPreview.DSPreview_OnChangeState(SRC_BACKGROUND_OnChangeState);
                        }
                    }
                    else
                    {
                        sourcePreview = new DSPreview(Program.Settings.Options.TIMELINE_AUDIOSCRUB);
                        ((DSPreview)sourcePreview).OnChangeState += new DSPreview.DSPreview_OnChangeState(SRC_BACKGROUND_OnChangeState);
                    }


                    sourcePreview.NewInstance(fClip.filename);
                    sourcePreview.Visible = true;
                    sourcePreview.Dock = DockStyle.Fill;

                    if (previewMonitor != null) previewMonitor.Controls.Add(sourcePreview);


                    LOAD_AUDIOFILE_MACRO(fClip);

                    Timeline.HideMarks();
                }
            }
        }

        private void ListSources0_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                cliplist_downpos = e.Location;
            }
        }

        private void ListSources0_MouseMove(object sender, MouseEventArgs e)
        {
            if (ListSources0.SelectedItem != null)
            {
                CMxpTL_ClipInfo fClip = (CMxpTL_ClipInfo)ListSources0.SelectedItem;

                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    int xdiff = Math.Abs(e.Location.X - cliplist_downpos.X);
                    int ydiff = Math.Abs(e.Location.Y - cliplist_downpos.Y);

                    if (xdiff > 15 || ydiff > 15)
                    {
                        fClip.Clipping_in = 0;
                        fClip.Clipping_out = fClip.Tcout;
                        currentDragging_source = fClip;
                        this.DoDragDrop(fClip, DragDropEffects.Copy);
                    }
                }
            }
        }

        #endregion SOURCE LIST EVENTS

        

        private void picVirtualClip_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                virtualClip_downpos = Cursor.Position;
            }
        }

        private void picVirtualClip_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                int xdiff = Math.Abs(Cursor.Position.X - virtualClip_downpos.X);
                int ydiff = Math.Abs(Cursor.Position.Y - virtualClip_downpos.Y);

                if (xdiff > 15 || ydiff > 15)
                {
                    currentOpened_source.Tag            = NewsMontange.Helpers.CProjectsHelper.enSouceType.background;
                    currentDragging_source              = currentOpened_source;
                    currentOpened_source.Clipping_in    = Timeline.CurrentMarkIn;
                    currentOpened_source.Clipping_out   = Timeline.CurrentMarkOut;

                    btnVirtualClip.DoDragDrop(currentOpened_source, DragDropEffects.Copy);
                }
            }
        }

        

        #region PUBLIC PROPERTIES

        public Source SourcePreview
        {
            get { return sourcePreview; }
            set
            {
                sourcePreview = value;
            }
        }

        public Control PreviewMonitor
        {
            get { return previewMonitor; }
            set { previewMonitor = value; }
        }

        public CProjectsHelper ProjectHelper
        {
            get { return projectHelper; }
            set
            { 
                projectHelper = value;
            }
        }

        public CMxpTL_ClipInfo CurrentOpened
        {
            get { return currentOpened_source; }
        }

        public CMxpTL_ClipInfo CurrentDragging
        {
            get { return currentDragging_source; }
        }
        
        public CWaveFormMaker Waveform_maker
        {
            get { return waveform_maker; }
            set
            {
                waveform_maker = value;
                waveform_maker.OnRendered += new CWaveFormMaker.wfmk_OnRendered(waveform_maker_OnRendered);
            }
        }

        public MxpTmeLine Montage_timeline
        {
            get { return montage_timeline; }
            set
            {
                montage_timeline = value;
            }
        }

        #endregion PUBLIC PROPERTIES



        #region PUBLIC METHODS

        public void UnloadOpenedClips()
        {
            CloseCurrentPreview();
        }

        #endregion PUBLIC METHODS



        #region PRIVATE METHODS
        
        private void CloseCurrentPreview()
        {
            if (sourcePreview != null) sourcePreview.Close();

            if (Timeline.Tracks[0].Clips.Count > 0)
            {
                Timeline.Tracks[0].Clips[0].Delete();
                Timeline.TCOUT = (long)Timeline.FPS * 60 * 5;
                Timeline.ZoomAll();
                //Timeline.Enabled = false;
            }
        }

        private void LOAD_AUDIOFILE_MACRO(CMxpTL_ClipInfo clip_info)
        {
            // SETUP TIMELINE DURATION AND ZOOM
            Timeline.TCOUT = clip_info.Tcout;
            Timeline.ZoomIn = 0;
            Timeline.ZoomOut = Timeline.TCOUT;

            // CREATING NEW CLIP ON TRACK
            CMxpClip voice_over_clip = Timeline.Tracks[0].AddClip("VOICE OVER", enCMxpClipType.Audio, clip_info, null, 0, clip_info.Tcout, 0, clip_info.Tcout);
            Timeline.Tracks[0].Clips[0].BackColor       = Color.DimGray;
            Timeline.Tracks[0].Clips[0].CanEditAudio    = false;

            UpdateWaveform();
        }

        #endregion PRIVATE METHODS



        #region CONTROL EVENTS

        private void SRC_BACKGROUND_GiveFeedback(object sender, GiveFeedbackEventArgs e)
        {

        }

        private void SRC_BACKGROUND_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {
            if (e.Action == DragAction.Drop)
            {
                montage_timeline.HideDragObject();
                //Timeline.HideMarks();
                //tc_markin.TimeCode = 0;
                //tc_markout.TimeCode = 0;
            }
        }

        private void SRC_BACKGROUND_ParentChanged(object sender, EventArgs e)
        {
            if (this.Parent == null)
            {
                if (previewMonitor.Controls.Count > 0)
                {
                    previewMonitor.Controls[0].Parent = null;
                }
            }
            else
            {
                if (sourcePreview != null)
                {
                    previewMonitor.Controls.Add(sourcePreview);
                    sourcePreview.Play();
                    sourcePreview.Pause();
                }
            }
        }

        #endregion CONTROL EVENTS



        private void UpdateWaveform()
        {
            if (CurrentOpened != null)
            {
                if (Timeline.Tracks.Count > 0 && Timeline.Tracks[0].Clips.Count > 0)
                {
                    CMxpClip audioclip = Timeline.Tracks[0].Clips[0];

                    Bitmap wfb = waveform_maker.getWaveForm(CurrentOpened, Timeline.ZoomIn, Timeline.ZoomOut);

                    Bitmap dest = new Bitmap(audioclip.DisplayInfo.VisibleWidth, Timeline.Tracks[0].Height);

                    Graphics grf = Graphics.FromImage(dest);

                    grf.DrawImage(wfb,
                                    new Rectangle(0, 0, dest.Width, dest.Height),
                                    new Rectangle(0, 0, wfb.Width, wfb.Height),
                                    GraphicsUnit.Pixel);

                    grf.Dispose();
                    wfb.Dispose();

                    Timeline.Tracks[0].Clips[0].BackgraoundImage = dest;
                }
            }
        }

        void waveform_maker_OnRendered(object Sender, CMxpTL_ClipInfo clip_info, CWaveFormMaker.CWaveFormClip wave_form, Bitmap bmp, int id)
        {
            UpdateWaveform();
        }



        private void picOpen_Click(object sender, EventArgs e)
        {
            openFilesource.Title = "Import an existing audio file";
            openFilesource.Filter = "AUDIO FILE|*.wav";

            if (Program.Settings.Options.BACKGROUND_FILTER.Length > 0)
            {
                openFilesource.Filter = "AUDIO FILE|" + string.Join(";", Program.Settings.Options.BACKGROUND_FILTER);
            }


            if (openFilesource.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string prjfile = Path.Combine(projectHelper.CurrentProject.Folder_VoiceFiles, Path.GetFileName(openFilesource.FileName));
                projectHelper.AddSource(openFilesource.FileName, "", 2, CProjectsHelper.enSouceType.background);
            }
        }


        void voice_over_clip_OnBeforeRender(object Sender, Graphics graph, int width, int height, long tcin, long tcout)
        {
            return;
            Bitmap wfb = new Bitmap(width, height);


            CMxpClip clip = (CMxpClip)Sender;
            long zoomin     = Timeline.ZoomIn;
            long zoomout    = Timeline.ZoomOut;
            int img_x = clip.X;
            int img_w = clip.X + clip.Width;

            Console.WriteLine("CLIP X : " + clip.X);
            //if (img_x < 0)
            //{
            //    img_x = 0;
            //}
            //if (img_w > mxptl.Width)
            //{
            //    img_w = mxptl.Width - img_x;
            //}

            long start_tc   = Timeline.GetTimecodeAt(img_x);
            long end_tc     = Timeline.GetTimecodeAt(img_x + img_w);

            //start_tc        = start_tc - clip.TimcodeIn;
            //end_tc          = end_tc - clip.TimcodeIn;

            //Console.WriteLine("IN > " + CMxpTlGrid.Frame2TC(start_tc) + " - OUT > " + CMxpTlGrid.Frame2TC(end_tc));


            int precision = 3000;
            //if (mxptl.IsOnAction(MxpTimelineAction.isPanning)) precision = 2000;

            //wf.Draw_async(wfb, new Pen(Brushes.Yellow, 1), start_tc * 40, end_tc * 40, precision, false, clip);
        }

        private void btnZoomAll_Load(object sender, EventArgs e)
        {
            if (Timeline.Enabled) Timeline.ZoomAll();
        }

        private void btnPlayMarker_Click(object sender, EventArgs e)
        {
            Timeline.Request_PlayMarked();
        }


        private void pnlLine_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawLine(Pens.Silver, 0, 0, 0, e.ClipRectangle.Height - 1);
            e.Graphics.DrawLine(Pens.Silver, 0, e.ClipRectangle.Height - 1, e.ClipRectangle.Width - 1, e.ClipRectangle.Height - 1);
            e.Graphics.DrawLine(Pens.Silver, e.ClipRectangle.Width - 1, e.ClipRectangle.Height - 1, e.ClipRectangle.Width - 1, 0);

            base.OnPaint(e);
        }

        private void tc_markin_OnTimecodeChanged(object Sender, long timecode)
        {
            if (Timeline.Enabled)
            {
                Timeline.MarkIn(timecode);
            }
        }

        private void tc_markout_OnTimecodeChanged(object Sender, long timecode)
        {
            if (Timeline.Enabled)
            {
                Timeline.MarkOut(timecode);
            }
        }

        private void removeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ListSources0.SelectedIndex >= 0)
            {
                CMxpTL_ClipInfo fClip = (CMxpTL_ClipInfo)ListSources0.SelectedItem;

                Program.Logger.info(" [REMOVING SOURCE CLIP] BACKGROUND : " + fClip.filename);

                int used_times = 0;

                foreach (CMxpClip clip in montage_timeline.Tracks.AllClips)
                {
                    if (clip.ClipInfo.filename.Equals(fClip.filename, StringComparison.OrdinalIgnoreCase))
                    {
                        used_times++;
                    }
                }

                if (used_times > 0)
                {
                    Program.Logger.info("   > Clip user " + used_times + " times, operation aborted...");
                    MessageBox.Show("This clip is used " + used_times + " in current project, so is not removable...");
                }
                else
                {
                    projectHelper.RemoveSource(fClip.filename);
                    Program.Logger.info("   > Clip removed");
                }
            }
        }

        private void TimeCode_Position_OnTimecodeChanged(object Sender, long timecode)
        {
            if(Timeline.Enabled) Timeline.SetCurrentPositionAsExternal(timecode);
        }
        
        void SRC_BACKGROUND_OnChangeState(object Sender, DSPreview.MAPreviewState State)
        {
            switch (State)
            {
                case DSPreview.MAPreviewState.None:
                    break;
                case DSPreview.MAPreviewState.Playing:
                    break;
                case DSPreview.MAPreviewState.PlayingMarked:
                    break;
                case DSPreview.MAPreviewState.Stoped:
                    break;
                case DSPreview.MAPreviewState.Cued:
                    break;
                case DSPreview.MAPreviewState.Paused:
                    if (Timeline.InternalEngine.EngineStatus == InternalEngineStatus.play)
                    {
                        Timeline.InternalEngine.Pause();

                        if (Timeline.IsMarked)
                        {
                            // SE LO STATO E' PLAY E C'E' UNA MARCATURA VUOL DIRE CHE SONO IN PLAYMARK
                            // IL THREAD FA FINIRE A VOLTE IL CURSORE OLTRE LA MARCATURA, SOLO GRAFICAMENTE
                            // QUESTO CORREGGE IL DIFETTO
                            Timeline.CurrentPosition = Timeline.CurrentMarkOut;
                        }
                    }
                    break;
                case DSPreview.MAPreviewState.Finished:
                    break;
                default:
                    break;
            }
        }

        private void btnVirtualClip_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {
            if (e.Action == DragAction.Drop)
            {
                montage_timeline.HideDragObject();
            }
        }
    }

}
