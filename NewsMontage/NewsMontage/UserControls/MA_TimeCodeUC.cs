﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NewsMontange.UserControls
{
    public partial class MA_TimeCodeUC : UserControl
    {
        private Color backColor_Selected = Color.DimGray;

        public delegate void MATC_OnTimecodeChanged(object Sender, long timecode);

        public event MATC_OnTimecodeChanged OnTimecodeChanged = null;


        private long timecode   = 0;
        private float fps       = 25;



        public MA_TimeCodeUC()
        {
            InitializeComponent();

            maskedTextBox1.Text = Frame2TC(0, fps);
        }



        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Up:
                    {
                        this.timecode++;
                        maskedTextBox1.Text = Frame2TC(timecode, fps);
                        if (OnTimecodeChanged != null) OnTimecodeChanged(this, timecode);
                        return true;
                    }
                case Keys.Down:
                    {
                        if (timecode > 0)
                        {
                            this.timecode--;
                            maskedTextBox1.Text = Frame2TC(timecode, fps);
                            if (OnTimecodeChanged != null) OnTimecodeChanged(this, timecode);
                        }
                        return true;
                    }
                case Keys.Enter:
                    {
                        timecode = TC2Frame(maskedTextBox1.Text, fps);
                        if (OnTimecodeChanged != null) OnTimecodeChanged(this, timecode);
                        Parent.Focus();
                        return true;
                    }
                case Keys.Right:
                    {
                        if (maskedTextBox1.SelectionStart < maskedTextBox1.Text.Length - 1)
                        {
                            maskedTextBox1.Select(maskedTextBox1.SelectionStart + 1, 0);
                        }
                        return true;
                    }
                case Keys.Left:
                    {
                        if (maskedTextBox1.SelectionStart > 0)
                        {
                            maskedTextBox1.Select(maskedTextBox1.SelectionStart - 1, 0);
                        }
                        return true;
                    }
                default:
                    {
                        return base.ProcessCmdKey(ref msg, keyData);
                    }
            }
        }
        
        protected override void OnFontChanged(EventArgs e)
        {
            maskedTextBox1.Font = this.Font;
            base.OnFontChanged(e);
        }

        protected override void OnForeColorChanged(EventArgs e)
        {
            maskedTextBox1.ForeColor = this.ForeColor;
            base.OnForeColorChanged(e);
        }




        #region PUBLIC PROPERTIES

        public long TimeCode
        {
            get { return TC2Frame(maskedTextBox1.Text, fps); }
            set
            {
                timecode = value;
                maskedTextBox1.Text = Frame2TC(value, fps);
            }
        }

        public Color BorderColor
        {
            get { return mA_NmPanel1.BorderColor; }
            set
            {
                mA_NmPanel1.BorderColor = value;
            }
        }

        public Color BorderFocusesColor
        {
            get { return mA_NmPanel1.BorderFocusesColor; }
            set
            {
                mA_NmPanel1.BorderFocusesColor = value;
            }
        }

        public Color BackColor_Selected
        {
            get { return backColor_Selected; }
            set { backColor_Selected = value; Refresh(); }
        }

        public float FPS
        {
            get { return fps; }
            set { fps = value; }
        }

        #endregion PUBLIC PROPERTIES



        private void maskedTextBox1_Enter(object sender, EventArgs e)
        {
            panel1.BackColor = backColor_Selected;
            base.OnGotFocus(e);
        }

        private void maskedTextBox1_Leave(object sender, EventArgs e)
        {
            panel1.BackColor = mA_NmPanel1.BackColor;
            base.OnLostFocus(e);
        }

        private void panel1_BackColorChanged(object sender, EventArgs e)
        {
            maskedTextBox1.BackColor = panel1.BackColor;
            maskedTextBox1.Refresh();
        }





        #region STATIC FUNCTIONS

        public static string Frame2TC(long frame, float fps)
        {
            string result   = "";
            long ffTot      = frame;

            int ss      = (int)(frame / fps);
            int ff      = (int)(frame - (ss * fps));
            int mm      = (int)(ss / 60);
            ss          = ss - mm * 60;
            int hh      = (int)(mm / 60);
            mm          = mm - hh * 60;

            result += hh.ToString().PadLeft(2, '0') + ":";
            result += mm.ToString().PadLeft(2, '0') + ":";
            result += ss.ToString().PadLeft(2, '0') + ":";
            result += ff.ToString().PadLeft(2, '0');

            return result;
        }

        public static Int32 TC2Frame(string tc, float fps)
        {
            string[] tci = tc.Split(':', '.', ',', '-', '_');

            Int32 res = 0;

            for (int i = tci.Length - 1; i >= 0; i--)
            {
                int cur = 0;
                Int32.TryParse(tci[i], out cur);

                if (i == 0) res += (int)(cur * 60 * 60 * fps);
                if (i == 1) res += (int)(cur * 60 * fps);
                if (i == 2) res += (int)(cur * fps);
                if (i == 3) res += cur;
            }

            return res;
        }

        #endregion STATIC FUNCTIONS
    }
}
