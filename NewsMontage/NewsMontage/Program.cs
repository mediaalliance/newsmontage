﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using NewsMontange.Classes;
using MediaAlliance.Logger;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using NewsMontange.Language;
using System.Threading;
using MediaAlliance.ProductKey;


namespace NewsMontange
{
    static class Program
    {
        public static frmMain MainForm          = null;
        public static CSettings Settings        = null;
        public static ILogger Logger            = null;
        public static enVideoStandards Standard = enVideoStandards.custom;
        public static string LanguageFile       = "";

        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (File.Exists("firstrun.dat"))
            {
                File.Delete("firstrun.dat");
                Process.Start("NMSettingsEditor.exe");
                Environment.Exit(1);
            }



            #region CHECK ONLY ONE INSTANCE

            bool Ok;

            //System.Threading.Mutex m = new System.Threading.Mutex(true, "NewsMontage", out Ok);

            Process[] pProc = Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName);

            if (pProc.Length > 1)
            {
                MessageBox.Show("News Montage is already open");
                Environment.Exit(1);
            }

            Mutex appSingleton = new System.Threading.Mutex(false, "NewsMontage");
            if (!appSingleton.WaitOne(0, false))

            //if (!Ok)
            {
                MessageBox.Show("News Montage is already open");
                Environment.Exit(1);
            }

            #endregion CHECK ONLY ONE INSTANCE



            // INITIALIZE LOG

            if (!Directory.Exists("Log")) Directory.CreateDirectory("Log");
            
            Logger = CLoggerManager.instance().logInstance();

            FileVersionInfo fv = FileVersionInfo.GetVersionInfo(Application.ExecutablePath);
            
            Logger.info("===========================================================================");
            Logger.info(" NEWSMONTAGE NEW SESSION STARTED");
            Logger.info("   Software ver.   : " + Application.ProductVersion + " build " + fv.ProductBuildPart);
            Logger.info("   Company name    : " + Application.CompanyName);
            Logger.info("   Software path   : " + Application.ExecutablePath);
            Logger.info("   MACHINE NAME    : " + Environment.MachineName);
            Logger.info("   DOMAIN NAME     : " + Environment.UserDomainName);
            Logger.info("   USER NAME       : " + Environment.UserName);
            Logger.info("   IS DEBUG VER    : " + fv.IsDebug);
            Logger.info("===========================================================================");

            Logger.info("");
            Logger.info(" Assembly loaded : ");
            Logger.info("");


            AppDomain MyDomain = AppDomain.CurrentDomain;

            Assembly[] AssembliesLoaded = MyDomain.GetAssemblies();

            foreach (Assembly MyAssembly in AssembliesLoaded)
            {
                Logger.info(" > " + MyAssembly.GetName().Name + " - [" + MyAssembly.GetName().Version + "]");
            }

            Logger.info("");


            // INITIALIZE SETTINGS CLASS MANAGER




            try
            {

                Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                try
                {
                    MainForm = new frmMain(args);
                }
                catch (System.Exception ex)
                {
                    Logger.error(" Error on running frmMain");
                    Logger.error(ex);
                }

                #region SETTINGS LOAD AND LOG

                Settings = new CSettings();

                Settings.OnLoaded                       += MainForm.Settings_OnLoaded;
                Settings.OnSetupFormRequested           += MainForm.Settings_OnSetupFormRequested;
                Settings.RENDER_Options.OnChangeProfile += MainForm.ChangedRenderProfile;


                Settings.Load();

                Logger.info("===========================================================================");
                Logger.info("");
                Logger.info(" Settings Loaded : ");
                Logger.info("");

                foreach (FieldInfo fInfo in Settings.GetType().GetFields())
                {
                    object subclass = fInfo.GetValue(Settings);

                    Logger.info("Settings." + fInfo.Name + " : [" + subclass.ToString() + "]");

                    foreach (FieldInfo fSubInfo in subclass.GetType().GetFields())
                    {
                        Logger.info(("   > " + fSubInfo.Name).PadRight(30, ' ') + " : " + fSubInfo.GetValue(subclass).ToString());
                    }
                }

                Logger.info("===========================================================================");
                Logger.info("");

                #endregion SETTINGS LOAD AND LOG


                //Application.Run(MainForm);

                Application.Run(LicenseHelpers.GetContext<frmMain>(MainForm, Application.ProductName, 0, MainForm.Formlicense_language));
                //Application.Run(new test());
            }
            catch (Exception ex)
            {
                Logger.error(">>> Unexpected error");
                Logger.error(ex, true);
            }

            Logger.info("");
            Logger.info("SESSION CLOSED");
            Logger.info("===========================================================================");
            Logger.info("===========================================================================");
        }

        static void RENDER_Options_OnChangeProfile(object Sender, string profile)
        {
            throw new NotImplementedException();
        }


        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            Logger.error(">>> Unexpected Thread Exception");
            Logger.error(e.Exception, true);
        }
    }
}