﻿using MediaAlliance.Controls;
namespace NewsMontange
{
    partial class frmMain
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Liberare le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.mnuClip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.eliminaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clipInfoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.autoFadeAudioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.pnlClipSplit = new System.Windows.Forms.Panel();
            this.btnClipCut2 = new MediaAlliance.Controls.MAButton();
            this.pnlClipRemover = new System.Windows.Forms.Panel();
            this.btnClipRemover = new MediaAlliance.Controls.MAButton();
            this.pnlClipBlanker = new System.Windows.Forms.Panel();
            this.btnBlank = new MediaAlliance.Controls.MAButton();
            this.pnlBtnStepBW = new System.Windows.Forms.Panel();
            this.btnStepBW = new MediaAlliance.Controls.MAButton();
            this.pnlBtnStepFW = new System.Windows.Forms.Panel();
            this.btnStepFW = new MediaAlliance.Controls.MAButton();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnZoomFit = new MediaAlliance.Controls.MAButton();
            this.pnlZoomAll = new System.Windows.Forms.Panel();
            this.btnZAll2 = new MediaAlliance.Controls.MAButton();
            this.lblProjectStandard = new System.Windows.Forms.Label();
            this.spOutprvOutTL = new System.Windows.Forms.SplitContainer();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ProjectInfoUC = new NewsMontange.UserControls.MA_NmProjectInfo();
            this.pnlPreviewMain = new System.Windows.Forms.Panel();
            this.mA_NmPanel1 = new NewsMontange.MA_NmPanel();
            this.lblOnlineTrack = new System.Windows.Forms.Label();
            this.lblDuration = new NewsMontange.UserControls.MA_TimeCodeUC();
            this.lblCurrentTC0 = new NewsMontange.UserControls.MA_TimeCodeUC();
            this.pnlMainButtons = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnFrameFFW = new MediaAlliance.Controls.MAButton();
            this.btnPlay = new MediaAlliance.Controls.MAButton();
            this.btnPause = new MediaAlliance.Controls.MAButton();
            this.btnFrameREW = new MediaAlliance.Controls.MAButton();
            this.lblCurrentClip = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.picPreview1 = new System.Windows.Forms.Panel();
            this.mA_NmPanel2 = new NewsMontange.MA_NmPanel();
            this.mxptl = new MediaAlliance.Controls.MxpTimeLine.MxpTmeLine();
            this.mnuTimeline = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.markInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.markOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlTrackInfo = new System.Windows.Forms.Panel();
            this.pnlTLMain_Buttons = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startupMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rollbackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mediaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mediaAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportAudioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.preferenceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.internalLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.clipInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.projectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recoverBlankOnStartToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.activationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmAppAlert = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFieldsAlert = new System.Windows.Forms.ToolStripMenuItem();
            this.spMontageSources = new System.Windows.Forms.SplitContainer();
            this.spSrcprvSrcTL = new System.Windows.Forms.SplitContainer();
            this.mA_NmPanel3 = new NewsMontange.MA_NmPanel();
            this.lblCurrentTC_SRC = new NewsMontange.UserControls.MA_TimeCodeUC();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_FrameFFwSrc = new MediaAlliance.Controls.MAButton();
            this.btn_PlaySrc = new MediaAlliance.Controls.MAButton();
            this.btn_PauseSrc = new MediaAlliance.Controls.MAButton();
            this.btn_FrameREWSrc = new MediaAlliance.Controls.MAButton();
            this.preview_monitor_src = new System.Windows.Forms.Panel();
            this.mA_NmPanel4 = new NewsMontange.MA_NmPanel();
            this.source_tab = new NewsMontange.MA_NmPanel_Tabs();
            this.lblGeneratingWaves = new System.Windows.Forms.Panel();
            this.openFilesource = new System.Windows.Forms.OpenFileDialog();
            this.pnlPrjInfo = new System.Windows.Forms.Panel();
            this.pnlMenuContainer = new System.Windows.Forms.Panel();
            this.tmrProjectDurationUpdater = new System.Windows.Forms.Timer(this.components);
            this.pnlStatus = new System.Windows.Forms.Panel();
            this.lblAutosave = new System.Windows.Forms.Label();
            this.pnlWaveWorking = new System.Windows.Forms.Panel();
            this.picWorking = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ttip = new System.Windows.Forms.ToolTip(this.components);
            this.mnuClip.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.pnlClipSplit.SuspendLayout();
            this.pnlClipRemover.SuspendLayout();
            this.pnlClipBlanker.SuspendLayout();
            this.pnlBtnStepBW.SuspendLayout();
            this.pnlBtnStepFW.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlZoomAll.SuspendLayout();
            this.spOutprvOutTL.Panel1.SuspendLayout();
            this.spOutprvOutTL.Panel2.SuspendLayout();
            this.spOutprvOutTL.SuspendLayout();
            this.panel3.SuspendLayout();
            this.pnlPreviewMain.SuspendLayout();
            this.mA_NmPanel1.SuspendLayout();
            this.pnlMainButtons.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.mA_NmPanel2.SuspendLayout();
            this.mnuTimeline.SuspendLayout();
            this.pnlTLMain_Buttons.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.spMontageSources.Panel1.SuspendLayout();
            this.spMontageSources.Panel2.SuspendLayout();
            this.spMontageSources.SuspendLayout();
            this.spSrcprvSrcTL.Panel1.SuspendLayout();
            this.spSrcprvSrcTL.Panel2.SuspendLayout();
            this.spSrcprvSrcTL.SuspendLayout();
            this.mA_NmPanel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.mA_NmPanel4.SuspendLayout();
            this.pnlPrjInfo.SuspendLayout();
            this.pnlMenuContainer.SuspendLayout();
            this.pnlStatus.SuspendLayout();
            this.pnlWaveWorking.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picWorking)).BeginInit();
            this.SuspendLayout();
            // 
            // mnuClip
            // 
            this.mnuClip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eliminaToolStripMenuItem,
            this.clipInfoToolStripMenuItem1,
            this.autoFadeAudioToolStripMenuItem});
            this.mnuClip.Name = "mnuClip";
            this.mnuClip.Size = new System.Drawing.Size(160, 70);
            this.mnuClip.Opening += new System.ComponentModel.CancelEventHandler(this.mnuClip_Opening);
            // 
            // eliminaToolStripMenuItem
            // 
            this.eliminaToolStripMenuItem.Image = global::NewsMontange.Properties.Resources.mnu_delete_icon;
            this.eliminaToolStripMenuItem.Name = "eliminaToolStripMenuItem";
            this.eliminaToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.eliminaToolStripMenuItem.Text = "Delete";
            this.eliminaToolStripMenuItem.Click += new System.EventHandler(this.eliminaToolStripMenuItem_Click);
            // 
            // clipInfoToolStripMenuItem1
            // 
            this.clipInfoToolStripMenuItem1.Image = global::NewsMontange.Properties.Resources.mnu_clipinfo_icon;
            this.clipInfoToolStripMenuItem1.Name = "clipInfoToolStripMenuItem1";
            this.clipInfoToolStripMenuItem1.Size = new System.Drawing.Size(159, 22);
            this.clipInfoToolStripMenuItem1.Text = "Clip info";
            this.clipInfoToolStripMenuItem1.Click += new System.EventHandler(this.clipInfoToolStripMenuItem1_Click);
            // 
            // autoFadeAudioToolStripMenuItem
            // 
            this.autoFadeAudioToolStripMenuItem.Name = "autoFadeAudioToolStripMenuItem";
            this.autoFadeAudioToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.autoFadeAudioToolStripMenuItem.Text = "Auto fade audio";
            this.autoFadeAudioToolStripMenuItem.Click += new System.EventHandler(this.autoFadeAudioToolStripMenuItem_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 6;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.Controls.Add(this.pnlClipSplit, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.pnlClipRemover, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.pnlClipBlanker, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.pnlBtnStepBW, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.pnlBtnStepFW, 5, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.Size = new System.Drawing.Size(275, 40);
            this.tableLayoutPanel3.TabIndex = 54;
            // 
            // pnlClipSplit
            // 
            this.pnlClipSplit.Controls.Add(this.btnClipCut2);
            this.pnlClipSplit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlClipSplit.Location = new System.Drawing.Point(0, 0);
            this.pnlClipSplit.Margin = new System.Windows.Forms.Padding(0);
            this.pnlClipSplit.Name = "pnlClipSplit";
            this.pnlClipSplit.Size = new System.Drawing.Size(45, 48);
            this.pnlClipSplit.TabIndex = 55;
            // 
            // btnClipCut2
            // 
            this.btnClipCut2.AutoSize = true;
            this.btnClipCut2.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.btnClipCut2.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.automatic;
            this.btnClipCut2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClipCut2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnClipCut2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnClipCut2.Image_Click = null;
            this.btnClipCut2.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnClipCut2.Image_Hover")));
            this.btnClipCut2.Image_Normal = null;
            this.btnClipCut2.Image_Type = MediaAlliance.Controls.NmButtonIcon.Razor;
            this.btnClipCut2.InternalPadding = new System.Windows.Forms.Padding(0);
            this.btnClipCut2.Location = new System.Drawing.Point(0, 0);
            this.btnClipCut2.MAMenu = null;
            this.btnClipCut2.Margin = new System.Windows.Forms.Padding(87, 572, 87, 572);
            this.btnClipCut2.Menu = null;
            this.btnClipCut2.Name = "btnClipCut2";
            this.btnClipCut2.RGB_Matrix = new float[] {
        1F,
        1F,
        1F};
            this.btnClipCut2.Shortcut = System.Windows.Forms.Keys.None;
            this.btnClipCut2.ShortCutFixPosition = new System.Drawing.Point(0, 0);
            this.btnClipCut2.ShortcutTextColor = System.Drawing.Color.Silver;
            this.btnClipCut2.Size = new System.Drawing.Size(42, 42);
            this.btnClipCut2.Style = MediaAlliance.Controls.NmButtonStyle.Button;
            this.btnClipCut2.TabIndex = 53;
            this.btnClipCut2.Title = "";
            this.btnClipCut2.TitleAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.btnClipCut2.TitleVisibile = false;
            this.btnClipCut2.Tooltip_Text = "Tooltip text";
            this.btnClipCut2.UseInternalToolTip = true;
            this.btnClipCut2.Click += new System.EventHandler(this.picSplit_Click);
            // 
            // pnlClipRemover
            // 
            this.pnlClipRemover.Controls.Add(this.btnClipRemover);
            this.pnlClipRemover.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlClipRemover.Location = new System.Drawing.Point(45, 0);
            this.pnlClipRemover.Margin = new System.Windows.Forms.Padding(0);
            this.pnlClipRemover.Name = "pnlClipRemover";
            this.pnlClipRemover.Size = new System.Drawing.Size(45, 48);
            this.pnlClipRemover.TabIndex = 56;
            // 
            // btnClipRemover
            // 
            this.btnClipRemover.AutoSize = true;
            this.btnClipRemover.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.btnClipRemover.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.automatic;
            this.btnClipRemover.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClipRemover.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnClipRemover.Enabled = false;
            this.btnClipRemover.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnClipRemover.Image_Click = null;
            this.btnClipRemover.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnClipRemover.Image_Hover")));
            this.btnClipRemover.Image_Normal = null;
            this.btnClipRemover.Image_Type = MediaAlliance.Controls.NmButtonIcon.ClipEraser;
            this.btnClipRemover.InternalPadding = new System.Windows.Forms.Padding(0);
            this.btnClipRemover.Location = new System.Drawing.Point(0, 0);
            this.btnClipRemover.MAMenu = null;
            this.btnClipRemover.Margin = new System.Windows.Forms.Padding(87, 572, 87, 572);
            this.btnClipRemover.Menu = null;
            this.btnClipRemover.Name = "btnClipRemover";
            this.btnClipRemover.RGB_Matrix = new float[] {
        1F,
        1F,
        1F};
            this.btnClipRemover.Shortcut = System.Windows.Forms.Keys.None;
            this.btnClipRemover.ShortCutFixPosition = new System.Drawing.Point(0, 0);
            this.btnClipRemover.ShortcutTextColor = System.Drawing.Color.Silver;
            this.btnClipRemover.Size = new System.Drawing.Size(42, 42);
            this.btnClipRemover.Style = MediaAlliance.Controls.NmButtonStyle.Button;
            this.btnClipRemover.TabIndex = 53;
            this.btnClipRemover.Title = "";
            this.btnClipRemover.TitleAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.btnClipRemover.TitleVisibile = false;
            this.btnClipRemover.Tooltip_Text = "Tooltip text";
            this.btnClipRemover.UseInternalToolTip = true;
            this.btnClipRemover.Click += new System.EventHandler(this.btnClipRemover_Click);
            // 
            // pnlClipBlanker
            // 
            this.pnlClipBlanker.Controls.Add(this.btnBlank);
            this.pnlClipBlanker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlClipBlanker.Location = new System.Drawing.Point(90, 0);
            this.pnlClipBlanker.Margin = new System.Windows.Forms.Padding(0);
            this.pnlClipBlanker.Name = "pnlClipBlanker";
            this.pnlClipBlanker.Size = new System.Drawing.Size(45, 48);
            this.pnlClipBlanker.TabIndex = 57;
            // 
            // btnBlank
            // 
            this.btnBlank.AutoSize = true;
            this.btnBlank.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.btnBlank.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.automatic;
            this.btnBlank.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBlank.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnBlank.Enabled = false;
            this.btnBlank.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnBlank.Image_Click = null;
            this.btnBlank.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnBlank.Image_Hover")));
            this.btnBlank.Image_Normal = null;
            this.btnBlank.Image_Type = MediaAlliance.Controls.NmButtonIcon.ClipBlanker;
            this.btnBlank.InternalPadding = new System.Windows.Forms.Padding(0);
            this.btnBlank.Location = new System.Drawing.Point(0, 0);
            this.btnBlank.MAMenu = null;
            this.btnBlank.Margin = new System.Windows.Forms.Padding(87, 572, 87, 572);
            this.btnBlank.Menu = null;
            this.btnBlank.Name = "btnBlank";
            this.btnBlank.RGB_Matrix = new float[] {
        1F,
        1F,
        1F};
            this.btnBlank.Shortcut = System.Windows.Forms.Keys.None;
            this.btnBlank.ShortCutFixPosition = new System.Drawing.Point(0, 0);
            this.btnBlank.ShortcutTextColor = System.Drawing.Color.Silver;
            this.btnBlank.Size = new System.Drawing.Size(42, 42);
            this.btnBlank.Style = MediaAlliance.Controls.NmButtonStyle.Button;
            this.btnBlank.TabIndex = 53;
            this.btnBlank.Title = "";
            this.btnBlank.TitleAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBlank.TitleVisibile = false;
            this.btnBlank.Tooltip_Text = "Tooltip text";
            this.btnBlank.UseInternalToolTip = true;
            this.btnBlank.Click += new System.EventHandler(this.btnBlank_Click);
            // 
            // pnlBtnStepBW
            // 
            this.pnlBtnStepBW.Controls.Add(this.btnStepBW);
            this.pnlBtnStepBW.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBtnStepBW.Location = new System.Drawing.Point(180, 0);
            this.pnlBtnStepBW.Margin = new System.Windows.Forms.Padding(0);
            this.pnlBtnStepBW.Name = "pnlBtnStepBW";
            this.pnlBtnStepBW.Size = new System.Drawing.Size(45, 48);
            this.pnlBtnStepBW.TabIndex = 58;
            // 
            // btnStepBW
            // 
            this.btnStepBW.AutoSize = true;
            this.btnStepBW.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.btnStepBW.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.automatic;
            this.btnStepBW.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStepBW.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnStepBW.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnStepBW.Image_Click = null;
            this.btnStepBW.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnStepBW.Image_Hover")));
            this.btnStepBW.Image_Normal = global::NewsMontange.Properties.Resources.BTN_StepBack;
            this.btnStepBW.Image_Type = MediaAlliance.Controls.NmButtonIcon.Custom;
            this.btnStepBW.InternalPadding = new System.Windows.Forms.Padding(0);
            this.btnStepBW.Location = new System.Drawing.Point(0, 0);
            this.btnStepBW.MAMenu = null;
            this.btnStepBW.Margin = new System.Windows.Forms.Padding(87, 572, 87, 572);
            this.btnStepBW.Menu = null;
            this.btnStepBW.Name = "btnStepBW";
            this.btnStepBW.RGB_Matrix = new float[] {
        1F,
        1F,
        1F};
            this.btnStepBW.Shortcut = System.Windows.Forms.Keys.None;
            this.btnStepBW.ShortCutFixPosition = new System.Drawing.Point(0, 0);
            this.btnStepBW.ShortcutTextColor = System.Drawing.Color.Silver;
            this.btnStepBW.Size = new System.Drawing.Size(42, 42);
            this.btnStepBW.Style = MediaAlliance.Controls.NmButtonStyle.Button;
            this.btnStepBW.TabIndex = 53;
            this.btnStepBW.Title = "";
            this.btnStepBW.TitleAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.btnStepBW.TitleVisibile = false;
            this.btnStepBW.Tooltip_Text = "Tooltip text";
            this.btnStepBW.UseInternalToolTip = true;
            this.btnStepBW.Click += new System.EventHandler(this.btnStepBW_Click);
            // 
            // pnlBtnStepFW
            // 
            this.pnlBtnStepFW.Controls.Add(this.btnStepFW);
            this.pnlBtnStepFW.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBtnStepFW.Location = new System.Drawing.Point(225, 0);
            this.pnlBtnStepFW.Margin = new System.Windows.Forms.Padding(0);
            this.pnlBtnStepFW.Name = "pnlBtnStepFW";
            this.pnlBtnStepFW.Size = new System.Drawing.Size(50, 48);
            this.pnlBtnStepFW.TabIndex = 59;
            // 
            // btnStepFW
            // 
            this.btnStepFW.AutoSize = true;
            this.btnStepFW.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.btnStepFW.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.automatic;
            this.btnStepFW.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStepFW.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnStepFW.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnStepFW.Image_Click = null;
            this.btnStepFW.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnStepFW.Image_Hover")));
            this.btnStepFW.Image_Normal = global::NewsMontange.Properties.Resources.BTN_StepForward;
            this.btnStepFW.Image_Type = MediaAlliance.Controls.NmButtonIcon.Custom;
            this.btnStepFW.InternalPadding = new System.Windows.Forms.Padding(0);
            this.btnStepFW.Location = new System.Drawing.Point(0, 0);
            this.btnStepFW.MAMenu = null;
            this.btnStepFW.Margin = new System.Windows.Forms.Padding(87, 572, 87, 572);
            this.btnStepFW.Menu = null;
            this.btnStepFW.Name = "btnStepFW";
            this.btnStepFW.RGB_Matrix = new float[] {
        1F,
        1F,
        1F};
            this.btnStepFW.Shortcut = System.Windows.Forms.Keys.None;
            this.btnStepFW.ShortCutFixPosition = new System.Drawing.Point(0, 0);
            this.btnStepFW.ShortcutTextColor = System.Drawing.Color.Silver;
            this.btnStepFW.Size = new System.Drawing.Size(42, 42);
            this.btnStepFW.Style = MediaAlliance.Controls.NmButtonStyle.Button;
            this.btnStepFW.TabIndex = 53;
            this.btnStepFW.Title = "";
            this.btnStepFW.TitleAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.btnStepFW.TitleVisibile = false;
            this.btnStepFW.Tooltip_Text = "Tooltip text";
            this.btnStepFW.UseInternalToolTip = true;
            this.btnStepFW.Click += new System.EventHandler(this.btnStepFW_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.pnlZoomAll, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(780, 0);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(122, 42);
            this.tableLayoutPanel4.TabIndex = 55;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnZoomFit);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(51, 42);
            this.panel2.TabIndex = 1;
            // 
            // btnZoomFit
            // 
            this.btnZoomFit.AutoSize = true;
            this.btnZoomFit.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.btnZoomFit.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.automatic;
            this.btnZoomFit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnZoomFit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnZoomFit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnZoomFit.Image_Click = null;
            this.btnZoomFit.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnZoomFit.Image_Hover")));
            this.btnZoomFit.Image_Normal = null;
            this.btnZoomFit.Image_Type = MediaAlliance.Controls.NmButtonIcon.ZoomFit;
            this.btnZoomFit.InternalPadding = new System.Windows.Forms.Padding(0);
            this.btnZoomFit.Location = new System.Drawing.Point(0, 0);
            this.btnZoomFit.MAMenu = null;
            this.btnZoomFit.Margin = new System.Windows.Forms.Padding(87, 572, 87, 572);
            this.btnZoomFit.Menu = null;
            this.btnZoomFit.Name = "btnZoomFit";
            this.btnZoomFit.RGB_Matrix = new float[] {
        1F,
        1F,
        1F};
            this.btnZoomFit.Shortcut = System.Windows.Forms.Keys.None;
            this.btnZoomFit.ShortCutFixPosition = new System.Drawing.Point(0, 0);
            this.btnZoomFit.ShortcutTextColor = System.Drawing.Color.Silver;
            this.btnZoomFit.Size = new System.Drawing.Size(42, 42);
            this.btnZoomFit.Style = MediaAlliance.Controls.NmButtonStyle.Button;
            this.btnZoomFit.TabIndex = 52;
            this.btnZoomFit.Title = "";
            this.btnZoomFit.TitleAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.btnZoomFit.TitleVisibile = false;
            this.btnZoomFit.Tooltip_Text = "Tooltip text";
            this.btnZoomFit.UseInternalToolTip = true;
            this.btnZoomFit.Click += new System.EventHandler(this.btnZoomFit_Click);
            // 
            // pnlZoomAll
            // 
            this.pnlZoomAll.Controls.Add(this.btnZAll2);
            this.pnlZoomAll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlZoomAll.Location = new System.Drawing.Point(51, 0);
            this.pnlZoomAll.Margin = new System.Windows.Forms.Padding(0);
            this.pnlZoomAll.Name = "pnlZoomAll";
            this.pnlZoomAll.Size = new System.Drawing.Size(51, 42);
            this.pnlZoomAll.TabIndex = 0;
            // 
            // btnZAll2
            // 
            this.btnZAll2.AutoSize = true;
            this.btnZAll2.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.btnZAll2.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.automatic;
            this.btnZAll2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnZAll2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnZAll2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnZAll2.Image_Click = null;
            this.btnZAll2.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnZAll2.Image_Hover")));
            this.btnZAll2.Image_Normal = null;
            this.btnZAll2.Image_Type = MediaAlliance.Controls.NmButtonIcon.ZoomAll;
            this.btnZAll2.InternalPadding = new System.Windows.Forms.Padding(0);
            this.btnZAll2.Location = new System.Drawing.Point(0, 0);
            this.btnZAll2.MAMenu = null;
            this.btnZAll2.Margin = new System.Windows.Forms.Padding(87, 572, 87, 572);
            this.btnZAll2.Menu = null;
            this.btnZAll2.Name = "btnZAll2";
            this.btnZAll2.RGB_Matrix = new float[] {
        1F,
        1F,
        1F};
            this.btnZAll2.Shortcut = System.Windows.Forms.Keys.None;
            this.btnZAll2.ShortCutFixPosition = new System.Drawing.Point(0, 0);
            this.btnZAll2.ShortcutTextColor = System.Drawing.Color.Silver;
            this.btnZAll2.Size = new System.Drawing.Size(42, 42);
            this.btnZAll2.Style = MediaAlliance.Controls.NmButtonStyle.Button;
            this.btnZAll2.TabIndex = 52;
            this.btnZAll2.Title = "";
            this.btnZAll2.TitleAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.btnZAll2.TitleVisibile = false;
            this.btnZAll2.Tooltip_Text = "Tooltip text";
            this.btnZAll2.UseInternalToolTip = true;
            this.btnZAll2.Click += new System.EventHandler(this.picZoomAll_Click);
            // 
            // lblProjectStandard
            // 
            this.lblProjectStandard.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblProjectStandard.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProjectStandard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lblProjectStandard.Location = new System.Drawing.Point(547, 0);
            this.lblProjectStandard.Name = "lblProjectStandard";
            this.lblProjectStandard.Size = new System.Drawing.Size(729, 25);
            this.lblProjectStandard.TabIndex = 0;
            this.lblProjectStandard.Text = "PAL";
            this.lblProjectStandard.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // spOutprvOutTL
            // 
            this.spOutprvOutTL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spOutprvOutTL.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.spOutprvOutTL.IsSplitterFixed = true;
            this.spOutprvOutTL.Location = new System.Drawing.Point(0, 0);
            this.spOutprvOutTL.Name = "spOutprvOutTL";
            // 
            // spOutprvOutTL.Panel1
            // 
            this.spOutprvOutTL.Panel1.Controls.Add(this.panel3);
            // 
            // spOutprvOutTL.Panel2
            // 
            this.spOutprvOutTL.Panel2.Controls.Add(this.mA_NmPanel2);
            this.spOutprvOutTL.Panel2.Controls.Add(this.pnlTLMain_Buttons);
            this.spOutprvOutTL.Size = new System.Drawing.Size(1276, 502);
            this.spOutprvOutTL.SplitterDistance = 370;
            this.spOutprvOutTL.TabIndex = 22;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.ProjectInfoUC);
            this.panel3.Controls.Add(this.pnlPreviewMain);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(370, 502);
            this.panel3.TabIndex = 33;
            // 
            // ProjectInfoUC
            // 
            this.ProjectInfoUC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ProjectInfoUC.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProjectInfoUC.Location = new System.Drawing.Point(0, 422);
            this.ProjectInfoUC.Name = "ProjectInfoUC";
            this.ProjectInfoUC.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.ProjectInfoUC.ProjectInfo = null;
            this.ProjectInfoUC.Size = new System.Drawing.Size(370, 80);
            this.ProjectInfoUC.TabIndex = 27;
            this.ProjectInfoUC.OnFullinfoClick += new NewsMontange.UserControls.MA_NmProjectInfo.PrjInfo_OnFullinfoClick(this.ProjectInfoUC_OnFullinfoClick);
            // 
            // pnlPreviewMain
            // 
            this.pnlPreviewMain.Controls.Add(this.mA_NmPanel1);
            this.pnlPreviewMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlPreviewMain.Location = new System.Drawing.Point(0, 0);
            this.pnlPreviewMain.Margin = new System.Windows.Forms.Padding(0);
            this.pnlPreviewMain.Name = "pnlPreviewMain";
            this.pnlPreviewMain.Padding = new System.Windows.Forms.Padding(1);
            this.pnlPreviewMain.Size = new System.Drawing.Size(370, 422);
            this.pnlPreviewMain.TabIndex = 26;
            // 
            // mA_NmPanel1
            // 
            this.mA_NmPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(91)))), ((int)(((byte)(91)))), ((int)(((byte)(91)))));
            this.mA_NmPanel1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.mA_NmPanel1.BorderFocusesColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(185)))), ((int)(((byte)(45)))));
            this.mA_NmPanel1.Controls.Add(this.lblOnlineTrack);
            this.mA_NmPanel1.Controls.Add(this.lblDuration);
            this.mA_NmPanel1.Controls.Add(this.lblCurrentTC0);
            this.mA_NmPanel1.Controls.Add(this.pnlMainButtons);
            this.mA_NmPanel1.Controls.Add(this.lblCurrentClip);
            this.mA_NmPanel1.Controls.Add(this.label3);
            this.mA_NmPanel1.Controls.Add(this.label1);
            this.mA_NmPanel1.Controls.Add(this.picPreview1);
            this.mA_NmPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mA_NmPanel1.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mA_NmPanel1.ForeColor = System.Drawing.Color.DarkGray;
            this.mA_NmPanel1.IsFocused = false;
            this.mA_NmPanel1.Location = new System.Drawing.Point(1, 1);
            this.mA_NmPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.mA_NmPanel1.Name = "mA_NmPanel1";
            this.mA_NmPanel1.Padding = new System.Windows.Forms.Padding(8, 15, 8, 8);
            this.mA_NmPanel1.Size = new System.Drawing.Size(368, 420);
            this.mA_NmPanel1.TabIndex = 27;
            this.mA_NmPanel1.Title = "OUTPUT";
            // 
            // lblOnlineTrack
            // 
            this.lblOnlineTrack.BackColor = System.Drawing.Color.Gray;
            this.lblOnlineTrack.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOnlineTrack.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblOnlineTrack.Location = new System.Drawing.Point(11, 353);
            this.lblOnlineTrack.Name = "lblOnlineTrack";
            this.lblOnlineTrack.Size = new System.Drawing.Size(27, 15);
            this.lblOnlineTrack.TabIndex = 55;
            this.lblOnlineTrack.Text = "0";
            this.lblOnlineTrack.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDuration
            // 
            this.lblDuration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lblDuration.BackColor_Selected = System.Drawing.Color.DimGray;
            this.lblDuration.BorderColor = System.Drawing.Color.DarkGoldenrod;
            this.lblDuration.BorderFocusesColor = System.Drawing.Color.LightSalmon;
            this.lblDuration.Enabled = false;
            this.lblDuration.Font = new System.Drawing.Font("LCD_PS", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDuration.ForeColor = System.Drawing.Color.SlateGray;
            this.lblDuration.FPS = 25F;
            this.lblDuration.Location = new System.Drawing.Point(214, 319);
            this.lblDuration.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lblDuration.Name = "lblDuration";
            this.lblDuration.Size = new System.Drawing.Size(143, 30);
            this.lblDuration.TabIndex = 54;
            this.lblDuration.TimeCode = ((long)(0));
            // 
            // lblCurrentTC0
            // 
            this.lblCurrentTC0.BackColor_Selected = System.Drawing.Color.DimGray;
            this.lblCurrentTC0.BorderColor = System.Drawing.Color.DarkGoldenrod;
            this.lblCurrentTC0.BorderFocusesColor = System.Drawing.Color.LightSalmon;
            this.lblCurrentTC0.Font = new System.Drawing.Font("LCD_PS", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentTC0.ForeColor = System.Drawing.Color.LightGray;
            this.lblCurrentTC0.FPS = 25F;
            this.lblCurrentTC0.Location = new System.Drawing.Point(8, 319);
            this.lblCurrentTC0.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lblCurrentTC0.Name = "lblCurrentTC0";
            this.lblCurrentTC0.Size = new System.Drawing.Size(143, 30);
            this.lblCurrentTC0.TabIndex = 53;
            this.lblCurrentTC0.TimeCode = ((long)(0));
            this.lblCurrentTC0.OnTimecodeChanged += new NewsMontange.UserControls.MA_TimeCodeUC.MATC_OnTimecodeChanged(this.lblCurrentTC0_OnTimecodeChanged);
            // 
            // pnlMainButtons
            // 
            this.pnlMainButtons.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlMainButtons.Controls.Add(this.tableLayoutPanel1);
            this.pnlMainButtons.Location = new System.Drawing.Point(89, 375);
            this.pnlMainButtons.Name = "pnlMainButtons";
            this.pnlMainButtons.Size = new System.Drawing.Size(191, 42);
            this.pnlMainButtons.TabIndex = 43;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.btnFrameFFW, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnPlay, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnPause, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnFrameREW, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(191, 42);
            this.tableLayoutPanel1.TabIndex = 43;
            // 
            // btnFrameFFW
            // 
            this.btnFrameFFW.BackColor = System.Drawing.Color.Transparent;
            this.btnFrameFFW.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.btnFrameFFW.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.automatic;
            this.btnFrameFFW.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFrameFFW.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFrameFFW.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnFrameFFW.Image_Click = null;
            this.btnFrameFFW.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnFrameFFW.Image_Hover")));
            this.btnFrameFFW.Image_Normal = null;
            this.btnFrameFFW.Image_Type = MediaAlliance.Controls.NmButtonIcon.NextFrame;
            this.btnFrameFFW.InternalPadding = new System.Windows.Forms.Padding(0);
            this.btnFrameFFW.Location = new System.Drawing.Point(143, 3);
            this.btnFrameFFW.MAMenu = null;
            this.btnFrameFFW.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnFrameFFW.Menu = null;
            this.btnFrameFFW.Name = "btnFrameFFW";
            this.btnFrameFFW.RGB_Matrix = new float[] {
        1F,
        1F,
        1F};
            this.btnFrameFFW.Shortcut = System.Windows.Forms.Keys.None;
            this.btnFrameFFW.ShortCutFixPosition = new System.Drawing.Point(0, 0);
            this.btnFrameFFW.ShortcutTextColor = System.Drawing.Color.Silver;
            this.btnFrameFFW.Size = new System.Drawing.Size(46, 36);
            this.btnFrameFFW.Style = MediaAlliance.Controls.NmButtonStyle.Button;
            this.btnFrameFFW.TabIndex = 41;
            this.btnFrameFFW.Title = "";
            this.btnFrameFFW.TitleAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFrameFFW.TitleVisibile = false;
            this.btnFrameFFW.Tooltip_Text = "Tooltip text";
            this.btnFrameFFW.UseInternalToolTip = true;
            this.btnFrameFFW.Click += new System.EventHandler(this.btnFrameFFW_Click);
            // 
            // btnPlay
            // 
            this.btnPlay.BackColor = System.Drawing.Color.Transparent;
            this.btnPlay.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.btnPlay.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.automatic;
            this.btnPlay.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPlay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPlay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnPlay.Image_Click = null;
            this.btnPlay.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnPlay.Image_Hover")));
            this.btnPlay.Image_Normal = null;
            this.btnPlay.Image_Type = MediaAlliance.Controls.NmButtonIcon.Play;
            this.btnPlay.InternalPadding = new System.Windows.Forms.Padding(0);
            this.btnPlay.Location = new System.Drawing.Point(49, 3);
            this.btnPlay.MAMenu = null;
            this.btnPlay.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnPlay.Menu = null;
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.RGB_Matrix = new float[] {
        1F,
        1F,
        1F};
            this.btnPlay.Shortcut = System.Windows.Forms.Keys.None;
            this.btnPlay.ShortCutFixPosition = new System.Drawing.Point(0, 0);
            this.btnPlay.ShortcutTextColor = System.Drawing.Color.Silver;
            this.btnPlay.Size = new System.Drawing.Size(43, 36);
            this.btnPlay.Style = MediaAlliance.Controls.NmButtonStyle.Button;
            this.btnPlay.TabIndex = 39;
            this.btnPlay.Title = "";
            this.btnPlay.TitleAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.btnPlay.TitleVisibile = false;
            this.btnPlay.Tooltip_Text = "Tooltip text";
            this.btnPlay.UseInternalToolTip = true;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click_1);
            // 
            // btnPause
            // 
            this.btnPause.BackColor = System.Drawing.Color.Transparent;
            this.btnPause.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.btnPause.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.automatic;
            this.btnPause.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPause.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPause.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnPause.Image_Click = null;
            this.btnPause.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnPause.Image_Hover")));
            this.btnPause.Image_Normal = null;
            this.btnPause.Image_Type = MediaAlliance.Controls.NmButtonIcon.Pause;
            this.btnPause.InternalPadding = new System.Windows.Forms.Padding(0);
            this.btnPause.Location = new System.Drawing.Point(96, 3);
            this.btnPause.MAMenu = null;
            this.btnPause.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnPause.Menu = null;
            this.btnPause.Name = "btnPause";
            this.btnPause.RGB_Matrix = new float[] {
        1F,
        1F,
        1F};
            this.btnPause.Shortcut = System.Windows.Forms.Keys.None;
            this.btnPause.ShortCutFixPosition = new System.Drawing.Point(0, 0);
            this.btnPause.ShortcutTextColor = System.Drawing.Color.Silver;
            this.btnPause.Size = new System.Drawing.Size(43, 36);
            this.btnPause.Style = MediaAlliance.Controls.NmButtonStyle.Button;
            this.btnPause.TabIndex = 40;
            this.btnPause.Title = "";
            this.btnPause.TitleAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.btnPause.TitleVisibile = false;
            this.btnPause.Tooltip_Text = "Tooltip text";
            this.btnPause.UseInternalToolTip = true;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // btnFrameREW
            // 
            this.btnFrameREW.BackColor = System.Drawing.Color.Transparent;
            this.btnFrameREW.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.btnFrameREW.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.automatic;
            this.btnFrameREW.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFrameREW.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFrameREW.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnFrameREW.Image_Click = null;
            this.btnFrameREW.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btnFrameREW.Image_Hover")));
            this.btnFrameREW.Image_Normal = null;
            this.btnFrameREW.Image_Type = MediaAlliance.Controls.NmButtonIcon.BackFrame;
            this.btnFrameREW.InternalPadding = new System.Windows.Forms.Padding(0);
            this.btnFrameREW.Location = new System.Drawing.Point(2, 3);
            this.btnFrameREW.MAMenu = null;
            this.btnFrameREW.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnFrameREW.Menu = null;
            this.btnFrameREW.Name = "btnFrameREW";
            this.btnFrameREW.RGB_Matrix = new float[] {
        1F,
        1F,
        1F};
            this.btnFrameREW.Shortcut = System.Windows.Forms.Keys.None;
            this.btnFrameREW.ShortCutFixPosition = new System.Drawing.Point(0, 0);
            this.btnFrameREW.ShortcutTextColor = System.Drawing.Color.Silver;
            this.btnFrameREW.Size = new System.Drawing.Size(43, 36);
            this.btnFrameREW.Style = MediaAlliance.Controls.NmButtonStyle.Button;
            this.btnFrameREW.TabIndex = 42;
            this.btnFrameREW.Title = "";
            this.btnFrameREW.TitleAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFrameREW.TitleVisibile = false;
            this.btnFrameREW.Tooltip_Text = "Tooltip text";
            this.btnFrameREW.UseInternalToolTip = true;
            this.btnFrameREW.Click += new System.EventHandler(this.btnFrameREW_Click);
            this.btnFrameREW.MouseEnter += new System.EventHandler(this.btnFrameREW_MouseEnter);
            // 
            // lblCurrentClip
            // 
            this.lblCurrentClip.BackColor = System.Drawing.Color.Gray;
            this.lblCurrentClip.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentClip.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCurrentClip.Location = new System.Drawing.Point(44, 353);
            this.lblCurrentClip.Name = "lblCurrentClip";
            this.lblCurrentClip.Size = new System.Drawing.Size(316, 15);
            this.lblCurrentClip.TabIndex = 29;
            this.lblCurrentClip.Text = "no clip";
            this.lblCurrentClip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblCurrentClip.MouseMove += new System.Windows.Forms.MouseEventHandler(this.lblCurrentClip_MouseMove);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(214, 305);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 12);
            this.label3.TabIndex = 28;
            this.label3.Text = "Duration";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 305);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 12);
            this.label1.TabIndex = 27;
            this.label1.Text = "Current Position";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // picPreview1
            // 
            this.picPreview1.BackColor = System.Drawing.Color.Black;
            this.picPreview1.Location = new System.Drawing.Point(8, 15);
            this.picPreview1.Margin = new System.Windows.Forms.Padding(0);
            this.picPreview1.Name = "picPreview1";
            this.picPreview1.Size = new System.Drawing.Size(352, 288);
            this.picPreview1.TabIndex = 0;
            // 
            // mA_NmPanel2
            // 
            this.mA_NmPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(91)))), ((int)(((byte)(91)))), ((int)(((byte)(91)))));
            this.mA_NmPanel2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.mA_NmPanel2.BorderFocusesColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(185)))), ((int)(((byte)(45)))));
            this.mA_NmPanel2.Controls.Add(this.mxptl);
            this.mA_NmPanel2.Controls.Add(this.pnlTrackInfo);
            this.mA_NmPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mA_NmPanel2.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mA_NmPanel2.ForeColor = System.Drawing.Color.DarkGray;
            this.mA_NmPanel2.IsFocused = false;
            this.mA_NmPanel2.Location = new System.Drawing.Point(0, 0);
            this.mA_NmPanel2.Name = "mA_NmPanel2";
            this.mA_NmPanel2.Padding = new System.Windows.Forms.Padding(8, 15, 8, 8);
            this.mA_NmPanel2.Size = new System.Drawing.Size(902, 460);
            this.mA_NmPanel2.TabIndex = 1;
            this.mA_NmPanel2.Title = "TIMELINE";
            // 
            // mxptl
            // 
            this.mxptl.AllowDrop = true;
            this.mxptl.AutoAdaptLines = false;
            this.mxptl.CanDisableTracks = true;
            this.mxptl.CanDragClip = true;
            this.mxptl.CanDragoutClips = false;
            this.mxptl.CanDragoutMarkers = false;
            this.mxptl.CanEditAudioNodes = true;
            this.mxptl.CanMarkOverRectime = true;
            this.mxptl.CanModifyMarkerByGUI = true;
            this.mxptl.CanSeekOverClip = true;
            this.mxptl.CanSeekOverRectime = true;
            this.mxptl.ClipColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.mxptl.ClipOfContextMenu = null;
            this.mxptl.ColorFocus = System.Drawing.Color.LightSalmon;
            this.mxptl.ContextMenuStrip = this.mnuTimeline;
            this.mxptl.CurrentPosition = ((long)(0));
            this.mxptl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mxptl.FireDisplayClipEventOnPan = false;
            this.mxptl.FPS = 25F;
            this.mxptl.Location = new System.Drawing.Point(80, 15);
            this.mxptl.Margin = new System.Windows.Forms.Padding(0);
            this.mxptl.Menu_Clips = this.mnuClip;
            this.mxptl.Menu_General = null;
            this.mxptl.Name = "mxptl";
            this.mxptl.RecTime = ((long)(0));
            this.mxptl.RecTimeBarHeight = 0;
            this.mxptl.SelectedClipColor = System.Drawing.Color.LightSalmon;
            this.mxptl.ShowFocus = true;
            this.mxptl.ShowFocusBorder = false;
            this.mxptl.Size = new System.Drawing.Size(814, 437);
            this.mxptl.TabIndex = 0;
            this.mxptl.TCIN = ((long)(0));
            this.mxptl.TCOUT = ((long)(200));
            this.mxptl.TimeBarHeight = 22;
            this.mxptl.TimeLineBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.mxptl.TimeLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.mxptl.TrackBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.mxptl.TrackColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(125)))), ((int)(((byte)(127)))));
            this.mxptl.TrackHandleColor = System.Drawing.Color.Silver;
            this.mxptl.TrackHeight = 70;
            this.mxptl.TrackInfoControl_Container = this.pnlTrackInfo;
            this.mxptl.UndoTimes = 10;
            this.mxptl.ZoomBarHeight = 20;
            this.mxptl.ZoomIn = ((long)(0));
            this.mxptl.ZoomLimitMin = 1F;
            this.mxptl.ZoomOut = ((long)(0));
            this.mxptl.OnZoomChanged += new MediaAlliance.Controls.MxpTimeLine.MXPTL_ZoomAreaChanged(this.mxptl_OnZoomChanged);
            this.mxptl.OnChangePosition += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnChangePosition(this.mxptl_OnChangePosition);
            this.mxptl.OnChangeClipSelection += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnChangeClipSelection(this.mxptl_OnChangeClipSelection);
            this.mxptl.OnChangeMarker += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnChangeMarker(this.mxptl_OnChangeMarker);
            this.mxptl.OnEndingClipEditing += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnEndingClipEditing(this.mxptl_OnEndingClipEditing);
            this.mxptl.OnTimecodeEvent += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnTimecodeEvent(this.mxptl_OnTimecodeEvent);
            this.mxptl.OnOnlineItemsTableChanged += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnOnlineItemsTableChanged(this.mxptl_OnOnlineItemsTableChanged);
            this.mxptl.OnItemChangeOnlineStatus += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnItemChangeOnlineStatus(this.mxptl_OnItemChangeOnlineStatus);
            this.mxptl.OnInternalEngineEvent += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnInternalEngineEvent(this.mxptl_OnInternalEngineEvent);
            this.mxptl.OnChangeActiveActions += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnChangeActiveActions(this.mxptl_OnChangeActiveActions);
            this.mxptl.OnExitedFromActions += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnExitedFromActions(this.mxptl_OnExitedFromActions);
            this.mxptl.OnClipAdded += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnClipAdded(this.mxptl_OnClipAdded);
            this.mxptl.OnClipRemoving += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnClipRemoving(this.mxptl_OnClipRemoving);
            this.mxptl.OnClipDisplayInfoChanged += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnClipDisplayInfoChanged(this.mxptl_OnClipDisplayInfoChanged);
            this.mxptl.OnAdvanceDraggingEvent += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnAdvanceDraggingEvent(this.mxptl_OnAdvanceDraggingEvent);
            this.mxptl.OnAdvenceDragDrop += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnAdvenceDragDrop(this.mxptl_OnAdvenceDragDrop);
            this.mxptl.OnUndoEvent += new MediaAlliance.Controls.MxpTimeLine.MXPTL_OnUndoEvent(this.mxptl_OnUndoEvent);
            // 
            // mnuTimeline
            // 
            this.mnuTimeline.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.markInToolStripMenuItem,
            this.markOutToolStripMenuItem});
            this.mnuTimeline.Name = "mnuTimeline";
            this.mnuTimeline.Size = new System.Drawing.Size(123, 48);
            // 
            // markInToolStripMenuItem
            // 
            this.markInToolStripMenuItem.Name = "markInToolStripMenuItem";
            this.markInToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.markInToolStripMenuItem.Text = "Mark in";
            this.markInToolStripMenuItem.Click += new System.EventHandler(this.markInToolStripMenuItem_Click);
            // 
            // markOutToolStripMenuItem
            // 
            this.markOutToolStripMenuItem.Name = "markOutToolStripMenuItem";
            this.markOutToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.markOutToolStripMenuItem.Text = "Mark out";
            this.markOutToolStripMenuItem.Click += new System.EventHandler(this.markOutToolStripMenuItem_Click);
            // 
            // pnlTrackInfo
            // 
            this.pnlTrackInfo.BackColor = System.Drawing.Color.Transparent;
            this.pnlTrackInfo.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlTrackInfo.Location = new System.Drawing.Point(8, 15);
            this.pnlTrackInfo.Name = "pnlTrackInfo";
            this.pnlTrackInfo.Size = new System.Drawing.Size(72, 437);
            this.pnlTrackInfo.TabIndex = 1;
            // 
            // pnlTLMain_Buttons
            // 
            this.pnlTLMain_Buttons.Controls.Add(this.button1);
            this.pnlTLMain_Buttons.Controls.Add(this.tableLayoutPanel4);
            this.pnlTLMain_Buttons.Controls.Add(this.tableLayoutPanel3);
            this.pnlTLMain_Buttons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlTLMain_Buttons.Location = new System.Drawing.Point(0, 460);
            this.pnlTLMain_Buttons.Margin = new System.Windows.Forms.Padding(0);
            this.pnlTLMain_Buttons.Name = "pnlTLMain_Buttons";
            this.pnlTLMain_Buttons.Size = new System.Drawing.Size(902, 42);
            this.pnlTLMain_Buttons.TabIndex = 56;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(690, 16);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 27;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.AutoSize = false;
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.internalLogToolStripMenuItem,
            this.toolStripMenuItem1,
            this.projectToolStripMenuItem,
            this.aboutToolStripMenuItem,
            this.tsmAppAlert,
            this.tsmFieldsAlert});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(583, 25);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.BackColor = System.Drawing.Color.Gray;
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startupMenuToolStripMenuItem,
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.rollbackToolStripMenuItem,
            this.exportToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 21);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // startupMenuToolStripMenuItem
            // 
            this.startupMenuToolStripMenuItem.Name = "startupMenuToolStripMenuItem";
            this.startupMenuToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.startupMenuToolStripMenuItem.Text = "Startup Menu";
            this.startupMenuToolStripMenuItem.Click += new System.EventHandler(this.startupMenuToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Visible = false;
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Image = global::NewsMontange.Properties.Resources.save;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // rollbackToolStripMenuItem
            // 
            this.rollbackToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.rollbackToolStripMenuItem.Image = global::NewsMontange.Properties.Resources.Rollback_autosave;
            this.rollbackToolStripMenuItem.Name = "rollbackToolStripMenuItem";
            this.rollbackToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.rollbackToolStripMenuItem.Text = "Rollback";
            this.rollbackToolStripMenuItem.Click += new System.EventHandler(this.rollbackToolStripMenuItem_Click);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.exportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mediaToolStripMenuItem,
            this.mediaAsToolStripMenuItem,
            this.exportAudioToolStripMenuItem});
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.exportToolStripMenuItem.Text = "Export";
            // 
            // mediaToolStripMenuItem
            // 
            this.mediaToolStripMenuItem.Name = "mediaToolStripMenuItem";
            this.mediaToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.mediaToolStripMenuItem.Text = "Media";
            this.mediaToolStripMenuItem.Click += new System.EventHandler(this.mediaToolStripMenuItem_Click);
            // 
            // mediaAsToolStripMenuItem
            // 
            this.mediaAsToolStripMenuItem.Name = "mediaAsToolStripMenuItem";
            this.mediaAsToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.mediaAsToolStripMenuItem.Text = "Media As";
            this.mediaAsToolStripMenuItem.Click += new System.EventHandler(this.mediaAsToolStripMenuItem_Click);
            // 
            // exportAudioToolStripMenuItem
            // 
            this.exportAudioToolStripMenuItem.Name = "exportAudioToolStripMenuItem";
            this.exportAudioToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.exportAudioToolStripMenuItem.Text = "Export audio";
            this.exportAudioToolStripMenuItem.Click += new System.EventHandler(this.exportAudioToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem,
            this.toolStripSeparator1,
            this.preferenceToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 21);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.undoToolStripMenuItem.Text = "Undo";
            this.undoToolStripMenuItem.Click += new System.EventHandler(this.undoToolStripMenuItem_Click);
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.redoToolStripMenuItem.Text = "Redo";
            this.redoToolStripMenuItem.Visible = false;
            this.redoToolStripMenuItem.Click += new System.EventHandler(this.redoToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(127, 6);
            // 
            // preferenceToolStripMenuItem
            // 
            this.preferenceToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.preferenceToolStripMenuItem.Image = global::NewsMontange.Properties.Resources.BTN_Settings;
            this.preferenceToolStripMenuItem.Name = "preferenceToolStripMenuItem";
            this.preferenceToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.preferenceToolStripMenuItem.Text = "Preference";
            this.preferenceToolStripMenuItem.Click += new System.EventHandler(this.preferenceToolStripMenuItem_Click);
            // 
            // internalLogToolStripMenuItem
            // 
            this.internalLogToolStripMenuItem.Name = "internalLogToolStripMenuItem";
            this.internalLogToolStripMenuItem.Size = new System.Drawing.Size(82, 21);
            this.internalLogToolStripMenuItem.Text = "Internal Log";
            this.internalLogToolStripMenuItem.Click += new System.EventHandler(this.internalLogToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clipInfoToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(61, 21);
            this.toolStripMenuItem1.Text = "Options";
            this.toolStripMenuItem1.DropDownOpening += new System.EventHandler(this.toolStripMenuItem1_DropDownOpening);
            // 
            // clipInfoToolStripMenuItem
            // 
            this.clipInfoToolStripMenuItem.Name = "clipInfoToolStripMenuItem";
            this.clipInfoToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.clipInfoToolStripMenuItem.Text = "Clip Info";
            this.clipInfoToolStripMenuItem.Click += new System.EventHandler(this.clipInfoToolStripMenuItem_Click);
            // 
            // projectToolStripMenuItem
            // 
            this.projectToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.recoverBlankOnStartToolStripMenuItem});
            this.projectToolStripMenuItem.Name = "projectToolStripMenuItem";
            this.projectToolStripMenuItem.Size = new System.Drawing.Size(56, 21);
            this.projectToolStripMenuItem.Text = "Project";
            // 
            // recoverBlankOnStartToolStripMenuItem
            // 
            this.recoverBlankOnStartToolStripMenuItem.Name = "recoverBlankOnStartToolStripMenuItem";
            this.recoverBlankOnStartToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.recoverBlankOnStartToolStripMenuItem.Text = "Recover blank on start";
            this.recoverBlankOnStartToolStripMenuItem.Click += new System.EventHandler(this.recoverBlankOnStartToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem1,
            this.activationToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.aboutToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem1
            // 
            this.aboutToolStripMenuItem1.Name = "aboutToolStripMenuItem1";
            this.aboutToolStripMenuItem1.Size = new System.Drawing.Size(128, 22);
            this.aboutToolStripMenuItem1.Text = "About";
            this.aboutToolStripMenuItem1.Click += new System.EventHandler(this.aboutToolStripMenuItem1_Click);
            // 
            // activationToolStripMenuItem
            // 
            this.activationToolStripMenuItem.Name = "activationToolStripMenuItem";
            this.activationToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.activationToolStripMenuItem.Text = "Activation";
            this.activationToolStripMenuItem.Click += new System.EventHandler(this.activationToolStripMenuItem_Click);
            // 
            // tsmAppAlert
            // 
            this.tsmAppAlert.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsmAppAlert.Image = global::NewsMontange.Properties.Resources.alert_red;
            this.tsmAppAlert.Name = "tsmAppAlert";
            this.tsmAppAlert.Size = new System.Drawing.Size(28, 21);
            this.tsmAppAlert.Text = "toolStripMenuItem2";
            this.tsmAppAlert.Click += new System.EventHandler(this.tsmAppAlert_Click);
            // 
            // tsmFieldsAlert
            // 
            this.tsmFieldsAlert.AutoSize = false;
            this.tsmFieldsAlert.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsmFieldsAlert.Image = global::NewsMontange.Properties.Resources.ICO_FieldsInverted;
            this.tsmFieldsAlert.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmFieldsAlert.Name = "tsmFieldsAlert";
            this.tsmFieldsAlert.Size = new System.Drawing.Size(35, 21);
            this.tsmFieldsAlert.Text = "toolStripMenuItem2";
            this.tsmFieldsAlert.Visible = false;
            // 
            // spMontageSources
            // 
            this.spMontageSources.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spMontageSources.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.spMontageSources.IsSplitterFixed = true;
            this.spMontageSources.Location = new System.Drawing.Point(4, 25);
            this.spMontageSources.Name = "spMontageSources";
            this.spMontageSources.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spMontageSources.Panel1
            // 
            this.spMontageSources.Panel1.Controls.Add(this.spOutprvOutTL);
            // 
            // spMontageSources.Panel2
            // 
            this.spMontageSources.Panel2.Controls.Add(this.spSrcprvSrcTL);
            this.spMontageSources.Size = new System.Drawing.Size(1276, 886);
            this.spMontageSources.SplitterDistance = 502;
            this.spMontageSources.TabIndex = 34;
            // 
            // spSrcprvSrcTL
            // 
            this.spSrcprvSrcTL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spSrcprvSrcTL.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.spSrcprvSrcTL.IsSplitterFixed = true;
            this.spSrcprvSrcTL.Location = new System.Drawing.Point(0, 0);
            this.spSrcprvSrcTL.Name = "spSrcprvSrcTL";
            // 
            // spSrcprvSrcTL.Panel1
            // 
            this.spSrcprvSrcTL.Panel1.Controls.Add(this.mA_NmPanel3);
            // 
            // spSrcprvSrcTL.Panel2
            // 
            this.spSrcprvSrcTL.Panel2.Controls.Add(this.mA_NmPanel4);
            this.spSrcprvSrcTL.Size = new System.Drawing.Size(1276, 380);
            this.spSrcprvSrcTL.SplitterDistance = 370;
            this.spSrcprvSrcTL.TabIndex = 0;
            // 
            // mA_NmPanel3
            // 
            this.mA_NmPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(91)))), ((int)(((byte)(91)))), ((int)(((byte)(91)))));
            this.mA_NmPanel3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.mA_NmPanel3.BorderFocusesColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(185)))), ((int)(((byte)(45)))));
            this.mA_NmPanel3.Controls.Add(this.lblCurrentTC_SRC);
            this.mA_NmPanel3.Controls.Add(this.panel1);
            this.mA_NmPanel3.Controls.Add(this.preview_monitor_src);
            this.mA_NmPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mA_NmPanel3.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mA_NmPanel3.ForeColor = System.Drawing.Color.DarkGray;
            this.mA_NmPanel3.IsFocused = false;
            this.mA_NmPanel3.Location = new System.Drawing.Point(0, 0);
            this.mA_NmPanel3.Name = "mA_NmPanel3";
            this.mA_NmPanel3.Padding = new System.Windows.Forms.Padding(8, 15, 8, 8);
            this.mA_NmPanel3.Size = new System.Drawing.Size(370, 380);
            this.mA_NmPanel3.TabIndex = 34;
            this.mA_NmPanel3.Title = "SOURCE PREVIEW";
            // 
            // lblCurrentTC_SRC
            // 
            this.lblCurrentTC_SRC.BackColor_Selected = System.Drawing.Color.DimGray;
            this.lblCurrentTC_SRC.BorderColor = System.Drawing.Color.DarkGoldenrod;
            this.lblCurrentTC_SRC.BorderFocusesColor = System.Drawing.Color.LightSalmon;
            this.lblCurrentTC_SRC.Enabled = false;
            this.lblCurrentTC_SRC.Font = new System.Drawing.Font("LCD_PS", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentTC_SRC.ForeColor = System.Drawing.Color.LightGray;
            this.lblCurrentTC_SRC.FPS = 25F;
            this.lblCurrentTC_SRC.Location = new System.Drawing.Point(9, 305);
            this.lblCurrentTC_SRC.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lblCurrentTC_SRC.Name = "lblCurrentTC_SRC";
            this.lblCurrentTC_SRC.Size = new System.Drawing.Size(143, 30);
            this.lblCurrentTC_SRC.TabIndex = 54;
            this.lblCurrentTC_SRC.TimeCode = ((long)(0));
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.Controls.Add(this.tableLayoutPanel2);
            this.panel1.Location = new System.Drawing.Point(90, 336);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(189, 42);
            this.panel1.TabIndex = 44;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.btn_FrameFFwSrc, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.btn_PlaySrc, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.btn_PauseSrc, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.btn_FrameREWSrc, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(189, 42);
            this.tableLayoutPanel2.TabIndex = 55;
            // 
            // btn_FrameFFwSrc
            // 
            this.btn_FrameFFwSrc.BackColor = System.Drawing.Color.Transparent;
            this.btn_FrameFFwSrc.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.btn_FrameFFwSrc.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.automatic;
            this.btn_FrameFFwSrc.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_FrameFFwSrc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_FrameFFwSrc.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btn_FrameFFwSrc.Image_Click = null;
            this.btn_FrameFFwSrc.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btn_FrameFFwSrc.Image_Hover")));
            this.btn_FrameFFwSrc.Image_Normal = null;
            this.btn_FrameFFwSrc.Image_Type = MediaAlliance.Controls.NmButtonIcon.NextFrame;
            this.btn_FrameFFwSrc.InternalPadding = new System.Windows.Forms.Padding(0);
            this.btn_FrameFFwSrc.Location = new System.Drawing.Point(143, 3);
            this.btn_FrameFFwSrc.MAMenu = null;
            this.btn_FrameFFwSrc.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btn_FrameFFwSrc.Menu = null;
            this.btn_FrameFFwSrc.Name = "btn_FrameFFwSrc";
            this.btn_FrameFFwSrc.RGB_Matrix = new float[] {
        1F,
        1F,
        1F};
            this.btn_FrameFFwSrc.Shortcut = System.Windows.Forms.Keys.None;
            this.btn_FrameFFwSrc.ShortCutFixPosition = new System.Drawing.Point(0, 0);
            this.btn_FrameFFwSrc.ShortcutTextColor = System.Drawing.Color.Silver;
            this.btn_FrameFFwSrc.Size = new System.Drawing.Size(44, 36);
            this.btn_FrameFFwSrc.Style = MediaAlliance.Controls.NmButtonStyle.Button;
            this.btn_FrameFFwSrc.TabIndex = 41;
            this.btn_FrameFFwSrc.Title = "";
            this.btn_FrameFFwSrc.TitleAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_FrameFFwSrc.TitleVisibile = false;
            this.btn_FrameFFwSrc.Tooltip_Text = "Tooltip text";
            this.btn_FrameFFwSrc.UseInternalToolTip = true;
            this.btn_FrameFFwSrc.Click += new System.EventHandler(this.btn_FrameFFwSrc_Click);
            // 
            // btn_PlaySrc
            // 
            this.btn_PlaySrc.BackColor = System.Drawing.Color.Transparent;
            this.btn_PlaySrc.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.btn_PlaySrc.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.automatic;
            this.btn_PlaySrc.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_PlaySrc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_PlaySrc.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btn_PlaySrc.Image_Click = null;
            this.btn_PlaySrc.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btn_PlaySrc.Image_Hover")));
            this.btn_PlaySrc.Image_Normal = null;
            this.btn_PlaySrc.Image_Type = MediaAlliance.Controls.NmButtonIcon.Play;
            this.btn_PlaySrc.InternalPadding = new System.Windows.Forms.Padding(0);
            this.btn_PlaySrc.Location = new System.Drawing.Point(49, 3);
            this.btn_PlaySrc.MAMenu = null;
            this.btn_PlaySrc.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btn_PlaySrc.Menu = null;
            this.btn_PlaySrc.Name = "btn_PlaySrc";
            this.btn_PlaySrc.RGB_Matrix = new float[] {
        1F,
        1F,
        1F};
            this.btn_PlaySrc.Shortcut = System.Windows.Forms.Keys.None;
            this.btn_PlaySrc.ShortCutFixPosition = new System.Drawing.Point(0, 0);
            this.btn_PlaySrc.ShortcutTextColor = System.Drawing.Color.Silver;
            this.btn_PlaySrc.Size = new System.Drawing.Size(43, 36);
            this.btn_PlaySrc.Style = MediaAlliance.Controls.NmButtonStyle.Button;
            this.btn_PlaySrc.TabIndex = 39;
            this.btn_PlaySrc.Title = "";
            this.btn_PlaySrc.TitleAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_PlaySrc.TitleVisibile = false;
            this.btn_PlaySrc.Tooltip_Text = "Tooltip text";
            this.btn_PlaySrc.UseInternalToolTip = true;
            this.btn_PlaySrc.Click += new System.EventHandler(this.btn_PlaySrc_Click);
            // 
            // btn_PauseSrc
            // 
            this.btn_PauseSrc.BackColor = System.Drawing.Color.Transparent;
            this.btn_PauseSrc.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.btn_PauseSrc.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.automatic;
            this.btn_PauseSrc.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_PauseSrc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_PauseSrc.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btn_PauseSrc.Image_Click = null;
            this.btn_PauseSrc.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btn_PauseSrc.Image_Hover")));
            this.btn_PauseSrc.Image_Normal = null;
            this.btn_PauseSrc.Image_Type = MediaAlliance.Controls.NmButtonIcon.Pause;
            this.btn_PauseSrc.InternalPadding = new System.Windows.Forms.Padding(0);
            this.btn_PauseSrc.Location = new System.Drawing.Point(96, 3);
            this.btn_PauseSrc.MAMenu = null;
            this.btn_PauseSrc.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btn_PauseSrc.Menu = null;
            this.btn_PauseSrc.Name = "btn_PauseSrc";
            this.btn_PauseSrc.RGB_Matrix = new float[] {
        1F,
        1F,
        1F};
            this.btn_PauseSrc.Shortcut = System.Windows.Forms.Keys.None;
            this.btn_PauseSrc.ShortCutFixPosition = new System.Drawing.Point(0, 0);
            this.btn_PauseSrc.ShortcutTextColor = System.Drawing.Color.Silver;
            this.btn_PauseSrc.Size = new System.Drawing.Size(43, 36);
            this.btn_PauseSrc.Style = MediaAlliance.Controls.NmButtonStyle.Button;
            this.btn_PauseSrc.TabIndex = 40;
            this.btn_PauseSrc.Title = "";
            this.btn_PauseSrc.TitleAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_PauseSrc.TitleVisibile = false;
            this.btn_PauseSrc.Tooltip_Text = "Tooltip text";
            this.btn_PauseSrc.UseInternalToolTip = true;
            this.btn_PauseSrc.Click += new System.EventHandler(this.btn_PauseSrc_Click);
            // 
            // btn_FrameREWSrc
            // 
            this.btn_FrameREWSrc.BackColor = System.Drawing.Color.Transparent;
            this.btn_FrameREWSrc.ButtonImageSizeMode = MediaAlliance.Controls.ImageSizeMode.original;
            this.btn_FrameREWSrc.ClickEffectStyle = MediaAlliance.Controls.NmButtonClickEffectStyle.automatic;
            this.btn_FrameREWSrc.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_FrameREWSrc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_FrameREWSrc.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btn_FrameREWSrc.Image_Click = null;
            this.btn_FrameREWSrc.Image_Hover = ((System.Drawing.Image)(resources.GetObject("btn_FrameREWSrc.Image_Hover")));
            this.btn_FrameREWSrc.Image_Normal = null;
            this.btn_FrameREWSrc.Image_Type = MediaAlliance.Controls.NmButtonIcon.BackFrame;
            this.btn_FrameREWSrc.InternalPadding = new System.Windows.Forms.Padding(0);
            this.btn_FrameREWSrc.Location = new System.Drawing.Point(2, 3);
            this.btn_FrameREWSrc.MAMenu = null;
            this.btn_FrameREWSrc.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btn_FrameREWSrc.Menu = null;
            this.btn_FrameREWSrc.Name = "btn_FrameREWSrc";
            this.btn_FrameREWSrc.RGB_Matrix = new float[] {
        1F,
        1F,
        1F};
            this.btn_FrameREWSrc.Shortcut = System.Windows.Forms.Keys.None;
            this.btn_FrameREWSrc.ShortCutFixPosition = new System.Drawing.Point(0, 0);
            this.btn_FrameREWSrc.ShortcutTextColor = System.Drawing.Color.Silver;
            this.btn_FrameREWSrc.Size = new System.Drawing.Size(43, 36);
            this.btn_FrameREWSrc.Style = MediaAlliance.Controls.NmButtonStyle.Button;
            this.btn_FrameREWSrc.TabIndex = 42;
            this.btn_FrameREWSrc.Title = "";
            this.btn_FrameREWSrc.TitleAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_FrameREWSrc.TitleVisibile = false;
            this.btn_FrameREWSrc.Tooltip_Text = "Tooltip text";
            this.btn_FrameREWSrc.UseInternalToolTip = true;
            this.btn_FrameREWSrc.Click += new System.EventHandler(this.btn_FrameREWSrc_Click);
            // 
            // preview_monitor_src
            // 
            this.preview_monitor_src.BackColor = System.Drawing.Color.Black;
            this.preview_monitor_src.Location = new System.Drawing.Point(8, 15);
            this.preview_monitor_src.Name = "preview_monitor_src";
            this.preview_monitor_src.Size = new System.Drawing.Size(352, 288);
            this.preview_monitor_src.TabIndex = 33;
            // 
            // mA_NmPanel4
            // 
            this.mA_NmPanel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(91)))), ((int)(((byte)(91)))), ((int)(((byte)(91)))));
            this.mA_NmPanel4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.mA_NmPanel4.BorderFocusesColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(185)))), ((int)(((byte)(45)))));
            this.mA_NmPanel4.Controls.Add(this.source_tab);
            this.mA_NmPanel4.Controls.Add(this.lblGeneratingWaves);
            this.mA_NmPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mA_NmPanel4.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mA_NmPanel4.ForeColor = System.Drawing.Color.DarkGray;
            this.mA_NmPanel4.IsFocused = false;
            this.mA_NmPanel4.Location = new System.Drawing.Point(0, 0);
            this.mA_NmPanel4.Name = "mA_NmPanel4";
            this.mA_NmPanel4.Padding = new System.Windows.Forms.Padding(0, 0, 0, 8);
            this.mA_NmPanel4.Size = new System.Drawing.Size(902, 380);
            this.mA_NmPanel4.TabIndex = 40;
            this.mA_NmPanel4.Title = "";
            // 
            // source_tab
            // 
            this.source_tab.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(91)))), ((int)(((byte)(91)))), ((int)(((byte)(91)))));
            this.source_tab.BackgroundImage = global::NewsMontange.Properties.Resources.SRCTAB_Backg_Top;
            this.source_tab.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.source_tab.BorderFocusesColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(185)))), ((int)(((byte)(45)))));
            this.source_tab.Horizontal_aperture = 350;
            this.source_tab.IsFocused = false;
            this.source_tab.Location = new System.Drawing.Point(0, 0);
            this.source_tab.Name = "source_tab";
            this.source_tab.Padding = new System.Windows.Forms.Padding(1, 0, 0, 0);
            this.source_tab.Size = new System.Drawing.Size(435, 38);
            this.source_tab.TabIndex = 50;
            this.source_tab.Title = "";
            this.source_tab.Verical_aperture = 30;
            // 
            // lblGeneratingWaves
            // 
            this.lblGeneratingWaves.BackColor = System.Drawing.Color.Transparent;
            this.lblGeneratingWaves.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGeneratingWaves.Location = new System.Drawing.Point(0, 0);
            this.lblGeneratingWaves.Name = "lblGeneratingWaves";
            this.lblGeneratingWaves.Padding = new System.Windows.Forms.Padding(8, 2, 8, 0);
            this.lblGeneratingWaves.Size = new System.Drawing.Size(902, 372);
            this.lblGeneratingWaves.TabIndex = 1;
            // 
            // pnlPrjInfo
            // 
            this.pnlPrjInfo.Controls.Add(this.pnlMenuContainer);
            this.pnlPrjInfo.Controls.Add(this.lblProjectStandard);
            this.pnlPrjInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlPrjInfo.Location = new System.Drawing.Point(4, 0);
            this.pnlPrjInfo.Name = "pnlPrjInfo";
            this.pnlPrjInfo.Size = new System.Drawing.Size(1276, 25);
            this.pnlPrjInfo.TabIndex = 35;
            // 
            // pnlMenuContainer
            // 
            this.pnlMenuContainer.Controls.Add(this.menuStrip1);
            this.pnlMenuContainer.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlMenuContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlMenuContainer.Name = "pnlMenuContainer";
            this.pnlMenuContainer.Size = new System.Drawing.Size(583, 25);
            this.pnlMenuContainer.TabIndex = 3;
            // 
            // tmrProjectDurationUpdater
            // 
            this.tmrProjectDurationUpdater.Enabled = true;
            this.tmrProjectDurationUpdater.Interval = 1000;
            this.tmrProjectDurationUpdater.Tick += new System.EventHandler(this.tmrProjectDurationUpdater_Tick);
            // 
            // pnlStatus
            // 
            this.pnlStatus.Controls.Add(this.lblAutosave);
            this.pnlStatus.Controls.Add(this.pnlWaveWorking);
            this.pnlStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlStatus.Location = new System.Drawing.Point(4, 911);
            this.pnlStatus.Name = "pnlStatus";
            this.pnlStatus.Size = new System.Drawing.Size(1276, 25);
            this.pnlStatus.TabIndex = 51;
            // 
            // lblAutosave
            // 
            this.lblAutosave.AutoSize = true;
            this.lblAutosave.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAutosave.Location = new System.Drawing.Point(3, 4);
            this.lblAutosave.Name = "lblAutosave";
            this.lblAutosave.Size = new System.Drawing.Size(69, 17);
            this.lblAutosave.TabIndex = 1;
            this.lblAutosave.Text = "Autosave : ";
            // 
            // pnlWaveWorking
            // 
            this.pnlWaveWorking.BackColor = System.Drawing.Color.Gray;
            this.pnlWaveWorking.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlWaveWorking.Controls.Add(this.picWorking);
            this.pnlWaveWorking.Controls.Add(this.label2);
            this.pnlWaveWorking.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlWaveWorking.Location = new System.Drawing.Point(1113, 0);
            this.pnlWaveWorking.Name = "pnlWaveWorking";
            this.pnlWaveWorking.Size = new System.Drawing.Size(163, 25);
            this.pnlWaveWorking.TabIndex = 0;
            this.pnlWaveWorking.Visible = false;
            // 
            // picWorking
            // 
            this.picWorking.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picWorking.Image = global::NewsMontange.Properties.Resources.Working;
            this.picWorking.Location = new System.Drawing.Point(123, 0);
            this.picWorking.Name = "picWorking";
            this.picWorking.Size = new System.Drawing.Size(38, 23);
            this.picWorking.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picWorking.TabIndex = 1;
            this.picWorking.TabStop = false;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DarkRed;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 23);
            this.label2.TabIndex = 0;
            this.label2.Text = "Waveform Generating";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(1284, 936);
            this.Controls.Add(this.spMontageSources);
            this.Controls.Add(this.pnlStatus);
            this.Controls.Add(this.pnlPrjInfo);
            this.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(1300, 974);
            this.Name = "frmMain";
            this.Padding = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Media Alliance - News Montage";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Shown += new System.EventHandler(this.frmMain_Shown);
            this.ResizeBegin += new System.EventHandler(this.frmMain_ResizeBegin);
            this.ResizeEnd += new System.EventHandler(this.frmMain_ResizeEnd);
            this.SizeChanged += new System.EventHandler(this.frmMain_SizeChanged);
            this.mnuClip.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.pnlClipSplit.ResumeLayout(false);
            this.pnlClipSplit.PerformLayout();
            this.pnlClipRemover.ResumeLayout(false);
            this.pnlClipRemover.PerformLayout();
            this.pnlClipBlanker.ResumeLayout(false);
            this.pnlClipBlanker.PerformLayout();
            this.pnlBtnStepBW.ResumeLayout(false);
            this.pnlBtnStepBW.PerformLayout();
            this.pnlBtnStepFW.ResumeLayout(false);
            this.pnlBtnStepFW.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.pnlZoomAll.ResumeLayout(false);
            this.pnlZoomAll.PerformLayout();
            this.spOutprvOutTL.Panel1.ResumeLayout(false);
            this.spOutprvOutTL.Panel2.ResumeLayout(false);
            this.spOutprvOutTL.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.pnlPreviewMain.ResumeLayout(false);
            this.mA_NmPanel1.ResumeLayout(false);
            this.pnlMainButtons.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.mA_NmPanel2.ResumeLayout(false);
            this.mnuTimeline.ResumeLayout(false);
            this.pnlTLMain_Buttons.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.spMontageSources.Panel1.ResumeLayout(false);
            this.spMontageSources.Panel2.ResumeLayout(false);
            this.spMontageSources.ResumeLayout(false);
            this.spSrcprvSrcTL.Panel1.ResumeLayout(false);
            this.spSrcprvSrcTL.Panel2.ResumeLayout(false);
            this.spSrcprvSrcTL.ResumeLayout(false);
            this.mA_NmPanel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.mA_NmPanel4.ResumeLayout(false);
            this.pnlPrjInfo.ResumeLayout(false);
            this.pnlMenuContainer.ResumeLayout(false);
            this.pnlStatus.ResumeLayout(false);
            this.pnlStatus.PerformLayout();
            this.pnlWaveWorking.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picWorking)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MediaAlliance.Controls.MxpTimeLine.MxpTmeLine mxptl;
        private System.Windows.Forms.ContextMenuStrip mnuClip;
        private System.Windows.Forms.ToolStripMenuItem eliminaToolStripMenuItem;
        private System.Windows.Forms.SplitContainer spOutprvOutTL;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.Panel pnlTrackInfo;
        private System.Windows.Forms.ToolStripMenuItem internalLogToolStripMenuItem;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel preview_monitor_src;
        private System.Windows.Forms.SplitContainer spMontageSources;
        private System.Windows.Forms.Panel pnlPreviewMain;
        private MA_NmPanel mA_NmPanel1;
        private System.Windows.Forms.Panel picPreview1;
        private MA_NmPanel mA_NmPanel2;
        private MA_NmPanel mA_NmPanel3;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFilesource;
        private MA_NmPanel mA_NmPanel4;
        private System.Windows.Forms.Panel lblGeneratingWaves;
        private System.Windows.Forms.SplitContainer spSrcprvSrcTL;
        private MA_NmPanel_Tabs source_tab;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblCurrentClip;
        private System.Windows.Forms.ToolStripMenuItem startupMenuToolStripMenuItem;
        private MediaAlliance.Controls.MAButton btnPlay;
        private MediaAlliance.Controls.MAButton btnFrameREW;
        private MediaAlliance.Controls.MAButton btnFrameFFW;
        private MediaAlliance.Controls.MAButton btnPause;
        private System.Windows.Forms.Panel pnlMainButtons;
        private System.Windows.Forms.Panel pnlPrjInfo;
        private System.Windows.Forms.Label lblProjectStandard;
        private System.Windows.Forms.Panel panel1;
        private MediaAlliance.Controls.MAButton btn_FrameREWSrc;
        private MediaAlliance.Controls.MAButton btn_PlaySrc;
        private MediaAlliance.Controls.MAButton btn_FrameFFwSrc;
        private MediaAlliance.Controls.MAButton btn_PauseSrc;
        private MediaAlliance.Controls.MAButton btnZAll2;
        private MediaAlliance.Controls.MAButton btnClipCut2;
        private MediaAlliance.Controls.MAButton btnStepBW;
        private UserControls.MA_TimeCodeUC lblCurrentTC0;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem clipInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clipInfoToolStripMenuItem1;
        private UserControls.MA_TimeCodeUC lblDuration;
        private UserControls.MA_TimeCodeUC lblCurrentTC_SRC;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Panel pnlClipRemover;
        private MediaAlliance.Controls.MAButton btnClipRemover;
        private System.Windows.Forms.Panel pnlClipSplit;
        private System.Windows.Forms.Panel pnlClipBlanker;
        private MediaAlliance.Controls.MAButton btnBlank;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Panel pnlZoomAll;
        private System.Windows.Forms.Label lblOnlineTrack;
        private System.Windows.Forms.Timer tmrProjectDurationUpdater;
        private System.Windows.Forms.Panel pnlStatus;
        private System.Windows.Forms.Panel pnlWaveWorking;
        private System.Windows.Forms.PictureBox picWorking;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem projectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recoverBlankOnStartToolStripMenuItem;
        private System.Windows.Forms.Panel pnlTLMain_Buttons;
        private System.Windows.Forms.Panel pnlMenuContainer;
        private UserControls.MA_NmProjectInfo ProjectInfoUC;
        private System.Windows.Forms.ToolStripMenuItem rollbackToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mediaToolStripMenuItem;
        private System.Windows.Forms.Label lblAutosave;
        private System.Windows.Forms.ToolStripMenuItem autoFadeAudioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmAppAlert;
        private System.Windows.Forms.ToolStripMenuItem mediaAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preferenceToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exportAudioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmFieldsAlert;
        private System.Windows.Forms.ToolTip ttip;
        private System.Windows.Forms.Panel pnlBtnStepBW;
        private System.Windows.Forms.Panel pnlBtnStepFW;
        private MAButton btnStepFW;
        private System.Windows.Forms.ContextMenuStrip mnuTimeline;
        private System.Windows.Forms.ToolStripMenuItem markInToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem markOutToolStripMenuItem;
        private System.Windows.Forms.Panel panel2;
        private MAButton btnZoomFit;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem activationToolStripMenuItem;
    }
}

