﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewsMontange
{
    public class CVirtualClip
    {
        public long markin      = 0;
        public long markout     = 0;
        public long duration    = 0;
        public object Tag       = null;
    }

    public enum enFocusedArea
    {
        Undefined,
        MontageArea,
        OutputPreview,
        SourcePreview,
        SourceManager
    }

    [Flags]
    public enum enApplicationStatus
    { 
        undifined           = 0x0,
        recordingAudio      = 0x1
    }

    public enum enVideoStandards
    {
        custom,
        PAL,
        NTSC
    }
}
