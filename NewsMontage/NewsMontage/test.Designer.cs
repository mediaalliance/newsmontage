﻿using MediaAlliance.Controls;
namespace NewsMontange
{
    partial class test
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(test));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.mA_NmButton3 = new MediaAlliance.Controls.MAButton();
            this.mA_NmButton2 = new MediaAlliance.Controls.MAButton();
            this.mA_NmButton1 = new MediaAlliance.Controls.MAButton();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.radProgressBarElement1 = new Telerik.WinControls.UI.RadProgressBarElement();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.radMenu1 = new Telerik.WinControls.UI.RadMenu();
            this.radMenuItem1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2 = new Telerik.WinControls.UI.RadMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(26, 65);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(840, 85);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(191, 301);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(118, 121);
            this.listBox1.TabIndex = 4;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(26, 170);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(840, 85);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            this.pictureBox2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseMove);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(280, 301);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 33);
            this.button1.TabIndex = 6;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // mA_NmButton3
            // 
            this.mA_NmButton3.Image_Hover = ((System.Drawing.Image)(resources.GetObject("mA_NmButton3.Image_Hover")));
            this.mA_NmButton3.Image_Normal = null;
            this.mA_NmButton3.Image_Type = MediaAlliance.Controls.NmButtonIcon.Play;
            this.mA_NmButton3.Location = new System.Drawing.Point(128, 340);
            this.mA_NmButton3.Name = "mA_NmButton3";
            this.mA_NmButton3.Size = new System.Drawing.Size(45, 59);
            this.mA_NmButton3.TabIndex = 2;
            // 
            // mA_NmButton2
            // 
            this.mA_NmButton2.Enabled = false;
            this.mA_NmButton2.Image_Hover = ((System.Drawing.Image)(resources.GetObject("mA_NmButton2.Image_Hover")));
            this.mA_NmButton2.Image_Normal = null;
            this.mA_NmButton2.Image_Type = MediaAlliance.Controls.NmButtonIcon.Play;
            this.mA_NmButton2.Location = new System.Drawing.Point(77, 340);
            this.mA_NmButton2.Name = "mA_NmButton2";
            this.mA_NmButton2.Size = new System.Drawing.Size(45, 59);
            this.mA_NmButton2.TabIndex = 1;
            // 
            // mA_NmButton1
            // 
            this.mA_NmButton1.Image_Hover = ((System.Drawing.Image)(resources.GetObject("mA_NmButton1.Image_Hover")));
            this.mA_NmButton1.Image_Normal = null;
            this.mA_NmButton1.Image_Type = MediaAlliance.Controls.NmButtonIcon.Play;
            this.mA_NmButton1.Location = new System.Drawing.Point(26, 340);
            this.mA_NmButton1.Name = "mA_NmButton1";
            this.mA_NmButton1.Size = new System.Drawing.Size(45, 59);
            this.mA_NmButton1.TabIndex = 0;
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.AutoSize = true;
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radProgressBarElement1});
            this.radStatusStrip1.LayoutStyle = Telerik.WinControls.UI.RadStatusBarLayoutStyle.Stack;
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 469);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(1052, 28);
            this.radStatusStrip1.SizingGrip = false;
            this.radStatusStrip1.TabIndex = 7;
            this.radStatusStrip1.Text = "radStatusStrip1";
            // 
            // radProgressBarElement1
            // 
            this.radProgressBarElement1.Margin = new System.Windows.Forms.Padding(1);
            this.radProgressBarElement1.Name = "radProgressBarElement1";
            this.radStatusStrip1.SetSpring(this.radProgressBarElement1, false);
            this.radProgressBarElement1.Text = "radProgressBarElement1";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox1.Location = new System.Drawing.Point(514, 340);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(100, 96);
            this.richTextBox1.TabIndex = 8;
            this.richTextBox1.Text = "";
            // 
            // radMenu1
            // 
            this.radMenu1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem1});
            this.radMenu1.Location = new System.Drawing.Point(0, 0);
            this.radMenu1.Name = "radMenu1";
            this.radMenu1.Size = new System.Drawing.Size(1052, 24);
            this.radMenu1.TabIndex = 9;
            this.radMenu1.Text = "radMenu1";
            // 
            // radMenuItem1
            // 
            this.radMenuItem1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem2});
            this.radMenuItem1.Name = "radMenuItem1";
            this.radMenuItem1.Text = "radMenuItem1";
            // 
            // radMenuItem2
            // 
            this.radMenuItem2.Name = "radMenuItem2";
            this.radMenuItem2.Text = "FILE";
            this.radMenuItem2.Click += new System.EventHandler(this.radMenuItem2_Click);
            // 
            // test
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1052, 497);
            this.Controls.Add(this.radMenu1);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.radStatusStrip1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.mA_NmButton3);
            this.Controls.Add(this.mA_NmButton2);
            this.Controls.Add(this.mA_NmButton1);
            this.Name = "test";
            this.Text = "test";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MediaAlliance.Controls.MAButton mA_NmButton1;
        private MediaAlliance.Controls.MAButton mA_NmButton2;
        private MediaAlliance.Controls.MAButton mA_NmButton3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button button1;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.RadProgressBarElement radProgressBarElement1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private Telerik.WinControls.UI.RadMenu radMenu1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2;
    }
}