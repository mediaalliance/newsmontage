@Echo Off
::Set Mode=1 to Disable, Mode=0 to Enable as the default if nothing specified on the command line.
Set _Mode=0
If /I "%~1"=="Disable" Set _Mode=1
If /I "%~1"=="Enable" Set _Mode=0
Reg Add HKLM\SOFTWARE\Microsoft\DirectDraw /V EmulationOnly /T REG_DWORD /D %_Mode% /F
Reg Add HKLM\SOFTWARE\Microsoft\Direct3D\Drivers /V SoftwareOnly /T REG_DWORD /D %_Mode% /F